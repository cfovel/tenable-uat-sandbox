/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ProvisioningController {
    global ProvisioningController() {

    }
    @RemoteAction
    global static String createOrganization(String organizationName, String organizationTemplate, Boolean createStandard, Boolean createContent, Boolean createKnowledge, String refreshGuid) {
        return null;
    }
    @RemoteAction
    global static String linkToOrganization(String code) {
        return null;
    }
    @RemoteAction
    global static String toggleBetweenPlatforms() {
        return null;
    }
    @RemoteAction
    global static void updateIndexlessHandshake() {

    }
}
