<aura:component controller='CoveoV2.CoveoLightningApex' access='global' extensible='true' abstract='true'>
  <!-- Top level attributes, exposed through the .design file and editable in the community builder -->
  <aura:attribute name='name' type='String' access='global' />
  <aura:attribute name='searchHub' type='String' access='global' />
  <aura:attribute name='autoInitialize' type='Boolean' default='true' access='global' />
  <aura:attribute name='autoInjectBasicQuery' type='Boolean' default='true' access='global' />
  <aura:attribute name='autoInjectBasicOptions' type='Boolean' default='true' access='global' />
  <aura:attribute name='debug' type='Boolean' default='false' access='global' />

  <!-- External attributes not exposed in the community builder -->
  <aura:attribute name='customScripts' type='String' default="" access='global' />
  <aura:attribute name='loggerLevel' type='Integer' default="-1" access='global' />
  <aura:attribute name='searchContext' type='Object' access='global' />

  <!-- Internal attributes not exposed in the community builder -->
  <aura:attribute name='loader' type='String' access='global' />
  <aura:attribute name='repos' type='String[]' access='global' />
  <aura:attribute name='urlIframe' type='String' access='global' />
  <aura:attribute name='token' type='String' access='global' />
  <aura:attribute name='errorMessageDisplay' type='List' default='[]' access='global' />

  <aura:attribute name='isAdmin' type='Boolean' default='false' access='global' />
  <aura:attribute name='allowFallbackOnAdmin' type='Boolean' default='false' access='global' />
  <aura:attribute name='isPreviewMode' type='Boolean' access='global' />
  <aura:attribute name='displayAdvancedSettings' type='Boolean' default='false' access='global' />
  <aura:attribute name='interfaceLoaded' type='Boolean' default='false' access='global' />
  <aura:attribute name='interfaceInitialized' type='Boolean' default='false' access='global' />
  <aura:attribute name='initializationData' type='Object' access='global' />
  <aura:attribute name='isEndpointSet' type='Boolean' default='false' access='global' />
  <aura:attribute name='isResourcesLoaded' type='Boolean' default='false' access='global' />
  <aura:attribute name='isComponentScriptsLoaded' type='Boolean' default='false' access='global' />
  <aura:attribute name='isCustomScriptsLoaded' type='Boolean' default='false' access='global' />
  <aura:attribute name='isInterfaceEditorResourcesLoaded' type='Boolean' default='false' access='global' />
  <aura:attribute name='isLivePreviewBuilder' type='Boolean' access='global' default='false' />

  <!-- Protected Attributes -->
  <aura:attribute name='componentType' type='String' default="Search" access='public' />

   <!-- Public events. -->
  <aura:registerEvent name='ResourcesLoaded' type='CoveoV2:ResourcesLoaded' access='global' />
  <aura:registerEvent name='InterfaceEditorResourcesLoaded' type='CoveoV2:ResourcesLoaded' access='global' />
  <aura:registerEvent name='InterfaceCreatorLoaded' type='CoveoV2:InterfaceCreatorLoaded' access='global' />
  <aura:registerEvent name='InterfaceEditorToolboxLoaded' type='CoveoV2:InterfaceEditorToolboxLoaded' access='global' />
  <aura:registerEvent name='InterfaceContentLoaded' type='CoveoV2:InterfaceContentLoaded' access='global' />
  <aura:registerEvent name='InitializationDataLoaded' type='CoveoV2:InitializationDataLoaded' access='global' />
  <aura:registerEvent name='InterfaceContentInitialized' type='CoveoV2:InterfaceContentInitialized' access='global' />
  <aura:registerEvent name='GetSearchToken' type='CoveoV2:GetSearchToken' access='global' />

  <!-- Trigger change events. -->
  <aura:handler name="change" value="{!v.interfaceLoaded}" action="{!c.interfaceLoaded}"/>
  <aura:handler name="change" value="{!v.interfaceInitialized}" action="{!c.interfaceInitialized}"/>

  <aura:handler event="aura:doneRendering" action="{!c.doneRendering}"/>

  <!-- Initialization handlers -->
  <aura:handler name="init" value="{!this}" action="{!c.executeComponentInitilization}"/>

  <aura:handler name="change" value="{!v.isResourcesLoaded}" action="{!c.initializeComponent}"/>
  <aura:handler name="change" value="{!v.initializationData}" action="{!c.initializeComponent}"/>

  <aura:handler name="change" value="{!v.isInterfaceEditorResourcesLoaded}" action="{!c.initializeInterfaceEditor}"/>

  <!-- Watcher for setup of search page to init the editor interface -->
  <aura:handler name="change" value="{!v.loader}" action="{!c.initializeInterfaceEditor}"/>

  <!-- Attribute change rendering -->
  <aura:handler name="change" value="{!v.errorMessageDisplay}" action="{!c.handleErrorMessageDisplayChange}"/>
  <aura:handler name="change" value="{!v.allowFallbackOnAdmin}" action="{!c.handleAllowFallbackOnAdminChange}"/>
  <aura:handler name="change" value="{!v.isLivePreviewBuilder}" action="{!c.handleIsLivePreviewBuilderChange}"/>

  <aura:if isTrue="{!v.displayAdvancedSettings}" >
    <CoveoV2:AdvancedConfiguration pageName="{!v.name}" cancel="{!c.hideAdvancedSettings}" save="{!c.hideAdvancedSettings}" />
  </aura:if>

  <div aura:id='CoveoUsePreviewModeWarning' class='LightningWarningMessage'>To interact with this component, access the Preview mode.</div>
  <div id='CoveoFallbackToAdminWarning' aura:id='CoveoFallbackToAdminWarning' class='LightningWarningMessage'>
    You need to enable security on this component, as it could expose confidential information
    (see <a href="https://developers.coveo.com/x/owkvAg">Allowing User Impersonation</a>).
  </div>
  <div id='CoveoPageCreator' aura:id='CoveoPageCreator' class='CoveoPageCreator hidden'></div>
  <div id='CoveoPageDeleter' aura:id='CoveoPageDeleter' class='CoveoPageDeleter'></div>
  <div id='CoveoPageContent' aura:id='CoveoPageContent' class='CoveoPageContent'></div>

  <!-- Error Display section for both User and Admin error messages -->
  <aura:if isTrue="{!not(empty(v.errorMessageDisplay))}">
    <div class="visible errorMessageDisplay">
      <div class='coveo-sprites-coveo-logo logo-in-error-title'></div>
      <hr/>
      <aura:iteration items="{!v.errorMessageDisplay}" var="error" indexVar="index">
        <h2>{!error.title}</h2>
        <h3>
          <aura:unescapedHtml value="{!error.message}"/>
        </h3>
        <hr/>
      </aura:iteration>
    </div>
  </aura:if>

  <!-- The body of the sub component will normally be the resources of the JSSearch : scripts/css -->
  {!v.body}

  <div id='CoveoInterfaceEditorToolbox' aura:id='CoveoInterfaceEditorToolbox'></div>
</aura:component>