({
  /**
   /**
   * Execute the component initialization.
   * At this point, the UI isn't rendered and the scripts are not loaded.
   *
   * @param  {Component} cmp  The component
   * @param  {Event} event    The event
   * @param  {Object} helper  The component helper funcitons
   */
  executeComponentInitilization: function() {
    'use strict'

    // Fix for Salesforce Polyfill problems with promises in IE11.
    // We will be able to remove that when Salesforce will update their promise
    // implementation or that es6-promise adds a new condition.
    window.salesforceOriginalPromise = window.Promise;
  },

  /**
   * onScriptLoaded - Action fired when the script are loaded
   *
   * @param  {Component} cmp  The component
   * @param  {Event} event    The event
   * @param  {Object} helper  The component helper funcitons
   */
  onScriptLoaded: function (cmp, event, helper) {
    'use strict'

    // Fix for Salesforce Polyfill problems with promises in IE11.
    // We will be able to remove that when Salesforce will update their promise
    // implementation or that es6-promise adds a new condition.
    if (window.salesforceOriginalPromise !== undefined) {
      window.Promise = window.salesforceOriginalPromise;
    }

    var searchHub = cmp.get('v.searchHub');
    var name = cmp.get('v.name');
    var searchPageName = cmp.get('v.searchPageName');
    var placeholder = cmp.get('v.placeholder');
    var enableQuerySuggestAddon = cmp.get('v.enableQuerySuggestAddon');
    var triggerQueryOnClear = cmp.get('v.triggerQueryOnClear');
    var enableQuerySyntax = cmp.get('v.enableQuerySyntax');
    var enableFieldAddon = cmp.get('v.enableFieldAddon');
    var enableQueryExtensionAddon = cmp.get('v.enableQueryExtensionAddon');

    var customTokenGeneration = cmp.get('v.customTokenGeneration');
    var deferredToken = Coveo.$.Deferred();

    if (customTokenGeneration) {
      helper.debugLog(cmp, 'Custom token generation');
      var getTokenEvent = cmp.getEvent('GetSearchToken');

      getTokenEvent.setParams({
        'deferred': deferredToken
      });

      getTokenEvent.fire();
    }

    var getInitializationAction = cmp.get('c.getToken');
    var searchInterfaceElement = Coveo.$('#standaloneSearchbox');

    getInitializationAction.setParams({
      searchHub: searchHub,
      name: name
    });

    getInitializationAction.setCallback(this, function (initializationResponse) {
      helper.debugLog(cmp, 'getInitializationAction returned');
      var parsed = JSON.parse(initializationResponse.getReturnValue());

      deferredToken.done(function (searchToken) {
        Coveo.SearchEndpoint.endpoints[name] = new Coveo.SearchEndpoint({
          restUri: parsed.platformUri + '/rest/search/',
          accessToken: searchToken,
          isGuestUser: parsed.isGuestUser
        });

        helper.debugLog(cmp, 'Searchbox initialization');
        searchInterfaceElement.coveo('init', {
          SearchInterface: {
            endpoint: Coveo.SearchEndpoint.endpoints[name],
            enableHistory: false,
            autoTriggerQuery: false
          },
          Analytics: {
            token: searchToken,
            endpoint: parsed.analyticUri,
            searchHub: searchHub
          },
          StandaloneSearchbox: {
            searchPageName: searchPageName,
            placeholder: placeholder,
            enableQuerySuggestAddon: enableQuerySuggestAddon,
            triggerQueryOnClear: triggerQueryOnClear,
            enableQuerySyntax: enableQuerySyntax,
            enableFieldAddon: enableFieldAddon,
            enableQueryExtensionAddon: enableQueryExtensionAddon
          }
        });
      });

      if (!customTokenGeneration) {
        if (parsed.errorTitle) {
          helper.debugLog(cmp, 'Initialization returned with error');

          var errorTitle = Coveo.$('#errorTitle');
          var errorDiv = Coveo.$('#error');

          errorDiv.show();
          searchInterfaceElement.find('input').attr('disabled', true);
          errorTitle.text(parsed.errorTitle);
        } else {
          helper.debugLog(cmp, 'Resolving with default token');
          deferredToken.resolve(parsed.token);
        }
      }
    });

    var externalSearchInterface = Coveo.$('#search:not(.CoveoCaseCreationInterface)');
    if (externalSearchInterface) {
      externalSearchInterface.on('afterInitialization', function () {
        searchInterfaceElement.trigger('searchPageInitialized',
          { searchInterface: externalSearchInterface.coveo(Coveo.SearchInterface) }
        );
      });
    }

    $A.enqueueAction(getInitializationAction);
  }
})