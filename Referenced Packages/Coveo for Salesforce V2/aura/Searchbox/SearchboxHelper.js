({
  /**
   * Logs the messages if the component is in `debug` mode
   *
   * @param  {Component} cmp                  Component
   * @param  {String} message                 Log message
   */
  debugLog: function (cmp, message) {
    'use strict';

    if (typeof cmp !== 'object' && message !== undefined && typeof cmp === 'string') {
      console.log('DEVELOPER YOU NEED TO SPECIFY THE COMPONENT TO THE DEBUGLOG METHOD')
      console.log(message);
    } else if (cmp.get('v.debug')) {
      console.log('[' + new Date().toISOString() + '] ' + message);
    }
  }
})