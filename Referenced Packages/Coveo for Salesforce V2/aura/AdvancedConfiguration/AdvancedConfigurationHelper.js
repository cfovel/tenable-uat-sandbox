({
  /**
   * userIdToString - Transforms a userID object to a string
   *
   * @param  {Object}  userIds    userID object
   * @return {String}  String     representation of userID
   */
  userIdToString: function(userIds) {
    'use strict';
    var ids = [];

    _.each(userIds, function(userId) {
      ids.push('name=' + userId.name + ',provider=' + userId.provider + ',type=' + userId.type);
    });

    return ids.join(';');
  }
})