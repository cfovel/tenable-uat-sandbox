({
  /**
   * onInit - Initialization of the component
   *
   * @param  {Component} cmp  The component
   * @param  {Event} event    The event
   * @param  {Object} helper  The component helper funcitons
   */
  onInit: function(cmp, event, helper) {
    'use strict';

    console.log('Loading advanced settings');
    var getCurrentConfigAction = cmp.get('c.getAdvancedConfiguration');

    getCurrentConfigAction.setParams({
      name: cmp.get('v.pageName')
    });
    getCurrentConfigAction.setCallback(this, function(response) {
      if (response.getState() === 'SUCCESS') {
        var parsed = JSON.parse(response.getReturnValue());

        // Check for null values
        if (parsed.filter === null) {
          parsed.filter = undefined;
        }
        if (parsed.userGroups === null) {
          parsed.userGroups = [';'];
        }
        cmp.set('v.filter', parsed.filter || '');
        cmp.set('v.anonymousUser', parsed.anonymousUser);
        cmp.set('v.additionalUserIdentities', helper.userIdToString(parsed.additionalUserIdentities));
        cmp.set('v.userGroups', parsed.userGroups.join(';'));
        cmp.set('v.customTokenGeneration', parsed.customTokenGeneration);
      }
    });
    $A.enqueueAction(getCurrentConfigAction);
  },

  /**
   * save - Save action
   *
   * @param  {Component} cmp   The Component
   */
  save: function (cmp) {
    'use strict';

    console.log('Saving advanced settings');
    var filter = cmp.find('filter').get('v.value');
    var anonymousUser = cmp.find('anonymousUser').get('v.value');
    var additionalUserIdentities = cmp.find('additionalUserIdentities').get('v.value');
    var userGroups = cmp.find('userGroups').get('v.value');
    var customTokenGeneration = cmp.find('customTokenGeneration').get('v.value');
    var name = cmp.get('v.pageName');
    var saveAction = cmp.get('c.saveAdvancedConfiguration');

    saveAction.setParams({
      name: name,
      filter: filter,
      anonymousUser: anonymousUser,
      additionalUserIdentities: additionalUserIdentities,
      userGroups: userGroups,
      customTokenGeneration: customTokenGeneration
    });
    saveAction.setCallback(this, function(response) {
      if (response.getState() === 'SUCCESS') {
        cmp.getEvent('save').fire();

        window.location.reload();
      }
    });
    $A.enqueueAction(saveAction);
  },

  /**
   * cancel - Cancel press.
   *
   * @param  {Component} cmp   The Component
   */
  cancel: function(cmp) {
    'use strict';
    cmp.getEvent('cancel').fire();
  }
});