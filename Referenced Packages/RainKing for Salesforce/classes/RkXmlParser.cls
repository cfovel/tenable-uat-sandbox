/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class RkXmlParser {
global class CompanyList {
    global List<rkpi2.RkXmlParser.CompanyListCompany> companyListCompany;
    global CompanyList() {

    }
}
global class CompanyListCompany {
    global String companyId;
    global String companyName;
    global String version;
    global CompanyListCompany() {

    }
}
global class CompanyTech {
    global String techGroup;
    global String techList;
    global CompanyTech() {

    }
}
global class ContactGroup {
    global String contactListId;
    global String contactListName;
    global String dateCreated;
    global ContactGroup() {

    }
}
global class ContactList {
    global List<rkpi2.RkXmlParser.ContactListContact> contactListContact;
    global ContactList() {

    }
}
global class ContactListContact {
    global String city;
    global String contactId;
    global String firstName;
    global String lastName;
    global String state;
    global String title;
    global String version;
    global ContactListContact() {

    }
}
global class DirectReport {
    global String contactId;
    global String location;
    global String name;
    global String title;
    global DirectReport() {

    }
}
global class Education {
    global String degree;
    global String graduationYear;
    global String major;
    global String name;
    global Education() {

    }
}
global class Employment {
    global String endDate;
    global String name;
    global String startDate;
    global String title;
    global Employment() {

    }
}
global class Location {
    global String address;
    global String city;
    global String country;
    global String fax;
    global String hq;
    global String location;
    global String phone;
    global String state;
    global String zip;
    global Location() {

    }
}
global class MyCompany {
    global String address1;
    global String address2;
    global String city;
    global String companyId;
    global String companyIntelligence;
    global String companyName;
    global List<rkpi2.RkXmlParser.CompanyTech> companyTechs;
    global String country;
    global String dunsNumber;
    global String employeeCount;
    global String fax;
    global String fye;
    global String generalInfo;
    global String industry;
    global String itBudget;
    global String itEmployeeCount;
    global String itManagerCount;
    global Datetime lastUpdated;
    global List<rkpi2.RkXmlParser.Location> locations;
    global String owningOrgId;
    global String phone;
    global List<rkpi2.RkXmlParser.Researcher> researcher;
    global String revenue;
    global String sector;
    global String state;
    global String url;
    global String version;
    global String zip;
    global MyCompany() {

    }
}
global class MyContact {
    global String address1;
    global String address2;
    global String city;
    global List<rkpi2.RkXmlParser.ContactGroup> contactGroups;
    global String contactId;
    global String country;
    global List<rkpi2.RkXmlParser.DirectReport> directReports;
    global List<rkpi2.RkXmlParser.Education> educationHistory;
    global String email;
    global String ext;
    global String fax;
    global String firstName;
    global String intelligence;
    global String lastName;
    global String level;
    global String nickName;
    global List<rkpi2.RkXmlParser.PeRanking> peRankings;
    global String phone;
    global String salutation;
    global String state;
    global String supervisor;
    global List<rkpi2.RkXmlParser.Technology> technologies;
    global String title;
    global String version;
    global List<rkpi2.RkXmlParser.Employment> workHistory;
    global String zip;
    global MyContact() {

    }
}
global class MyDescription {
    global String msg;
    global MyDescription() {

    }
}
global class MyScoops {
    global List<rkpi2.RkXmlParser.Scoop> scoop;
    global MyScoops() {

    }
}
global class MyStatus {
    global String id;
    global String name;
    global MyStatus() {

    }
}
global class PeRanking {
    global String name;
    global String rank;
    global PeRanking() {

    }
}
global class Researcher {
    global String email;
    global String firstName;
    global String lastName;
    global String researchId;
    global Researcher() {

    }
}
global class Scoop {
    global String insideScoopId;
    global String news;
    global String scoopDate;
    global String scoopType;
    global String typeId;
    global String version;
    global Scoop() {

    }
}
global class Technology {
    global String myGroup;
    global String name;
    global Technology() {

    }
}
}
