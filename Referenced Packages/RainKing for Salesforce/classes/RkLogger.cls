/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class RkLogger {
global class RkLog {
    global Integer cid;
    global Integer pid;
    global String recordType;
    global String tab;
    global String target;
    global RkLog() {

    }
}
}
