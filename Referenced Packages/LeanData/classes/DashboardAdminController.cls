/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class DashboardAdminController {
    @RemoteAction
    global static String checkCODAStatus(Long lastCODASuccessTimeStamp) {
        return null;
    }
    @RemoteAction
    global static Map<String,String> downloadDataFiles(String userId) {
        return null;
    }
    @RemoteAction
    global static Long executeCODA() {
        return null;
    }
    @RemoteAction
    global static String getEncryptedConfig() {
        return null;
    }
    @RemoteAction
    global static Map<String,Object> leadUpdate(String leadId, String accountId, Boolean isSimple) {
        return null;
    }
    @RemoteAction
    global static String rescheduleJob() {
        return null;
    }
    @RemoteAction
    global static String updateConfig(String configString) {
        return null;
    }
}
