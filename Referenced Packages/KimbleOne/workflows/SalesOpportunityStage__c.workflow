<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SalesOppStageApprovalStatusApproved</fullName>
        <description>Set Sales Opportunity Stage Approval Status to Approved.</description>
        <field>ApprovalStatus__c</field>
        <formula>&quot;Approved&quot;</formula>
        <name>SalesOppStageApprovalStatusApproved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SalesOppStageApprovalStatusRejected</fullName>
        <description>Set Sales Opportunity Stage Approval Status to Rejected.</description>
        <field>ApprovalStatus__c</field>
        <formula>&quot;Rejected&quot;</formula>
        <name>SalesOppStageApprovalStatusRejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>TemplateSalesOppStageFieldUpdates</fullName>
        <actions>
            <name>SalesOppStageApprovalStatusApproved</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SalesOppStageApprovalStatusRejected</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>SalesOpportunityStage__c.ApprovalStatus__c</field>
            <operation>equals</operation>
            <value>X</value>
        </criteriaItems>
        <description>Template workflow to deploy Field Updates.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
