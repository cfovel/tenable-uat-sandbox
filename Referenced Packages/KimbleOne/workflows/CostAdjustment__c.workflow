<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CostAdjustApprovalStatusApproved</fullName>
        <description>Set Cost Adjustment Approval Status to Approved.</description>
        <field>ApprovalStatus__c</field>
        <formula>&quot;Approved&quot;</formula>
        <name>CostAdjustApprovalStatusApproved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CostAdjustApprovalStatusRejected</fullName>
        <description>Set Cost Adjustment Approval Status to Rejected.</description>
        <field>ApprovalStatus__c</field>
        <formula>&quot;Rejected&quot;</formula>
        <name>CostAdjustApprovalStatusRejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>TemplateCostAdjustmentFieldUpdates</fullName>
        <actions>
            <name>CostAdjustApprovalStatusApproved</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CostAdjustApprovalStatusRejected</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>CostAdjustment__c.ApprovalStatus__c</field>
            <operation>equals</operation>
            <value>X</value>
        </criteriaItems>
        <description>Template workflow to deploy Field Updates</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
