<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>PeriodRevenueApprovalStatusApproved</fullName>
        <description>Set PeriodRevenue ApprovalStatus to Approved.</description>
        <field>ApprovalStatus__c</field>
        <formula>&quot;Approved&quot;</formula>
        <name>PeriodRevenueApprovalStatusApproved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PeriodRevenueApprovalStatusRejected</fullName>
        <description>Set PeriodRevenue ApprovalStatus to Rejected.</description>
        <field>ApprovalStatus__c</field>
        <formula>&quot;Rejected&quot;</formula>
        <name>PeriodRevenueApprovalStatusRejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>TemplatePeriodRevenueFieldUpdates</fullName>
        <actions>
            <name>PeriodRevenueApprovalStatusApproved</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PeriodRevenueApprovalStatusRejected</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>PeriodRevenue__c.ApprovalStatus__c</field>
            <operation>equals</operation>
            <value>X</value>
        </criteriaItems>
        <description>Template workflow to deploy Field Updates.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
