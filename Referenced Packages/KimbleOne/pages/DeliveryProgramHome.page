<apex:page standardcontroller="KimbleOne__DeliveryProgram__c" extensions="KimbleOne.DeliveryProgramHomeController" sidebar="false" >

<apex:composition template="KimbleOne__SiteMaster">

<apex:define name="Menu">

	<c:Menu DefaultHelpPage="DeliveryProgams.html" MenuContextType="DeliveryProgram" MenuContextId="{!KimbleOne__DeliveryProgram__c.Id}"  MenuContextName="{!KimbleOne__DeliveryProgram__c.Name}"/>

</apex:define>
     
<apex:define name="Content">
    
	<chatter:feedWithFollowers entityId="{!KimbleOne__DeliveryProgram__c.Id}"/>
	
	<!-- Ajax loaded pages don't load external resources so they have to be loaded in the parent page -->
	<apex:includeScript value="{!URLFOR($Resource.KimbleOne__jQueryBarGraph, 'jqBarGraph.1.1.min.js')}"/>
	<apex:stylesheet value="{!URLFOR($Resource.KimbleOne__KimbleStyles, 'CardStyle.css')}"/>
	<apex:stylesheet value="{!URLFOR($Resource.KimbleOne__KimbleStyles, 'CharmStyle.css')}"/>
	<apex:includeScript value="{!URLFOR($Resource.KimbleOne__KimbleJS, 'Charm.js')}"/>
 
	<div id="dashboard-layout" class="dashboardLayout">

		<div id="dashboardContainer" class="dashboardContainer">
			<apex:pageMessages escape="false"/>

			<div class="full-width-dashboard-container">
				<div id="engagementStatusContainer" context-id="{!KimbleOne__DeliveryProgram__c.Id}" pageSource="DeliveryGroupStatusCard" class="cardContainer"></div>
				<div id="revenueSummaryContainer" context-id="{!KimbleOne__DeliveryProgram__c.Id}" display-currency="{!JSENCODE(TheDisplayCurrencyIsoCode)}" pageSource="RevenueSummaryCard" class="cardContainer"></div>
				<div id="marginSummaryContainer" context-id="{!KimbleOne__DeliveryProgram__c.Id}" display-currency="{!JSENCODE(TheDisplayCurrencyIsoCode)}" pageSource="MarginSummaryCard" class="cardContainer"></div>
			</div>
			
			<div id="financialSummaryContainer" context-id="{!KimbleOne__DeliveryProgram__c.Id}" display-currency="{!JSENCODE(TheDisplayCurrencyIsoCode)}" pageSource="DeliveryProgramFinancialSummaryCard" class="cardContainer"></div>
		</div>
	</div>

	<script>	
		jQuery(document).ready(function()
		{							
			jQuery.each( jQuery('.dashboardContainer'), function(i, dashboardContainer) {
					jQuery('div[pageSource]', dashboardContainer).each(function() {
			   		var dashboardComponent = jQuery( this );
			   		var pageSourceName = dashboardComponent.attr('pageSource');
			   		
			   		// kick off the population of the page with an ajax call - always provide ID, only provide currency if on the source element
			   		var displayCurrencyParam = '';
			   		if(dashboardComponent.attr('display-currency') != '')
			   		{
			   			displayCurrencyParam = '&currencyISOCode=' + dashboardComponent.attr('display-currency');
			   		}
			   		
					jQuery.ajax({ url: kimble.pages[pageSourceName] + dashboardComponent.attr('context-id') + displayCurrencyParam})
						.done(function(html) {
							if(html != null && html != '')
							{
								jQuery(html).appendTo("#" + dashboardComponent.attr('id'));
							}
							else
							{
								// nothing to render in the component
								// remove the container for this component to allow the page to re-flow
								dashboardComponent.remove();
							}
						})
						.fail(function(xhr, status, error) {
							if(error !=null && error !="") {
								console.log('ajax component load error: ' + error);
								// hide the container for this component
								dashboardComponent.hide();
							}									
						})				
			   });
			})			
			
		});
	</script>	
			
	<style>
		
		#engagementStatusContainer {
			overflow:hidden;
			height: 208px;
			display: table-cell;
			width: 100%;
		}
			
		#revenueSummaryContainer {
			display: table-cell;
			height: 208px;
		}
		
		#marginSummaryContainer {
			display: table-cell;
			height: 208px;
		}
		
		#riskSummaryContainer, #issueSummaryContainer {
			display: table-cell;
			width: 50%;
		}
				
		/* ensure that the financial summary spans the screen */
		#financialSummaryContainer, 
		#financialSummaryContainer .card,
		.cardContainer .card
		{
			display: block;
		}
		
		.full-width-dashboard-container {
			display: table;
			width: 100%;
			margin-bottom: 5px;
		}
									
		.cardBodyTable
		{
			width: 100%;
		}
		
		/* override SF .message to fit in with dashboard style */
		.message
		{
			margin: 3px 2px;
		}
		.dashboard-footer {
			text-align: center;
			background-color: #F5F5F5;
			border-radius: 4px;
			-webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);
			box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);
			padding-top: 3px;
			margin-left: 2px;
			margin-right: 2px;
		}
		.delivery-group-print-icon {
			font-size: 18px;
			position: relative;
			left: -26px;
			top: -8px;
			color: #6F6F6F;
		}
		.primary-element-container {
		    display: inline-block;
		    vertical-align: top;
		    margin-right:8px;
		}
		
		.engagement-name {
			width: 222px;
		}
		
		.rightAlignDataCell {
		    text-align: right;
		}
		
	    .DeliveryStatus
	    {
			font-size: 18px;
			float: right;
		  	color: #ddd;
	    }
	   	    
		.DeliveryStatusRed {color: #E50000}
		.DeliveryStatusAmber {color: #FFB732}
		.DeliveryStatusGreen {color: #32b42a}
	</style>

</apex:define>

</apex:composition>

</apex:page>