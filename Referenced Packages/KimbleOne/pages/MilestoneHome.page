<apex:page standardcontroller="KimbleOne__Milestone__c" extensions="KimbleOne.MilestoneHomeController,KimbleOne.SecurityController" standardStylesheets="true" showHeader="true" sidebar="false" >
  	<apex:Composition template="KimbleOne__SiteMaster">
		<apex:define name="Menu">
			<c:Menu DefaultHelpPage="Milestones.html" MenuContextType="DeliveryGroup" MenuContextId="{!KimbleOne__Milestone__c.DeliveryElement__r.KimbleOne__DeliveryGroup__c}"  MenuContextName="{!KimbleOne__Milestone__c.DeliveryElement__r.DeliveryGroup__r.Name}" CurrentPage="{!$Page.KimbleOne__DeliveryGroupMilestones}" ChildName="{!KimbleOne__Milestone__c.Name}"/>
		</apex:define>
		<apex:define name="Content">
			
			<apex:form id="TheForm">
					<apex:pageMessages escape="false"/>
					<apex:pageBlock title="{!$Label.kimbleone__MilestoneHeading} - {!KimbleOne__Milestone__c.Name}">
			 		    	<apex:pageBlockButtons location="top">
			 		    		<apex:outputPanel layout="none" rendered="{!$ObjectType.KimbleOne__Milestone__c.updateable}">
			 		    			<apex:outputPanel layout="none" rendered="{!KimbleOne__Milestone__c.MilestoneStatus__r.KimbleOne__Enum__c = 'Open' && !IsSupplierRequisitioned}">
			 		    				<input class="btn" type="button" value="{!$Label.Edit}" onclick="initialiseModalBoxy('#DeliveryElementManageMilestonesEditPopup', '{!$Label.Edit} {!$ObjectType.Milestone__c.label}');showModalBoxy();"/>
			 		    			</apex:outputPanel>
			 		    		
				 		    		<apex:outputPanel layout="none" rendered="{!ISBLANK(KimbleOne__Milestone__c.KimbleOne__Event__c) && KimbleOne__Milestone__c.DeliveryElement__r.KimbleOne__ProbabilityCodeEnum__c = 'P1' && KimbleOne__Milestone__c.MilestoneStatus__r.KimbleOne__Enum__c = 'Open' && !RequiresSupplierRequisition}">
				 		    			<input class="btn" type="button" value="{!$Label.kimbleone__Complete}" onclick="initialiseModalBoxy('#DeliveryElementManageMilestonesCompletePopup', '{!$Label.kimbleone__Complete} {!$ObjectType.Milestone__c.label}');showModalBoxy();"/>
				 		    		</apex:outputPanel>
			 		    		
				 		    		<apex:outputPanel layout="none" rendered="{!KimbleOne__Milestone__c.DeliveryElement__r.KimbleOne__ProbabilityCodeEnum__c = 'P1' && KimbleOne__Milestone__c.MilestoneStatus__r.KimbleOne__Enum__c = 'Open' && RequiresSupplierRequisition && IsSupplierRequisitioned && IsSupplierRequisitionApproved}">
			 			    			<apex:outputPanel layout="none" rendered="{!SupplierRequisitionType = 'Actualisation'}">
			 			    				<input class="btn" type="button" value="{!$Label.kimbleone__Complete}" onclick="initialiseModalBoxy('#DeliveryElementManageMilestonesCompletePopup', '{!$Label.kimbleone__Complete} {!$ObjectType.Milestone__c.label}');showModalBoxy();"/>
			 		    				</apex:outputPanel>
			 		    			
			 		    				<apex:outputPanel layout="none" rendered="{!SupplierRequisitionType = 'GoodsReceivedNote'}">
			 		    					<input class="btn" type="button" value="{!$Label.ReceiveGoodsLabel}" onclick="safeNavigateToSForceURL('/{!$ObjectType.GoodsReceivedNote__c.KeyPrefix}');"/>
			 		    				</apex:outputPanel>
			 		    		
				 		    			<apex:outputPanel layout="none" rendered="{!SupplierRequisitionType = 'SupplierInvoice'}">
				 		    				<input class="btn" type="button" value="{!$Label.ReceiveSupplierInvoiceLabel}" onclick="safeNavigateToSForceURL('/{!$ObjectType.SupplierInvoice__c.KeyPrefix}');"/>
				 		    			</apex:outputPanel>
				 		    		</apex:outputPanel>
			 					
			 		    		
			 		    			<apex:commandButton action="{!RevertMilestone}"  rerender="TheForm, MilestoneApprovalHistory" rendered="{!CanRevertMilestone && HasPermission['RevertCompletedMilestone']}" value="{!$Label.kimbleone__RevertToDraft}"/>
			 		    		
			 		    		</apex:outputPanel>
			 		    	</apex:pageBlockButtons>
			 		    	
			 		    	<apex:pageBlockSection columns="2">
									<apex:outputField value="{!KimbleOne__Milestone__c.Name}"/>
									
									<apex:pageBlockSectionItem >
										<apex:outputLabel value="{!$ObjectType.KimbleOne__Milestone__c.Fields.KimbleOne__MilestoneType__c.Label}"></apex:outputLabel>
										<apex:outputField value="{!KimbleOne__Milestone__c.MilestoneType__r.Name}"/>																				
									</apex:pageBlockSectionItem>
									
									<apex:outputField value="{!KimbleOne__Milestone__c.KimbleOne__Event__c}" />
									
									<apex:outputField value="{!KimbleOne__Milestone__c.KimbleOne__MilestoneDate__c}" rendered="{!ISBLANK(PlanMilestone)}"/>
									
									<apex:outputField value="{!PlanMilestone.KimbleOne__TrackingPlan__c}" rendered="{!!ISBLANK(PlanMilestone)}"/>
									
									<apex:outputField value="{!PlanMilestone.KimbleOne__BaselineDate__c}" rendered="{!!ISBLANK(PlanMilestone)}"/>
									
									<apex:outputField value="{!PlanMilestone.KimbleOne__EstimatedDate__c}" rendered="{!!ISBLANK(PlanMilestone)}"/>
									
									<apex:outputField value="{!PlanMilestone.KimbleOne__CompletionDate__c}" rendered="{!!ISBLANK(PlanMilestone)}"/>
									
									<apex:pageBlockSectionItem >
										<apex:outputLabel value="{!$ObjectType.KimbleOne__Milestone__c.Fields.KimbleOne__MilestoneValue__c.Label}"></apex:outputLabel>
										<apex:outputPanel layout="none">
											<apex:outputPanel layout="none" rendered="{!KimbleOne__Milestone__c.MilestoneType__r.KimbleOne__Enum__c == 'Revenue'}">
												<apex:outputField value="{!KimbleOne__Milestone__c.KimbleOne__InvoicingCurrencyIsoCode__c}"/>&nbsp;
												<apex:outputField value="{!KimbleOne__Milestone__c.KimbleOne__InvoicingCurrencyMilestoneValue__c}"/>
											</apex:outputPanel>
											<apex:outputPanel layout="none" rendered="{!KimbleOne__Milestone__c.MilestoneType__r.KimbleOne__Enum__c == 'Cost'}">
												<apex:outputField value="{!KimbleOne__Milestone__c.KimbleOne__MilestoneValue__c}"/>
											</apex:outputPanel>	
										</apex:outputPanel>																		
									</apex:pageBlockSectionItem>
									
									<apex:pageBlockSectionItem rendered="{!KimbleOne__Milestone__c.DeliveryElement__r.Product__r.ProductDomain__r.RevenueGenerationModel__r.KimbleOne__IsUnitGenerated__c}">
										<apex:outputLabel value="{!$ObjectType.KimbleOne__Milestone__c.Fields.KimbleOne__MilestoneUnits__c.Label}"></apex:outputLabel>
										<apex:outputField value="{!KimbleOne__Milestone__c.KimbleOne__MilestoneUnits__c}"/>																				
									</apex:pageBlockSectionItem>
									
									<apex:pageBlockSectionItem rendered="{!KimbleOne__Milestone__c.DeliveryElement__r.Product__r.ProductDomain__r.RevenueGenerationModel__r.KimbleOne__IsUnitGenerated__c}">
										<apex:outputLabel value="{!$ObjectType.KimbleOne__DeliveryElement__c.Fields.KimbleOne__RevenueUnitType__c.Label}"></apex:outputLabel>
										<apex:outputField value="{!KimbleOne__Milestone__c.DeliveryElement__r.RevenueUnitType__r.Name}"/>																				
									</apex:pageBlockSectionItem>
									
									<apex:pageBlockSectionItem rendered="{!KimbleOne__Milestone__c.DeliveryElement__r.Product__r.ProductDomain__r.CostGenerationModel__r.KimbleOne__IsUnitGenerated__c}">
										<apex:outputLabel value="{!$ObjectType.KimbleOne__Milestone__c.Fields.KimbleOne__MilestoneUnits__c.Label}"></apex:outputLabel>
										<apex:outputField value="{!KimbleOne__Milestone__c.KimbleOne__MilestoneUnits__c}"/>																				
									</apex:pageBlockSectionItem>
									
									<apex:pageBlockSectionItem rendered="{!KimbleOne__Milestone__c.DeliveryElement__r.Product__r.ProductDomain__r.CostGenerationModel__r.KimbleOne__IsUnitGenerated__c}">
										<apex:outputLabel value="{!$ObjectType.KimbleOne__DeliveryElement__c.Fields.KimbleOne__CostUnitType__c.Label}"></apex:outputLabel>
										<apex:outputField value="{!KimbleOne__Milestone__c.DeliveryElement__r.CostUnitType__r.Name}"/>																				
									</apex:pageBlockSectionItem>
									
									<apex:pageBlockSectionItem >
										<apex:outputLabel value="{!$ObjectType.KimbleOne__Milestone__c.Fields.KimbleOne__MilestoneStatus__c.Label}"></apex:outputLabel>
										<apex:outputField value="{!KimbleOne__Milestone__c.MilestoneStatus__r.Name}"/>																				
									</apex:pageBlockSectionItem>
									
									<apex:outputField value="{!KimbleOne__Milestone__c.KimbleOne__Notes__c}"/>
									
									<apex:repeat value="{!$ObjectType.KimbleOne__Milestone__c.FieldSets.KimbleOne__MilestoneDetails}" var="f">
     									<apex:outputField value="{!KimbleOne__Milestone__c[f]}"/>
   									</apex:repeat>
      			
							</apex:pageBlockSection>		
			 			</apex:pageBlock>
				</apex:form>
				
				<div id="DeliveryElementManageMilestonesEditPopup" style="display:none"> 
					<apex:outputPanel id="EditMilestone">
			     	  <apex:form >
			     	      	<div id="EditMilestoneErrors">
			           				<apex:pageMessages escape="false"/>
			           			</div>
			     	      	<apex:pageBlock >
			           		<apex:pageBlockButtons location="bottom">
								<apex:commandButton styleclass="DisableButtonWhileBusy" action="{!SaveMilestone}" value="{!$Label.kimbleone__Save}" oncomplete="hideModalBoxyWithErrorCheck('#EditMilestoneErrors')" reRender="EditMilestone, TheForm"/>
								<input class="btn" type="button" onclick="hideModalBoxy();" value="{!$Label.Cancel}"/>
							</apex:pageBlockButtons>
		             		<apex:pageblockSection collapsible="false" showheader="false" columns="1">               
		               			<apex:inputfield value="{!KimbleOne__Milestone__c.Name}" required="true" /> 
		                		
		                		<apex:pageBlockSectionItem rendered="{!ISBLANK(PlanMilestone) && KimbleOne__Milestone__c.DeliveryElement__r.Product__r.ProductDomain__r.RevenueGenerationModel__r.KimbleOne__IsUnitGenerated__c == false && KimbleOne__Milestone__c.MilestoneType__r.KimbleOne__Enum__c == 'Revenue'}">
				                   	<apex:outputLabel >{!$ObjectType.KimbleOne__Milestone__c.Fields.KimbleOne__MilestoneValue__c.Label} {!KimbleOne__Milestone__c.KimbleOne__InvoicingCurrencyIsoCode__c}</apex:outputLabel>
				                   	<apex:inputField styleclass="MilestoneValueTextEdit" value="{!KimbleOne__Milestone__c.KimbleOne__InvoicingCurrencyMilestoneValue__c}" required="true"/>
								</apex:pageBlockSectionItem>
								
								<apex:pageBlockSectionItem rendered="{!KimbleOne__Milestone__c.DeliveryElement__r.Product__r.ProductDomain__r.CostGenerationModel__r.KimbleOne__IsUnitGenerated__c == false && KimbleOne__Milestone__c.MilestoneType__r.KimbleOne__Enum__c == 'Cost'}">
				                   	<apex:outputLabel >{!$ObjectType.KimbleOne__Milestone__c.Fields.KimbleOne__MilestoneValue__c.Label} {!KimbleOne__Milestone__c.CurrencyIsoCode}</apex:outputLabel>
				                   	<apex:inputField styleclass="MilestoneValueTextEdit" value="{!KimbleOne__Milestone__c.KimbleOne__MilestoneValue__c}" required="true"/>
								</apex:pageBlockSectionItem>
		               			
		               			<apex:pageBlockSectionItem rendered="{!ISBLANK(PlanMilestone) && KimbleOne__Milestone__c.DeliveryElement__r.Product__r.ProductDomain__r.RevenueGenerationModel__r.KimbleOne__IsUnitGenerated__c == true && KimbleOne__Milestone__c.MilestoneType__r.KimbleOne__Enum__c == 'Revenue'}">
				                   	<apex:outputLabel >{!$ObjectType.KimbleOne__Milestone__c.Fields.KimbleOne__UnitValue__c.Label} {!KimbleOne__Milestone__c.KimbleOne__InvoicingCurrencyIsoCode__c}</apex:outputLabel>
				                   	<apex:inputField styleclass="MilestoneValueTextEdit" value="{!KimbleOne__Milestone__c.KimbleOne__InvoicingCurrencyUnitValue__c}" required="true"/>
								</apex:pageBlockSectionItem>
		               			
		               			<apex:pageBlockSectionItem rendered="{!KimbleOne__Milestone__c.DeliveryElement__r.Product__r.ProductDomain__r.CostGenerationModel__r.KimbleOne__IsUnitGenerated__c == true && KimbleOne__Milestone__c.MilestoneType__r.KimbleOne__Enum__c == 'Cost'}">
				                   	<apex:outputLabel >{!$ObjectType.KimbleOne__Milestone__c.Fields.KimbleOne__UnitValue__c.Label} {!KimbleOne__Milestone__c.CurrencyIsoCode}</apex:outputLabel>
				                   	<apex:inputField styleclass="MilestoneValueTextEdit" value="{!KimbleOne__Milestone__c.KimbleOne__UnitValue__c}" required="true"/>
								</apex:pageBlockSectionItem>
		               			
		               			<apex:inputfield styleclass="datePickerPopup" value="{!KimbleOne__Milestone__c.KimbleOne__MilestoneDate__c}" required="true" rendered="{!ISBLANK(PlanMilestone)}"/>
		               			
		               			<apex:inputField styleclass="datePickerPopup" value="{!PlanMilestone.KimbleOne__EstimatedDate__c}" required="true" rendered="{!!ISBLANK(PlanMilestone)}"/>
		               			
								<apex:pageBlockSectionItem rendered="{!KimbleOne__Milestone__c.DeliveryElement__r.Product__r.ProductDomain__r.RevenueGenerationModel__r.KimbleOne__IsUnitGenerated__c}">
									<apex:outputLabel value="{!$ObjectType.KimbleOne__Milestone__c.Fields.KimbleOne__MilestoneUnits__c.Label}"></apex:outputLabel>
									<apex:inputField value="{!KimbleOne__Milestone__c.KimbleOne__MilestoneUnits__c}" required="true"/>																				
								</apex:pageBlockSectionItem>
		               			
		               			<apex:pageBlockSectionItem rendered="{!KimbleOne__Milestone__c.DeliveryElement__r.Product__r.ProductDomain__r.CostGenerationModel__r.KimbleOne__IsUnitGenerated__c}">
									<apex:outputLabel value="{!$ObjectType.KimbleOne__Milestone__c.Fields.KimbleOne__MilestoneUnits__c.Label}"></apex:outputLabel>
									<apex:inputField value="{!KimbleOne__Milestone__c.KimbleOne__MilestoneUnits__c}" required="true"/>																				
								</apex:pageBlockSectionItem>
		               			
		               			<apex:inputField value="{!KimbleOne__Milestone__c.KimbleOne__Notes__c}"/>
		              			               			                
				            </apex:pageBlockSection>
			        	</apex:pageBlock>
					</apex:form>
        		</apex:outputPanel> 
			</div>
			
			<div id="DeliveryElementManageMilestonesCompletePopup" style="display:none"> 
					<apex:outputPanel id="CompleteMilestone">
			     	  <apex:form >
			     	      	<div id="CompleteMilestoneErrors">
			           				<apex:pageMessages escape="false"/>
			           			</div>
			     	      	<apex:pageBlock >
			           		<apex:pageBlockButtons location="bottom">
								<apex:commandButton styleclass="DisableButtonWhileBusy" action="{!CompleteCostMilestone}" value="{!$Label.kimbleone__Complete}" oncomplete="hideModalBoxyWithErrorCheck('#CompleteMilestoneErrors')" rendered="{!KimbleOne__Milestone__c.MilestoneType__r.KimbleOne__Enum__c = 'Cost'}" reRender="CompleteMilestone, TheForm" />
								<apex:commandButton styleclass="DisableButtonWhileBusy" action="{!CompleteRevenueMilestone}" value="{!$Label.kimbleone__Complete}" oncomplete="hideModalBoxyWithErrorCheck('#CompleteMilestoneErrors')" rendered="{!KimbleOne__Milestone__c.MilestoneType__r.KimbleOne__Enum__c = 'Revenue'}" reRender="CompleteMilestone, TheForm" />
								
								<input class="btn" type="button" onclick="hideModalBoxy();" value="{!$Label.Cancel}"/>
							</apex:pageBlockButtons>
		             		<apex:pageblockSection collapsible="false" showheader="false" columns="1">               
		               			
		               			<apex:inputField styleclass="datePickerPopup" value="{!PlanMilestone.KimbleOne__CompletionDate__c}" required="true" rendered="{!!ISBLANK(PlanMilestone)}"/>
		               			
								<apex:inputField value="{!KimbleOne__Milestone__c.KimbleOne__Notes__c}" style="width:200px" />
												              			               			                
				            </apex:pageBlockSection>
			        	</apex:pageBlock>
					</apex:form>
        		</apex:outputPanel> 
			</div>
				
				<apex:relatedList list="NotesAndAttachments" />
					
				<!-- Approval History -->
				<apex:relatedList list="ProcessSteps" id="MilestoneApprovalHistory" rendered="{!!RequiresSupplierRequisition}"></apex:relatedList>
				<script>
						$(document).ready(function(){
    						$(function() {
    							hideSalesForceApprovalProcessButtons();
    						})
    					});
    					function hideSalesForceApprovalProcessButtons()
    					{
    							$('input[name="piSubmit"]').remove();
    							$('input[name="piRemove"]').remove();
    					}
				</script>
		</apex:define>
	</apex:Composition>
</apex:page>