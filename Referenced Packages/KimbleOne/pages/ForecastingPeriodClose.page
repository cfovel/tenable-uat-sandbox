<apex:page controller="KimbleOne.ForecastingPeriodCloseController" extensions="KimbleOne.SecurityController" sidebar="false" tabstyle="PeriodManagement__tab">
 
<apex:composition template="KimbleOne__SiteMaster">

<apex:define name="Menu">

	<c:Menu DefaultHelpPage="CloseForecastingPeriod.html" MenuContextType="Organisation" MenuContextName="Period Management"/>
	
</apex:define>

<apex:define name="Content">
	<apex:includeScript value="{!URLFOR($Resource.KimbleOne__jQueryBarGraph, 'jqBarGraph.1.1.min.js')}"/>   
    <apex:pageMessages escape="false"/>
 
	<apex:form >  
      	<div>
				<div style="position: relative;">
					
					<span style="font-weight:bold;position:relative;top:-10px;">{!timePeriod.Name}&nbsp;&nbsp;&nbsp;{<apex:outputField value="{!timePeriod.KimbleOne__StartDate__c}"/>&nbsp;-&nbsp;<apex:outputField value="{!timePeriod.KimbleOne__EndDate__c}"/>}</span>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<apex:outputLink value="#" onclick="GoPreviousPeriod()" title="{!$Label.kimbleone__PreviousPeriod}">
							<apex:image url="{!URLFOR($Resource.KimbleOne__AssignmentImages, 'PreviousButtonSquare.png')}"
								alt="{!$Label.kimbleone__PreviousPeriod}" width="30" height="30" />
						</apex:outputLink> 
						
						<span>
							<apex:inputText styleClass="datepicker" style="width:0px;border:0px; margin:0px; padding:0px;" value="{!GoDate.KimbleOne__InputDate__c}">
								<apex:actionSupport event="onchange" action="{!ForecastingPeriodChange}" />
							</apex:inputText>
							<script>
							$(window).load(function(){
								
									$('.datepicker').datepicker({
										dateFormat: kimble.regional.dateFormat,
										showOtherMonths: true,
										selectOtherMonths: true,
										buttonText: '{!$Label.OpenTimePeriodDropdownToolTip}',
										buttonImageOnly: true,
										buttonImage: '{!URLFOR($Resource.AssignmentImages, 'CalendarButtonSquare.png')}',
										showOn: "button",
										});
								});
						</script></span>
						
						<apex:outputLink value="#" onclick="GoNextPeriod()" title="{!$Label.kimbleone__NextPeriod}">
							<apex:image url="{!URLFOR($Resource.KimbleOne__AssignmentImages, 'NextButtonSquare.png')}"
								alt="{!$Label.kimbleone__NextPeriod}" width="30" height="30" />
						</apex:outputLink>
						
				</div>
         		
			</div>
      	<apex:pageBlock title="{!$Label.kimbleone__PeriodSummaryLabel} ({!timePeriod.BusinessUnitGroup__r.CurrencyISOCode})">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					
					<td width="32%" valign="top" class="graphText" style="background-color:#FFF">
						<div id="divForUncommittedRevenueGraph" class="graphText"></div>
					</td>
					<td width="1%"></td>
					<td width="32%" valign="top" class="graphText" style="background-color:#FFF">
						<div id="divForUnRecognisedRevenuesGraph" class="graphText"></div>
					</td>
					<td width="1%"></td>
					<td width="32%" valign="top" class="graphText" style="background-color:#FFF">
						<div id="divForUnRecognisedCostsGraph" class="graphText"></div>
					</td>
					<td width="1%"></td>
				</tr>
			</table>
			
		</apex:pageBlock>
		
		<script>
		
	 	$(document).ready(function(){
		 	var summaryData = jQuery.parseJSON('<apex:outputText value="{!JSENCODE(ThePeriodSummaryJson)}"/>');
		 	
		 	var uncommittedRevenueGraphData = new Array(
		   		 [summaryData.UncommittedRevenueSales,'<a href="javascript:void(0)" onclick="changeView(\'UncommittedRevenueSales\')">' + summaryData.UncommittedSalesOpportunitiesCount + ' {!$ObjectType.KimbleOne__Proposal__c.LabelPlural}</a>','#B42D34'],
		   	 	 [summaryData.UncommittedRevenueDeliveryGroup,'<a href="javascript:void(0)" onclick="changeView(\'UncommittedRevenueDeliveryGroup\')">' + summaryData.UncommittedDeliveryGroupsCount + ' {!$ObjectType.KimbleOne__DeliveryGroup__c.LabelPlural}</a>','#E58E1A'],
		   	 	 [summaryData.UncommittedRevenueDeliveryElement,'<a href="javascript:void(0)" onclick="changeView(\'UncommittedRevenueDeliveryElement\')">' + summaryData.UncommittedDeliveryElementsCount + ' {!$ObjectType.KimbleOne__DeliveryElement__c.LabelPlural}</a>','#E58E1A']);
		   	 	 
		 	$('#divForUncommittedRevenueGraph').jqBarGraph({ data: uncommittedRevenueGraphData,title: '<div class="graphTitle">{!$Label.UncommittedRevenueLabel}</div>', height:150, width:360, animate: false, barSpace: 6}); 
		 	
		 	jQuery.each($('div.graphValuedivForUncommittedRevenueGraph'),function(i,obj){
		 		$(obj).text($.formatNumber($(obj).text(), {format: "#,##0", locale:kimble.locale, isFullLocale:false}));
		 	});
		 	
		 	var unrecognisedRevenuesGraphData = new Array(
		   		 [summaryData.UnrecognisedMilestoneRevenues,'<a href="javascript:void(0)" onclick="changeView(\'UnrecognisedMilestoneRevenue\')">' + summaryData.UnrecognisedMilestoneRevenuesCount + ' {!$ObjectType.Milestone__c.LabelPlural}</a>','#729BC7'],
		   	 	 [summaryData.UnrecognisedPeriodRevenues,'<a href="javascript:void(0)" onclick="changeView(\'UnrecognisedPeriodRevenue\')">' + summaryData.UnrecognisedPeriodRevenuesCount + ' {!$ObjectType.KimbleOne__PeriodRevenue__c.LabelPlural}</a>','#729BC7'],
		   	 	 [summaryData.UnrecognisedPerElementRevenues,'<a href="javascript:void(0)" onclick="changeView(\'UnrecognisedPerElementRevenue\')">' + summaryData.UnrecognisedPerElementRevenuesCount + ' {!$ObjectType.KimbleOne__DeliveryElement__c.LabelPlural}</a>','#729BC7']);
		   	 	 
		 	$('#divForUnRecognisedRevenuesGraph').jqBarGraph({ data: unrecognisedRevenuesGraphData,title: '<div class="graphTitle">{!$Label.UnrecognisedRevenueLabel}</div>', height:150, width:330, animate: false, barSpace: 6}); 
		 	
		 	jQuery.each($('div.graphValuedivForUnRecognisedRevenuesGraph'),function(i,obj){
		 		$(obj).text($.formatNumber($(obj).text(), {format: "#,##0", locale:kimble.locale, isFullLocale:false}));
		 	});
		 	
		 	var unrecognisedCostsGraphData = new Array(
		   		 [summaryData.UnrecognisedMilestoneCosts,'<a href="javascript:void(0)" onclick="changeView(\'UnrecognisedMilestoneCost\')">' + summaryData.UnrecognisedMilestoneCostsCount + ' {!$ObjectType.Milestone__c.LabelPlural}</a>','#729BC7'],
		   	 	 [summaryData.UnrecognisedPeriodCosts,'<a href="javascript:void(0)" onclick="changeView(\'UnrecognisedPeriodCost\')">' + summaryData.UnrecognisedPeriodCostsCount + ' {!$ObjectType.KimbleOne__PeriodCost__c.LabelPlural}</a>','#729BC7']);
		   	 	 
		 	$('#divForUnRecognisedCostsGraph').jqBarGraph({ data: unrecognisedCostsGraphData,title: '<div class="graphTitle">{!$Label.UnrecognisedCostsLabel}</div>', height:150, width:230, animate: false, barSpace: 6}); 
		 	
		 	jQuery.each($('div.graphValuedivForUnRecognisedCostsGraph'),function(i,obj){
		 		$(obj).text($.formatNumber($(obj).text(), {format: "#,##0", locale:kimble.locale, isFullLocale:false}));
		 	});
		 	
	 	});
	 	
	 	
	 	
	</script>	
	<style>
		.graphText {font-size: 10px;white-space: nowrap;font-weight: bold;color: #4A4A56;}
		.graphTitle {font-size: 12px;white-space: nowrap;font-weight: bold;color: #4A4A56;margin-bottom:4px; width:260px;}
		.graphLabeldivForUncommittedRevenueGraph{height:20px;}
		.graphLabeldivForUnRecognisedRevenuesGraph{height:20px;}
		.graphLabeldivForUnRecognisedCostsGraph{height:20px;}
		
		
	</style>
      	
      	<apex:actionStatus id="ViewLoadingStatus">
			<apex:facet name="start">
				<div style="margin-top:20px; margin-bottom:20px;margin-left:12px">
					<span style="font-weight: bold;color: #4A4A56;font-size: 91%;"><img src="{!URLFOR($Resource.KimbleOne__LoadingImage)}" style="margin-right: 8px;position: relative;top: 2px;"/>{!$Label.kimbleone__Loading}</span>
				</div>
			</apex:facet>
			<apex:facet name="stop">
				
			<apex:pageBlock id="TheViews" rendered="{!TheSelectedView != ''}">
			<apex:outputPanel layout="none" rendered="{!TheSelectedView = UncommittedRevenueSales}">
				
				<apex:pageBlockSection collapsible="false" columns="1" title="{!$ObjectType.KimbleOne__Proposal__c.LabelPlural}" ></apex:pageBlockSection>
			
				<apex:pageBlockTable value="{!ThePagedUncommittedRevenueSales.TheRecords}" var="record">
	          		<apex:column value="{!record.Proposal.KimbleOne__Account__c}"/>               		
              		<apex:column headerValue="{!$ObjectType.KimbleOne__Proposal__c.Fields.Name.Label}"> 
		  				<apex:outputLink value="/{!record.Proposal.Id}" >{!record.Proposal.Name}</apex:outputLink>		
					</apex:column>					
					<apex:column value="{!record.Proposal.KimbleOne__AcceptanceDate__c}" />
					<apex:column value="{!record.Proposal.KimbleOne__ForecastStatus__c}" />
					<apex:column value="{!record.ContractRevenue.KimbleOne__InputCurrency__c}" headerValue="{!$ObjectType.KimbleOne__Proposal__c.Fields.KimbleOne__ContractRevenue__c.Label}"/>
					<apex:column value="{!record.Proposal.OwnerId}" />  
	            </apex:pageBlockTable>
       
	            <div style="float:left; margin-top:15px;">
					<apex:outputText value="{!ThePagedUncommittedRevenueSales.CurrentPageDescription}"/>
				</div>
				<div style="float: right; margin-right: 15px; margin-top:15px;">
					    	<apex:commandLink style="color: #015BA7;text-decoration: none;" action="{!GoPrevious}" rerender="TheViews" rendered="{!ThePagedUncommittedRevenueSales.CanGoPrevious}" value="{!$Label.kimbleone__PreviousPageLink}"/>
					    	<apex:outputText styleclass="greyedLink" rendered="{!!ThePagedUncommittedRevenueSales.CanGoPrevious}" value="{!$Label.kimbleone__PreviousPageLink}"/>
					    	&nbsp;|&nbsp;
					    	<apex:commandLink style="color: #015BA7;text-decoration: none;" action="{!GoNext}" rerender="TheViews" rendered="{!ThePagedUncommittedRevenueSales.CanGoNext}" value="{!$Label.kimbleone__NextPageLink}"/>
					    	<apex:outputText styleclass="greyedLink" rendered="{!!ThePagedUncommittedRevenueSales.CanGoNext}" value="{!$Label.kimbleone__NextPageLink}"/>
				</div>
				<div style="clear:both;"></div>
				
			</apex:outputPanel>
			
			<apex:outputPanel layout="none" rendered="{!TheSelectedView = UncommittedRevenueDeliveryGroup}">
				
				<apex:pageBlockSection collapsible="false" columns="1" title="{!$ObjectType.KimbleOne__DeliveryGroup__c.LabelPlural}" ></apex:pageBlockSection>
			
				<apex:pageBlockTable value="{!ThePagedUncommittedRevenueDeliveryGroup.TheRecords}" var="record">
	          		<apex:column value="{!record.DeliveryGroup__r.KimbleOne__Account__c}" headerValue="{!$ObjectType.Account.Fields.Name.Label}"/>               		
              		<apex:column headerValue="{!$ObjectType.KimbleOne__Proposal__c.Fields.Name.Label}">
              			<apex:outputField value="{!record.KimbleOne__Proposal__c}" rendered="{!!ISBLANK(record.KimbleOne__Proposal__c)}"/>
              			<apex:outputField value="{!record.DeliveryGroup__r.KimbleOne__Proposal__c}" rendered="{!ISBLANK(record.KimbleOne__Proposal__c)}"/>
              		</apex:column> 
		  			<apex:column headerValue="{!$ObjectType.KimbleOne__DeliveryGroup__c.Fields.Name.Label}">
						<apex:outputLink value="/{!record.KimbleOne__DeliveryGroup__c}" >{!record.DeliveryGroup__r.Name}</apex:outputLink>		
					</apex:column>
															
					<apex:column value="{!record.DeliveryGroup__r.OwnerId}" headerValue="{!$ObjectType.KimbleOne__DeliveryGroup__c.Fields.OwnerId.Label}"/>               		              		            
					<apex:column value="{!record.KimbleOne__ContractRevenue__c}" headerValue="{!$ObjectType.KimbleOne__DeliveryGroup__c.Fields.KimbleOne__ContractRevenue__c.Label}"/>               		              		            
					<apex:column value="{!record.KimbleOne__StartDate__c}" headerValue="{!$ObjectType.KimbleOne__DeliveryGroup__c.Fields.KimbleOne__ExpectedStartDate__c.Label}"/>   
	            </apex:pageBlockTable>
       
	            <div style="float:left; margin-top:15px;">
					<apex:outputText value="{!ThePagedUncommittedRevenueDeliveryGroup.CurrentPageDescription}"/>
				</div>
				<div style="float: right; margin-right: 15px; margin-top:15px;">
					    	<apex:commandLink style="color: #015BA7;text-decoration: none;" action="{!GoPrevious}" rerender="TheViews" rendered="{!ThePagedUncommittedRevenueDeliveryGroup.CanGoPrevious}" value="{!$Label.kimbleone__PreviousPageLink}"/>
					    	<apex:outputText styleclass="greyedLink" rendered="{!!ThePagedUncommittedRevenueDeliveryGroup.CanGoPrevious}" value="{!$Label.kimbleone__PreviousPageLink}"/>
					    	&nbsp;|&nbsp;
					    	<apex:commandLink style="color: #015BA7;text-decoration: none;" action="{!GoNext}" rerender="TheViews" rendered="{!ThePagedUncommittedRevenueDeliveryGroup.CanGoNext}" value="{!$Label.kimbleone__NextPageLink}"/>
					    	<apex:outputText styleclass="greyedLink" rendered="{!!ThePagedUncommittedRevenueDeliveryGroup.CanGoNext}" value="{!$Label.kimbleone__NextPageLink}"/>
				</div> 
				<div style="clear:both;"></div>
				
			</apex:outputPanel>
			
			<apex:outputPanel layout="none" rendered="{!TheSelectedView = UncommittedRevenueDeliveryElement}">
				
				<apex:pageBlockSection collapsible="false" columns="1" title="{!$ObjectType.KimbleOne__DeliveryElement__c.LabelPlural}" ></apex:pageBlockSection>
			
				<apex:pageBlockTable value="{!ThePagedUncommittedRevenueDeliveryElement.TheRecords}" var="record">
	          		<apex:column value="{!record.DeliveryGroup__r.KimbleOne__Account__c}" headerValue="{!$ObjectType.Account.Fields.Name.Label}"/>               		
              		<apex:column headerValue="{!$ObjectType.KimbleOne__Proposal__c.Fields.Name.Label}">
              			<apex:outputField value="{!record.KimbleOne__Proposal__c}" rendered="{!!ISBLANK(record.KimbleOne__Proposal__c)}"/>
              			<apex:outputField value="{!record.DeliveryGroup__r.KimbleOne__Proposal__c}" rendered="{!ISBLANK(record.KimbleOne__Proposal__c)}"/>
              		</apex:column> 
		  			<apex:column headerValue="{!$ObjectType.KimbleOne__DeliveryGroup__c.Fields.Name.Label}">
						<apex:outputLink value="/{!record.KimbleOne__DeliveryGroup__c}" >{!record.DeliveryGroup__r.Name}</apex:outputLink>		
					</apex:column>
					<apex:column headerValue="{!$ObjectType.KimbleOne__DeliveryElement__c.Fields.Name.Label}">
						<apex:outputText value="{!record.Name}" />		
					</apex:column>
										
					<apex:column value="{!record.DeliveryGroup__r.OwnerId}" headerValue="{!$ObjectType.KimbleOne__DeliveryGroup__c.Fields.OwnerId.Label}"/>               		              		            
					<apex:column value="{!record.KimbleOne__ContractRevenue__c}" headerValue="{!$ObjectType.KimbleOne__DeliveryGroup__c.Fields.KimbleOne__ContractRevenue__c.Label}"/>               		              		            
					<apex:column value="{!record.KimbleOne__StartDate__c}" headerValue="{!$ObjectType.KimbleOne__DeliveryGroup__c.Fields.KimbleOne__ExpectedStartDate__c.Label}"/>   
	            </apex:pageBlockTable>
       
	            <div style="float:left; margin-top:15px;">
					<apex:outputText value="{!ThePagedUncommittedRevenueDeliveryElement.CurrentPageDescription}"/>
				</div>
				<div style="float: right; margin-right: 15px; margin-top:15px;">
					    	<apex:commandLink style="color: #015BA7;text-decoration: none;" action="{!GoPrevious}" rerender="TheViews" rendered="{!ThePagedUncommittedRevenueDeliveryElement.CanGoPrevious}" value="{!$Label.kimbleone__PreviousPageLink}"/>
					    	<apex:outputText styleclass="greyedLink" rendered="{!!ThePagedUncommittedRevenueDeliveryElement.CanGoPrevious}" value="{!$Label.kimbleone__PreviousPageLink}"/>
					    	&nbsp;|&nbsp;
					    	<apex:commandLink style="color: #015BA7;text-decoration: none;" action="{!GoNext}" rerender="TheViews" rendered="{!ThePagedUncommittedRevenueDeliveryElement.CanGoNext}" value="{!$Label.kimbleone__NextPageLink}"/>
					    	<apex:outputText styleclass="greyedLink" rendered="{!!ThePagedUncommittedRevenueDeliveryElement.CanGoNext}" value="{!$Label.kimbleone__NextPageLink}"/>
				</div> 
				<div style="clear:both;"></div>
				
			</apex:outputPanel>
			
			<apex:outputPanel layout="none" rendered="{!TheSelectedView = UnrecognisedMilestoneRevenue}">
				
				<apex:pageBlockSection collapsible="false" columns="1" title="{!$Label.kimbleone__Revenue} {!$ObjectType.KimbleOne__Milestone__c.LabelPlural}" ></apex:pageBlockSection>
			
				<apex:pageBlockTable value="{!ThePagedUnrecognisedMilestoneRevenues.TheRecords}" var="record">
	          		   <apex:column value="{!record.Milestone.DeliveryElement__r.DeliveryGroup__r.KimbleOne__Account__c}" headerValue="{!$ObjectType.Account.Fields.Name.Label}"/> 
              		<apex:column headerValue="{!$ObjectType.KimbleOne__DeliveryGroup__c.Fields.Name.Label}"> 
						<apex:outputLink value="{!$Page.KimbleOne__DeliveryGroupMilestones}?id={!record.Milestone.DeliveryElement__r.KimbleOne__DeliveryGroup__c}" >{!record.Milestone.DeliveryElement__r.DeliveryGroup__r.Name}</apex:outputLink>
					</apex:column>
					<apex:column value="{!record.Milestone.Name}" headerValue="{!$ObjectType.KimbleOne__Milestone__c.Fields.Name.Label}"/> 
					<apex:column value="{!record.Milestone.KimbleOne__MilestoneDate__c}" headerValue="{!$ObjectType.KimbleOne__Milestone__c.Fields.KimbleOne__MilestoneDate__c.Label}"/> 
              		<apex:column value="{!record.Milestone.KimbleOne__MilestoneValue__c}" headerValue="{!$ObjectType.KimbleOne__Milestone__c.Fields.KimbleOne__MilestoneValue__c.Label}"/> 
              		<apex:column value="{!record.Milestone.KimbleOne__MilestoneStatus__c}" headerValue="{!$ObjectType.KimbleOne__Milestone__c.Fields.KimbleOne__MilestoneStatus__c.Label}"/> 
              		<apex:column value="{!record.Milestone.KimbleOne__ApprovalStatus__c}" headerValue="{!$ObjectType.KimbleOne__Milestone__c.Fields.KimbleOne__ApprovalStatus__c.Label}"/> 
           			<apex:column value="{!record.PendingStep.ActorId}"/>
	            </apex:pageBlockTable>
       
	            <div style="float:left; margin-top:15px;">
					<apex:outputText value="{!ThePagedUnrecognisedMilestoneRevenues.CurrentPageDescription}"/>
				</div>
				<div style="float: right; margin-right: 15px; margin-top:15px;">
					    	<apex:commandLink style="color: #015BA7;text-decoration: none;" action="{!GoPrevious}" rerender="TheViews" rendered="{!ThePagedUnrecognisedMilestoneRevenues.CanGoPrevious}" value="{!$Label.kimbleone__PreviousPageLink}"/>
					    	<apex:outputText styleclass="greyedLink" rendered="{!!ThePagedUnrecognisedMilestoneRevenues.CanGoPrevious}" value="{!$Label.kimbleone__PreviousPageLink}"/>
					    	&nbsp;|&nbsp;
					    	<apex:commandLink style="color: #015BA7;text-decoration: none;" action="{!GoNext}" rerender="TheViews" rendered="{!ThePagedUnrecognisedMilestoneRevenues.CanGoNext}" value="{!$Label.kimbleone__NextPageLink}"/>
					    	<apex:outputText styleclass="greyedLink" rendered="{!!ThePagedUnrecognisedMilestoneRevenues.CanGoNext}" value="{!$Label.kimbleone__NextPageLink}"/>
				</div> 
				<div style="clear:both;"></div>
				
			</apex:outputPanel>
			
			<apex:outputPanel layout="none" rendered="{!TheSelectedView = UnrecognisedMilestoneCost}">
				
				<apex:pageBlockSection collapsible="false" columns="1" title="{!$Label.kimbleone__Cost} {!$ObjectType.KimbleOne__Milestone__c.LabelPlural}" ></apex:pageBlockSection>
			
				<apex:pageBlockTable value="{!ThePagedUnrecognisedMilestoneCosts.TheRecords}" var="record">
	          		   <apex:column value="{!record.Milestone.DeliveryElement__r.DeliveryGroup__r.KimbleOne__Account__c}" headerValue="{!$ObjectType.Account.Fields.Name.Label}"/> 
              		<apex:column headerValue="{!$ObjectType.KimbleOne__DeliveryGroup__c.Fields.Name.Label}"> 
						<apex:outputLink value="{!$Page.KimbleOne__DeliveryGroupMilestones}?id={!record.Milestone.DeliveryElement__r.KimbleOne__DeliveryGroup__c}" >{!record.Milestone.DeliveryElement__r.DeliveryGroup__r.Name}</apex:outputLink>
					</apex:column>
					<apex:column value="{!record.Milestone.Name}" headerValue="{!$ObjectType.KimbleOne__Milestone__c.Fields.Name.Label}"/> 
					<apex:column value="{!record.Milestone.KimbleOne__MilestoneDate__c}" headerValue="{!$ObjectType.KimbleOne__Milestone__c.Fields.KimbleOne__MilestoneDate__c.Label}"/> 
              		<apex:column value="{!record.Milestone.KimbleOne__MilestoneValue__c}" headerValue="{!$ObjectType.KimbleOne__Milestone__c.Fields.KimbleOne__MilestoneValue__c.Label}"/> 
              		<apex:column value="{!record.Milestone.KimbleOne__MilestoneStatus__c}" headerValue="{!$ObjectType.KimbleOne__Milestone__c.Fields.KimbleOne__MilestoneStatus__c.Label}"/> 
              		<apex:column value="{!record.Milestone.KimbleOne__ApprovalStatus__c}" headerValue="{!$ObjectType.KimbleOne__Milestone__c.Fields.KimbleOne__ApprovalStatus__c.Label}"/> 
           			<apex:column value="{!record.PendingStep.ActorId}"/>
	            </apex:pageBlockTable>
       
	            <div style="float:left; margin-top:15px;">
					<apex:outputText value="{!ThePagedUnrecognisedMilestoneCosts.CurrentPageDescription}"/>
				</div>
				<div style="float: right; margin-right: 15px; margin-top:15px;">
					    	<apex:commandLink style="color: #015BA7;text-decoration: none;" action="{!GoPrevious}" rerender="TheViews" rendered="{!ThePagedUnrecognisedMilestoneCosts.CanGoPrevious}" value="{!$Label.kimbleone__PreviousPageLink}"/>
					    	<apex:outputText styleclass="greyedLink" rendered="{!!ThePagedUnrecognisedMilestoneCosts.CanGoPrevious}" value="{!$Label.kimbleone__PreviousPageLink}"/>
					    	&nbsp;|&nbsp;
					    	<apex:commandLink style="color: #015BA7;text-decoration: none;" action="{!GoNext}" rerender="TheViews" rendered="{!ThePagedUnrecognisedMilestoneCosts.CanGoNext}" value="{!$Label.kimbleone__NextPageLink}"/>
					    	<apex:outputText styleclass="greyedLink" rendered="{!!ThePagedUnrecognisedMilestoneCosts.CanGoNext}" value="{!$Label.kimbleone__NextPageLink}"/>
				</div> 
				<div style="clear:both;"></div>
				
			</apex:outputPanel>
			
			<apex:outputPanel layout="none" rendered="{!TheSelectedView = UnrecognisedPeriodRevenue}">
				
				<apex:pageBlockSection collapsible="false" columns="1" title="{!$ObjectType.KimbleOne__PeriodRevenue__c.LabelPlural}" ></apex:pageBlockSection>
			
				<apex:pageBlockTable value="{!ThePagedUnrecognisedPeriodRevenues.TheRecords}" var="record">
	          		<apex:column value="{!record.PeriodRevenue.DeliveryElement__r.DeliveryGroup__r.KimbleOne__Account__c}" headerValue="{!$ObjectType.Account.Fields.Name.Label}"/> 
              		<apex:column value="{!record.PeriodRevenue.DeliveryElement__r.KimbleOne__DeliveryGroup__c}" headerValue="{!$ObjectType.KimbleOne__DeliveryGroup__c.Fields.Name.Label}"/> 
					<apex:column value="{!record.PeriodRevenue.KimbleOne__ForecastUnits__c}" headerValue="{!$ObjectType.KimbleOne__PeriodRevenue__c.Fields.KimbleOne__ForecastUnits__c.Label}"/> 
              		<apex:column value="{!record.PeriodRevenue.KimbleOne__ForecastRevenue__c}"  headerValue="{!$ObjectType.KimbleOne__PeriodRevenue__c.Fields.KimbleOne__ForecastRevenue__c.Label}"/> 
              		<apex:column value="{!record.PeriodRevenue.Status__r.Name}"  headerValue="{!$ObjectType.KimbleOne__PeriodRevenue__c.Fields.KimbleOne__Status__c.Label}"/>
              		<apex:column value="{!record.PeriodRevenue.KimbleOne__ApprovalStatus__c}"  headerValue="{!$ObjectType.KimbleOne__PeriodRevenue__c.Fields.KimbleOne__ApprovalStatus__c.Label}"/>
              		<apex:column value="{!record.PendingStep.ActorId}"/>
	            </apex:pageBlockTable>
       
	            <div style="float:left; margin-top:15px;">
					<apex:outputText value="{!ThePagedUnrecognisedPeriodRevenues.CurrentPageDescription}"/>
				</div>
				<div style="float: right; margin-right: 15px; margin-top:15px;">
					    	<apex:commandLink style="color: #015BA7;text-decoration: none;" action="{!GoPrevious}" rerender="TheViews" rendered="{!ThePagedUnrecognisedPeriodRevenues.CanGoPrevious}" value="{!$Label.kimbleone__PreviousPageLink}"/>
					    	<apex:outputText styleclass="greyedLink" rendered="{!!ThePagedUnrecognisedPeriodRevenues.CanGoPrevious}" value="{!$Label.kimbleone__PreviousPageLink}"/>
					    	&nbsp;|&nbsp;
					    	<apex:commandLink style="color: #015BA7;text-decoration: none;" action="{!GoNext}" rerender="TheViews" rendered="{!ThePagedUnrecognisedPeriodRevenues.CanGoNext}" value="{!$Label.kimbleone__NextPageLink}"/>
					    	<apex:outputText styleclass="greyedLink" rendered="{!!ThePagedUnrecognisedPeriodRevenues.CanGoNext}" value="{!$Label.kimbleone__NextPageLink}"/>
				</div> 
				<div style="clear:both;"></div>
				
			</apex:outputPanel>
			
			<apex:outputPanel layout="none" rendered="{!TheSelectedView = UnrecognisedPeriodCost}">
				
				<apex:pageBlockSection collapsible="false" columns="1" title="{!$ObjectType.KimbleOne__PeriodCost__c.LabelPlural}" ></apex:pageBlockSection>
			
				<apex:pageBlockTable value="{!ThePagedUnrecognisedPeriodCosts.TheRecords}" var="record">
	          		<apex:column value="{!record.PeriodCost.DeliveryElement__r.DeliveryGroup__r.KimbleOne__Account__c}" headerValue="{!$ObjectType.Account.Fields.Name.Label}"/> 
              		<apex:column value="{!record.PeriodCost.DeliveryElement__r.KimbleOne__DeliveryGroup__c}" headerValue="{!$ObjectType.KimbleOne__DeliveryGroup__c.Fields.Name.Label}"/> 
					<apex:column value="{!record.PeriodCost.KimbleOne__ForecastUnits__c}" headerValue="{!$ObjectType.KimbleOne__PeriodCost__c.Fields.KimbleOne__ForecastUnits__c.Label}"/> 
              		<apex:column value="{!record.PeriodCost.KimbleOne__ForecastCost__c}" headerValue="{!$ObjectType.KimbleOne__PeriodCost__c.Fields.KimbleOne__ForecastCost__c.Label}"/> 
              		<apex:column value="{!record.PeriodCost.Status__r.Name}" headerValue="{!$ObjectType.KimbleOne__PeriodCost__c.Fields.KimbleOne__Status__c.Label}"/>
              		<apex:column value="{!record.PeriodCost.KimbleOne__ApprovalStatus__c}" headerValue="{!$ObjectType.KimbleOne__PeriodCost__c.Fields.KimbleOne__ApprovalStatus__c.Label}"/>
              		<apex:column value="{!record.PendingStep.ActorId}"/>
	            </apex:pageBlockTable>
       
	            <div style="float:left; margin-top:15px;">
					<apex:outputText value="{!ThePagedUnrecognisedPeriodCosts.CurrentPageDescription}"/>
				</div>
				<div style="float: right; margin-right: 15px; margin-top:15px;">
					    	<apex:commandLink style="color: #015BA7;text-decoration: none;" action="{!GoPrevious}" rerender="TheViews" rendered="{!ThePagedUnrecognisedPeriodCosts.CanGoPrevious}" value="{!$Label.kimbleone__PreviousPageLink}"/>
					    	<apex:outputText styleclass="greyedLink" rendered="{!!ThePagedUnrecognisedPeriodCosts.CanGoPrevious}" value="{!$Label.kimbleone__PreviousPageLink}"/>
					    	&nbsp;|&nbsp;
					    	<apex:commandLink style="color: #015BA7;text-decoration: none;" action="{!GoNext}" rerender="TheViews" rendered="{!ThePagedUnrecognisedPeriodCosts.CanGoNext}" value="{!$Label.kimbleone__NextPageLink}"/>
					    	<apex:outputText styleclass="greyedLink" rendered="{!!ThePagedUnrecognisedPeriodCosts.CanGoNext}" value="{!$Label.kimbleone__NextPageLink}"/>
				</div> 
				<div style="clear:both;"></div>
				
			</apex:outputPanel>

			<apex:outputPanel layout="none" rendered="{!TheSelectedView = UnrecognisedPerElementRevenue}">
				
				<apex:pageBlockSection collapsible="false" columns="1" title="{!$ObjectType.KimbleOne__DeliveryElement__c.LabelPlural}" ></apex:pageBlockSection>
			
				<apex:pageBlockTable value="{!ThePagedUnrecognisedPerElementRevenues.TheRecords}" var="record">
	          		<apex:column value="{!record.DeliveryElement.DeliveryGroup__r.KimbleOne__Account__c}" headerValue="{!$ObjectType.Account.Fields.Name.Label}"/> 
              		<apex:column value="{!record.DeliveryElement.KimbleOne__DeliveryGroup__c}" headerValue="{!$ObjectType.KimbleOne__DeliveryGroup__c.Fields.Name.Label}"/> 
					<apex:column value="{!record.DeliveryElement.Name}" headerValue="{!$ObjectType.KimbleOne__DeliveryElement__c.Fields.Name.Label}"/> 
					<apex:column value="{!record.DeliveryElement.KimbleOne__ContractRevenue__c}" headerValue="{!$ObjectType.KimbleOne__DeliveryElement__c.Fields.KimbleOne__ContractRevenue__c.Label}"/> 
              	</apex:pageBlockTable>
       
	            <div style="float:left; margin-top:15px;">
					<apex:outputText value="{!ThePagedUnrecognisedPerElementRevenues.CurrentPageDescription}"/>
				</div>
				<div style="float: right; margin-right: 15px; margin-top:15px;">
					    	<apex:commandLink style="color: #015BA7;text-decoration: none;" action="{!GoPrevious}" rerender="TheViews" rendered="{!ThePagedUnrecognisedPerElementRevenues.CanGoPrevious}" value="{!$Label.kimbleone__PreviousPageLink}"/>
					    	<apex:outputText styleclass="greyedLink" rendered="{!!ThePagedUnrecognisedPerElementRevenues.CanGoPrevious}" value="{!$Label.kimbleone__PreviousPageLink}"/>
					    	&nbsp;|&nbsp;
					    	<apex:commandLink style="color: #015BA7;text-decoration: none;" action="{!GoNext}" rerender="TheViews" rendered="{!ThePagedUnrecognisedPerElementRevenues.CanGoNext}" value="{!$Label.kimbleone__NextPageLink}"/>
					    	<apex:outputText styleclass="greyedLink" rendered="{!!ThePagedUnrecognisedPerElementRevenues.CanGoNext}" value="{!$Label.kimbleone__NextPageLink}"/>
				</div> 
				<div style="clear:both;"></div>
				
			</apex:outputPanel>

		</apex:pageBlock>
			
			</apex:facet>
		</apex:actionStatus>
      	
      	<apex:pageBlock >
			<apex:commandButton rendered="{!CanClosePeriod && HasPermission['CloseForecastingPeriod']}" id="closeForecastingPeriodBtn" onclick="var d = confirmDelete();if(!d) return false;" action="{!closePeriod}" value="{!$Label.kimbleone__Close}"/>
			<apex:commandButton rendered="{!CanReOpenPeriod && HasPermission['RevertClosedForecastingPeriod']}" id="reOpenForecastingPeriodBtn" onclick="var d = confirmDelete();if(!d) return false;" action="{!reOpenPeriod}" value="{!$Label.kimbleone__ReOpenButton}"/>
			
		</apex:pageBlock>
		<apex:actionFunction action="{!ChangeView}" name="changeView" rerender="ViewLoadingStatus" Status="ViewLoadingStatus"> 
			<apex:param name="viewName" value="" />
		</apex:actionFunction>
		<apex:actionFunction action="{!GoPreviousPeriod}" name="GoPreviousPeriod" />
			<apex:actionFunction action="{!GoNextPeriod}" name="GoNextPeriod"/ >
	</apex:form>
  
</apex:define>

</apex:composition>

</apex:page>