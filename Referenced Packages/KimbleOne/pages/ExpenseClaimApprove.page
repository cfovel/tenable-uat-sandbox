<apex:page standardcontroller="KimbleOne__ExpenseClaim__c"
	extensions="KimbleOne.ExpenseClaimApproveController,KimbleOne.SecurityController" 
	sidebar="false">

	<apex:composition template="KimbleOne__SiteMaster">

		<apex:define name="Menu">
			<c:Menu MenuContextName="{!KimbleOne__ExpenseClaim__c.Name}"  />
		</apex:define>

		<apex:define name="Content">

			<apex:form >
				<apex:actionFunction action="{!AddClaimAttachment}" name="addClaimAttachment" rerender="ClaimAttachmentsPanel, pageErrors" oncomplete="hideModalBoxy()">
					<apex:param name="attachmentId" value="" />
				</apex:actionFunction> 
				<apex:actionFunction action="{!AddItemAttachment}" name="addItemAttachment" rerender=" ExpenseItemPane,Expenses, pageErrors" oncomplete="hideModalBoxy()">
					<apex:param name="attachmentId" value="" />
					<apex:param name="expenseId" value="" />
				</apex:actionFunction> 
           		<apex:actionFunction action="{!GetExpenseItem}" name="editExpenseItem" rerender="ExpenseItemDetails" oncomplete="showModalBoxy()">
					<apex:param name="ExpenseItem" value="" />
				</apex:actionFunction>
				<apex:actionFunction action="{!SelectAttachment}" name="selectAttachment" rerender="ExpenseClaimPane, ClaimAttachmentsPopup, ExpenseItemPane" oncomplete="showReceipt()">
					<apex:param name="attachmentId" value="" />
				</apex:actionFunction> 
				<apex:actionFunction action="{!DeleteAttachment}" name="deleteAttachment" rerender="Expenses, ExpenseItemPane, ClaimAttachmentsPanel, pageErrors,AttachmentReceiptPanel" oncomplete="hideReceipt()">
					<apex:param name="attachmentId" value="" />
				</apex:actionFunction>
					
				<apex:outputPanel id="pageErrors">
    				<apex:pageMessages escape="false" />
				</apex:outputPanel>
		
				
				<apex:pageBlock title="" mode="edit">

					<apex:pageBlockButtons >
						<apex:commandButton id="RevertToDraft" action="{!RevertToDraft}" value="{!$Label.kimbleone__RevertToDraft}" rendered="{!AllowRevert  && HasPermission['RevertApprovedExpenseClaim']}" disabled="{!DisableRevert}" />
						<input class="btn" type="button" onclick="window.open('{!$Page.ExpenseClaimPrint}?id={!ExpenseClaim__c.Id}','_printview')" value="{!$Label.Print}" />
						<input class="btn" type="button" onclick="window.open('{!$Page.ExpenseClaimPrint}?id={!ExpenseClaim__c.Id}&forClientApproval=1','_printview')" value="{!$Label.PrintSignoffDocumentLabel}" />
						<input class="btn" type="button" onclick="safeNavigateToSForceURL('{!$Page.ExpenseClaimAttachmentsPdf}?id={!ExpenseClaim__c.Id}');" value="{!$Label.GenerateImageAttachmentsAsPDFButton}" />
						<apex:outputPanel layout="none" rendered="{!!IsApproved}">
							<input type="button" class="btn" value="{!$Label.kimbleone__Add} {!$Label.kimbleone__ReceiptLabel}" onclick="showFileUploadPopup('');"/>
						</apex:outputPanel>
					</apex:pageBlockButtons>

					<apex:outputpanel id="ExpenseClaimPane">
						<table cellspacing="0" cellpadding="0" border="0" width="100%">
							<tr>
								<td style="vertical-align:top">
									<apex:pageBlockSection title="{!$Label.kimbleone__ExpenseClaimApproval}" columns="2">

							<apex:outputField id="reference"
								value="{!KimbleOne__ExpenseClaim__c.KimbleOne__Reference__c}" />
							<apex:outputField id="approvalStatus"
								value="{!KimbleOne__ExpenseClaim__c.KimbleOne__ApprovalStatus__c}" />
							<apex:outputField id="resource"
								value="{!KimbleOne__ExpenseClaim__c.KimbleOne__Resource__c}" />
							<apex:outputField id="approver1"
								value="{!KimbleOne__ExpenseClaim__c.KimbleOne__Approver1User__c}" />
							<apex:outputField id="deliveryGroup"
								value="{!TheClaim.ResourcedActivity__r.KimbleOne__DeliveryGroup__c}" />
							<apex:outputField id="approver2"
								value="{!KimbleOne__ExpenseClaim__c.KimbleOne__Approver2User__c}" />
							<apex:outputField id="activity"
								value="{!TheClaim.ResourcedActivity__r.Name}" />
							<apex:outputField id="approvalComments"
								value="{!KimbleOne__ExpenseClaim__c.KimbleOne__ApprovalComments__c}" />
							<apex:pageBlockSectionItem />
							<apex:pageBlockSectionItem >
								<apex:outputLabel value="{!$ObjectType.KimbleOne__ExpenseClaim__c.Fields.KimbleOne__Status__c.Label}" />
								<apex:outputField value="{!KimbleOne__ExpenseClaim__c.Status__r.Name}" />
							</apex:pageBlockSectionItem>							
							
							<apex:outputField id="totrev"
								value="{!KimbleOne__ExpenseClaim__c.KimbleOne__TotalExpenseRevenue__c}" />
							<apex:pageBlockSectionItem >
								<apex:outputLabel value="{!$ObjectType.KimbleOne__ExpenseClaim__c.Fields.KimbleOne__InvoicingCurrencyTotalExpenseRevenue__c.Label}" />
								<apex:outputPanel layout="none">
									<apex:outputText value="{!KimbleOne__ExpenseClaim__c.KimbleOne__InvoicingCurrencyIsoCode__c}" styleclass="kimbleCurrencyCode" />
									<apex:outputField value="{!KimbleOne__ExpenseClaim__c.KimbleOne__InvoicingCurrencyTotalExpenseRevenue__c}" />
								</apex:outputPanel>
							</apex:pageBlockSectionItem>
							<apex:outputField id="totamt"
								value="{!KimbleOne__ExpenseClaim__c.KimbleOne__TotalExpenseCost__c}" />
							<apex:pageBlockSectionItem >
								<apex:outputLabel value="{!$ObjectType.KimbleOne__ExpenseClaim__c.Fields.KimbleOne__ReimbursementCcyTotalReimbursementAmount__c.Label}" />
								<apex:outputPanel layout="none">
									<apex:outputText value="{!KimbleOne__ExpenseClaim__c.KimbleOne__ReimbursementCurrencyIsoCode__c}" styleclass="kimbleCurrencyCode" />
									<apex:outputField value="{!KimbleOne__ExpenseClaim__c.KimbleOne__ReimbursementCcyTotalReimbursementAmount__c}" />
								</apex:outputPanel>
							</apex:pageBlockSectionItem>
							
							<apex:pageBlockSectionItem />
							<apex:pageBlockSectionItem >
								<apex:outputLabel value="{!$ObjectType.KimbleOne__ExpenseClaim__c.Fields.KimbleOne__ResourcePayment__c.Label}" />
								<apex:outputField value="{!KimbleOne__ExpenseClaim__c.KimbleOne__ResourcePayment__c}" />
							</apex:pageBlockSectionItem>
							
						</apex:pageBlockSection>

								</td>
								<td style="vertical-align:top;padding-left:3px;padding-right:3px;">
									<div class="pbSubheader first brandTertiaryBgr tertiaryPalette"><h3>{!$Label.ExpenseClaimReceiptsHeading}</h3></div>
									<apex:outputPanel id="ClaimAttachmentsPanel">
									<div id="thumbnails" class="ui-widget ui-corner-all thumbnailContainer">
										<apex:repeat value="{!ClaimAttachments}" var="attachment">
											
											<apex:outputPanel layout="none" rendered="{!attachment.ContentType = 'application/pdf'}">
												<div class="ui-widget {!IF(SelectedAttachmentId==attachment.Id,'ui-state-highlight','ui-widget-inactive')}" style="float: left;cursor:pointer; height: 64px;width: 64px;padding: 5px;margin: 8px;text-align: center;">
													<apex:image title="{!$Label.kimbleone__PdfLabel}" onclick="selectAttachment('{!attachment.Id}')"  value="{!URLFOR($Resource.KimbleOne__AttcIcon)}" width="64" height="64" rendered="{!!ISBLANK(attachment.Id)}"/>
												</div>
											</apex:outputPanel>
											<apex:outputPanel layout="none" rendered="{!attachment.ContentType != 'application/pdf'}">
												<div class="ui-widget {!IF(SelectedAttachmentId==attachment.Id,'ui-state-highlight','ui-widget-inactive')}" style="float: left;cursor:pointer; height: 64px;width: 64px;padding: 5px;margin: 8px;text-align: center;">
													<apex:image title="{!$Label.kimbleone__ImageLabel}" onclick="selectAttachment('{!JSINHTMLENCODE(attachment.Id)}')"  value="/servlet/servlet.FileDownload?file={!attachment.Id}" width="64" height="64" rendered="{!!ISBLANK(attachment.Id)}"/>
												</div>
											</apex:outputPanel>
											
										</apex:repeat>
										<div style="clear:both;"></div>
									</div>
									</apex:outputPanel>
								</td>
							</tr>	
						</table>
						
					</apex:outputpanel>
					
					<apex:PageBlockSection Title="{!$ObjectType.KimbleOne__ExpenseItem__c.Labelplural}" columns="1">
					
						<apex:outputpanel id="ExpenseItemPane" >
							<apex:pageBlockTable var="expenseItem" value="{!ExpenseItems}" >

								<apex:column headerValue="{!$Label.kimbleone__ActionLinksHeader}"
									rendered="{!KimbleOne__ExpenseClaim__c.KimbleOne__ApprovalStatus__c==StatusReadyForApproval && CanUpdate}">
									<a href="#" onclick="initialiseModalBoxy('#EditExpenseItemPopup', '{!$ObjectType.KimbleOne__ExpenseItem__c.label}');editExpenseItem('{!JSENCODE(expenseItem.Id)}')">{!$Label.kimbleone__Edit}</a>
								</apex:column>

								<apex:column value="{!expenseItem.KimbleOne__IncurredDate__c}"
									headerValue="{!$ObjectType.KimbleOne__ExpenseItem__c.Fields.KimbleOne__IncurredDate__c.Label}" />
								<apex:column value="{!expenseItem.KimbleOne__Reference__c}"
									headerValue="{!$ObjectType.KimbleOne__ExpenseItem__c.Fields.KimbleOne__Reference__c.Label}" />
								<apex:column value="{!expenseItem.ActivityExpenseCategory__r.Name} {!IF(expenseItem.ActivityExpenseCategory__r.ExpenseCategory__r.Name!=expenseItem.ActivityExpenseCategory__r.Name,'('+expenseItem.ActivityExpenseCategory__r.ExpenseCategory__r.Name+')','')} "
									headerValue="{!$Label.kimbleone__ExpenseCategory}" />
								<apex:column headerValue="{!$Label.kimbleone__Units}">
									<apex:outputPanel layout="none" rendered="{!ExpenseTypePerUnit==expenseItem.ActivityExpenseCategory__r.ExpenseCategory__r.ExpenseType__r.KimbleOne__Enum__c}" >
										<apex:outputField value="{!expenseItem.KimbleOne__ExpenseUnits__c}" /> &nbsp;
										<apex:outputText value="{!expenseItem.ActivityExpenseCategory__r.ExpenseCategory__r.UnitType__r.KimbleOne__Suffix__c}" />
									</apex:outputPanel>
								</apex:column>
								<apex:column headerValue="{!$ObjectType.KimbleOne__ExpenseItem__c.Fields.KimbleOne__IncurredCurrencyGrossAmount__c.Label}">
									<apex:outputPanel layout="none" rendered="{! expenseItem.ActivityExpenseCategory__r.ExpenseCategory__r.ExpenseType__r.Enum__c <> ExpenseTypePerUnit }" >
										<apex:outputText value="{!expenseItem.KimbleOne__IncurredCurrencyIsoCode__c}" styleclass="kimbleCurrencyCode" />
										<apex:outputField value="{!expenseItem.KimbleOne__IncurredCurrencyGrossAmount__c}" />
									</apex:outputPanel>
								</apex:column>
								<apex:column headerValue="{!$ObjectType.KimbleOne__ExpenseItem__c.Fields.KimbleOne__ExchangeRateConversionFactor__c.Label}">
									<apex:outputField value="{!expenseItem.KimbleOne__ExchangeRateConversionFactor__c}" 
										rendered="{! expenseItem.ActivityExpenseCategory__r.ExpenseCategory__r.ExpenseType__r.Enum__c <> ExpenseTypePerUnit
													 && expenseItem.IncurredCurrencyIsoCode__c <> expenseItem.CurrencyIsoCode }" />
								</apex:column>
								
								<apex:column value="{!expenseItem.KimbleOne__ExpenseNetAmount__c}" headerValue="{!$ObjectType.KimbleOne__ExpenseItem__c.Fields.KimbleOne__ExpenseNetAmount__c.Label}" />
									
								<apex:column value="{!expenseItem.KimbleOne__ExpenseTaxAmount__c}" headerValue="{!$ObjectType.KimbleOne__ExpenseItem__c.Fields.KimbleOne__ExpenseTaxAmount__c.Label}" title="{!expenseItem.TaxCode__r.Name}"/>
								
								<apex:column headerValue="{!$ObjectType.KimbleOne__ExpenseItem__c.Fields.KimbleOne__ExpenseRevenue__c.Label}">
									<apex:outputText value="{!expenseItem.KimbleOne__InvoicingCurrencyIsoCode__c}" styleclass="kimbleCurrencyCode" />
									<apex:outputField value="{!expenseItem.KimbleOne__InvoicingCurrencyExpenseRevenue__c}" />
								</apex:column>
								
								<apex:column headerValue="{!$ObjectType.KimbleOne__ExpenseItem__c.Fields.KimbleOne__ReimbursementCurrencyReimbursementAmount__c.Label}">
									<apex:outputText value="{!expenseItem.KimbleOne__ReimbursementCurrencyIsoCode__c}" styleclass="kimbleCurrencyCode" />
									<apex:outputField value="{!expenseItem.KimbleOne__ReimbursementCurrencyReimbursementAmount__c}" />
								</apex:column>
								
								<apex:column value="{!expenseItem.KimbleOne__StartLocation__c}"
									headerValue="{!$ObjectType.KimbleOne__ExpenseItem__c.Fields.KimbleOne__StartLocation__c.Label}" />
								<apex:column value="{!expenseItem.KimbleOne__EndLocation__c}"
									headerValue="{!$ObjectType.KimbleOne__ExpenseItem__c.Fields.KimbleOne__EndLocation__c.Label}" />
								<apex:column value="{!expenseItem.KimbleOne__Attendees__c}"
									headerValue="{!$ObjectType.KimbleOne__ExpenseItem__c.Fields.KimbleOne__Attendees__c.Label}" />
								
								
								<apex:column headervalue="{!$Label.kimbleone__ReceiptLabel}">
		      						<apex:inputField value="{!expenseItem.KimbleOne__IsReceipted__c}" rendered="{!expenseItem.Attachments.size == 0}" />
		      						<apex:outputLink styleclass="actionLink" value="javascript:void(0)" onclick="showFileUploadPopup('{!JSINHTMLENCODE(expenseItem.Id)}');" rendered="{!expenseItem.Attachments.size == 0}">{!$Label.kimbleone__Add}</apex:outputLink>
      								<apex:outputPanel layout="none" rendered="{!expenseItem.Attachments.size == 1}">
			      						<div class="ui-widget {!IF(SelectedAttachmentId==expenseItem.Attachments[0].Id,'ui-state-highlight','ui-widget-inactive')}" style="cursor:pointer; height: 32px;width: 32px;padding: 5px;margin: 8px;text-align: center;">
											<apex:image title="{!IF(expenseItem.Attachments[0].ContentType = 'application/pdf', 'pdf','image')}" onclick="selectAttachment('{!expenseItem.Attachments[0].Id}')"  value="{!URLFOR($Resource.KimbleOne__AttcIcon)}" width="32" height="32" rendered="{!!ISBLANK(expenseItem.Attachments[0].Id)}"/>
										</div>
									</apex:outputPanel>
						
				      			</apex:column>
								
								<apex:column value="{!expenseItem.KimbleOne__Notes__c}" headerValue="{!$ObjectType.KimbleOne__ExpenseItem__c.Fields.KimbleOne__Notes__c.Label}" />

							</apex:pageBlockTable>
						</apex:outputpanel>

					</apex:PageBlockSection>

				</apex:pageBlock>
			
			<div id="divClaimAttachmentsPopup">
			<apex:outputPanel id="ClaimAttachmentsPopup">
				<apex:outputPanel id="AttachmentReceiptPanel">
					<apex:outputPanel layout="none" rendered="{!!ISBLANK(SelectedAttachmentId)}">
						<apex:outputPanel rendered="{!!IsApproved}">
							<input type="button" class="btn" value="{!$Label.DeleteButton}" onclick="deleteAttachment('{!JSINHTMLENCODE(SelectedAttachmentId)}');"/><br/><br/>
						</apex:outputPanel>
						<div id="resizable">
							<iframe id="attachmentsIFrame" src="/servlet/servlet.FileDownload?file={!SelectedAttachmentId}" style="border:0px;"></iframe>
						</div>
					</apex:outputPanel>
				</apex:outputPanel>	
			</apex:outputPanel>
			</div>
			
				<script>
			function calAmountEdit()
			{
				if((!isNaN($('.IncurredCurrencyGrossAmountEdit').val())) && $('.IncurredCurrencyGrossAmountEdit').val()!=''
					&& (!isNaN($('.ExchangeRateConversionFactorEditText').val())) && $('.ExchangeRateConversionFactorEditText').val()!='') 
					$('.ExpenseGrossAmountEditText').val(''+(eval($('.IncurredCurrencyGrossAmountEdit').val())*$('.ExchangeRateConversionFactorEditText').val())); 
				else 
					$('.ExpenseGrossAmountEditText').val('');
			}
		</script>

			</apex:form>

			<apex:relatedList list="ProcessSteps"></apex:relatedList>

			<div id="EditExpenseItemPopup" style="display: none"><apex:outputPanel id="ExpenseItemDetails">

				<apex:form id="EditExpenseItemForm">

					<apex:pageBlock id="ExpenseFields">
						
						<div id="expenseerrors"><apex:pageMessages escape="false"/></div>
						
						<apex:pageBlockButtons location="bottom">
							<apex:commandButton value="{!$Label.kimbleone__Save}" action="{!SaveExpenseItem}" oncomplete="hideModalBoxyWithErrorCheck('#expenseerrors');" rerender="ExpenseItemDetails, ExpenseItemPane, ExpenseClaimPane"  styleclass="DisableButtonWhileBusy" />
							<input class="btn" type="button" onclick="hideModalBoxy();"
								value="{!$Label.Cancel}" />
						</apex:pageBlockButtons>
						
						<apex:pageblockSection collapsible="false" showheader="false" columns="1">
							
							<apex:pageBlockSectionItem labelStyle="width:30%">
								<apex:outputLabel value="{!$Label.kimbleone__ResourceActivityAlternativeLabel}" />
								<apex:outputText value="{!expenseItem.ActivityExpenseCategory__r.ResourcedActivity__r.Name}" />
							</apex:pageBlockSectionItem>
							
							<apex:pageBlockSectionItem labelStyle="width:30%">
								<apex:outputLabel value="{!$Label.kimbleone__ExpenseCategory}" />
								<apex:outputText value="{!expenseItem.ActivityExpenseCategory__r.Name}" />
							</apex:pageBlockSectionItem>
							
							<apex:pageBlockSectionItem >
								<apex:outputLabel value="{!$ObjectType.KimbleOne__ExpenseItem__c.Fields.KimbleOne__IncurredDate__c.Label}" />
								<apex:outputField value="{!expenseItem.KimbleOne__IncurredDate__c}" />
							</apex:pageBlockSectionItem>
							
							<apex:pageBlockSectionItem rendered="{!!IsPerOccurrence && HasPermission['RevertApprovedExpenseClaim']}" >
								<apex:outputText value="{!$ObjectType.KimbleOne__ExpenseItem__c.Fields.KimbleOne__ExpenseUnits__c.Label} ({!expenseItem.ActivityExpenseCategory__r.ExpenseCategory__r.UnitType__r.KimbleOne__Suffix__c})" />
								<apex:inputField value="{!expenseItem.KimbleOne__ExpenseUnits__c}" >
									<apex:actionSupport event="onchange" action="{!IncurredAmountChanged}" rerender="ExpenseFields" onsubmit="DisableParentsChildren($('.DisableButtonWhileBusy'),'Reloading...');"/>
								</apex:inputField>									
							</apex:pageBlockSectionItem>
							
							<apex:pageBlockSectionItem rendered="{!IsPerOccurrence && HasPermission['RevertApprovedExpenseClaim']}" >
								<apex:outputText value="{!$ObjectType.KimbleOne__ExpenseItem__c.Fields.KimbleOne__ExpenseGrossAmount__c.Label} {!expenseItem.KimbleOne__IncurredCurrencyIsoCode__c}" />
								<apex:inputField value="{!expenseItem.KimbleOne__IncurredCurrencyGrossAmount__c}" required="true">
									<apex:actionSupport event="onchange" action="{!IncurredAmountChanged}" rerender="ExpenseFields" onsubmit="DisableParentsChildren($('.DisableButtonWhileBusy'),'Reloading...');"/>
								</apex:inputField>
							</apex:pageBlockSectionItem>
							
							<apex:pageBlockSectionItem rendered="{!IsPerOccurrence}">
								<apex:outputText value="{!$ObjectType.KimbleOne__ExpenseItem__c.Fields.KimbleOne__TaxCode__c.Label}" />
								
								<apex:selectList value="{!expenseItem.KimbleOne__TaxCode__c}" size="1">
									<apex:selectOptions value="{!TheTaxCodes}" />
									<apex:actionSupport event="onchange" action="{!TaxCodeChanged}" rerender="ExpenseFields" onsubmit="DisableParentsChildren($('.DisableButtonWhileBusy'),'Reloading...');"/>
								</apex:selectList>
								
							</apex:pageBlockSectionItem>

							<apex:pageBlockSectionItem rendered="{!IsPerOccurrence}">
								<apex:outputText value="{!$ObjectType.KimbleOne__ExpenseItem__c.Fields.KimbleOne__IncurredCurrencyTaxAmount__c.Label} {!expenseItem.KimbleOne__IncurredCurrencyIsoCode__c}" />
								<apex:inputField value="{!expenseItem.KimbleOne__IncurredCurrencyTaxAmount__c}" required="true">
									<apex:actionSupport event="onchange" action="{!TaxAmountChanged}" rerender="ExpenseFields" onsubmit="DisableParentsChildren($('.DisableButtonWhileBusy'),'Reloading...');"/>
								</apex:inputField>
							</apex:pageBlockSectionItem>
							
							<apex:pageBlockSectionItem rendered="{!ShowExchangeRate && IsPerOccurrence}" >
								<apex:outputText value="{!$ObjectType.KimbleOne__ExpenseItem__c.Fields.KimbleOne__ExchangeRateConversionFactor__c.Label}" />
								<apex:inputField value="{!expenseItem.KimbleOne__ExchangeRateConversionFactor__c}" >
									<apex:actionSupport event="onchange" action="{!ExchangeRateChanged}" rerender="ExpenseFields" onsubmit="DisableParentsChildren($('.DisableButtonWhileBusy'),'Reloading...');"/>
								</apex:inputField>
							</apex:pageBlockSectionItem>
							
							<apex:pageBlockSectionItem rendered="{!ShowExchangeRate && IsPerOccurrence}" >
								<apex:outputText value="{!$ObjectType.KimbleOne__ExpenseItem__c.Fields.KimbleOne__ExpenseGrossAmount__c.Label} {!expenseItem.CurrencyIsoCode}" />
								<apex:outputField value="{!expenseItem.KimbleOne__ExpenseGrossAmount__c}"/>
							</apex:pageBlockSectionItem>
							
							<apex:pageBlockSectionItem rendered="{!IsPerOccurrence}">
								<apex:outputText value="{!$ObjectType.KimbleOne__ExpenseItem__c.Fields.KimbleOne__ExpenseNetAmount__c.Label} {!expenseItem.CurrencyIsoCode}" />
								<apex:inputField value="{!expenseItem.KimbleOne__ExpenseNetAmount__c}" required="true">
									<apex:actionSupport event="onchange" action="{!NetAmountChanged}" rerender="ExpenseFields" onsubmit="DisableParentsChildren($('.DisableButtonWhileBusy'),'Reloading...');"/>
								</apex:inputField>
							</apex:pageBlockSectionItem>
							
							<apex:pageBlockSectionItem >
								<apex:outputText value="{!$ObjectType.KimbleOne__ExpenseItem__c.Fields.KimbleOne__ExpenseRevenue__c.Label} {!expenseItem.KimbleOne__InvoicingCurrencyIsoCode__c}" />
								<apex:inputField value="{!expenseItem.KimbleOne__InvoicingCurrencyExpenseRevenue__c}" required="true">
									<apex:actionSupport event="onchange" action="{!RevenueAmountChanged}" rerender="ExpenseFields" onsubmit="DisableParentsChildren($('.DisableButtonWhileBusy'),'Reloading...');"/>
								</apex:inputField>
							</apex:pageBlockSectionItem>

							<apex:pageBlockSectionItem >
								<apex:outputLabel value="{!$ObjectType.KimbleOne__ExpenseItem__c.Fields.KimbleOne__Notes__c.Label}" />
								<apex:inputTextarea rows="4" cols="45" value="{!expenseItem.KimbleOne__Notes__c}" />
							</apex:pageBlockSectionItem>
							
							<apex:inputField value="{!expenseItem.KimbleOne__StartLocation__c}" required="true"
								rendered="{!expenseItem.ActivityExpenseCategory__r.ExpenseCategory__r.KimbleOne__HasStartLocation__c}" />
							
							<apex:inputField value="{!expenseItem.KimbleOne__EndLocation__c}" required="true"
								rendered="{!expenseItem.ActivityExpenseCategory__r.ExpenseCategory__r.KimbleOne__HasEndLocation__c}" />
							
							<apex:inputField value="{!expenseItem.KimbleOne__Attendees__c}" required="true"
								rendered="{!expenseItem.ActivityExpenseCategory__r.ExpenseCategory__r.KimbleOne__HasAttendees__c}" />

						</apex:pageblockSection>

					
					</apex:pageBlock>
					
				</apex:form>
			</apex:outputPanel></div>
			<div id="fileUploadPopupDiv" style="display:none">
				<apex:outputPanel id="FileUploadPopupPanel" layout="none">
					<input id="expenseItemAttachment" type="hidden" value=""/>
					<iframe id="fileUploadIFrame" src="{!$Page.FileUpload}?ObjectId={!KimbleOne__ExpenseClaim__c.KimbleOne__Resource__c}" style="border: 0;height: 122px;width: 286px;"></iframe>
				</apex:outputPanel>
			</div>
			<script>
						var receipt = null;					
  						
  						var iframesize = {width:406, height:450};
  						
  						function showReceipt()
  						{
  							if (receipt == null)
  								receipt = new Boxy($('#divClaimAttachmentsPopup'), {title: '{!$Label.SelectedReceiptLabel}', draggable:true, closeable:true});
  							else
  								receipt.show();
  							
  							jQuery('#attachmentsIFrame').width(iframesize.width).height(iframesize.height);
  							
  							jQuery('#resizable').resizable({
								resize: function(event, ui){
									jQuery('#attachmentsIFrame').width(ui.size.width).height(ui.size.height)},
								start: function(event, ui) {
							        jQuery('#attachmentsIFrame').hide();
							         },
							    stop: function(event, ui) {
							        jQuery('#attachmentsIFrame').show();
							        iframesize.width = jQuery('#attachmentsIFrame').width();
							        iframesize.height = jQuery('#attachmentsIFrame').height();
							      }
								})
  						}
  						function hideReceipt()
  						{
  							if(receipt!=null)
  								receipt.hide();
  						}
						$(document).ready(function(){
    							
    							
    							$('input[name="piSubmit"]').remove();
    							$('input[name="piRemove"]').remove();
    							
    							$('.ReceiptLinks').each(function()
    							{
    								//.attr('width','500px').attr('height','500px')
    								var imagePane=$('<div>').append($('<img>').attr('src','{!JSINHTMLENCODE(baseUrl)}'+$(this).attr('href').replace('..','')));
									$(this).qtip({
									   content: imagePane.html() ,
									    adjust: {target:$(this), mouse: false, screen: true },
									   position: {
      corner: {
         target: 'leftBottom',
         tooltip: 'bottomRight'
      }},
									   style: { 
									      width: 600,
									      padding: 0,
									      background: '#FFFFFF',
									      color: 'black',
									      textAlign: 'left',
									      border: {
									         width: 1,
									         radius: 5,
									         color: '#FF00FF'
									      },
									      tip: true,
									      name: 'dark' // Inherit the rest of the attributes from the preset dark style
									   }
									});
    								
    							});
    							
    							
    						
    					});
    					
    			
    		function showFileUploadPopup(pos)
			{
				initialiseModalBoxy('#fileUploadPopupDiv', '{!$Label.UploadReceiptHeading}');
				$('#expenseItemAttachment').val(pos);
				showModalBoxy();
				document.getElementById('fileUploadIFrame').src = document.getElementById('fileUploadIFrame').src;
			}
    		function cancelAttachment()
			{
				hideModalBoxy();
			}
			function addAttachment(id)
			{
				if($('#expenseItemAttachment').val() == '')
					addClaimAttachment(id);
				else
					addItemAttachment(id, $('#expenseItemAttachment').val());
			}
					
				</script>
	<div id="aaa">
	
	</div>
		
	<style>
		body {
	-webkit-touch-callout: none;
	-webkit-user-select: none;
	-khtml-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}
		.thumbnailContainer{background-color:#FFF;border-color: gray;margin-bottom:4px;}
	</style>
	
	</apex:define>

	</apex:composition>

</apex:page>