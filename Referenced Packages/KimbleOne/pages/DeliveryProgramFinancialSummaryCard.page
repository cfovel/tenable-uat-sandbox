<apex:page controller="KimbleOne.DeliveryProgramFinSummaryCardController" showHeader="false" sidebar="false" contentType="text/css" tabStyle="KimbleOne__ReferenceData__c">    
<apex:outputPanel layout="none" rendered="{!FinancialSummaries.size > 0}">
	<div class="cardHeading inverted">
		<h3 class="cardTitle"><apex:outputText value="{!$Label.kimbleone__DeliveryGroupFinancialSummary}"/></h3>
	</div>

	<div class="cardBody">
		<table class="list cardBodyTable" border="0" cellpadding="0" cellspacing="0">
			<thead>
				<tr class="headerRow">
					<th class="headerRow" colspan="2"><h2></h2></th>
					<apex:outputPanel layout="none" rendered="{!ShowCosts}">
						<th colspan="4" class="headerRow" style="text-align: center;"><h2><apex:outputText value="{!$Label.kimbleone__BaselineLabel} ({!TheDisplayCurrencyIsoCode})"/></h2></th>
					</apex:outputPanel>
					<apex:outputPanel layout="none" rendered="{!!ShowCosts}">
						<th colspan="2" class="headerRow" style="text-align: center;"><h2><apex:outputText value="{!$Label.kimbleone__BaselineLabel} ({!TheDisplayCurrencyIsoCode})"/></h2></th>
					</apex:outputPanel>
					<apex:outputPanel layout="none" rendered="{!ShowCosts}">
						<th colspan="4" class="headerRow" style="text-align: center;"><h2><apex:outputText value="{!$Label.kimbleone__ProjectedLabel} ({!TheDisplayCurrencyIsoCode})"/></h2></th>
					</apex:outputPanel>
					<apex:outputPanel layout="none" rendered="{!!ShowCosts}">
						<th colspan="2" class="headerRow" style="text-align: center;"><h2><apex:outputText value="{!$Label.kimbleone__ProjectedLabel} ({!TheDisplayCurrencyIsoCode})"/></h2></th>
					</apex:outputPanel>
					
					<apex:outputPanel layout="none" rendered="{!ShowCosts}">
						<th colspan="4" class="headerRow" style="text-align: center;"><h2><apex:outputText value="{!$Label.kimbleone__ActualLabel} ({!TheDisplayCurrencyIsoCode})"/></h2></th>
					</apex:outputPanel>
					<apex:outputPanel layout="none" rendered="{!!ShowCosts}">
						<th colspan="2" class="headerRow" style="text-align: center;"><h2><apex:outputText value="{!$Label.kimbleone__ActualLabel} ({!TheDisplayCurrencyIsoCode})"/></h2></th>
					</apex:outputPanel>
					<th class="headerRow"></th>
				</tr>
				<tr class="headerRow">
					<th class="headerRow"></th>
					<th class="headerRow engagement-name"><h2><apex:outputText value="{!$ObjectType.KimbleOne__DeliveryGroup__c.Fields.Name.Label}"/></h2></th>
					<th class="headerRow"><h2>{!$Label.kimbleone__UsageLabel}</h2></th>
					<th class="headerRow"><h2>{!$Label.kimbleone__RevenueLabel}</h2></th>
					<apex:outputPanel layout="none" rendered="{!ShowCosts}">
						<th class="headerRow"><h2>{!$Label.kimbleone__CostLabel}</h2></th>
						<th class="headerRow"><h2>{!$Label.kimbleone__MarginLabel} (%)</h2></th>
					</apex:outputPanel>
					<th class="headerRow"><h2>{!$Label.kimbleone__UsageLabel}</h2></th>
					<th class="headerRow"><h2>{!$Label.kimbleone__RevenueLabel}</h2></th>
					<apex:outputPanel layout="none" rendered="{!ShowCosts}">
						<th class="headerRow"><h2>{!$Label.kimbleone__CostLabel}</h2></th>
						<th class="headerRow"><h2>{!$Label.kimbleone__MarginLabel} (%)</h2></th>
					</apex:outputPanel>
					<th class="headerRow"><h2>{!$Label.kimbleone__UsageLabel}</h2></th>
					<th class="headerRow"><h2>{!$Label.kimbleone__RevenueLabel}</h2></th>
					<apex:outputPanel layout="none" rendered="{!ShowCosts}">
						<th class="headerRow"><h2>{!$Label.kimbleone__CostLabel}</h2></th>
						<th class="headerRow"><h2>{!$Label.kimbleone__MarginLabel} (%)</h2></th>
					</apex:outputPanel>
					<th class="headerRow"></th>
				</tr>
			</thead>
			<tbody>
				<apex:repeat value="{!FinancialSummaries}" var="fs">
					<tr class="dataRow delivery-group-row" delivery-group-id="{!fs.Id}">
						<td class="dataCell">
							<div class="title-card-heading-icon">
								<div class="fa fa-square kimble-probability {!JSENCODE(fs.ForecastStatusStyleClass)}" title="{!JSENCODE(fs.ForecastStatusName)}"></div>
							</div>										
						</td>
						<td class="dataCell">
							<apex:outputLink title="{!$ObjectType.KimbleOne__DeliveryGroup__c.Fields.OwnerId.Label}: {!JSENCODE(fs.Owner)}" styleClass="undecorated-link" value="/{!fs.Id}"><apex:outputText value="{!fs.DeliveryGroupName}"/></apex:outputLink>
						</td>
						<td class="dataCell" style="background-color: #EEE;">
							<apex:outputField rendered="{!fs.IsResourced}" value="{!fs.BaselineUsage.KimbleOne__InputDecimal__c} " />
							<apex:outputText rendered="{!fs.IsResourced}"  value=" ({!fs.UsageUnits})"/>						
						</td>
						<td class="rightAlignDataCell" style="background-color: #EEE;"><apex:outputField value="{!fs.BaselineContractRevenue.KimbleOne__InputDecimal__c}" /></td>
						
						<apex:outputPanel layout="none" rendered="{!ShowCosts}">
							<td class="rightAlignDataCell" style="background-color: #EEE;"><apex:outputField value="{!fs.BaselineContractCost.KimbleOne__InputDecimal__c}" /></td>
							<td class="rightAlignDataCell" style="background-color: #EEE; {!IF(!ISBLANK(fs.BaselineContractMargin.KimbleOne__InputInteger__c) && fs.BaselineContractMargin.KimbleOne__InputInteger__c < 0, 'color:red;font-weight:bold;','')}"><apex:outputField value="{!fs.BaselineContractMargin.KimbleOne__InputInteger__c}" /></td>
						</apex:outputPanel>
						
						<td class="dataCell">
							<apex:outputField rendered="{!fs.IsResourced}" value="{!fs.Usage.KimbleOne__InputDecimal__c} " />
							<apex:outputText rendered="{!fs.IsResourced}"  value=" ({!fs.UsageUnits})"/>		
						</td>
						<td class="rightAlignDataCell">
							<apex:outputField value="{!fs.ContractRevenue.KimbleOne__InputDecimal__c}" />
						</td>
						<apex:outputPanel layout="none" rendered="{!ShowCosts}">
							<td class="rightAlignDataCell">
								<apex:outputField value="{!fs.ContractCost.KimbleOne__InputDecimal__c}" />
							</td>
							<td class="rightAlignDataCell" style="{!IF(!ISBLANK(fs.ContractMargin.KimbleOne__InputInteger__c) && fs.ContractMargin.KimbleOne__InputInteger__c < 0, 'color:red;font-weight:bold;','')}"><apex:outputField value="{!fs.ContractMargin.KimbleOne__InputInteger__c}" /></td>
						</apex:outputPanel>
						
						<td class="dataCell">
							<apex:outputField rendered="{!fs.IsResourced}" value="{!fs.ActualUsage.KimbleOne__InputDecimal__c} " />
							<apex:outputText rendered="{!fs.IsResourced}"  value=" ({!fs.UsageUnits})"/>	
						</td>
						<td class="rightAlignDataCell"><apex:outputField value="{!fs.RecognisedServicesRevenue.KimbleOne__InputDecimal__c}" /></td>
						<apex:outputPanel layout="none" rendered="{!ShowCosts}">
							<td class="rightAlignDataCell"><apex:outputField value="{!fs.RecognisedServicesCost.KimbleOne__InputDecimal__c}" /></td>
							<td class="rightAlignDataCell" style="{!IF(!ISBLANK(fs.RecognisedServicesMargin.KimbleOne__InputInteger__c) && fs.RecognisedServicesMargin.KimbleOne__InputInteger__c < 0, 'color:red;font-weight:bold;','')}"><apex:outputField value="{!fs.RecognisedServicesMargin.KimbleOne__InputInteger__c}" /></td>
						</apex:outputPanel>
						
						<td class="dataCell">
							<div class="DeliveryStatus {!JSENCODE(fs.DeliveryStatusStyleClass)} fa fa-circle" title="{!$Label.StatusLabel}: {!JSENCODE(fs.DeliveryStatusName)}"></div>										
						</td>
					</tr>
				</apex:repeat>
			</tbody>
		</table>
	</div>
</apex:outputPanel>   
</apex:page>