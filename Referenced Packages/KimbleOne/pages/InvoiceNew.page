<apex:page standardController="KimbleOne__Invoice__c" extensions="KimbleOne.InvoiceNewController" showHeader="true" sidebar="false" standardStylesheets="true" tabStyle="KimbleOne__Invoice__c">

<apex:composition template="KimbleOne__SiteMaster">
    <apex:define name="Menu">
      
    </apex:define>
    
    <apex:define name="Content">
    	<apex:includeScript value="{!URLFOR($Resource.KimbleOne__InvoiceManagement)}" />
    	<apex:form id="TheForm">
    	<div id="pageErrors">
    		<apex:pageMessages escape="False" />
		</div>
		
		<apex:pageBlock title="{!$Label.kimbleone__CreateNewInvoiceHeading}">
           	<apex:pageBlockButtons location="bottom">
           		<apex:commandButton action="{!Cancel}" styleclass="DisableButtonWhileBusy" value="{!$Label.kimbleone__Cancel}" rerender="TheForm" rendered="{!TheInvoiceCreationWizard.CanCancel}"/>
           		<apex:commandButton id="goPreviousBtn" action="{!TheInvoiceCreationWizard.GoPrevious}" styleclass="DisableButtonWhileBusy" value="{!$Label.kimbleone__Previous}" reRender="TheForm" rendered="{!TheInvoiceCreationWizard.CanGoPrevious && TheInvoiceCreationWizard.CurrentStep.StepName != 'SelectDeliveryElements'}"/>
            	<apex:commandButton id="goNextBtn" action="{!TheInvoiceCreationWizard.GoNext}" styleclass="DisableButtonWhileBusy theNextButton" value="{!TheInvoiceCreationWizard.NextLabel}" reRender="TheForm" rendered="{!TheInvoiceCreationWizard.CanGoNext}"/>
            </apex:pageBlockButtons>
			
			<!-- SELECT ACCOUNT -->
           	<apex:pageblockSection collapsible="false" showheader="true" title="{!$Label.kimbleone__SelectAccountHeading}" columns="1" rendered="{!TheInvoiceCreationWizard.CurrentStep.StepName == 'SelectAccountStep'}">
				<apex:inputField value="{!TheInvoiceCreationWizard.TheInvoice.KimbleOne__Account__c}"/>
				
			</apex:pageblockSection>
			
			<!--  Run the invoice schedule jobs (if any) -->
			<apex:outputPanel layout="none" rendered="{!TheInvoiceCreationWizard.CurrentStep.StepName == 'RunInvoiceScheduleJobs'}">
			
				<apex:actionFunction action="{!TheInvoiceCreationWizard.GoNext}" name="GoNext" reRender="TheForm"></apex:actionFunction>
				<div id="InvoiceScheduleJobs">
				
				</div>
				<script>
					$(function () {
		                	loadJobsInline($('#InvoiceScheduleJobs'), '{!JSINHTMLENCODE(TheInvoiceCreationWizard.TheInvoice.Account__c)}','{!$Label.InitialisingInvoicingdataMessage}', invoicingJobsComplete);
		            });
		            
		            function invoicingJobsComplete(ranJobs)
		            {
		            	if (ranJobs) // see if there are more to run
		            		refreshJobs();
		            	else
		            		GoNext();
		            }
		            
		        </script>
		        
		        <apex:actionFunction name="refreshJobs" rerender="TheForm"/>
		        
			</apex:outputPanel>	
			
           	<!-- SELECT DELIVERY ELEMENTS TO INCLUDE -->
			<apex:outputPanel layout="none" rendered="{!TheInvoiceCreationWizard.CurrentStep.StepName == 'SelectDeliveryElements'}">
			<apex:pageblockSection >
				<apex:outputField value="{!TheInvoiceCreationWizard.TheInvoice.KimbleOne__Account__c}"/>
				<apex:pageBlockSectionItem >
							<!-- render the selector but hidden if there is only one choice to preserve the layout and pass through the default to the controller -->
							<apex:outputText value="{!$ObjectType.KimbleOne__Invoice__c.Fields.KimbleOne__Locale__c.Label}" styleClass="{!IF(TheInvoiceLocales.size == 1,'hidden-item','')}"/>
							<apex:selectList id="invoiceLocaleSelect" value="{!TheInvoiceCreationWizard.TheInvoice.KimbleOne__Locale__c}" size="1" styleClass="{!IF(TheInvoiceLocales.size == 1,'hidden-item','')}">
								<apex:selectOptions value="{!TheInvoiceLocales}"/>
							</apex:selectList> 
				</apex:pageBlockSectionItem>
				
				<apex:inputField value="{!TheInvoiceCreationWizard.TheInvoice.KimbleOne__InvoiceDate__c}">
					<apex:actionSupport event="onchange" action="{!TheInvoiceCreationWizard.InvoiceDateChange}" rerender="InvoiceDeliveryElements"/>
				</apex:inputField>
				
				<apex:pageBlockSectionItem >
						<apex:outputText value="{!$ObjectType.KimbleOne__Invoice__c.Fields.KimbleOne__InvoiceFormat__c.Label}" />
						<apex:selectList id="invoiceFormatSelect" value="{!TheInvoiceCreationWizard.TheInvoice.KimbleOne__InvoiceFormat__c}" size="1">
							<apex:selectOptions value="{!TheInvoiceFormats}"/>
						</apex:selectList> 
				</apex:pageBlockSectionItem> 

			</apex:pageblockSection>
			
			<apex:pageblockSection id="InvoiceDeliveryElements" collapsible="false" showheader="true" title="{!$Label.kimbleone__SelectElementsToIncludeonInvoiceHeading}" columns="1" >
           		<apex:outputPanel rendered="{!TheInvoiceCreationWizard.TheGroupsAndElementsForSelection.size == 0}">
           			<apex:outputText value="{!$Label.kimbleone__NothingToInvoiceMessage}"/>
           		</apex:outputPanel>
           		<!--  if TheInvoiceCreationWizard.TheGroupsAndElementsForSelection.length == 1 then render list else render tree -->
           		<apex:repeat value="{!TheInvoiceCreationWizard.TheGroupsAndElementsForSelection}" var="grp">
           			<apex:pageBlockTable value="{!grp.DeliveryElements__r}" var="element">
						<apex:facet name="header"><apex:outputlink value="/{!grp.Id}"><apex:outputText value="{!grp.Name}"/></apex:outputlink></apex:facet>
						<apex:column width="10%" style="text-align:center;" headerValue="">
							<apex:inputField value="{!TheInvoiceCreationWizard.TheDeliveryElementsForSelection[element.Id].InputCheckbox__c}"/>
						</apex:column>
						<apex:column width="40%" value="{!element.Name}" />
						<apex:column width="25%" headervalue="{!$Label.kimbleone__InvoiceableAmountLabel}">
							<apex:outputPanel layout="block">
								<apex:outputText value="{!$Label.kimbleone__ServicesLabel} "/> 
								<apex:outputText value="{!element.KimbleOne__InvoicingCurrencyIsoCode__c}" styleclass="kimbleCurrencyCode" />
								<apex:outputField value="{!element.KimbleOne__InvoicingCcyServicesInvoiceableAmount__c}" />
							</apex:outputPanel>
							<apex:outputPanel layout="block" rendered="{!element.Product__r.ProductDomain__r.RevenueGenerationModel__r.KimbleOne__IsExpenseGenerated__c}">
								<apex:outputText value="{!$Label.kimbleone__ExpensesLabel} "/> 
								<apex:outputText value="{!element.KimbleOne__InvoicingCurrencyIsoCode__c}" styleclass="kimbleCurrencyCode" />
								<apex:outputField value="{!element.KimbleOne__InvoicingCcyExpensesInvoiceableAmount__c}" />
							</apex:outputPanel>
						</apex:column>
						<apex:column headervalue="{!$ObjectType.KimbleOne__DeliveryElement__c.Fields.KimbleOne__BillingAccount__c.Label}">
							<apex:outputField value="{!element.KimbleOne__BillingAccount__c}"/>
						</apex:column>
						<apex:column headervalue="{!$ObjectType.KimbleOne__DeliveryElement__c.Fields.KimbleOne__TaxCode__c.Label}">
							<apex:outputField value="{!element.KimbleOne__TaxCode__c}"/>
						</apex:column>
						<apex:column headerValue="{!$Label.kimbleone__PurchaseOrdersLabel}">
							<div>
								<apex:outputText value="{!element.PurchaseOrderRule__r.Name}"/>
							</div>
							<!-- The ALL cases -->
							<apex:outputPanel layout="block" rendered="{!TheInvoiceCreationWizard.DeliveryElementPurchaseOrderRemaining[element.id].AllRemaining.InputCurrency__c > 0}">
								{!$Label.PurchaseOrderCoverRemainingLabel}&nbsp;<apex:outputField value="{!TheInvoiceCreationWizard.DeliveryElementPurchaseOrderRemaining[element.id].AllRemaining.InputCurrency__c}"/> 
							</apex:outputPanel>
							
							<!--  not the ALL cases -->
							<apex:outputPanel layout="block" rendered="{!TheInvoiceCreationWizard.DeliveryElementPurchaseOrderRemaining[element.id].ServicesRemaining.InputCurrency__c > 0}">
								{!$Label.ServicesPurchaseOrderCoverRemainingLabel}&nbsp;<apex:outputField value="{!TheInvoiceCreationWizard.DeliveryElementPurchaseOrderRemaining[element.id].ServicesRemaining.InputCurrency__c}"/> 
							</apex:outputPanel>
							
							<apex:outputPanel layout="block" rendered="{!TheInvoiceCreationWizard.DeliveryElementPurchaseOrderRemaining[element.id].ExpensesRemaining.InputCurrency__c > 0}">
								{!$Label.ExpensesPurchaseOrderCoverRemainingLabel}&nbsp;<apex:outputField value="{!TheInvoiceCreationWizard.DeliveryElementPurchaseOrderRemaining[element.id].ExpensesRemaining.InputCurrency__c}"/>
							</apex:outputPanel>
							
						</apex:column>
					</apex:pageBlockTable>
				
           		</apex:repeat>
           	
           	</apex:pageblockSection>
           	<script type="text/javascript">
				$(function () {
					getReferenceData();
				});
			</script>
			</apex:outputPanel>
			<!-- SELECT DELIVERY ELEMENT ITEMS TO INCLUDE -->
			<apex:outputPanel rendered="{!TheInvoiceCreationWizard.CurrentStep.StepName == 'SelectDeliveryElementItemsToInclude'}">
			
			<apex:outputPanel id="NetAmountPanel">
				<apex:pageblockSection collapsible="false" showheader="false" columns="3" rendered="{!TheInvoiceCreationWizard.CurrentStep.StepName != 'SelectAccountStep'}">
	           		
	           		<apex:pageBlockSectionItem dataStyle="width:15%">
	           			<apex:outputText value="{!$ObjectType.KimbleOne__DeliveryElement__c.Fields.KimbleOne__BillingAccount__c.Label}" />
	           			<apex:outputField value="{!TheInvoiceCreationWizard.TheInvoice.KimbleOne__Account__c}"/>
	           		</apex:pageBlockSectionItem>
	           		<apex:pageBlockSectionItem dataStyle="width:15%">
	           			<apex:outputText value="{!$ObjectType.KimbleOne__Invoice__c.Fields.KimbleOne__InvoiceDate__c.Label}" />
	           			<apex:outputField value="{!TheInvoiceCreationWizard.TheInvoice.KimbleOne__InvoiceDate__c}"/>
	           		</apex:pageBlockSectionItem>
	           		<apex:pageBlockSectionItem dataStyle="width:15%">
	           			<apex:outputText value="{!$ObjectType.KimbleOne__Invoice__c.Fields.KimbleOne__NetAmount__c.Label}" />
	           			<span id="DeliveryElementSelectedItemsTotal"></span>	           			
	           		</apex:pageBlockSectionItem>
	           		
	           	</apex:pageblockSection>
           	</apex:outputPanel>
           
			<apex:pageblockSection collapsible="false" showheader="true" title="{!$Label.kimbleone__SelectItemstoincludeforHeading} {!TheInvoiceCreationWizard.TheDeliveryElementsToBeInvoicedMap[TheInvoiceCreationWizard.CurrentDeliveryElementId].Name}" columns="1"></apex:pageblockSection>
					
				<table cellpadding="0" cellspacing="2" border="0" width="100%">
					<tr>
						<td width="100%">
							<div id="jsTreeContainer" style="height:100%;overflow-y:auto">
								<div id="selectRevenueItems" ></div>
							</div>
						</td>
						<td>
							<apex:outputPanel layout="none" rendered="{!TheInvoiceCreationWizard.TheDeliveryElementsToBeInvoicedMap[TheInvoiceCreationWizard.CurrentDeliveryElementId].PurchaseOrderRule__r.IsExpensesPOCoverRequired__c || TheInvoiceCreationWizard.TheDeliveryElementsToBeInvoicedMap[TheInvoiceCreationWizard.CurrentDeliveryElementId].PurchaseOrderRule__r.IsServicesPOCoverRequired__c}">
								<div id="purchaseOrdersContainer" style="height:100%;width:400px;margin-left:8px;border: 1px solid #D4DADC;background-color: #FFF">
									<div id="header" style="background: white url('/img/alohaSkin/grid_headerbg.gif') 0 bottom repeat-x;color: black;font-weight: bold;font-size: 11px;height: 15px;padding: 5px 3px 3px 5px;border-bottom: 1px solid #CCC">{!$Label.kimbleone__AvailablePurchaseOrdersHeading}</div>
									
								</div>
								
								
							</apex:outputPanel>
						</td>
					</tr>
				</table>
				<style>
					.poContainer{float:left; margin-left:10px;}
					.poAllocationContainer{height:258px;min-width: 102px;}
					.poAllocationItemContainer{position: relative;height: 258px;display: inline-block;width: 102px;}
					.poName {text-align: center; font-weight:bold; font-size:0.8em;margin-bottom: 4px;}
					.allocationItem{position:absolute; bottom:0px; width:100px;text-align: center;margin-right:2px;}
					.remainingAmount{position:relative; top:-16px; text-align: center; width:100%; font-weight:bold; font-size:0.8em;}
					.Services{background-color:#729BC7}
					.Expenses{background-color:#E68F1A}
					.Deposit {background-color:#B42B33}
					.All {background-color:#028753}
					.errorItem {background-image: url('{!URLFOR($Resource.KimbleOne__JobImages, 'failed16.gif')}') !important;	background-repeat: no-repeat !important; background-position: center right !important;}
				</style>
				
				<apex:inputText styleClass="TheInvoiceableItemsJson" style="display:none" value="{!TheInvoiceCreationWizard.DeliveryElementRevenueItemAllocationMap[TheInvoiceCreationWizard.CurrentDeliveryElementId].TheInvoiceableItemsJson}"/>
				<apex:inputText styleClass="ThePOAllocationsJson" style="display:none"  value="{!TheInvoiceCreationWizard.DeliveryElementRevenueItemAllocationMap[TheInvoiceCreationWizard.CurrentDeliveryElementId].ThePOAllocationsJson}"/>
				<apex:inputText styleClass="TheDepositsJson" style="display:none"  value="{!TheInvoiceCreationWizard.DeliveryElementRevenueItemAllocationMap[TheInvoiceCreationWizard.CurrentDeliveryElementId].TheDepositsJson}"/>
				
				<script type="text/javascript">
			
				var theData;
				var thePOAllocations;
				//var theDeposits;
				
				var theCurrencyCode = '<apex:outputText value="{!TheInvoiceCreationWizard.TheInvoice.CurrencyISOCode}"/>';
				
				var allowPartItemInvoicing = {!TheInvoiceCreationWizard.TheAccount.AllowPartItemInvoicing__c};
				
				var allocations = new Array();
				
				$(function () {
					
					theData = jQuery.parseJSON($('.TheInvoiceableItemsJson').val());
					
					thePOAllocations = jQuery.parseJSON($('.ThePOAllocationsJson').val());
					
					//theDeposits = jQuery.parseJSON($('.TheDepositsJson').val());
					
					jQuery.each(theData, function(i, obj){
						updateFormattedAmount(obj);
					});
					
					initialisePurchaseOrders();
					
					buildRevenueItemsTree();
				});	
				
			</script>
			
			<style>
				#jsonTree a:hover {text-decoration:none; color:black; cursor:default;}
				.jstree-grid-wrapper{border: 1px solid #D4DADC;}
				.jstree-grid-header-regular{background: white url('/img/alohaSkin/grid_headerbg.gif') 0 bottom repeat-x; color:black;font-weight: bold;font-size: 11px;}
				.jstree-grid-container {border-bottom: 1px solid #CCC;}
				.jstree-grid-separator{padding-top: 7px;}
				.jstree-kimble-invoice.jstree-focused{background: #FFF;height: 275px;}
			</style>
				
			</apex:outputPanel>
			
			<!-- INCLUDE INVOICE ADJUSTMENT -->
			<apex:outputPanel layout="none" rendered="{!TheInvoiceCreationWizard.CurrentStep.StepName == 'IncludeInvoiceAdjustment'}">
           	<apex:pageblockSection collapsible="false" showheader="true" title="{!$Label.kimbleone__ConfirmDetailsHeading}" columns="3" >
				<apex:pageBlockSectionItem dataStyle="width:15%">
           			<apex:outputText value="{!$ObjectType.KimbleOne__DeliveryElement__c.Fields.KimbleOne__BillingAccount__c.Label}" />
           			<apex:outputField value="{!TheInvoiceCreationWizard.TheInvoice.KimbleOne__Account__c}"/>
           		</apex:pageBlockSectionItem>
           		<apex:pageBlockSectionItem dataStyle="width:15%">
           			<apex:outputText value="{!$ObjectType.KimbleOne__Invoice__c.Fields.KimbleOne__InvoiceDate__c.Label}" />
           			<apex:outputField value="{!TheInvoiceCreationWizard.TheInvoice.KimbleOne__InvoiceDate__c}"/>
           		</apex:pageBlockSectionItem>
           		<apex:pageBlockSectionItem dataStyle="width:15%">
           			<apex:outputText value="{!$ObjectType.KimbleOne__Invoice__c.Fields.KimbleOne__NetAmount__c.Label} {!TheInvoiceCreationWizard.TheInvoice.CurrencyISOCode}" />
           			<apex:outputField label="{!$Label.kimbleone__RunningNetTotalLabel}" value="{!TheInvoiceCreationWizard.InvoiceRunningTotal.KimbleOne__InputCurrency__c}"/>
           		</apex:pageBlockSectionItem>
			</apex:pageblockSection>
			
			<apex:pageBlockSection collapsible="false" columns="1" title="{!$Label.kimbleone__AddInvoiceAdjustmentHeading}">
				
				<apex:pageBlockSectionItem >
						<apex:outputText value="{!$ObjectType.KimbleOne__InvoiceAdjustment__c.Label} {!TheInvoiceCreationWizard.TheInvoice.CurrencyISOCode}" />
						<apex:inputField value="{!TheInvoiceCreationWizard.TheInvoiceAdjustment.KimbleOne__AdjustmentValue__c}" />
				</apex:pageBlockSectionItem>
				
				<apex:pageBlockSectionItem >
					<apex:outputText value="{!$ObjectType.KimbleOne__InvoiceLine__c.Fields.KimbleOne__Narrative__c.Label}" />
					<apex:inputText value="{!TheInvoiceCreationWizard.AdjustmentLineNarrative}"/>
				</apex:pageBlockSectionItem>
			
			</apex:pageblockSection>
			
			<script type="text/javascript">
				$(function () {
					getReferenceData();
				});
			</script>
			
			</apex:outputPanel>
			
			<!-- CONFIRM INVOICE DETAILS -->
			<apex:outputPanel layout="none" rendered="{!TheInvoiceCreationWizard.CurrentStep.StepName == 'ConfirmInvoiceDetails'}">
           	<apex:pageblockSection collapsible="false" showheader="true" title="{!$Label.kimbleone__ConfirmDetailsHeading}" columns="2" >
				<apex:outputField value="{!TheInvoiceCreationWizard.TheInvoice.KimbleOne__Account__c}"/>
				<apex:outputField label="{!$ObjectType.KimbleOne__Invoice__c.Fields.KimbleOne__NetAmount__c.Label} {!TheInvoiceCreationWizard.TheInvoice.CurrencyISOCode}" value="{!TheInvoiceCreationWizard.InvoiceRunningTotal.KimbleOne__InputCurrency__c}"/>
				
				<apex:inputField value="{!TheInvoiceCreationWizard.TheInvoice.KimbleOne__InvoiceDate__c}" />
				
				<apex:inputField required="true" value="{!TheInvoiceCreationWizard.TheInvoice.KimbleOne__InvoicePaymentTermDays__c}" />
				
				<apex:pageBlockSectionItem >
						<apex:outputText value="{!$ObjectType.KimbleOne__Invoice__c.Fields.KimbleOne__BusinessUnitTradingEntity__c.Label}" />
						<apex:inputText id="businessUnitTradingEntitySelect" value="{!TheInvoiceCreationWizard.TheInvoice.KimbleOne__BusinessUnitTradingEntity__c}" styleclass="kimblecustomlookup GetTradingEntityList null true false" required="true"/> 
				</apex:pageBlockSectionItem>
				
				<apex:pageBlockSectionItem >
						<apex:outputText value="{!$ObjectType.KimbleOne__Invoice__c.Fields.KimbleOne__ReceivableBankAccount__c.Label}" />
						<apex:inputText id="receivableBankAccountSelect" value="{!TheInvoiceCreationWizard.TheInvoice.KimbleOne__ReceivableBankAccount__c}" styleclass="kimblecustomlookup GetBankAccountList null true false" required="true"/> 
				</apex:pageBlockSectionItem>
				
				<apex:inputText value="{!TheInvoiceCreationWizard.TheInvoice.KimbleOne__Contact__c}" styleclass="kimblecustomlookup GetAllAccountContacts {!TheInvoiceCreationWizard.TheInvoice.KimbleOne__Account__c} false true"/>
				
				<apex:inputField value="{!TheInvoiceCreationWizard.TheInvoice.KimbleOne__Notes__c}"/>
			</apex:pageblockSection>
			
			<apex:pageBlockSection columns="1" title="{!$Label.kimbleone__PresentationDetailsHeading}">
				
				<apex:pageBlockSectionItem >
						<apex:outputText value="{!$ObjectType.KimbleOne__Invoice__c.Fields.KimbleOne__InvoiceTemplate__c.Label}" />
						<apex:inputText value="{!TheInvoiceCreationWizard.TheInvoice.KimbleOne__InvoiceTemplate__c}" styleclass="kimblecustomlookup GetCommercialDocumentTemplateList Invoice true false"/> 
				</apex:pageBlockSectionItem>
				
				<apex:inputField value="{!TheInvoiceCreationWizard.TheInvoice.KimbleOne__InvoiceContextDescription__c}" style="width:400px" rendered="{!TheInvoiceCreationWizard.InvoiceFormat.KimbleOne__ShowContext__c}" />
			
			</apex:pageblockSection>
			
			<apex:pageBlockSection columns="2" title="{!$Label.kimbleone__CustomDetailsHeading}" rendered="{!$ObjectType.KimbleOne__Invoice__c.FieldSets.KimbleOne__InvoiceDetails.size != 0}">
				<apex:repeat value="{!$ObjectType.KimbleOne__Invoice__c.FieldSets.KimbleOne__InvoiceDetails}" var="i">
     					<apex:inputField value="{!KimbleOne__Invoice__c[i]}"/>
   					</apex:repeat>
   			</apex:pageBlockSection>
			
			<script type="text/javascript">
				$(function () {
					getReferenceData();
				});
			</script>
			
			</apex:outputPanel>
			
			<apex:outputPanel rendered="{!TheInvoiceCreationWizard.HasFinished}">
                 <script>
		                safeNavigateToSForceURL('{!JSENCODE(RedirectInvoicePreview)}');
		          </script>
	               	   
           	</apex:outputPanel>
					
			</apex:pageBlock>
	
		</apex:form>
		
		<style>
		.mainTitle {white-space:nowrap;}
		.tableStyle {border-collapse: collapse; border-spacing: 0px 0px; }
        .colStyle1 { width: 33%;text-align:right; padding-top:3px; padding-bottom:5px}
        .colStyle2 { width: 33%; padding-left:20px; padding-top:5px; padding-bottom:5px}
        .colStyle3 { width: 33%;text-align:right; padding-top:5px; padding-bottom:5px}
        .rowstyle { border-bottom-style:solid; border-bottom-width:1px;border-bottom-color:#E8E8E8 }
    </style>

    </apex:define>
</apex:composition>

</apex:page>