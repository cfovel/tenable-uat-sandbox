<apex:page standardController="KimbleOne__InterfaceRun__c" extensions="KimbleOne.PaymentStatementHomeController" showHeader="true" sidebar="false" standardStylesheets="true">
<apex:composition template="KimbleOne__SiteMaster">
    	<apex:define name="Menu"></apex:define> 
    
    	<apex:define name="Content">
    		<apex:form id="TheForm">
	    		<div id="pageErrors">
	    			<apex:pageMessages escape="false"/>
				</div>
				<apex:pageBlock title="{!$Label.kimbleone__PaymentStatementHeading}" mode="edit">
					
					<apex:pageBlockButtons location="top">
	           			<apex:commandButton action="{!Delete}" value="{!$Label.kimbleone__DeleteButton}" rendered="{!CompletedLinesCount.KimbleOne__InputInteger__c = 0}"/>
	           			<apex:commandButton action="{!Confirm}" value="{!$Label.kimbleone__ConfirmButton}" rendered="{!KimbleOne__InterfaceRun__c.Status__r.KimbleOne__Enum__c = 'AwaitingConfirmation'}" rerender="TheForm"/>	            				           			
          			</apex:pageBlockButtons>
  			      	
  			      	<apex:pageBlockSection columns="2" collapsible="false" showheader="true" title="{!$Label.kimbleone__FileDetailsHeading}">
			      		
			      		<apex:outputField value="{!KimbleOne__InterfaceRun__c.KimbleOne__Status__c}"/>
			      		<apex:outputField value="{!KimbleOne__InterfaceRun__c.KimbleOne__NumberOfRecords__c}"/>
			      		
			      		<apex:pageBlockSectionItem >
							<apex:outputLabel value="{!$Label.kimbleone__NumberOfIgnoredLinesLabel}" />
							<apex:outputField value="{!IgnoredLinesCount.KimbleOne__InputInteger__c}"/> 
			      		</apex:pageBlockSectionItem>
			      		
			      		<apex:pageBlockSectionItem >
							<apex:outputLabel value="{!$Label.kimbleone__NumberOfErroredLinesLabel}" />
							<apex:outputField value="{!ErroredLinesCount.KimbleOne__InputInteger__c}"/> 
			      		</apex:pageBlockSectionItem>
			      		
			      		<apex:pageBlockSectionItem >
							<apex:outputLabel value="{!$Label.kimbleone__NumberOfCompletedLinesLabel}" />
							<apex:outputField value="{!CompletedLinesCount.KimbleOne__InputInteger__c}"/> 
			      		</apex:pageBlockSectionItem>
			      		
			      		<apex:pageBlockSectionItem >
							<apex:outputLabel value="{!$Label.kimbleone__NumberOfReadyLinesLabel}" />
							<apex:outputField value="{!ReadyLinesCount.KimbleOne__InputInteger__c}"/> 
			      		</apex:pageBlockSectionItem>
			      		
			      	</apex:pageBlockSection>
			    	
			    	<apex:pageBlockSection columns="1" collapsible="false" showheader="false">
			    	
			    		<apex:pageBlockSectionItem >
							<apex:outputLabel value="{!$Label.kimbleone__FilterbystatusLabel}" />
							<apex:selectList id="ActivityList" value="{!FilterStatusId}"  size="1">
								<apex:actionSupport event="onchange" action="{!UpdateLines}" rerender="TheForm"/>
								<apex:selectOptions value="{!TheLineStates}" />
							</apex:selectList>
			      		</apex:pageBlockSectionItem>
			    		<apex:inputHidden value="{!TheCurrentPageNumber}"/>
			    	 
			    		<apex:pageBlockTable value="{!ThePagedMatchingLines}" var="line">
			    		<apex:column headervalue="{!$Label.kimbleone__ActionLinksHeader}" style="text-align:center;" styleclass="actionColumn" width="60px">
      						<apex:outputPanel layout="block" rendered="{!line.Status__r.KimbleOne__Enum__c != 'Ignored' && line.Status__r.KimbleOne__Enum__c != 'Completed'}">
      							<apex:outputLink styleclass="actionLink" value="javascript:void(0)" onclick="$(this).closest('tr').find('.loadingImage').show();$(this).parent().hide();ignoreItem('{!JSINHTMLENCODE(line.id)}')" >{!$Label.kimbleone__IgnoreLink}</apex:outputLink>&nbsp;|&nbsp;
      							<apex:outputLink styleclass="actionLink" value="javascript:void(0)" onclick="initialiseModalBoxy('#LinePopup', '{!$ObjectType.KimbleOne__PaymentStatementLine__c.Label}');viewItem('{!JSINHTMLENCODE(line.id)}')">{!$Label.kimbleone__View}</apex:outputLink>	
      						</apex:outputPanel>
      						<apex:outputLink styleclass="actionLink" value="javascript:void(0)" onclick="$(this).next().show();$(this).hide();reinstateItem('{!JSINHTMLENCODE(line.id)}')" rendered="{!line.Status__r.KimbleOne__Enum__c = 'Ignored'}">{!$Label.kimbleone__ReinstateLink}</apex:outputLink>
      						
      						<apex:outputLink styleclass="actionLink" value="/{!line.KimbleOne__Payment__c}" rendered="{!line.Status__r.KimbleOne__Enum__c = 'Completed'}">{!$Label.kimbleone__ViewPaymentLink}</apex:outputLink>
      						
      						<span class="loadingImage" style="display:none;"><img src="{!URLFOR($Resource.LoadingImage)}" /></span>
      						
      					</apex:column>
      					<apex:column value="{!line.KimbleOne__LineNumber__c}" title="{!line.KimbleOne__TransactionIdentifier__c}"/>
      					<apex:column value="{!line.KimbleOne__Account__c}"/>
      					<apex:column value="{!line.KimbleOne__Invoice__c}"/>
      					<apex:column value="{!line.KimbleOne__PaymentReference__c}"/>
      					<apex:column value="{!line.KimbleOne__PaymentDate__c}"/>
      					<apex:column value="{!line.KimbleOne__PaymentAmount__c}"/>
      					<apex:column value="{!line.KimbleOne__ErrorMessage__c}"/>
      					
			    	</apex:pageBlockTable>
			    	</apex:pageBlockSection>
			    	<div style="float: right; margin-right: 15px; margin-bottom:15px;">
			    		<apex:commandLink style="color: #015BA7;text-decoration: none;" action="{!GoPrevious}" rendered="{!CanGoPrevious}" value="{!$Label.kimbleone__PreviousPageLink}"/>
			    		<apex:outputText styleclass="greyedLink" rendered="{!!CanGoPrevious}" value="{!$Label.kimbleone__PreviousPageLink}"/>
			    		&nbsp;|&nbsp;
			    		<apex:commandLink style="color: #015BA7;text-decoration: none;" action="{!GoNext}" rendered="{!CanGoNext}" value="{!$Label.kimbleone__NextPageLink}"/>
			    		<apex:outputText styleclass="greyedLink" rendered="{!!CanGoNext}" value="{!$Label.kimbleone__NextPageLink}"/>
		
			    	</div>
			    	<div style="clear:both;"></div>
				</apex:pageBlock>
				
				<apex:actionFunction action="{!ViewItem}" name="viewItem" oncomplete="showModalBoxy();" rerender="LineDetails">
					<apex:param name="lineId" value=""/>
				</apex:actionFunction>
				
				<apex:actionFunction action="{!IgnoreItem}" name="ignoreItem" rerender="TheForm">
					<apex:param name="lineId" value=""/>
				</apex:actionFunction>
				<apex:actionFunction action="{!ReinstateItem}" name="reinstateItem" rerender="TheForm">
					<apex:param name="lineId" value=""/>
				</apex:actionFunction>
				 </apex:form>
				 
				<div id="LinePopup" style="display: none;">
					<apex:outputPanel id="LineDetails">
						<apex:form >
						<div id="errors">
							<apex:pageMessages escape="false" />
						</div>
						<apex:pageBlock title="{!$Label.kimbleone__LineHeading} {!TheCurrentLine.KimbleOne__LineNumber__c}" mode="edit">
							<apex:pageBlockButtons location="bottom">
	           					<apex:commandButton value="{!$Label.kimbleone__IgnoreButton}" action="{!IgnoreCurrentItem}" styleclass="DisableButtonWhileBusy" rendered="{!TheCurrentLine.Status__r.KimbleOne__Enum__c != 'Ignored' && TheCurrentLine.Status__r.KimbleOne__Enum__c != 'Completed'}" rerender="TheForm" oncomplete="hideModalBoxyWithErrorCheck('#errors');"/>
	           					<input type="button" onclick="hideModalBoxy()" class="btn" value="{!$Label.Cancel}" />         				           			
          					</apex:pageBlockButtons>
          					<apex:pageBlockSection columns="1" collapsible="false" showheader="false">
          						<apex:outputField value="{!TheCurrentLine.KimbleOne__TransactionIdentifier__c}"/>
          						<apex:outputField value="{!TheCurrentLine.KimbleOne__Status__c}"/>
          						<apex:outputField value="{!TheCurrentLine.KimbleOne__Account__c}"/>
          						<apex:outputField value="{!TheCurrentLine.KimbleOne__Invoice__c}"/>
          						<apex:outputField value="{!TheCurrentLine.KimbleOne__PaymentReference__c}"/>
          						<apex:outputField value="{!TheCurrentLine.KimbleOne__PaymentDate__c}"/>
          						<apex:outputField value="{!TheCurrentLine.KimbleOne__PaymentAmount__c}"/>
          						<apex:pageBlockSectionItem >
									<apex:outputLabel value="{!$ObjectType.KimbleOne__PaymentStatementLine__c.Fields.KimbleOne__RawData__c.Label}" />
									<apex:outputPanel layout="block" style="width:200px"><apex:outputField value="{!TheCurrentLine.KimbleOne__RawData__c}" style="width:200px;"/></apex:outputPanel>
								</apex:pageBlockSectionItem>	
          						
          						<apex:outputField value="{!TheCurrentLine.KimbleOne__ErrorMessage__c}" />
          						
          					</apex:pageBlockSection>
						</apex:pageBlock>
						</apex:form>
					</apex:outputPanel>
				</div>
   				
   			
   			<apex:relatedList list="NotesAndAttachments" />
   			
		</apex:define>
	</apex:composition>
</apex:page>