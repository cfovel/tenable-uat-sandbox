/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/v1.1/tasks/*')
global class TaskRestService {
    global TaskRestService() {

    }
    @HttpDelete
    global static void doDelete() {

    }
    @HttpPost
    global static void doPost(List<KimbleOne.PlannedActivityDTO.Task> tsks, String ResourcedActivityId) {

    }
    @HttpPut
    global static void doPut(List<KimbleOne.PlannedActivityDTO.Task> tsks, String ResourcedActivityId) {

    }
}
