/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/v1.0/ExpenseItems/*')
global class ExpenseItemsRestService {
    global ExpenseItemsRestService() {

    }
    @HttpDelete
    global static void DeleteExpenseItem() {

    }
    @HttpPost
    global static KimbleOne.TimeAndExpenseRestService.ExpenseItemIdentifierDto InsertExpenseItem() {
        return null;
    }
    @HttpPut
    global static KimbleOne.TimeAndExpenseRestService.ExpenseItemIdentifierDto UpdateExpenseItem() {
        return null;
    }
}
