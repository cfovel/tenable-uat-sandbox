/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ReferenceDataWebService {
    global ReferenceDataWebService() {

    }
    webService static List<Account> GetAccounts(String filter) {
        return null;
    }
    webService static List<AggregateResult> GetActiveCurrencyTypes(String filter) {
        return null;
    }
    webService static List<KimbleOne__ActivityAssignmentTemplate__c> GetActivityAssignmentTemplatesByProduct(String filter) {
        return null;
    }
    webService static List<KimbleOne.ReferenceDataWebService.MockRefenceData> GetActivityExpenseCategoriesForEntry(String filter1) {
        return null;
    }
    webService static List<KimbleOne.ReferenceDataWebService.MockRefenceData> GetActivityExpenseCategoriesForResourceLocationAndType(String filter1, String resourceType, String resourceLocation) {
        return null;
    }
    webService static List<KimbleOne.ReferenceDataWebService.MockRefenceData> GetActivityRateForEntryExcludeTaskAndReference(String filter1, String filter2) {
        return null;
    }
    webService static List<KimbleOne.ReferenceDataWebService.MockRefenceData> GetActivityRateForEntry(String filter1, String filter2) {
        return null;
    }
    webService static List<KimbleOne__ActivityRole__c> GetActivityRolesByActivityType(String filter) {
        return null;
    }
    webService static List<Contact> GetAllAccountContacts(String filter) {
        return null;
    }
    webService static List<User> GetAllActiveUsers(String filter) {
        return null;
    }
    webService static List<KimbleOne__ActivityRole__c> GetAllActivityRoles(String filter) {
        return null;
    }
    webService static List<KimbleOne__ForecastStatus__c> GetAllForecastStatuses(String filter) {
        return null;
    }
    webService static List<KimbleOne__Location__c> GetAllLocations(String filter) {
        return null;
    }
    webService static List<KimbleOne__ResourceType__c> GetAllResourceTypes(String filter) {
        return null;
    }
    webService static List<KimbleOne__Resource__c> GetAllResources(String filter) {
        return null;
    }
    webService static List<KimbleOne__TaskCategory__c> GetAllTaskCategories(String filter) {
        return null;
    }
    webService static List<KimbleOne__AnalysisDimension__c> GetAnalysisDimensions(String filter) {
        return null;
    }
    webService static List<KimbleOne__AnalysisFact__c> GetAnalysisFacts(String filter) {
        return null;
    }
    webService static List<KimbleOne__ApproverType__c> GetApproverTypes(String filter) {
        return null;
    }
    webService static List<KimbleOne__BankAccount__c> GetBankAccountList(String filter1) {
        return null;
    }
    webService static List<KimbleOne__BusinessUnitGroup__c> GetBusinessUnitGroups(String filter) {
        return null;
    }
    webService static List<KimbleOne__BusinessUnit__c> GetBusinessUnitList(String filter) {
        return null;
    }
    webService static List<KimbleOne__Calendar__c> GetCalendars(String filter) {
        return null;
    }
    webService static List<KimbleOne__CandidateStage__c> GetCandidateStageList(String filter1) {
        return null;
    }
    webService static List<KimbleOne__CandidateStatus__c> GetCandidateStatusList(String filter1) {
        return null;
    }
    webService static List<KimbleOne__ReferenceData__c> GetChangeControlStatuses(String filter) {
        return null;
    }
    webService static List<KimbleOne__CommercialDocumentTemplate__c> GetCommercialDocumentTemplateList(String filter1) {
        return null;
    }
    webService static List<KimbleOne__CommissionScheme__c> GetCommissionScheme(String filter) {
        return null;
    }
    webService static List<AggregateResult> GetCreditNoteStatusList(String filter) {
        return null;
    }
    webService static List<KimbleOne__ForecastStatus__c> GetDeliveryProcessForecastStatuses(String filter) {
        return null;
    }
    webService static List<KimbleOne__DeliveryProgram__c> GetDeliveryPrograms(String filter) {
        return null;
    }
    webService static List<KimbleOne__DeliveryStatus__c> GetDeliveryStatusList(String filter1) {
        return null;
    }
    webService static List<KimbleOne__ExpenseCategory__c> GetExpenseCategories(String filter) {
        return null;
    }
    webService static List<KimbleOne__ForecastStatus__c> GetForecastStatuses(String filter) {
        return null;
    }
    webService static List<KimbleOne__Grade__c> GetGradesByResourceClass(String filter) {
        return null;
    }
    webService static List<Group> GetGroups(String filter) {
        return null;
    }
    webService static List<KimbleOne__InvoiceFormat__c> GetInvoiceFormatList(String filter) {
        return null;
    }
    webService static List<KimbleOne__ReferenceData__c> GetItems(String domain) {
        return null;
    }
    webService static List<KimbleOne__MarketingCampaign__c> GetMarketingCampaigns(String filter) {
        return null;
    }
    webService static List<KimbleOne__ReferenceData__c> GetMilestoneTypeWithCostAndRevenueFilter2(String filter) {
        return null;
    }
    webService static List<KimbleOne__ReferenceData__c> GetMilestoneTypeWithCostAndRevenueFilter(String filter) {
        return null;
    }
    webService static List<KimbleOne__BusinessUnit__c> GetOperatingEntityList(String filter) {
        return null;
    }
    webService static List<KimbleOne__OpportunitySource__c> GetOpportunitySources(String filter) {
        return null;
    }
    webService static List<KimbleOne__PerformanceAnalysis__c> GetPerformanceAnalysis(String filter) {
        return null;
    }
    webService static List<KimbleOne__PeriodRateBand__c> GetPeriodRateBandNotItems(String id) {
        return null;
    }
    webService static List<KimbleOne.ReferenceDataWebService.MockRefenceData> GetPlanTaskForEntry(String filter1, String filter2) {
        return null;
    }
    webService static List<KimbleOne__BusinessUnit__c> GetPrimaryOrganisationalEntityList(String filter) {
        return null;
    }
    webService static List<KimbleOne__ProductDomain__c> GetProductDomainList(String filter1) {
        return null;
    }
    webService static List<KimbleOne__ProductGroup__c> GetProductGroupList(String filter1) {
        return null;
    }
    webService static List<KimbleOne__Product__c> GetProductList(String filter1) {
        return null;
    }
    webService static List<KimbleOne__Proposition__c> GetPropositions(String filter) {
        return null;
    }
    webService static List<KimbleOne__PurchaseOrderRule__c> GetPurchaseOrderRules(String filter) {
        return null;
    }
    webService static List<KimbleOne__ReferenceData__c> GetRebillingRulesPerUnit(String filter) {
        return null;
    }
    webService static List<KimbleOne__ReferenceData__c> GetReimbursementRulesPerUnit(String filter) {
        return null;
    }
    webService static List<KimbleOne__Resource__c> GetResourceGroups(String filter) {
        return null;
    }
    webService static List<KimbleOne__ResourceType__c> GetResourceTypesNotGenericList(String filter1) {
        return null;
    }
    webService static List<KimbleOne__ResourceType__c> GetResourceTypes(String filter) {
        return null;
    }
    webService static List<KimbleOne__ResourcedActivity__c> GetResourcedActivitiesReadyForUnavailabilityEntry(String filter) {
        return null;
    }
    webService static List<KimbleOne__Resource__c> GetResourcesNotGeneric(String filter) {
        return null;
    }
    webService static List<KimbleOne__RevenueAdjustmentReason__c> GetRevenueAdjustmentReasons(String filter) {
        return null;
    }
    webService static List<KimbleOne__RiskImpact__c> GetRiskImpacts(String filter) {
        return null;
    }
    webService static List<KimbleOne__RiskProbability__c> GetRiskProbabilities(String filter) {
        return null;
    }
    webService static List<KimbleOne__SalesOpportunity__c> GetSalesOpportunities(String filter) {
        return null;
    }
    webService static List<KimbleOne__ForecastStatus__c> GetSalesProcessForecastStatuses(String filter) {
        return null;
    }
    webService static List<KimbleOne__BusinessUnit__c> GetSecondaryOrganisationalEntityList(String filter) {
        return null;
    }
    webService static List<KimbleOne__TaxCode__c> GetTaxCodeList(String filter) {
        return null;
    }
    webService static List<KimbleOne__ReferenceData__c> GetTimePatternRules(String filter) {
        return null;
    }
    webService static List<KimbleOne__TimePattern__c> GetTimePatterns(String filter) {
        return null;
    }
    webService static List<KimbleOne__TimePeriod__c> GetTimePeriodsForecastAtThisLevel(String filter) {
        return null;
    }
    webService static List<KimbleOne__TimePeriod__c> GetTimePeriodsOpenAndForecastAtThisLevel(String filter) {
        return null;
    }
    webService static List<KimbleOne__TimePeriod__c> GetTimePeriodsOpenAndTrackAtThisLevel(String filter) {
        return null;
    }
    webService static List<KimbleOne__BusinessUnit__c> GetTradingEntityList(String filter) {
        return null;
    }
    webService static List<KimbleOne__UnitType__c> GetUnitTypeList(String filter) {
        return null;
    }
    webService static List<KimbleOne__UnitTypeStatus__c> GetUnitTypeStatuses(String filter) {
        return null;
    }
    webService static List<User> GetUsersWithoutAResource(String filter) {
        return null;
    }
    webService static List<User> GetUsersWithoutCommissionAgreements(String filter) {
        return null;
    }
    webService static List<User> GetUsersWithoutResourceWithPlatformLicense(String filter) {
        return null;
    }
global class JSMessage {
    @WebService
    webService List<String> messages;
    @WebService
    webService Boolean wasSuccessful;
    global JSMessage() {

    }
}
global class MockRefenceData implements System.Comparable {
    @WebService
    webService String Id;
    @WebService
    webService String Name;
    global MockRefenceData() {

    }
    global Integer compareTo(Object compareTo) {
        return null;
    }
}
}
