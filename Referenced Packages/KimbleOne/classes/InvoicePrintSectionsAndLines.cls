/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class InvoicePrintSectionsAndLines {
    global Map<Id,Boolean> CanShowSectionInTabularFormat {
        get;
        set;
    }
    global List<KimbleOne__InvoiceSection__c> TheSectionsAndLines {
        get;
        set;
    }
    global InvoicePrintSectionsAndLines() {

    }
}
