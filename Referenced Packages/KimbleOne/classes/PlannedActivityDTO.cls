/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PlannedActivityDTO {
    global PlannedActivityDTO() {

    }
global class Assignment {
    global Assignment() {

    }
}
global class Predecessor {
    global Predecessor() {

    }
}
global class Project {
}
global class Resource {
    global Resource() {

    }
}
global class ResourceWithODate {
    global ResourceWithODate() {

    }
}
global class Task {
}
}
