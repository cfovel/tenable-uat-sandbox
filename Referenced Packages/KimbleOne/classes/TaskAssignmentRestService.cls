/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/v1.1/taskassignments/*')
global class TaskAssignmentRestService {
    global TaskAssignmentRestService() {

    }
    @HttpDelete
    global static void doDelete() {

    }
    @HttpPost
    global static void doPost(List<KimbleOne.PlannedActivityDTO.Assignment> tskAssignments, String ResourcedActivityId) {

    }
    @HttpPut
    global static void doPut(List<KimbleOne.PlannedActivityDTO.Assignment> tskAssignments) {

    }
}
