/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ExportSystemService {
    global ExportSystemService() {

    }
    global static void ExportExpenseClaimsAndSaveAsAttachment(KimbleOne.IExportStrategy exportStrategy) {

    }
    global static void ExportExpenseItemsAndSaveAsAttachment(KimbleOne.IExportStrategy exportStrategy) {

    }
}
