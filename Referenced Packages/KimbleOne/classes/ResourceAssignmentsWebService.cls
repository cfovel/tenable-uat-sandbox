/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ResourceAssignmentsWebService {
    global ResourceAssignmentsWebService() {

    }
    webService static List<KimbleOne__Resource__c> FindResourcesById(List<String> resources) {
        return null;
    }
    webService static List<KimbleOne__Resource__c> FindResourcesByTypeBusUnitAcctGradeLoc(List<String> resourceTypes, List<String> businessUnits, List<String> grades, List<String> accounts, List<String> locations) {
        return null;
    }
    webService static List<KimbleOne__Resource__c> FindResourcesForAccounts(List<String> resourceTypes, List<String> businessUnits, List<String> grades, List<String> accounts) {
        return null;
    }
    webService static List<KimbleOne__Resource__c> FindResources(List<String> resourceTypes, List<String> businessUnits, List<String> grades) {
        return null;
    }
    webService static List<KimbleOne__Resource__c> GetResources(List<String> resources) {
        return null;
    }
}
