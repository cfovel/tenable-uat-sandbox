/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class FinanceExportStrategyCsv implements KimbleOne.IFinanceExportStrategy {
    global FinanceExportStrategyCsv(Set<Id> transactionItemIds) {

    }
    global String ContentType() {
        return null;
    }
    global String Description() {
        return null;
    }
    global String Filename() {
        return null;
    }
    global Blob GenerateTheBody() {
        return null;
    }
    global Set<Id> TransactionItemIds() {
        return null;
    }
}
