/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class AddressDTO {
    global String Addressee {
        get;
        set;
    }
    global String City {
        get;
        set;
    }
    global String CompanyName {
        get;
        set;
    }
    global String Country {
        get;
        set;
    }
    global String PostalCode {
        get;
        set;
    }
    global String State {
        get;
        set;
    }
    global String Street {
        get;
        set;
    }
    global AddressDTO() {

    }
}
