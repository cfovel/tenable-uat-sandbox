/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/v1.1/plannedactivities/*')
global class PlannedActivityRestService {
    global PlannedActivityRestService() {

    }
    @HttpGet
    global static List<KimbleOne.PlannedActivityDTO.Project> doGet() {
        return null;
    }
    @HttpPut
    global static void doPost(List<KimbleOne.PlannedActivityDTO.ResourceWithODate> Resources) {

    }
    @HttpPost
    global static void savePlan(KimbleOne.PlannedActivityDTO.Project thePlan) {

    }
}
