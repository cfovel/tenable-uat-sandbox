({
	doInit : function(component, event, helper) {
		helper.getAppSettings(component);
		helper.requestSession(component);
	},
	handleGotSessionEvent : function(component, event, helper) {
		var session = event.getParams();
		if(!$A.util.isEmpty(session.session.BNSKSession.Key)) {
			helper.setCurrentSession(component,session.session);
			helper.setSessionParams(component,session.session);
			helper.setUrl(component,session.session);
			component.set('v.hasSession',true);
		}

	}
})