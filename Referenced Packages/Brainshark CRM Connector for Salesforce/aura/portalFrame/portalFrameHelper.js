({
    contentUrls: {
        content : {url:'/brainshark/brainshark.net/salesportal/home.aspx',addParam:''},
        learning: {url:'/brainshark/brainshark.net/learning/home.aspx',addParam:''},
        coaching: {url:'/brainshark/brainshark.net/coaching/home.aspx',addParam:''},
        reporting: {url:'/brainshark/brainshark.net/Reporting/home.aspx',addParam:''},
        favorites: {url:'/brainshark/brainshark.net/salesportal/favorites.aspx',addParam:''},
		profile: {url:'/brainshark/brainshark.net/sfdc/profile.aspx',addParam:''}
    },
    setUrl : function(component) {
		var session = component.get('v.session');
        var appHost = component.get('v.appHost');
		var sessionParams = component.get('v.sessionParams');
        var uiContent = component.get('v.uiContent');
        var searchTerm = $A.util.isEmpty(component.get('v.searchTerm')) ? '' : component.get('v.searchTerm');
		var searchParam = searchTerm.indexOf('/') === -1 ? '/q' : ''; //If the user searches using another flag then skip the /q
		if(!$A.util.isEmpty(session) && !$A.util.isEmpty(appHost) && !$A.util.isEmpty(sessionParams)) {
			//Lets put some info on the url
			console.log('innards of seturl');
			var params = $A.util.isEmpty(this.contentUrls[uiContent.toLowerCase()].addParam) ? '' : this.contentUrls[uiContent.toLowerCase()].addParam;
			var profileType = '&profile=private';

			//If we are on the profile page see if we are looking at our own or someone else. If we are not on the
			//a page with the User object then always show our private profile
			if(uiContent.toLowerCase() === 'profile') {
				profileType = (component.get('v.sObjectName') === 'User' && (component.get('v.recordId') !== session.SFDCUserID)) ? '&profile=public' : profileType;
				params += profileType;
			}
			var baseParams = this.getBaseUrlParameters(component,session);
			//add the params together and add the final sfdc.view to tell us we are in a LC
			var params = params + baseParams + '&sfdc.view=lc';

			if(!$A.util.isEmpty(searchTerm)) {
				component.set('v.addParam',params + '#search'+ searchParam + searchTerm);
			} else {
				component.set('v.addParam',params);
			}
        	component.set('v.contentUrl',appHost + this.contentUrls[uiContent.toLowerCase()].url);
			//component.set('v.hasSession',true);
		}


    }

})