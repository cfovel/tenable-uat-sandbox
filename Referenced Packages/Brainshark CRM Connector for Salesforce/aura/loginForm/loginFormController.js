({
	doInit: function(component, event, helper) {
		helper.checkingSession = false;
		helper.getUserSession(component);
	},
	submitForm : function(component, event, helper) {
		if(helper.validateUsername(component,true) &&
			helper.validatePassword(component,true)) {
				helper.tryLogin(component);
		}
	},
	validateUsername : function(component, event, helper) {
		var username = component.find('username');
		username.set('v.errors',null);
		if(helper.validateUsername(component,true) &&
			helper.validatePassword(component,false)) {
				helper.disableLoginButton(component,false);
		} else {
			helper.disableLoginButton(component,true);
		}
	},
	validatePassword : function(component, event, helper) {
		var password = component.find('password');
		password.set('v.errors',null);
		if(helper.validatePassword(component,true) &&
			helper.validateUsername(component,false)) {
				helper.disableLoginButton(component,false);
		} else {
			helper.disableLoginButton(component,true);
		}

	},
	gatherSessionEvent: function(component,event, helper) {
		//Only handle event from other senders - not ourself
		if(event.getParam('eventType') === 'REQUEST') {
			helper.getUserSession(component);
		}
	}
})