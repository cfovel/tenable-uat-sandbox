({
	checkingSession: false,
	getUserSession : function(component) {
		if(!$A.util.isUndefinedOrNull(component.get('v.session'))) {
			this.fireSessionEvent(component,component.get('v.session'));
		}
		var action = component.get('c.getActiveUserSession');
		if(!this.checkingSession) {
			this.checkingSession = true;
			action.setCallback(this,function(response) {
				var state = response.getState();
				if(component.isValid() && state === 'SUCCESS') {
					var ret = response.getReturnValue();
					if(ret.BNSKSession.Id === -1) {
						this.showLoginForm(component);
					} else {
						if(component.get('v.autoHide')) {
							this.hideLoginForm(component);
						} else {
							this.showLoginForm(component);
						}
						component.set('v.session',ret);
						this.fireSessionEvent(component,ret);
					}
				}else{
					var errors = response.getError();
					var errMessage = '';
					for (var errI = 0; errI < errors.length; errI++) {
						errMessage += (errMessage === '' ? '' : '</br>');
						errMessage += errors[errI].message;
					}
					this.throwAppMessage(component,'ERROR',state,'There was an error.',component.get('v.componentName'),errMessage,0);


				}
				this.checkingSession = false;
			});
			$A.enqueueAction(action);
		}
    },
	showLoginError: function(component,show) {
		if(show) {
			$A.util.removeClass(component.find('loginErrorMessage'),'slds-hide');
		} else {
			$A.util.addClass(component.find('loginErrorMessage'),'slds-hide');
		}
	},
	showLoginForm: function(component) {
		component.set('v.loginNeeded',true);
	},
	hideLoginForm: function(component) {
		component.set('v.loginNeeded',false);
	},
	disableLoginButton: function(component,on) {
		component.find('loginButton').set('v.disabled',on);
	},
	clearValidationErrors: function(component) {
		this.showLoginError(component,false)
		component.find('username').set('v.errors',null);
		component.find('password').set('v.errors',null);
	},
	validateUsername: function(component,showError) {
		var username = component.find('username');
		var isValid = false;
		this.showLoginError(component,false);
		if($A.util.isEmpty(username.get('v.value'))) {
			if(showError) {
				username.set('v.errors',[{message:'Cannot be blank!'}]);
			}
		} else {
			isValid = true;
			if(showError) {
				username.set('v.errors',null);
			}
		}
		return isValid;
	},
	validatePassword: function(component,showError) {
		var password = component.find('password');
		var isValid = false;
		this.showLoginError(component,false);
		if($A.util.isEmpty(password.get('v.value'))) {
			if(showError) {
				password.set('v.errors',[{message:'Cannot be blank!'}]);
			}
		} else {
			isValid = true;
			if(showError) {
				password.set('v.errors',null);
			}
		}
		return isValid;
	},
	clearLoginForm: function(component) {
		component.find('username').set('v.value','');
		component.find('password').set('v.value','');
	},
	tryLogin: function(component) {
		var validForm = false;
		var fldUsername = component.find('username');
		var fldPassword = component.find('password');
		var username = fldUsername.get('v.value');
		var password = fldPassword.get('v.value');
        if(!$A.util.isEmpty(username) && !$A.util.isEmpty(password)) {

			var action = component.get('c.getUserSession');
			action.setParams({ username : username, password : password});
			action.setCallback(this,function(response) {
				var state = response.getState();
				if(component.isValid() && state === 'SUCCESS') {
					var ret = response.getReturnValue();

					if(ret.BNSKSession.Id === -1) {
						this.showLoginError(component,true);
						this.showLoginForm(component);
					} else {
						this.clearLoginForm(component);
						if(component.get('v.autoHide')) {
							this.hideLoginForm(component);
						} else {
							this.showLoginForm(component);
						}
						this.fireSessionEvent(component,ret);
					}
				}else{
					this.throwAppError(component,response.getError(),'There was an error.',component.get('v.componentName'));
				}
			});
			$A.enqueueAction(action);
		}
	},
	setDebugInfo: function(component,session) {
		if(component.get('v.showDebug')) {
			component.set('v.debugUId',session.BNSKSession.UId);
			component.set('v.debugSky',session.BNSKSession.Key);
			component.set('v.debugSid',session.BNSKSession.Id);
		}
	},
	fireSessionEvent: function(component,session) {
		this.setDebugInfo(component,session);
		var appEvent = $A.get("e.BRNSHRK:sessionEvent");
		if(typeof appEvent !== 'undefined') {
			appEvent.setParams({
				'eventType': 'SESSION',
				'session': session
			});
			appEvent.fire();
		}

	}
})