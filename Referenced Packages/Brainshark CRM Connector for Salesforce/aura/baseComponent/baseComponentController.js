({

	handleGlobalMessage : function(cmp, event, helper) {
		var msgCode = event.getParam('Code');
		var msgTarget = event.getParam('Target');
		var msgText = event.getParam('Message');
		var msgType = event.getParam('Type');
		var msgLogInfo = event.getParam('LogInfo');
		var msgTimeout = event.getParam('Timeout');
		var cmpName = cmp.get('v.componentName');
		var msgArea = cmp.find('bnsk-msg-container');
		if(msgCode !== '' && (msgTarget === '' || msgTarget === cmpName)) {
			//See if the message is for this target and show it if it is
			cmp.set('v.appMessage',msgText);
			cmp.set('v.appMessageType',msgType.toLowerCase());
			cmp.set('v.appMessageDetails',msgLogInfo);
			switch(msgType) {
				case 'ERROR':
					$A.log(msgLogInfo);
					break;
				default:
			}
			cmp.set('v.showAppMessage',true);
		} else {
			cmp.set('v.appMessage','');
			cmp.set('v.showAppMessage',false);
		}
	},
	handleSystemError: function(cmp, event, helper) {
		$A.log(cmp);
        $A.log(event);
	}
})