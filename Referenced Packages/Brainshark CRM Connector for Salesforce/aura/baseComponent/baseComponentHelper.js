({
	throwAppMessage: function(component,type,code,message,target,loginfo,timeout) {
		var appEvent = $A.get('e.BRNSHRK:messageEvent');
		appEvent.setParams({
		    'Code' : code,
		 	'Message' : message,
			'Type' : type,
			'Target' : target,
			'LogInfo' : typeof(loginfo) !== 'undefined' ? loginfo : '',
			'Timeout' : 0,//Future use
		});
		appEvent.fire();
	},
	throwAppError: function(component,errors,message,target) {

		var errMessage = '';
		for (var errI = 0; errI < errors.length; errI++) {
			errMessage += (errMessage === '' ? '' : '</br>');
			errMessage += errors[errI].message;
		}
		this.throwAppMessage(component,'ERROR','ERROR','There was an error.',component.get('v.componentName'),errMessage,0);

	},
	requestSession: function(component) {
		var appEvent = $A.get("e.BRNSHRK:sessionEvent");
		if(typeof appEvent !== 'undefined') {
			var session = {
				'BNSKSession': {
					'Id':-1,
					'Key':'',
					'UId':-1
				}
			};
			appEvent.setParams({
				'session' : session,
				'eventType': 'REQUEST'
			});
			appEvent.fire();
		}
	},
	getAppSettings : function(component) {
		var action = component.get('c.getAppSettings');
        action.setCallback(this,function(response) {
            var state = response.getState();
            if(component.isValid() && state === 'SUCCESS') {
                var ret = response.getReturnValue();
				var settings = JSON.parse(ret);
				component.set('v.appHost',settings.appHost);
				this.setUrl(component);
            }
        });
        $A.enqueueAction(action);
	},	
	setCurrentSession: function(cmp,session) {
		cmp.set('v.session',session);
	},
	setSessionParams: function(component,session) {
		var params = 'sid=' + session.BNSKSession.Id + '&sky=' + session.BNSKSession.Key + '&uid=' + session.BNSKSession.UId;
		component.set('v.sessionParams',params);
	},
	getBaseUrlParameters: function(component,session) {
		var SFDCUserID = $A.util.isUndefined(session.SFDCUserID) ? '&sfdc.userId=none' : '&sfdc.userId=' + session.SFDCUserID;
		var SFDCUserOrgID = $A.util.isUndefined(session.SFDCUserOrgID) ? '&sfdc.orgId=none' : '&sfdc.orgId=' + session.SFDCUserOrgID;
		var recId = $A.util.isEmpty(component.get('v.recordId')) ? '&sfdc.recordId=none' : '&sfdc.recordId=' + component.get('v.recordId');
		var objName = $A.util.isEmpty(component.get('v.sObjectName')) ? '&sfdc.sObjectName=none' : '&sfdc.sObjectName=' + component.get('v.sObjectName');

		return recId + objName + SFDCUserID + SFDCUserOrgID;
	}
})