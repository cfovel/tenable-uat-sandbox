({
	doInit : function(component, event, helper) {
		helper.getAppSettings(component);
		if(component.get('v.requireSession')) {
			helper.requestSession(component);
		} else {
			helper.setUrl(component);
		}
	},
	handlePresClick : function(component,event,helper) {
		var urlToOpen = component.get('v.presentationUrl') + helper.getParameters(component);
	    var urlEvent = $A.get("e.force:navigateToURL");
		//If we can use SF to open nicely then lets do it else just use window.open
		if(typeof urlEvent !== 'undefined') {
			urlEvent.setParams({
		      "url": urlToOpen
		    });
		    urlEvent.fire();
		} else {
			window.open(urlToOpen);
		}
	},
	handleGotSessionEvent : function(component, event, helper) {
		var session = event.getParams();
		if(!$A.util.isEmpty(session.session.BNSKSession.Key)) {
			helper.setCurrentSession(component,session.session);
			helper.setSessionParams(component,session.session);
			helper.setUrl(component,session.session);
			component.set('v.hasSession',true);
		}

	}
})