({
    setUrl : function(component) {
		var params = this.getParameters(component);
		component.set('v.addParam',params);
		component.set('v.contentUrl',component.get('v.presentationUrl'));
    },
	getParameters : function(component) {
		var params = '';
		var baseParams = '';
		var trackingCode = '';
		if(component.get('v.requireSession')) {
			var sessionParams = component.get('v.sessionParams');
			var appHost = component.get('v.appHost');
			var session = component.get('v.session');
		 	if(!$A.util.isEmpty(session) && !$A.util.isEmpty(appHost) && !$A.util.isEmpty(sessionParams)) {
				baseParams = this.getBaseUrlParameters(component,session);
			}
		}

		trackingCode = !$A.util.isEmpty(component.get('v.trackingCode')) ? '&tx=' + component.get('v.trackingCode') : '';
		params = (baseParams + trackingCode + '&sfdc.view=lc');
		params = component.get('v.presentationUrl').indexOf('?') === -1 && params.startsWith('&') ? '?' + params.substr(1) : params;
		return params;

	}

})