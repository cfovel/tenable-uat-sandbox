({
    contentUrls: {
		profile: {url:'/brainshark/brainshark.net/sfdc/profile.aspx',addParam:''}
    },
    setUrl : function(component) {
		var session = component.get('v.session');
        var appHost = component.get('v.appHost');
		var sessionParams = component.get('v.sessionParams');
		if(!$A.util.isEmpty(session) && !$A.util.isEmpty(appHost) && !$A.util.isEmpty(sessionParams)) {
			//Lets put some info on the url
			var params = '';
			//If we are on the profile page see if we are looking at our own or someone else. If we are not on the
			//a page with the User object then always show our private profile
			var profileType = (component.get('v.sObjectName') === 'User' && (component.get('v.recordId') !== session.SFDCUserID)) ? '&profile=public' : '&profile=private';;

			params += profileType;
			var baseParams = this.getBaseUrlParameters(component,session);
			//add the params together and add the final sfdc.view to tell us we are in a LC
			var params = params + baseParams + '&sfdc.view=lc';

			component.set('v.addParam',params);
        	component.set('v.contentUrl',appHost + '/brainshark/brainshark.net/sfdc/profile.aspx');
			//component.set('v.hasSession',true);
		}


    }

})