<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>The_Permissioner</defaultLandingTab>
    <description>The Permissioner us an application used to mass assign Permission Sets to Users.</description>
    <formFactors>Large</formFactors>
    <label>The Permissioner</label>
    <logo>The_Permissioner/The_Permissioner_logo.png</logo>
    <tab>The_Permissioner</tab>
    <tab>Tenable_U_Sales_SE</tab>
</CustomApplication>
