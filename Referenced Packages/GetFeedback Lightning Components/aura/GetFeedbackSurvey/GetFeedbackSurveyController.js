({
    doInit: function(component, event, helper) {
        var action = component.get("c.getCurrentUser");
        action.setCallback(this, function(a) {
            var user = a.getReturnValue();
            if (user) {
              component.set("v.currentContactId", user.ContactId);
              component.set("v.currentUserId", user.Id);
              component.set("v.currentUserName", user.Name);
            }
            component.set("v.isLoaded", true);
        });
        $A.enqueueAction(action);
        
        component.set("v.currentPage", window.location.pathname.split('/').pop());
    }
})