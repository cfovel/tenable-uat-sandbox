<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <content>trainingLocContact</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Custom Object to store all the potential contacts to work with at a Training Location/Venue.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>true</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Address__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Address of Contact</inlineHelpText>
        <label>Address</label>
        <length>75</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Alternate_Email__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Alternative email contact information</inlineHelpText>
        <label>Alternate Email</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Email</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>City__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>City of Venue/Location</inlineHelpText>
        <label>City</label>
        <length>50</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Contact_Role__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Choose the role that this contact is providing for this training location</inlineHelpText>
        <label>Contact Role</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Desktop Provider</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Enrollment Contact</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Facility Administrator</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Material Contact</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Reservation Coordinator</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Technical Contact</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Country__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Country of Venue/Location</inlineHelpText>
        <label>Country</label>
        <length>2</length>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Email__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Primary Email contact information</inlineHelpText>
        <label>Email</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Email</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Fax__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Main Fax Number</inlineHelpText>
        <label>Fax</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Phone</type>
    </fields>
    <fields>
        <fullName>First_Name__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Instructor&apos;s First Name</inlineHelpText>
        <label>First Name</label>
        <length>25</length>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Last_Name__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Last name of instructor</inlineHelpText>
        <label>Last Name</label>
        <length>25</length>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Mobile_Phone__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The Mobile/Cell Telephone Number of the Instructor.</inlineHelpText>
        <label>Mobile Phone</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Phone</type>
    </fields>
    <fields>
        <fullName>Phone__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Main Telephone Number</inlineHelpText>
        <label>Phone</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Phone</type>
    </fields>
    <fields>
        <fullName>Postal_Code__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Postal/Zip Code of Venue/Location</inlineHelpText>
        <label>Zip/Postal Code</label>
        <length>20</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>State__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>State of Venue/Location</inlineHelpText>
        <label>State/Province</label>
        <length>25</length>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Title__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The Job title of the Instructor.</inlineHelpText>
        <label>Title</label>
        <length>50</length>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Training_Location__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Link to the Location for this training contact</inlineHelpText>
        <label>Training Location</label>
        <referenceTo>Training_Location__c</referenceTo>
        <relationshipName>Training_Location_Contacts</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <label>Location Contact</label>
    <nameField>
        <displayFormat>TLC-{00000000}</displayFormat>
        <label>Training: Location Contacts Name</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Location Contacts</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
