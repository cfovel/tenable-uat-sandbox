trigger overrideAssignmentTrigger on OverrideAssignment__c (before insert, before update, before delete, after insert, after update) 
{
    new Triggers()
    .bind(Triggers.Evt.beforeinsert, new OverrideAssignmentTriggerHandler.OverrideAssignmentBeforeInsertHandler())
    .bind(Triggers.Evt.beforeupdate, new OverrideAssignmentTriggerHandler.OverrideAssignmentBeforeUpdateHandler())
    .bind(Triggers.Evt.beforedelete, new OverrideAssignmentTriggerHandler.OverrideAssignmentBeforeDeleteHandler())
    .bind(Triggers.Evt.afterinsert, new OverrideAssignmentTriggerHandler.OverrideAssignmentAfterInsertHandler())
    .bind(Triggers.Evt.afterupdate, new OverrideAssignmentTriggerHandler.OverrideAssignmentAfterUpdateHandler())
    .manage();
}