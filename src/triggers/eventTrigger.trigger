trigger eventTrigger on Event (after delete, after insert, after undelete, after update) {
/*******************************************************************************
*
* Event Trigger
* Version 1.0a
*
*******************************************************************************/
	new Triggers()
		.bind(Triggers.Evt.afterinsert, new eventTriggerHandler.EventAfterInsertHandler())
		.bind(Triggers.Evt.afterupdate, new eventTriggerHandler.EventAfterUpdateHandler())
		.bind(Triggers.Evt.afterdelete, new eventTriggerHandler.EventAfterDeleteHandler())
		.bind(Triggers.Evt.afterundelete, new eventTriggerHandler.EventAfterUndeleteHandler())
		.manage();

}