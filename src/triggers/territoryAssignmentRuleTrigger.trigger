trigger territoryAssignmentRuleTrigger on TerritoryAssignmentRule__c (before Update, before Delete) 
{
    new Triggers()
    .bind(Triggers.Evt.beforeupdate, new TerritoryAssignmentRuleTriggerHandler.TerritoryAssignmentRuleBeforeUpdateHandler())
    .bind(Triggers.Evt.beforedelete, new TerritoryAssignmentRuleTriggerHandler.TerritoryAssignmentRuleBeforeDeleteHandler())
    .manage();
}