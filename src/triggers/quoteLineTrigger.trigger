trigger quoteLineTrigger on QuoteLine__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
/*******************************************************************************
*
* QuoteLine Trigger
* Version 1.0a
* Copyright 2006-2014 (c) by CloudLogistix.com
* All Rights Reserved
* Your are allowed to make modifications only for yourself, not for distribution
* without permission from CloudLogistix.
*
*******************************************************************************/
	new Triggers()
		.bind(Triggers.Evt.beforeinsert, new quoteLineTriggerHandler.QuoteLineBeforeInsertHandler())
		.bind(Triggers.Evt.beforeupdate, new quoteLineTriggerHandler.QuoteLineBeforeUpdateHandler())
		.bind(Triggers.Evt.afterinsert, new quoteLineTriggerHandler.QuoteLineAfterInsertHandler())
		.bind(Triggers.Evt.afterupdate, new quoteLineTriggerHandler.QuoteLineAfterUpdateHandler())
		.bind(Triggers.Evt.afterdelete, new quoteLineTriggerHandler.QuoteLineAfterDeleteHandler())
		.bind(Triggers.Evt.afterundelete, new quoteLineTriggerHandler.QuoteLineAfterUndeleteHandler())
		.manage();
}