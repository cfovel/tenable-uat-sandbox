trigger campaignExpensesTrigger on Campaign_Expenses__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
/*******************************************************************************
*
* Campaign Expenses Trigger
* Version 1.0a
* Copyright 2006-2014 (c) by CloudLogistix.com
* All Rights Reserved
* Your are allowed to make modifications only for yourself, not for distribution
* without permission from CloudLogistix.
*
*******************************************************************************/
	new Triggers()
		.bind(Triggers.Evt.afterinsert, new campaignExpensesTriggerHandler.CampaignExpensesAfterInsertHandler())
		.bind(Triggers.Evt.afterupdate, new campaignExpensesTriggerHandler.CampaignExpensesAfterUpdateHandler())
		.bind(Triggers.Evt.afterdelete, new campaignExpensesTriggerHandler.CampaignExpensesAfterDeleteHandler())
		.bind(Triggers.Evt.afterundelete, new campaignExpensesTriggerHandler.CampaignExpensesAfterUndeleteHandler())
		.manage();
}