trigger areaTrigger on Area__c (before insert, before update, after insert, after update)
{
    new Triggers()
    .bind(Triggers.Evt.beforeinsert, new AreaTriggerHandler.AreaBeforeInsertHandler())
    .bind(Triggers.Evt.beforeupdate, new AreaTriggerHandler.AreaBeforeUpdateHandler())
    .bind(Triggers.Evt.afterinsert, new AreaTriggerHandler.AreaAfterInsertHandler())
    .bind(Triggers.Evt.afterupdate, new AreaTriggerHandler.AreaAfterUpdateHandler())
    .manage();
}