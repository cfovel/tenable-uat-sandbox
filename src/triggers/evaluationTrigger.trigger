trigger evaluationTrigger on Evaluation__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {

	new Triggers()
		.bind(Triggers.Evt.afterinsert, new evaluationTriggerHandler.evaluationAfterInsertHandler())
		.bind(Triggers.Evt.afterupdate, new evaluationTriggerHandler.evaluationAfterUpdateHandler())
		//Commented the below code on 03/21/17 by BM since we are not using those methods yet in the handler.  Once we start using it
		//we can uncomment it
		//.bind(Triggers.Evt.beforeinsert, new evaluationTriggerHandler.evaluationBeforeInsertHandler())
		//.bind(Triggers.Evt.beforeupdate, new evaluationTriggerHandler.evaluationBeforeUpdateHandler())
		//.bind(Triggers.Evt.afterdelete, new evaluationTriggerHandler.evaluationAfterDeleteHandler())
		.manage();
    
}