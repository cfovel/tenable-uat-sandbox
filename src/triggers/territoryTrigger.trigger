trigger territoryTrigger on Territory__c (before insert, before update, after insert, after update) 
{
    new Triggers()
    .bind(Triggers.Evt.beforeinsert, new TerritoryTriggerHandler.TerritoryBeforeInsertHandler())
    .bind(Triggers.Evt.beforeupdate, new TerritoryTriggerHandler.TerritoryBeforeUpdateHandler())
    .bind(Triggers.Evt.afterinsert, new TerritoryTriggerHandler.TerritoryAfterInsertHandler())
    .bind(Triggers.Evt.afterupdate, new TerritoryTriggerHandler.TerritoryAfterUpdateHandler())
    .manage();
}