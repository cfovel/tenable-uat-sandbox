trigger userTrigger on User (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
/*******************************************************************************
*
* User Trigger
* Version 1.0a
* Copyright 2006-2014 (c) by CloudLogistix.com
* All Rights Reserved
* Your are allowed to make modifications only for yourself, not for distribution
* without permission from CloudLogistix.
*
*******************************************************************************/
    new Triggers()
        .bind(Triggers.Evt.afterinsert, new userTriggerHandler.UserAfterInsertHandler())
        .bind(Triggers.Evt.afterupdate, new userTriggerHandler.UserAfterUpdateHandler())
        .manage();

}