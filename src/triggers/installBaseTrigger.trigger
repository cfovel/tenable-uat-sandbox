trigger installBaseTrigger on Install_Base__c (before insert) 
{
    new Triggers()
        .bind(Triggers.Evt.beforeinsert, new installBaseTriggerHandler.InstallBaseBeforeInsertHandler())
        .manage();
}