trigger caseTrigger on Case (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
/*******************************************************************************
*
* Case Trigger
* Version 1.0a
* Copyright 2006-2015 (c) by CloudLogistix.com
* All Rights Reserved
* Your are allowed to make modifications only for yourself, not for distribution
* without permission from CloudLogistix.
*
*******************************************************************************/
	new Triggers()
		.bind(Triggers.Evt.beforeinsert, new caseTriggerHandler.CaseBeforeInsertHandler())
		.bind(Triggers.Evt.beforeupdate, new caseTriggerHandler.CaseBeforeUpdateHandler())
		.bind(Triggers.Evt.afterinsert, new caseTriggerHandler.CaseAfterInsertHandler())
		.bind(Triggers.Evt.afterupdate, new caseTriggerHandler.CaseAfterUpdateHandler())
		.bind(Triggers.Evt.afterdelete, new caseTriggerHandler.CaseAfterDeleteHandler())
		.bind(Triggers.Evt.afterundelete, new caseTriggerHandler.CaseAfterUndeleteHandler())
		.manage();

}