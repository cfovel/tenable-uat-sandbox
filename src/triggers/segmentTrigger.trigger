trigger segmentTrigger on Segment__c (before insert, before update, after update) 
{
    new Triggers()
    .bind(Triggers.Evt.beforeinsert, new SegmentTriggerHandler.SegmentBeforeInsertHandler())
    .bind(Triggers.Evt.beforeupdate, new SegmentTriggerHandler.SegmentBeforeUpdateHandler())
    .bind(Triggers.Evt.afterupdate, new SegmentTriggerHandler.SegmentAfterUpdateHandler())
    .manage();
}