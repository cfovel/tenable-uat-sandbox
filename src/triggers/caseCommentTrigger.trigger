trigger caseCommentTrigger on CaseComment (after insert, after update) {

    new Triggers()
        .bind(Triggers.Evt.afterinsert, new caseCommentTriggerHandler.caseCommentAfterInsertHandler())
        .bind(Triggers.Evt.afterupdate, new caseCommentTriggerHandler.CaseCommentAfterUpdateHandler())
        .manage();

}