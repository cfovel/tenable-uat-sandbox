trigger campaignMemberTrigger on CampaignMember (after delete, after insert, after update, before delete, before insert, before update) {
/*******************************************************************************
*
* Campaign Member Trigger
* Version 1.0a
* Copyright 2006-2013 (c) by CloudLogistix.com
* All Rights Reserved
* Your are allowed to make modifications only for yourself, not for distribution
* without permission from CloudLogistix.
*
*******************************************************************************/
	new Triggers()
		.bind(Triggers.Evt.beforeinsert, new campaignMemberTriggerHandler.CampaignMemberBeforeInsertHandler())
		.bind(Triggers.Evt.beforeupdate, new campaignMemberTriggerHandler.CampaignMemberBeforeUpdateHandler())
		.bind(Triggers.Evt.afterinsert, new campaignMemberTriggerHandler.CampaignMemberAfterInsertHandler())
		.bind(Triggers.Evt.afterupdate, new campaignMemberTriggerHandler.CampaignMemberAfterUpdateHandler())
		.bind(Triggers.Evt.afterdelete, new campaignMemberTriggerHandler.CampaignMemberAfterDeleteHandler())
		.manage();

}