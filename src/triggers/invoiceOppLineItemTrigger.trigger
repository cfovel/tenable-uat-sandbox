trigger invoiceOppLineItemTrigger on InvoiceOppLineItem__c (after insert, after update, after delete, after undelete) {
/*******************************************************************************
*
* Invoice Opp Line Item Trigger
* Version 1.0
*
*******************************************************************************/
	new Triggers()
		.bind(Triggers.Evt.afterinsert, new invoiceOppLineItemTriggerHandler.InvoiceOppLineAfterInsertHandler())
		.bind(Triggers.Evt.afterupdate, new invoiceOppLineItemTriggerHandler.InvoiceOppLineAfterUpdateHandler())
        .bind(Triggers.Evt.afterdelete, new invoiceOppLineItemTriggerHandler.InvoiceOppLineAfterDeleteHandler())
        .bind(Triggers.Evt.afterundelete, new invoiceOppLineItemTriggerHandler.InvoiceOppLineAfterUndeleteHandler())
		.manage();
}