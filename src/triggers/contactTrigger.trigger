trigger contactTrigger on Contact (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
/*******************************************************************************
*
* Contact Trigger
* Version 1.0a
* Copyright 2006-2013 (c) by CloudLogistix.com
* All Rights Reserved
* Your are allowed to make modifications only for yourself, not for distribution
* without permission from CloudLogistix.
*
*******************************************************************************/
	new Triggers()
		.bind(Triggers.Evt.beforeinsert, new contactTriggerHandler.ContactBeforeInsertHandler())
		.bind(Triggers.Evt.beforeupdate, new contactTriggerHandler.ContactBeforeUpdateHandler())
		.bind(Triggers.Evt.afterinsert, new contactTriggerHandler.ContactAfterInsertHandler())
		.bind(Triggers.Evt.afterupdate, new contactTriggerHandler.ContactAfterUpdateHandler())
		.manage();
}