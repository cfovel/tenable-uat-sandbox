trigger oppPartnerTrigger on OppPartner__c (before insert, after insert, after undelete, after update) {
/*******************************************************************************
*
* OppPartner Trigger
*
*******************************************************************************/
	new Triggers()
		.bind(Triggers.Evt.beforeinsert, new oppPartnerTriggerHandler.OppPartnerBeforeInsertHandler())
		.bind(Triggers.Evt.afterinsert, new oppPartnerTriggerHandler.OppPartnerAfterInsertHandler())
		.bind(Triggers.Evt.afterupdate, new oppPartnerTriggerHandler.OppPartnerAfterUpdateHandler())
		.bind(Triggers.Evt.afterundelete, new oppPartnerTriggerHandler.OppPartnerAfterUndeleteHandler())
		.manage();
}