trigger territoryAssignmentTrigger on TerritoryAssignment__c (before Insert, before Update) 
{
    new Triggers()
    .bind(Triggers.Evt.beforeinsert, new TerritoryAssignmentTriggerHandler.TerritoryAssignmentBeforeInsertHandler())
    .bind(Triggers.Evt.beforeupdate, new TerritoryAssignmentTriggerHandler.TerritoryAssignmentBeforeUpdateHandler())
    .manage();
}