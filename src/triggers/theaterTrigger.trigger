trigger theaterTrigger on Theater__c (before insert, before update, after insert, after update) 
{
    new Triggers()
    .bind(Triggers.Evt.beforeinsert, new TheaterTriggerHandler.TheaterBeforeInsertHandler())
    .bind(Triggers.Evt.beforeupdate, new TheaterTriggerHandler.TheaterBeforeUpdateHandler())
    .bind(Triggers.Evt.afterinsert, new TheaterTriggerHandler.TheaterAfterInsertHandler())
    .bind(Triggers.Evt.afterupdate, new TheaterTriggerHandler.TheaterAfterUpdateHandler())
    .manage();
}