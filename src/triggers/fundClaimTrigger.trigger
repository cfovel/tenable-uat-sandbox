trigger fundClaimTrigger on Fund_Claim__c (before insert, before update) {
	
	new Triggers()
		.bind(Triggers.Evt.beforeinsert, new fundClaimTriggerHandler.FundClaimBeforeInsertHandler())
		.bind(Triggers.Evt.beforeupdate, new fundClaimTriggerHandler.FundClaimBeforeUpdateHandler())
		.manage();

}