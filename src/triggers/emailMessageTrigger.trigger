trigger emailMessageTrigger on EmailMessage (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
/*******************************************************************************
*
* Case Email Trigger
* Version 1.0a
* Copyright 2006-2013 (c) by CloudLogistix.com
* All Rights Reserved
* Your are allowed to make modifications only for yourself, not for distribution
* without permission from CloudLogistix.
*
*******************************************************************************/
	new Triggers()
		.bind(Triggers.Evt.afterinsert, new emailMessageTriggerHandler.emailMessageAfterInsertHandler())
		.manage();

}