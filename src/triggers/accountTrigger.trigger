trigger accountTrigger on Account (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
/*******************************************************************************
*
* Account Trigger
* Version 1.0a
* Copyright 2006-2013 (c) by CloudLogistix.com
* All Rights Reserved
* Your are allowed to make modifications only for yourself, not for distribution
* without permission from CloudLogistix.
*
*******************************************************************************/
	new Triggers()
		.bind(Triggers.Evt.beforeinsert, new accountTriggerHandler.AccountBeforeInsertHandler())
		.bind(Triggers.Evt.beforeupdate, new accountTriggerHandler.AccountBeforeUpdateHandler())
		.bind(Triggers.Evt.afterupdate, new accountTriggerHandler.AccountAfterUpdateHandler())
		.manage();
}