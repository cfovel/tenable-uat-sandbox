trigger oppLineTrigger on OpportunityLineItem (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
/*******************************************************************************
*
* Opportunity Line Trigger
* Version 1.0a
* Copyright 2006-2016 (c) by CloudLogistix.com
* All Rights Reserved
* Your are allowed to make modifications only for yourself, not for distribution
* without permission from CloudLogistix.
*
*******************************************************************************/
	new Triggers()
		.bind(Triggers.Evt.beforeinsert, new oppLinesTriggerHandler.OppLinesBeforeInsertHandler())
		.bind(Triggers.Evt.beforeupdate, new oppLinesTriggerHandler.OppLinesBeforeUpdateHandler())
		.bind(Triggers.Evt.afterinsert, new oppLinesTriggerHandler.OppLinesAfterInsertHandler())
//		.bind(Triggers.Evt.afterupdate, new oppLinesTriggerHandler.OppLinesAfterUpdateHandler())
		.manage();
}