/*************************************************************************************
Class: OpportunitySplitTrigger
Author: Bharathi.M
Details: This trigger calls the OpportunitySplitTriggerHandler class to update the territory 
         data from the Territory object. 

History : Created 06/07/2017 (0% code coverage)
***************************************************************************************/
trigger OpportunitySplitTrigger on OpportunitySplit (before Insert, before Update) 
{
    new Triggers()
    .bind(Triggers.Evt.beforeinsert, new OpportunitySplitTriggerHandler.OpportunitySplitBeforeInsertHandler())
    .bind(Triggers.Evt.beforeupdate, new OpportunitySplitTriggerHandler.OpportunitySplitBeforeUpdateHandler())
    .manage();
}