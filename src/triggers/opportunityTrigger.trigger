trigger opportunityTrigger on Opportunity (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
/*******************************************************************************
*
* Opportunity Trigger
* Version 1.0a
* Copyright 2006-2013 (c) by CloudLogistix.com
* All Rights Reserved
* Your are allowed to make modifications only for yourself, not for distribution
* without permission from CloudLogistix.
*
*******************************************************************************/
	new Triggers()
		.bind(Triggers.Evt.beforeinsert, new opportunityTriggerHandler.OpportunityBeforeInsertHandler())
		.bind(Triggers.Evt.beforeupdate, new opportunityTriggerHandler.OpportunityBeforeUpdateHandler())
		.bind(Triggers.Evt.afterinsert, new opportunityTriggerHandler.OpportunityAfterInsertHandler())
		.bind(Triggers.Evt.afterupdate, new opportunityTriggerHandler.OpportunityafterUpdateHandler())
		.manage();

}