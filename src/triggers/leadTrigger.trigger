trigger leadTrigger on Lead (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
/*******************************************************************************
*
* Lead Trigger
* Version 1.0a
* Copyright 2006-2013 (c) by CloudLogistix.com
* All Rights Reserved
* Your are allowed to make modifications only for yourself, not for distribution
* without permission from CloudLogistix.
*
*******************************************************************************/
	new Triggers()
		.bind(Triggers.Evt.beforeinsert, new leadTriggerHandler.LeadBeforeInsertHandler())
		.bind(Triggers.Evt.beforeupdate, new leadTriggerHandler.LeadBeforeUpdateHandler())
		.bind(Triggers.Evt.afterinsert, new leadTriggerHandler.LeadAfterInsertHandler())
		.bind(Triggers.Evt.afterupdate, new leadTriggerHandler.LeadAfterUpdateHandler())
		.manage();
}