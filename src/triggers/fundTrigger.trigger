trigger fundTrigger on MDF_Transaction__c (before insert, before update, after update, after delete, after undelete) {
    
    new Triggers()
        .bind(Triggers.Evt.beforeinsert, new fundTriggerHandler.FundBeforeInsertHandler())
        .bind(Triggers.Evt.beforeupdate, new fundTriggerHandler.FundBeforeUpdateHandler())
        .bind(Triggers.Evt.afterinsert, new fundTriggerHandler.FundAfterInsertHandler())
        .bind(Triggers.Evt.afterupdate, new fundTriggerHandler.FundAfterUpdateHandler())
        .bind(Triggers.Evt.afterdelete, new fundTriggerHandler.FundAfterDeleteHandler())
        .bind(Triggers.Evt.afterundelete, new fundTriggerHandler.FundAfterUndeleteHandler())
        .manage();

}