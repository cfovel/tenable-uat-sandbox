trigger contractTrigger on Contract (before insert, before update, after update) {
    
    new Triggers()
        .bind(Triggers.Evt.beforeinsert, new contractTriggerHandler.ContractBeforeInsertHandler())
        .bind(Triggers.Evt.beforeupdate, new contractTriggerHandler.ContractBeforeUpdateHandler())
        .bind(Triggers.Evt.afterupdate, new contractTriggerHandler.ContractAfterUpdateHandler())
        .manage();
}