trigger regionTrigger on Region__c (before insert, before update, after insert, after update)
{
    new Triggers()
    .bind(Triggers.Evt.beforeinsert, new RegionTriggerHandler.RegionBeforeInsertHandler())
    .bind(Triggers.Evt.beforeupdate, new RegionTriggerHandler.RegionBeforeUpdateHandler())
    .bind(Triggers.Evt.afterinsert, new RegionTriggerHandler.RegionAfterInsertHandler())
    .bind(Triggers.Evt.afterupdate, new RegionTriggerHandler.RegionAfterUpdateHandler())
    .manage();
}