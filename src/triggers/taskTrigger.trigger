trigger taskTrigger on Task (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
/*******************************************************************************
*
* Account Trigger
* Version 1.0a
* Copyright 2006-2013 (c) by CloudLogistix.com
* All Rights Reserved
* Your are allowed to make modifications only for yourself, not for distribution
* without permission from CloudLogistix.
*
*******************************************************************************/
	new Triggers()
		.bind(Triggers.Evt.beforeinsert, new tasksTriggerHandler.TaskBeforeInsertHandler())
		.bind(Triggers.Evt.beforedelete, new tasksTriggerHandler.TaskBeforeDeleteHandler())
		.bind(Triggers.Evt.afterinsert, new tasksTriggerHandler.TaskAfterInsertHandler())
		.bind(Triggers.Evt.afterupdate, new tasksTriggerHandler.TaskAfterUpdateHandler())
		.bind(Triggers.Evt.afterdelete, new tasksTriggerHandler.TaskAfterDeleteHandler())
		.bind(Triggers.Evt.afterundelete, new tasksTriggerHandler.TaskAfterUndeleteHandler())
		.manage();

}