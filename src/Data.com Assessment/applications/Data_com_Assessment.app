<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <label>Data.com Assessment</label>
    <logo>Clean_Assessment/Clean_Assessment_Tab.jpg</logo>
    <tab>Data_com_Assessment</tab>
    <tab>Evaluation__c</tab>
    <tab>Territory_Assignment_Temp__c</tab>
    <tab>Contact_Evaluation__c</tab>
    <tab>Tenable_U_Sales_SE</tab>
    <tab>Docebo_Enrollments__c</tab>
    <tab>Sales_Workspace</tab>
    <tab>Manager_Workspace</tab>
</CustomApplication>
