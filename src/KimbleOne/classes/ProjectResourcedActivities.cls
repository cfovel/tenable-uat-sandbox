/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/v1.0/plannedactivities/*')
global class ProjectResourcedActivities {
    global ProjectResourcedActivities() {

    }
    @HttpGet
    global static List<KimbleOne.ProjectResourcedActivities.Project> doGet() {
        return null;
    }
    @HttpPut
    global static void doPost(List<KimbleOne.ProjectResourcedActivities.ResourceWithODate> Resources) {

    }
global class Assignment {
    global Assignment() {

    }
}
global class Project {
    global Project() {

    }
}
global class Resource {
    global Resource() {

    }
}
global class ResourceWithODate {
    global ResourceWithODate() {

    }
}
global class Task {
    global Task() {

    }
}
}
