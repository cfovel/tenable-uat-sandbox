/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/v1.0/ConfigurationCatalog/*')
global class ConfigurationCatalogRestService {
    global ConfigurationCatalogRestService() {

    }
    @HttpDelete
    global static void DeleteAll() {

    }
    @HttpGet
    global static void GetAll() {

    }
    @HttpPost
    global static void PostAll() {

    }
}
