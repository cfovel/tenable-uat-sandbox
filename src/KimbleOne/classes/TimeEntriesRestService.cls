/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/v1.0/TimeEntries/*')
global class TimeEntriesRestService {
    global TimeEntriesRestService() {

    }
    @HttpDelete
    global static void DeleteTimeEntry() {

    }
    @HttpPost
    global static KimbleOne.TimeAndExpenseRestService.TimeEntryIdentifierDto InsertEntry() {
        return null;
    }
    @HttpPut
    global static KimbleOne.TimeAndExpenseRestService.TimeEntryIdentifierDto UpdateTimeEntry() {
        return null;
    }
}
