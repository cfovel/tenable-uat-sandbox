/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/v1.0/TimeAndExpense/*')
global class TimeAndExpenseRestService {
    global TimeAndExpenseRestService() {

    }
    @HttpPost
    global static KimbleOne.TimeAndExpenseRestService.TimeAndExpenseDto GetAll() {
        return null;
    }
global class ActivityDto {
}
global class ActivityRateBandDto {
}
global class AssignmentDto {
}
global class ExchangeRateDto {
}
global class ExpenseCategoryDto {
}
global class ExpenseClaimDto {
}
global class ExpenseClaimIdentifierDto {
}
global class ExpenseItemDto {
}
global class ExpenseItemIdentifierDto {
}
global class ForecastTimeEntryDto {
}
global class PlanTaskAssignmentDto {
}
global class TaxCodeDto {
}
global class TaxRateDto {
}
global class TimeAndExpenseDto {
}
global class TimeCategoryDto {
}
global class TimeEntryDto {
}
global class TimeEntryIdentifierDto {
}
global class TrackingPeriodDto {
}
}
