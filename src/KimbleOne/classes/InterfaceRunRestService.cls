/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/v1.0/InterfaceRun/*')
global class InterfaceRunRestService {
    global InterfaceRunRestService() {

    }
    @HttpPost
    global static void doPost(String interfaceRunId) {

    }
    @HttpPut
    global static void doPut(String interfaceTypeId, String externalRequestId) {

    }
}
