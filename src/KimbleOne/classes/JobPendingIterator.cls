/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class JobPendingIterator implements System.Iterator<KimbleOne__Job__c> {
    global JobPendingIterator(String jobPriority) {

    }
    global Boolean hasNext() {
        return null;
    }
    global KimbleOne__Job__c next() {
        return null;
    }
}
