/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/v1.0/ConfigurationDataImport/*')
global class ConfigurationDataImportRestService {
    global ConfigurationDataImportRestService() {

    }
    @HttpDelete
    global static void DeleteAll() {

    }
    @HttpPost
    global static Id PostAll() {
        return null;
    }
}
