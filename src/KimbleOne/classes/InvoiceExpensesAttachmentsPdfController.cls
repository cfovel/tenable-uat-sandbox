/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class InvoiceExpensesAttachmentsPdfController {
    global String documentUrl {
        get;
    }
    global Map<Id,List<Attachment>> ResourcesAndAttachments {
        get;
    }
    global List<KimbleOne__Resource__c> ResourcesWithExpenseAttachments {
        get;
    }
    global InvoiceExpensesAttachmentsPdfController(ApexPages.StandardController stdController) {

    }
}
