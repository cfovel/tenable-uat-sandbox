<apex:page controller="KimbleOne.ReferenceDataEditController">

<apex:composition template="KimbleOne__SiteMaster">

<apex:define name="Menu"></apex:define>  
   
<apex:define name="Content">

	<style>
		div#ChangeOrderBehaviourRulePopup label,
		div#ReferenceDataPopup label,
		div#UsageTrackingModelPopup label
		{
		    white-space: nowrap;
		}
	</style>

	<apex:sectionHeader title="{!$Label.kimbleone__referencedataedittitle}" subtitle=""/>
    
	<apex:messages />

    <apex:form rendered="{!ReferenceDatapassed}">
		<apex:outputPanel id="FilterDomainPanel" >
	    	<apex:pageBlock id="FilterDomainBlock" title="{!$ObjectType.KimbleOne__ReferenceData__c.Fields.KimbleOne__Domain__c.Label}">
		        <apex:selectList id="DomainsList" value="{!domainName}" size="1" onchange="refreshTable();"> 
					<apex:selectOptions value="{!Domains}"/>
		        </apex:selectList>
	    	</apex:pageBlock>
	    	<apex:actionFunction action="{!refreshTable}" name="refreshTable" rerender="DataPanel" oncomplete=""/> 	
	    </apex:outputPanel> 	 	
 	</apex:form>
 	
    <apex:form rendered="{!ReferenceDatapassed}">
		<apex:outputPanel id="DataPanel" >
	    	<apex:pageBlock id="DataBlock" title="{!$ObjectType.KimbleOne__ReferenceData__c.Fields.Name.Label}">
				<apex:pageBlockTable value="{!refDatList}" var="ReferenceDataLine">
	       			<apex:column headerValue="{!$Label.kimbleone__actionlinksheader}">
						<apex:outputLink value="#" onclick="editReferenceData('{!JSINHTMLENCODE(ReferenceDataLine.id)}');initialiseModalBoxy('#ReferenceDataPopup', '{!$Label.kimbleone__edit} {!$ObjectType.KimbleOne__ReferenceData__c.label}');$('#ReferenceDataPopup')[0].style.visibility='visible';">{!$Label.kimbleone__edit}</apex:outputLink>
		  			</apex:column>				
					<apex:column value="{!ReferenceDataLine.Name}" headerValue="{!$ObjectType.KimbleOne__ReferenceData__c.Fields.Name.Label}"/>
					<apex:column value="{!ReferenceDataLine.KimbleOne__Description__c}" headerValue="{!$ObjectType.KimbleOne__ReferenceData__c.Fields.KimbleOne__Description__c.Label}"/>
				</apex:pageBlockTable>	        
	    	</apex:pageBlock>  	
			<apex:actionFunction action="{!editReferenceData}" name="editReferenceData" rerender="ReferenceDataPanel, DataPanel" oncomplete="">
	       		<apex:param name="rdId" value="" />
	       	</apex:actionFunction>				       	       	     		    	
	    </apex:outputPanel> 	 	
 	</apex:form>
 
     <apex:form rendered="{!ChangeOrderBehaviourRulepassed}">
		<apex:outputPanel id="COBRPanel" >
	    	<apex:pageBlock id="COBRBlock" title="{!$ObjectType.KimbleOne__ChangeOrderBehaviourRule__c.Fields.Name.Label}">
				<apex:pageBlockTable value="{!COBRList}" var="COBRLine">
	       			<apex:column headerValue="{!$Label.kimbleone__actionlinksheader}">
						<apex:outputLink value="#" onclick="editChangeOrderBehaviourRule('{!JSINHTMLENCODE(COBRLine.id)}');initialiseModalBoxy('#ChangeOrderBehaviourRulePopup', '{!$Label.kimbleone__edit} {!$ObjectType.KimbleOne__ChangeOrderBehaviourRule__c.label}');$('#ChangeOrderBehaviourRulePopup')[0].style.visibility='visible';">{!$Label.kimbleone__edit}</apex:outputLink>
		  			</apex:column>				
					<apex:column value="{!COBRLine.Name}" headerValue="{!$ObjectType.KimbleOne__ChangeOrderBehaviourRule__c.Fields.Name.Label}"/>
					<apex:column value="{!COBRLine.KimbleOne__DefaultForecastStatus__c}" headerValue="{!$ObjectType.KimbleOne__ChangeOrderBehaviourRule__c.Fields.KimbleOne__DefaultForecastStatus__c.Label}"/>
					<apex:column value="{!COBRLine.KimbleOne__ProposalAcceptanceType__c}" headerValue="{!$ObjectType.KimbleOne__ChangeOrderBehaviourRule__c.Fields.KimbleOne__ProposalAcceptanceType__c.Label}"/>
				</apex:pageBlockTable>	        
	    	</apex:pageBlock>
			<apex:actionFunction action="{!editChangeOrderBehaviourRule}" name="editChangeOrderBehaviourRule" rerender="ChangeOrderBehaviourRulePanel, COBRPanel" oncomplete="">
	       		<apex:param name="cobrId" value="" />
	       	</apex:actionFunction>	    	       	 
	    </apex:outputPanel> 	 	
 	</apex:form>	

     <apex:form rendered="{!UsageTrackingModelpassed}">
		<apex:outputPanel id="UTMPanel" >
	    	<apex:pageBlock id="UTMBlock" title="{!$ObjectType.KimbleOne__UsageTrackingModel__c.Fields.Name.Label}">
				<apex:pageBlockTable value="{!UTMList}" var="UTMLine">
	       			<apex:column headerValue="{!$Label.kimbleone__actionlinksheader}">
						<apex:outputLink value="#" onclick="editUsageTrackingModel('{!JSINHTMLENCODE(UTMLine.id)}');initialiseModalBoxy('#UsageTrackingModelPopup', '{!$Label.kimbleone__edit} {!$ObjectType.KimbleOne__UsageTrackingModel__c.label}');$('#UsageTrackingModelPopup')[0].style.visibility='visible';">{!$Label.kimbleone__edit}</apex:outputLink>
		  			</apex:column>				
					<apex:column value="{!UTMLine.Name}" headerValue="{!$ObjectType.KimbleOne__UsageTrackingModel__c.Fields.Name.Label}"/>
					<apex:column value="{!UTMLine.KimbleOne__ActualisedForecastApprovalRequired__c}" headerValue="{!$ObjectType.KimbleOne__UsageTrackingModel__c.Fields.KimbleOne__ActualisedForecastApprovalRequired__c.Label}"/>
					<apex:column value="{!UTMLine.KimbleOne__ActualiseForecastOnPeriodChange__c}" headerValue="{!$ObjectType.KimbleOne__UsageTrackingModel__c.Fields.KimbleOne__ActualiseForecastOnPeriodChange__c.Label}"/>
					<apex:column value="{!UTMLine.KimbleOne__ExcludeFromActualChecks__c}" headerValue="{!$ObjectType.KimbleOne__UsageTrackingModel__c.Fields.KimbleOne__ExcludeFromActualChecks__c.Label}"/>
					<apex:column value="{!UTMLine.KimbleOne__IsActualUntracked__c}" headerValue="{!$ObjectType.KimbleOne__UsageTrackingModel__c.Fields.KimbleOne__IsActualUntracked__c.Label}"/>
				</apex:pageBlockTable>	        
	    	</apex:pageBlock> 
			<apex:actionFunction action="{!editUsageTrackingModel}" name="editUsageTrackingModel" rerender="UsageTrackingModelPanel, UTMPanel" oncomplete="">
	       		<apex:param name="utmId" value="" />
	       	</apex:actionFunction>		    	      	 
	    </apex:outputPanel> 	 	
 	</apex:form>

	<div id="ReferenceDataPopup" style="display:none;">  
		<apex:outputPanel id="ReferenceDataPanel">
     	  <apex:form >
     	  
           		<apex:pageBlock >    
           		
           			<div id="ReferenceDataErrors">
           				<apex:pageMessages escape="false"/>
           				
           			</div>
           			
            		<apex:pageBlockButtons location="bottom">
            			
						<apex:commandButton action="{!saveReferenceData}" value="{!$Label.kimbleone__save}" oncomplete="hideModalBoxyWithErrorCheck('#ReferenceDataErrors');refreshTable();" reRender="DataPanel"/>
						<input class="btn" type="button" onclick="hideModalBoxy();" value="{!$Label.Cancel}"/>
					
					</apex:pageBlockButtons>
					
             		<apex:pageblockSection collapsible="false" showheader="false"  columns="1">
             			<apex:inputfield value="{!editRefData.Name}" required="true" />
               			<apex:inputfield value="{!editRefData.KimbleOne__Description__c}" required="false" style="width:400px;"/>
		            </apex:pageBlockSection>            
        		</apex:pageBlock>
         	</apex:form>
        </apex:outputPanel> 
	</div>
	
	<div id="ChangeOrderBehaviourRulePopup" style="display:none;">  
		<apex:outputPanel id="ChangeOrderBehaviourRulePanel">
     	  <apex:form >
     	  
           		<apex:pageBlock >    
           		
           			<div id="ChangeOrderBehaviourRuleErrors">
           				<apex:pageMessages escape="false"/>
           				
           			</div>
           			
            		<apex:pageBlockButtons location="bottom">
            			
						<apex:commandButton action="{!saveChangeOrderBehaviourRule}" value="{!$Label.kimbleone__save}" oncomplete="hideModalBoxyWithErrorCheck('#ChangeOrderBehaviourRuleErrors');refreshTable();" reRender="COBRPanel"/>
						<input class="btn" type="button" onclick="hideModalBoxy();" value="{!$Label.Cancel}"/>
					
					</apex:pageBlockButtons>
					
             		<apex:pageblockSection collapsible="false" showheader="false"  columns="1">
             			<apex:inputfield value="{!editcobr.Name}" required="true" />
               			<apex:inputfield value="{!editcobr.KimbleOne__DefaultForecastStatus__c}" required="false" style="width:400px;"/>
               			<apex:inputfield value="{!editcobr.KimbleOne__ProposalAcceptanceType__c}" required="false" style="width:400px;"/>
                    </apex:pageBlockSection>            
        		</apex:pageBlock>
         	</apex:form>
        </apex:outputPanel> 
	</div>	

	<div id="UsageTrackingModelPopup" style="display:none;">  
		<apex:outputPanel id="UsageTrackingModelPanel">
     	  <apex:form >
     	  
           		<apex:pageBlock >    
           		
           			<div id="UsageTrackingModelErrors">
           				<apex:pageMessages escape="false"/>
           				
           			</div>
           			
            		<apex:pageBlockButtons location="bottom">
            			
						<apex:commandButton action="{!saveUsageTrackingModel}" value="{!$Label.kimbleone__save}" oncomplete="hideModalBoxyWithErrorCheck('#UsageTrackingModelErrors');refreshTable();" reRender="UTMPanel"/>
						<input class="btn" type="button" onclick="hideModalBoxy();" value="{!$Label.Cancel}"/>
					
					</apex:pageBlockButtons>
					
             		<apex:pageblockSection collapsible="false" showheader="false"  columns="1">
             			<apex:inputfield value="{!editutm.Name}" required="true" />
               			<apex:inputfield value="{!editutm.KimbleOne__ActualisedForecastApprovalRequired__c}" required="false"/>
               			<apex:inputfield value="{!editutm.KimbleOne__ActualiseForecastOnPeriodChange__c}" required="false"/>
               			<apex:inputfield value="{!editutm.KimbleOne__ExcludeFromActualChecks__c}" required="false"/>
               			<apex:inputfield value="{!editutm.KimbleOne__IsActualUntracked__c}" required="false"/>               			               			
                    </apex:pageBlockSection>            
        		</apex:pageBlock>
         	</apex:form>
        </apex:outputPanel> 
	</div>		
	
</apex:define>
</apex:composition>
<apex:outputPanel rendered="{!ReferenceDatapassed}">
<script>
	//Refresh onload
	window.onload= new function(){
		refreshTable();
	}	
</script>
</apex:outputPanel>
</apex:page>