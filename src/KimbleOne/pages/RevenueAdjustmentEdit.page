<apex:page standardcontroller="KimbleOne__RevenueAdjustment__c" extensions="KimbleOne.RevenueAdjustmentEditController" standardStylesheets="true" showHeader="true" sidebar="false" >
  	<apex:Composition template="KimbleOne__SiteMaster">
		<apex:define name="Menu">
			<c:Menu MenuContextType="DeliveryGroup" MenuContextId="{!KimbleOne__RevenueAdjustment__c.DeliveryElement__r.KimbleOne__DeliveryGroup__c}" MenuContextName="{!KimbleOne__RevenueAdjustment__c.DeliveryElement__r.DeliveryGroup__r.Name}" CurrentPage="{!$Page.KimbleOne__DeliveryElementRevenueAdjustments}"/>
		</apex:define>
		<apex:define name="Content">
			
			<!-- Revenue Adjustment Details -->
			<apex:outputPanel >
			<apex:form id="revenueAdjustmentPanel">
				
							    		    
						<apex:pageMessages escape="false"/>
						
						<apex:pageBlock title="{!$ObjectType.KimbleOne__RevenueAdjustment__c.Label} - {!KimbleOne__RevenueAdjustment__c.Name}">
			 		    	
			 		    	<apex:pageBlockButtons location="top">
    							<apex:commandButton id="save" action="{!SaveRevenueAdjustment}" styleclass="DisableButtonWhileBusy" value="{!$Label.kimbleone__save}" rerender="revenueAdjustmentPanel,revenueAdjustmentItemPanel"/>
    						</apex:pageBlockButtons>
			 		    	
			 		    	<apex:pageBlockSection columns="2">
									
									<apex:outputField value="{!KimbleOne__RevenueAdjustment__c.Account__r.Name}"/>																																								
									
									<apex:pageBlockSectionItem >
										<apex:outputText value="{!$ObjectType.KimbleOne__RevenueAdjustment__c.Fields.KimbleOne__RevenueAdjustmentReason__c.Label}" />
										<apex:selectList id="revenueItemAdjustmentReason" value="{!KimbleOne__RevenueAdjustment__c.KimbleOne__RevenueAdjustmentReason__c}" size="1" required="true">
											<apex:actionSupport event="onchange" rerender="revenueAdjustmentPanel" />
											<apex:selectOptions value="{!RevenueAdjustmentReasons}" />
										</apex:selectList>
									</apex:pageBlockSectionItem>
									
									<apex:outputField value="{!KimbleOne__RevenueAdjustment__c.Name}"/>
																		
									<apex:inputField value="{!KimbleOne__RevenueAdjustment__c.KimbleOne__Description__c}"/>
									
									<apex:inputField value="{!KimbleOne__RevenueAdjustment__c.KimbleOne__AdjustmentDate__c}" required="true"/>
									
									<!--  Delivery Element Scope and not Invoiceable -->
									<apex:pageBlockSectionItem rendered="{!IsDeliveryELementScope && !IsInvoiceable}">
										<apex:outputLabel value="{!$ObjectType.KimbleOne__RevenueAdjustment__c.Fields.KimbleOne__AdjustmentRevenue__c.Label}" />
										<apex:inputField value="{!UnsignedAdjustmentRevenue.KimbleOne__InputDecimal__c}" required="true"/>
									</apex:pageBlockSectionItem>
									
									<!--  Delivery Element Scope and Invoiceable -->
									<apex:pageBlockSectionItem rendered="{!IsDeliveryELementScope && IsInvoiceable}">
										<apex:outputLabel value="{!$ObjectType.KimbleOne__RevenueAdjustment__c.Fields.KimbleOne__AdjustmentRevenue__c.Label} {!KimbleOne__RevenueAdjustment__c.KimbleOne__InvoicingCurrencyIsoCode__c}" />
										<apex:inputField value="{!UnsignedInvoicingCurrencyAdjustmentRevenue.KimbleOne__InputDecimal__c}" required="true"/>
              						</apex:pageBlockSectionItem>
              						
              						<!--  Revenue Item Scope and not Invoiceable -->
									<apex:pageBlockSectionItem rendered="{!!IsDeliveryElementScope && !IsInvoiceable}" >
										<apex:outputLabel value="{!$ObjectType.KimbleOne__RevenueAdjustment__c.Fields.KimbleOne__AdjustmentRevenue__c.Label}" />
										<apex:outputField value="{!UnsignedAdjustmentRevenue.KimbleOne__InputDecimal__c}" />
									</apex:pageBlockSectionItem>
										
              						<!--  Revenue Item Scope and Invoiceable -->
									<apex:pageBlockSectionItem rendered="{!!IsDeliveryElementScope && IsInvoiceable}">
										<apex:outputLabel value="{!$ObjectType.KimbleOne__RevenueAdjustment__c.Fields.KimbleOne__AdjustmentRevenue__c.Label} {!KimbleOne__RevenueAdjustment__c.KimbleOne__InvoicingCurrencyIsoCode__c}" />
										<apex:outputField value="{!UnsignedInvoicingCurrencyAdjustmentRevenue.KimbleOne__InputDecimal__c}" />
              						</apex:pageBlockSectionItem>
									
									<apex:pageBlockSectionItem >
										<apex:outputLabel value="{!$ObjectType.KimbleOne__RevenueAdjustment__c.Fields.KimbleOne__Status__c.Label}"></apex:outputLabel>
										<apex:outputField value="{!KimbleOne__RevenueAdjustment__c.Status__r.Name}" />
									</apex:pageBlockSectionItem>

					        </apex:pageBlockSection>
			 			</apex:pageBlock>
					
				</apex:form>
			</apex:outputPanel>
			
				<!-- Revenue Adjustment Items Details -->
				<apex:outPutPanel id="revenueAdjustmentItemPanel">
						<apex:pageBlock >
	 						
						    <apex:pageBlockSection collapsible="false" showheader="true" columns="1">
								<apex:facet name="header">
									{!$ObjectType.KimbleOne__RevenueAdjustmentItem__c.LabelPlural}
								</apex:facet>
								<apex:facet name="body">
									<apex:pageBlockTable value="{!RevenueAdjustmentItems}" var="WOItem">
																	
										<apex:column headerValue="{!$ObjectType.KimbleOne__RevenueAdjustmentItem__c.Fields.KimbleOne__DeliveryElement__c.Label}" width="100px" style="text-align:left;" value="{!WOItem.KimbleOne__DeliveryElement__c}"></apex:column>																		
										<apex:column headerValue="{!$ObjectType.KimbleOne__RevenueAdjustmentItem__c.Fields.KimbleOne__AdjustmentRevenue__c.Label}" style="text-align:right;" width="100px" value="{!WOItem.KimbleOne__AdjustmentRevenue__c}"></apex:column>
										
										<!-- Invoice Types -->
										<apex:column headerValue="{!$ObjectType.KimbleOne__RevenueAdjustmentItem__c.Fields.KimbleOne__ActivityAssignment__c.Label}" width="100px" style="text-align:center;" value="{!WOItem.ActivityAssignment__r.Name}"></apex:column>
										<apex:column headerValue="{!$ObjectType.KimbleOne__RevenueAdjustmentItem__c.Fields.KimbleOne__TimeEntry__c.Label}" width="100px" style="text-align:center;" value="{!WOItem.TimeEntry__r.Resource__r.Name} {!WOItem.TimeEntry__r.TimePeriod__r.Name}"></apex:column>
										<apex:column headerValue="{!$ObjectType.KimbleOne__RevenueAdjustmentItem__c.Fields.KimbleOne__ExpenseItem__c.Label}" width="100px" style="text-align:center;" value="{!WOItem.ExpenseItem__r.Resource__r.Name} {!WOItem.ExpenseItem__r.ActivityExpenseCategory__r.Name}"></apex:column>																							
										<apex:column headerValue="{!$ObjectType.KimbleOne__RevenueAdjustmentItem__c.Fields.KimbleOne__PeriodRevenue__c.Label}" width="100px" style="text-align:center;" value="{!WOItem.PeriodRevenue__r.TimePeriod__r.Name}"></apex:column>
										<apex:column headerValue="{!$ObjectType.KimbleOne__RevenueAdjustmentItem__c.Fields.KimbleOne__Milestone__c.Label}" width="100px" style="text-align:center;" value="{!WOItem.Milestone__r.Name}"></apex:column>
										
									</apex:pageBlockTable>
								</apex:facet>
							</apex:pageBlockSection>

						</apex:pageBlock>
					</apex:outPutPanel>

		</apex:define>
	</apex:Composition>
</apex:page>