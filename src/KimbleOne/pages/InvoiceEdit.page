<apex:page standardController="KimbleOne__Invoice__c" action="{!CheckStatus}"  extensions="KimbleOne.InvoiceEditController" showHeader="true" sidebar="false" tabStyle="KimbleOne__Invoice__c">

<apex:composition template="KimbleOne__SiteMaster">
    <apex:define name="Menu">
      	<c:Menu DefaultHelpPage="ManagingInvoices.html" MenuContextType="Invoice" MenuContextId="{!KimbleOne__Invoice__c.Id}"  MenuContextName="{!KimbleOne__Invoice__c.KimbleOne__Reference__c}"/>
    </apex:define>
    
    <apex:define name="Content">
    	<apex:form id="TheForm">
    	<div id="pageErrors">
    		<apex:pageMessages escape="False" />
		</div>
		
		<apex:pageBlock title="{!$Label.kimbleone__editinvoiceheading}">
			<apex:pageblockButtons >
				<apex:commandButton action="{!save}" value="{!$Label.kimbleone__save}"/>
			</apex:pageblockButtons>
			<apex:pageBlockSection columns="2" title="{!$Label.kimbleone__invoicedetailsheading}">
				<apex:outputField value="{!KimbleOne__Invoice__c.KimbleOne__Account__c}" />
				<apex:pageBlockSectionItem >
					<apex:outputLabel for="invoiceStatus" value="{!$ObjectType.KimbleOne__Invoice__c.Fields.KimbleOne__Status__c.Label}"></apex:outputLabel>
					<apex:outputField id="invoiceStatus" value="{!KimbleOne__Invoice__c.Status__r.Name}" />
				</apex:pageBlockSectionItem> 
				
				<apex:inputField value="{!KimbleOne__Invoice__c.KimbleOne__InvoiceDate__c}" rendered="{!KimbleOne__Invoice__c.Status__r.KimbleOne__Enum__c == Draft}"/>
				<apex:outputField value="{!KimbleOne__Invoice__c.KimbleOne__InvoiceDate__c}" rendered="{!KimbleOne__Invoice__c.Status__r.KimbleOne__Enum__c != Draft}"/>
				
				<apex:inputField required="true" value="{!KimbleOne__Invoice__c.KimbleOne__InvoicePaymentTermDays__c}" rendered="{!KimbleOne__Invoice__c.Status__r.KimbleOne__Enum__c == Draft}"/>
				<apex:outputField value="{!KimbleOne__Invoice__c.KimbleOne__InvoicePaymentTermDays__c}" rendered="{!KimbleOne__Invoice__c.Status__r.KimbleOne__Enum__c != Draft}"/>
				
				<apex:pageBlockSectionItem rendered="{!KimbleOne__Invoice__c.Status__r.KimbleOne__Enum__c == Draft}">
						<apex:outputText value="{!$ObjectType.KimbleOne__Invoice__c.Fields.KimbleOne__BusinessUnitTradingEntity__c.Label}" />
						<apex:inputText value="{!KimbleOne__Invoice__c.KimbleOne__BusinessUnitTradingEntity__c}" styleclass="kimblecustomlookup GetTradingEntityList null true false" required="true"/> 
				</apex:pageBlockSectionItem>
				<apex:outputField value="{!KimbleOne__Invoice__c.KimbleOne__BusinessUnitTradingEntity__c}" rendered="{!KimbleOne__Invoice__c.Status__r.KimbleOne__Enum__c != Draft}"/>
				
				
				<apex:pageBlockSectionItem rendered="{!KimbleOne__Invoice__c.Status__r.KimbleOne__Enum__c == Draft}">
						<apex:outputText value="{!$ObjectType.KimbleOne__Invoice__c.Fields.KimbleOne__ReceivableBankAccount__c.Label}" />
						<apex:inputText value="{!KimbleOne__Invoice__c.KimbleOne__ReceivableBankAccount__c}" styleclass="kimblecustomlookup GetBankAccountList null true false" required="true"/> 
				</apex:pageBlockSectionItem>
				<apex:outputField value="{!KimbleOne__Invoice__c.KimbleOne__ReceivableBankAccount__c}" rendered="{!KimbleOne__Invoice__c.Status__r.KimbleOne__Enum__c != Draft}"/>
				
				<apex:outputField value="{!KimbleOne__Invoice__c.KimbleOne__NetAmount__c}"/>
				
				<apex:outputField value="{!KimbleOne__Invoice__c.KimbleOne__GrossAmount__c}"/>
				
				<apex:pageBlockSectionItem >
					<apex:outputLabel value="{!$ObjectType.KimbleOne__Invoice__c.Fields.KimbleOne__TaxAmount__c.Label}"></apex:outputLabel>
					<apex:outputField value="{!KimbleOne__Invoice__c.KimbleOne__TaxAmountSum__c}" rendered="{!KimbleOne__Invoice__c.KimbleOne__TaxAmountSum__c != 0}"/>
				</apex:pageBlockSectionItem>
				
				<apex:pageBlockSectionItem >
				&nbsp;
				</apex:pageBlockSectionItem>
				
				<apex:inputText value="{!KimbleOne__Invoice__c.KimbleOne__Contact__c}" styleclass="kimblecustomlookup GetAllAccountContacts {!KimbleOne__Invoice__c.KimbleOne__Account__c} false true"/>
				
				<apex:inputField value="{!KimbleOne__Invoice__c.KimbleOne__Notes__c}"/>
				
			</apex:pageBlockSection>
			
			<apex:pageBlockSection columns="2" title="{!$Label.kimbleone__presentationdetailsheading}">
				<apex:outputField value="{!KimbleOne__Invoice__c.KimbleOne__InvoiceFormat__c}" />
				
				<apex:pageBlockSectionItem rendered="{!KimbleOne__Invoice__c.Status__r.KimbleOne__Enum__c == Draft}">
						<apex:outputText value="{!$ObjectType.KimbleOne__Invoice__c.Fields.KimbleOne__InvoiceTemplate__c.Label}" />
						<apex:inputText value="{!KimbleOne__Invoice__c.KimbleOne__InvoiceTemplate__c}" styleclass="kimblecustomlookup GetCommercialDocumentTemplateList Invoice true false"/> 
				</apex:pageBlockSectionItem>
				<apex:inputField value="{!KimbleOne__Invoice__c.KimbleOne__InvoiceTemplate__c}" rendered="{!KimbleOne__Invoice__c.Status__r.KimbleOne__Enum__c != Draft}"/>
				
				<apex:inputField value="{!KimbleOne__Invoice__c.KimbleOne__InvoiceContextDescription__c}" rendered="{!KimbleOne__Invoice__c.InvoiceFormat__r.KimbleOne__ShowContext__c}" />
			</apex:pageBlockSection>
			
			<apex:pageBlockSection columns="2" title="{!$Label.kimbleone__customdetailsheading}" rendered="{!$ObjectType.KimbleOne__Invoice__c.FieldSets.KimbleOne__InvoiceDetails.size != 0}">
				<apex:repeat value="{!$ObjectType.KimbleOne__Invoice__c.FieldSets.KimbleOne__InvoiceDetails}" var="i">
     					<apex:inputField value="{!KimbleOne__Invoice__c[i]}"/>
   					</apex:repeat>
   			</apex:pageBlockSection>
			
			<apex:pageBlockSection columns="1" title="{!$Label.kimbleone__rejectioncommentsheading}" rendered="{!WasPreviouslyRejected}">
				<apex:outputField value="{!KimbleOne__Invoice__c.KimbleOne__ApprovalComments__c}"/>
			</apex:pageBlockSection>
			
		</apex:pageBlock>
		
		</apex:form>
	</apex:define>
</apex:composition>

</apex:page>