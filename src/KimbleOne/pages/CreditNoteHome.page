<apex:page standardController="KimbleOne__CreditNote__c" extensions="KimbleOne.CreditNoteHomeController,KimbleOne.SecurityController" showHeader="true" sidebar="false" tabStyle="KimbleOne__CreditNote__c">

<apex:composition template="KimbleOne__SiteMaster">
    <apex:define name="Menu">
      	<c:Menu DefaultHelpPage="CreateCreditNote.html" MenuContextType="CreditNote" MenuContextId="{!KimbleOne__CreditNote__c.Id}" MenuContextName="{!KimbleOne__CreditNote__c.KimbleOne__Reference__c}"/>
    </apex:define>
    
    <apex:define name="Content">
    	<apex:form id="TheForm">
    	<apex:pageMessages escape="false"/>
    	
    	<apex:pageBlock title="{!$ObjectType.KimbleOne__CreditNote__c.Label}">
    		
    		<apex:pageBlockButtons location="top">
    			<apex:commandButton id="sendCreditNoteForApprovalBtn" action="{!SendForApproval}" styleclass="DisableButtonWhileBusy" value="{!$Label.kimbleone__submitforapprovalbutton}" rerender="TheForm, ApprovalHistoryPanel" rendered="{!KimbleOne__CreditNote__c.Status__r.KimbleOne__Enum__c = 'Draft' && $ObjectType.KimbleOne__CreditNote__c.updateable}"/>
    			<apex:commandButton action="{!Del}" styleclass="DisableButtonWhileBusy" value="{!$Label.kimbleone__deletebutton}" rerender="TheForm" rendered="{!KimbleOne__CreditNote__c.Status__r.KimbleOne__Enum__c = 'Draft' && $ObjectType.KimbleOne__CreditNote__c.deletable}"/>
    			<apex:commandButton id="dispatchCreditNoteBtn" action="{!Dispatch}" styleclass="DisableButtonWhileBusy" value="{!$Label.kimbleone__dispatch}" rerender="TheForm" rendered="{!KimbleOne__CreditNote__c.Status__r.KimbleOne__Enum__c = 'Approved' && $ObjectType.KimbleOne__CreditNote__c.updateable && HasPermission['DispatchInvoice']}"/>
    			
    			<apex:outputPanel layout="none" rendered="{!KimbleOne__CreditNote__c.Status__r.KimbleOne__Enum__c = 'Dispatched'}">
    				<input id="sendEmailBtn" class="btn" type="button" onclick="initialiseModalBoxy('#sendEmailPopup', '{!$Label.EmailCreditNoteLabel}', jQuery('form[id$=\'TheForm\']'));initialiseEmail();" value="Email Credit Note" />
    			</apex:outputPanel>
    			
    		</apex:pageBlockButtons>
						
			<apex:pageBlockSection columns="2" title="{!$Label.kimbleone__creditnotedetailsheading}">
				<apex:outputField value="{!KimbleOne__CreditNote__c.KimbleOne__Account__c}" />
				<apex:pageBlockSectionItem >
					<apex:outputLabel value="{!$ObjectType.KimbleOne__CreditNote__c.Fields.KimbleOne__Status__c.Label}"></apex:outputLabel>
					<apex:outputField value="{!KimbleOne__CreditNote__c.Status__r.Name}" />
				</apex:pageBlockSectionItem> 
				
				<apex:outputField value="{!KimbleOne__CreditNote__c.KimbleOne__CreditNoteDate__c}"/>
				
				<apex:pageBlockSectionItem >
					<apex:outputLabel value="{!$ObjectType.KimbleOne__CreditNote__c.Fields.KimbleOne__Invoice__c.Label}"></apex:outputLabel>
					<apex:outputField value="{!KimbleOne__CreditNote__c.Invoice__r.KimbleOne__ReferenceLink__c}" />
				</apex:pageBlockSectionItem> 
				
				<apex:outputField value="{!KimbleOne__CreditNote__c.KimbleOne__ReasonCode__c}"/>
				
				<apex:outputField value="{!KimbleOne__CreditNote__c.KimbleOne__ReasonDescription__c}"/>
				
				<apex:pageBlockSectionItem >
					<apex:outputLabel value="{!$ObjectType.KimbleOne__CreditNote__c.Fields.KimbleOne__NetAmount__c.Label}"></apex:outputLabel>
					<apex:outputText value="{0, number, {!KimbleOne__CreditNote__c.CurrencyIsoCode} #,###.00}">
						<apex:param value="{!ROUND(KimbleOne__CreditNote__c.KimbleOne__NetAmount__c,2)}"></apex:param>
					</apex:outputText>
				</apex:pageBlockSectionItem>
				
				<apex:pageBlockSectionItem >
					<apex:outputLabel value="{!$ObjectType.KimbleOne__CreditNote__c.Fields.KimbleOne__GrossAmount__c.Label}"></apex:outputLabel>
					<apex:outputText value="{0, number, {!KimbleOne__CreditNote__c.CurrencyIsoCode} #,###.00}">
						<apex:param value="{!ROUND(KimbleOne__CreditNote__c.KimbleOne__GrossAmount__c,2)}"></apex:param>
					</apex:outputText>
				</apex:pageBlockSectionItem>
				
				<apex:pageBlockSectionItem >
					<apex:outputLabel value="{!$ObjectType.KimbleOne__CreditNote__c.Fields.KimbleOne__TaxAmount__c.Label}"></apex:outputLabel>
					<apex:outputText value="{0, number, {!KimbleOne__CreditNote__c.CurrencyIsoCode} #,###.00}" rendered="{!KimbleOne__CreditNote__c.KimbleOne__TaxAmountSum__c != 0}">
						<apex:param value="{!ROUND(KimbleOne__CreditNote__c.KimbleOne__TaxAmountSum__c,2)}"></apex:param>
					</apex:outputText>
				</apex:pageBlockSectionItem>
				
				<apex:outputText id="credit-note-reference" value="{!KimbleOne__CreditNote__c.KimbleOne__Reference__c}"/>
								
   			</apex:pageBlockSection>
			
			<apex:pageBlockSection rendered="{!KimbleOne__CreditNote__c.Status__r.KimbleOne__Enum__c == 'Dispatched'}">
				<apex:pageBlockSectionItem >
					<apex:outputLabel value="{!$Label.kimbleone__filelabel}" ></apex:outputLabel>
					<a id="theDispatchedCreditNoteFilename" href="/servlet/servlet.FileDownload?file={!JSENCODE(TheDispatchedCreditNoteAttachment.Id)}">{!JSENCODE(TheDispatchedCreditNoteAttachment.Name)}</a>
				</apex:pageBlockSectionItem>
			
			</apex:pageBlockSection>
			
			<apex:repeat value="{!TheCreditNoteSections}" var="section">
 				<apex:pageBlockSection columns="1" title="{!section.Name}" collapsible="false">
					<apex:pageBlockTable value="{!section.CreditNoteLines__r}" var="line">
						<apex:column width="15%" value="{!line.KimbleOne__LineNumber__c}"/>
						<apex:column width="55%" value="{!line.KimbleOne__Narrative__c}"/>
						<apex:column width="15%" value="{!line.KimbleOne__NetAmount__c}"/>
						<apex:column width="15%" value="{!line.KimbleOne__TaxAmount__c}"/>
					</apex:pageBlockTable>
				</apex:pageBlockSection>
			</apex:repeat>
			</apex:pageBlock>
			
			<apex:actionFunction action="{!InitialiseEmail}" name="initialiseEmail" rerender="sendEmailPanel" oncomplete="showModalBoxy()"></apex:actionFunction>
			
			<div id="sendEmailPopup" style="display:none">
				<apex:outputPanel id="sendEmailPanel">
				<apex:pageBlock >
						<div id="sendEmailErrors"><apex:pageMessages escape="false"/></div>
						<apex:pageBlockButtons location="bottom">
							<apex:commandButton value="{!$Label.kimbleone__ok}" styleclass="DisableButtonWhileBusy" action="{!SendEmail}" oncomplete="hideModalBoxyWithErrorCheck('#sendEmailErrors')" rerender="sendEmailPanel" />
							<input class="btn" type="button" onclick="hideModalBoxy();" value="{!$Label.Cancel}" />
						</apex:pageBlockButtons>
						<apex:pageBlockSection columns="1">
							
							<apex:pageBlockSectionItem >
								<apex:outputLabel value="{!$Label.kimbleone__emailtemplatelabel}"></apex:outputLabel>
								<apex:selectList id="emailTemplate" value="{!TheSelectedDocTemplateId}" size="1">
									<apex:selectOptions value="{!TheEmailTemplates}" />
								</apex:selectList>
							</apex:pageBlockSectionItem>
							
							<apex:pageBlockSectionItem >
								<apex:outputLabel value="{!$Label.kimbleone__contactlabel}"></apex:outputLabel>
								<apex:selectList id="contactToSelect" value="{!TheContactIdToEmail}" size="1" >
									<apex:selectOptions value="{!TheAccountContacts}" />
								</apex:selectList>
							</apex:pageBlockSectionItem>
							
						</apex:pageBlockSection>
						
					</apex:pageBlock>
			</apex:outputPanel>
			</div>
			
			</apex:form>
			
			<apex:outputPanel layout="none" id="ApprovalHistoryPanel">
			<apex:relatedList list="ProcessSteps" id="CreditNoteApprovalHistory"></apex:relatedList>
			<script>
						$(document).ready(function(){
    						$(function() {
    							hideSalesForceApprovalProcessButtons();
    						})
    					});
    					function hideSalesForceApprovalProcessButtons()
    					{
    							$('input[name="piSubmit"]').remove();
    							$('input[name="piRemove"]').remove();
    					}
				</script>
			</apex:outputPanel>
			
			<apex:relatedList list="ActivityHistories" id="CreditNoteActivities"></apex:relatedList>
			
	</apex:define>
</apex:composition>

</apex:page>