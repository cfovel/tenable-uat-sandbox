<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ExpenseClaimRejected</fullName>
        <description>Expense Claim Rejected</description>
        <protected>true</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>KimbleEmailTemplates/Kimble_ExpenseClaimRejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>ExpenseClaimApprovalStatusApproved</fullName>
        <description>Sets ExpenseClaim ApprovalStatus to Approved.</description>
        <field>ApprovalStatus__c</field>
        <formula>&quot;Approved&quot;</formula>
        <name>ExpenseClaimApprovalStatusApproved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ExpenseClaimApprovalStatusRejected</fullName>
        <description>Sets ExpenseClaim ApprovalStatus to Rejected.</description>
        <field>ApprovalStatus__c</field>
        <formula>&quot;Rejected&quot;</formula>
        <name>ExpenseClaimApprovalStatusRejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>ExpenseClaimRejected</fullName>
        <actions>
            <name>ExpenseClaimRejected</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>ExpenseClaim__c.ApprovalStatus__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <description>Notify user that Expense Claim Rejected.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TemplateExpenseClaimFieldUpdates</fullName>
        <actions>
            <name>ExpenseClaimApprovalStatusApproved</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ExpenseClaimApprovalStatusRejected</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>ExpenseClaim__c.ApprovalStatus__c</field>
            <operation>equals</operation>
            <value>X</value>
        </criteriaItems>
        <description>Template workflow rule to deploy Field Updates</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
