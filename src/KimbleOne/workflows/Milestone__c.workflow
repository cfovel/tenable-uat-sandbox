<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>RevenueMilestoneRejected</fullName>
        <description>Revenue Milestone Rejected</description>
        <protected>true</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>KimbleEmailTemplates/Kimble_RevenueMilestoneRejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>MilestoneApprovalStatusApproved</fullName>
        <description>Set MilestoneApprovalStatus to Approved</description>
        <field>ApprovalStatus__c</field>
        <formula>&quot;Approved&quot;</formula>
        <name>MilestoneApprovalStatusApproved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MilestoneApprovalStatusRejected</fullName>
        <description>Set Milestone ApprovalStatus to Rejected</description>
        <field>ApprovalStatus__c</field>
        <formula>&quot;Rejected&quot;</formula>
        <name>MilestoneApprovalStatusRejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>RevenueMilestoneRejected</fullName>
        <actions>
            <name>RevenueMilestoneRejected</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Notify user that Revenue Milestone Rejected.</description>
        <formula>MilestoneType__r.Enum__c = &apos;Revenue&apos; &amp;&amp;  ApprovalStatus__c = &apos;Rejected&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TemplateMilestoneFieldUpdates</fullName>
        <actions>
            <name>MilestoneApprovalStatusApproved</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>MilestoneApprovalStatusRejected</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Milestone__c.ApprovalStatus__c</field>
            <operation>equals</operation>
            <value>X</value>
        </criteriaItems>
        <description>Template workflow rule to deploy Field Updates.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
