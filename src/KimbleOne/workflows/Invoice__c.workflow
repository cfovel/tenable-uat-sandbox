<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>InvoiceRejected</fullName>
        <description>Invoice Rejected</description>
        <protected>true</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>KimbleEmailTemplates/Kimble_InvoiceRejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>InvoiceApprovalStatusApproved</fullName>
        <description>Set Invoice Approval Status to Approved.</description>
        <field>ApprovalStatus__c</field>
        <formula>&quot;Approved&quot;</formula>
        <name>InvoiceApprovalStatusApproved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>InvoiceApprovalStatusRejected</fullName>
        <description>Set Invoice Approval Status to Rejected.</description>
        <field>ApprovalStatus__c</field>
        <formula>&quot;Rejected&quot;</formula>
        <name>InvoiceApprovalStatusRejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>InvoiceRejected</fullName>
        <actions>
            <name>InvoiceRejected</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Invoice__c.ApprovalStatus__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <description>Notify user that Invoice Rejected.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TemplateInvoiceFieldUpdates</fullName>
        <actions>
            <name>InvoiceApprovalStatusApproved</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>InvoiceApprovalStatusRejected</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Invoice__c.ApprovalStatus__c</field>
            <operation>equals</operation>
            <value>X</value>
        </criteriaItems>
        <description>Template workflow to deploy Field Updates.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
