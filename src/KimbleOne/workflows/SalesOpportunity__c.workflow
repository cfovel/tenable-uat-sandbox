<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>NewSalesOpportunity</fullName>
        <actions>
            <name>BidReviewMeeting</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>Create tasks when new Sales Opportunity created</description>
        <formula>NOT( ISBLANK( CloseDate__c ) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>BidReviewMeeting</fullName>
        <assignedToType>owner</assignedToType>
        <description>Schedule bid review meeting 5 days before Close Date.</description>
        <dueDateOffset>-5</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>SalesOpportunity__c.CloseDate__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Bid Review Meeting</subject>
    </tasks>
</Workflow>
