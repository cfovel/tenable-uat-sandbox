<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CreditNoteApprovalStatusApproved</fullName>
        <description>Set Credit Note Approval Status to Approved.</description>
        <field>ApprovalStatus__c</field>
        <formula>&quot;Approved&quot;</formula>
        <name>CreditNoteApprovalStatusApproved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CreditNoteApprovalStatusRejected</fullName>
        <description>Set Credit Note Approval Status to Rejected.</description>
        <field>ApprovalStatus__c</field>
        <formula>&quot;Rejected&quot;</formula>
        <name>CreditNoteApprovalStatusRejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>TemplateCreditNoteFieldUpdates</fullName>
        <actions>
            <name>CreditNoteApprovalStatusApproved</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CreditNoteApprovalStatusRejected</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>CreditNote__c.ApprovalStatus__c</field>
            <operation>equals</operation>
            <value>X</value>
        </criteriaItems>
        <description>Template workflow to deploy Field Updates.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
