<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <customHelpPage>Help</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>ActivityAssignment__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Assignment</label>
        <referenceTo>ActivityAssignment__c</referenceTo>
        <relationshipLabel>Revenue Adjustment Items</relationshipLabel>
        <relationshipName>RevenueAdjustmentItems</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>AdjustmentRevenue__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Adjustment Revenue</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>DeliveryElement__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Delivery Element</label>
        <referenceTo>DeliveryElement__c</referenceTo>
        <relationshipLabel>Revenue Adjustment Items</relationshipLabel>
        <relationshipName>RevenueAdjustmentItems</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>true</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>ExpenseItem__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Expense Item</label>
        <referenceTo>ExpenseItem__c</referenceTo>
        <relationshipLabel>Revenue Adjustment Items</relationshipLabel>
        <relationshipName>RevenueAdjustmentItems</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ForecastingTimePeriod__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Forecasting Time Period</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>TimePeriod__c.Perspective__r.Enum__c</field>
                <operation>equals</operation>
                <value>Operational</value>
            </filterItems>
            <filterItems>
                <field>TimePeriod__c.PeriodType__r.ForecastAtThisLevel__c</field>
                <operation>equals</operation>
                <value>True</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>TimePeriod__c</referenceTo>
        <relationshipLabel>Revenue Adjustment Items</relationshipLabel>
        <relationshipName>RevenueAdjustmentItems</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>InvoiceableItem__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Invoiceable Item</label>
        <referenceTo>InvoiceableItem__c</referenceTo>
        <relationshipLabel>Revenue Adjustment Items</relationshipLabel>
        <relationshipName>RevenueAdjustmentItems</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>InvoicingCurrencyAdjustmentRevenue__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Invoicing Currency Adjustment Revenue</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>InvoicingCurrencyIsoCode__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Invoicing Currency</label>
        <length>3</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Milestone__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Milestone</label>
        <referenceTo>Milestone__c</referenceTo>
        <relationshipLabel>Revenue Adjustment Items</relationshipLabel>
        <relationshipName>RevenueAdjustmentItems</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>PeriodRevenue__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Period Revenue</label>
        <referenceTo>PeriodRevenue__c</referenceTo>
        <relationshipLabel>Revenue Adjustment Items</relationshipLabel>
        <relationshipName>RevenueAdjustmentItems</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>RevenueAdjustment__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Revenue Adjustment</label>
        <referenceTo>RevenueAdjustment__c</referenceTo>
        <relationshipLabel>Revenue Adjustment Items</relationshipLabel>
        <relationshipName>RevenueAdjustmentItems</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>TimeEntry__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Time Entry</label>
        <referenceTo>TimeEntry__c</referenceTo>
        <relationshipLabel>Revenue Adjustment Items</relationshipLabel>
        <relationshipName>RevenueAdjustmentItems</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Revenue Adjustment Item</label>
    <nameField>
        <displayFormat>{0}</displayFormat>
        <label>Revenue Adjustment Item Id</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Revenue Adjustment Items</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>ForecastingPeriodBusinessUnitGroup</fullName>
        <active>true</active>
        <description>Forecasting Period on Revenue Adjustment Item has same Business Unit Group context as the Delivery Element</description>
        <errorConditionFormula>NOT( ISBLANK( ForecastingTimePeriod__c ) )  &amp;&amp; ForecastingTimePeriod__r.BusinessUnitGroup__c &lt;&gt;  RevenueAdjustment__r.DeliveryElement__r.BusinessUnitGroup__c</errorConditionFormula>
        <errorMessage>Revenue Adjustment Item Forecasting Period has invalid Business Unit Group context</errorMessage>
    </validationRules>
</CustomObject>
