<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <content>ResourcedActivityTemplateEdit</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <content>ResourcedActivityTemplateEdit</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <customHelpPage>Help</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>ActualiseForecastOnly__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Actualise Forecast Only?</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>AllowFrameworkAssignment__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Allow Framework Assignment</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>CalendarIsPrivate__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Calendar Is Private</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>CalendarShowAs__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Calendar Show As</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>ReferenceData__c.Domain__c</field>
                <operation>equals</operation>
                <value>CalendarShowAs</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>ReferenceData__c</referenceTo>
        <relationshipLabel>Resourced Activity Templates (Calendar Show As)</relationshipLabel>
        <relationshipName>ResourcedActivityTemplateCalendar</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>CanSelfForecast__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Can Self Forecast?</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>DefaultUsage__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Z_Obsolete - Default Usage</label>
        <precision>7</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ForecastUnitType__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Forecast Unit Type</label>
        <referenceTo>UnitType__c</referenceTo>
        <relationshipLabel>Resourced Activity Templates (Forecast Unit Type)</relationshipLabel>
        <relationshipName>ResourcedActivityTemplatesForecast</relationshipName>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>HasExternalPlan__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Has External Plan</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>HasTaskStartEndTime__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Tasks have Start And End Times</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>HideForecastTimeEntries__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Hide Forecast Time Entries</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>InvoicingUnitType__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Invoicing Unit Type</label>
        <referenceTo>UnitType__c</referenceTo>
        <relationshipLabel>Resourced Activity Templates (Invoicing Unit Type)</relationshipLabel>
        <relationshipName>ResourcedActivityTemplatesInvoicing</relationshipName>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>PlanType__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Plan Type</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>ReferenceData__c.Domain__c</field>
                <operation>equals</operation>
                <value>PlanType</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>ReferenceData__c</referenceTo>
        <relationshipName>ResourcedActivityTemplate</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ProductDomain__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Product Domain</label>
        <referenceTo>ProductDomain__c</referenceTo>
        <relationshipLabel>Resourced Activity Templates</relationshipLabel>
        <relationshipName>ResourcedActivityTemplate</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ResourcedActivityType__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Resourced Activity Type</label>
        <referenceTo>ResourcedActivityType__c</referenceTo>
        <relationshipName>ResourcedActivityTemplate</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>ScheduledTimeRule__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Scheduled Time Rule</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>ReferenceData__c.Domain__c</field>
                <operation>equals</operation>
                <value>ScheduledTimeRule</value>
            </filterItems>
            <isOptional>true</isOptional>
        </lookupFilter>
        <referenceTo>ReferenceData__c</referenceTo>
        <relationshipLabel>Resourced Activity Templates (Scheduled Time Rule)</relationshipLabel>
        <relationshipName>ResourcedActivityTemplatesSchedTimeRule</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SelfForecastApprovalRequired__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Approval Required?</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>TimeEntryRule__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Time Entry Rule</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>ReferenceData__c.Domain__c</field>
                <operation>equals</operation>
                <value>TimeEntryRule</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>ReferenceData__c</referenceTo>
        <relationshipLabel>Resourced Activity Templates (Time Entry Rule)</relationshipLabel>
        <relationshipName>ResourcedActivityTemplatesTimeEntryRule</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>UsageAllocationType__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Usage Allocation Type</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>ReferenceData__c.Domain__c</field>
                <operation>equals</operation>
                <value>UsageAllocationType</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>ReferenceData__c</referenceTo>
        <relationshipLabel>Resourced Activity Templates (Usage Allocation Type)</relationshipLabel>
        <relationshipName>ResourcedActivityTemplates</relationshipName>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>UsageForecastFormat__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Z_Obsolete - Usage Forecast Format</label>
        <precision>7</precision>
        <required>false</required>
        <scale>4</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>UsageFormat__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Z_Obsolete - Usage Actual Format</label>
        <precision>7</precision>
        <required>false</required>
        <scale>4</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>UsageTrackingModel__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Usage Tracking Model</label>
        <referenceTo>UsageTrackingModel__c</referenceTo>
        <relationshipLabel>Resourced Activity Templates</relationshipLabel>
        <relationshipName>ResourcedActivityTemplates</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>UsageUnitType__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Usage Unit Type</label>
        <referenceTo>UnitType__c</referenceTo>
        <relationshipLabel>Resourced Activity Templates (Usage Unit Type)</relationshipLabel>
        <relationshipName>ResourcedActivityTemplate</relationshipName>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>UtilisationRule__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Utilisation Rule</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>ReferenceData__c.Domain__c</field>
                <operation>equals</operation>
                <value>UtilisationRule</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>ReferenceData__c</referenceTo>
        <relationshipName>ResourcedActivityTemplateUtilisationRule</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>WorkItemLevel__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Work Item Level</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>ReferenceData__c.Domain__c</field>
                <operation>equals</operation>
                <value>WorkItemLevel</value>
            </filterItems>
            <isOptional>true</isOptional>
        </lookupFilter>
        <referenceTo>ReferenceData__c</referenceTo>
        <relationshipLabel>Resourced Activity Templates (Work Item Level)</relationshipLabel>
        <relationshipName>ResourcedActivityTemplatesWorkItemLevel</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>WorkItemUnitType__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Work Item Units</label>
        <referenceTo>UnitType__c</referenceTo>
        <relationshipLabel>Resourced Activity Templates (Work Item Units)</relationshipLabel>
        <relationshipName>ResActivityTemplatesWorkItemUnitType</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Resourced Activity Template</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>CalendarShowAs__c</columns>
        <columns>CalendarIsPrivate__c</columns>
        <columns>DefaultUsage__c</columns>
        <columns>ProductDomain__c</columns>
        <columns>ResourcedActivityType__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <label>Template Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Resourced Activity Templates</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>ForecastUnitTypeMandatory</fullName>
        <active>true</active>
        <description>Forecast Unit Type is Mandatory</description>
        <errorConditionFormula>ISBLANK(ForecastUnitType__c)</errorConditionFormula>
        <errorDisplayField>ForecastUnitType__c</errorDisplayField>
        <errorMessage>Forecast Unit Type must be entered</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>UsageAllocationTypeMandatory</fullName>
        <active>true</active>
        <errorConditionFormula>ISBLANK(UsageAllocationType__c)</errorConditionFormula>
        <errorDisplayField>UsageAllocationType__c</errorDisplayField>
        <errorMessage>Usage Allocation Type must be entered</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>UsageAllocationType_Mandatory</fullName>
        <active>true</active>
        <errorConditionFormula>ISBLANK( UsageAllocationType__c )</errorConditionFormula>
        <errorMessage>ERROR:UsageAllocationType_Is_Mandatory</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>UsageUnitTypeMandatory</fullName>
        <active>true</active>
        <description>Usage Unit Type is Mandatory</description>
        <errorConditionFormula>ISBLANK(UsageUnitType__c)</errorConditionFormula>
        <errorDisplayField>UsageUnitType__c</errorDisplayField>
        <errorMessage>Usage Unit Type must be entered</errorMessage>
    </validationRules>
</CustomObject>
