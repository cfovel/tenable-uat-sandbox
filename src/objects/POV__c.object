<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>An object for Tenable sellers and SE’s to manage proof of value (POV) activities to improve the management and tracking of high value POV activities.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Auto-populated upon creation; you can edit value after creation</inlineHelpText>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>POVs</relationshipLabel>
        <relationshipName>POVs</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Approval_Status__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Has this POV been submitted and/or approved?</inlineHelpText>
        <label>Approval Status</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Pending</fullName>
                    <default>true</default>
                </value>
                <value>
                    <fullName>In Review</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Approved</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Rejected</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Count_Completed__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Number of Completed POV Success Criteria associated with this POV</inlineHelpText>
        <label>Count: Completed</label>
        <summaryFilterItems>
            <field>POV_Success_Criteria__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </summaryFilterItems>
        <summaryForeignKey>POV_Success_Criteria__c.POV__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Count_Failed__c</fullName>
        <externalId>false</externalId>
        <label>Count: Failed</label>
        <summaryFilterItems>
            <field>POV_Success_Criteria__c.Status__c</field>
            <operation>equals</operation>
            <value>Failed</value>
        </summaryFilterItems>
        <summaryForeignKey>POV_Success_Criteria__c.POV__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Count_In_Review__c</fullName>
        <externalId>false</externalId>
        <label>Count: In Review</label>
        <summaryFilterItems>
            <field>POV_Success_Criteria__c.Status__c</field>
            <operation>equals</operation>
            <value>In Review</value>
        </summaryFilterItems>
        <summaryForeignKey>POV_Success_Criteria__c.POV__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Count_In_Testing__c</fullName>
        <externalId>false</externalId>
        <label>Count: In Testing</label>
        <summaryFilterItems>
            <field>POV_Success_Criteria__c.Status__c</field>
            <operation>equals</operation>
            <value>In Testing</value>
        </summaryFilterItems>
        <summaryForeignKey>POV_Success_Criteria__c.POV__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Count_Total__c</fullName>
        <externalId>false</externalId>
        <label>Count: Total</label>
        <summaryForeignKey>POV_Success_Criteria__c.POV__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Integration_Partners__c</fullName>
        <externalId>false</externalId>
        <label>Integration Partners</label>
        <length>32768</length>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Html</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>Opportunity_Amount__c</fullName>
        <externalId>false</externalId>
        <formula>Opportunity__r.Amount</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Value taken from associated Opportunity</inlineHelpText>
        <label>Opportunity Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Opportunity_Close_Date__c</fullName>
        <externalId>false</externalId>
        <formula>Opportunity__r.CloseDate</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Value taken from associated Opportunity</inlineHelpText>
        <label>Opportunity Close Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Opportunity_Owner__c</fullName>
        <externalId>false</externalId>
        <formula>Opportunity__r.Owner.FirstName &amp; &quot; &quot; &amp; Opportunity__r.Owner.LastName</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Value taken from associated Opportunity</inlineHelpText>
        <label>Opportunity Owner</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opportunity_Probability__c</fullName>
        <externalId>false</externalId>
        <formula>Opportunity__r.Probability</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Value taken from associated Opportunity</inlineHelpText>
        <label>Opportunity Probability</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Opportunity_Stage__c</fullName>
        <externalId>false</externalId>
        <formula>TEXT(Opportunity__r.StageName)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Value taken from associated Opportunity</inlineHelpText>
        <label>Opportunity Stage</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opportunity__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Opportunity</label>
        <referenceTo>Opportunity</referenceTo>
        <relationshipLabel>POVs</relationshipLabel>
        <relationshipName>POVs</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>POV_Age__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ISPICKVAL(POV_Stage__c, &quot;Completed&quot;), POV_Completion_Date__c - POV_Start_Date__c, TODAY() - POV_Start_Date__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>POV Age</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>POV_Completion_Date__c</fullName>
        <externalId>false</externalId>
        <label>POV Completion Date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>POV_Current_Status__c</fullName>
        <externalId>false</externalId>
        <label>POV Current Status</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Html</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>POV_Primary_Contact__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>The person you&apos;re primarily working with on this POV</inlineHelpText>
        <label>POV Primary Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>POVs</relationshipLabel>
        <relationshipName>POVs</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>POV_Product_s__c</fullName>
        <externalId>false</externalId>
        <label>POV Product(s)</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>true</sorted>
                <value>
                    <fullName>SecurityCenter</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>SecurityCenter CV</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Tenable.io Container Security</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Tenable.io Industrial Security</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Tenable.io Vulnerability Management</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Tenable.io Web Application Scanning</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>POV_SE_Lead_Manager__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Auto-populated upon creation; you can edit value after creation</inlineHelpText>
        <label>POV SE Lead Manager</label>
        <referenceTo>User</referenceTo>
        <relationshipName>POVs1</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>POV_SE_Lead__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>SE that is managing this POV</inlineHelpText>
        <label>POV SE Lead</label>
        <lookupFilter>
            <active>true</active>
            <booleanFilter>1 OR 2</booleanFilter>
            <errorMessage>The user selected does not have a SE Role</errorMessage>
            <filterItems>
                <field>User.UserRole.Name</field>
                <operation>contains</operation>
                <value>SE -</value>
            </filterItems>
            <filterItems>
                <field>User.UserRole.Name</field>
                <operation>equals</operation>
                <value>AMER SE Management</value>
            </filterItems>
            <infoMessage>This search is filtered to SE users only</infoMessage>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>User</referenceTo>
        <relationshipName>POVs</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>POV_Stage__c</fullName>
        <externalId>false</externalId>
        <label>POV Stage</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Preparation</fullName>
                    <default>true</default>
                </value>
                <value>
                    <fullName>Active</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>At Risk</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Completed</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Not Required</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>POV_Start_Date__c</fullName>
        <externalId>false</externalId>
        <label>POV Start Date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Tech_Win_Loss__c</fullName>
        <externalId>false</externalId>
        <label>Tech Win/Loss</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Tech Win</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Tech Loss</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>POV</label>
    <nameField>
        <label>POV Name</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>POVs</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>Active_Stage_Once_Approved</fullName>
        <active>true</active>
        <errorConditionFormula>NOT(ISPICKVAL(POV_Stage__c, &quot;Preparation&quot;))
&amp;&amp;
NOT(ISPICKVAL(POV_Stage__c, &quot;Not Required&quot;))
&amp;&amp;
NOT(ISPICKVAL(Approval_Status__c, &quot;Approved&quot;))
&amp;&amp;
Opportunity_Probability__c &lt;&gt; 0.0</errorConditionFormula>
        <errorDisplayField>POV_Stage__c</errorDisplayField>
        <errorMessage>POV Stage cannot be progressed beyond Preparation until the POV has been Approved (please Submit for Approval)</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Active_Stage_Once_Probability_Hits_50</fullName>
        <active>true</active>
        <errorConditionFormula>NOT(ISPICKVAL(POV_Stage__c, &quot;Preparation&quot;))
&amp;&amp;
NOT(ISPICKVAL(POV_Stage__c, &quot;Not Required&quot;))
&amp;&amp;
(Opportunity_Probability__c &lt;= 0.49
&amp;&amp;
Opportunity_Probability__c &lt;&gt; 0.0)</errorConditionFormula>
        <errorDisplayField>POV_Stage__c</errorDisplayField>
        <errorMessage>POV Stage cannot be progressed beyond Preparation until the related Opportunity has a Probability of 50% or more</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>No_POVStartDate_When_Stage_Not_Required</fullName>
        <active>true</active>
        <errorConditionFormula>ISPICKVAL(POV_Stage__c, &quot;Not Required&quot;)
&amp;&amp;
NOT(ISBLANK(POV_Start_Date__c))</errorConditionFormula>
        <errorDisplayField>POV_Start_Date__c</errorDisplayField>
        <errorMessage>The POV Start Date field cannot be filled out when the POV Stage is Not Required</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>No_POV_Completion_Date_Until_Completed</fullName>
        <active>true</active>
        <errorConditionFormula>NOT(ISPICKVAL(POV_Stage__c, &quot;Completed&quot;))
&amp;&amp;
NOT(ISBLANK(POV_Completion_Date__c))</errorConditionFormula>
        <errorDisplayField>POV_Completion_Date__c</errorDisplayField>
        <errorMessage>The POV Completion Date field cannot be filled out until the POV Stage is Completed</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Require_POV_Completion_Date</fullName>
        <active>true</active>
        <errorConditionFormula>ISBLANK(POV_Completion_Date__c)
&amp;&amp;
ISPICKVAL(POV_Stage__c, &quot;Completed&quot;)</errorConditionFormula>
        <errorDisplayField>POV_Completion_Date__c</errorDisplayField>
        <errorMessage>When POV Stage is Completed you must enter a POV Completion Date</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Require_POV_Start_Date</fullName>
        <active>true</active>
        <errorConditionFormula>ISBLANK(POV_Start_Date__c)
&amp;&amp;
NOT(ISPICKVAL(POV_Stage__c, &quot;Preparation&quot;))
&amp;&amp;
NOT(ISPICKVAL(POV_Stage__c, &quot;Not Required&quot;))</errorConditionFormula>
        <errorDisplayField>POV_Start_Date__c</errorDisplayField>
        <errorMessage>When POV Stage advances beyond Preparation you must enter a POV Start Date</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Tech_WinLoss_Required_When_Completed</fullName>
        <active>true</active>
        <errorConditionFormula>ISPICKVAL(POV_Stage__c, &quot;Completed&quot;) 
&amp;&amp;
ISBLANK(TEXT(Tech_Win_Loss__c))</errorConditionFormula>
        <errorDisplayField>Tech_Win_Loss__c</errorDisplayField>
        <errorMessage>Tech Win/Loss is required when the POV Stage is Completed</errorMessage>
    </validationRules>
    <webLinks>
        <fullName>S_Docs</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <height>600</height>
        <linkType>url</linkType>
        <masterLabel>S-Docs</masterLabel>
        <openType>sidebar</openType>
        <protected>false</protected>
        <url>/apex/SDOC__SDCreate1?id={!POV__c.Id}&amp;Object=POV__c</url>
    </webLinks>
</CustomObject>
