<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>Adjustment__c</fullName>
        <defaultValue>false</defaultValue>
        <description>INC-41875 new Boolean field Adjustment__c</description>
        <externalId>false</externalId>
        <label>Adjustment</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>AnnualContractValue__c</fullName>
        <description>CHN-469 New object &quot;Booking&quot; to capture data from various areas within SFDC for use with Xactly tool for commissions. Annual Contract Value will be pulled from</description>
        <externalId>false</externalId>
        <label>Annual Contract Value</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Area__c</fullName>
        <description>CHN-469 New object &quot;Booking&quot; to capture data from various areas within SFDC for use with Xactly tool for commissions. Area will be pulled from the Opportunity Split</description>
        <externalId>false</externalId>
        <label>Area</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CommissionOnly__c</fullName>
        <defaultValue>false</defaultValue>
        <description>CHN-469 New object &quot;Booking&quot; to capture data from various areas within SFDC for use with Xactly tool for commissions. Default False</description>
        <externalId>false</externalId>
        <label>Commission Only</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>CustomerStatus__c</fullName>
        <description>INC-55262 New field Customer Status necessary for Xactly</description>
        <externalId>false</externalId>
        <label>Customer Status</label>
        <length>100</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Distributor__c</fullName>
        <description>CHN-469 New object &quot;Booking&quot; to capture data from various areas within SFDC for use with Xactly tool for commissions. Distributor will be pulled from the InvoiceOppLineItem populated from NetSuite</description>
        <externalId>false</externalId>
        <label>Distributor</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EndCustomerName__c</fullName>
        <description>CHN-469 New object &quot;Booking&quot; to capture data from various areas within SFDC for use with Xactly tool for commissions. End Customer Name will be pulled from the InvoicedLineItem object from NetSuite</description>
        <externalId>false</externalId>
        <label>End Customer Name</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ImplementationType__c</fullName>
        <description>INC-54219 new fields necessary for 2018 commissions integration to Xactly</description>
        <externalId>false</externalId>
        <label>Implementation Type</label>
        <length>100</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>IncentiveDate__c</fullName>
        <description>CHN-469 New object &quot;Booking&quot; to capture data from various areas within SFDC for use with Xactly tool for commissions. Incentive Date will be pulled from the object</description>
        <externalId>false</externalId>
        <label>Incentive Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>InvoiceOppLineItem__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>CHN-469 New object &quot;Booking&quot; to capture data from various areas within SFDC for use with Xactly tool for commissions.</description>
        <externalId>false</externalId>
        <label>InvoiceOppLineItem</label>
        <referenceTo>InvoiceOppLineItem__c</referenceTo>
        <relationshipLabel>Bookings</relationshipLabel>
        <relationshipName>Bookings</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>OppRegistrationType__c</fullName>
        <description>INC-54219 new fields necessary for 2018 commissions integration to Xactly</description>
        <externalId>false</externalId>
        <label>Opp Registration Type</label>
        <length>100</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>OpportunityAmount__c</fullName>
        <description>CHN-469 New object &quot;Booking&quot; to capture data from various areas within SFDC for use with Xactly tool for commissions.</description>
        <externalId>false</externalId>
        <label>Opportunity Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>OpportunityLineItemID__c</fullName>
        <description>CHN-469 New object &quot;Booking&quot; to capture data from various areas within SFDC for use with Xactly tool for commissions. Lookup to Opportunity Line Item is not possible at this time so this stores the record&apos;s ID.</description>
        <externalId>false</externalId>
        <label>Opportunity Line Item ID</label>
        <length>18</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opportunity__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>CHN-469 New object &quot;Booking&quot; to capture data from various areas within SFDC for use with Xactly tool for commissions.</description>
        <externalId>false</externalId>
        <label>Opportunity</label>
        <referenceTo>Opportunity</referenceTo>
        <relationshipLabel>Bookings</relationshipLabel>
        <relationshipName>Bookings</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>OrderCode__c</fullName>
        <description>CHN-469 New object &quot;Booking&quot; to capture data from various areas within SFDC for use with Xactly tool for commissions. Order Code will be pulled from the object</description>
        <externalId>false</externalId>
        <label>Order Code</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ProductCategory__c</fullName>
        <description>CHN-469 New object &quot;Booking&quot; to capture data from various areas within SFDC for use with Xactly tool for commissions.</description>
        <externalId>false</externalId>
        <label>Product Category</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ProductCode__c</fullName>
        <description>CHN-469 New object &quot;Booking&quot; to capture data from various areas within SFDC for use with Xactly tool for commissions. Product Code will be pulled from the Opportunity Line Item object</description>
        <externalId>false</externalId>
        <label>Product Code</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ProductFamily__c</fullName>
        <description>CHN-469 New object &quot;Booking&quot; to capture data from various areas within SFDC for use with Xactly tool for commissions.</description>
        <externalId>false</externalId>
        <label>Product Family</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ProductName__c</fullName>
        <description>CHN-469 New object &quot;Booking&quot; to capture data from various areas within SFDC for use with Xactly tool for commissions. Product Name will be pulled from the object</description>
        <externalId>false</externalId>
        <label>Product Name</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ProductType__c</fullName>
        <description>CHN-469 New object &quot;Booking&quot; to capture data from various areas within SFDC for use with Xactly tool for commissions.</description>
        <externalId>false</externalId>
        <label>Product Type</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Quantity__c</fullName>
        <description>CHN-469 New object &quot;Booking&quot; to capture data from various areas within SFDC for use with Xactly tool for commissions. Quantity will be pulled from the object</description>
        <externalId>false</externalId>
        <label>Quantity</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Region__c</fullName>
        <description>CHN-469 New object &quot;Booking&quot; to capture data from various areas within SFDC for use with Xactly tool for commissions. Region will be pulled from the Opportunity Split</description>
        <externalId>false</externalId>
        <label>Region</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>RenewalOwner__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>CHN-469 New object &quot;Booking&quot; to capture data from various areas within SFDC for use with Xactly tool for commissions. Renewal Owner will be pulled from the Opportunity</description>
        <externalId>false</externalId>
        <label>Renewal Owner</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Bookings1</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Reseller__c</fullName>
        <description>CHN-469 New object &quot;Booking&quot; to capture data from various areas within SFDC for use with Xactly tool for commissions. Distributor will be pulled from</description>
        <externalId>false</externalId>
        <label>Reseller</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Segment__c</fullName>
        <description>CHN-469 New object &quot;Booking&quot; to capture data from various areas within SFDC for use with Xactly tool for commissions. Segment will be pulled from the Opportunity Split</description>
        <externalId>false</externalId>
        <label>Segment</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SplitOwner__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>CHN-469 New object &quot;Booking&quot; to capture data from various areas within SFDC for use with Xactly tool for commissions. Split Owner will be populated based on Opportunity Split</description>
        <externalId>false</externalId>
        <label>Split Owner</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Bookings</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SplitPercentage__c</fullName>
        <description>CHN-469 New object &quot;Booking&quot; to capture data from various areas within SFDC for use with Xactly tool for commissions. Split Percentage will be based on Opportunity Split</description>
        <externalId>false</externalId>
        <label>Split Percentage</label>
        <precision>18</precision>
        <required>false</required>
        <scale>15</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>SubSegment__c</fullName>
        <description>INC-54219 new fields necessary for 2018 commissions integration to Xactly</description>
        <externalId>false</externalId>
        <label>Sub-Segment</label>
        <length>100</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Territory__c</fullName>
        <description>CHN-469 New object &quot;Booking&quot; to capture data from various areas within SFDC for use with Xactly tool for commissions. Territory will be pulled from the Opportunity Split</description>
        <externalId>false</externalId>
        <label>Territory</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Theater__c</fullName>
        <description>CHN-469 New object &quot;Booking&quot; to capture data from various areas within SFDC for use with Xactly tool for commissions. Theater will be pulled from the Opportunity</description>
        <externalId>false</externalId>
        <label>Theater</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TotalContractValue__c</fullName>
        <description>CHN-469 New object &quot;Booking&quot; to capture data from various areas within SFDC for use with Xactly tool for commissions.</description>
        <externalId>false</externalId>
        <label>Total Contract Value</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TransactionSource__c</fullName>
        <description>CHN-469 New object &quot;Booking&quot; to capture data from various areas within SFDC for use with Xactly tool for commissions.</description>
        <externalId>false</externalId>
        <label>Transaction Source</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TransactionSubType__c</fullName>
        <description>CHN-469 New object &quot;Booking&quot; to capture data from various areas within SFDC for use with Xactly tool for commissions.</description>
        <externalId>false</externalId>
        <label>Transaction Sub-Type</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TransactionType__c</fullName>
        <description>CHN-469 New object &quot;Booking&quot; to capture data from various areas within SFDC for use with Xactly tool for commissions.</description>
        <externalId>false</externalId>
        <label>Transaction Type</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Booking</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Booking Name</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Bookings</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
