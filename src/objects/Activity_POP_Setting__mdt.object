<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <fields>
        <fullName>Activity_Type__c</fullName>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Activity Type</label>
        <required>true</required>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Account Planning</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Advertising</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Balance</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Catalog</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Collateral</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Direct Mail</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Email</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Events</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Floor Days</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Funded Heads</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>List Purchase</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Merchandise</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Newsletter/Communications</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Sales Incentives</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Sales Meetings</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Sales Training</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Search Marketing</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Sponsorships</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Success Stories</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Survey</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Telemarketing</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Webinars</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Filter_Logic__c</fullName>
        <description>Logical grouping of POP items, for example,  1 AND (2 OR 3 OR 4).  This should only be populated on the first line number for a given Activity Type.  If this is not populated for an Activity Type, the default logic is AND between each POP item.</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Filter Logic</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Line_Number__c</fullName>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Line Number</label>
        <precision>2</precision>
        <required>true</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Proof_of_Performance__c</fullName>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Proof of Performance</label>
        <required>true</required>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>true</sorted>
                <value>
                    <fullName>Ad screenshot</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Agenda</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Collateral piece</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Copy of call script</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Copy of communication</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Copy of email content</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Copy of final story</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Copy of final survey</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Copy of invitation</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Copy of mailer</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Copy of partner invoice (in Local Currency)</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Copy of placement</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Copy of script</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Event email invitation</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>List of attendees</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>List of names purchased</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>List of prizes and quantities</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>List of winners</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Photo</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Picture of merchandise</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Please contact Tenable for Proof of Performance requirments</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Proof of Tenable representation</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Sponsorship agreement</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Summary of activity</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Summary of incentive</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Summary of purpose</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Summary of Tenable representation</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Tenable presentation</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Video clip</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Qualifier__c</fullName>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Qualifier</label>
        <required>false</required>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>featuring Tenable logo</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>featuring proof of Tenable representation</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>illustrating proof of Tenable representation</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Activity POP Setting</label>
    <pluralLabel>Activity POP Settings</pluralLabel>
    <visibility>Public</visibility>
</CustomObject>
