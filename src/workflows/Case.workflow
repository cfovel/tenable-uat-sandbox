<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Case</fullName>
        <description>Case: Notification to Owner of reopened case</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>support@tenable.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/SUPPORTCaseescalationnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>Case_Internal_Notification_of_P1_Case_APAC_Case</fullName>
        <description>Case: Internal Notification of P1 Case APAC Case</description>
        <protected>false</protected>
        <recipients>
            <recipient>gjackson@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sterry@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@tenable.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Generated_Emails/Case_Case_P1_Updated_HTML</template>
    </alerts>
    <alerts>
        <fullName>Case_Internal_Notification_of_P1_Case_Closed</fullName>
        <description>Case: Internal Notification of P1 Case Closed</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Customer Success Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales Engineer</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Technical Support Acct Mgr (TSAM)</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Territory Manager (TM)</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Case_P1_Notifications</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>noreply@tenable.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Generated_Emails/Case_Case_P1_Updated_HTML</template>
    </alerts>
    <alerts>
        <fullName>Case_Internal_Notification_of_Priority_Change_to_P1</fullName>
        <description>Case: Internal Notification of Priority Change to P1</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Customer Success Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales Engineer</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Technical Support Acct Mgr (TSAM)</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Territory Manager (TM)</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Case_P1_Notifications</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>noreply@tenable.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Generated_Emails/Case_Case_P1_Updated_HTML</template>
    </alerts>
    <alerts>
        <fullName>Case_Notification_Closed_Licensing_Case_Updated</fullName>
        <description>Case: Notification Closed Licensing Case Updated</description>
        <protected>false</protected>
        <recipients>
            <recipient>jturpin@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Generated_Emails/Case_Notification_Closed_License_Case_Updated</template>
    </alerts>
    <alerts>
        <fullName>Case_Notification_to_Customer_Case_On_Hold</fullName>
        <description>Case: Notification to Customer Case On Hold</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@tenable.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Generated_Emails/Case_Notification_to_Customer_of_On_Hold</template>
    </alerts>
    <alerts>
        <fullName>Case_Notification_to_Customer_Case_On_Hold_14_Days</fullName>
        <description>Case: Notification to Customer Case On Hold - 14 Days</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@tenable.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Generated_Emails/Case_Notification_to_Customer_of_On_Hold_14_Days</template>
    </alerts>
    <alerts>
        <fullName>Case_Notification_to_Customer_Case_On_Hold_21_Days</fullName>
        <description>Case: Notification to Customer Case On Hold - 21 Days</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@tenable.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Generated_Emails/Case_Notification_to_Customer_of_On_Hold_21_Days</template>
    </alerts>
    <alerts>
        <fullName>Case_Notification_to_Customer_Case_Opened</fullName>
        <description>Case: Notification to Customer Case Opened</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@tenable.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Generated_Emails/Case_Notification_to_Customer_Case_Opened</template>
    </alerts>
    <alerts>
        <fullName>Case_Notification_to_Customer_Case_Pending_Customer</fullName>
        <description>Case: Notification to Customer Case Pending Customer</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@tenable.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Generated_Emails/Case_Notification_to_Customer_of_Pending_Customer</template>
    </alerts>
    <alerts>
        <fullName>Case_Notification_to_Customer_Case_Suspended</fullName>
        <description>Case: Notification to Customer Case Suspended</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@tenable.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Generated_Emails/Case_Notification_to_Customer_of_Suspended_Case</template>
    </alerts>
    <alerts>
        <fullName>Case_Notification_to_Customer_Case_Suspended_14_Day_Reminder</fullName>
        <description>Case: Notification to Customer Case Suspended 14 Day Reminder</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@tenable.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Generated_Emails/Case_Notification_to_Customer_of_Suspended_Case_14_Days</template>
    </alerts>
    <alerts>
        <fullName>Case_Notification_to_Customer_Case_Suspended_5_Day_Reminder</fullName>
        <description>Case: Notification to Customer Case Suspended 5 Day Reminder</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@tenable.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Generated_Emails/Case_Notification_to_Customer_of_Suspended_Case_5_Days</template>
    </alerts>
    <alerts>
        <fullName>Case_Notification_to_Customer_Case_Suspended_7_Day_Reminder</fullName>
        <description>Case: Notification to Customer Case Suspended 7 Day Reminder</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@tenable.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Generated_Emails/Case_Notification_to_Customer_of_Suspended_Case_7_Days</template>
    </alerts>
    <alerts>
        <fullName>Case_Notification_to_Customer_Case_Suspended_8_Day_Reminder</fullName>
        <description>Case: Notification to Customer Case Suspended 8 Day Reminder</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@tenable.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Generated_Emails/Case_Notification_to_Customer_of_Suspended_Case_8_Days</template>
    </alerts>
    <alerts>
        <fullName>Case_Notification_to_Customer_of_Closing_Case</fullName>
        <description>Case: Notification to Customer Case Closed</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@tenable.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Generated_Emails/Case_Notification_to_Customer_Case_Closed</template>
    </alerts>
    <alerts>
        <fullName>Case_Notification_to_Customer_of_New_Case_Comment2</fullName>
        <description>Case: Notification to Customer of New Case Comment</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@tenable.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Generated_Emails/Case_Notification_to_Customer_Case_Updated</template>
    </alerts>
    <alerts>
        <fullName>Case_Notification_to_Customer_of_Pending_Case_Close</fullName>
        <description>Case: Notification to Customer of Pending Close</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@tenable.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Generated_Emails/Case_Notification_to_Customer_of_Pending_Close</template>
    </alerts>
    <alerts>
        <fullName>Cases_Notification_of_Partner_Case_Opened</fullName>
        <description>Cases: Notification of Partner Case Opened</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>channelops@tenable.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Generated_Emails/Case_Notification_to_Partner_Case_Opened</template>
    </alerts>
    <alerts>
        <fullName>Notification_of_Integration_Case_Approval</fullName>
        <description>Notification of Integration Case Approval</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>support@tenable.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_Approval_Received</template>
    </alerts>
    <alerts>
        <fullName>Notification_of_Integration_Case_Rejection</fullName>
        <description>Notification of Integration Case Rejection</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>support@tenable.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_Approval_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Send_GetFeedback_cSat_Survey_to_Contact_Email</fullName>
        <description>Send GetFeedback cSat Survey to Contact Email</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@tenable.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GetFeedback/GetFeedback_Ticket_Close_cSat</template>
    </alerts>
    <fieldUpdates>
        <fullName>Case_Set_Account_Management_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Account_Management_Case</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Case: Set Account Management Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Set_Customer_Care_Record_Type</fullName>
        <description>Updates record type to Customer Care</description>
        <field>RecordTypeId</field>
        <lookupValue>Customer_Care_Case</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Case: Set Customer Care Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Set_License_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>License_Case</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Case: Set License Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Set_Partner_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Partner_Case</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Case: Set Partner Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Set_Support_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Standard_Case</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Case: Set Support Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Uncheck_Undeliverable_Checkbox</fullName>
        <description>Sets the Undeliverable Contact Field to False.</description>
        <field>Undeliverable_Contact__c</field>
        <literalValue>0</literalValue>
        <name>Case: Uncheck Undeliverable Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Case_ACAS_Checkbox_to_True</fullName>
        <description>When a case is assigned to the Support - ACAS queue, update the checkbox to true.</description>
        <field>ACAS__c</field>
        <literalValue>1</literalValue>
        <name>Case: Update Case ACAS Checkbox to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Case_Pending_Close_Date</fullName>
        <description>Updates the Case Pending Close Date to &quot;Now&quot;</description>
        <field>Date_Time_Pending_Close__c</field>
        <formula>NOW()</formula>
        <name>Case: Update Case Pending Close Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Case_Pending_Customer_Date</fullName>
        <description>Updates the Case Pending Customer Date to &quot;Now&quot;</description>
        <field>Date_Time_Pending_Customer__c</field>
        <formula>NOW()</formula>
        <name>Case: Update Case Pending Customer Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Case_Res_Detail_No_Closure</fullName>
        <description>Updates the Case Resolution Detail to No Closure Confirmation</description>
        <field>Resolution_Detail__c</field>
        <literalValue>No Closure Confirmation</literalValue>
        <name>Case: Update Case Res Detail No Closure</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Case_Working_Date</fullName>
        <description>Updates the Case Working Date to &quot;Now&quot;</description>
        <field>Date_Time_Working__c</field>
        <formula>NOW()</formula>
        <name>Case: Update Case Working Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_First_Status_Change_Date</fullName>
        <description>Updates the First Status Change Date to NOW()</description>
        <field>First_Status_Change_Date__c</field>
        <formula>NOW()</formula>
        <name>Case: Update First Status Change Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_GF_cSat_Survey_Sent</fullName>
        <field>GF_cSat_Survey_Sent__c</field>
        <formula>NOW()</formula>
        <name>Case: Update GF cSat Survey Sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_LMS_Customer_ID</fullName>
        <description>Updates the LMS Customer ID to match the ID on the related Account.</description>
        <field>LMS_Customer_ID__c</field>
        <formula>Account.LMS_Customer_ID__c</formula>
        <name>Case: Update LMS Customer ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Record_Type_Standard</fullName>
        <description>When a new case is created on the Community update to Standard record type</description>
        <field>RecordTypeId</field>
        <lookupValue>Standard_Case</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Case: Update Record Type Standard</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Resolution_Non_Responsive</fullName>
        <description>Updates case Resolution to &quot;Customer Non-Responsive&quot;</description>
        <field>Reason</field>
        <literalValue>Customer Non-Responsive</literalValue>
        <name>Case: Update Resolution Non-Responsive</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Status_to_Closed</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Case: Update Status to Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Status_to_New</fullName>
        <field>Status</field>
        <literalValue>New</literalValue>
        <name>Case: Update Status to New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Status_to_Open</fullName>
        <description>Update the Status field to Open</description>
        <field>Status</field>
        <literalValue>Open</literalValue>
        <name>Case: Update Status to Open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Status_to_Pending_Close</fullName>
        <field>Status</field>
        <literalValue>Pending Close</literalValue>
        <name>Case: Update Status to Pending Close</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Status_to_Suspended</fullName>
        <field>Status</field>
        <literalValue>Suspended</literalValue>
        <name>Case: Update Status to Suspended</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Community_Reassign_case_ownership</fullName>
        <description>Field update to change owner from the community user that created the case to a Support queue.</description>
        <field>OwnerId</field>
        <lookupValue>Support_Standard</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Community - Reassign case ownership</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Status_to_Submitted</fullName>
        <field>Status</field>
        <literalValue>Submitted</literalValue>
        <name>Set Approval Status to Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_Status_to_Approved</fullName>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>Set Case Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_Status_to_Rejected</fullName>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>Set Case Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Origin_to_Portal</fullName>
        <description>Updates the &quot;Case Origin&quot; field to &quot;Portal&quot;</description>
        <field>Origin</field>
        <literalValue>Portal</literalValue>
        <name>Update Case Origin to Portal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Case%3A APAC Priority 1 Case Closed Notification</fullName>
        <actions>
            <name>Case_Internal_Notification_of_P1_Case_APAC_Case</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notify distribution list when P1 Case Closes for APAC Accounts</description>
        <formula>AND( RecordType.Name=&apos;Standard Case&apos;, ISPICKVAL(Priority,&apos;P1 - Critical&apos;), ISCHANGED(IsClosed),  IsClosed = True, Account.Theater__c = &apos;APAC&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A APAC Priority 1 Case Created Notification</fullName>
        <actions>
            <name>Case_Internal_Notification_of_P1_Case_APAC_Case</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Standard Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>P1 - Critical</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Theater__c</field>
            <operation>equals</operation>
            <value>APAC</value>
        </criteriaItems>
        <description>Notify distribution list when P1 Case is created for APAC Accounts</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A APAC Priority Changed to P1 Notification</fullName>
        <actions>
            <name>Case_Internal_Notification_of_P1_Case_APAC_Case</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notify distribution list when Case Priority changes to P1 for APAC Accounts</description>
        <formula>AND( RecordType.Name =&apos;Standard Case&apos;, ISCHANGED(Priority), ISPICKVAL(Priority, &apos;P1 - Critical&apos;), IsClosed = False, Account.Theater__c = &apos;APAC&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A License Type to Support Type</fullName>
        <actions>
            <name>Case_Set_Support_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Change Record Type when we flip a Case from Licensing to Support</description>
        <formula>AND(
OR(
CONTAINS(Owner:Queue.QueueName,&quot;Standard&quot;),
CONTAINS(Owner:Queue.QueueName,&quot;PCI&quot;),
CONTAINS(Owner:Queue.QueueName,&quot;ACAS&quot;)
),
CONTAINS(RecordType.Name,&quot;License&quot;)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Notification Pending Customer</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending Customer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Standard Case</value>
        </criteriaItems>
        <description>When Case is &quot;Pending Customer&quot;, send reminder email 4 days later. After 7 days, update to &quot;Suspended&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Notification_to_Customer_Case_Pending_Customer</name>
                <type>Alert</type>
            </actions>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Update_Status_to_Suspended</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case%3A Notification Pending Customer to Suspended Case</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending Customer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Standard Case</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Notification_to_Customer_Case_Suspended</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Case_Update_Status_to_Suspended</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case%3A Notification to Customer Case Closed</fullName>
        <actions>
            <name>Case_Notification_to_Customer_of_Closing_Case</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Standard Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Undeliverable_Contact__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>When Case is Closed, Send notification to customer</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Notification to Customer Case On Hold</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>On Hold</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Standard Case</value>
        </criteriaItems>
        <description>When case status is set to &quot;On Hold&quot; send notification email to customer 1 week later, if case is still opened and has not be updated a week later, close the case.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Notification_to_Customer_Case_On_Hold</name>
                <type>Alert</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Update_Case_Res_Detail_No_Closure</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Case_Update_Resolution_Non_Responsive</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Case_Update_Status_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case%3A Notification to Customer Case On Hold %28NEW%29</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>On Hold</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Standard Case</value>
        </criteriaItems>
        <description>Sends notification 7 days after status is set, sends reminder every 7 days. After 28 days, closes the case.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Notification_to_Customer_Case_On_Hold</name>
                <type>Alert</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Notification_to_Customer_Case_On_Hold_14_Days</name>
                <type>Alert</type>
            </actions>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Notification_to_Customer_Case_On_Hold_21_Days</name>
                <type>Alert</type>
            </actions>
            <timeLength>21</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Update_Status_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>28</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case%3A Notification to Customer Case Opened</fullName>
        <actions>
            <name>Case_Notification_to_Customer_Case_Opened</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Standard Case,License Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>notContain</operation>
            <value>Your request has been logged,After Hours Alert,hre E-Mail an IT-Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notEqual</operation>
            <value>aprovisionament@cesicat.cat</value>
        </criteriaItems>
        <description>When  a new support case is opened, send an email notification to the customer</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Notification to Customer Suspended Case</fullName>
        <actions>
            <name>Case_Notification_to_Customer_Case_Suspended</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Suspended</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Standard Case</value>
        </criteriaItems>
        <description>Sends notification when status is set to suspended, sends reminder on the 7th and 14th day after suspension. After 21 days, closes the case.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Notification_to_Customer_Case_Suspended_7_Day_Reminder</name>
                <type>Alert</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Notification_to_Customer_Case_Suspended_14_Day_Reminder</name>
                <type>Alert</type>
            </actions>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Update_Status_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>21</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case%3A Notification to Customer Suspended Case %28NEW%29</fullName>
        <actions>
            <name>Case_Notification_to_Customer_Case_Suspended</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Suspended</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Standard Case</value>
        </criteriaItems>
        <description>Sends notification when status is set to suspended, sends reminder on the 5th and 8th day after suspension. After 10 days, closes the case.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Notification_to_Customer_Case_Suspended_5_Day_Reminder</name>
                <type>Alert</type>
            </actions>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Notification_to_Customer_Case_Suspended_8_Day_Reminder</name>
                <type>Alert</type>
            </actions>
            <timeLength>8</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Update_Status_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case%3A Notification to Customer of Pending Close %28New%29</fullName>
        <actions>
            <name>Case_Notification_to_Customer_of_Pending_Case_Close</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending Close</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Standard Case</value>
        </criteriaItems>
        <description>When case is updated to &quot;Pending Close&quot;, send notification to customer that case will close in 4 days. 4 Days later, close the case.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Update_Status_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case%3A Notification to Partner Case Opened</fullName>
        <actions>
            <name>Cases_Notification_of_Partner_Case_Opened</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Case</value>
        </criteriaItems>
        <description>When  a new support case is opened, send an email notification to the customer</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Priority 1 Case Closed Notification</fullName>
        <actions>
            <name>Case_Internal_Notification_of_P1_Case_Closed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notify distribution list when P1 Case Closes</description>
        <formula>AND( RecordType.Name=&apos;Standard Case&apos;, ISPICKVAL(Priority,&apos;P1 - Critical&apos;), ISCHANGED(IsClosed),  IsClosed = True )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Priority 1 Case Created Notification</fullName>
        <actions>
            <name>Case_Internal_Notification_of_Priority_Change_to_P1</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Standard Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>P1 - Critical</value>
        </criteriaItems>
        <description>Notify distribution list when P1 Case is created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Priority Changed to P1 Notification</fullName>
        <actions>
            <name>Case_Internal_Notification_of_Priority_Change_to_P1</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notify distribution list when Case Priority changes to P1</description>
        <formula>AND( RecordType.Name =&apos;Standard Case&apos;, ISCHANGED(Priority), ISPICKVAL(Priority, &apos;P1 - Critical&apos;), IsClosed = False )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Reopened Customer Community Case</fullName>
        <actions>
            <name>Case</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Identifies cases that are reopened by a customer community user within 30 days of when the case was originally closed.</description>
        <formula>NOT(ISNEW()) &amp;&amp; ISCHANGED(IsClosed) &amp;&amp; IsClosed = False &amp;&amp; ISPICKVAL($User.UserType,&quot;High Volume Portal&quot;) &amp;&amp; Record_Type_Name__c = &quot;Standard&quot; &amp;&amp; NOW()- ClosedDate  &lt;=  30</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Set Status to Open Upon an Edit for Training Cases</fullName>
        <actions>
            <name>Case_Update_Status_to_Open</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a Training Case is first edited, change the Status to Open</description>
        <formula>LastModifiedById &lt;&gt; &quot;00560000001T5Tz&quot;  &amp;&amp;  NOT(ISNEW())  &amp;&amp;  RecordTypeId = &quot;012600000001H9b&quot;  &amp;&amp;  ISCHANGED(LastModifiedDate)  &amp;&amp;  NOT(ISPICKVAL(Status, &quot;Escalated&quot;))  &amp;&amp; NOT(ISPICKVAL(Status, &quot;Pending Customer&quot;))  &amp;&amp;  NOT(ISPICKVAL(Status, &quot;Closed&quot;))  &amp;&amp;  NOT(ISPICKVAL(Status, &quot;Partner&quot;)) &amp;&amp;  NOT(ISPICKVAL(Status, &quot;Customer Updated&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Uncheck Undeliverable Checkbox</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Undeliverable_Contact__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>When Case is marked as an Undeliverable Contact, update the checkbox to false 1 hour later. Allows the case to be re-opened if contact is out of office and later responds.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Uncheck_Undeliverable_Checkbox</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case%3A Update ACAS Checkbox to True</fullName>
        <actions>
            <name>Case_Update_Case_ACAS_Checkbox_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Support - ACAS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ACAS__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <description>When a Case is created and assigned to the ACAS queue, update the ACAS Checkbox to true.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Update Case First Status Change Date</fullName>
        <actions>
            <name>Case_Update_First_Status_Change_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.First_Status_Change_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>When the case status no longer is &quot;New&quot; and the First Status Change field is blank - update the First Status Change field date/time.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Update Case Origin to Portal</fullName>
        <actions>
            <name>Community_Reassign_case_ownership</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Case_Origin_to_Portal</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserType</field>
            <operation>equals</operation>
            <value>High Volume Portal</value>
        </criteriaItems>
        <description>Identifies cases created by high volume portal users and updates the Case Origin to &quot;Portal&quot;.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Update Case Pending Close Date Field</fullName>
        <actions>
            <name>Case_Update_Case_Pending_Close_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending Close</value>
        </criteriaItems>
        <description>When Case status = Pending Close, update the Case Pending Close Date to Now.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Update Case Pending Customer Date Field</fullName>
        <actions>
            <name>Case_Update_Case_Pending_Customer_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending Customer</value>
        </criteriaItems>
        <description>When Case status = Pending Customer, update the Case Pending Customer Date to Now.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Update Case Working Date Field</fullName>
        <actions>
            <name>Case_Update_Case_Working_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Working</value>
        </criteriaItems>
        <description>When Case status = Working, update the Case Working Date to Now.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Update LMS Customer ID</fullName>
        <actions>
            <name>Case_Update_LMS_Customer_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.LMS_Customer_ID__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CreatedById</field>
            <operation>notEqual</operation>
            <value>Automation User</value>
        </criteriaItems>
        <description>When Case does not have an LMS customer ID, update the LMS Customer ID to match the Account.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Update Record Type and Status-Account Management</fullName>
        <actions>
            <name>Case_Set_Account_Management_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Update_Status_to_New</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the case Owner is updated to the Account Management Queue, update the Record Type and Status.</description>
        <formula>AND(ISCHANGED( OwnerId ), 
Owner:Queue.QueueName = &quot;Support - Account Management&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Update Record Type and Status-Customer Care</fullName>
        <actions>
            <name>Case_Set_Customer_Care_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Update_Status_to_New</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the case Owner is updated to the Customer Care Queue, update the Record Type and Status.</description>
        <formula>AND(
ISCHANGED(
OwnerId ),
OR(
Owner:Queue.QueueName = &quot;Support - Customer Care&quot;,
Owner:Queue.QueueName = &quot;AMER - CSM&quot;,
Owner:Queue.QueueName = &quot;APAC - CSM&quot;,
Owner:Queue.QueueName = &quot;FED - CSM&quot;,
Owner:Queue.QueueName = &quot;EMEA - CSM&quot;)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Update Record Type and Status-License</fullName>
        <actions>
            <name>Case_Set_License_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Update_Status_to_New</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the case Owner is updated to the License Queue, update the Record Type and Status.</description>
        <formula>AND(ISCHANGED( OwnerId ), 
Owner:Queue.QueueName = &quot;Support - Licensing&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Update Record Type and Status-Partner</fullName>
        <actions>
            <name>Case_Set_Partner_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Update_Status_to_New</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the case Owner is updated to the Partner Queue, update the Record Type and Status.</description>
        <formula>AND(ISCHANGED( OwnerId ), 
Owner:Queue.QueueName = &quot;Support - Partners&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Update Record Type and Status-Standard</fullName>
        <actions>
            <name>Case_Set_Support_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Update_Status_to_New</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the case Owner is updated to the Standard Queue, update the Record Type and Status.</description>
        <formula>AND(ISCHANGED( OwnerId ), 
Owner:Queue.QueueName = &quot;Support - Standard&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Update Record Type of Community Case</fullName>
        <actions>
            <name>Case_Update_Record_Type_Standard</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Community Case</value>
        </criteriaItems>
        <description>When a case is created by a community user update the Record Type to Standard.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Update Suspended Case to Closed</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Suspended</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Standard Case</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Update_Case_Res_Detail_No_Closure</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Case_Update_Resolution_Non_Responsive</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Case_Update_Status_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case%3A Update to License Type</fullName>
        <actions>
            <name>Case_Set_License_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Change Record Type when we flip a Case to Licensing</description>
        <formula>AND(CONTAINS(Owner:Queue.QueueName,&quot;Licensing&quot;),
NOT(CONTAINS(RecordType.DeveloperName,&quot;License&quot;))
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>GetFeedback%3A Case Closed</fullName>
        <actions>
            <name>Send_GetFeedback_cSat_Survey_to_Contact_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Case_Update_GF_cSat_Survey_Sent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Closed = True, Survey has not been sent in previous 7 days, owner is a user, not a queue, CloseDate is today, Standard Case</description>
        <formula>AND((IsClosed == True),  	ISNULL( GF_cSat_Survey_Sent__c ),  	OR( ISBLANK(Contact.GetFeedback_cSat_Last_Requested_On__c) , 		(TODAY() - DATEVALUE(Contact.GetFeedback_cSat_Last_Requested_On__c)) &gt; 7),  	BEGINS( OwnerId , &quot;005&quot;),  	DATEVALUE(ClosedDate) = TODAY(), 	CONTAINS(RecordType.Name,&quot;Standard&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
