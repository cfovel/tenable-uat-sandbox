<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Account_2_hr_Notification_Escalation_Reminder</fullName>
        <ccEmails>support-principals@tenable.com</ccEmails>
        <description>Account: 2 hr Notification Escalation Reminder</description>
        <protected>false</protected>
        <recipients>
            <recipient>acacopardo@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>afirman@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>bknott@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>dhampton@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>gcain@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jcox@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jravenscroft@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rmoody@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tkisaberth@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Account_Email_Templates/Account_Second_Notification_of_Escalated_Case</template>
    </alerts>
    <alerts>
        <fullName>Account_Notification_Account_De_Escalated</fullName>
        <ccEmails>support-principals@tenable.com</ccEmails>
        <description>Account: Notification Account De-Escalated</description>
        <protected>false</protected>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Customer Success Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Inside Sales Rep (ISR)</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales Engineer</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>acacopardo@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>afirman@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>bking@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>bknott@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>dhampton@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>gcain@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jcox@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jravenscroft@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mbein@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rmoody@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tkisaberth@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Escalated_By__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Account_Email_Templates/Account_Notification_of_Account_De_Escalation</template>
    </alerts>
    <alerts>
        <fullName>Account_Notification_Escalation_Engineer_Assigned</fullName>
        <ccEmails>support-principals@tenable.com</ccEmails>
        <description>Account: Notification Escalation Engineer Assigned</description>
        <protected>false</protected>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Customer Success Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Inside Sales Rep (ISR)</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales Engineer</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>acacopardo@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>afirman@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>bking@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>bknott@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>dhampton@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>gcain@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jcox@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jravenscroft@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mbein@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rmoody@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tkisaberth@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Escalated_By__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Account_Email_Templates/Account_Notification_of_Escalated_Case_Esc_Engineer_Assigned</template>
    </alerts>
    <alerts>
        <fullName>Account_Notification_of_Escalated_Account</fullName>
        <ccEmails>support-principals@tenable.com</ccEmails>
        <description>Account: Notification of Escalated Account</description>
        <protected>false</protected>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Customer Success Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Inside Sales Rep (ISR)</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales Engineer</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>AMER_Sales_Director_Customer_Success</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Manager_Customer_Success_Manager_EMEA</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>acacopardo@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>afirman@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>bking@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>bknott@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>dhampton@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>gcain@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jcox@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jravenscroft@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mbein@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rmoody@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tkisaberth@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Escalated_By__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Account_Email_Templates/Account_First_Notification_of_Escalated_Case</template>
    </alerts>
    <alerts>
        <fullName>Account_Notify_E_Staff</fullName>
        <description>Account: Notify E-Staff</description>
        <protected>false</protected>
        <recipients>
            <recipient>jorr@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Account_Email_Templates/Account_Notify_E_Staff</template>
    </alerts>
    <fieldUpdates>
        <fullName>Account_Update_Date_Time_Escalated_NOW</fullName>
        <description>Updates the Date/Time Escalated field to &quot;NOW&quot;</description>
        <field>DateTime_Escalated__c</field>
        <formula>NOW()</formula>
        <name>Account: Update Date/Time Escalated NOW</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Update_Effective_Date</fullName>
        <field>Effective_Date__c</field>
        <formula>IF(
MONTH( TODAY() ) = 12,
DATE( YEAR( TODAY()), 12, 31 ),
DATE( YEAR( TODAY()), MONTH ( TODAY()) + 1, 1 ))</formula>
        <name>Account: Update Effective Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Account%3A Escalation 2 Hr Reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.Escalated__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Escalation_Engineer__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>If an Escalation Engineer hasn&apos;t been assigned within 2 hours of the Account being escalated, send a reminder.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Account_2_hr_Notification_Escalation_Reminder</name>
                <type>Alert</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Account%3A Escalation Engineer Assigned</fullName>
        <actions>
            <name>Account_Notification_Escalation_Engineer_Assigned</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Escalation_Engineer__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>When an Escalation Engineer is assigned, send notification.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notification Account Escalated</fullName>
        <actions>
            <name>Account_Notification_of_Escalated_Account</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Escalated__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Notification that an account has been escalated</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notification of Account De-Escalation</fullName>
        <actions>
            <name>Account_Notification_Account_De_Escalated</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When an account is de-escalated, send notification.</description>
        <formula>AND(ISCHANGED(Escalated__c ), 
Escalated__c = FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify E-Staff</fullName>
        <actions>
            <name>Account_Notify_E_Staff</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Notify_E_Staff__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>account escalation email alert to E-Staff (#SR-84392)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Owner Effective Date Stamp</fullName>
        <actions>
            <name>Account_Update_Effective_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When an account owner is updated, stamp the Effective Date field with the first of the following month.</description>
        <formula>ISCHANGED( OwnerId )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Update Date%2FTime Escalated</fullName>
        <actions>
            <name>Account_Update_Date_Time_Escalated_NOW</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Escalated__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>When an account is updated, update the Date/time Escalated to Now.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>a1I60000007k5uSEAQ_Purge</fullName>
        <active>true</active>
        <description>Account.a1I60000007k5uSEAQ_Purge</description>
        <formula>OR(ISCHANGED(BillingStreet),ISCHANGED(BillingCity),ISCHANGED(BillingState),ISCHANGED(BillingPostalCode),ISCHANGED(BillingCountry))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>a1L60000004T8r7EAC_Purge</fullName>
        <active>true</active>
        <description>Account.a1L60000004T8r7EAC_Purge</description>
        <formula>OR(ISCHANGED(BillingStreet),ISCHANGED(BillingCity),ISCHANGED(BillingState),ISCHANGED(BillingPostalCode),ISCHANGED(BillingCountry))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
