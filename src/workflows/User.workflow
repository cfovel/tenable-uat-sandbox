<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Disable_User</fullName>
        <field>IsActive</field>
        <literalValue>0</literalValue>
        <name>Disable User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Turn_off_disable_request</fullName>
        <field>Future_Disable_User__c</field>
        <literalValue>0</literalValue>
        <name>Turn off disable request</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>User%3A Terminate User</fullName>
        <active>true</active>
        <criteriaItems>
            <field>User.Future_Disable_User__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Disable_User</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Turn_off_disable_request</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>User.Disable_Date_Time__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
