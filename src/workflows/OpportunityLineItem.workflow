<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>OppProd_Set_Gross_Price</fullName>
        <field>Gross_Price__c</field>
        <formula>Gross_Price_calc__c</formula>
        <name>OppProd: Set Gross Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Line_Update_Negative_Line_Trade_In_T</fullName>
        <description>CHN-960 Any negative line should have Trade-In checked - Passed to NS and impacts ARM</description>
        <field>Trade_In__c</field>
        <literalValue>1</literalValue>
        <name>Opp Line Update Negative Line Trade In T</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Order_Category_to_New</fullName>
        <field>Order_Category__c</field>
        <literalValue>New</literalValue>
        <name>Update Order Category to New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Opp Line Update Negative Line Trade In True</fullName>
        <actions>
            <name>Opp_Line_Update_Negative_Line_Trade_In_T</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 4) AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>OpportunityLineItem.TotalPrice</field>
            <operation>lessThan</operation>
            <value>USD 0</value>
        </criteriaItems>
        <criteriaItems>
            <field>OpportunityLineItem.Trade_In__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>OpportunityLineItem.Quantity</field>
            <operation>lessThan</operation>
            <value>0</value>
        </criteriaItems>
        <description>CHN-960 Any negative line should have Trade-In checked - Passed to NS and impacts ARM</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OppProd%3A Populate Gross Price</fullName>
        <actions>
            <name>OppProd_Set_Gross_Price</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( 
ISNEW(), 
NOT( Opportunity.IsWon ) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Order Category to New</fullName>
        <actions>
            <name>Update_Order_Category_to_New</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Stage_Number__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Stage_Number__c</field>
            <operation>lessThan</operation>
            <value>10</value>
        </criteriaItems>
        <criteriaItems>
            <field>OpportunityLineItem.Order_Category__c</field>
            <operation>equals</operation>
            <value>Add-on</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.IsWon</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <description>INC-50926 Order Category should be only New and Renewal -- Legacy quote tool uses Add-On value for selection of products so temporary workflow rule needed to change Add-On to New at the OLI</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
