<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Opps_AMER_Big_Deal_Alert</fullName>
        <description>Opps: AMER Big Deal Alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>bking@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>dkorba@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jhuffard@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lserlenga@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mkirby@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pcrutchfield@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rderaison@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rgula@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>svintz@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tkisaberth@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Email_Templates/Opp_Big_Deal_Alert</template>
    </alerts>
    <alerts>
        <fullName>Opps_AMER_Big_Deal_Alert_Loss</fullName>
        <description>Opps: AMER Big Deal Alert: Loss</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>erandall@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jhuffard@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jnegron@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rderaison@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>svintz@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tkisaberth@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Email_Templates/Opp_Big_Deal_Alert_Loss</template>
    </alerts>
    <alerts>
        <fullName>Opps_AMER_Central_Big_Deal_Alert</fullName>
        <description>Opps: AMER Central Big Deal Alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>AMER_Sales_Director_Central</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>bking@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>dkorba@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jhuffard@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lserlenga@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mkirby@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pcrutchfield@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rderaison@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rgula@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>svintz@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tkisaberth@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Email_Templates/Opp_Big_Deal_Alert</template>
    </alerts>
    <alerts>
        <fullName>Opps_AMER_East_Big_Deal_Alert</fullName>
        <description>Opps: AMER East Big Deal Alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>AMER_Sales_Director_East</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>bking@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>dkorba@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jhuffard@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lserlenga@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mkirby@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pcrutchfield@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rderaison@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rgula@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>svintz@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tkisaberth@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Email_Templates/Opp_Big_Deal_Alert</template>
    </alerts>
    <alerts>
        <fullName>Opps_AMER_West_Big_Deal_Alert</fullName>
        <description>Opps: AMER West Big Deal Alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>AMER_Sales_Director_West</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>bking@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>dkorba@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jhuffard@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lserlenga@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mkirby@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pcrutchfield@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rderaison@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rgula@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>svintz@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tkisaberth@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Email_Templates/Opp_Big_Deal_Alert</template>
    </alerts>
    <alerts>
        <fullName>Opps_APAC_Big_Deal_Alert</fullName>
        <description>Opps: APAC Big Deal Alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>gjackson@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jhuffard@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ngallash@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rderaison@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>svintz@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tkisaberth@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Email_Templates/Opp_Big_Deal_Alert</template>
    </alerts>
    <alerts>
        <fullName>Opps_APAC_Big_Deal_Alert_Loss</fullName>
        <description>Opps: APAC Big Deal Alert: Loss</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>erandall@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>gjackson@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jhuffard@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jnegron@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rderaison@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>svintz@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tkisaberth@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Email_Templates/Opp_Big_Deal_Alert_Loss</template>
    </alerts>
    <alerts>
        <fullName>Opps_EMEA_Big_Deal_Alert</fullName>
        <description>Opps: EMEA Big Deal Alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>dcummins@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ibignell@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jhuffard@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rderaison@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>svintz@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tkisaberth@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Email_Templates/Opp_Big_Deal_Alert</template>
    </alerts>
    <alerts>
        <fullName>Opps_EMEA_Big_Deal_Alert_Loss</fullName>
        <description>Opps: EMEA Big Deal Alert: Loss</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>dcummins@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>erandall@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jhuffard@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jnegron@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rderaison@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>svintz@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tkisaberth@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Email_Templates/Opp_Big_Deal_Alert_Loss</template>
    </alerts>
    <alerts>
        <fullName>Opps_Notification_of_Booking_Approval</fullName>
        <description>Opps: Notification of Booking Approval</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>rpaper@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tavakian@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tbandini@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Booking_Approval_Received</template>
    </alerts>
    <alerts>
        <fullName>Opps_Notification_of_Booking_Approval_Carlene_Shores</fullName>
        <description>Opps: Notification of Booking Approval Carlene Shores</description>
        <protected>false</protected>
        <recipients>
            <recipient>cshores@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jmcmurtrey@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mtrexler@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rstewart@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tglinka@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Booking_Approval_Received</template>
    </alerts>
    <alerts>
        <fullName>Opps_Notification_of_Booking_Approval_Joel_Fossett</fullName>
        <description>Opps: Notification of Booking Approval Joel Fossett</description>
        <protected>false</protected>
        <recipients>
            <recipient>jfossett@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Booking_Approval_Received</template>
    </alerts>
    <alerts>
        <fullName>Opps_Notification_of_Booking_Rejected</fullName>
        <description>Opps: Notification of Booking Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Booking_Approval_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Opps_Notification_of_Booking_Rejected_Carlene_Shores</fullName>
        <description>Opps: Notification of Booking Rejected Carlene Shores</description>
        <protected>false</protected>
        <recipients>
            <recipient>cshores@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rstewart@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tglinka@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Booking_Approval_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Opps_Notification_of_RFP</fullName>
        <ccEmails>proposals@tenable.com</ccEmails>
        <description>Opps: Notification of RFP</description>
        <protected>false</protected>
        <recipients>
            <recipient>vbokhari@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Email_Templates/Opp_Notification_of_new_RFP</template>
    </alerts>
    <alerts>
        <fullName>Opps_Notification_of_Sales_Order_Rejected</fullName>
        <description>Opps: Notification of Sales Order Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>cdunn@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>clevin@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kdavis@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kluyten@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lregan@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>shilbert@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Email_Templates/Opp_Sales_Order_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Reclass_Status_Notification</fullName>
        <description>Reclass Status Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>New_Business_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Renewal_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>reclass@tenable.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Opportunity_Email_Templates/Reclass_Status_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Change_Transaction_Source_to_Var_Portal</fullName>
        <description>Changes picklist value to Var Portal</description>
        <field>Transaction_Source__c</field>
        <literalValue>VAR Portal</literalValue>
        <name>Change Transaction Source to Var Portal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contracted_Checked</fullName>
        <description>Checks Contracted Box</description>
        <field>SBQQ__Contracted__c</field>
        <literalValue>1</literalValue>
        <name>Contracted Checked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Naming_Convention</fullName>
        <field>Name</field>
        <formula>&quot;Renewal - &quot; &amp; Account.Name &amp; &quot;: &quot; &amp; Account.LMS_Customer_ID__c &amp; &quot; - &quot; &amp; TEXT(SBQQ__RenewedContract__r.EndDate)</formula>
        <name>Opp Naming Convention</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Update_Approval_Date_to_NOW</fullName>
        <description>Sets booking Approval Date to the current day&apos;s date/time</description>
        <field>Booking_Approval_Date__c</field>
        <formula>NOW()</formula>
        <name>Opp: Update Approval Date to NOW</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opps_Clear_Approval_Date</fullName>
        <field>Booking_Approval_Date__c</field>
        <name>Opps: Clear Approval Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opps_Clear_NS_Reject_Reason</fullName>
        <field>NetSuite_Sales_Order_Reason__c</field>
        <name>Opps: Clear NS Reject Reason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opps_Clear_NS_Rejection_Details</fullName>
        <field>Netsuite_Rejection_Details__c</field>
        <name>Opps: Clear NS Rejection Details</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opps_Clear_NS_Sales_Order_Rejected</fullName>
        <field>NetSuite_Sales_Order_Rejected__c</field>
        <literalValue>0</literalValue>
        <name>Opps: Clear NS Sales Order Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opps_Expire_Deal_Registration</fullName>
        <field>Registered_Deal__c</field>
        <literalValue>0</literalValue>
        <name>Opps: Expire Deal Registration</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opps_Set_NS_Status_Pending</fullName>
        <field>NetSuite_Sales_Order_Status__c</field>
        <formula>&apos;Pending Approval&apos;</formula>
        <name>Opps: Set NS Status Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opps_Set_Renewal_Reclass_Reqd_to_True</fullName>
        <field>Renewal_Reclass_Required__c</field>
        <literalValue>1</literalValue>
        <name>Opps: Set Renewal Reclass Reqd to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opps_Update_Active_Opportunity_Date</fullName>
        <description>Updates Active Opportunity Date to Today.</description>
        <field>Active_Opportunity_Date__c</field>
        <formula>TODAY()</formula>
        <name>Opps: Update Active Opportunity Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opps_Update_Active_Opportunity_Date_Null</fullName>
        <description>Update Active Opportunity date to Null</description>
        <field>Active_Opportunity_Date__c</field>
        <name>Opps:Update Active Opportunity Date Null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opps_Update_Close_Date_to_Today</fullName>
        <field>CloseDate</field>
        <formula>TODAY()</formula>
        <name>Opps: Update Close Date to Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opps_Update_Stage_to_Closed_Won</fullName>
        <description>Updates the Opportunity stage value to Closed-Won</description>
        <field>StageName</field>
        <literalValue>Closed - Won</literalValue>
        <name>Opps: Update Stage to Closed-Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opps_Update_Stage_to_Pending_Booking</fullName>
        <field>StageName</field>
        <literalValue>Pending Booking</literalValue>
        <name>Opps: Update Stage to Pending Booking</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opps_Update_Stage_to_Waiting_for_PO</fullName>
        <description>Updates the Opportunity stage to Waiting for Purchase Order</description>
        <field>StageName</field>
        <literalValue>Waiting for Purchase Order</literalValue>
        <name>Opps: Update Stage to Waiting for PO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opps_Update_Submission_Date_to_NOW</fullName>
        <description>Sets the Booking Submission Date to current day&apos;s Date/time</description>
        <field>Booking_Submission_Date__c</field>
        <formula>NOW()</formula>
        <name>Opps: Update Submission Date to NOW</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Oppty_Update_Tracked_Steps</fullName>
        <description>Updates the Tracked Steps field every time Next Steps is changed</description>
        <field>Tracked_Steps__c</field>
        <formula>TEXT(TODAY())&amp; &quot;: &quot; &amp;  NextStep &amp; BR() &amp; PRIORVALUE( Tracked_Steps__c )</formula>
        <name>Oppty Update Tracked Steps</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Save_Lost_Date</fullName>
        <field>Lost_Date__c</field>
        <formula>TODAY()</formula>
        <name>Save Lost Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Contracted_to_False</fullName>
        <field>SBQQ__Contracted__c</field>
        <literalValue>0</literalValue>
        <name>Set Contracted to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_Change_Date_Stamp</fullName>
        <description>INC-33448 Automated field on the opportunity where we datestamp when the stage name changes</description>
        <field>Latest_Stage_Change_Date__c</field>
        <formula>Today()</formula>
        <name>Stage Change Date Stamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Clear Contract Checkbox on Creation</fullName>
        <actions>
            <name>Set_Contracted_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When an opp is cloned, this workflow sets the &quot;Contracted&quot; checkbox to FALSE.</description>
        <formula>Amount &gt; 0</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Closed Won - Contracted</fullName>
        <actions>
            <name>Contracted_Checked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed - Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Transaction_Source__c</field>
            <operation>equals</operation>
            <value>Standard</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Amount</field>
            <operation>greaterOrEqual</operation>
            <value>USD 0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Primary_Quote_Approval_Status_sbqq__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <description>Checks the contracted box when the opportunity stage is set to &quot;closed won&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Notification of Sales Order Rejected</fullName>
        <actions>
            <name>Opps_Notification_of_Sales_Order_Rejected</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.NetSuite_Sales_Order_Rejected__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Send email notification to Deal Desk when Sales Order is rejected in NetSuite</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Notification of new RFP</fullName>
        <actions>
            <name>Opps_Notification_of_RFP</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Active_RFP_RFI__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Notification to Carlene Shores of Opp Approved</fullName>
        <actions>
            <name>Opps_Notification_of_Booking_Approval_Carlene_Shores</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When a member of Charlene Shores team has a booking approved, send notification to her.</description>
        <formula>AND(ISPICKVAL(PRIORVALUE(StageName ), &quot;Pending Booking&quot;), 
ISPICKVAL(StageName , &quot;Closed - Won&quot;), 
OR (Owner.ManagerId = &quot;005600000023Q5z&quot;,
Owner.ManagerId = &quot;00530000000gecG&quot;,
Owner.ManagerId = &quot;00560000000mQUI&quot;,
Owner.ManagerId = &quot;00560000004ADz3&quot;,
Owner.ManagerId = &quot;00560000004c0A4&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Notification to Carlene Shores of Opp Rejected</fullName>
        <actions>
            <name>Opps_Notification_of_Booking_Rejected_Carlene_Shores</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When a member of Charlene Shores team has a booking rejected, send notification to her.</description>
        <formula>AND(ISPICKVAL(PRIORVALUE(StageName ), &quot;Pending Booking&quot;),  ISPICKVAL(StageName , &quot;Purchasing&quot;),  OR (Owner.ManagerId = &quot;005600000023Q5z&quot;, Owner.ManagerId = &quot;00530000000gecG&quot;, Owner.ManagerId = &quot;00560000000mQUI&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Notification to Joel Fossett of Opp Approved</fullName>
        <actions>
            <name>Opps_Notification_of_Booking_Approval_Joel_Fossett</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When customer care has a booking approved, send notification to Joel Fossett.</description>
        <formula>AND(ISPICKVAL(PRIORVALUE(StageName ), &quot;Pending Booking&quot;),  ISPICKVAL(StageName , &quot;Closed - Won&quot;),   Owner.UserRole.Name = &quot;Customer Care&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity%3A IsPortalOrder_Set Transaction Source</fullName>
        <actions>
            <name>Change_Transaction_Source_to_Var_Portal</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.IsPortalOrder__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>If IsPortalOrder = True, change Transaction Source to &quot;VarPortal&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opps%3A Expire Deal Reg</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Deal_Registration_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Opps_Expire_Deal_Registration</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.Deal_Registration_Date__c</offsetFromField>
            <timeLength>120</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Opps%3A Expire Registered Deal</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Deal_Registration_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Opps_Expire_Deal_Registration</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.Deal_Registration_Expiration_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Opps%3A Save Lost Date</fullName>
        <actions>
            <name>Save_Lost_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(
OR(
ISPICKVAL(StageName, &quot;Closed - Lost&quot;),
ISPICKVAL(StageName, &quot;Closed - No Decision&quot;)
),
AND(
NOT(ISPICKVAL(PRIORVALUE(StageName), &quot;Closed - Lost&quot;)),
NOT(ISPICKVAL(PRIORVALUE(StageName), &quot;Closed - No Decision&quot;))
)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opps%3A Update Active Opportunity Date</fullName>
        <actions>
            <name>Opps_Update_Active_Opportunity_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Probability</field>
            <operation>greaterOrEqual</operation>
            <value>10</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Active_Opportunity_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Populates the value of the &quot;Active Opportunity Date &quot; field to TODAY when the probability of the Opportunity is greater than or equal to 10%, and the date is not blank.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opps%3A Update Renewal Reclass Required</fullName>
        <actions>
            <name>Opps_Set_Renewal_Reclass_Reqd_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Renewal Reclassification</value>
        </criteriaItems>
        <description>Set the Renewal Reclass Required checkbox to True, when the record type is Renewal Reclass</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opps%3AUpdate Active Opportunity Date to Null</fullName>
        <actions>
            <name>Opps_Update_Active_Opportunity_Date_Null</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Active_Opportunity_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Probability</field>
            <operation>lessThan</operation>
            <value>10</value>
        </criteriaItems>
        <description>When the Opp Status is moved back to a probability less than 10%, update Active Opportunity date to null.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Oppty Update Tracked Steps</fullName>
        <actions>
            <name>Oppty_Update_Tracked_Steps</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the Tracked Steps field every time the Next Steps field is changed</description>
        <formula>OR( ISCHANGED( NextStep ), AND(ISNEW(), NOT(ISBLANK(NextStep))) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Renewal Opportunity Naming Convention</fullName>
        <actions>
            <name>Opp_Naming_Convention</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.SBQQ__Renewal__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Sets the Opportunity Name on renewal opps.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Stage Change Date Stamp</fullName>
        <actions>
            <name>Stage_Change_Date_Stamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>INC-33448 Automated field on the opportunity where we datestamp when the stage name changes</description>
        <formula>ISCHANGED(StageName )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
