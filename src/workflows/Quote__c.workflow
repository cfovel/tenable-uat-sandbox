<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Quote_Approval_Denied</fullName>
        <description>Quote Approval Denied</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Quotes/Quotes_Approval_Denied</template>
    </alerts>
    <alerts>
        <fullName>Quote_Approval_Received</fullName>
        <description>Quote Approval Received</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Quotes/Quotes_Approval_Received</template>
    </alerts>
    <fieldUpdates>
        <fullName>Clear_Approved_Date</fullName>
        <field>Date_Time_Approved__c</field>
        <name>Clear Approved Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Submitted_Date</fullName>
        <field>Date_Time_Submitted__c</field>
        <name>Clear Submitted Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Set_back_to_Draft</fullName>
        <field>Approval_Stage__c</field>
        <literalValue>Draft</literalValue>
        <name>Quote: Set back to Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quotes_Approve_Quote</fullName>
        <field>Approval_Stage__c</field>
        <literalValue>Approved</literalValue>
        <name>Quotes: Approve Quote</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quotes_Approved_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Approved</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Quotes: Approved Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quotes_Set_to_Denied</fullName>
        <field>Approval_Stage__c</field>
        <literalValue>Denied</literalValue>
        <name>Quotes: Set to Denied</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quotes_Set_to_In_Review</fullName>
        <field>Approval_Stage__c</field>
        <literalValue>In Review</literalValue>
        <name>Quotes: Set to In Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Save_Appoved_Date</fullName>
        <field>Date_Time_Approved__c</field>
        <formula>Now()</formula>
        <name>Save Appoved Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Save_Submitted_Date</fullName>
        <field>Date_Time_Submitted__c</field>
        <formula>Now()</formula>
        <name>Save Submitted Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Distributor_Nessus_Discount</fullName>
        <field>Nessus_Discount__c</field>
        <formula>IF(Distributor__c &lt;&gt; &apos;&apos; &amp;&amp; Nessus_List__c &lt;&gt; 0, 1- ( Distributor_Net_Price__c / Nessus_List__c ),IF(Partner__c != &apos;&apos; &amp;&amp; Nessus_List__c &lt;&gt; 0, 1 - ( Nessus_Net__c / Nessus_List__c) 
,0))</formula>
        <name>Update Distributor Nessus Discount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Nessus_Auth_Level</fullName>
        <field>Distributor_Nessus_Auth_Level__c</field>
        <formula>IF(Distributor__c != &apos;&apos;, 
IF( 
FIND(&quot;%&quot;,TEXT(Distributor__r.Nessus_Authorization_Level__c)) &gt; 0, 
VALUE(MID(TEXT(Distributor__r.Nessus_Authorization_Level__c),FIND(&quot;%&quot;,TEXT(Distributor__r.Nessus_Authorization_Level__c))-2,2))/100.0, 
0.00), 
IF(Partner__c != &apos;&apos;,IF(FIND(&quot;%&quot;,TEXT(Partner__r.Nessus_Authorization_Level__c)) &gt; 0, 
VALUE(MID(TEXT(Partner__r.Nessus_Authorization_Level__c),FIND(&quot;%&quot;,TEXT(Partner__r.Nessus_Authorization_Level__c))-2,2))/100.0, 
0.00),0))</formula>
        <name>Update Nessus Auth Level</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SC_Auth</fullName>
        <field>Security_Center_Auth_Level__c</field>
        <formula>IF(Distributor__c != &apos;&apos;, 
IF( 
FIND(&quot;%&quot;,TEXT(Distributor__r.Security_Center_Authorization_Level__c)) &gt; 0, 
VALUE(MID(TEXT(Distributor__r.Security_Center_Authorization_Level__c),FIND(&quot;%&quot;,TEXT(Distributor__r.Security_Center_Authorization_Level__c))-2,2))/100.0, 
0.00), 
IF(Partner__c != &apos;&apos;,IF(FIND(&quot;%&quot;,TEXT(Partner__r.Security_Center_Authorization_Level__c)) &gt; 0, 
VALUE(MID(TEXT(Partner__r.Security_Center_Authorization_Level__c),FIND(&quot;%&quot;,TEXT(Partner__r.Security_Center_Authorization_Level__c))-2,2))/100.0, 
0.00),0))</formula>
        <name>Update SC Auth</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SC_Discount_Field</fullName>
        <field>Security_Center_Discount__c</field>
        <formula>IF(Distributor__c &lt;&gt; &apos;&apos; &amp;&amp; SecurityCenter_List__c &lt;&gt; 0, 1- ( Distributor_SC_Net_Price__c / SecurityCenter_List__c ),IF(Partner__c != &apos;&apos; &amp;&amp; SecurityCenter_List__c &lt;&gt; 0, 1 - (SecurityCenter_Net__c / SecurityCenter_List__c) 
,0))</formula>
        <name>Update SC Discount Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Nessus Auth</fullName>
        <actions>
            <name>Update_Nessus_Auth_Level</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Opportunity__c != NULL</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Nessus Discount Workflow</fullName>
        <actions>
            <name>Update_Distributor_Nessus_Discount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Opportunity__c != NULL</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update SC Auth Level</fullName>
        <actions>
            <name>Update_SC_Auth</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Opportunity__c != NULL</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update SC Discount Workflow</fullName>
        <actions>
            <name>Update_SC_Discount_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Opportunity__c != NULL</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
