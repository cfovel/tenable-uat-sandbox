<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Leads_Notification_to_Acct_Mgr_of_Deal_Registration_Acceptance</fullName>
        <description>Leads: Notification to Account Manager of Deal Registration Acceptance</description>
        <protected>false</protected>
        <recipients>
            <field>TM_Managers_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Partner_Community_Templates/Leads_Approval_Received</template>
    </alerts>
    <alerts>
        <fullName>Leads_Notification_to_Acct_Mgr_of_Deal_Registration_Rejection</fullName>
        <description>Leads: Notification to Account Manager of Deal Registration Rejection</description>
        <protected>false</protected>
        <recipients>
            <field>TM_Managers_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Partner_Community_Templates/Leads_Approval_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Leads_Notification_to_CM</fullName>
        <description>Leads: Notification to CM</description>
        <protected>false</protected>
        <recipients>
            <field>CM_Managers_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Channel_Managers_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Partner_Community_Templates/Leads_Approval_Request</template>
    </alerts>
    <alerts>
        <fullName>Leads_Notification_to_Partner_of_Deal_Registration_Acceptance</fullName>
        <description>Leads: Notification to Partner of Deal Registration Acceptance</description>
        <protected>false</protected>
        <recipients>
            <field>CM_Managers_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Channel_Managers_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Primary_Sales_Email_Address__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>TM_Managers_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>TM_Deal_Reg_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Partner_Community_Templates/Leads_Approval_Received</template>
    </alerts>
    <alerts>
        <fullName>Leads_Notification_to_Partner_of_Deal_Registration_Rejection</fullName>
        <description>Leads: Notification to Partner of Deal Registration Rejection</description>
        <protected>false</protected>
        <recipients>
            <field>CM_Managers_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Channel_Managers_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Primary_Sales_Email_Address__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>TM_Managers_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>TM_Deal_Reg_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Partner_Community_Templates/Leads_Approval_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Leads_Notification_to_TM_Approvers_Manager</fullName>
        <description>Leads: Notification to TM Approvers Manager</description>
        <protected>false</protected>
        <recipients>
            <field>TM_Managers_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Partner_Community_Templates/Leads_Notify_TM_Manager_of_Approval_Request</template>
    </alerts>
    <alerts>
        <fullName>Leads_Notify_Internal_to_perform_conversion_Central</fullName>
        <description>Leads: Notify Internal to perform conversion - Central</description>
        <protected>false</protected>
        <recipients>
            <recipient>AMER_Lead_Assigner_s</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>cshores@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jhughes@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Partner_Community_Templates/Leads_Approval_Received_Internal_Notification</template>
    </alerts>
    <alerts>
        <fullName>Leads_Notify_Internal_to_perform_conversion_East</fullName>
        <description>Leads: Notify Internal to perform conversion - East</description>
        <protected>false</protected>
        <recipients>
            <recipient>AMER_Lead_Assigner_s</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>cshores@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jhughes@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Partner_Community_Templates/Leads_Approval_Received_Internal_Notification</template>
    </alerts>
    <alerts>
        <fullName>Leads_Notify_Internal_to_perform_conversion_LATAM</fullName>
        <description>Leads: Notify Internal to perform conversion - LATAM</description>
        <protected>false</protected>
        <recipients>
            <recipient>AMER_Lead_Assigner_s</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>jhughes@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mmoreira@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Partner_Community_Templates/Leads_Approval_Received_Internal_Notification</template>
    </alerts>
    <alerts>
        <fullName>Leads_Notify_Internal_to_perform_conversion_Mid_Atlantic</fullName>
        <description>Leads: Notify Internal to perform conversion - Mid-Atlantic</description>
        <protected>false</protected>
        <recipients>
            <recipient>AMER_Lead_Assigner_s</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>cshores@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jhughes@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Partner_Community_Templates/Leads_Approval_Received_Internal_Notification</template>
    </alerts>
    <alerts>
        <fullName>Leads_Notify_Internal_to_perform_conversion_South_Central</fullName>
        <description>Leads: Notify Internal to perform conversion - South Central</description>
        <protected>false</protected>
        <recipients>
            <recipient>AMER_Lead_Assigner_s</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>cshores@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jhughes@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Partner_Community_Templates/Leads_Approval_Received_Internal_Notification</template>
    </alerts>
    <alerts>
        <fullName>Leads_Notify_Internal_to_perform_conversion_Southeast</fullName>
        <description>Leads: Notify Internal to perform conversion - Southeast</description>
        <protected>false</protected>
        <recipients>
            <recipient>AMER_Lead_Assigner_s</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>cshores@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jhughes@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Partner_Community_Templates/Leads_Approval_Received_Internal_Notification</template>
    </alerts>
    <alerts>
        <fullName>Leads_Notify_Internal_to_perform_conversion_West</fullName>
        <description>Leads: Notify Internal to perform conversion - West</description>
        <protected>false</protected>
        <recipients>
            <recipient>AMER_Lead_Assigner_s</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>cshores@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jhughes@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Partner_Community_Templates/Leads_Approval_Received_Internal_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Change_Owner</fullName>
        <field>OwnerId</field>
        <lookupValue>Marketing_Invalid</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Change Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Acceptance_Date</fullName>
        <field>Partner_Acceptance_Date__c</field>
        <name>Clear Acceptance Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Deal_Reg_Elig</fullName>
        <field>Deal_Reg_Eligibility__c</field>
        <literalValue>0</literalValue>
        <name>Clear Deal Reg Elig</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Rejection_Date</fullName>
        <field>Partner_Reject_Date__c</field>
        <name>Clear Rejection Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Disqualified_Lead</fullName>
        <field>Status</field>
        <literalValue>Invalid</literalValue>
        <name>Disqualified Lead</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Update_Rejected_by_TM</fullName>
        <field>Rejected_by_TM__c</field>
        <literalValue>1</literalValue>
        <name>Lead: Update Rejected by TM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Update_Rejected_by_TM_to_False</fullName>
        <field>Rejected_by_TM__c</field>
        <literalValue>0</literalValue>
        <name>Lead: Update Rejected by TM to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leads_Assigned_to_SDR_Date</fullName>
        <field>Assigned_to_SDR_Date__c</field>
        <formula>today()</formula>
        <name>Leads: Assigned to SDR Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leads_Bad_Data_Ownership_change</fullName>
        <field>OwnerId</field>
        <lookupValue>Marketing_Bad_Data</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Leads: Bad Data Ownership change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leads_Clear_PID</fullName>
        <field>PID__c</field>
        <name>Leads: Clear PID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leads_Contacted_by_Partner_Date</fullName>
        <field>Contacted_By_Partner_Date__c</field>
        <formula>today()</formula>
        <name>Leads: Contacted by Partner Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leads_Created_Source_Marketing</fullName>
        <field>Created_Source__c</field>
        <literalValue>Marketing</literalValue>
        <name>Leads: Created Source - Marketing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leads_Created_Source_Partner</fullName>
        <field>Created_Source__c</field>
        <literalValue>Partner</literalValue>
        <name>Leads: Created Source - Partner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leads_Created_Source_Sales</fullName>
        <field>Created_Source__c</field>
        <literalValue>Sales</literalValue>
        <name>Leads: Created Source - Sales</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leads_Date_Lead_Status_Changed</fullName>
        <field>Date_Lead_Status_Changed__c</field>
        <formula>TODAY()</formula>
        <name>Leads: Date Lead Status Changed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leads_Dead_Reassignment</fullName>
        <field>OwnerId</field>
        <lookupValue>Marketing</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Leads: Dead Reassignment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leads_Force_Reassignment</fullName>
        <field>FireAssignmentRules__c</field>
        <literalValue>1</literalValue>
        <name>Leads: Force Reassignment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leads_MM_First_MQL_Date</fullName>
        <field>MM_First_MQL_Date__c</field>
        <formula>MQL_Date__c</formula>
        <name>Leads: MM - First MQL Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leads_MM_First_Working_Date</fullName>
        <field>MM_First_Working_Date__c</field>
        <formula>TODAY()</formula>
        <name>Leads: MM - First Working Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leads_Mark_as_Closed</fullName>
        <field>Status</field>
        <literalValue>Recycle</literalValue>
        <name>Leads: Mark as Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leads_Nessus_Enterprise_Cloud_Expr_Date</fullName>
        <field>Date_Nessus_Ent_Cloud_Eval_Expires__c</field>
        <formula>Today() + 15</formula>
        <name>Leads: Nessus Enterprise Cloud Expr Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leads_Nessus_Enterprise_Expiration_Date</fullName>
        <field>Date_Nessus_Ent_Eval_Expires__c</field>
        <formula>Today() + 15</formula>
        <name>Leads: Nessus Enterprise Expiration Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leads_Nessus_Pro_Eval_Expr_Date</fullName>
        <field>Date_Nessus_Eval_Expires__c</field>
        <formula>Today() + 7</formula>
        <name>Leads: Nessus Pro Eval Expr Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leads_Partner_Booked_Date</fullName>
        <field>Partner_Booked_Date__c</field>
        <formula>today()</formula>
        <name>Leads: Partner Booked Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leads_Partner_Lead_Record_Type_Change</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Partner_Leads</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Leads: Partner Lead Record Type Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leads_Partner_Lead_Status_Changed</fullName>
        <field>Date_Partner_Lead_Status_Changed__c</field>
        <formula>TODAY()</formula>
        <name>Leads: Partner Lead Status Changed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leads_Partner_Pass_to_Tracking_Date</fullName>
        <field>Partner_Pass_to_Date__c</field>
        <formula>Today()</formula>
        <name>Leads: Partner Pass-to Tracking Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leads_Populate_Converted_From_Lead</fullName>
        <field>Converted_From_Lead__c</field>
        <literalValue>1</literalValue>
        <name>Leads: Populate Converted From Lead</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leads_Recycle_Assignment</fullName>
        <field>OwnerId</field>
        <lookupValue>Marketing_Recycle</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Leads: Recycle Assignment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leads_Recycle_Lead</fullName>
        <field>Status</field>
        <literalValue>Recycle</literalValue>
        <name>Leads: Recycle Lead</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leads_Returned_to_SDR</fullName>
        <field>Returned_to_SDR_Date__c</field>
        <formula>today()</formula>
        <name>Leads: Returned to SDR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leads_Save_Contact_Date</fullName>
        <field>Contact_Date__c</field>
        <formula>Today()</formula>
        <name>Leads: Save Contact Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leads_Save_SAL_Date</fullName>
        <field>SAL_Date__c</field>
        <formula>Today()</formula>
        <name>Leads: Save SAL Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leads_Save_SQL_Date</fullName>
        <field>SQL_Date__c</field>
        <formula>Today()</formula>
        <name>Leads: Save SQL Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leads_Set_Default_Lead_Status</fullName>
        <field>Status</field>
        <literalValue>Open</literalValue>
        <name>Leads: Set Default Lead Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leads_Standard_Lead_Record_Type_Change</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Standard_Leads</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Leads: Standard Lead Record Type Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leads_Status_Detail_Partner_30_Day_SLA</fullName>
        <field>Bad_Data_Reasons__c</field>
        <literalValue>Partner 30 Day SLA</literalValue>
        <name>Leads: Status Detail Partner 30 Day SLA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leads_Status_Detail_Partner_7_Day_SLA</fullName>
        <description>Updates status detail value to &quot;Partner 7 Day SLA&quot;</description>
        <field>Bad_Data_Reasons__c</field>
        <literalValue>Partner 7 Day SLA</literalValue>
        <name>Leads: Status Detail Partner 7 Day SLA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leads_Support_Community_Request_Record</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Support_Community_Request</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Leads: Support Community Request Record</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leads_eComm_Booked</fullName>
        <field>eComm_Booked_Date__c</field>
        <formula>today()</formula>
        <name>Leads: eComm Booked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MALatitude_c_Purge</fullName>
        <field>MALatitude__c</field>
        <name>Lead.MALatitude_c_Purge</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MALongitude_c_Purge</fullName>
        <field>MALongitude__c</field>
        <name>Lead.MALongitude_c_Purge</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mark_Lead_as_Accepted</fullName>
        <field>Status</field>
        <literalValue>Accepted</literalValue>
        <name>Mark Lead as Accepted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mark_Lead_as_Rejected</fullName>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>Mark Lead as Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mark_Lead_as_Submitted</fullName>
        <field>Status</field>
        <literalValue>Submitted</literalValue>
        <name>Mark Lead as Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RainKing_Assign_to_Mktng</fullName>
        <field>OwnerId</field>
        <lookupValue>Marketing</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>RainKing Assign to Mktng</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Lead_Status</fullName>
        <field>Status</field>
        <literalValue>Not Contacted</literalValue>
        <name>Reset Lead Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Save_Acceptance_Date</fullName>
        <field>Partner_Acceptance_Date__c</field>
        <formula>Today()</formula>
        <name>Save Acceptance Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Save_Reject_Date</fullName>
        <field>Partner_Reject_Date__c</field>
        <formula>Today()</formula>
        <name>Save Reject Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Save_Submission_Date</fullName>
        <field>Partner_Submit_Date__c</field>
        <formula>Today()</formula>
        <name>Save Submission Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CM_Managers_Email</fullName>
        <field>CM_Managers_Email__c</field>
        <formula>Owner:User.Channel_Manager__r.Manager.Email</formula>
        <name>Set CM Managers Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Channel_Managers_Email</fullName>
        <description>Set Channel Manager&apos;s Email to Partner Account owner&apos;s Channel Manager&apos;s email address.</description>
        <field>Channel_Managers_Email__c</field>
        <formula>Owner:User.Channel_Manager__r.Email</formula>
        <name>Set Channel Managers Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_SDRs_Email</fullName>
        <field>SDRs_Email__c</field>
        <formula>Owner:User.Email</formula>
        <name>Set SDRs Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_SDRs_Name</fullName>
        <field>SDRs_Name__c</field>
        <formula>Owner:User.FirstName  &amp; &quot; &quot; &amp; Owner:User.LastName</formula>
        <name>Set SDRs Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Leads%3A Assigned to SDR</fullName>
        <actions>
            <name>Leads_Assigned_to_SDR_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Partner_Lead_Status__c</field>
            <operation>equals</operation>
            <value>Assigned to SDR,In EMEA Regional Queue</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A Bad Data Reassignment</fullName>
        <actions>
            <name>Leads_Bad_Data_Ownership_change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Bad Data</value>
        </criteriaItems>
        <description>Reassigns leads marked as Bad Data to a special queue</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A Contacted by Partner</fullName>
        <actions>
            <name>Leads_Contacted_by_Partner_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Contacted_by_Partner__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A Convert Notification - Central</fullName>
        <actions>
            <name>Leads_Notify_Internal_to_perform_conversion_Central</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Accepted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Sales_Area__c</field>
            <operation>equals</operation>
            <value>AMER</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Sales_Region__c</field>
            <operation>equals</operation>
            <value>North Central,South Central,Central</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A Convert Notification - East</fullName>
        <actions>
            <name>Leads_Notify_Internal_to_perform_conversion_East</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Accepted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Sales_Area__c</field>
            <operation>equals</operation>
            <value>AMER</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Sales_Region__c</field>
            <operation>equals</operation>
            <value>Mid-Atlantic,Northeast,East,Southeast</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A Convert Notification - LATAM</fullName>
        <actions>
            <name>Leads_Notify_Internal_to_perform_conversion_LATAM</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Accepted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Sales_Area__c</field>
            <operation>equals</operation>
            <value>LATAM</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A Convert Notification - West</fullName>
        <actions>
            <name>Leads_Notify_Internal_to_perform_conversion_West</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Accepted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Sales_Area__c</field>
            <operation>equals</operation>
            <value>AMER</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Sales_Region__c</field>
            <operation>equals</operation>
            <value>West</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A Created Source - Marketing</fullName>
        <actions>
            <name>Leads_Created_Source_Marketing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>(CreatedById = &quot;00560000001T5Tz&quot; &amp;&amp; NOT(BEGINS(PID__c, &quot;005&quot;))) ||  (CreatedById = &quot;00560000001T5Tz&quot; &amp;&amp; ISBLANK(PID__c)) ||  (CONTAINS(CreatedBy.Profile.Name, &quot;Marketing Advanced User&quot;)) ||  (CONTAINS(CreatedBy.Profile.Name, &quot;Marketing User&quot;)) ||  (CreatedById = &quot;00560000006W7gq&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A Created Source - Partner</fullName>
        <actions>
            <name>Leads_Created_Source_Partner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>(CreatedBy.ProfileId = &quot;00e600000017VUd&quot;) || (CreatedById = &quot;00560000001T5Tz&quot; &amp;&amp; BEGINS(PID__c, &quot;005&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A Created Source - Sales</fullName>
        <actions>
            <name>Leads_Created_Source_Sales</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>CONTAINS(CreatedBy.Profile.Name, &quot;ISR&quot;) || CONTAINS(CreatedBy.Profile.Name, &quot;Tenable - Sales&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A Dead Reassignment</fullName>
        <actions>
            <name>Leads_Dead_Reassignment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Dead</value>
        </criteriaItems>
        <description>Changes a partner lead marked as Dead to the Marketing queue so they don&apos;t see it anymore.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A Force Reassignment</fullName>
        <actions>
            <name>Leads_Force_Reassignment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Marketing - Reassignments</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>startsWith</operation>
            <value>00G60000002CaCk</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A Lead Status Changed</fullName>
        <actions>
            <name>Leads_Date_Lead_Status_Changed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>updates Date Lead Status Changed field</description>
        <formula>ISCHANGED( Status )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A MDF Clawback</fullName>
        <active>false</active>
        <formula>IsConverted = False &amp;&amp; 
Outreach_Exception__c = False &amp;&amp; 
Is_Partner_Owned__c = True &amp;&amp; 
MM_First_Campaign_Type__c = &quot;MDF Marketing&quot; &amp;&amp; 
(ISPICKVAL(Status, &quot;Open&quot;) || ISPICKVAL(Status, &quot;Not Contacted&quot;) || ISPICKVAL(Status, &quot;Suspect&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Lead.Partner_Took_Ownership_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Leads_Force_Reassignment</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Lead.Partner_Took_Ownership_Date__c</offsetFromField>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Leads%3A MM - First MQL Date</fullName>
        <actions>
            <name>Leads_MM_First_MQL_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( ISNULL( MM_First_MQL_Date__c ) , NOT(ISNULL( MQL_Date__c)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A MM - First Working Date</fullName>
        <actions>
            <name>Leads_MM_First_Working_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISNULL(MM_First_Working_Date__c) &amp;&amp; ISPICKVAL(Status, &quot;Working&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A Mark Closed an Opted-out Lead</fullName>
        <actions>
            <name>Leads_Mark_as_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.DoNotCall</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.HasOptedOutOfEmail</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A Nessus Enterprise Cloud Expiration Date</fullName>
        <actions>
            <name>Leads_Nessus_Enterprise_Cloud_Expr_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Nessus_Ent_Cloud_Eval_Start_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A Nessus Enterprise Expiration Date</fullName>
        <actions>
            <name>Leads_Nessus_Enterprise_Expiration_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Nessus_Ent_Eval_Start_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A Nessus Professional Expiration Date</fullName>
        <actions>
            <name>Leads_Nessus_Pro_Eval_Expr_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Date_Nessus_Eval_Activation_Sent__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A Partner Booked</fullName>
        <actions>
            <name>Leads_Partner_Booked_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Partner_Lead_Status__c</field>
            <operation>equals</operation>
            <value>Partner Booked</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A Partner Lead Record Type</fullName>
        <actions>
            <name>Leads_Partner_Lead_Record_Type_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reset_Lead_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Change the record type when a lead is flipped to a partner</description>
        <formula>AND(
ISCHANGED(Lead_Owner__c),
Lead_Owner__r.LastName = &quot;Partner&quot;
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A Partner Lead SLA%27s</fullName>
        <active>true</active>
        <description>Set-up Time-based workflow for 3 Business Days after a lead has been flipped if it&apos;s still not contact attempted at all</description>
        <formula>AND( Is_Partner_Owned__c, NOT(Exclude_from_Workflow__c), NOT(ISNULL(Partner_Pass_to_Date__c)), ISPICKVAL(Status, &quot;Not Contacted&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Leads_Recycle_Lead</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Leads_Status_Detail_Partner_7_Day_SLA</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Lead.Partner_Pass_to_Date__c</offsetFromField>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Leads%3A Partner Lead SLA%27s %2830 days%29</fullName>
        <active>true</active>
        <description>Set-up Time-based workflow for 30 Business Days after a lead has been attempted but still attempted</description>
        <formula>AND( Is_Partner_Owned__c, NOT(Exclude_from_Workflow__c), NOT(ISNULL(Partner_Pass_to_Date__c)), CONTAINS(TEXT(Status),&quot;Attempt&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Leads_Recycle_Lead</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Leads_Status_Detail_Partner_30_Day_SLA</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Lead.Partner_Pass_to_Date__c</offsetFromField>
            <timeLength>37</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Leads%3A Partner Lead Status Changed</fullName>
        <actions>
            <name>Leads_Partner_Lead_Status_Changed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>updates Date Partner Lead Status Changed field</description>
        <formula>ISCHANGED( Partner_Lead_Status__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A Partner Pass-to Tracking Date</fullName>
        <actions>
            <name>Leads_Partner_Pass_to_Tracking_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Change the record type when a lead is flipped to a partner</description>
        <formula>AND(
PRIORVALUE(Is_Partner_Owned__c)=FALSE,
Is_Partner_Owned__c=TRUE
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A Pass to Partner</fullName>
        <actions>
            <name>Leads_Partner_Lead_Record_Type_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Leads_Partner_Pass_to_Tracking_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Change the record type when a lead is flipped to a partner</description>
        <formula>AND( PRIORVALUE(Is_Partner_Owned__c)=FALSE, Is_Partner_Owned__c=TRUE )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A Populate Converted From Lead field</fullName>
        <actions>
            <name>Leads_Populate_Converted_From_Lead</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A Reassign Tier 4 Leads</fullName>
        <actions>
            <name>Change_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Disqualified_Lead</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Country</field>
            <operation>equals</operation>
            <value>IR,SD,KP,CU,SY</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A Recycle Reassignment</fullName>
        <actions>
            <name>Leads_Clear_PID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Leads_Recycle_Assignment</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Leads_Standard_Lead_Record_Type_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Closed,Recycle</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.CreatedByPartner__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.IsPartnerOwned__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Changes a partner lead marked as Recycle to the Marketing queue so they don&apos;t see it anymore.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A Reset Lead Status</fullName>
        <actions>
            <name>Reset_Lead_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Resets Lead Status to Not Contacted when a lead is flipped to a partner</description>
        <formula>AND( Exclude_from_Workflow__c=FALSE, PRIORVALUE(Is_Partner_Owned__c)=FALSE, Is_Partner_Owned__c=TRUE )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A Returned to SDR</fullName>
        <actions>
            <name>Leads_Returned_to_SDR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Partner_Lead_Status__c</field>
            <operation>equals</operation>
            <value>Returned to SDR,Returned to Tenable</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A Save Contact Date</fullName>
        <actions>
            <name>Leads_Save_Contact_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(
TEXT(Status)=&quot;Contacted&quot;,
OR(
CONTAINS(TEXT(PRIORVALUE(Status)),&quot;Attempt&quot;),
TEXT(PRIORVALUE(Status))=&quot;Not Contacted&quot;
),
ISNULL(Contact_Date__c) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A Save SAL Date</fullName>
        <actions>
            <name>Leads_Save_SAL_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(
ISPICKVAL(PRIORVALUE(Status),&quot;Not Contacted&quot;),
OR(
CONTAINS(TEXT(Status),&quot;Attempt&quot;),
TEXT(Status)=&quot;Contacted&quot;
),
ISNULL(SAL_Date__c) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A Save SQL Date</fullName>
        <actions>
            <name>Leads_Save_SQL_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Qualified</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A Set CMs Email</fullName>
        <actions>
            <name>Set_CM_Managers_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Channel_Managers_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OwnerID__c</field>
            <operation>startsWith</operation>
            <value>005</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Owner_Type__c</field>
            <operation>equals</operation>
            <value>Partner</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A Set Default Lead Status</fullName>
        <actions>
            <name>Leads_Set_Default_Lead_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A Set SDR info</fullName>
        <actions>
            <name>Set_SDRs_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_SDRs_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update SDR info on lead when lead is assigned to SDR</description>
        <formula>ISPICKVAL(Owner:User.Sales_Classification__c,&quot;SDR&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A Standard Lead Record Type</fullName>
        <actions>
            <name>Leads_Standard_Lead_Record_Type_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Change the record type when a lead is flipped from a partner</description>
        <formula>AND(
OR(
ISPICKVAL(Owner:User.UserType,&quot;Standard&quot;),
Owner:Queue.Id != &quot;&quot;
),
RecordType.Name = &quot;Partner Leads&quot;
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A Support Community Request Record Type</fullName>
        <actions>
            <name>Leads_Support_Community_Request_Record</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Support - Community Request</value>
        </criteriaItems>
        <description>update the Record Type to &apos;Support Community Request&apos; when the Lead is owned by &apos;Support - Community Request&apos;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Leads%3A eComm Booked</fullName>
        <actions>
            <name>Leads_eComm_Booked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Partner_Lead_Status__c</field>
            <operation>equals</operation>
            <value>eComm Booked</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>a1I60000007k5uUEAQ_Purge</fullName>
        <actions>
            <name>MALatitude_c_Purge</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>MALongitude_c_Purge</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Lead.a1I60000007k5uUEAQ_Purge</description>
        <formula>OR(ISCHANGED(Street),ISCHANGED(City),ISCHANGED(State),ISCHANGED(PostalCode),ISCHANGED(Country))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>a1L60000004T8r9EAC_Purge</fullName>
        <actions>
            <name>MALatitude_c_Purge</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>MALongitude_c_Purge</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Lead.a1L60000004T8r9EAC_Purge</description>
        <formula>OR(ISCHANGED(Street),ISCHANGED(City),ISCHANGED(State),ISCHANGED(PostalCode),ISCHANGED(Country))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
