<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Quote_Line_Update_Negative_Line_Trade_In</fullName>
        <description>CHN-960 Any negative line should have Trade-In checked - Passed to NS and impacts ARM</description>
        <field>Trade_In__c</field>
        <literalValue>1</literalValue>
        <name>Quote Line Update Negative Line Trade In</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Quote Line Update Negative Line Trade In True</fullName>
        <actions>
            <name>Quote_Line_Update_Negative_Line_Trade_In</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>QuoteLine__c.Extended_Net_Price__c</field>
            <operation>lessThan</operation>
            <value>USD 0</value>
        </criteriaItems>
        <criteriaItems>
            <field>QuoteLine__c.Trade_In__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <description>CHN-960 Any negative line should have Trade-In checked - Passed to NS and impacts ARM</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
