<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Campaign_MDF_Clawback_Notification</fullName>
        <description>Campaign: MDF Clawback Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Partner_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>cblando@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jorr@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>edgepartner@tenable.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Campaigns/Campaigns_MDF_Clawback_Notification</template>
    </alerts>
    <alerts>
        <fullName>Campaign_Mgr_Notification</fullName>
        <description>Campaign Mgr Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>eshimko@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Campaign_Campaign_Mgr_Notification</template>
    </alerts>
    <alerts>
        <fullName>Cmpns_2nd_reminder_actual_costs_need_to_be_added</fullName>
        <description>Cmpns:2nd reminder actual costs need to be added</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Campaign_Actual_Costs_Needed</template>
    </alerts>
    <alerts>
        <fullName>Cmpns_2nd_reminder_for_owner_to_submit_for_approval</fullName>
        <description>Cmpns: 2nd reminder for owner to submit for approval</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Campaign_Notification_Campaign_has_not_been_submitted</template>
    </alerts>
    <alerts>
        <fullName>Cmpns_Actual_costs_need_to_be_added</fullName>
        <description>Cmpns:Actual costs need to be added</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Campaign_Actual_Costs_Needed</template>
    </alerts>
    <alerts>
        <fullName>Cmpns_Final_Notification_of_List_Attached</fullName>
        <ccEmails>CampaignServices@tenable.com</ccEmails>
        <description>Cmpns: Final Notification of List Attached</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cmpns_Final_Notification_of_List_Attached</template>
    </alerts>
    <alerts>
        <fullName>Cmpns_Final_Notification_to_Upload_Lead_List</fullName>
        <description>Cmpns: Final Notification to Upload Lead List</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>cgreco@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cmpns_Final_Notification_to_Upload_Lead_List</template>
    </alerts>
    <alerts>
        <fullName>Cmpns_Final_reminder_for_owner_to_submit_for_approval_add_manager</fullName>
        <description>Cmpns: Final reminder for owner to submit for approval - add manager</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Owner_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Campaign_Notification_Campaign_has_not_been_submitted</template>
    </alerts>
    <alerts>
        <fullName>Cmpns_Notification_of_Campaign_Complete</fullName>
        <description>Cmpns: Notification of Campaign Complete</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cmpns_Notification_of_Campaign_Completed</template>
    </alerts>
    <alerts>
        <fullName>Cmpns_Notification_of_Campaign_Leads_Imported</fullName>
        <ccEmails>campaignservices@tenable.com</ccEmails>
        <ccEmails>lead-administrators@tenable.com</ccEmails>
        <description>Cmpns: Notification of Campaign Leads Imported- AMER</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>jhughes@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mkelley@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Campaign_Notification_of_Leads_Imported</template>
    </alerts>
    <alerts>
        <fullName>Cmpns_Notification_of_Campaign_Leads_Imported_APAC</fullName>
        <ccEmails>campaignservices@tenable.com</ccEmails>
        <ccEmails>apacsdrs@tenable.com</ccEmails>
        <description>Cmpns: Notification of Campaign Leads Imported-APAC</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>hscoggin@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rhealey@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Campaign_Notification_of_Leads_Imported</template>
    </alerts>
    <alerts>
        <fullName>Cmpns_Notification_of_Campaign_Leads_Imported_EMEA</fullName>
        <ccEmails>campaignservices@tenable.com</ccEmails>
        <description>Cmpns: Notification of Campaign Leads Imported- EMEA</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>acosta@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mqayum-millard@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sgowing@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Campaign_Notification_of_Leads_Imported</template>
    </alerts>
    <alerts>
        <fullName>Cmpns_Notification_of_List_Attached</fullName>
        <ccEmails>CampaignServices@tenable.com</ccEmails>
        <description>Cmpns: Notification of List Attached</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cmpns_Notification_of_List_Attached</template>
    </alerts>
    <alerts>
        <fullName>Cmpns_Notification_of_New_Campaign_Created</fullName>
        <description>Cmpns: Notification of New Campaign Created</description>
        <protected>false</protected>
        <recipients>
            <recipient>abeers@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Campaign_New_Campaign_Notification</template>
    </alerts>
    <alerts>
        <fullName>Cmpns_Notification_of_Requirements_Updated</fullName>
        <description>Cmpns: Notification of Requirements Updated</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cmpns_Notification_of_Update_to_Requirements_after_Approved</template>
    </alerts>
    <alerts>
        <fullName>Cmpns_Notification_to_Upload_Lead_List</fullName>
        <description>Cmpns: Notification to Upload Lead List</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cmpns_Notification_to_Upload_Lead_List</template>
    </alerts>
    <alerts>
        <fullName>Cmpns_Request_Actual_costs_and_add_Manger</fullName>
        <description>Cmpns:Request Actual costs and add Manger</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Owner_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Campaign_Actual_Costs_Needed</template>
    </alerts>
    <alerts>
        <fullName>Cmpns_remind_owner_to_submit_for_approval</fullName>
        <description>Cmpns: Remind owner to submit for approval</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Campaign_Notification_Campaign_has_not_been_submitted</template>
    </alerts>
    <alerts>
        <fullName>Notification_of_Campaign_Approval</fullName>
        <ccEmails>campaignservices@tenable.com</ccEmails>
        <description>Notification of Campaign Approval</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Campaign_Approval_Received</template>
    </alerts>
    <alerts>
        <fullName>Notification_of_Campaign_Approval_Owner_Creator_Notification</fullName>
        <description>Notification of Campaign Approval (Owner/Creator Notification)</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Campaign_Approval_Received</template>
    </alerts>
    <alerts>
        <fullName>Notification_of_Campaign_Rejection</fullName>
        <description>Notification of Campaign Rejection</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Campaign_Approval_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Notification_of_Campaign_Update</fullName>
        <ccEmails>campaignservices@tenable.com</ccEmails>
        <description>Notification of Campaign Update</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Campaigns_Approved_Campaign_Update</template>
    </alerts>
    <alerts>
        <fullName>Send_An_Email_When_Not_Approved_Mktg_Plan_Is_Checked</fullName>
        <description>Send An Email When Not Approved Mktg Plan Is Checked</description>
        <protected>false</protected>
        <recipients>
            <recipient>dduncan@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jbeck@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rlopez@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Campaign_Not_part_of_Mktg_Plan</template>
    </alerts>
    <fieldUpdates>
        <fullName>Budget_Approved_Date</fullName>
        <description>Budget Approved Date is stamped via Campaign Approval Process when set to status &quot;Budget Approval&quot; or &quot;Approved&quot; and previously null 
CHN-116 Campaign Approval Process change</description>
        <field>Budget_Approved_Date__c</field>
        <formula>Today()</formula>
        <name>Budget Approved Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Campaign_10_Days_After_Completion</fullName>
        <field>X10_Days_After_Completion__c</field>
        <formula>TODAY() + 10</formula>
        <name>Campaign: 10 Days After Completion</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cmpgns_Set_Active_True</fullName>
        <field>IsActive</field>
        <literalValue>1</literalValue>
        <name>Cmpgns: Set Active True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cmpns_Update_Expected_Response</fullName>
        <description>Estimated Number of MCLs/Estimated Total Audience</description>
        <field>ExpectedResponse</field>
        <formula>Number_of_Target_Leads__c  /  Estimated_Total_Audience__c</formula>
        <name>Cmpns: Update Expected Response %</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cmpns_Update_Expected_Revenue</fullName>
        <description>Estimated Number of MCLs X Estimated MCL to SQL X Estimated SQL to Won X Estimated AOV</description>
        <field>ExpectedRevenue</field>
        <formula>Number_of_Target_Leads__c *  Estimated_MCL_to_SQL_Conversion__c *  Estimated_SQL_to_Win_Rate__c * Estimated_AOV__c</formula>
        <name>Cmpns: Update Expected Revenue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cmpns_Update_Status_to_Needs_Leads</fullName>
        <field>Status</field>
        <literalValue>Needs Leads</literalValue>
        <name>Cmpns: Update Status to Needs Leads</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Notification</fullName>
        <field>Campaign_Modified_Notification__c</field>
        <literalValue>0</literalValue>
        <name>Reset Notification</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Active_Status</fullName>
        <field>IsActive</field>
        <literalValue>1</literalValue>
        <name>Set Active Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Status_Budget_Approved</fullName>
        <description>CHN-116 Approval Status goes to Budget Approval</description>
        <field>Approval_Status__c</field>
        <literalValue>Budget Approval</literalValue>
        <name>Set Approval Status to Budget Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Status_to_Approved</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Set Approval Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Status_to_Open</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Open</literalValue>
        <name>Set Approval Status to Open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Status_to_Pending</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Set Approval Status to Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Status_to_Rejected</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Set Approval Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Campaign Auto Approved</fullName>
        <actions>
            <name>Set_Approval_Status_to_Approved</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Campaign_Svcs_picklist__c</field>
            <operation>equals</operation>
            <value>None</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.BudgetedCost</field>
            <operation>lessThan</operation>
            <value>&quot;USD 10,000&quot;</value>
        </criteriaItems>
        <description>CHN-116 If Campaign SVC Requirements is None and Budget is less than 10K than update status to Approved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Campaign Budget Approval Date Stamp</fullName>
        <actions>
            <name>Budget_Approved_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>CHN-116 workflow to update the budget approval date stamp when campaign approval is Approved. Steps in approval process will update this field but this is a catch-all in case not stamped along the way and don&apos;t want to overwrite the date.</description>
        <formula>(ISCHANGED(Approval_Status__c) &amp;&amp;  ISPICKVAL(Approval_Status__c, &quot;Approved&quot; ) &amp;&amp; ISBLANK(Budget_Approved_Date__c ))  ||  (ISNEW() &amp;&amp;  ISPICKVAL(Approval_Status__c, &quot;Approved&quot; ) &amp;&amp; ISBLANK(Budget_Approved_Date__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Campaign%3A Default to Active</fullName>
        <actions>
            <name>Cmpgns_Set_Active_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.IsActive</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>When a Campaign is created, set Active to true</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Campaign%3A MDF Clawback</fullName>
        <actions>
            <name>Campaign_10_Days_After_Completion</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Campaign.Status</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <description>Date stamp the &apos;10 Days After Completion&apos; field then notify the Partner Contact and Campaign Owner that the MDF Clawback countdown is 5 days away from kicking in</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Campaign_MDF_Clawback_Notification</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Campaign.X10_Days_After_Completion__c</offsetFromField>
            <timeLength>-5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Campaign%3A Notification of Leads Imported</fullName>
        <actions>
            <name>Cmpns_Notification_of_Campaign_Leads_Imported</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Status</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.OwnerId</field>
            <operation>equals</operation>
            <value>Penney Farris,David Schreiber</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Number_of_Target_Leads__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <description>Notification to Jonathan Hughes when leads from corporate level program have been imported. (campaign has been marked as &quot;Completed&quot;)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Campaign%3A Notification of Leads Imported-AMER</fullName>
        <actions>
            <name>Cmpns_Notification_of_Campaign_Leads_Imported</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Status</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.OwnerId</field>
            <operation>equals</operation>
            <value>Elizabeth Edwards,Jessica Robillard,Victoria Oclassen,Robin Rottinghaus,Mandy Kelley</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Number_of_Target_Leads__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <description>Notifications to campaignservices@tenable.com, lead-administrators@tenable.com, Mandy Kelley and Jonathan Hughes when leads from corporate level program have been imported. (campaign has been marked as &quot;Completed&quot;)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Campaign%3A Notification of Leads Imported-APAC</fullName>
        <actions>
            <name>Cmpns_Notification_of_Campaign_Leads_Imported_APAC</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Status</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.OwnerId</field>
            <operation>equals</operation>
            <value>Robert Healey,Michelle Tham,Bill Ting,Hollis Scoggin</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Number_of_Target_Leads__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <description>Notifications to campaignservices@tenable.com, Robert Healey and Ian Foo when leads from corporate level program have been imported. (campaign has been marked as &quot;Completed&quot;)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Campaign%3A Notification of Leads Imported-EMEA</fullName>
        <actions>
            <name>Cmpns_Notification_of_Campaign_Leads_Imported_EMEA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Status</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.OwnerId</field>
            <operation>equals</operation>
            <value>Mehreen Qayum-Millard,Jessica Patey,Claudia Meisinger,Isabelle Laguerre,Lianne Johnson,Firas Al-Tamimi</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Number_of_Target_Leads__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <description>Notifications to campaignservices@tenable.com, Mehreen Qayum-Millard and Sarah Gowing when leads from corporate level program have been imported. (campaign has been marked as &quot;Completed&quot;)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cmpns%3A Approved Modified Notification</fullName>
        <actions>
            <name>Notification_of_Campaign_Update</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Reset_Notification</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Campaign.Campaign_Modified_Notification__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.LastModifiedById</field>
            <operation>notContain</operation>
            <value>Automation User,David Korba,Sean Hoekzema,Aryeh Pliskin,Kelsea Bond,Jana Marshall</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Cmpns%3A Marketing Notification of New Campaign</fullName>
        <actions>
            <name>Cmpns_Notification_of_New_Campaign_Created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notification to Marketing to review a new campaign once its been created to prevent multiple campaigns happening at the same time.</description>
        <formula>Id  &lt;&gt; null</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Cmpns%3A Needs Leads Notification</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Campaign.Type</field>
            <operation>notEqual</operation>
            <value>Webinar,Website Form</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Approval_Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Status</field>
            <operation>notEqual</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Number_of_Target_Leads__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <description>When a campaign is not a Webinar, send notification to the owner to upload the lead list the day after the campaign has ended.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Cmpns_Notification_to_Upload_Lead_List</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Cmpns_Update_Status_to_Needs_Leads</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Campaign.EndDate</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Cmpns%3A Needs Leads Notification-2</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Type</field>
            <operation>notEqual</operation>
            <value>Webinar,Website Form</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Approval_Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Status</field>
            <operation>notEqual</operation>
            <value>Completed,Aborted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Number_of_Target_Leads__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Non_Lead_Gen__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Exclude_from_Metrics__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>When a campaign is not a Webinar, send notification to the owner to upload the lead list two days after the campaign has ended.
INC-13189: added new criteria and removed Status update action
INC-70508: do not fire if Status = Aborted</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Cmpns_Notification_to_Upload_Lead_List</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Campaign.EndDate</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Cmpns%3A Notification of Campaign Completed</fullName>
        <actions>
            <name>Cmpns_Notification_of_Campaign_Complete</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Status</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <description>When a campaign is marked as complete, send a notification to the owner of the campaign.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cmpns%3A Notification of List Attached</fullName>
        <actions>
            <name>Cmpns_Notification_of_List_Attached</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Status</field>
            <operation>equals</operation>
            <value>List Attached</value>
        </criteriaItems>
        <description>When the campaign status is updated to &quot;List Attached&apos; send notification to the campaign services team. If status is still &quot;List Attached&quot; 2 days later, send reminder. Final notice is sent 4 days later if status is still &quot;List Attached&quot;.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Cmpns_Notification_of_List_Attached</name>
                <type>Alert</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Cmpns_Final_Notification_of_List_Attached</name>
                <type>Alert</type>
            </actions>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Cmpns%3A Second and Third Notification to Upload Lead list</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Campaign.Type</field>
            <operation>notEqual</operation>
            <value>Webinar,Website Form</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Status</field>
            <operation>equals</operation>
            <value>Needs Leads</value>
        </criteriaItems>
        <description>When a campaign has been in the &quot;Needs Leads&quot; status for more than 3 days, send another notification. If it goes past 5 days send final notification.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Cmpns_Notification_to_Upload_Lead_List</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Campaign.EndDate</offsetFromField>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Cmpns_Final_Notification_to_Upload_Lead_List</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Campaign.EndDate</offsetFromField>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Cmpns%3A Second and Third Notification to Upload Lead list-2</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Type</field>
            <operation>notEqual</operation>
            <value>Webinar,Website Form</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Status</field>
            <operation>equals</operation>
            <value>Needs Leads</value>
        </criteriaItems>
        <description>When a campaign has been in the &quot;Needs Leads&quot; status for more than 5 days, send another notification. If it goes past 7 days send final notification.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Cmpns_Notification_to_Upload_Lead_List</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Campaign.EndDate</offsetFromField>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Cmpns_Final_Notification_to_Upload_Lead_List</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Campaign.EndDate</offsetFromField>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Cmpns%3A Send Email Alert When Not Part Of Approved Mktg Plan Is Checked</fullName>
        <actions>
            <name>Send_An_Email_When_Not_Approved_Mktg_Plan_Is_Checked</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Not_part_of_Approved_Mktg_Plan__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cmpns%3A Submit for Approval Reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Has_Children__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Exclude_from_Metrics__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Number_of_Campaign_Members__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Approval_Status__c</field>
            <operation>notEqual</operation>
            <value>Approved,Submitted</value>
        </criteriaItems>
        <description>Reminder to Submit Campaign for Approval</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Cmpns_2nd_reminder_for_owner_to_submit_for_approval</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Campaign.EndDate</offsetFromField>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Cmpns_Final_reminder_for_owner_to_submit_for_approval_add_manager</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Campaign.EndDate</offsetFromField>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Cmpns_remind_owner_to_submit_for_approval</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Campaign.EndDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Cmpns%3A Update Expected Response</fullName>
        <actions>
            <name>Cmpns_Update_Expected_Response</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Number_of_Target_Leads__c</field>
            <operation>notEqual</operation>
            <value>,0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Estimated_Total_Audience__c</field>
            <operation>notEqual</operation>
            <value>,0</value>
        </criteriaItems>
        <description>Calculate &quot;Expected Response (%)&quot; field. [Estimated Number of MCLS / Estimated Total Audience].
-INC-2379: Updated criteria so this rule will NOT fire if 0s are entered</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cmpns%3A Update Expected Revenue</fullName>
        <actions>
            <name>Cmpns_Update_Expected_Revenue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Number_of_Target_Leads__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Estimated_AOV__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Estimated_MCL_to_SQL_Conversion__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Estimated_SQL_to_Win_Rate__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Number_of_Target_Leads__c * Estimated_MCL_to_SQL_Conversion__c * Estimated_SQL_to_Win_Rate__c * Estimated_AOV__c</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cmpns%3AUpdate Actual Cost</fullName>
        <active>true</active>
        <booleanFilter>1 and 2 And 3 And 4 and 5 and 6 and 7</booleanFilter>
        <criteriaItems>
            <field>Campaign.Status</field>
            <operation>notEqual</operation>
            <value>Aborted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.ActualCost</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Approval_Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Exclude_from_Metrics__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Has_Children__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.No_Cost__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.BudgetedCost</field>
            <operation>notEqual</operation>
            <value>USD 0</value>
        </criteriaItems>
        <description>Once its 10 days past the end date of a campaign, if actual cost is blank a reminder email will be sent to the campaign owner</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Cmpns_Actual_costs_need_to_be_added</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Campaign.EndDate</offsetFromField>
            <timeLength>10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Cmpns_2nd_reminder_actual_costs_need_to_be_added</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Campaign.EndDate</offsetFromField>
            <timeLength>12</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Cmpns_Request_Actual_costs_and_add_Manger</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Campaign.EndDate</offsetFromField>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
