<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Claim_Processing_Claim_Paid_Notification</fullName>
        <description>Claim Processing Claim Paid Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>Channel Marketing Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Partner Marketing/Finance</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Channel_Marketing_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Partner_Community_Templates/Fund_Claim_Claim_Paid</template>
    </alerts>
    <alerts>
        <fullName>Claim_Processing_Completed_Notification</fullName>
        <description>Claim Processing Completed Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Channel_Marketing_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>partners@tenable.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Partner_Community_Templates/Fund_Claim_Approval_Completed</template>
    </alerts>
    <alerts>
        <fullName>Claim_Processing_Internal_Approval_Notification</fullName>
        <ccEmails>accountspayable@tenable.com</ccEmails>
        <description>Claim Processing Internal Approval Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>kespina@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kmonahan@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rlopez@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>partners@tenable.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Fund_Claim_Approval_Internal</template>
    </alerts>
    <alerts>
        <fullName>Claim_Processing_Partner_Approval_Notification</fullName>
        <description>Claim Processing Partner Approval Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>Channel Marketing Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Partner Marketing/Finance</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Channel_Marketing_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Partner_Community_Templates/Fund_Claim_Approval_Completed</template>
    </alerts>
    <alerts>
        <fullName>Claim_Processing_Rejected_Notification</fullName>
        <description>Claim Processing Rejected Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Channel_Marketing_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>partners@tenable.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Partner_Community_Templates/Fund_Claim_Approval_Rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>Fund_Claim_Approved_Date_Time_is_Now</fullName>
        <description>Updates the Approved Date/Time to &quot;Now&quot;</description>
        <field>Approved_Date_Time__c</field>
        <formula>Now()</formula>
        <name>Fund Claim: Approved Date/Time is Now</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fund_Claim_In_Process_Date_Time_is_Now</fullName>
        <description>Updates the In Process Date/Time to &quot;now&quot;</description>
        <field>In_Process_Date_Time__c</field>
        <formula>Now()</formula>
        <name>Fund Claim: In Process Date/Time is Now</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fund_Claim_Paid_Date_Time_is_Now</fullName>
        <description>Updates the Paid Date/Time to &quot;now&quot;</description>
        <field>Paid_Date_Time__c</field>
        <formula>Now()</formula>
        <name>Fund Claim: Paid Date/Time is Now</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fund_Claim_Status_is_In_Process</fullName>
        <field>Status__c</field>
        <literalValue>In Process</literalValue>
        <name>Fund Claim: Status is In Process</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fund_Claim_Status_is_Paid</fullName>
        <field>Status__c</field>
        <literalValue>Paid</literalValue>
        <name>Fund Claim: Status is Paid</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fund_Claim_Update_Status_to_Approved</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Fund Claim: Update Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fund_Claim_Update_Status_to_In_Review</fullName>
        <field>Status__c</field>
        <literalValue>In Review</literalValue>
        <name>Fund Claim: Update Status to In Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fund_Claim_Update_Status_to_Open</fullName>
        <field>Status__c</field>
        <literalValue>Open</literalValue>
        <name>Fund Claim: Update Status to Open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fund_Claim_Update_Status_to_Rejected</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Fund Claim: Update Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fund_Claim_Update_Status_to_Submitted</fullName>
        <field>Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Fund Claim: Update Status to Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Fund Claim%3A Notification of Claim In Process</fullName>
        <actions>
            <name>Claim_Processing_Completed_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Fund_Claim__c.Status__c</field>
            <operation>equals</operation>
            <value>In Process</value>
        </criteriaItems>
        <description>Sends an email notification to the Partner when the Status of the Fund is Set to &quot;In Process&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Fund Claim%3A Update Approved Date%2FTime</fullName>
        <actions>
            <name>Fund_Claim_Approved_Date_Time_is_Now</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Fund_Claim__c.Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <description>Stamps the Approved Date/Time field with the Date/Time status was updated to Approved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Fund Claim%3A Update In Process Date%2FTime</fullName>
        <actions>
            <name>Fund_Claim_In_Process_Date_Time_is_Now</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Fund_Claim__c.Status__c</field>
            <operation>equals</operation>
            <value>In Process</value>
        </criteriaItems>
        <description>Stamps the In Process Date/Time field with the Date/Time status was updated to Approved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Fund Claim%3A Update Paid Date%2FTime</fullName>
        <actions>
            <name>Claim_Processing_Claim_Paid_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Fund_Claim_Paid_Date_Time_is_Now</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Fund_Claim__c.Status__c</field>
            <operation>equals</operation>
            <value>Paid</value>
        </criteriaItems>
        <description>Stamps the In Paid Date/Time field with the Date/Time status was updated to Approved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
