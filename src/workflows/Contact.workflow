<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Contacts_Created_Source_Marketing</fullName>
        <field>Created_Source__c</field>
        <literalValue>Marketing</literalValue>
        <name>Contacts: Created Source - Marketing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contacts_Created_Source_Sales</fullName>
        <field>Created_Source__c</field>
        <literalValue>Sales</literalValue>
        <name>Contacts: Created Source - Sales</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contacts_Nessus_Entr_Cloud_Expr_Date</fullName>
        <field>Date_Nessus_Ent_Cloud_Eval_Expires__c</field>
        <formula>Today() + 15</formula>
        <name>Contacts: Nessus Entr Cloud Expr Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contacts_Nessus_Entr_Expiration_Date</fullName>
        <field>Date_Nessus_Ent_Eval_Expires__c</field>
        <formula>Today() + 15</formula>
        <name>Contacts: Nessus Entr Expiration Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contacts_Nessus_Pro_Eval_Expr_Date</fullName>
        <field>Date_Nessus_Eval_Expires__c</field>
        <formula>Today() + 7</formula>
        <name>Contacts: Nessus Pro Eval Expr Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Contacts%3A Created Source - Marketing</fullName>
        <actions>
            <name>Contacts_Created_Source_Marketing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>(CONTAINS(CreatedBy.Profile.Name, &quot;Marketing Advanced User&quot;) || CONTAINS(CreatedBy.Profile.Name, &quot;Marketing User&quot;) ||  (CONTAINS(CreatedBy.Profile.Name, &quot;Partner Manager User&quot;) &amp;&amp; CONTAINS(CreatedBy.UserRole.Name, &quot;Marketing&quot;)) || (CreatedById = &quot;00560000006W7gq&quot;)) &amp;&amp; (Converted_From_Lead__c = FALSE)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contacts%3A Created Source - Sales</fullName>
        <actions>
            <name>Contacts_Created_Source_Sales</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>(CONTAINS(CreatedBy.Profile.Name, &quot;ISR&quot;) || CONTAINS(CreatedBy.Profile.Name, &quot;Tenable - Sales&quot;) ||  (CONTAINS(CreatedBy.Profile.Name, &quot;Partner Manager User&quot;) &amp;&amp; CONTAINS(CreatedBy.UserRole.Name, &quot;Sales&quot;))) &amp;&amp; (Converted_From_Lead__c = FALSE)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contacts%3A Nessus Enterprise Cloud Expiration Date</fullName>
        <actions>
            <name>Contacts_Nessus_Entr_Cloud_Expr_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Nessus_Ent_Cloud_Eval_Start_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contacts%3A Nessus Enterprise Expiration Date</fullName>
        <actions>
            <name>Contacts_Nessus_Entr_Expiration_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Nessus_Ent_Eval_Start_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contacts%3A Nessus Professional Expiration Date</fullName>
        <actions>
            <name>Contacts_Nessus_Pro_Eval_Expr_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Date_Nessus_Eval_Activation_Sent__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
