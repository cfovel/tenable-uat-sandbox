<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Case_Email_Update_Customer_Updated_Date</fullName>
        <field>Date_Time_Customer_Updated__c</field>
        <formula>Now()</formula>
        <name>Case Email: Update Customer Updated Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Email_Update_Status_Cust_Updated</fullName>
        <description>Updates Status to &quot;Customer Updated&quot;</description>
        <field>Status</field>
        <literalValue>Customer Updated</literalValue>
        <name>Case Email: Update Status Cust Updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Owner_is_ACAS_Queue</fullName>
        <description>Updates case owner to the ACAS Queue</description>
        <field>OwnerId</field>
        <lookupValue>Support_ACAS</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case: Owner is ACAS Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Owner_is_Eval_Queue</fullName>
        <description>Updates case owner to the Evals queue</description>
        <field>OwnerId</field>
        <lookupValue>Support_Evals</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case: Owner is Eval Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_ACAS_Checkbox_to_True</fullName>
        <description>Updates the ACAS Checkbox to True.</description>
        <field>ACAS__c</field>
        <literalValue>1</literalValue>
        <name>Case: Update ACAS Checkbox to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Owner_to_PCI_Queue</fullName>
        <description>Updates the Case Owner to the PCI Queue</description>
        <field>OwnerId</field>
        <lookupValue>Support_PCI</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case: Update Owner to PCI Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_PCI_Checkbox_to_True</fullName>
        <field>PCI__c</field>
        <literalValue>1</literalValue>
        <name>Case: Update PCI Checkbox to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Product_to_Nessus_Cloud</fullName>
        <description>Updates Product to &quot;Nessus Cloud&quot;</description>
        <field>Product__c</field>
        <literalValue>Nessus Cloud</literalValue>
        <name>Case: Update Product to Nessus Cloud</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Q_I_Sub_Top_PCI_ASV_Review</fullName>
        <description>Updates Question/Issue Sub-Topic to &quot;PCI ASV Review Process&quot;</description>
        <field>Question_Issue_Sub_Topic__c</field>
        <literalValue>PCI ASV Review Process</literalValue>
        <name>Case: Update Q/I Sub-Top PCI ASV Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Q_I_to_PCI_Scanning_Service</fullName>
        <description>Updates Question/Issue field to &quot;PCI Scanning Service&quot;</description>
        <field>Question_Issue__c</field>
        <literalValue>PCI Scanning Service</literalValue>
        <name>Case: Update Q/I to PCI Scanning Service</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Case Email%3A Status Customer Updated</fullName>
        <actions>
            <name>Case_Email_Update_Customer_Updated_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Email_Update_Status_Cust_Updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>EmailMessage.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Undeliverable_Contact__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>When a new email is received, update the status to &quot;Customer Updated&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Address To is ACAS</fullName>
        <actions>
            <name>Case_Owner_is_ACAS_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Update_ACAS_Checkbox_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.ToAddress</field>
            <operation>equals</operation>
            <value>supportacas@tenable.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <description>When a new email is received, and ACAS is the To Address, update the Case Owner to the ACAS Queue.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Address To is Evaluations</fullName>
        <actions>
            <name>Case_Owner_is_Eval_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.ToAddress</field>
            <operation>equals</operation>
            <value>eval@tenable.com,eval-support@tenable.com,eval@gapps.tenable.com,eval@nessus.org,eval@tenablesecurity.com,eval@cvs.nessus.org</value>
        </criteriaItems>
        <description>When a new email is received, and Evaluations is the To Address, update the Case Owner to the Eval Queue.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Address To is PCI</fullName>
        <actions>
            <name>Case_Update_Owner_to_PCI_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Update_PCI_Checkbox_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Update_Product_to_Nessus_Cloud</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Update_Q_I_Sub_Top_PCI_ASV_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Update_Q_I_to_PCI_Scanning_Service</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.ToAddress</field>
            <operation>equals</operation>
            <value>pci-req@tenable.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <description>When a new email is received, and PCI is the To Address, update the Case Owner to the PCI Queue.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
