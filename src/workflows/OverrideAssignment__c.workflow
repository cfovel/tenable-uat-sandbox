<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Named_Account_Approved</fullName>
        <field>Approved__c</field>
        <literalValue>1</literalValue>
        <name>Named Account: Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Named_Account_Approved_At</fullName>
        <field>ApprovedAt__c</field>
        <formula>NOW()</formula>
        <name>Named Account: Approved At</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
