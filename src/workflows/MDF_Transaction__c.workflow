<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Funds_Internal_Notification_of_Acceptance_for_Events</fullName>
        <ccEmails>Tenable_Events@tenable.com</ccEmails>
        <description>Funds: Internal Notification of Acceptance for Events</description>
        <protected>false</protected>
        <recipients>
            <recipient>kespina@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kmonahan@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rlopez@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>partners@tenable.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Partner_Community_Templates/Funds_Approval_Received_on_Events</template>
    </alerts>
    <alerts>
        <fullName>Funds_Notification_1_week_before_Claims_due</fullName>
        <description>Funds: Notification 1 week before Claims due</description>
        <protected>false</protected>
        <recipients>
            <recipient>Channel Marketing Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Partner Marketing/Finance</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Regional Marketing Leader</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Channel_Marketing_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>partners@tenable.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Partner_Community_Templates/Funds_Notification_1_week_before_Claims_due</template>
    </alerts>
    <alerts>
        <fullName>Funds_Notification_3_weeks_before_Claims_due</fullName>
        <description>Funds: Notification 3 weeks before Claims due</description>
        <protected>false</protected>
        <recipients>
            <recipient>Channel Marketing Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Partner Marketing/Finance</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Regional Marketing Leader</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Channel_Marketing_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>partners@tenable.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Partner_Community_Templates/Funds_Notification_3_weeks_before_Claims_due</template>
    </alerts>
    <alerts>
        <fullName>Funds_Notification_to_Partner_of_Acceptance</fullName>
        <description>Funds: Notification to Partner of Acceptance</description>
        <protected>false</protected>
        <recipients>
            <recipient>Channel Marketing Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Partner Marketing/Finance</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Channel_Marketing_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>partners@tenable.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Partner_Community_Templates/Funds_Approval_Received</template>
    </alerts>
    <alerts>
        <fullName>Funds_Notification_to_Partner_of_Rejection</fullName>
        <description>Funds: Notification to Partner of Rejection</description>
        <protected>false</protected>
        <recipients>
            <recipient>Channel Marketing Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Partner Marketing/Finance</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Channel_Marketing_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>partners@tenable.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Partner_Community_Templates/Funds_Approval_Rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>Clear_Approved_Date</fullName>
        <field>Date_Approved__c</field>
        <name>Clear Approved Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Rejected_Date</fullName>
        <field>Date_Rejected__c</field>
        <name>Clear Rejected Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Submit_Date</fullName>
        <field>Date_Submitted__c</field>
        <name>Clear Submit Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fund_Reset_POP_Acknowledged</fullName>
        <field>POP_Acknowledged__c</field>
        <literalValue>0</literalValue>
        <name>Fund: Reset POP Acknowledged</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MDF_Change_Record_Type_to_Approved</fullName>
        <field>RecordTypeId</field>
        <lookupValue>ApprovedMDF</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>MDF: Change Record Type to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MDF_Change_Record_Type_to_Unapproved</fullName>
        <field>RecordTypeId</field>
        <lookupValue>UnapprovedMDF</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>MDF: Change Record Type to Unapproved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MDF_Status_set_back_to_Open</fullName>
        <field>Status__c</field>
        <literalValue>Open</literalValue>
        <name>MDF Status set back to Open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MDF_Status_set_to_Approved</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>MDF Status set to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MDF_Status_set_to_In_Review</fullName>
        <field>Status__c</field>
        <literalValue>In Review</literalValue>
        <name>MDF Status set to In Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MDF_Status_set_to_Pending_Final_Approval</fullName>
        <description>Sets the status of the Fund to &quot;Pending Final Approval&quot;</description>
        <field>Status__c</field>
        <literalValue>Pending Final Approval</literalValue>
        <name>MDF Status set to Pending Final Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MDF_Status_set_to_Rejected</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>MDF Status set to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MDF_Status_set_to_Submitted</fullName>
        <field>Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>MDF Status set to Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approved_Date</fullName>
        <field>Date_Approved__c</field>
        <formula>Today()</formula>
        <name>Set Approved Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Submit_Date</fullName>
        <field>Date_Submitted__c</field>
        <formula>Today()</formula>
        <name>Set Submit Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Fund%3A Notification of Claims Due</fullName>
        <active>true</active>
        <criteriaItems>
            <field>MDF_Transaction__c.Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>MDF_Transaction__c.Total_Claims__c</field>
            <operation>lessOrEqual</operation>
            <value>USD 0</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Funds_Notification_1_week_before_Claims_due</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>MDF_Transaction__c.Claim_Due_Date__c</offsetFromField>
            <timeLength>-7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Funds_Notification_3_weeks_before_Claims_due</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>MDF_Transaction__c.Claim_Due_Date__c</offsetFromField>
            <timeLength>-21</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Fund%3A Reset POP Acknowledged</fullName>
        <actions>
            <name>Fund_Reset_POP_Acknowledged</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If Activity Type changes, set POP Acknowledged to False.</description>
        <formula>AND( POP_Acknowledged__c ,ISCHANGED(Activity_Type__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Fund%3A Update Status to Approved</fullName>
        <actions>
            <name>Funds_Notification_to_Partner_of_Acceptance</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MDF_Status_set_to_Approved</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Approved_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>MDF_Transaction__c.Status__c</field>
            <operation>equals</operation>
            <value>Pending Final Approval</value>
        </criteriaItems>
        <criteriaItems>
            <field>MDF_Transaction__c.Tenable_P_O_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>When a PO is entered, update the status of the Fund to Approved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Update_Fund_PO_Number</fullName>
        <assignedTo>kmonahan@tenable.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>10</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Update Fund PO Number</subject>
    </tasks>
</Workflow>
