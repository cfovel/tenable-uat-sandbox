<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Contract_Approval_Received</fullName>
        <description>Contract: Approval Received</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>kespina@tenable.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>CC_User__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Receiving_Employee__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>edgepartner@tenable.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Contract_Approval_Received</template>
    </alerts>
    <alerts>
        <fullName>Contract_Approval_Rejected</fullName>
        <description>Contract: Approval Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>CC_User__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Receiving_Employee__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>edgepartner@tenable.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Contract_Approval_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Contract_Notification_to_Receiving_Employee_that_Contract_is_Signed</fullName>
        <description>Contract: Notification to Receiving Employee that Contract is Signed</description>
        <protected>false</protected>
        <recipients>
            <field>CC_User__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Receiving_Employee__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Contract_Contract_Signed</template>
    </alerts>
    <fieldUpdates>
        <fullName>Check_renewal_forecast</fullName>
        <description>check&apos;s renewal forecast on the contract to generate a renewal opportunity</description>
        <field>SBQQ__RenewalForecast__c</field>
        <literalValue>1</literalValue>
        <name>Check renewal forecast</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_Completed_Date_Stamp</fullName>
        <description>Updates Completed Date to Now</description>
        <field>Completed_Date__c</field>
        <formula>NOW()</formula>
        <name>Contract: Completed Date Stamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_Director_Approved</fullName>
        <field>Director_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Contract: Director Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_Legal_Approved</fullName>
        <field>Legal_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Contract: Legal Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_Manager_Approved</fullName>
        <field>Manager_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Contract: Manager Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_PO_Date_Stamp</fullName>
        <field>PO_Date__c</field>
        <formula>NOW()</formula>
        <name>Contract: PO Date Stamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_Status_is_Approved</fullName>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>Contract: Status is Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_Status_is_Draft</fullName>
        <field>Status</field>
        <literalValue>Draft</literalValue>
        <name>Contract: Status is Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_Status_is_In_Process</fullName>
        <field>Status</field>
        <literalValue>In Process</literalValue>
        <name>Contract: Status is In Process</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_Status_is_Rejected</fullName>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>Contract: Status is Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_Status_is_Submitted</fullName>
        <field>Status</field>
        <literalValue>Submitted</literalValue>
        <name>Contract: Status is Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_Status_set_to_Complete</fullName>
        <field>Status</field>
        <literalValue>Complete</literalValue>
        <name>Contract: Status set to Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_Submitted_Date_Stamp</fullName>
        <description>Stamps the Submitted Date field with &quot;now&quot;</description>
        <field>Submitted_Date__c</field>
        <formula>NOW()</formula>
        <name>Contract: Submitted Date Stamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_VP_Approved</fullName>
        <field>VP_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Contract: VP Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Check Renewal Forecast</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Contract.RecordTypeId</field>
            <operation>equals</operation>
            <value>CPQ Contract</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract.Legacy_Migration_Contract__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract.Renewal_Opp_Time_for_WF__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Generates a renewal opportunity for non-migrated CPQ contracts.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Check_renewal_forecast</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Contract.Renewal_Opp_Time_for_WF__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Contract%3A Completed Date Stamp</fullName>
        <actions>
            <name>Contract_Completed_Date_Stamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract.Status</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <description>When Status is updated to &quot;Complete&quot; update the Completed Date to Now.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contract%3A Contract Signed</fullName>
        <actions>
            <name>Contract_Notification_to_Receiving_Employee_that_Contract_is_Signed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract.Status</field>
            <operation>equals</operation>
            <value>Signed</value>
        </criteriaItems>
        <description>When a contract status is updated to signed, send email notification to the user in the Receiving Employee field.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contract%3A PO Date Stamp</fullName>
        <actions>
            <name>Contract_PO_Date_Stamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract.Tenable_P_O_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Updates the PO Date field with the date that the PO number was updated</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contract%3A Update Director Approved</fullName>
        <actions>
            <name>Contract_Director_Approved</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract.BypassMarketingApproval__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contract%3A Update Manager Approved</fullName>
        <actions>
            <name>Contract_Manager_Approved</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract.BypassMgrApproval__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contract%3A Update Status to Complete</fullName>
        <actions>
            <name>Contract_Status_set_to_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract.Tenable_P_O_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Update_Contract_PO_Number</fullName>
        <assignedTo>kmonahan@tenable.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Update Contract PO Number</subject>
    </tasks>
</Workflow>
