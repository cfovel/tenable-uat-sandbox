<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Cert_Set_Expiration_Date</fullName>
        <field>Certification_Expiration_Date__c</field>
        <formula>IF( 
AND( 
MONTH( Certification_Date__c ) = 2, 
DAY( Certification_Date__c) = 29, 
NOT( 
OR( 
MOD( YEAR( Certification_Date__c), 400 ) = 0, 
AND( 
MOD( YEAR( Certification_Date__c), 4 ) = 0, 
MOD( YEAR( Certification_Date__c), 100 ) != 0 
) 
) 
) 
), 
DATE( YEAR( Certification_Date__c) + 2, 3, 1), 
DATE( YEAR( Certification_Date__c) + 2, MONTH( Certification_Date__c ), DAY( Certification_Date__c) ) 
)</formula>
        <name>Cert: Set Expiration Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Cert%3A Set Expiration Date</fullName>
        <actions>
            <name>Cert_Set_Expiration_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a new certification is created or the certification date is updated to a new date - update the expiration date to be two years from the certification date.</description>
        <formula>OR( NOT( ISBLANK( Certification_Date__c ) ), 
ISCHANGED(Certification_Date__c) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
