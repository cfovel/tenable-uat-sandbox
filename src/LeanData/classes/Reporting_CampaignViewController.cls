/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Reporting_CampaignViewController {
    @RemoteAction
    global static Map<String,Object> getViewObjectWithAdditional(String campaignId, Map<String,Object> additionalFields) {
        return null;
    }
    @RemoteAction
    global static Map<String,Object> getViewObject(String campaignId) {
        return null;
    }
}
