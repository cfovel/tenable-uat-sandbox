/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/LeanDataAPI/*')
global class LeanDataAPI {
    global LeanDataAPI() {

    }
    @HttpPost
    global static String restPost() {
        return null;
    }
    global static Map<String,Object> roundRobinAPI(Map<String,Object> params) {
        return null;
    }
}
