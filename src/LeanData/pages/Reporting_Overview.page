<apex:page sideBar="false" standardStylesheets="false" docType="html-5.0" controller="LeanData.Reporting_BaseController" >
    <style>
        .bs .dg_inner-wrapper-inner-header {
            width: 100%;
            font-size: 16px;
            color: #555555;
            padding-left: 62.5px;
            padding-top: 35px;
        }
        
        .bs .left-content {
            width: 627.5px;
            float: left;
        }
        
        .bs .right-content {
            width: 375.5px;
            float: left;
        }
        
        .bs .reach-goal-number {
            font-size: 50px;
            color: #70797b;
        }
        
        .bs .left-content-goal-box-outer {
            float: right;
            width: 200px;
            margin-right: -35px;
        }
        
        .bs .left-content-goal-box {
            border: 2px solid #8da8bf;
            padding: 6px 10px;
            margin-left: auto;
            margin-right: auto;
            width: max-content;
        }
        
        .bs .charts-container {
            clear: both;
            padding-top: 15px;
            margin-left: 97.5px;
            width: 530px;
            height: 275px;
        }
        
        .bs .right-content-goal-box {
            padding-top: 30px;
            padding-bottom: 30px;
            color: #9a9a9a;
            font-size: 25px;
            text-align: center;
            margin-right: 67.5px;
            margin-left: 57.5px;
            width: 252.5;
            border: 1px solid var(--lt-grey-2);
            border-radius: 5px;
        }
        
        .bs .charts-legend {
            width: 100%;
            text-align: center;
            padding: 20px 0px;
            font-size: 15px;
        }
        
        .bs .chart-legend-table {
            width: 100%;
            margin-left: 60px;
        }
        
        .bs .chart-legend-table td {
            color: #555555 !important;
            text-align: left;
            font-family: open-sans-light;
        }
        
        .bs .legend-color-box {
            width: 15px;
            height: 15px;
            float: left;
            margin-right: 10px;
        }
        
        .bs .right-content-button-wrapper {
            padding-top: 10px;
            width: 100%;
            text-align: center;
        }
        
        .highcharts-button {
            display: none;
        }
    </style>
    <apex:composition template="LeanData__Reporting_Template">
        <apex:define name="customHeaderContent">
            <div class="customHeaderContent-right">
                <apex:commandLink rerender="x" html-data-toggle="modal" html-data-target="#infoModal">
                    <apex:image value="{!URLFOR($Resource.LeanData__Reporting_Images, 'Info_Button_Icon.png')}" styleClass="customHeaderContent-right-information" />
                </apex:commandLink>
            </div>
        </apex:define>
        <apex:define name="mainContent">
            <div class="dg_inner-wrapper" style="margin-right:20px;">
                <div class="dg_inner-wrapper-header">
                    Bookings
                </div>
                <div class="dg_inner-wrapper-content">
                    <div class="dg_inner-wrapper-inner-header" style="float:left;">
                        New Bookings Per Quarter
                    </div>
                    <div class="left-content">
                        <div class="left-content-goal-box-outer">
                            <div class="left-content-goal-box bookings-goal">
                                <span class="bookings-goal-number">$2.5</span>
                            </div>
                        </div>
                        <div class="charts-container bookings-chart">
                        </div>
                        <div class="charts-legend">
                            <div>
                                <table class="chart-legend-table" style="width: 100%;">
                                    <tr>
                                        <td>
                                            <div class="legend-color-box darkBlueBG" /> Marketing Sourced
                                        </td>
                                        <td>
                                            <div class="legend-color-box yellowBG" /> Marketing Influenced
                                        </td>
                                        <td>
                                            <div class="legend-color-box blueBG" /> Sales Only
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="right-content">
                        <div class="right-content-goal-box open-sans-light">
                            <span class="reach-goal-number reach-bookings-goal-number"> </span>
                            <br/>
                            <div class="reach-bookings-goal-text" style="margin-top:5px;">To Reach Goal</div>
                        </div>
                        <div class="right-content-button-wrapper">
                            <a href="/apex/Reporting_Bookings">
                                <div class="main-button darkBlueBG" style="margin: auto;">
                                    Close More Deals
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!----- End Bookings Wrapper ------>
            <div class="dg_inner-wrapper">
                <div class="dg_inner-wrapper-header">
                    <div style="margin:auto;clear:both;">Pipeline</div>
                    <!-- <div class="open-pipeline" style="color: #1abc9c; font-size: 12.5px; float:right;margin-top:-48px;margin-right: 30px;">
                        Open Pipeline: $12.5M
                    </div> -->
                </div>
                <div class="dg_inner-wrapper-content">
                    <div class="dg_inner-wrapper-inner-header" style="float:left;">
                        Pipeline Created Per Quarter
                    </div>
                    <div class="left-content">
                        <div class="left-content-goal-box-outer">
                            <div class="left-content-goal-box pipeline-goal">
                                <span class="pipeline-goal-number">$2.5</span>
                            </div>
                        </div>
                        <div class="charts-container pipeline-chart">
                        </div>
                        <div class="charts-legend">
                            <div>
                                <table class="chart-legend-table" style="width: 100%;">
                                    <tr>
                                        <td>
                                            <div class="legend-color-box darkBlueBG" /> Marketing Sourced
                                        </td>
                                        <td>
                                            <div class="legend-color-box yellowBG" /> Marketing Influenced
                                        </td>
                                        <td>
                                            <div class="legend-color-box blueBG" /> Sales Only
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="right-content">
                        <div class="right-content-goal-box open-sans-light">
                            <span class="reach-goal-number reach-pipeline-goal-number"> </span>
                            <br/>
                            <div class="reach-pipeline-goal-text" style="margin-top:5px;">To Reach Goal</div>
                        </div>
                        <div class="right-content-button-wrapper">
                            <a href="/apex/Reporting_Pipeline">
                                <div class="main-button darkBlueBG" style="margin: auto;">
                                    Create More Pipeline
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Pipeline Wrapper -->
            <div class="dg_inner-wrapper">
                <div class="dg_inner-wrapper-header">
                    Target Accounts
                </div>
                <div class="dg_inner-wrapper-content">
                    <div class="dg_inner-wrapper-inner-header" style="float:left;margin-left:100px;">
                        Account Coverage
                    </div>
                    <div class="left-content">
                        <div class="charts-container target-accounts-chart">
                        </div>
                    </div>
                    <div class="right-content">
                        <div class="right-content-goal-box open-sans-light">
                            <span class="reach-goal-number accounts-without-opportunities"></span>
                            <br/>
                            <div style="margin-top:5px;">Accounts Without Opportunities</div>
                        </div>
                        <div class="right-content-button-wrapper">
                            <a href="/apex/Reporting_TargetAccounts">
                                <div class="main-button darkBlueBG" style="margin: auto;">
                                    Increase Coverage
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Target Account Wrapper -->
            
            <!-- Begin Tooltip -->
            <div id="infoModal" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content" style="display: inline-block;background: #eeefef;border-radius: 0px; padding-left: 20px;">
                        
                        <apex:commandLink rerender="x" html-data-dismiss="modal" style="float:right;margin:5px 5px 0 0">
                            <apex:image width="15px" height="15px" value="{!URLFOR($Resource.LeanData__Reporting_Images, 'Close_Window_Icon.png')}"/>
                        </apex:commandLink>
                        
                        <div class="dg_inner-wrapper" style="width:615px">
                            <div class="dg_inner-wrapper-header">
                                Tooltip | Overview
                            </div>
                            
                            <div class="dg_inner-wrapper-content" style="height:400px;overflow:auto">
                                <p>The Marketing Performance Overview orients you to the overall health of the business. See how you’ve performed in recent quarters and how you are progressing towards your goals.</p>
                                
                                <h5>Bookings</h5>
                                <p>Opportunities closed won per quarter. </p>
                                
                                <h6>Goal</h6>
                                <p>How you are progressing towards your sales targets. </p>
                                
                                <h6>Marketing Sourced</h6>
                                <p>As defined in your advanced settings page. Marketing Sourced tells you which campaigns are best at generating new pipeline.</p>
                                
                                <h6>Marketing Influenced</h6>
                                <p>All marketing touches split equally in a multi-touch attribution model. If a custom weighting model has been configured, Marketing Influenced will reflect that weighting.</p>
                                
                                <h6>Sales Only</h6>
                                <p>Opportunities without any relevant marketing touches are considered sales only.</p>
                                
                                <h5>Pipeline</h5>
                                <p>New opportunities created per quarter. </p>
                                
                                <h5>Target Accounts</h5>
                                <p>Your current target accounts as defined on the Advanced Settings page. The chart is segmented by customers along with accounts with &amp; without opportunities. </p>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of Tooltip -->
        </apex:define>
    </apex:composition>
    <script>
        Highcharts.seriesTypes.column.prototype.pointAttrToOptions.dashstyle = 'dashStyle';
        j$(document).ready(function() {
            setPageTitle('Marketing Performance');
            setActiveNav('home-nav');
            buildReport();

        });

        function buildReport() {
            console.log('begin');
            overviewReport = new OverviewWrapper();
        }

        var OverviewWrapper = function() {
            this.quarterlyMetrics = {};

            //----Metrics for last 4 quarters
            this.quarters = [];
            this.quarterNames = [];
            this.targetAccountMetrics = {};
            this.queryQuarterlyMetrics();
        }

        OverviewWrapper.prototype.queryQuarterlyMetrics = function() {
            var tempQuarterlyMetrics = {};
            Visualforce.remoting.Manager.invokeAction('{!$RemoteAction.Reporting_BaseController.remoteQueryQuarterlyMetrics}',
                function(result, event) {
                    if (event.type === 'exception') {
                        console.log("exception");
                        console.log(event);
                    } else if (event.status) {
                        for (var i = 0; i < result.length; i++) {
                            tempQuarterlyMetrics[result[i][ns+'Numerical_Index__c']] = result[i];
                        }
                        console.log('QM Queried');
                        console.log(tempQuarterlyMetrics);
                        setQuarterlyMetrics(tempQuarterlyMetrics);
                    }
                });
        };

        function setQuarterlyMetrics(quarterlyMetrics) {
            overviewReport.quarterlyMetrics = quarterlyMetrics;
            overviewReport.getTargetAccountMetrics();
        }

        OverviewWrapper.prototype.getTargetAccountMetrics = function() {
            var tempTargetAccountMetrics = {};
            Visualforce.remoting.Manager.invokeAction('{!$RemoteAction.Reporting_BaseController.remoteTargetAccountOverviewMetrics}',
                function(result, event) {
                    if (event.type === 'exception') {
                        console.log("exception");
                        console.log(event);
                    } else if (event.status) {
                        console.log('Remoted Target Account results : ');
                        console.log(result);
                        tempTargetAccountMetrics = result;
                        setTargetAccountMetrics(tempTargetAccountMetrics);
                    }
                });
        };

        function setTargetAccountMetrics(targetAccountMetrics) {
            overviewReport.targetAccountMetrics = targetAccountMetrics;
            overviewReport.calculateMetrics();
        }

        OverviewWrapper.prototype.calculateMetrics = function() {
            var quarterMetricsKeys = Object.keys(this.quarterlyMetrics);
            console.log(quarterMetricsKeys);
            for (var i = quarterMetricsKeys.length - 1; i > quarterMetricsKeys.length - 5; i--) {
                this.quarterNames.unshift(this.quarterlyMetrics[quarterMetricsKeys[i]].Name);
                this.quarters.unshift(this.quarterlyMetrics[quarterMetricsKeys[i]]);
                if (i == 0)
                    break;
            }
            this.quarterNames[this.quarterNames.length - 1] = 'QTD';
            console.log(getArrayVals(this.quarters, ns+'Total_Bookings__c'));
            console.log(this.quarters);
            this.renderReports();
        };

        OverviewWrapper.prototype.renderReports = function() {
            drawBookingsChart();
            drawPipelineChart();
            drawTargetAccountsChart();

            if (getNumericalArrayVals(overviewReport.quarters, ns+ 'Quarterly_Bookings_Goal__c', 1.0)[3] - getNumericalArrayVals(overviewReport.quarters, ns+'Total_Bookings__c', 1.0)[3] < 0) {
                j$('.reach-bookings-goal-text').html('Over Goal');
                j$('.reach-bookings-goal-number').html(formatFieldByType(Math.abs(getNumericalArrayVals(overviewReport.quarters, ns+'Quarterly_Bookings_Goal__c', 1.0)[3] - getNumericalArrayVals(overviewReport.quarters, ns + 'Total_Bookings__c', 1.0)[3]), 'Currency'));
            } else {
                j$('.reach-bookings-goal-number').html(formatFieldByType((getNumericalArrayVals(overviewReport.quarters, ns + 'Quarterly_Bookings_Goal__c', 1.0)[3] - getNumericalArrayVals(overviewReport.quarters, ns + 'Total_Bookings__c', 1.0)[3]), 'Currency'));
            }

            if (getNumericalArrayVals(overviewReport.quarters, ns+'Quarterly_Pipeline_Goal__c', 1.0)[3] - getNumericalArrayVals(overviewReport.quarters, ns+'Total_Pipeline__c', 1.0)[3] < 0) {
                j$('.reach-pipeline-goal-text').html('Over Goal');
                j$('.reach-pipeline-goal-number').html(formatFieldByType(Math.abs(getNumericalArrayVals(overviewReport.quarters, ns + 'Quarterly_Pipeline_Goal__c', 1.0)[3] - getNumericalArrayVals(overviewReport.quarters, ns + 'Total_Pipeline__c', 1.0)[3]), 'Currency'));
            } else {
                j$('.reach-pipeline-goal-number').html(formatFieldByType((getNumericalArrayVals(overviewReport.quarters, ns + 'Quarterly_Pipeline_Goal__c', 1.0)[3] - getNumericalArrayVals(overviewReport.quarters, ns + 'Total_Pipeline__c', 1.0)[3]), 'Currency'));
            }

            j$('.bookings-goal-number').html(formatFieldByType(getNumericalArrayVals(overviewReport.quarters, ns + 'Quarterly_Bookings_Goal__c', 1.0)[3], 'Currency'));
            j$('.pipeline-goal-number').html(formatFieldByType(getNumericalArrayVals(overviewReport.quarters, ns + 'Quarterly_Pipeline_Goal__c', 1.0)[3], 'Currency'));
            j$('.accounts-without-opportunities').html(formatFieldByType(overviewReport.targetAccountMetrics['Accounts Without Opportunities'], 'Integer'));
        };

        function drawBookingsChart() {
            var divisor = getDivisor( Math.max.apply(null, getArrayVals( overviewReport.quarters, ns + 'Total_Bookings__c' ) ) );
            var divisor_suffix = getDivisorChar( Math.max.apply(null, getArrayVals( overviewReport.quarters, ns + 'Total_Bookings__c' ) ) );
            var divisor_word = getDivisorWord( divisor  );
            var maxY = Math.max.apply(null, getArrayVals( overviewReport.quarters, ns + 'Total_Bookings__c' ));
            var maxGoal = (getArrayVals(overviewReport.quarters, ns + 'Quarterly_Bookings_Goal__c' )[3]);

            if( maxGoal > maxY ) maxY = maxGoal;
            
            maxY = (maxY+1) / divisor;
            
            console.log('maxy' + maxY );
            j$('.bookings-chart').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: ''
                },
                xAxis: {
                    categories: overviewReport.quarterNames
                },
                yAxis: {
                    min: 0,
                    max: maxY,
                    title: {
                        text: '$ ' + divisor_word 
                    },
                    labels: {
                        format: '{value}' + divisor_suffix 
                    }
                },
                legend: {
                    enabled: false
                },
                credits: {
                    enabled: false
                },
                plotOptions: {
                    column: {
                        stacking: 'normal'
                    }
                },
                tooltip: {
                    valueSuffix: ' ' + divisor_suffix
                },
                colors: ['#ffffff', '#45b7e6', '#ffc708', '#1b527f'],
                series: [{
                    name: 'Goal',
                    data: [0, 0, 0, (getNumericalArrayVals(overviewReport.quarters, ns + 'Quarterly_Bookings_Goal__c', divisor )[3] - getNumericalArrayVals(overviewReport.quarters, ns + 'Total_Bookings__c', divisor )[3])],
                    borderColor: '#1abc9c',
                    dashStyle: 'dash',
                    maxPointWidth: 40
                }, {
                    name: 'Sales Only',
                    data: getNumericalArrayVals(overviewReport.quarters, ns + 'Sales_Only_Bookings__c', divisor ),
                    maxPointWidth: 40
                }, {
                    name: 'Marketing Influenced',
                    data: getNumericalArrayVals(overviewReport.quarters, ns + 'Marketing_Influenced_Bookings__c', divisor ),
                    maxPointWidth: 40
                }, {
                    name: 'Marketing Sourced',
                    data: getNumericalArrayVals(overviewReport.quarters, ns + 'Marketing_Sourced_Bookings__c', divisor ),
                    maxPointWidth: 40
                }]
            });
        }

        function drawPipelineChart() {
            var divisor = getDivisor( Math.max.apply(null, getArrayVals( overviewReport.quarters, ns + 'Total_Pipeline__c' ) ) );
            var divisor_suffix = getDivisorChar( Math.max.apply(null, getArrayVals( overviewReport.quarters, ns + 'Total_Pipeline__c' ) ) );
            var divisor_word = getDivisorWord( divisor  );
            var maxY = Math.max.apply(null, getArrayVals( overviewReport.quarters, ns + 'Total_Pipeline__c' ));
            var maxGoal = (getArrayVals(overviewReport.quarters, ns + 'Quarterly_Pipeline_Goal__c' )[3]);

            if( maxGoal > maxY ) maxY = maxGoal;
            
            maxY = (maxY+1) / divisor;

            j$('.pipeline-chart').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: ''
                },
                xAxis: {
                    categories: overviewReport.quarterNames
                },
                yAxis: {
                    min: 0,
                    max: maxY,
                    title: {
                        text: '$ '+ divisor_word
                    },
                    labels: {
                        format: '{value}' + divisor_suffix 
                    },
                    tickAmount: 5
                },
                legend: {
                    enabled: false
                },
                credits: {
                    enabled: false
                },
                plotOptions: {
                    column: {
                        stacking: 'normal'
                    }
                },
                tooltip: {
                    valueSuffix: ' ' + divisor_suffix
                },
                colors: ['#ffffff', '#45b7e6', '#ffc708', '#1b527f'],
                series: [{
                    name: 'Goal',
                    data: [0, 0, 0, roundToTwo(getNumericalArrayVals(overviewReport.quarters, ns + 'Quarterly_Pipeline_Goal__c', divisor )[3] - getNumericalArrayVals(overviewReport.quarters, ns + 'Total_Pipeline__c',divisor )[3] )],
                    borderColor: '#1abc9c',
                    dashStyle: 'dash',
                    maxPointWidth: 40
                }, {
                    name: 'Sales Only',
                    data: getNumericalArrayVals(overviewReport.quarters, ns + 'Sales_Only_Pipeline__c', divisor ),
                    maxPointWidth: 40
                }, {
                    name: 'Marketing Influenced',
                    data: getNumericalArrayVals(overviewReport.quarters, ns + 'Marketing_Influenced_Pipeline__c', divisor ),
                    maxPointWidth: 40
                }, {
                    name: 'Marketing Sourced',
                    data: getNumericalArrayVals(overviewReport.quarters, ns + 'Marketing_Sourced_Pipeline__c', divisor ),
                    maxPointWidth: 40
                }]
            });
        }

        function drawTargetAccountsChart() {
            j$('.target-accounts-chart').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: ''
                },
                tooltip: {
                    enabled: false
                },
                credits: {
                    enabled: false
                },
                legend: {
                    align: 'right',
                    layout: 'vertical',
                    verticalAlign: 'top',
                    itemMarginTop: 20,
                    itemStyle: {
                        fontWeight: 'normal',
                        fontFamily: 'open-sans-light',
                        fontSize: '15px',
                        color: '#555555'
                    }
                },
                title: {
                    text: overviewReport.targetAccountMetrics['Total Target Accounts'],
                    align: 'center',
                    verticalAlign: 'middle',
                    x: -135.15,
                    y: 10,
                    style: {
                        fontFamily: 'open-sans-light',
                        fontSize: '27px',
                        color: '#555555'
                    }
                },
                colors: ['#45b7e6', '#1b527f', '#ffc708'],
                plotOptions: {
                    pie: {
                        allowPointSelect: false,
                        cursor: 'pointer',
                        dataLabels: {
                            style: {
                                textShadow: false,
                                fontWeight: 400,
                                fontFamily: 'Open Sans',
                                fontSize: '11px'
                            },
                            color: 'white',
                            enabled: true,
                            distance: -20,
                            formatter: function(){
                                return Math.round(this.y) + '%';
                            },
                        },
                        showInLegend: true,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    innerSize: '65%',
                    states: {
                        hover: {
                            enabled: false
                        }
                    },
                    data: [{
                        name: 'Customer',
                        y: overviewReport.targetAccountMetrics['Customer %']
                    }, {
                        name: 'Accounts With Opportunities',
                        y: overviewReport.targetAccountMetrics['Accounts With Opportunities %']
                    }, {
                        name: 'Accounts Without Opportunities',
                        y: overviewReport.targetAccountMetrics['Accounts Without Opportunities %']
                    }]
                }]
            });
        }
    </script>
</apex:page>