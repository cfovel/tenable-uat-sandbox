<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Custom Object to manage the Change Control process for SFDC. Allows tracking of requests and the implementation of specific changes.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Change_Control__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Change Control</label>
        <referenceTo>Change_Control__c</referenceTo>
        <relationshipName>Change_Objects</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Change_Notes__c</fullName>
        <deprecated>false</deprecated>
        <description>Any description as to the change made</description>
        <externalId>false</externalId>
        <label>Change Notes</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Item_Changed__c</fullName>
        <deprecated>false</deprecated>
        <description>Selection of which detailed sub-item is being modified.</description>
        <externalId>false</externalId>
        <label>Item Changed</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <controllingField>Object_Affected__c</controllingField>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Assignment Rule</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Custom App</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Custom Field</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Custom Link</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Custom Object</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Custom s-Control</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Custom Tab</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Dashboard</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Field Updates</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Flow</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Page Layout</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Process Builder</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Profiles</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Public Groups</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Record Type</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Report</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Sharing Rules</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Standard Field</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>System Policies</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Validation Rules</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Workflow Rules</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
            <valueSettings>
                <controllingFieldValue>Products</controllingFieldValue>
                <controllingFieldValue>Custom Object</controllingFieldValue>
                <controllingFieldValue>Users</controllingFieldValue>
                <controllingFieldValue>Campaigns</controllingFieldValue>
                <controllingFieldValue>Leads</controllingFieldValue>
                <controllingFieldValue>Accounts</controllingFieldValue>
                <controllingFieldValue>Contacts</controllingFieldValue>
                <controllingFieldValue>Opportunities</controllingFieldValue>
                <controllingFieldValue>Contracts</controllingFieldValue>
                <controllingFieldValue>Cases</controllingFieldValue>
                <controllingFieldValue>Solutions</controllingFieldValue>
                <valueName>Custom Field</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Products</controllingFieldValue>
                <controllingFieldValue>Custom Object</controllingFieldValue>
                <controllingFieldValue>Home</controllingFieldValue>
                <controllingFieldValue>Campaigns</controllingFieldValue>
                <controllingFieldValue>Leads</controllingFieldValue>
                <controllingFieldValue>Accounts</controllingFieldValue>
                <controllingFieldValue>Contacts</controllingFieldValue>
                <controllingFieldValue>Opportunities</controllingFieldValue>
                <controllingFieldValue>Contracts</controllingFieldValue>
                <controllingFieldValue>Cases</controllingFieldValue>
                <controllingFieldValue>Solutions</controllingFieldValue>
                <valueName>Custom Link</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Products</controllingFieldValue>
                <controllingFieldValue>Custom Object</controllingFieldValue>
                <controllingFieldValue>Home</controllingFieldValue>
                <controllingFieldValue>Campaigns</controllingFieldValue>
                <controllingFieldValue>Leads</controllingFieldValue>
                <controllingFieldValue>Accounts</controllingFieldValue>
                <controllingFieldValue>Contacts</controllingFieldValue>
                <controllingFieldValue>Opportunities</controllingFieldValue>
                <controllingFieldValue>Contracts</controllingFieldValue>
                <controllingFieldValue>Cases</controllingFieldValue>
                <controllingFieldValue>Solutions</controllingFieldValue>
                <valueName>Custom s-Control</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Products</controllingFieldValue>
                <controllingFieldValue>Dashboards</controllingFieldValue>
                <controllingFieldValue>Documents</controllingFieldValue>
                <controllingFieldValue>Custom Object</controllingFieldValue>
                <controllingFieldValue>Users</controllingFieldValue>
                <controllingFieldValue>Campaigns</controllingFieldValue>
                <controllingFieldValue>Leads</controllingFieldValue>
                <controllingFieldValue>Accounts</controllingFieldValue>
                <controllingFieldValue>Contacts</controllingFieldValue>
                <controllingFieldValue>Opportunities</controllingFieldValue>
                <controllingFieldValue>Forecasts</controllingFieldValue>
                <controllingFieldValue>Contracts</controllingFieldValue>
                <controllingFieldValue>Cases</controllingFieldValue>
                <controllingFieldValue>Solutions</controllingFieldValue>
                <valueName>Dashboard</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Products</controllingFieldValue>
                <controllingFieldValue>Reports</controllingFieldValue>
                <controllingFieldValue>Dashboards</controllingFieldValue>
                <controllingFieldValue>Documents</controllingFieldValue>
                <controllingFieldValue>Custom Object</controllingFieldValue>
                <controllingFieldValue>Users</controllingFieldValue>
                <controllingFieldValue>Home</controllingFieldValue>
                <controllingFieldValue>Campaigns</controllingFieldValue>
                <controllingFieldValue>Leads</controllingFieldValue>
                <controllingFieldValue>Accounts</controllingFieldValue>
                <controllingFieldValue>Contacts</controllingFieldValue>
                <controllingFieldValue>Opportunities</controllingFieldValue>
                <controllingFieldValue>Forecasts</controllingFieldValue>
                <controllingFieldValue>Contracts</controllingFieldValue>
                <controllingFieldValue>Cases</controllingFieldValue>
                <controllingFieldValue>Solutions</controllingFieldValue>
                <valueName>Other</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Products</controllingFieldValue>
                <controllingFieldValue>Custom Object</controllingFieldValue>
                <controllingFieldValue>Home</controllingFieldValue>
                <controllingFieldValue>Campaigns</controllingFieldValue>
                <controllingFieldValue>Leads</controllingFieldValue>
                <controllingFieldValue>Accounts</controllingFieldValue>
                <controllingFieldValue>Contacts</controllingFieldValue>
                <controllingFieldValue>Opportunities</controllingFieldValue>
                <controllingFieldValue>Contracts</controllingFieldValue>
                <controllingFieldValue>Cases</controllingFieldValue>
                <controllingFieldValue>Solutions</controllingFieldValue>
                <valueName>Page Layout</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Products</controllingFieldValue>
                <controllingFieldValue>Reports</controllingFieldValue>
                <controllingFieldValue>Dashboards</controllingFieldValue>
                <controllingFieldValue>Documents</controllingFieldValue>
                <controllingFieldValue>Custom Object</controllingFieldValue>
                <controllingFieldValue>Users</controllingFieldValue>
                <controllingFieldValue>Home</controllingFieldValue>
                <controllingFieldValue>Campaigns</controllingFieldValue>
                <controllingFieldValue>Leads</controllingFieldValue>
                <controllingFieldValue>Accounts</controllingFieldValue>
                <controllingFieldValue>Contacts</controllingFieldValue>
                <controllingFieldValue>Opportunities</controllingFieldValue>
                <controllingFieldValue>Forecasts</controllingFieldValue>
                <controllingFieldValue>Contracts</controllingFieldValue>
                <controllingFieldValue>Cases</controllingFieldValue>
                <controllingFieldValue>Solutions</controllingFieldValue>
                <valueName>Profiles</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Products</controllingFieldValue>
                <controllingFieldValue>Reports</controllingFieldValue>
                <controllingFieldValue>Documents</controllingFieldValue>
                <controllingFieldValue>Custom Object</controllingFieldValue>
                <controllingFieldValue>Users</controllingFieldValue>
                <controllingFieldValue>Campaigns</controllingFieldValue>
                <controllingFieldValue>Leads</controllingFieldValue>
                <controllingFieldValue>Accounts</controllingFieldValue>
                <controllingFieldValue>Contacts</controllingFieldValue>
                <controllingFieldValue>Opportunities</controllingFieldValue>
                <controllingFieldValue>Forecasts</controllingFieldValue>
                <controllingFieldValue>Contracts</controllingFieldValue>
                <controllingFieldValue>Cases</controllingFieldValue>
                <controllingFieldValue>Solutions</controllingFieldValue>
                <valueName>Report</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Products</controllingFieldValue>
                <controllingFieldValue>Custom Object</controllingFieldValue>
                <controllingFieldValue>Users</controllingFieldValue>
                <controllingFieldValue>Campaigns</controllingFieldValue>
                <controllingFieldValue>Leads</controllingFieldValue>
                <controllingFieldValue>Accounts</controllingFieldValue>
                <controllingFieldValue>Contacts</controllingFieldValue>
                <controllingFieldValue>Opportunities</controllingFieldValue>
                <controllingFieldValue>Contracts</controllingFieldValue>
                <controllingFieldValue>Cases</controllingFieldValue>
                <controllingFieldValue>Solutions</controllingFieldValue>
                <valueName>Standard Field</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Products</controllingFieldValue>
                <controllingFieldValue>Custom Object</controllingFieldValue>
                <controllingFieldValue>Users</controllingFieldValue>
                <controllingFieldValue>Campaigns</controllingFieldValue>
                <controllingFieldValue>Leads</controllingFieldValue>
                <controllingFieldValue>Accounts</controllingFieldValue>
                <controllingFieldValue>Contacts</controllingFieldValue>
                <controllingFieldValue>Opportunities</controllingFieldValue>
                <controllingFieldValue>Contracts</controllingFieldValue>
                <controllingFieldValue>Cases</controllingFieldValue>
                <controllingFieldValue>Solutions</controllingFieldValue>
                <valueName>Validation Rules</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Custom Object</controllingFieldValue>
                <valueName>Custom App</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Custom Object</controllingFieldValue>
                <valueName>Custom Object</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Custom Object</controllingFieldValue>
                <valueName>Custom Tab</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Custom Object</controllingFieldValue>
                <controllingFieldValue>Users</controllingFieldValue>
                <controllingFieldValue>Campaigns</controllingFieldValue>
                <controllingFieldValue>Leads</controllingFieldValue>
                <controllingFieldValue>Accounts</controllingFieldValue>
                <controllingFieldValue>Contacts</controllingFieldValue>
                <controllingFieldValue>Opportunities</controllingFieldValue>
                <controllingFieldValue>Contracts</controllingFieldValue>
                <controllingFieldValue>Cases</controllingFieldValue>
                <controllingFieldValue>Solutions</controllingFieldValue>
                <valueName>Field Updates</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Custom Object</controllingFieldValue>
                <controllingFieldValue>Campaigns</controllingFieldValue>
                <controllingFieldValue>Leads</controllingFieldValue>
                <controllingFieldValue>Accounts</controllingFieldValue>
                <controllingFieldValue>Contacts</controllingFieldValue>
                <controllingFieldValue>Opportunities</controllingFieldValue>
                <controllingFieldValue>Contracts</controllingFieldValue>
                <controllingFieldValue>Cases</controllingFieldValue>
                <valueName>Flow</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Custom Object</controllingFieldValue>
                <controllingFieldValue>Campaigns</controllingFieldValue>
                <controllingFieldValue>Leads</controllingFieldValue>
                <controllingFieldValue>Accounts</controllingFieldValue>
                <controllingFieldValue>Contacts</controllingFieldValue>
                <controllingFieldValue>Opportunities</controllingFieldValue>
                <controllingFieldValue>Contracts</controllingFieldValue>
                <controllingFieldValue>Cases</controllingFieldValue>
                <valueName>Process Builder</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Custom Object</controllingFieldValue>
                <controllingFieldValue>Users</controllingFieldValue>
                <controllingFieldValue>Leads</controllingFieldValue>
                <controllingFieldValue>Accounts</controllingFieldValue>
                <controllingFieldValue>Contacts</controllingFieldValue>
                <controllingFieldValue>Opportunities</controllingFieldValue>
                <controllingFieldValue>Contracts</controllingFieldValue>
                <controllingFieldValue>Cases</controllingFieldValue>
                <controllingFieldValue>Solutions</controllingFieldValue>
                <valueName>Public Groups</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Custom Object</controllingFieldValue>
                <controllingFieldValue>Campaigns</controllingFieldValue>
                <controllingFieldValue>Leads</controllingFieldValue>
                <controllingFieldValue>Accounts</controllingFieldValue>
                <controllingFieldValue>Contacts</controllingFieldValue>
                <controllingFieldValue>Opportunities</controllingFieldValue>
                <controllingFieldValue>Contracts</controllingFieldValue>
                <controllingFieldValue>Cases</controllingFieldValue>
                <controllingFieldValue>Solutions</controllingFieldValue>
                <valueName>Record Type</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Custom Object</controllingFieldValue>
                <controllingFieldValue>Users</controllingFieldValue>
                <controllingFieldValue>Leads</controllingFieldValue>
                <controllingFieldValue>Accounts</controllingFieldValue>
                <controllingFieldValue>Contacts</controllingFieldValue>
                <controllingFieldValue>Opportunities</controllingFieldValue>
                <controllingFieldValue>Contracts</controllingFieldValue>
                <controllingFieldValue>Cases</controllingFieldValue>
                <controllingFieldValue>Solutions</controllingFieldValue>
                <valueName>Sharing Rules</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Custom Object</controllingFieldValue>
                <controllingFieldValue>Users</controllingFieldValue>
                <controllingFieldValue>Campaigns</controllingFieldValue>
                <controllingFieldValue>Leads</controllingFieldValue>
                <controllingFieldValue>Accounts</controllingFieldValue>
                <controllingFieldValue>Contacts</controllingFieldValue>
                <controllingFieldValue>Opportunities</controllingFieldValue>
                <controllingFieldValue>Contracts</controllingFieldValue>
                <controllingFieldValue>Cases</controllingFieldValue>
                <controllingFieldValue>Solutions</controllingFieldValue>
                <valueName>Workflow Rules</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Users</controllingFieldValue>
                <valueName>System Policies</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Leads</controllingFieldValue>
                <valueName>Assignment Rule</valueName>
            </valueSettings>
        </valueSet>
    </fields>
    <fields>
        <fullName>Object_Affected__c</fullName>
        <deprecated>false</deprecated>
        <description>Select which is the object in SFDC that will be modified</description>
        <externalId>false</externalId>
        <label>Object Affected</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Home</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Campaigns</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Leads</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Accounts</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Contacts</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Opportunities</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Forecasts</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Contracts</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Cases</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Solutions</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Products</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Reports</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Dashboards</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Documents</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Custom Object</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Users</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Change Object</label>
    <nameField>
        <displayFormat>CCO-{00000000}</displayFormat>
        <label>Change Control Object Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Change Objects</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
