/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class AttachToCaseController {
    global AttachToCaseController() {

    }
    @RemoteAction
    global static CoveoV2.AttachToCaseController.AttachToCaseResponse AttachToCase(CoveoV2.AttachToCaseController.ResultToAttachArg result) {
        return null;
    }
    @RemoteAction
    global static CoveoV2.AttachToCaseController.AttachToCaseResponse DetachFromCase(String uriHash, Id sfkbid, Id CaseId) {
        return null;
    }
global class AttachToCaseResponse {
    global AttachToCaseResponse() {

    }
}
global class ResultToAttachArg {
    global ResultToAttachArg() {

    }
}
}
