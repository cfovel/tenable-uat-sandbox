({
  afterRender: function(cmp, helper) {
    'use strict';
    var initialized = cmp.get('v.initialized');

    // This is necessary because component life cycle in Salesforce
    // is not reliable when dealing with custom theme layouts.
    // When switching from page to page, the "scriptLoaded" event
    // won't always be triggered.
    // Since the dom is deleted when you change page, we need to "re-init" everything
    // on each new render.
    if (typeof Coveo !== 'undefined' && Coveo.StandaloneSearchbox && !cmp.get('v.scriptLoaded')) {
      cmp.set('v.scriptLoaded', true);
    }

    if (!initialized) {
      helper.init(cmp, helper);
    }
  },

  unrender: function(cmp) {
    'use strict';
    cmp.set('v.initialized', false);
  }
});