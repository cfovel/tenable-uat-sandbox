({
  /**
   * Logs the messages if the component is in `debug` mode
   *
   * @param  {Component} cmp                  Component
   * @param  {String} message                 Log message
   */
  debugLog: function(cmp, message) {
    'use strict';
    if (typeof cmp !== 'object' && message !== undefined && typeof cmp === 'string') {
      console.log('DEVELOPER YOU NEED TO SPECIFY THE COMPONENT TO THE DEBUGLOG METHOD');
      console.log(message);
    } else if (cmp.get('v.debug')) {
      console.log('[' + new Date().toISOString() + '] ' + message);
    }
  },

  init: function(cmp, helper) {
    'use strict';
    var initializationData = cmp.get('v.initializationData');
    var initializationDataRequested = cmp.get('v.initializationDataRequested');
    var scriptLoaded = cmp.get('v.scriptLoaded');
    var initialized = cmp.get('v.initialized');

    if (!initializationData && !initializationDataRequested && scriptLoaded) {
      helper.getInitializationData(cmp, helper);

      return;
    }

    if (scriptLoaded && initializationData && !initialized) {
      helper.initFramework(cmp, helper);
    }
  },

  getInitializationData: function(cmp, helper) {
    'use strict';
    helper.debugLog(cmp, 'getInitializationData');

    var getInitializationAction = cmp.get('c.getToken');
    var customTokenGeneration = cmp.get('v.customTokenGeneration');
    var searchHub = cmp.get('v.searchHub');
    var name = cmp.get('v.name');

    getInitializationAction.setParams({
      searchHub: searchHub,
      name: name
    });

    // Lets fire the GetSearchToken event if we want a custom token
    var deferredToken = Coveo.$.Deferred();

    if (customTokenGeneration) {
      helper.debugLog(cmp, 'Custom token generation');
      var getTokenEvent = cmp.getEvent('GetSearchToken');

      getTokenEvent.setParams({
        deferred: deferredToken
      });

      getTokenEvent.fire();
    }

    getInitializationAction.setCallback(this, function(initializationResponse) {
      helper.debugLog(cmp, 'getInitializationAction returned');

      var parsed = JSON.parse(initializationResponse.getReturnValue());
      cmp.set('v.initializationData', parsed);

      // The token promise, in case there is a custom token generation
      deferredToken.done(function(searchToken) {
        cmp.set('v.searchToken', searchToken);
        helper.initFramework(cmp, helper);
      });

      if (!customTokenGeneration) {
        if (parsed.errorTitle) {
          // Something went wrong, display the error
          helper.debugLog(cmp, 'Initialization returned with error');

          var errorDiv = Coveo.$('#error');
          errorDiv.show();

          var searchInterfaceElement = Coveo.$('#standaloneSearchbox');
          searchInterfaceElement.find('input').attr('disabled', true);

          var errorTitle = Coveo.$('#errorTitle');
          errorTitle.text(parsed.errorTitle);
        } else {
          // Token was retrieved, we can resolve the token promise
          helper.debugLog(cmp, 'Resolving with default token');
          deferredToken.resolve(parsed.token);
        }
      }
    });

    cmp.set('v.initializationDataRequested', true);
    $A.enqueueAction(getInitializationAction);
  },

  initFramework: function(cmp, helper) {
    'use strict';
    helper.debugLog(cmp, 'Searchbox initialization');

    var searchInterfaceElement = Coveo.$('#standaloneSearchbox');
    var initializationData = cmp.get('v.initializationData');
    var searchToken = cmp.get('v.searchToken');

    var searchHub = cmp.get('v.searchHub');
    var name = cmp.get('v.name');
    var searchPageName = cmp.get('v.searchPageName');
    var placeholder = cmp.get('v.placeholder');
    var enableQuerySuggestAddon = cmp.get('v.enableQuerySuggestAddon');
    var triggerQueryOnClear = cmp.get('v.triggerQueryOnClear');
    var enableQuerySyntax = cmp.get('v.enableQuerySyntax');
    var enableFieldAddon = cmp.get('v.enableFieldAddon');
    var enableQueryExtensionAddon = cmp.get('v.enableQueryExtensionAddon');

    Coveo.SearchEndpoint.endpoints[name] = new Coveo.SearchEndpoint({
      restUri: initializationData.platformUri + '/rest/search/',
      accessToken: searchToken,
      isGuestUser: initializationData.isGuestUser
    });

    searchInterfaceElement.coveo('init', {
      SearchInterface: {
        endpoint: Coveo.SearchEndpoint.endpoints[name],
        enableHistory: false,
        autoTriggerQuery: false
      },
      Analytics: {
        token: searchToken,
        endpoint: initializationData.analyticUri,
        searchHub: searchHub
      },
      StandaloneSearchbox: {
        searchPageName: searchPageName,
        placeholder: placeholder,
        enableQuerySuggestAddon: enableQuerySuggestAddon,
        triggerQueryOnClear: triggerQueryOnClear,
        enableQuerySyntax: enableQuerySyntax,
        enableFieldAddon: enableFieldAddon,
        enableQueryExtensionAddon: enableQueryExtensionAddon
      }
    });

    cmp.set('v.initialized', true);
  }
});