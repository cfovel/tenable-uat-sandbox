({
  /**
   * onScriptLoaded - Action fired when the script are loaded
   *
   * @param  {Component} cmp  The component
   * @param  {Event} event    The event
   * @param  {Object} helper  The component helper funcitons
   */
  onScriptLoaded: function(cmp, event, helper) {
    'use strict';

    helper.debugLog(cmp, 'onScriptLoaded');
    cmp.set('v.scriptLoaded', true);

    helper.init(cmp, helper);
  }
});