({
  /**
   * interfaceLoaded - Loads the component interface
   *
   * @param  {Component} cmp  The component
   * @param  {Event} event    The event
   * @param  {Object} helper  The component helper funcitons
   */
  interfaceLoaded: function(cmp, event, helper) {
    'use strict';
    helper.debugLog(cmp, 'BaseController:interfaceLoaded');

    if (helper.attributeIsTrueOrTruthy(cmp.get('v.interfaceLoaded'))) {
      var interfaceContentLoaded = cmp.getEvent('InterfaceContentLoaded');

      interfaceContentLoaded.fire();
    }
  },

   /**
   * executeComponentInitilization - Execute the component initialization.
   * At this point, the UI isn't rendered and the scripts are not loaded.
   *
   * @param  {Component} cmp  The component
   * @param  {Event} event    The event
   * @param  {Object} helper  The component helper funcitons
   */
  executeComponentInitilization: function(cmp, event, helper) {
    'use strict';
    helper.debugLog(cmp, 'BaseController:executeComponentInitilization');

    window.instanceOf = function(arg1, arg2) {
      var returnValue;

      if (arg1 === null || arg1 === undefined || arg2 === null || arg2 === undefined) {
        return false;
      }

      if (arg2 === HTMLElement || arg2 === HTMLInputElement ) {
        returnValue = typeof arg1 === 'object' &&
        'nodeType' in arg1 &&
        arg1.nodeType === 1 &&
        arg1.cloneNode !== undefined;

      } else if (arg2 === Window) {
        returnValue = arg1.toString().indexOf('Window') > -1;
      } else {
        returnValue = arg1 instanceof arg2;
      }

      return returnValue;
    };

    // For custom scripts execution.
    if (window.coveoCustomScripts === undefined) {
      window.coveoCustomScripts = [];
    }

    var name = cmp.get('v.name');
    // Check cmp name is valid
    if ( !helper.isValidLightningComponentName( name ) ) {
      var message = 'The name "' + cmp.get('v.name') + '" is not a valid component name.' +
        ' Your name can only contain alphanumeric and underscore characters, must begin with a letter,' +
        ' and cannot end with an underscore or contain two consecutive underscores.'
      helper.addErrorMessage(cmp, message, '')
    }


    // Set complex default values.
    cmp.set('v.searchContext', {});
    helper.updateIsLivePreviewBuilder(cmp);

    if (helper.attributeIsTrueOrTruthy(cmp.get('v.debug'))) {
      window.CoveoStartTime = Date.now();
    }

    // Load the custom scripts if there are any.
    if (helper.hasCustomScripts(cmp)) {
      helper.debugLog(cmp, 'BaseController:executeComponentInitilization - loadCustomScripts');
      helper.loadCustomScripts(cmp, helper);
    }

    var getInitializationAction = helper.getInitializationAction(cmp, helper);
    $A.enqueueAction(getInitializationAction);
  },

   /**
   * interfaceInitialized - trigger an interface initialized event.
   *
   * @param  {Component} cmp  The component
   * @param  {Event} event    The event
   * @param  {Object} helper  The component helper funcitons
   */
  interfaceInitialized: function(cmp, event, helper) {
    'use strict';
    helper.debugLog(cmp, 'BaseController:interfaceInitialized');
    if (helper.attributeIsTrueOrTruthy(cmp.get('v.interfaceInitialized'))) {
      var interfaceContentInitializedEvent = cmp.getEvent('InterfaceContentInitialized');

      interfaceContentInitializedEvent.fire();
    }
  },

  /**
   * doneRendering - Action fired when the component has done to render
   *
   * @param  {Component} cmp  The component
   * @param  {Event} event    The event
   * @param  {Object} helper  The component helper funcitons
   */
  doneRendering: function(cmp, event, helper) {
    'use strict';
    helper.debugLog(cmp, 'BaseController:doneRendering');

    helper.updateIsLivePreviewBuilder(cmp);

    if (cmp.get('v.interfaceLoaded') &&
        cmp.get('v.isResourcesLoaded') &&
        cmp.get('v.isEndpointSet') &&
        !cmp.get('v.interfaceInitialized')) {
      helper.debugLog(cmp, 'BaseController:initializing the interface.');
      var name = cmp.get('v.name');
      var searchHub = cmp.get('v.searchHub');

      if (searchHub === undefined || searchHub === '') {
        searchHub = name;
      }

      helper.initializeInterface(cmp, helper, name, searchHub);
    } else {
      if (!cmp.get('v.interfaceLoaded')) {
        helper.debugLog(cmp, 'BaseController:interface not loaded yet.');
      }
      if (!cmp.get('v.isResourcesLoaded')) {
        helper.debugLog(cmp, 'BaseController:resources not loaded yet.');
      }
      if (!cmp.get('v.isEndpointSet')) {
        helper.debugLog(cmp, 'BaseController:endpoint not set yet.');
      }

      if (cmp.get('v.interfaceInitialized')) {
        helper.debugLog(cmp, 'BaseController:interface already initialized.');
      }
    }
  },

  /**
   * onScriptLoaded - Action fired when the script are loaded
   *
   * @param  {Component} cmp  The component
   * @param  {Event} event    The event
   * @param  {Object} helper  The component helper funcitons
   */
  onScriptLoaded: function (cmp, event, helper) {
    'use strict';
    helper.debugLog(cmp, 'BaseController:onScriptLoaded');

    if (window.Coveo === undefined) {
      // There was a problem with the script loading. Maybe LockerService?
      helper.debugLog(cmp, 'BaseController:onScriptLoaded - Coveo script not loaded correctly. Stopping everything.');

      return;
    }

    cmp.set('v.isComponentScriptsLoaded', true);

    helper.afterScriptLoaded(cmp, helper);
  },

  /**
   * Action fired when the custom scripts are loaded
   *
   * @param  {Component} cmp  The component
   * @param  {Event} event    The event
   * @param  {Object} helper  The component helper funcitons
   */
  onCustomScriptLoaded: function (cmp, event, helper) {
    'use strict';
    helper.debugLog(cmp, 'BaseController:onCustomScriptLoaded');
    cmp.set('v.isCustomScriptsLoaded', true);

    helper.afterScriptLoaded(cmp, helper);
  },

  /**
   * Initialize the component. This means initializing the Coveo Search interfaces
   * when the right conditions are met.
   *
   * @param {any} cmp The component.
   * @param {any} event The event.
   * @param {any} helper The helper.
   */
  initializeComponent: function(cmp, event, helper) {
    'use strict';
    helper.debugLog(cmp, 'BaseController:initializeComponent');

    var initializationData = cmp.get('v.initializationData');
    var isAdmin = cmp.get('v.isAdmin');

    if (initializationData) {
      if (initializationData.error) {
        helper.debugLog(cmp, 'BaseHelper:BaseController::initializeComponent-errorTitle');

        if (isAdmin) {
          // This sucks : The iframe we're in is sandboxed in preview mode :
          // We can't escape with a target _top or window.open
          // So we cannot provide a direct link
          helper.addErrorMessage(cmp, initializationData.errorTitle,
            'Access the Configuration page installed with the Coveo package.')
        } else {
          helper.addErrorMessage(cmp, initializationData.errorTitle, '')
        }

      } else {
        if (!cmp.get('v.interfaceLoaded')) {
          helper.fillPageContent(cmp, helper);
        } else {
          if (!initializationData) {
            helper.debugLog(cmp, 'BaseController:initialize - Initialization Data not loaded yet.');
          }
          if (cmp.get('v.interfaceLoaded')) {
            helper.debugLog(cmp, 'BaseController:initialize Interface already filled.');
          }
        }

        // Make sure everything is loaded correctly.
        if (initializationData && cmp.get('v.isResourcesLoaded')) {
          helper.initializeComponent(cmp, helper);
        } else {
          if (!window) {
            helper.debugLog(cmp, 'BaseController:window is undefined.');
          }
          if (window && window.Coveo === undefined) {
            helper.debugLog(cmp, 'BaseController:Coveo is not loaded yet.');
          }
        }
      }
    }
  },

  /**
   * Handle the error info message change.
   *
   * @param {any} cmp The component.
   * @param {any} event The event.
   * @param {any} helper The helper.
   */
  handleErrorMessageDisplayChange: function(cmp, event, helper) {
    'use strict';

    helper.debugLog(cmp, 'BaseController:handleErrorMessageDisplayChange - ' +
      'Error info:' + JSON.stringify( cmp.get('v.errorMessageDisplay') ));
  },

  /**
   * We load the interface editor scripts seperatly from the rest.
   * When the script are loaded, fire the InterfaceEditorResourcesLoaded event.
   * p.s. component DOM modifications doesn't work in ScriptLoaded events, that's why we must fire an event.
   *
   * @param {any} cmp The component.
   * @param {any} event The event.
   * @param {any} helper The helper.
   */
  onInterfaceEditorScriptLoaded: function(cmp, event, helper) {
    'use strict';

    helper.debugLog(cmp, 'BaseController:onInterfaceEditorScriptLoaded');

    cmp.set('v.isInterfaceEditorResourcesLoaded', true);

    var interfaceEditorRresourcesLoadedEvent = cmp.getEvent('InterfaceEditorResourcesLoaded');

    interfaceEditorRresourcesLoadedEvent.fire();
  },

  /**
   * Initialize the admin UI. It means to load the Interface Creator (if needed) and the interface editor.
   *
   * @param {any} cmp The component.
   * @param {any} event The event.
   * @param {any} helper The helper.
   */
  initializeInterfaceEditor: function(cmp, event, helper) {
    'use strict';
    helper.debugLog(cmp, 'BaseController:initializeInterfaceEditor');

    if ( cmp.get('v.isInterfaceEditorResourcesLoaded') ) {

      helper.debugLog(cmp, 'BaseController:initializeInterfaceEditor - isResourcesLoaded:true');
      helper.constructAdminUi(cmp, helper);

    }
  },

   /**
   * Handle change on the allow fallback on admin attribute.
   *
   * @param {any} cmp The component.
   * @param {any} event The event.
   * @param {any} helper The helper.
   */
  handleAllowFallbackOnAdminChange: function(cmp, event, helper) {
    'use strict';

    helper.debugLog(cmp, 'BaseController:handleAllowFallbackOnAdminChange');
    if (cmp.get('v.allowFallbackOnAdmin')) {
      helper.showFallbackToAdminWarning(cmp);
    } else {
      helper.hideFallbackToAdminWarning(cmp);
    }
  },

  /**
   * Hide the advanced settings.
   *
   * @param {any} cmp The component.
   * @param {any} event The event.
   * @param {any} helper The helper.
   */
  hideAdvancedSettings: function(cmp, event, helper) {
    'use strict';
    helper.debugLog(cmp, 'BaseController:hideAdvancedSettings');

    cmp.set('v.displayAdvancedSettings', false);
  },

  /**
   * Handle any change to the interactions enable variable.
   *
   * @param {any} cmp The component.
   * @param {any} event The event.
   * @param {any} helper The helper.
   */
  handleIsLivePreviewBuilderChange: function(cmp, event, helper) {
    'use strict';
    helper.debugLog(cmp, 'BaseController:handleIsLivePreviewBuilderChange');
    var warning = cmp.find('CoveoUsePreviewModeWarning');

    if (cmp.get('v.isLivePreviewBuilder')) {
      $A.util.addClass(warning, 'visibleWarning');
    } else {
      $A.util.removeClass(warning, 'visibleWarning');
    }
  }
})