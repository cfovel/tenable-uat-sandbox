({
  /**
   * unrender - Unrender the component
   *
   * @param  {component} cmp    Component
   * @param  {Object} helper    Base component helper object to access its controller functions
   * @return {Component}        Unrendered component
   */
  unrender: function(cmp, helper) {
    'use strict';
    helper.debugLog(cmp, 'BaseRenderer:unrender');
    var name = cmp.get('v.name');
    var searchEndpoint = helper.getEndpoint(cmp, name);
    var endpointName = helper.getEndpointName(name);
    var searchInterface = helper.getSearchInterface(cmp);

    if (searchInterface && searchInterface.coveo) {
      // Nuke the search interface and search endpoint bound events.
      // By triggering the nuke method, the Coveo library will remove all event handlers bound to the window and body.
      // This way, by removing the Coveo DOM elements, all Coveo event handlers will be dead.
      searchInterface.coveo('nuke');
    }
    if (searchEndpoint) {
      searchEndpoint.nuke();
    }
    if (Coveo && Coveo.SearchEndpoint && Coveo.SearchEndpoint.endpoints) {
      Coveo.SearchEndpoint.endpoints[endpointName] = undefined;
    }

    // Salesforce will remove the DOM elements.
    return this.superUnrender();
  }
})