({
  /**
   * Check if the page is ready to render
   *
   * @param  {Component} cmp                 Component.
   * @param  {Array}                          repos Repositories
   * @return {Boolean}                        true if `repo` is undefined or `repo` length is 0 or `Coveo` is undefined
   */
  pageIsNotReadyToRender: function (cmp, repos) {
    'use strict';

    return repos === undefined ||
      repos.length === 0 ||
      window.Coveo === undefined ||
      !cmp.get('v.isResourcesLoaded');
  },

  /**
   * Wrapping method to check if the response is a success.
   *
   * @param  {Object} lightningResponse       Response from salesforce
   * @param  {Function} callback              Callback function
   * @param  {Object} context                 Context
   */
  successOrLogError: function (lightningResponse, callback, context) {
    'use strict';

    if (lightningResponse.getState() === 'SUCCESS') {
      callback.call(context, lightningResponse.getReturnValue());
    } else {
      console.log(lightningResponse);
    }
  },

  /**
   * Create the interface editor
   *
   * @param  {Component} cmp                  Component
   * @param  {String} name                    Component name
   * @param  {String} endpoint                API endpoint
   * @param  {Object} repos                   Repositories
   * @param  {Object} helper                  Helper
   */
  createInterfaceCreator: function (cmp, name, endpoint, repos, helper) {
    'use strict';

    this.debugLog(cmp, 'BaseHelper:createInterfaceCreator');

    new Coveo.InterfaceEditor.InterfaceCreator({
      create: $A.getCallback(function (creationName, repositories) {
        helper.create(cmp, repositories, name, helper);

        try {
          helper.sendTrack(cmp, 'Lightning Search Page Created');
        } catch (e) {
          helper.debugLog(cmp, e.message);
        }
      }),
      repositories: repos,
      endpoint: endpoint,
      authenticate: $A.getCallback(function () {
        return helper.authenticate(cmp);
      })
    }).dom.appendTo('#CoveoPageCreator');

    this.sendPage(cmp, 'Lightning Interface Creator');
  },

  /**
   * Sends a page event to Segment
   *
   * @param  {Component}  cmp        component
   * @param  {String}     pageName   The viewed page name
   */
  sendPage: function (cmp, pageName) {
    'use strict'

    this.debugLog(cmp, 'BaseHelper:sendPage');

    try {
      var sendPageAction = cmp.get('c.sendPage');

      sendPageAction.setParams({ name: pageName });
      $A.enqueueAction(sendPageAction);
    } catch (e) {
      this.debugLog(cmp, e.message);
    }
  },

  /**
   * Sends a tack event to Segment
   *
   * @param  {Component}  cmp         Component
   * @param  {String}     eventName   The Track event name
   */
  sendTrack: function (cmp, eventName) {
    'use strict'

    this.debugLog(cmp, 'BaseHelper:sendPage');

    try {
      var sendTrackAction = cmp.get('c.sendTrack');

      sendTrackAction.setParams({ event: eventName });
      $A.enqueueAction(sendTrackAction);
    } catch (e) {
      this.debugLog(cmp, e.message);
    }
  },

  /**
   * Show the interface editor toolbox
   *
   * @param  {type} cmp                       Component
   * @param  {type} repos                     Repositories
   * @param  {type} name                      Component name
   * @param  {type} searchHub                 Search hub
   * @param  {type} helper                    Helper
   */
  showInterfaceEditorToolbox: function (cmp, repos, name, searchHub, helper) {
    'use strict';

    this.debugLog(cmp, 'BaseHelper:showInterfaceEditorToolbox');


    var ie = new Coveo.InterfaceEditor.LightningInterfaceToolbox({
      repositories: repos,
      currentPage: name,
      edit: $A.getCallback(function (id) {
        helper.edit(id, cmp, helper);
      }),
      delete: $A.getCallback(function (id) {
        helper.del(id, cmp);
      }),
      cmp: cmp
    });

    var toolbox = cmp.find('CoveoInterfaceEditorToolbox');

    // Add a check for undefined value
    if (toolbox) {
      ie.dom.appendTo(toolbox.getElement());
    } else {
      this.debugLog(cmp, 'BaseHelper:showInterfaceEditorToolbox - Couldn\'t find toolbox.');
    }
  },

  /**
   * Show the warning about allowing fallback to admin.
   *
   * @param  {type} cmp                       Component
   */
  showFallbackToAdminWarning: function (cmp) {
    'use strict';
    this.debugLog(cmp, 'BaseHelper:showFallbackToAdminWarning');

    var warning = cmp.find('CoveoFallbackToAdminWarning');

    $A.util.addClass(warning, 'visibleWarning');
  },

  /**
   * Hide the warning about allowing fallback to admin.
   *
   * @param  {type} cmp                       Component
   */
  hideFallbackToAdminWarning: function (cmp) {
    'use strict';
    this.debugLog(cmp, 'BaseHelper:hideFallbackToAdminWarning');

    var warning = cmp.find('CoveoFallbackToAdminWarning');

    $A.util.removeClass(warning, 'visibleWarning');
  },

  /**
   * @param  {type} cmp Component
   * @returns {String} the page type to create
   */
  getPageTypeToCreate: function (cmp) {
    'use strict';
    if (this.isSearchComponent(cmp)) {
      return 'DefaultSearch';
    } else if (this.isCaseCreationComponent(cmp)) {
      return 'CaseCreation';
    }

    throw new Error('Implementations needs to specify their page type!');
  },

  /**
   * Create a Search page using the provided parameters.
   *
   * @param  {Component} cmp                  Component
   * @param  {Object} repositories            Repositories
   * @param  {String} name                    Component name
   * @param  {String} helper                  Helper
   */
  create: function (cmp, repositories, name, helper) {
    'use strict';

    this.debugLog(cmp, 'BaseHelper:create');
    if (this.isLivePreview() ||
      this.isSitePreview()) {
      this.createFromLivePreview(cmp, repositories, name, helper);
    } else {
      this.createFromPublishedSite(cmp, repositories, name, helper);
    }
  },

  /**
   * Create a component from the live preview
   *
   * @param  {Component} cmp                  Component
   * @param  {Object} repositories            Repositories
   * @param  {String} name                    Component name
   * @param  {String} helper                  Helper
   */
  createFromLivePreview: function (cmp, repositories, name, helper) {
    'use strict';

    helper.debugLog(cmp, 'BaseHelper:createFromLivePreview');
    // Live preview is under another domain than normal sf pages.
    // Session ID is invalid to create pages using metadata API.
    // Create an iframe on a visual force page which will do the job for us...

    var pageUrlAction = cmp.get('c.getPageCreatorUrl');

    pageUrlAction.setCallback(this, function (responsePageUrl) {
      if (pageUrlAction.getState() === 'SUCCESS') {
        var iframe = document.createElement('iframe');

        $A.util.addClass(iframe, 'coveo-iframe-page-creator');

        var baseUrl = responsePageUrl.getReturnValue();
        var iframeSource = baseUrl + '?pageName=' + encodeURIComponent(name) +
          '&repos=' + encodeURIComponent(repositories.join(',')) +
          '&pageType=' + encodeURIComponent(helper.getPageTypeToCreate(cmp));

        iframe.src = iframeSource;
        iframe.addEventListener('load', $A.getCallback(function () {
          var getLoaderAndRepoAction = cmp.get('c.getLoaderAndRepo');

          getLoaderAndRepoAction.setParams({
            name: name
          });
          getLoaderAndRepoAction.setCallback(this, function (responseLoaderAndRepo) {
            if (responseLoaderAndRepo.getState() === 'SUCCESS') {
              helper.setLoaderAndRepoAttribute(cmp, responseLoaderAndRepo, helper);
            } else {
              helper.debugLog(cmp, responsePageUrl);
            }
          });
          $A.enqueueAction(getLoaderAndRepoAction);

          iframe.parentNode.removeChild(iframe);
        }));
        cmp.getElement().appendChild(iframe);
      } else if (pageUrlAction.getState() === 'ERROR') {
        helper.debugLog(cmp, pageUrlAction.getError());
      }
    });
    $A.enqueueAction(pageUrlAction);
  },

  /**
   * Create a component from a published site
   *
   * @param  {Component} cmp                  Component
   * @param  {Object} repositories            Repositories
   * @param  {String} name                    Component name
   * @param  {String} helper                  helper
   */
  createFromPublishedSite: function (cmp, repositories, name, helper) {
    'use strict';

    helper.debugLog(cmp, 'BaseHelper:createFromPublishedSite');
    var createAction = cmp.get('c.create');

    createAction.setParams({
      reposID: repositories.join(','),
      name: encodeURIComponent(name),
      pageType: helper.getPageTypeToCreate(cmp)
    });
    createAction.setCallback(this, function (responseCreate) {
      if (responseCreate.getState() === 'SUCCESS') {
        helper.setLoaderAndRepoAttribute(cmp, responseCreate, helper);
      } else {
        console.log(responseCreate);
      }
    });
    $A.enqueueAction(createAction);
  },

  /**
  * Will parse the string for script templates and register it to the Coveo framework.
  *
  * @param  {string} content The HTML page content as a string.
  */
  registerTemplates: function (content) {
    'use strict';

    var domParser = new DOMParser();

    var parsedComponent = domParser.parseFromString(content, 'text/html');
    var templates = parsedComponent.getElementsByClassName('result-template');

    if (templates) {
      for (var index = 0; index < templates.length; index++) {
        var template = templates[index];
        var div = document.createElement('div');

        div.innerHTML = template.innerHTML;
        for (var i = 0; i < template.attributes.length; i++) {
          var attribute = template.attributes[i];

          // LockerService doesn't like it when we set the 'type' attribute on any elements.
          // Let's not set it. In LockerService, we only support HtmlTemplate and the JSUI default
          // on them when no type attribute is set.
          if (attribute.name.toLowerCase() !== 'type') {
            div.setAttribute(attribute.name, attribute.value);
          }
        }

        var templateType = template.getAttribute('type');
        var coveoTemplate = templateType.toLowerCase() === 'text/x-underscore-template' ?
          new Coveo.UnderscoreTemplate(div) :
          new Coveo.HtmlTemplate(div);

        // The method definition is:
        // registerTemplate(name: string,
        // template: any,
        // publicTemplate: boolean = true,
        // defaultTemplate: boolean = false,
        // resultListTemplate: boolean = false)
        //
        // Because it's a result list template, we must make sure we set it at true, otherwise, use defaults parameters.
        Coveo.TemplateCache.registerTemplate(template.getAttribute('id'), coveoTemplate, true, false, true);
      }
    }
  },

  /**
   * Set the loader and repositories attributes
   *
   * @param  {Object} cmp                     Component
   * @param  {Object} response                Response from server
   * @param  {Object} helper                  Helper
   */
  setLoaderAndRepoAttribute: function (cmp, response, helper) {
    'use strict';

    this.debugLog(cmp, 'BaseHelper:setLoaderAndRepoAttribute');
    var loaderAndRepo = JSON.parse(response.getReturnValue());
    var loader = loaderAndRepo[0];
    var repos = loaderAndRepo[1];

    cmp.set('v.loader', loader);
    cmp.set('v.repos', repos);

    // Reload everything from the server.
    var getInitializationAction = helper.getInitializationAction(cmp, helper, true);

    $A.enqueueAction(getInitializationAction);
  },

  /**
   * Authentication
   *
   * @param  {Component} cmp                  Component
   * @return {Promise}                        Promise
   */
  authenticate: function (cmp) {
    'use strict';

    this.debugLog(cmp, 'BaseHelper:authenticate');
    var deferred = Coveo.$.Deferred();

    var authenticateAction = cmp.get('c.authenticate');

    authenticateAction.setCallback(this, function (authenticateResponse) {
      if (authenticateResponse.getState() === 'SUCCESS') {
        deferred.resolve(authenticateResponse.getReturnValue());
      }
    });
    $A.enqueueAction(authenticateAction);

    return deferred;
  },

  /**
   * Delete wrapper to delete a page.
   *
   * @param  {String} id                      Component ID
   * @param  {Component} cmp                  Component
   */
  del: function (id, cmp) {
    'use strict';

    this.debugLog(cmp, 'BaseHelper:del');
    if (this.isLivePreview() ||
      this.isSitePreview()) {
      this.delFromLivePreview(id, cmp);
    } else {
      this.delFromPublishedSite(id, cmp);
    }
  },

  /**
   * Delete from live preview
   *
   * @param  {String} id                      Component ID
   * @param  {Component} cmp                  Component
   */
  delFromLivePreview: function (id, cmp) {
    'use strict';

    this.debugLog(cmp, 'BaseHelper:delFromLivePreview');
    // Live preview is under another domain than normal sf pages.
    // Session ID is invalid to create pages using metadata API.
    // Create an iframe on a visual force page which will do the job for us...
    var pageUrlAction = cmp.get('c.getPageCreatorUrl');

    pageUrlAction.setCallback(this, function (responsePageUrl) {
      if (pageUrlAction.getState() === 'SUCCESS') {
        var baseUrl = responsePageUrl.getReturnValue();
        var iframe = document.createElement('iframe');

        iframe.src = baseUrl + '?pageName=' + encodeURIComponent(id) + '&doDelete=1';

        // With JQUERY 3.0.0 some function have been removed
        // Breaking change: .load(), .unload(), and .error() removed
        // https://jquery.com/upgrade-guide/3.0/#breaking-change-load-unload-and-error-removed
        iframe.addEventListener('load', function () {
          window.location.reload();
        });

        cmp.find('CoveoPageDeleter')
          .getElement()
          .appendChild(iframe);
      }
    });
    $A.enqueueAction(pageUrlAction);
  },

  /**
   * Delete from published site
   *
   * @param  {String} id                      Component ID
   * @param  {Component} cmp                  Component
   */
  delFromPublishedSite: function (id, cmp) {
    'use strict';

    this.debugLog(cmp, 'BaseHelper:delFromPublishedSite');
    var actionDelete = cmp.get('c.delPage');

    actionDelete.setParams({
      id: id
    });
    actionDelete.setCallback(this, function (responseDelete) {
      if (responseDelete.getState() === 'SUCCESS') {
        window.location.reload();
      }
    });
    $A.enqueueAction(actionDelete);
  },

  /**
   * Edit parameters stylesheet name
   *
   * @param  {type} cmp Component
   * @return {String} Stylesheet name
   */
  getEditParamsStylesheet: function (cmp) {
    'use strict';
    this.debugLog(cmp, 'BaseHelper:getEditParamsStylesheet');

    if (this.isCaseCreationComponent(cmp)) {
      return 'CoveoCaseCreation.css';
    }

    return 'CoveoFullSearchNewDesign.css';
  },

  /**
   * Edit
   *
   * @param  {String} id                      Component ID
   * @param  {Component} cmp                  Component
   * @param  {Component} helper               Helper
   */
  edit: function (id, cmp, helper) {
    'use strict';

    this.debugLog(cmp, 'BaseHelper:edit');

    var name = cmp.get('v.name');
    var searchHub = cmp.get('v.searchHub');
    var searchContext = JSON.stringify(cmp.get('v.searchContext'));

    var initializationData = cmp.get('v.initializationData');
    var pageConfiguration = initializationData.pageConfiguration;
    var userGroups = '';
    var filter = '';
    var additionalUserIdentities = '';

    if (pageConfiguration) {
      filter = pageConfiguration.filter;

      if (pageConfiguration.userGroups) {
        userGroups = pageConfiguration.userGroups.join(';');
      }

      if (pageConfiguration.additionalUserIdentities) {
        additionalUserIdentities = this.userIdToString(pageConfiguration.additionalUserIdentities);
      }
    } else {
      this.debugLog(cmp, 'BaseHelper:edit - page configuration is null.');
    }

    var getEditAction = cmp.get('c.getEditRedirectUrl');

    getEditAction.setParams({
      params: {
        searchInterface: id,
        returnUrl: window.location.href,
        stylesheet: helper.getEditParamsStylesheet(cmp),
        filter: filter,
        searchHub: searchHub,
        userGroups: userGroups,
        additionalUserIdentities: additionalUserIdentities,
        searchContext: searchContext,
        pageName: name,
        additionalInitOptions: 'SearchInterface : {enableHistory : true}'
      },
      pageUrl: location.origin
    });
    getEditAction.setCallback(this, function (responseGetEdit) {
      if (responseGetEdit.getState() === 'SUCCESS') {
        window.location.href = responseGetEdit.getReturnValue();
      } else {
        console.log(responseGetEdit);
      }
    });
    $A.enqueueAction(getEditAction);
  },

  /**
   * Check if the attribute is set to true
   *
   * @param  {Object} attribute               The attribute
   * @return {Boolean}                        True if the attribute is set to true
   */
  attributeIsTrueOrTruthy: function (attribute) {
    'use strict';

    return attribute && (attribute === true || attribute.toLowerCase() === 'true');
  },

  getPageContent: function (cmp) {
    'use strict';
    this.debugLog(cmp, 'BaseHelper:getPageContent');

    var contentElement;
    var pageComponent = cmp.find('CoveoPageContent');

    if (!$A.util.isEmpty(pageComponent)) {
      contentElement = pageComponent.getElement()
    }

    if ($A.util.isEmpty(contentElement)) {
      this.debugLog(cmp, 'BaseHelper:Could not find page content');
    }

    return contentElement;
  },

  /**
   * Get the search interface component
   *
   * @param  {Component} cmp                  Component
   * @return {Element}                        Search interface
   */
  getSearchInterface: function (cmp) {
    'use strict';

    this.debugLog(cmp, 'BaseHelper:getSearchInterface');
    var contentElement = this.getPageContent(cmp);

    var searchInterface = contentElement.getElementsByClassName('CoveoSearchInterface');

    if (!searchInterface || searchInterface.length === 0) {
      searchInterface = contentElement.getElementsByClassName('CoveoCaseCreationInterface');
    }
    if (!searchInterface || searchInterface.length === 0) {
      searchInterface = contentElement.firstChild;
    }

    if (!searchInterface) {
      this.debugLog(cmp, 'BaseHelper: Could not find search interface');
    }

    return Coveo.$(searchInterface);
  },

  /**
   * Set the language after the page load.
   */
  setLanguageAfterPageLoaded: function () {
    'use strict';

    // Localized string are normally loaded on DOMReady.
    // Use this to force their loading after the scripts avec loaded.
    Coveo.setLanguageAfterPageLoaded();
  },

  /**
   * Show the page content.
   *
   * @param  {Component} cmp                  Component
   * @param  {Object} helper                  Helper
   */
  showPageContent: function (cmp, helper) {
    'use strict';
    helper.debugLog(cmp, 'BaseHelper:showPageContent');

    $A.util.addClass(cmp.find('CoveoPageContent').getElement(), 'visible');

    helper.debugLog(cmp, 'BaseHelper:Component fully loaded.')
    if (helper.attributeIsTrueOrTruthy(cmp.get('v.debug'))) {
      var totalTime = Date.now() - window.CoveoStartTime;

      helper.debugLog(cmp, 'BaseHelper:Total initialization time: ' + totalTime + 'ms.');
    }
  },

  /**
   * Interface initialization
   *
   * @param  {Component} cmp                  Component
   * @param  {Object} helper                  Helper
   * @param  {String} name                    Component name
   * @param  {String} searchHub               Searchhub token value
   */
  initializeInterface: function (cmp, helper, name, searchHub) {
    'use strict';

    helper.debugLog(cmp, 'BaseHelper:initializeInterface');
    var searchInterface = helper.getSearchInterface(cmp);

    // Prevent multiple initializations of the SearchComponent
    if (searchInterface !== undefined && searchInterface.coveo(Coveo.SearchInterface) !== undefined) {
      helper.debugLog(cmp, 'BaseHelper:Component already initialized, ignoring');

      return;
    }

    var deferred = Coveo.$.Deferred();
    var scriptExecutionPromise = helper.executeCustomScripts(cmp, helper, deferred);

    helper.debugLog(cmp, 'BaseHelper:initializeInterface - registering promise on script execution.');
    scriptExecutionPromise.then($A.getCallback(function () {
      helper.debugLog(cmp, 'BaseHelper:initializeInterface - callback');
      if (cmp.isValid()) {
        if (helper.attributeIsTrueOrTruthy(cmp.get('v.autoInjectBasicQuery'))) {
          var query = helper.getQuery(cmp);

          if (query) {
            searchInterface.on('buildingQuery', function (event, args) {
              args.queryBuilder.expression.add(query);
            });
          }
        }

        searchInterface.on('querySuccess', $A.getCallback(function () {
          helper.showPageContent(cmp, helper);
        }));

        searchInterface.on('queryError', $A.getCallback(function () {
          helper.showPageContent(cmp, helper);
        }));

        var initData = cmp.get('v.initializationData');

        helper.injectExternalInfos(cmp, initData);

        searchInterface.coveo('options', {
          Analytics: {
            endpoint: initData.analyticsUri
          }
        });

        if (helper.attributeIsTrueOrTruthy(cmp.get('v.autoInjectBasicOptions'))) {
          helper.debugLog(cmp, 'BaseHelper:autoInjectBasicOptions');
          var endpoint = helper.getEndpoint(cmp, name);
          var token = cmp.get('v.token');

          searchInterface.coveo('options', {
            SearchInterface: {
              hideUntilFirstQuery: false,
              endpoint: endpoint
            },
            Analytics: {
              searchHub: searchHub,
              token: token
            },
            DidYouMean: {
              enableAutoCorrection: false
            }
          });
        }

        if (helper.isCaseCreationComponent(cmp)) {
          helper.initializeChildInterfaceCaseCreation(cmp, helper);
        }

        if (helper.attributeIsTrueOrTruthy(cmp.get('v.autoInitialize'))) {
          helper.executeRecommendationInitialization(cmp, helper);

          if (helper.isCaseCreationComponent(cmp)) {
            helper.executeInitializationCaseCreation(cmp);
          } else {
            // Let's notify the CommunitySearchbox that we are initialized
            searchInterface.on(Coveo.InitializationEvents.afterInitialization, function () {
              // Triggers a searchPageInitialized event on the StandaloneSearchbox root element
              // with the SearchInterface instance.
              Coveo.$('#standaloneSearchbox').trigger(Coveo.CommunitySearchboxEvent.searchPageInitialized, {
                searchInterface: Coveo.Component.get(searchInterface.get(0), Coveo.SearchInterface, true)
              });
            });

            helper.executeInitialization(cmp);
          }
        }

        cmp.set('v.interfaceInitialized', true);
      } else {
        console.log('BaseHelper:initializeInterface - Invalid component.')
      }
    }));

    // jQuery 3.x supports catch but not 2.x.
    // If people import 2.x, make sure we don't use the catch method.
    if (scriptExecutionPromise.catch !== undefined) {
      scriptExecutionPromise.catch(function (e) {
        console.log('Problem with scriptExecutionPromise:' + e);
      });
    } else {
      helper.debugLog(cmp, 'Catch method is undefined for the script execution promise.');
    }

    // Resolve the initial promise.
    deferred.resolve();
  },

  /**
   * Get the query to inject into the basic query.
   *
   * @param  {Component} cmp                  Component
   * @returns {String} The query to inject.
   */
  getQuery: function (cmp) {
    'use strict';

    this.debugLog(cmp, 'BaseHelper:getQuery');

    var query = decodeURIComponent(window.location.pathname.substr(window.location.pathname.lastIndexOf('/') + 1));

    // If we are in live preview with "Example Search Term", let's use @uri instead.
    if (query &&
      (this.isLivePreview() ||
        this.isSitePreview()) &&
      query === 'Example search term') {
      query = '@uri';
      this.debugLog(cmp, 'BaseHelper:getQuery - Changing the query to @uri when in the live and site preview.');
    }

    this.debugLog(cmp, 'BaseHelper:getQuery - The query is: "' + query + '"');

    return query;
  },

  /**
   * Execute initialization
   *
   * @param  {Component} cmp                  Component
   */
  executeInitialization: function (cmp) {
    'use strict';

    this.debugLog(cmp, 'BaseHelper:executeInitialization');
    var searchInterfaceElement = this.getSearchInterface(cmp);

    this.debugLog(cmp, 'BaseHelper:init');

    searchInterfaceElement.on('afterComponentsInitialization', function () {
      var searchInterface = Coveo.Component.get(searchInterfaceElement.get(0), Coveo.SearchInterface);

      if (searchInterface && searchInterface.usageAnalytics) {
        searchInterface.usageAnalytics.setOriginContext('CommunitySearch');
      }
    });

    searchInterfaceElement.coveo('init');

    this.debugLog(cmp, 'BaseHelper:Search interface initialized.');
  },

  /**
   * Initialize the recommendation components with the search interface if needed.
   * @param  {Component} cmp Component
   * @param  {Helper} helper Helper
   */
  executeRecommendationInitialization: function(cmp, helper) {
    'use strict'
    helper.debugLog(cmp, 'BaseHelper:executeRecommendationInitialization');
    var searchInterface = helper.getSearchInterface(cmp);
    // Check if we have a valid search interface.
    if (searchInterface && searchInterface.length > 0) {

      // Find the recommendation components inside the search interface.
      var recommendationComponents = searchInterface.find('.CoveoRecommendation');
      if (recommendationComponents !== undefined) {

        // Initialize the recommendation element using the search interface.
        recommendationComponents.toArray().forEach(function(recommendationElement) {
          helper.debugLog(cmp, 'BaseHelper:executeRecommendationInitialization with id ' + recommendationElement.id);
          Coveo.initRecommendation(recommendationElement, searchInterface[0]);
        });
      }
    }
  },

  /**
   * Update the isLivePreviewBuilder value based on the current page state.
   *
   * @param  {Component} cmp Component
   */
  updateIsLivePreviewBuilder: function (cmp) {
    'use strict';
    // When we are in the live preview builder, the body tag has the 'interactions-enabled' class.
    // (Compatible with locker service)
    var isLivePreviewBuilder = this.isLivePreview() &&
      $A.util.hasClass(document.getElementsByTagName('body')[0], 'interactions-enabled');

    // Update the component variable if necessary.
    if (cmp.get('v.isLivePreviewBuilder') !== isLivePreviewBuilder) {
      cmp.set('v.isLivePreviewBuilder', isLivePreviewBuilder)
    }
  },

  /**
   * Check the name of the lighting component to make sure it follows the Salesforce restrictions listed below.
   * @param   {String} name      Component name
   * @return  {Boolean}          Whether or not the component name is ok
   */
  isValidLightningComponentName: function (name) {
    'use strict';

    // Restrictions from JIRA see https://coveord.atlassian.net/browse/SFINT-529
    // 1) only contain \w
    // 2) start with A-Za-z
    // 3) be unique           ** NOT DOING THIS **
    // 4) NO spaces ' '
    // 5) NOT END WITH '_'
    // 6) NOT CONTAIN '__'
    var regexString = /(?=^((?!(__)).)*$)(?=^[A-Z]{1}[\w]+[^_]$)/i;

    return regexString.test(name);

  },

  /**
   * Checks for librairies conflict
   * @param  {Component} cmp                  Component
   * @param  {Object} helper                  Helper
   */
  checkLibConflicts: function (cmp, helper) {
    'use strict';

    // The lightning framework / community builder uses an older version of underscore which breaks our stuff
    // Coveo._copy will save a copy of the right version of underscore
    // This is useful if there is multiple coveo component in the same page load
    if (Coveo._ === undefined || Coveo._.VERSION !== '1.6.0') {
      helper.debugLog(cmp, 'BaseHelper:Found conflicting version of underscore');
      if (Coveo._copy && Coveo._copy.VERSION === '1.6.0') {
        Coveo._ = Coveo._copy;
      } else if (_ && _.noConflict) {
        Coveo._ = _.noConflict();
      }
    } else if (Coveo._ !== undefined && Coveo._.VERSION === '1.6.0' && Coveo._copy === undefined) {
      Coveo._copy = Coveo._;
    }
  },

  /**
   * Checks if the page is in live preview mode
   *
   * @return {Boolean}                        True if the page is in live preview mode
   */
  isLivePreview: function () {
    'use strict';

    // In Spring 18, "livrepreview" was changed to --live in the community URL.
    return location.origin.indexOf('--live') !== -1 || location.origin.indexOf('livepreview') !== -1;
  },

  /**
   * Check if the page is in site preview mode
   *
   * @return {Boolean}                        True if the page is in site preview mode
   */
  isSitePreview: function () {
    'use strict';

    // In Spring 18, "sitepreview" was changed to --preview in the community URL.
    return location.origin.indexOf('--preview') !== -1 || location.origin.indexOf('sitepreview') !== -1;
  },

  /**
   * Setup API endpoints for our search
   *
   * @param  {Component} cmp                  Component
   * @param  {Object} parsed                  Parsed object
   * @param  {String} token                   Access token
   * @param  {String} searchName              Endpoint
   */
  setupEndpoint: function (cmp, parsed, token, searchName) {
    'use strict';

    this.debugLog(cmp, 'BaseHelper:setupEndpoint');

    var endpointName = this.getEndpointName(searchName);

    Coveo.SearchEndpoint.endpoints[endpointName] = new Coveo.SearchEndpoint({
      restUri: parsed.platformUri + '/rest/search',
      accessToken: token,
      isGuestUser: parsed.isGuestUser
    });

    cmp.set('v.isEndpointSet', true);
  },

  /**
   * Get endpoint name
   *
   * @param  {String} searchName              Endpoint
   * @return {String}                         Endpoint name
   */
  getEndpointName: function (searchName) {
    'use strict';

    return searchName + 'Endpoint';
  },

  /**
   * Return the Coveo endpoint associated with the passed endpoint name.
   *
   * @param  {Component} cmp                  Component
   * @param  {String} searchName              Endpoint
   * @return {String}                         Endpoint
   */
  getEndpoint: function (cmp, searchName) {
    'use strict';

    this.debugLog(cmp, 'BaseHelper:getEndpoint');
    var endpointName = this.getEndpointName(searchName);

    return Coveo.SearchEndpoint.endpoints[endpointName];
  },

  /**
   * Logs the messages if the component is in `debug` mode
   *
   * @param  {Component} cmp                  Component
   * @param  {String} message                 Log message
   */
  debugLog: function (cmp, message) {
    'use strict';

    if (typeof cmp !== 'object' && message !== undefined && typeof cmp === 'string') {
      console.log('DEVELOPER YOU NEED TO SPECIFY THE COMPONENT TO THE DEBUGLOG METHOD')
      console.log(message);
    } else if (this.attributeIsTrueOrTruthy(cmp.get('v.debug'))) {
      console.log('[' + new Date().toISOString() + '] ' + message);
    }
  },

  /**
   * addErrorMessage - Adds an error message to the UI
   *
   * @param  {Component} cmp                  Component
   * @param  {String} title                   Log title
   * @param  {String} message                 Log message
   */
  addErrorMessage: function (cmp, title, message) {
    'use strict';

    this.debugLog(cmp, 'BaseHelper::addErrorMessage')

    var errorMessageDisplay = cmp.get('v.errorMessageDisplay')

    // Prevent duplicate error messages ( not great but works for now )
    for ( var x = 0; x < errorMessageDisplay.length; x++ ) {
      if ( errorMessageDisplay[x].title === title && errorMessageDisplay[x].message === message ) {
        return
      }
    }

    // Add title, body to error message list
    errorMessageDisplay.push({
      title: title,
      message: message
    })
    cmp.set('v.errorMessageDisplay', errorMessageDisplay)
  },

  /**
   * Get the component type.
   * @param  {Component} cmp Component
   * @returns {String} The component type.
   */
  getComponentType: function (cmp) {
    'use strict';
    this.debugLog(cmp, 'BaseHelper:getComponentType');

    return cmp.get('v.componentType');
  },

  /**
   * Inject external informations on the window object.
   *
   * @param  {Component} cmp                  Component
   * @param  {Object} initData                 The initialization data.
   */
  injectExternalInfos: function (cmp, initData) {
    'use strict';

    for (var key in initData) {

      // If the key starts with the right prefix, inject the key and value.
      if (key && key.indexOf('ExternalInfo') === 0) {
        this.injectExternalInfo(cmp, key, initData[key]);
      }
    }
  },

  /**
   * Inject external information on the window object.
   *
   * @param  {Component} cmp                  Component
   * @param  {String} key                 The external information key. E.G.: "ExternalInfo.picklistValues"
   * @param  {Object} info                Tthe external information to inject onto the key.
   */
  injectExternalInfo: function (cmp, key, info) {
    'use strict';

    this.debugLog(cmp, 'BaseHelper:Enter injectExternalInfo with key "' + key + '"');

    // Returns if the window object isn't defined.
    if (!window) {
      this.debugLog(cmp, 'BaseHelper:Window undefined. Exiting injectExternalInfo.');

      return;
    }

    // Get the key parts (E.G.: "ExternalInfo" and "picklistValues")
    var parts = key.split('.');

    var currentWindowObject = window;
    var currentPart = parts[0];

    // Create the object tree.
    for (var i = 0; i < parts.length - 1; i++) {
      // Create the object if its undefined.
      if (!currentWindowObject[currentPart]) {
        currentWindowObject[currentPart] = {};
      }

      // update the current part and object to the next one.
      currentWindowObject = currentWindowObject[currentPart];
      currentPart = parts[i + 1];
    }

    // Assign the information to the last path part using the last window object.
    var lastPart = parts[parts.length - 1];

    currentWindowObject[lastPart] = info;
  },

  /**
  * Remove the component from the body.
  *
  * @param  {cmp} component        The component to remove.
  */
  removeComponent: function (component) {
    'use strict';
    $A.util.removeClass(component, 'visible');
    $A.util.addClass(component, 'hidden');
    if (component) {
      var element = component.getElement();
      $A.util.removeClass(element, 'visible');
      $A.util.addClass(element, 'hidden');
    }
  },

  /**
  * Add the content to the current page.
  *
  * @param  {Component} cmp                  Component
  * @param  {Object} loader        The loader to extract content from.
  */
  addLoaderContent: function (cmp, loader) {
    'use strict';

    this.debugLog(cmp, 'BaseHelper:addLoaderContent');
    var fakeDiv = document.createElement('div');

    fakeDiv.innerHTML = loader.content;

    // If LS is enabled we need to register all the templates
    // and remove them from the DOM.
    if (this.isLockerServiceEnabled()) {
      this.debugLog(cmp, 'BaseHelper:addLoaderContent - Registering result templates');
      this.registerTemplates(loader.content);

      var templates = fakeDiv.getElementsByClassName('result-template');

      // Remove all the result templates.
      this.debugLog(cmp, 'BaseHelper:addLoaderContent - Cleaning the DOM');
      while (templates.length > 0) {
        var template = templates[0];

        template.parentNode.removeChild(template);
      }

    }
    cmp.find('CoveoPageContent').getElement().innerHTML = fakeDiv.innerHTML;
  },

  /**
  * Detect if LockerService is enabled
  *
  * @return {Boolean} If LockerService is enabled
  */
  isLockerServiceEnabled: function () {
    'use strict'

    // Change this when we move to API lvl 40.
    return false;
  },

  /**
  * Get the initialization action.
  * @param  {Component} cmp                  Component
  * @param  {Object} helper                  Helper
  * @param  {String} ignoreExisting          Whether or not to bypass storage cache.
  * @returns {LightningActiom} The action to execute for the initialization.
  */
  getInitializationAction: function (cmp, helper, ignoreExisting) {
    'use strict';
    this.debugLog(cmp, 'BaseHelper:getInitializationAction');

    var ignoreExistingAttribute = ignoreExisting;

    // Default value is false.
    if (ignoreExistingAttribute === undefined) {
      ignoreExistingAttribute = false;
    }

    var name = cmp.get('v.name');
    var searchHub = cmp.get('v.searchHub');

    if (searchHub === undefined || searchHub === '') {
      searchHub = name;
    }

    // This will return the following data :
    // token -> search token, used for analytics
    // repo -> list of possible repository(A tab in the interface), and their content
    // loader -> Serialized apex util class that loads the page content (if already created;)
    // platformUri -> the current platform uri (dev/staging/prod/stratus etc)
    // analyticsUri -> the current analytics uri (dev/staging/prod/stratus etc)
    var getInitializationAction = cmp.get('c.getInitializationData');

    getInitializationAction.setParams({
      tokenParams: {
        name: name,
        searchHub: searchHub
      },
      componentType: helper.getComponentType(cmp)
    });

    getInitializationAction.setCallback(this, function (initializationResponse) {
      // For storable actions in the cache, the framework returns the cached response immediately and
      // also tries to refresh the data for quick display the next time it’s requested.
      // Therefore, storable actions might have their callbacks invoked more than once:
      // first with cached data, then with updated data from the server.
      helper.successOrLogError(initializationResponse, function (valueInitialization) {
        helper.debugLog(cmp, 'BaseHelper:getInitializationAction received.');

        var parsed = JSON.parse(valueInitialization);
        var isAdmin = parsed.isAdmin;
        var allowFallbackOnAdmin = parsed.allowFallbackOnAdmin;

        // Let's check if it's the second time we load everything.
        if (!$A.util.isEmpty(cmp.get('v.loader'))) {
          // If it's the second time, let's clear and retry everything.
          helper.clearState(cmp);
        }

        cmp.set('v.isAdmin', isAdmin);
        cmp.set('v.allowFallbackOnAdmin', allowFallbackOnAdmin);

        if (parsed.loader !== undefined) {
          cmp.set('v.loader', JSON.parse(parsed.loader));
        }

        if (parsed.repo !== undefined) {
          cmp.set('v.repos', JSON.parse(parsed.repo));
        }

        cmp.set('v.initializationData', parsed);

        helper.debugLog(cmp, 'BaseHelper:getInitializationAction Action done.');


        var initializationLoadedEvent = cmp.getEvent('InitializationDataLoaded');

        initializationLoadedEvent.fire();
      });
    });

    if (cmp.get('v.useAuraCache')) {
      // We want to force refresh the initialization action when we are in live and site preview.
      ignoreExistingAttribute = ignoreExistingAttribute || this.isLivePreview() || this.isSitePreview();

      var options = {
        ignoreExisting: ignoreExistingAttribute
      }

      getInitializationAction.setStorable(options);
    }

    return getInitializationAction;
  },

  /**
   *  Clear the component state so it can be rerendered after a `getInitializationAction`.
   *
   * @param {any} cmp The component
   */
  clearState: function (cmp) {
    'use strict';
    cmp.set('v.token', undefined);
    cmp.set('v.errorMessageDisplay', undefined);
    cmp.set('v.displayAdvancedSettings', false);
    cmp.set('v.interfaceLoaded', false);
    cmp.set('v.interfaceInitialized', false);
    cmp.set('v.isEndpointSet', false);
  },

  /**
   * Initialize the component. Will fetch the token if necessary and setup the endpoint.
   * Must be done after that the JS sources and the initializationData are loaded.
   *
   * @param {any} cmp The component
   * @param {any} helper  The helper.
   */
  initializeComponent: function (cmp, helper) {
    'use strict';
    this.debugLog(cmp, 'BaseHelper:BaseHelper::initializeComponent');

    var parsed = cmp.get('v.initializationData');
    var name = cmp.get('v.name');
    var isAdmin = cmp.get('v.isAdmin');

    if (parsed.error) {
      this.debugLog(cmp, 'BaseHelper:BaseHelper::initializeComponent-addErrorMessage');

      if (isAdmin) {
            // This sucks : The iframe we're in is sandboxed in preview mode :
            // We can't escape with a target _top or window.open
            // So we cannot provide a direct link
        this.addErrorMessage(cmp, parsed.errorTitle,
          'Access the Configuration page installed with the Coveo package.')
      } else {
        this.addErrorMessage(cmp, parsed.errorTitle, '')
      }

    } else {
      this.debugLog(cmp, 'BaseHelper:BaseHelper::initializeComponent-getToken');
      var token = parsed.token;

      if (token === 'CustomTokenGeneration') {
        var getTokenEvent = cmp.getEvent('GetSearchToken');
        var deferred = $.Deferred();

        deferred.done(function (value) {
          if (value.searchToken) {
            token = value.searchToken;
            cmp.set('v.token', token);
            helper.setupEndpoint(cmp, parsed, token, name);
          } else {
            console.log('Value does not contains a search token', value);
          }
        });
        getTokenEvent.setParams({
          'deferred': deferred
        });
        getTokenEvent.fire();
      } else {
        cmp.set('v.token', token);
        helper.setupEndpoint(cmp, parsed, token, name);
      }
    }
  },

  /**
  * @param {any} loader The loader to test.
  * @returns {Boolean} Whether or not the passed loader is valid.
  */
  isValidLoader: function (loader) {
    'use strict';

    return !(loader === undefined || loader.id === undefined || loader.id === '' || loader.id === null);
  },

  fillPageContent: function (cmp, helper) {
    'use strict';
    this.debugLog(cmp, 'BaseHelper:fillPageContent');

    var loader = cmp.get('v.loader');
    var repos = cmp.get('v.repos');

    if (helper.pageIsNotReadyToRender(cmp, repos)) {
      helper.debugLog(cmp, 'BaseHelper:fillPageContent-pageIsNotReadyToRender');

      return;
    }

    var pageContent = cmp.find('CoveoPageContent');
    var hasChildNodes = pageContent && pageContent.getElement() && pageContent.getElement().children.length > 0;

    if (helper.isValidLoader(loader) && !hasChildNodes) {
      helper.debugLog(cmp, 'BaseHelper:fillPageContent-loadui');
      helper.removeComponent(cmp.find('CoveoPageCreator'))
      helper.addLoaderContent(cmp, loader);
      cmp.set('v.interfaceLoaded', true);
    }
  },

  /**
   * Construct the administration UI. This includes the interface editor and the interface creator.
   *
   * @param {any} cmp The component.
   * @param {any} helper  The helper.
   */
  constructAdminUi: function (cmp, helper) {
    'use strict';
    helper.debugLog(cmp, 'BaseHelper:constructAdminUi');

    var name = cmp.get('v.name');
    var searchHub = cmp.get('v.searchHub');
    var isAdmin = cmp.get('v.isAdmin');
    var repos = cmp.get('v.repos');
    var loader = cmp.get('v.loader');

    if (searchHub === undefined || searchHub === '') {
      helper.debugLog(cmp, 'BaseHelper:constructAdminUi-settingSearchHub');
      searchHub = name;
    }


    if (!helper.isValidLoader(loader) &&
      !helper.isComponentInError(cmp)) {
      helper.debugLog(cmp, 'BaseHelper:fillPageContent-CoveoPageCreator');
      var endpoint = helper.getEndpoint(cmp, name);

      helper.createInterfaceCreator(cmp, name, endpoint, repos, helper);
      helper.renderInterfaceCreator(cmp);
    }

    if (isAdmin && helper.shouldWeShowInterfaceEditorToolbox(cmp, helper)) {
      helper.debugLog(cmp, 'BaseHelper:constructAdminUi-isAdmin');
      helper.showInterfaceEditorToolbox(cmp, repos, name, searchHub, helper);
    }
  },

  /**
    * Logic for if we should show the interface editor toolbar.
    *
    * @param  {any}   cmp      The component.
    * @param  {any}   helper   The helper.
    *
    * @return   {bool}  Should we show it.
    */
  shouldWeShowInterfaceEditorToolbox: function (cmp, helper) {
    'use strict';

    helper.debugLog(cmp, 'BaseHelper:shouldWeShowInterfaceEditorToolbox');
    var loader = cmp.get('v.loader');

    return helper.isValidLoader(loader) && !helper.isComponentInError(cmp);
  },

  /*
   * Render or not the Page Creator.
   *
   * @param {any} cmp
   * @returns
   */
  renderInterfaceCreator: function (cmp) {
    'use strict';
    this.debugLog(cmp, 'BaseHelper:renderInterfaceCreator');

    var pageCreatorElement = cmp.find('CoveoPageCreator').getElement();

    $A.util.addClass(pageCreatorElement, 'visible');
    $A.util.removeClass(pageCreatorElement, 'hidden');
  },

  /*
   * Whether or not the component contains errors to display.
   *
   * @param {any} cmp
   * @returns
   */
  isComponentInError: function (cmp) {
    'use strict';

    return !$A.util.isEmpty(cmp.get('v.errorMessageDisplay'))
  },

  /**
 * userIdToString - Transforms a userID object to a string
 *
 * @param  {Object}  userIds    userID object
 * @return {String}  String     representation of userID
 */
  userIdToString: function (userIds) {
    'use strict';
    var ids = [];

    _.each(userIds, function (userId) {
      ids.push('name=' + userId.name + ',provider=' + userId.provider + ',type=' + userId.type);
    });

    return ids.join(';');
  },

  /**
   * Initialize the child interface. This is useful when there are
   * additionnal stuff to to before executing the initialization.
   *
   * @param {any} cmp The component.
   * @param {any} helper The helper.
   */
  initializeChildInterfaceCaseCreation: function (cmp, helper) {
    'use strict';
    this.debugLog(cmp, 'BaseHelper:initializeChildInterfaceCaseCreation');
    var contentElement = helper.getPageContent(cmp);

    if (contentElement) {
      $A.util.addClass(contentElement, 'visible');
      helper.handleCaseCreation(cmp, helper, contentElement);
    } else {
      console.log('Could not find contentElement.');
    }
  },

  /**
   * Handle when we click on the case creation button.
   *
   * @param  {Component} cmp    Component
   * @param  {Object} helper    Base component helper object to access its controller functions
   */
  handleCaseCreation: function (cmp, helper) {
    'use strict';
    this.debugLog(cmp, 'CaseCreationHelper:handleCaseCreation');
    var searchInterface = helper.getSearchInterface(cmp);

    searchInterface.on(Coveo.CaseCreationEvents.createCase, $A.getCallback(function (e, args) {
      var createCaseAction = cmp.get('c.createCase');

      createCaseAction.setParams({
        fields: JSON.stringify(args.fields),
        redirectUrl: args.redirectUrl,
        visitId: args.visitId,
        useDefaultRule: args.useDefaultRule
      });
      createCaseAction.setCallback(this, function (createCaseResponse) {
        if (createCaseResponse.getState() !== 'SUCCESS') {
          Coveo.$('.CoveoCaseCreation').trigger(Coveo.CaseCreationEvents.createCaseError);
        }

        helper.successOrLogError(createCaseResponse, function (caseCreationData) {
          var caseData = JSON.parse(caseCreationData);
          var navToSObjEvt = $A.get('e.force:navigateToSObject');

          navToSObjEvt.setParams({
            recordId: caseData.Id
          });

          Coveo.$('.CoveoCaseCreation').coveo('logCreatedCase', caseData.Id)
            .catch(function() {
              helper.debugLog(cmp, 'CaseCreationHelper:handleCaseCreation - Cannot logCreatedCase');
            })
            .then(function() {
              navToSObjEvt.fire();
            });
        });
      });
      $A.enqueueAction(createCaseAction);
    }));
  },

  /**
   * Component initialization for case creation.
   *
   * @param  {Component} cmp    Component
   */
  executeInitializationCaseCreation: function (cmp) {
    'use strict';
    this.debugLog(cmp, 'BaseHelper:executeInitialization');

    var searchInterfaceElement = this.getSearchInterface(cmp);
    var name = cmp.get('v.name');
    var endpoint = this.getEndpoint(cmp, name);
    var options = {
      CaseCreationInterface: {
        endpoint: endpoint
      }
    };

    this.debugLog(cmp, 'BaseHelper:initCaseCreation');
    searchInterfaceElement.coveo('initCaseCreation', options);
  },

  /**
   * @param  {Component} cmp Component
   * @return {Boolean} Whether or not the current component is the Case Creation component.
   */
  isCaseCreationComponent: function (cmp) {
    'use strict';
    var componentType = this.getComponentType(cmp);

    return componentType === 'CaseCreation';
  },

  /**
   * @param  {Component} cmp Component
   * @return {Boolean} Whether or not the current component is the Search component.
   */
  isSearchComponent: function (cmp) {
    'use strict';
    var componentType = this.getComponentType(cmp);

    return componentType === 'Search';
  },

  /**
   * Enable the Coveo Search UI log if needed.
   * @param  {Component} cmp Component
   */
  enableCoveoDebugLogs: function (cmp) {
    'use strict';

    if (cmp.get('v.debug') &&
      window.Coveo !== undefined &&
      window.Coveo.Logger !== undefined &&
      cmp.get('v.loggerLevel') !== -1) {
      Coveo.Logger.level = cmp.get('v.loggerLevel');
    }
  },

  /**
   * Method executed after that the custom scripts AND the Coveo scripts are loaded.
   * It will fire the resource loaded event if needed and load the necessary stuff.
   *
   * @param {any} cmp The component.
   * @param {any} helper The helper.
   */
  afterScriptLoaded: function (cmp, helper) {
    'use strict';
    this.debugLog(cmp, 'BaseHelper:afterScriptLoaded');

    // Wait for the custom script to execute and fire the resource loaded event.
    if (cmp.get('v.isComponentScriptsLoaded') &&
      (!helper.hasCustomScripts(cmp) || cmp.get('v.isCustomScriptsLoaded'))) {
      helper.enableCoveoDebugLogs(cmp);
      helper.checkLibConflicts(cmp, helper);
      helper.setLanguageAfterPageLoaded();

      cmp.set('v.isPreviewMode', helper.isLivePreview());
      cmp.set('v.isResourcesLoaded', true);

      var resourcesLoadedEvent = cmp.getEvent('ResourcesLoaded');

      resourcesLoadedEvent.fire();
    }
  },

  /**
   * Dynamically load the specified custom scripts using the ltng:require component.
   *
   * @param {any} cmp The component.
   * @param {any} helper The helper.
   */
  loadCustomScripts: function (cmp, helper) {
    'use strict';
    helper.debugLog(cmp, 'BaseHelper:loadCustomScripts');

    var scripts = cmp.get('v.customScripts');

    $A.createComponent('ltng:require',
      {
        'scripts': scripts,
        'afterScriptsLoaded': cmp.getReference('c.onCustomScriptLoaded')
      },
      function (requireComponent, status, errorMessage) {
        if (status === 'SUCCESS') {
          helper.debugLog(cmp, 'BaseHelper:loadCustomScripts - SUCCESS');

          // We must add the component to the body so it can fire the "afterScriptsLoaded" event.
          var body = cmp.get('v.body');
          body.push(requireComponent);
          cmp.set('v.body', body);
        } else if (status === 'INCOMPLETE') {
          helper.debugLog(cmp, 'BaseHelper:loadCustomScripts - INCOMPLETE');
        } else if (status === 'ERROR') {
          helper.debugLog(cmp, 'BaseHelper:loadCustomScripts - Error:' + errorMessage);
        } else {
          helper.debugLog(cmp, 'BaseHelper:loadCustomScripts - Weird status:' + status);
        }
      }
    );
  },

  /**
   * Execute the dynamically loaded scripts using a predefined global script.
   *
   * It will execute custom scripts based on that order:
   * - window.coveoCustomScripts["globalId"](promise, cmp)
   * - window.coveoCustomScripts["localId"](promise, cmp)
   * - window.coveoCustomScripts["pageName"](promise, cmp)
   * - window.coveoCustomScripts["default"](promise, cmp)
   *
   * if the method returns a promise, it chains every promise in that order and will return.
   * If no method returns a promise, it will return the provided promise.
   *
   * @param {any} cmp The component.
   * @param {any} helper The helper.
   * @param {any} promise The Javascript promise to bind custom scripts to.
   * @return {Promise} the promise to execute.
   */
  executeCustomScripts: function (cmp, helper, promise) {
    'use strict';
    helper.debugLog(cmp, 'BaseHelper:executeCustomScripts');

    // Assign the default promise.
    var scriptExecutionPromise = promise;

    if (window.coveoCustomScripts !== undefined) {
      // Create an array of script names.
      var scriptNames = [];

      // Check the global id.
      var globalId = cmp.getGlobalId();
      if (globalId !== undefined) {
        scriptNames.push(globalId);
      } else {
        helper.debugLog(cmp, 'BaseHelper:executeCustomScripts - Cannot get global id.');
      }

      // Check the global id.
      var localId = cmp.getLocalId();
      if (localId !== undefined) {
        scriptNames.push(localId);
      } else {
        helper.debugLog(cmp, 'BaseHelper:executeCustomScripts - Cannot get local id.');
      }

      // Check the component name.
      var name = cmp.get('v.name');
      if (name !== undefined) {
        scriptNames.push(name);
      } else {
        helper.debugLog(cmp, 'BaseHelper:executeCustomScripts - Cannot get name.');
      }

      // Add the default.
      scriptNames.push('default');

      // for all availiable names check if a script is defined.
      for (var i = 0; i < scriptNames.length; i++) {
        var currentScriptName = scriptNames[i];

        // Check if a script has that name and it's really a function.
        if (window.coveoCustomScripts[currentScriptName] !== undefined &&
          typeof window.coveoCustomScripts[currentScriptName] === 'function') {

          helper.debugLog(cmp, 'BaseHelper:executeCustomScripts - Executing:' + currentScriptName);
          var returnedPromise = window.coveoCustomScripts[currentScriptName](scriptExecutionPromise, cmp);

          // If the script returns a promise, let's use that one instead.
          if (returnedPromise !== undefined && returnedPromise.then !== undefined) {
            scriptExecutionPromise = returnedPromise;
          }

          helper.debugLog(cmp, 'BaseHelper:executeCustomScripts - Execution done:' + currentScriptName);
        }
      }
    }

    // Return the chained promises.
    return scriptExecutionPromise;
  },

  /**
   * @param {any} cmp The component.
   * @returns {Boolean} Whether or not the component has custom scripts defined.
   */
  hasCustomScripts: function (cmp) {
    'use strict';
    this.debugLog(cmp, 'BaseHelper:hasCustomScripts');
    var scripts = cmp.get('v.customScripts');

    return scripts !== undefined && !$A.util.isEmpty(scripts);
  }
})