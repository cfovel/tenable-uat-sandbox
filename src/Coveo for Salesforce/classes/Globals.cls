/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Globals {
    global Globals() {

    }
    global static String generateSearchToken() {
        return null;
    }
    global static String generateSearchToken(Map<String,Object> params) {
        return null;
    }
}
