/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class SearchContext {
    global SearchContext() {

    }
    global static CoveoV2.SearchContext fromJson(String values) {
        return null;
    }
    global static CoveoV2.SearchContext fromObject(Object values) {
        return null;
    }
    global Object get(String key) {
        return null;
    }
    global Object put(String key, Object value) {
        return null;
    }
    global String toJson() {
        return null;
    }
    global Map<String,Object> values() {
        return null;
    }
}
