/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class SendEmail {
    global SendEmail() {

    }
global class ContactOrLeadDetail implements System.Comparable {
}
global class DocumentDetails {
    global DocumentDetails() {

    }
}
global class DocumentRequest {
    global DocumentRequest() {

    }
}
global class DocumentResponse {
    global DocumentResponse() {

    }
}
global class Error {
}
global class LookupRequest {
    global LookupRequest() {

    }
}
global class LookupResponse {
    global LookupResponse() {

    }
}
global class PreviewRequest {
    global PreviewRequest() {

    }
}
global class SortData {
}
}
