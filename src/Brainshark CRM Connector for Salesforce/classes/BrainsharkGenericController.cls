/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class BrainsharkGenericController {
    global String apiHost {
        get;
    }
    global String appHost {
        get;
    }
    global String errorMessage {
        get;
    }
    global Boolean isChatterApp {
        get;
    }
    global String message;
    global String serverName {
        get;
    }
    global String sessionParams {
        get;
    }
    global String slidesharkHost {
        get;
    }
    global BrainsharkGenericController() {

    }
    global BrainsharkGenericController(ApexPages.StandardController standardController) {

    }
    global System.PageReference initPage() {
        return null;
    }
}
