<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Message</label>
    <protected>false</protected>
    <values>
        <field>Initial_Message__c</field>
        <value xsi:type="xsd:string">Welcome to the Tenable Community! We want your experience to be the best. With that in mind here are a couple of suggestions:

1) Search first! If you have entered the Tenable Community with a question or issue we believe we have a solution for you. Search across a wide range of Tenable content and discussions.
2) Post to our Q&amp;A Forums to access both Tenable employees and nearly 100,000 of your peers answers and knowledge. Access the Q&amp;A Forums from the Answer tab or from one of the Featured Topics on the Homepage. These Topics include Knowledgebase Articles as well as forum posts.
3) Join a Group! We recommend our Product Announcement group on the Collaborate tab, but feel free to join others that may interest you.

Lastly, if you want to communicate with me, the Community Manager, or you have a suggestion to improve this your Tenable Community please join the Community Corner group here https://community.tenable.com/s/group/0F9f2000000PQ9KCAW</value>
    </values>
    <values>
        <field>Six_Month_Message__c</field>
        <value xsi:type="xsd:string">Are you getting the most this Tenable Community has to offer? Here are some recommendations:

1) Search first! If you have entered the Tenable Community with a question or issue we believe we have a solution for you. Search across a wide range of Tenable content and discussions.
2) Post to our Q&amp;A Forums to access both Tenable employees and nearly 100,000 of your peers answers and knowledge. Access the Q&amp;A Forums from the Answer tab or from one of the Featured Topics on the Homepage. These Topics include Knowledgebase Articles as well as forum posts.
3) Join a Group! We recommend our Product Announcement group on the Collaborate tab, but feel free to join others that may interest you.

Lastly, if you want to communicate with me, the Community Manager, or you have a suggestion to improve this your Tenable Community please join the Community Corner group here https://community.tenable.com/s/group/0F9f2000000PQ9KCAW</value>
    </values>
</CustomMetadata>
