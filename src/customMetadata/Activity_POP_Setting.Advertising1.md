<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Advertising1</label>
    <protected>false</protected>
    <values>
        <field>Activity_Type__c</field>
        <value xsi:type="xsd:string">Advertising</value>
    </values>
    <values>
        <field>Filter_Logic__c</field>
        <value xsi:type="xsd:string">(1 OR 2 OR 3 OR 4) AND 5 AND 6</value>
    </values>
    <values>
        <field>Line_Number__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>Proof_of_Performance__c</field>
        <value xsi:type="xsd:string">Ad screenshot</value>
    </values>
    <values>
        <field>Qualifier__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
