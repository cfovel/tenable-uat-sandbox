<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Auth0 Production</label>
    <protected>false</protected>
    <values>
        <field>Black_Listed_Fields__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Child_Objects__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Client_ID__c</field>
        <value xsi:type="xsd:string">gK3eF78SodOY9I0KAY6E5n65IKrbG4vk</value>
    </values>
    <values>
        <field>Client_Secret__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Email_Address__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Endpoint_URL__c</field>
        <value xsi:type="xsd:string">https://tenable-prod.auth0.com/dbconnections/change_password</value>
    </values>
    <values>
        <field>Environment_Ind__c</field>
        <value xsi:type="xsd:double">2.0</value>
    </values>
    <values>
        <field>Parent_Objects__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Related_Fields__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Request_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>URL_Suffix__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
