<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Unit Price over 100 GB</label>
    <protected>false</protected>
    <values>
        <field>Container_Size__c</field>
        <value xsi:type="xsd:string">More than 100 GB</value>
    </values>
    <values>
        <field>Container_Size_in_GB__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Unit_Price__c</field>
        <value xsi:type="xsd:double">1500.0</value>
    </values>
</CustomMetadata>
