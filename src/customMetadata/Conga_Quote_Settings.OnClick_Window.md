<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>OnClick Window</label>
    <protected>false</protected>
    <values>
        <field>Email_Template_Id__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>QuoteLine_Query_Id__c</field>
        <value xsi:type="xsd:string">a0Q6000000HZ77P</value>
    </values>
    <values>
        <field>Quote_Query_Id__c</field>
        <value xsi:type="xsd:string">a0Q6000000HZ77K</value>
    </values>
    <values>
        <field>Quote_Template_Id__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Section_Email1__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Section_Email2__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Section_Print__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Section_Start__c</field>
        <value xsi:type="xsd:string">window.open(&apos;https://www.appextremes.com/apps/Conga/PointMerge.aspx?SessionId=</value>
    </values>
    <values>
        <field>Section_eSign1__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Section_eSign2__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Section_eSign3__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Section_eSign4__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>eSign_Template_Id__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
