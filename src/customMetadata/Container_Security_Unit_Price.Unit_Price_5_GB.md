<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Unit Price 5 GB</label>
    <protected>false</protected>
    <values>
        <field>Container_Size__c</field>
        <value xsi:type="xsd:string">5 GB</value>
    </values>
    <values>
        <field>Container_Size_in_GB__c</field>
        <value xsi:type="xsd:double">5.0</value>
    </values>
    <values>
        <field>Unit_Price__c</field>
        <value xsi:type="xsd:double">3000.0</value>
    </values>
</CustomMetadata>
