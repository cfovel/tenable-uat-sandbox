<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Operations</label>
    <protected>false</protected>
    <values>
        <field>Is_Sales__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Sales_Classification__c</field>
        <value xsi:type="xsd:string">OPS</value>
    </values>
</CustomMetadata>
