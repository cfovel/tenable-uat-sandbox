<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>purchaseOrderCreate</label>
    <protected>false</protected>
    <values>
        <field>Black_Listed_Fields__c</field>
        <value xsi:type="xsd:string">Account:Shipping_Map__c,Contract:MVSA_in_Place__c</value>
    </values>
    <values>
        <field>Child_Objects__c</field>
        <value xsi:type="xsd:string">Purchase_Order_Item__c</value>
    </values>
    <values>
        <field>Client_ID__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Client_Secret__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Email_Address__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Endpoint_URL__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Environment_Ind__c</field>
        <value xsi:type="xsd:double">88.0</value>
    </values>
    <values>
        <field>Parent_Objects__c</field>
        <value xsi:type="xsd:string">Account</value>
    </values>
    <values>
        <field>Related_Fields__c</field>
        <value xsi:type="xsd:string">Contract:Director_Approver__r.email,Contract: Receiving_Employee__r.email,Contract: Receiving_Employee_Manager__r.name,Account:Owner.email</value>
    </values>
    <values>
        <field>Request_Type__c</field>
        <value xsi:type="xsd:string">POST</value>
    </values>
    <values>
        <field>URL_Suffix__c</field>
        <value xsi:type="xsd:string">finance/purchaseOrder</value>
    </values>
</CustomMetadata>
