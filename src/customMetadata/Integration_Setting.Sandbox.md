<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Sandbox</label>
    <protected>false</protected>
    <values>
        <field>Black_Listed_Fields__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Child_Objects__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Client_ID__c</field>
        <value xsi:type="xsd:string">3e46172d32644e228d920603519d86b9</value>
    </values>
    <values>
        <field>Client_Secret__c</field>
        <value xsi:type="xsd:string">d651ef71a72344fbBAAB619A0B9CFD04</value>
    </values>
    <values>
        <field>Email_Address__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Endpoint_URL__c</field>
        <value xsi:type="xsd:string">https://tns-salesforce-sb.cloudhub.io/1.0/</value>
    </values>
    <values>
        <field>Environment_Ind__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>Parent_Objects__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Related_Fields__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Request_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>URL_Suffix__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
