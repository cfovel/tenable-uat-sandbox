<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Production</label>
    <protected>false</protected>
    <values>
        <field>Black_Listed_Fields__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Child_Objects__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Client_ID__c</field>
        <value xsi:type="xsd:string">372fb809c86147bdadc2af6b43410a5b</value>
    </values>
    <values>
        <field>Client_Secret__c</field>
        <value xsi:type="xsd:string">e2d51210e86b4c0fB5191E5337BF0EC9</value>
    </values>
    <values>
        <field>Email_Address__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Endpoint_URL__c</field>
        <value xsi:type="xsd:string">https://tns-salesforce-prod.cloudhub.io/1.0/</value>
    </values>
    <values>
        <field>Environment_Ind__c</field>
        <value xsi:type="xsd:double">98.0</value>
    </values>
    <values>
        <field>Parent_Objects__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Related_Fields__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Request_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>URL_Suffix__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
