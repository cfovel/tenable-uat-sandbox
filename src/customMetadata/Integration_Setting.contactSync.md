<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>contactSync</label>
    <protected>false</protected>
    <values>
        <field>Black_Listed_Fields__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Child_Objects__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Client_ID__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Client_Secret__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Email_Address__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Endpoint_URL__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Environment_Ind__c</field>
        <value xsi:type="xsd:double">86.0</value>
    </values>
    <values>
        <field>Parent_Objects__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Related_Fields__c</field>
        <value xsi:type="xsd:string">Contact:Account.Subsidiary__c,Contact:Account.LMS_Customer_ID__c</value>
    </values>
    <values>
        <field>Request_Type__c</field>
        <value xsi:type="xsd:string">POST</value>
    </values>
    <values>
        <field>URL_Suffix__c</field>
        <value xsi:type="xsd:string">contact</value>
    </values>
</CustomMetadata>
