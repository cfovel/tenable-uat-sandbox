<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Pending Engineering - Priority : P3</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>CaseTimeOffset1__c</field>
        <value xsi:type="xsd:double">48.0</value>
    </values>
    <values>
        <field>CaseTimeOffset2__c</field>
        <value xsi:type="xsd:double">52.0</value>
    </values>
    <values>
        <field>Priority__c</field>
        <value xsi:type="xsd:string">P3 - Medium</value>
    </values>
    <values>
        <field>Status__c</field>
        <value xsi:type="xsd:string">Pending Engineering</value>
    </values>
</CustomMetadata>
