({
    fetchPostToPicklistVal: function(component,fieldName,elementId){
        var action = component.get("c.GetPostTovalues");
        action.setCallback(this,function(response) {
            if(response.getState() == "SUCCESS")
            {
                component.set('v.PostToMap',response.getReturnValue());
                var posttomapdata = component.get('v.PostToMap');
                var opts = [];
                
                if(posttomapdata != undefined)
                {
                    opts.push({
                        class: "optionClass",
                        label: "Choose ---",
                        value: ""
                    });
                    for(var key in posttomapdata)
                    {
                        opts.push({
                            class: "optionClass",
                            label: posttomapdata[key],
                            value: key
                        });
                    } 
                }
                component.find(elementId).set("v.options",opts);
            }
        });
        
        $A.enqueueAction(action);
    },
    fetchTopicPicklistVal: function(component,fieldName,elementId){
        var action = component.get("c.GetTopicvalues");
        action.setCallback(this,function(response) {
            if(response.getState() == "SUCCESS")
            {
                component.set('v.TopicsMap',response.getReturnValue());
                var topicsmapdata = component.get('v.TopicsMap');
                var opts = [];
                
                if(topicsmapdata != undefined)
                {
                    opts.push({
                        class: "optionClass",
                        label: "Choose ---",
                        value: ""
                    });
                    for(var key in topicsmapdata)
                    {
                        opts.push({
                            class: "optionClass",
                            label: topicsmapdata[key],
                            value: key
                        });
                    } 
                }
                component.find(elementId).set("v.options",opts);
            }
        });
        
        $A.enqueueAction(action);
    },
    GetUserId : function(component){
        var action = component.get("c.GetUserId");
        action.setCallback(this,function(response) {
            if(response.getState() === "SUCCESS")
            {
                console.log('The value of response.getReturnValue() is : ' + response.getReturnValue());
                component.set("v.UserLicense",response.getReturnValue());
            }
        })
        $A.enqueueAction(action);
    },
    CreateNewRecord : function(component) {
        console.log("Inside AskAQuestionComponentHelper.CreateNewRecord");
        var updetails = component.find("DetailInfo").get('v.value');
        console.log("updetails: " + updetails);
        updetails = updetails.replace(/<br>/g,'<p>&nbsp;</p>');
        updetails = updetails.replace(/<h2>/g,'<b>');
        updetails = updetails.replace(/<\/h2>/g,'</b>');
        //updetails = updetails.replace(/<p>/g,'');
        //updetails = updetails.replace(/<\/p>/g,'\r\n');
        console.log("updetails: " + updetails);
        var PostToId = component.find("SelectPostTo").get("v.value");
        console.log("PostToId: " + PostToId);
        var QuestionInfo = component.find("Question").get("v.value");
        console.log("QuestionInfo: " + QuestionInfo);
        var DetailInfo = updetails;
        console.log("DetailInfo: " + DetailInfo);
        var TopicsId = component.find("SelectTopic").get("v.value");
        console.log("TopicsId: " + TopicsId);
        var FileName = component.get("v.fileName");
        console.log("FileName: " + FileName);
        var FilePath = component.get("v.filePath");
        console.log("FilePath: " + FilePath);
        var FileData = component.get("v.fileData");
        console.log("FileData: " + FileData);
        
        var reader = new FileReader();        

        reader.onloadend = function() {
           var resultsData =reader.result;
           var testData=  resultsData.match(/,(.*)$/)[1];
            
            var action = component.get("c.InsertQuestionRecords");
        	action.setParams({
                "PostId"    : PostToId, 
                "Question"  : QuestionInfo,
                "Details"   : DetailInfo,
                "TopicId"   : TopicsId,
                "FileTitle" : FileName,
                "FilePath"  : FilePath,
                "FileData"  : testData
            });
            action.setCallback(this,function(response) {
                if(response.getState() == "SUCCESS") {
                    component.set('v.RecordId',response.getReturnValue());
                    var RecId = component.get('v.RecordId');
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": RecId,
                        "slideDevName": "Detail"
                    });
                    navEvt.fire();
                    component.set("v.isOpen",false);
                } else {
                    console.log('There was an error');
                }
            });
            
            $A.enqueueAction(action);  
              
        };
   		
        if(FileData != undefined && FileData != ""){
        	reader.readAsDataURL(FileData);    
        } else {
            var action = component.get("c.InsertQuestionRecords");
        	action.setParams({
            "PostId"    : PostToId, 
            "Question"  : QuestionInfo,
            "Details"   : DetailInfo,
            "TopicId"   : TopicsId
            });
            action.setCallback(this,function(response) {
                if(response.getState() == "SUCCESS") {
                    component.set('v.RecordId',response.getReturnValue());
                    var RecId = component.get('v.RecordId');
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": RecId,
                        "slideDevName": "Detail"
                    });
                    navEvt.fire();
                    component.set("v.isOpen",false);
                } else {
                    console.log('There was an error');
                }
            });
            
            $A.enqueueAction(action);  
        }
    },
})