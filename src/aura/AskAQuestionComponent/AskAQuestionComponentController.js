({
    showModal : function(component, event, helper) {
        var UserLicense = component.get('v.UserLicense');
        if(UserLicense == 'Guest User License')
        {
            component.set('v.errorMessage','You need to be logged into the community to ask a question');
        }
        else
        {
            component.set('v.errorMessage','');
        }
        component.set("v.isOpen",true);
        var mainmodal = component.find('divMainDialog');
        $A.util.addClass(mainmodal,'slds-fade-in-open');
        helper.fetchPostToPicklistVal(component,'Post To','SelectPostTo');
        helper.fetchTopicPicklistVal(component,'Topic','SelectTopic');
    },
    
    closeModal : function(component,event, helper) {
        component.set("v.isOpen",false);
        var mainmodal = component.find('divMainDialog');
        $A.util.removeClass(mainmodal,'slds-fade-in-open');
    },
    
    closeFileModal : function(component,event,helper) {
        component.set("v.FUploadOpen",false);
        var mainmodal = component.find('divMainDialog');
        var filemodal = component.find('divFileDialog');
        $A.util.removeClass(filemodal,'slds-fade-in-open');  
        $A.util.addClass(mainmodal,'slds-fade-in-open');
        $A.util.removeClass(mainmodal,'slds-backdrop');
        $A.util.removeClass(mainmodal,'slds-backdrop--open'); 
    },
    
    ShowAccordionDetails : function(component,event,helper) {
        var toggledownicon = component.find("downicon");
        var togglerighticon = component.find("righticon");
        var toggleContainerdiv = component.find("containerdiv");
        
        $A.util.toggleClass(togglerighticon,"slds-show");
        $A.util.toggleClass(toggledownicon,"slds-hide");
        $A.util.toggleClass(toggleContainerdiv,"slds-hide");
    },
    
    saveQuestionRecord : function(component,event,helper) {
        console.log("Entering saveQuestionRecord...");
        //var replacehtmltag = /(<([^>]+)>)/ig;
        var validItem = true;
        
        var requiredFieldIdToMessage = {
            'SelectPostTo': "Please select a topic.",
            'Question': "What do you want to ask?",
            'DetailInfo': "What do you want to ask?",
            'SelectTopic': "Please select a product."
        };
        console.log("requiredFieldIdToMessage: " + JSON.stringify(requiredFieldIdToMessage));
        var FileName = component.get('v.fileName');
        console.log("FileName: " + FileName);
        var FilePath = component.get('v.filePath');
        console.log("FilePath: " + FilePath);
        var FileData = component.get('v.fileData');
        console.log("FileData: " + FileData);
        
        for (var requiredFieldId in requiredFieldIdToMessage) {
            console.log("requiredFieldId: " + requiredFieldId);
            var requiredFieldComponent = component.find(requiredFieldId);
            if ($A.util.isEmpty(requiredFieldComponent.get('v.value'))) {
                console.log("Missing required field: " + requiredFieldId);
                validItem = false;
                requiredFieldComponent.set("v.errors", [{message: requiredFieldIdToMessage[requiredFieldId]}]);
                if (requiredFieldId === "DetailInfo") {
                    requiredFieldComponent.set("v.valid", false);
                    requiredFieldComponent.set("v.messageWhenBadInput", requiredFieldIdToMessage[requiredFieldId]);
                }
            } else {
                console.log("Required field populated, clearing errors: " + requiredFieldId);
                if (requiredFieldId === "DetailInfo") {
                    requiredFieldComponent.set("v.valid", true);
                    requiredFieldComponent.set("v.messageWhenBadInput", null);
                } else {
	                requiredFieldComponent.set("v.errors", null);
                }
            }
        }
 
        if(validItem) {
            console.log("Item is valid");
            helper.CreateNewRecord(component);
        }
    },
    OpenFileList: function(component,event,helper)
    {
        component.set("v.FUploadOpen",true);
        var mainmodal = component.find('divMainDialog');
        var filemodal = component.find('divFileDialog');
        $A.util.removeClass(mainmodal,'slds-fade-in-open');
        $A.util.addClass(mainmodal,'slds-backdrop');
        $A.util.addClass(mainmodal,'slds-backdrop--open');
        $A.util.addClass(filemodal,'slds-fade-in-open');       
    },
    doInit: function(component,event,helper)
    {
        var toggleContainerdiv = component.find("containerdiv");
        $A.util.toggleClass(toggleContainerdiv,"slds-show");
        
        var buttonlabel = component.get("v.ButtonName");
        
        if( buttonlabel == '')
        {
            component.set("v.ButtonName","Ask a question");
        }
        else
        {
            component.set("v.ButtonName",buttonlabel);
        }
        helper.GetUserId(component);
    },
    UploadFiles : function(component,event,helper)
    {
        var mainmodal = component.find('divMainDialog');
        var filemodal = component.find('divFileDialog');
        var fileValue = document.getElementById('uploadfile').value;        
        console.log('The value of fileValue is : ' + fileValue);
        
        var fileData= document.getElementById('uploadfile').files[0];
               
        if (fileValue) 
        {
            var startIndex = (fileValue.indexOf('\\') >= 0 ? fileValue.lastIndexOf('\\') : fileValue.lastIndexOf('/'));
            var FileName = fileValue.substring(startIndex);
            if (FileName.indexOf('\\') === 0 || FileName.indexOf('/') === 0) 
            {
                FileName = FileName.substring(1);
            }
        }
        
        component.set("v.fileName",FileName);
        component.set("v.filePath",fileValue);
        component.set("v.fileData",fileData);
       
        
        $A.util.removeClass(filemodal,'slds-fade-in-open');
        $A.util.removeClass(mainmodal,'slds-backdrop');
        $A.util.removeClass(mainmodal,'slds-backdrop--open');
        $A.util.addClass(mainmodal,'slds-fade-in-open');  
    },
    RemovePill: function(component,event,helper)
    {
        event.preventDefault();
    },
})