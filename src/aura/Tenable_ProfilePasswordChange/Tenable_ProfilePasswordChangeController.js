/**
 * Created by lucassoderstrum on 6/5/18.
 */
({
    goToUrl : function(component, event, helper) {
        console.log('in url');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": component.get("v.profileUrl")
        });
        urlEvent.fire();
    }
})