(
    {
        generateSearchToken: function(component, event, helper) {
            //   console.log('window location ----->' + window.location.pathname);
            if (window.location.pathname.indexOf('/global-search/text') == -1) {
                //     var deferred = event.getParam('deferred');
                var action = component.get('c.getToken');
                
                // The response will contain the token.
                // It is very important to resolve the deferred parameter
                // with a JSON containing searchToken as a key.
                console.log('GENERATE SEARCH TOKEN')
                action.setCallback(this, function(response) {
                    if (response.getState() == 'SUCCESS') {
                        console.log('GENERATE SEARCH TOKEN - SUCCESS')
                        console.log('GENERATE SEARCH TOKEN - ' + response.getReturnValue())
                        //deferred.resolve({
                        //  searchToken: response.getReturnValue()
                        //})
                        
                        
                        console.log('GENERATE SEARCH TOKEN - INIT SEARCH BOS')
                        Coveo.SearchEndpoint.configureCloudV2Endpoint("",  response.getReturnValue());
                        var root = Coveo.$$(document).find("#searchbox");
                        Coveo.initSearchbox(root, component.get('v.searchUrl'));
                        
                    }
                })
                
                // Queue the action using the framework available methods.
                $A.enqueueAction(action);
            
            }
        }
    })