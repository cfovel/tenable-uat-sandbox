({
	getNotificationPreference : function(component, event, helper) {
        var action = component.get("c.getEmailPreference");
        action.setParams({
        });
        console.log(action);
        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var disabledAllEmailFields = response.getReturnValue();
            	component.set("v.getNotifications", !disabledAllEmailFields);
            } else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    goToNext : function(component, event, helper) {
        var compEvent = component.getEvent("pageChange");
        compEvent.setParams({"message" : "6", "slide" : "Notification"});
        compEvent.fire();
        
    },
    goBack : function(component, event, helper) {
        var compEvent = component.getEvent("pageChange");
        compEvent.setParams({"message" : "4", "slide" : ""});
        compEvent.fire();
    },
    updateNotifications : function(component, event, helper) {
        console.log('22');
        var action = component.get("c.updatePreferences");
        action.setParams({
            "decision": component.get("v.getNotifications")
        });
        console.log(action);
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                console.log('resolve24: ' + response.getReturnValue());
                console.log('23');
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    }
})