({
    init : function(component, event, helper) {
        console.log('Tenable_OnboardingNotifications:init');
        helper.getNotificationPreference(component, event, helper);
    },
	goToNext : function(component, event, helper) {
	    console.log('in js controller');
        helper.updateNotifications(component, event, helper);
        helper.goToNext(component, event, helper);
	},
    goBack : function(component, event, helper) {
        helper.goBack(component, event, helper);
	},
    radioChange : function(component, event, helper) {
        var selected = event.getSource().get("v.label");
        if (selected == 'Yes') {
            component.set("v.getNotifications", true);
        } else if (selected == 'No') {
            component.set("v.getNotifications", false);
        }
    }
    
})