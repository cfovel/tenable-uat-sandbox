({
	init : function(component, event, helper) {
      //retrieve logged user support access config
	  component.set("v.ShowCollab", false); 
	  component.set("v.ShowCases", false);
      var action = component.get("c.getFooterConditions");
      action.setCallback(this, function(response) {   
        var state = response.getState();
        if(state === "SUCCESS") {
          var FooterViewRender = JSON.parse(response.getReturnValue());
          console.log(FooterViewRender);
          if( FooterViewRender.isSuccess ){
	        component.set("v.ShowCollab", FooterViewRender.ShowCollab); 
	        component.set("v.ShowCases", FooterViewRender.ShowCases);
          }
          else{
            console.log('Problem getting footer render conditions, server error: ' + FooterViewRender.errMsg);
          }
        } else {
          console.log('Problem getting footer render conditions, response state: ' + state);
        }
      });
      $A.enqueueAction(action);
	},
    navigateToURL : function(component, event, helper) {
      var urlEvent = $A.get("e.force:navigateToURL");
      urlEvent.setParams({
        "url": "google.com"
      });
      var triggerCmp = component.find("triggerAnswers");
      var ctarget = event.currentTarget;
      if (triggerCmp) {
        var source = event.getSource();
        var label = source.get("v.label");
        //triggerCmp.set("v.label", label);
      }
        
      //urlEvent.fire();      
    }
})