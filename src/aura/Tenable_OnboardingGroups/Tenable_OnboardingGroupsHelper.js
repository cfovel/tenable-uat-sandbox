// Tenable needs to write a custom description for each group, so instaed of iterating over a list the component will just have separate attributes for each group.
// To make sure that the each custom description lines up with the group, this class makes four individual calls to the db for each group, instead of making a call for a list of all four. .
({
    init : function(component, event, helper) {
    var group1 = component.get("v.group1"); 
            var group2 = component.get("v.group2");
            var group3 = component.get("v.group3");
            var group4 = component.get("v.group4");
            var groups = [group1, group2, group3, group4];
            console.log('groups: ' + groups);

             var action = component.get("c.getGroups");
        console.log(action);
        action.setParams({
            "groupIds": groups
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                    component.set("v.groups", response.getReturnValue());
                component.set("v.group1Joined", true);

            } else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
        this.init1(component, event, helper);
        this.getEmailNotficationSettings(component, event, helper);
    },

    init1 : function(component, event, helper) {
        console.log('in 2');
        var group1 = component.get("v.group1");
        var action = component.get("c.getGroup");
        console.log(action);
        action.setParams({
            "groupId": group1
        });

        // var action = component.get("c.getGroups");
        // console.log(action);
        // action.setParams({
        //     "groupIds": groups
        // });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                //    component.set("v.groups", response.getReturnValue());
                var follow = response.getReturnValue();
                console.log('tester1: ' + follow.id);
                console.log('tester1: ' + follow.name);
                console.log('tester1: ' + follow.following);
                console.log('tester1: ' + follow.notificationFrequency);
                component.set("v.group1Wrapper", follow);
                component.set("v.group1Joined", follow.following);
                if(follow.notificationFrequency != null && follow.notificationFrequency != "")
					component.set("v.group1Notification", follow.notificationFrequency);
            } else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
        this.init2(component, event, helper);
    },
    init2 : function(component, event, helper) {
        console.log('in 2');
        var group2 = component.get("v.group2");
        var action = component.get("c.getGroup");
        console.log(action);
        action.setParams({
            "groupId": group2
        });

        // var action = component.get("c.getGroups");
        // console.log(action);
        // action.setParams({
        //     "groupIds": groups
        // });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                //    component.set("v.groups", response.getReturnValue());
                var follow = response.getReturnValue();
                console.log('tester2: ' + follow.id);
                console.log('tester2: ' + follow.name);
                console.log('tester2: ' + follow.following);
                component.set("v.group2Wrapper", follow);
                component.set("v.group2Joined", follow.following);
                if(follow.notificationFrequency != null && follow.notificationFrequency != "")
					component.set("v.group2Notification", follow.notificationFrequency);

            } else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
        this.init3(component, event, helper);
    },
    init3 : function(component, event, helper) {
        console.log('in 2');
        var group3 = component.get("v.group3");
        var action = component.get("c.getGroup");
        console.log(action);
        action.setParams({
            "groupId": group3
        });

        // var action = component.get("c.getGroups");
        // console.log(action);
        // action.setParams({
        //     "groupIds": groups
        // });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                //    component.set("v.groups", response.getReturnValue());
                var follow = response.getReturnValue();
                console.log('tester3: ' + follow.id);
                console.log('tester3: ' + follow.name);
                console.log('tester3: ' + follow.following);
                component.set("v.group3Wrapper", follow);
                component.set("v.group3Joined", follow.following);
                if(follow.notificationFrequency != null && follow.notificationFrequency != "")
					component.set("v.group3Notification", follow.notificationFrequency);

            } else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
        this.init4(component, event, helper);
    },
    init4 : function(component, event, helper) {
        console.log('in 2');
        var group4 = component.get("v.group4");
        var action = component.get("c.getGroup");
        console.log(action);
        action.setParams({
            "groupId": group4
        });

        // var action = component.get("c.getGroups");
        // console.log(action);
        // action.setParams({
        //     "groupIds": groups
        // });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                //    component.set("v.groups", response.getReturnValue());
                var follow = response.getReturnValue();
                console.log('tester4: ' + follow.id);
                console.log('tester4: ' + follow.name);
                console.log('tester4: ' + follow.following);
                component.set("v.group4Wrapper", follow);
                component.set("v.group4Joined", follow.following);
                if(follow.notificationFrequency != null && follow.notificationFrequency != "")
					component.set("v.group4Notification", follow.notificationFrequency);

            } else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
	goToNext : function(component, event, helper) {
        var compEvent = component.getEvent("pageChange");
        compEvent.setParams({"message" : "5", "slide" : "Group"});
        compEvent.fire();
	},
        goBack : function(component, event, helper) {
        var compEvent = component.getEvent("pageChange");
        compEvent.setParams({"message" : "3", "slide" : ""});
        compEvent.fire();
    },
    joinGroups : function(component, event, helper) {
        console.log('in helper8');
        console.log('grp 1 not: ' + component.get("v.group1Notification"));
        console.log('get groups: ' + component.get("v.group1Joined"));
        console.log('grp 2 not: ' + component.get("v.group2Notification"));
        console.log('get groups: ' + component.get("v.group2Joined"));
        console.log('grp 3 not: ' + component.get("v.group3Notification"));
        console.log('get groups: ' + component.get("v.group3Joined"));
        console.log('grp 4 not: ' + component.get("v.group4Notification"));
        console.log('get groups: ' + component.get("v.group4Joined"));
        var action = component.get("c.insertGroupMember");
        action.setParams({
            "groupOneId": component.get("v.group1"),
            "groupOneEmail": component.get("v.group1Notification"),
            "groupOneJoined": component.get("v.group1Joined"),
            "groupTwoId": component.get("v.group2"),
            "groupTwoEmail": component.get("v.group2Notification"),
            "groupTwoJoined": component.get("v.group2Joined"),
            "groupThreeId": component.get("v.group3"),
            "groupThreeEmail": component.get("v.group3Notification"),
            "groupThreeJoined": component.get("v.group3Joined"),
            "groupFourId": component.get("v.group4"),
            "groupFourEmail": component.get("v.group4Notification"),
            "groupFourJoined": component.get("v.group4Joined")
        });
        console.log(action);
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                console.log('resolved: ' + response.getReturnValue());
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);

    },
    
    getEmailNotficationSettings : function(component, event, helper){
    	var action = component.get("c.getEmailPreference");
        
    	action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var disabledAllEmailFields = response.getReturnValue();
            	component.set("v.emailNotificationEnabled", !disabledAllEmailFields);
            } else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
	}

})