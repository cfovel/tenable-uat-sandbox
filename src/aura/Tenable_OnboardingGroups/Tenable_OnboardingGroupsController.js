({
    init : function(component, event, helper) {
        helper.init(component, event, helper);	
	},
	goToNext : function(component, event, helper) {
        console.log('in controller helper8');
        helper.joinGroups(component, event, helper);
        helper.goToNext(component, event, helper);
	},
    goBack : function(component, event, helper) {
        helper.goBack(component, event, helper);
	},
    group1NotificationChange : function (cmp) {
        console.log('tester test');
        console.log('group1selectednotification: ' + cmp.get("v.group1Notification"));
       // var group1Selected = component.find("groupsNotification").get("v.value");
       // console.log('group1selected: ' + group1Selected);
       // component.set("v.group1Notification", group1Selected);
    },
    handleGroup1Click : function (cmp, event, helper) {
        var joined = cmp.get("v.group1Joined");
        if (joined){
            cmp.set("v.group1Joined", false);
        } else {
            cmp.set("v.group1Joined", true);
        }
       // cmp.set('v.group1Notification', !group1Notification);
    },
    handleGroup2Click : function (cmp, event, helper) {
        var joined = cmp.get("v.group2Joined");
        if (joined){
            cmp.set("v.group2Joined", false);
        } else {
            cmp.set("v.group2Joined", true);
        }
    },
    handleGroup3Click : function (cmp, event, helper) {
        var joined = cmp.get("v.group3Joined");
        if (joined){
            cmp.set("v.group3Joined", false);
        } else {
            cmp.set("v.group3Joined", true);
        }
    },
    handleGroup4Click : function (cmp, event, helper) {
        var joined = cmp.get("v.group4Joined");
        if (joined){
            cmp.set("v.group4Joined", false);
        } else {
            cmp.set("v.group4Joined", true);
        }
    },
    handleGroup1Remove : function (cmp, event, helper) {
            cmp.set("v.group1Joined", false);
    },
    handleGroup2Remove : function (cmp, event, helper) {
        cmp.set("v.group2Joined", false);
    },
    handleGroup3Remove : function (cmp, event, helper) {
        cmp.set("v.group3Joined", false);
    },
    handleGroup4Remove : function (cmp, event, helper) {
        cmp.set("v.group4Joined", false);
    }
})