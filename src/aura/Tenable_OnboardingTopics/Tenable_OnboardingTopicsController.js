({
    init : function(component, event, helper) {
        helper.getCategoryTopics(component, event, helper);
        helper.getProductTopics(component, event, helper);
    },
	goToNext : function(component, event, helper) {
        helper.goToNext(component, event, helper);
	},
    goBack : function(component, event, helper){
        helper.goBack(component, event, helper);
    },
    userFollowTopic : function (cmp, event, helper) {
        helper.userFollowTopic(cmp, event, helper);
    },
    userUnfollowTopic : function (cmp, event, helper) {
        helper.userUnfollowTopic(cmp, event, helper);
    }
})