({
    goToNext : function(component, event, helper) {
        var compEvent = component.getEvent("pageChange");
        compEvent.setParams({"message" : "4", "slide" : "Topic"});
        compEvent.fire();
    },
    goBack : function(component, event, helper) {
        var compEvent = component.getEvent("pageChange");
        compEvent.setParams({"message" : "2", "slide" : ""});
        compEvent.fire();
    },

     getCategoryTopics : function(component, event, helper) {
            console.log('in topic init');
        var topicsList = component.get('v.topicIds').replace(/\s/g,'').split(',');
        var action = component.get("c.getTopics");
        action.setParams({
            "topicIds": topicsList
        });
        console.log(action);
        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                console.log('1111: ' + response.getReturnValue());
                var returnedTopics = response.getReturnValue();
                    component.set("v.topics", returnedTopics);
            } else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },

	getProductTopics : function(component, event, helper) {
            console.log('in topic init');
        var topicsList = component.get('v.topicProductIds').replace(/\s/g,'').split(',');
        var action = component.get("c.getTopics");
        action.setParams({
            "topicIds": topicsList
        });
        console.log(action);
        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                console.log('1111: ' + response.getReturnValue());
                var returnedTopics = response.getReturnValue();
                    component.set("v.topicProducts", returnedTopics);
            } else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },

    userFollowTopic : function(component, event, helper) {
        var buttonName = event.getSource().get("v.name");
        console.log('button label: ' + buttonName);
        var action = component.get("c.followTopic");
        action.setParams({
            "topicId": buttonName
        });
        console.log(action);
        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                console.log('success');
            } else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);

        //after the topic follow is committed, re initialize the two lists in the component so the state updates to 'Follow' or 'Followed'
        this.getCategoryTopics(component, event, helper);
        this.getProductTopics(component, event, helper);

    },

    userUnfollowTopic : function(component, event, helper) {
        var buttonName = event.getSource().get("v.name");
        console.log('button label: ' + buttonName);
        var action = component.get("c.unfollowTopic");
        action.setParams({
            "topicId": buttonName
        });
        console.log(action);
        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                console.log('success');
            } else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
        //after the topic follow deletion is committed, re initialize the two lists in the component so the state updates to 'Follow' or 'Followed'
        this.getCategoryTopics(component, event, helper);
        this.getProductTopics(component, event, helper);

    }





})