({
   /* goToUrl: function(component, event) {
        var link = event.currentTarget.dataset.link;
        var action = $A.get('e.force:navigateToURL');
        action.setParams({
            'url': link
        });
        action.fire();
    },*/
    completeOnboarding : function(component, event, helper) {
        var compEvent = component.getEvent("pageChange");
        compEvent.setParams({"message": "Close", "slide": "Done"});
        compEvent.fire();
    },
    goBack : function(component, event, helper) {
        var compEvent = component.getEvent("pageChange");
        compEvent.setParams({"message" : "6", "slide" : ""});
        compEvent.fire();
    },
    goToProfile : function(component, event, helper) {
        var user = component.get("v.user");
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/s/profile/" + user.Id
        });
        urlEvent.fire();
    }
})