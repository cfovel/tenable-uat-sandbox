({

    handleClick: function(component, event, helper) {
        helper.goToUrl(component, event, helper);
    },
    completeOnboarding : function(component, event, helper) {
        helper.completeOnboarding(component, event, helper);
    },
    goBack : function(component, event, helper) {
        helper.goBack(component, event, helper);
    },
    goToProfile : function(component, event, helper) {
        helper.goToProfile(component, event, helper);
    },
})