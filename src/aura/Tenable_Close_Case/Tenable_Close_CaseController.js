({
    doInit : function(component, event, helper) {
      var recordId = component.get("v.recordId");
      var action = component.get("c.getCaseStatus");
      action.setParams({
        recordId: recordId
      });
      action.setCallback(this, function(a) {
          var caseStatusInfo = a.getReturnValue();
          var state = a.getState();
          if (state === "SUCCESS") {
            if( caseStatusInfo.status === "Closed" ){
              if(caseStatusInfo.reopenEligible === true){
                $A.util.removeClass(component.find("reopenCaseButton").getElement(), "notVisible");
              }else{
                $A.util.removeClass(component.find("msgReopenPanel").getElement(), "notVisible");
              }
            }else{
              $A.util.removeClass(component.find("closeCaseButton").getElement(), "notVisible");
            }
          }
          else if (state === "ERROR") {}
          
      });
      $A.enqueueAction(action);
    },
    reopenCaseDialog : function(component, event, helper) {
	  helper.showPopupHelper(component, 'modalDialogReopen', 'slds-fade-in-');
	  helper.showPopupHelper(component,'modalBackdrop','slds-backdrop--');
    },
	openCloseCaseDialog : function(component, event, helper) {
	  helper.showPopupHelper(component, 'modalDialog', 'slds-fade-in-');
	  helper.showPopupHelper(component,'modalBackdrop','slds-backdrop--');
	},
	destroyCloseCaseDialog : function(component, event, helper) {
	  helper.hidePopupHelper(component, 'modalDialog', 'slds-fade-in-');
	  helper.hidePopupHelper(component, 'modalBackdrop', 'slds-backdrop--');	
	},
	destroyReopenCaseDialog : function(component, event, helper) {
	  helper.hidePopupHelper(component, 'modalDialogReopen', 'slds-fade-in-');
	  helper.hidePopupHelper(component, 'modalBackdrop', 'slds-backdrop--');	
	},
    save : function(component, event, helper) {
      helper.save(component);
    },
    reopen : function(component, event, helper) {
      var caseReopenComment = component.find("caseReopenComment").get("v.value");
      var recordId = component.get("v.recordId");
      var action = component.get("c.reopenCaseWithComments");
      action.setParams({
        recordId: recordId,
        CaseCommentContent: caseReopenComment
      });
      action.setCallback(this, function(a) {
          var caseStatusInfo = a.getReturnValue();
          var state = a.getState();
          if (state === "SUCCESS") {
		    helper.hidePopupHelper(component, 'modalDialogReopen', 'slds-fade-in-');
		    helper.hidePopupHelper(component, 'modalBackdrop', 'slds-backdrop--');
            $A.get('e.force:refreshView').fire();
          }
      });
      $A.enqueueAction(action);
    },
    waiting: function(component, event, helper) {
      $A.util.addClass(component.find("saving").getElement(), "saving");
      $A.util.addClass(component.find("reopening").getElement(), "saving");
      $A.util.addClass(component.find("btnPanel").getElement(), "notVisible");
      $A.util.addClass(component.find("btnRPanel").getElement(), "notVisible");
    },
    doneWaiting: function(component, event, helper) {
      $A.util.removeClass(component.find("saving").getElement(), "saving");
      $A.util.removeClass(component.find("reopening").getElement(), "saving");
      $A.util.removeClass(component.find("btnPanel").getElement(), "notVisible");
      $A.util.removeClass(component.find("btnRPanel").getElement(), "notVisible");
      $A.util.addClass(component.find("saving").getElement(), "notSaving");
      $A.util.addClass(component.find("reopening").getElement(), "notSaving");
    },
})