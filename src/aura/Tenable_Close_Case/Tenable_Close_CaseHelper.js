({
    showPopupHelper: function(component, componentId, className){
        var modal = component.find(componentId);
        $A.util.removeClass(modal, className + 'hide');
        $A.util.addClass(modal, className + 'open');
    },    
    hidePopupHelper: function(component, componentId, className){
        var modal = component.find(componentId);
        $A.util.addClass(modal, className+'hide');
        $A.util.removeClass(modal, className+'open');
    },
    save : function(component) {
        //Clear UI message before trying for file upload again
        component.find('msgPanel').set("v.body",[]);
    	var close_action = component.get("c.closeCaseWithComments");
    	var caseClosingComment = component.find("caseClosingComment").get("v.value");
    	var recordId = component.get("v.recordId");
    	var Resolution = "Closed by Customer";
    	var self = this;
        close_action.setParams({
          recordId: recordId,
          CaseCommentContent: caseClosingComment,
          ResolutionDetail: Resolution
        });
        close_action.setCallback(this, function(a) {
          var caseRecord = a.getReturnValue();
          var state = a.getState();
          if (state === "SUCCESS") {
            //location.reload(true);
		    self.hidePopupHelper(component, 'modalDialog', 'slds-fade-in-');
		    self.hidePopupHelper(component, 'modalBackdrop', 'slds-backdrop--');
            $A.get('e.force:refreshView').fire();
          }
          else if (state === "ERROR") {
            var errors = a.getError();
            var errorMsg = "";
            if (errors) {
                if (errors[0] && errors[0].message) {
                	errorMsg = errors[0].message;
                }
            } else {
                errorMsg = "Unknown error";
            }

            // show error to the user using a message
            $A.createComponents([
                ["ui:message",{
                    "title" : "Error",
                    "severity" : "error",
                }],
                ["ui:outputText",{
                    "value" : errorMsg
                }]
                ],
                function(components, status, errorMessage){
                    if (status === "SUCCESS") {
                        var message = components[0];
                        var outputText = components[1];
                        // set the body of the ui:message to be the ui:outputText
                        message.set("v.body", outputText);
                        var msgPanel = component.find("msgPanel");
                        // Replace div body with the dynamic component
                        msgPanel.set("v.body", message);
                    }
                    else if (status === "INCOMPLETE") {
                        console.log("No response from server or client is offline.")
                        // Show offline error
                    }
                    else if (status === "ERROR") {
                        console.log("Error: " + errorMessage);
                        // Show error message
                    }
                }
            );                


          }
          
        });

        $A.run(function() {
            $A.enqueueAction(close_action); 
        });        
        
    }
})