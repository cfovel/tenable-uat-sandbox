({
	readCaseComments : function(component) {
      var recordId = component.get("v.recordId");
      var action = component.get("c.getCaseComments");
      action.setParams({
        recordId: recordId
      });
      action.setCallback(this, function(a) {
        var state = a.getState();
        console.log('---> '+state);
        if (state === "SUCCESS") {
          var caseCommentList = a.getReturnValue();
          console.log(caseCommentList);
          component.set('v.caseCommentList', caseCommentList);
        }
        console.log('******* print out end');
      });
	  $A.enqueueAction(action);
	},
	savingButtonState : function(component){
	  console.log('==> saving button state...');
	  component.find("btnSave").set("v.label", "Saving...");
	  $A.util.removeClass(component.find("btnSave").getElement(), "notSaving");
	  $A.util.addClass(component.find("btnSave").getElement(), "saving");
	},
	readyButtonState : function(component){
	  component.find("btnSave").set("v.label", "Submit");
	  $A.util.addClass(component.find("btnSave").getElement(), "notSaving");
	  $A.util.removeClass(component.find("btnSave").getElement(), "saving");
	}
})