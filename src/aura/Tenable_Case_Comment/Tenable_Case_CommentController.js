({
	doInit : function(component, event, helper) {
      var action = component.get("c.isCommentAllowed");
      var recordId = component.get("v.recordId");
      action.setParams({
        recordId: recordId
      });
      action.setCallback(this, function(a) {
        var state = a.getState();
        if (state === "SUCCESS") {
          var isCommentAllowed = a.getReturnValue();
          component.set("v.isCommentAllowed", isCommentAllowed);
        }
      });
	  $A.enqueueAction(action);
	  
	  helper.readCaseComments(component);
	},
	onRefreshView : function(component, event, helper) {
	   helper.readCaseComments(component);
	},
	save : function(component, event, helper) {
	  helper.savingButtonState(component);
      var recordId = component.get("v.recordId");
      var action = component.get("c.addCaseComments");
      var customerCaseComment = component.find("customerCaseComment").get("v.value");
      action.setParams({
        recordId: recordId,
        CaseCommentContent: customerCaseComment
      });
      action.setCallback(this, function(a) {
	    helper.readyButtonState(component);
	    component.find("customerCaseComment").set("v.value", "");
	    $A.get('e.force:refreshView').fire();
        var state = a.getState();
        console.log('===> state='+state);
        if (state === "SUCCESS") {
          helper.readCaseComments(component);
        }
      });
	  $A.enqueueAction(action);
	}
})