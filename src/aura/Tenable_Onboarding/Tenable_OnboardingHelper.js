({	         
    init : function(component, event, helper) {
        
        var action = component.get("c.getUserRecord");
        console.log(action);
        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var user = response.getReturnValue();
                var pageUrl = window.location.href;
                var uniquePage = pageUrl.includes('tenable-onboarding');
                // if user is on the own page for Onboarding, the flow should always start at screen 1
                if(uniquePage){
                    component.set("v.displayOnboarding", true);
                    component.set("v.user", user);
                // if user is on home page, flow should only pop up if the user hasn't completed it.  The user should also be directed to the first slide they haven't completed (if they login, complete part of the onboarding process, then log back in later )
                } else if (!user.Onboarding_Complete__c) {
                    component.set("v.displayOnboarding", true);
                    component.set("v.user", user);
                    console.log('new: ' + user.Completed_Notification_Slide__c);
                    if(user.Completed_Notification_Slide__c){
                        component.set("v.page", '6');
                    } else if(user.Completed_Groups_Slide__c){
                        component.set("v.page", '5');
                    } else if(user.Completed_Topics_Slide__c){
                        component.set("v.page", '4');
                    } else if(user.Completed_Profile_Slide__c){
                        component.set("v.page", '3');
                    } else if(user.Completed_Welcome_Slide__c){
                        component.set("v.page", '2');
                    }
                } 
            } else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action); 
    },
    
        handlePageChange : function(component, event, helper) {
        var slideComplete=event.getParam("slide");
        var pageNumber=event.getParam("message");
            var action = component.get("c.completeSlide");
            // if "message" equals "close", the user has just completed the entire modal, so the 'Complete_Modal__c' field should be checked and the modal closed
        if(pageNumber=='Close'){
            console.log('in close');
            action.setParams({
                "slide": slideComplete
            });
            console.log(action);
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                    console.log('in success');
                    component.set("v.page", pageNumber);
                } else {
                    console.log("Failed with state: " + state);
                }
            });
            $A.enqueueAction(action);
            this.closeModal(component, event, helper);
            // if "slide' is not blank, the user just completed a slide, and the database needs to be called to check the corresponding field on the User record
     } else if(slideComplete != ''){
        action.setParams({
            "slide": slideComplete
        });
        console.log(action);
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                console.log('in success');
                component.set("v.page", pageNumber);
            } else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);

        // if "slide" is blank, the user is going back to a previous screen and therefore the database doesn't need to be called to check a slide complete field on the User record.
            } else {
                component.set("v.page", pageNumber);
            }
	},
    closeModalFinal : function(component, event, helper) {
        console.log('in close modal final');
        var action = component.get("c.completeSlide");
            action.setParams({
                "slide": "Done"
            });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                console.log('modal completed');
            } else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },

    closeModal: function(component, event, helper) {
        component.set("v.displayOnboarding", false);
    }
})