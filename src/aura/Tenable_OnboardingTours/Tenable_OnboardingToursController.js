({
	goToNext : function(component, event, helper) {
        helper.goToNext(component, event, helper);
	},
    goBack : function(component, event, helper) {
        helper.goBack(component, event, helper);
	},
    completeOnboarding : function(component, event, helper) {
        helper.completeOnboarding(component, event, helper);
    },
    goBackToTours : function(component, event, helper) {
	    console.log('in first helper 1');
        helper.goBackToTours(component, event, helper);
    },
})