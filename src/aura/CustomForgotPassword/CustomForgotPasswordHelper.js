({
    validateUserNameAndPassword: function(component,event,helper) {
        
    },
    
    handleForgotPassword: function (component, event, helpler) {
        var username = component.find('username').get('v.value');
        console.log('The value of username is : ' + username);
        if (username != '')
        {
            var action = component.get("c.forgotPassword");
            
            action.setParams({username:username});
            action.setCallback(this, function(a) {
                console.log('The value of a.getReturnValue() is : ' + a.getReturnValue());
                if(a.getState() == 'SUCCESS')
                {
                    component.set("v.errorMessage",'We\'ve just sent you an email to reset your password.');
                    component.set("v.showError",true);
                }
                else if(a.getState() == 'ERROR')
                {
                    component.set("v.errorMessage",'There was an error.  Please contact your system admin.');
                    component.set("v.showError",true);
                }
            });
            $A.enqueueAction(action);
        }
        else
        {
            component.set("v.errorMessage",'Username cannot be blank');
            component.set("v.showError",true);
        }
        
    }
})