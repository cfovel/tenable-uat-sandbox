/**
 * Created by 7Summits on 6/5/18.
 */
({
    init: function (cmp, evt, helper) {
        console.log('in first init');
        helper.init(cmp, evt, helper);
        helper.getUser(cmp, evt, helper);

    },
    openWelcome: function (cmp, evt, helper) {
        cmp.set('v.page', '1');

    },
    openProfile: function (cmp, evt, helper) {
        cmp.set('v.page', '2');

    },

    openTopic: function (cmp, evt, helper) {
        cmp.set('v.page', '3');

    },
    openGroup: function (cmp, evt, helper) {
        cmp.set('v.page', '4');

    },
    openNotifications: function (cmp, evt, helper) {
        cmp.set('v.page', '5');

    },
    openTour: function (cmp, evt, helper) {
        cmp.set('v.page', '6');

    },
    handlePageChange : function(component, event, helper) {
        helper.handlePageChange(component, event, helper);
    },
    closeModal: function(component, event, helper) {
        component.set("v.displayOnboarding", false);
    }
})