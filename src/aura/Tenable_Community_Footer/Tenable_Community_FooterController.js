({
	init : function(component, event, helper) {
      //retrieve logged user support access config
      var action = component.get("c.getFooterConditions");
      action.setCallback(this, function(response) {   
        var state = response.getState();
        if(state === "SUCCESS") {
          var FooterViewRender = JSON.parse(response.getReturnValue());
          console.log(FooterViewRender);
          if( FooterViewRender.isSuccess ){
	        component.set("v.ShowCollab", FooterViewRender.ShowCollab); 
	        component.set("v.ShowCases", FooterViewRender.ShowCases);
          }
          else{
            console.log('Problem getting footer render conditions, server error: ' + FooterViewRender.errMsg);
          }
        } else {
          console.log('Problem getting footer render conditions, response state: ' + state);
        }
      });
      $A.enqueueAction(action);
	}
})