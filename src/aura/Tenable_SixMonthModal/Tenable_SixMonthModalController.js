/**
 * Created by 7Summits on 6/4/18.
 */
({
    init : function(component, event, helper) {
        var pageUrl = window.location.href;
        helper.init(component, event, helper);
        helper.getChatterGroupUrl(component, event, helper);
    },
    closeModal: function(component, event, helper) {
        component.set("v.displayModal", false);
        helper.completeModal(component, event, helper);
    }
})