/**
 * Created by 7Summits on 6/4/18.
 */
({
    init : function(component, event, helper) {

        var action = component.get("c.getUserRecord");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var user = response.getReturnValue();
                if(user.Display_6_Month_Modal__c){
                    component.set("v.displayModal", true);
                    component.set("v.user", user);
                } else {
                    component.set("v.displayModal", false);
                }
            }
        });
        $A.enqueueAction(action);
    },

    completeModal : function(component, event, helper) {
        console.log('in complete');
        var action = component.get("c.finishModal");
        action.setParams({
            "communityUser": component.get("v.user")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                console.log('6 month updated');
                } else {
                console.log('6 month update error');
                }
        });
        $A.enqueueAction(action);
    },


    getChatterGroupUrl : function(component, event, helper) {
        console.log('in complete');
        var action = component.get("c.getGroupUrl");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.feedbackLink", response.getReturnValue());
            } else {
                console.log('url not retrieved');
            }
        });
        $A.enqueueAction(action);
    }

})