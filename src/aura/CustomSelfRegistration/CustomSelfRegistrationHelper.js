({
    qsToEventMap: {
        'startURL'  : 'e.c:setStartUrl'
    },
    
    handleSelfRegister: function (component, event, helpler) {
        var accountId = component.get("v.accountId");
        var regConfirmUrl = component.get("v.regConfirmUrl");
        var firstname = component.find("firstname").get("v.value");
        var lastname = component.find("lastname").get("v.value");
        var email = component.find("email").get("v.value");
        var company = component.find("company").get("v.value");
        var lmsid = component.find("lmsid").get("v.value");
        var includePassword = component.get("v.includePasswordField");
        var password = component.find("password").get("v.value");
        var confirmPassword = component.find("confirmPassword").get("v.value");
        var action = component.get("c.selfRegister");
        var extraFields = JSON.stringify(component.get("v.extraFields"));   // somehow apex controllers refuse to deal with list of maps
        var startUrl = component.get("v.startUrl");
        
        startUrl = decodeURIComponent(startUrl);
        if($A.util.isEmpty(firstname))
        {
            component.set('v.showError',true);
            component.set('v.errorMessage','First name is required');
        }
        else if($A.util.isEmpty(lastname))
        {
            component.set('v.showError',true);
            component.set('v.errorMessage','Last name is required');
        }
        else if($A.util.isEmpty(email))
        {
            component.set('v.showError',true);
            component.set('v.errorMessage','Email is required');
        }
        else if($A.util.isEmpty(company))
        {
            component.set('v.showError',true);
            component.set('v.errorMessage','Company is required');
        }
        else
        {
            action.setParams({firstname:firstname,lastname:lastname,email:email,
                              company:company,lmsId:lmsid});
            action.setCallback(this, function(a){
                var state = a.getState();
                var rtnValue = a.getReturnValue();

                if(state === "SUCCESS")
                {
                    if (rtnValue !== null) 
                    {
                    	if (rtnValue.includes('invalid email'))
	                    {
	                    	component.set("v.errorMessage",'Invalid email address');
	                    }
	                    else
	                    {
	                    	component.set("v.errorMessage",rtnValue);
	                    }	
                        component.set("v.showError",true);
                        if (!rtnValue.includes('Insert failed'))
                        {
	                        component.find("firstname").set("v.value",'');
	                        component.find("lastname").set("v.value",'');
	                        component.find("email").set("v.value",'');
	                        component.find("company").set("v.value",'');
	                        component.find("lmsid").set("v.value",'');
                        }
                	}
                }
                else if (state === "ERROR")
                {
                    var errors = a.getError();
                    component.set("v.errorMessage",errors[0].message);
                    component.set("v.showError",true);
                }
            });
            $A.enqueueAction(action);
        }
    },
    
    getExtraFields : function (component, event, helpler) {
        var action = component.get("c.getExtraFields");
        action.setParam("extraFieldsFieldSet", component.get("v.extraFieldsFieldSet"));
        action.setCallback(this, function(a){
            var rtnValue = a.getReturnValue();
            if (rtnValue !== null) {
                component.set('v.extraFields',rtnValue);
            }
        });
        $A.enqueueAction(action);
    }    
})