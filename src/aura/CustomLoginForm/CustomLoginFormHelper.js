({
    
    qsToEventMap: {
        'startURL'  : 'e.c:setStartUrl'
    },
    
    handleLogin: function (component, event, helpler) {
        console.log('Inside handlelogin helper');
        //var action = component.get("c.login");
        var username = component.find("username").get("v.value");
        var password = component.find("password").get("v.value");
        var hiddenForm = document.getElementById("Auth0error");
        //var hiddenForm = document.getElementById("hiddenForm");
        //Below code is to add the first part of the url to make it an absolute URL instead of relative
        var urlinfo = window.location.protocol + "//" + window.location.host + "/";
        //var startUrl = component.get("v.startUrl");
        //console.login('The value of startUrl is : ' + startUrl);
        //startUrl = urlinfo + startUrl;
        
        //startUrl = decodeURIComponent("https%3A%2F%2Fdevmb-tenable.cs13.force.com%2Fsuccess%2Flogin%3Fso%3D00DW0000008pqxA%0A");
        //startUrl = urlinfo;
        
        //Instantiating the WebAuth variable for connecting to it
        if (!auth0) {
            console.log("I do not have access to the resource");
        } else {
            console.log("I have auth0 from the resource");
        }
        
        var webAuth = new auth0.WebAuth({
            domain:         'tenable-prod.auth0.com',
            clientID:       'gK3eF78SodOY9I0KAY6E5n65IKrbG4vk',
            //redirectUri:    'https://community.tenable.com/login?so=00D300000000pZp',
            redirectUri: urlinfo + 'login',  
            responseType:	'code'
        });
        
        var auth0Data = webAuth.redirect.loginForSalesforceWithCredentials({
            connection: 'Username-Password-Authentication',
            username: username,
            password: password,
            scope: 'openid email',
            hiddenForm: hiddenForm
        },function(err,authResult){
            //TODO make this so that errors or unauth'd requests show the form with the error
            if(err)
            {
                console.log("Unhiding the Auth0 dialog");
                hiddenForm.style.display = '';
            }
        });
        
        /*webAuth.parseHash((err, authResult) => {
            if (authResult) 
            {
            // Save the tokens from the authResult in local storage or a cookie
            localStorage.setItem('access_token', authResult.accessToken);
            localStorage.setItem('id_token', authResult.idToken);
        } 
       	else if (err) 
        {
            // Handle errors
            console.log(err);
        }
    });*/
        /*action.setParams({username:username, password:password, startUrl:startUrl});
        action.setCallback(this, function(a){
            var rtnValue = a.getReturnValue();
            if (rtnValue !== null) {
                component.set("v.errorMessage",rtnValue);
                component.set("v.showError",true);
            }
        });
        $A.enqueueAction(action);*/
    },
    
    getIsUsernamePasswordEnabled : function (component, event, helpler) {
        var action = component.get("c.getIsUsernamePasswordEnabled");
        action.setCallback(this, function(a){
            var rtnValue = a.getReturnValue();
            if (rtnValue !== null) {
                component.set('v.isUsernamePasswordEnabled',rtnValue);
            }
        });
        $A.enqueueAction(action);
    },
    
    getIsSelfRegistrationEnabled : function (component, event, helpler) {
        var action = component.get("c.getIsSelfRegistrationEnabled");
        action.setCallback(this, function(a){
            var rtnValue = a.getReturnValue();
            if (rtnValue !== null) {
                component.set('v.isSelfRegistrationEnabled',rtnValue);
            }
        });
        $A.enqueueAction(action);
    },
    
    getCommunityForgotPasswordUrl : function (component, event, helpler) {
        var action = component.get("c.getForgotPasswordUrl");
        action.setCallback(this, function(a){
            var rtnValue = a.getReturnValue();
            console.log('The value of rtnValue is : ' + rtnValue);
            if (rtnValue !== null) {
                component.set('v.communityForgotPasswordUrl',rtnValue);
            }
        });
        $A.enqueueAction(action);
    },
    
    getCommunitySelfRegisterUrl : function (component, event, helpler) {
        var action = component.get("c.getSelfRegistrationUrl");
        action.setCallback(this, function(a){
            var rtnValue = a.getReturnValue();
            if (rtnValue !== null) {
                component.set('v.communitySelfRegisterUrl',rtnValue);
            }
        });
        $A.enqueueAction(action);
    }
})