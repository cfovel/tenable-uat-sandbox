({
    updateName : function(component, event, helper) {
        console.log('1');
        var action = component.get("c.updateUserNames");
        console.log('4: ' + component.get("v.user.FirstName"))
        action.setParams({
            "currentUser": component.get("v.user")
        });
        console.log('2');
        console.log(action);
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                console.log('resolved: ' + response.getReturnValue());
                console.log('3');
             //   this.goToNext();
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);

    },
    goToNext : function(component, event, helper) {
        var compEvent = component.getEvent("pageChange");
        if ((component.get("v.user.FirstName") == null || component.get("v.user.FirstName") == '') ||
            (component.get("v.user.LastName") == null || component.get("v.user.LastName") == '')
        ) {
            component.set("v.error", "Values are required for First Name, Last Name");
        }else
        {
            compEvent.setParams({"message" : "3", "slide" : "Profile"});
            compEvent.fire();
        }

	},
        goBack : function(component, event, helper) {
        var compEvent = component.getEvent("pageChange");
            compEvent.setParams({"message" : "1", "slide" : ""});
                compEvent.fire();
    }
})