({
	goToNext : function(component, event, helper) {
        var isValidF = component.find("firstNameInput").get("v.validity").valid;
        var isValidL = component.find("lastNameInput").get("v.validity").valid;
        if(isValidF && isValidL){
        	helper.updateName(component, event, helper);
        	helper.goToNext(component, event, helper);    
        }
	},
    goBack : function(component, event, helper) {
        helper.goBack(component, event, helper);
	}
})