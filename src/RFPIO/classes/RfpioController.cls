/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class RfpioController {
    webService static String associateOrDissociateRecords(String json, Boolean isAssociate) {
        return null;
    }
    webService static void associateRecords(String json, Boolean isAssociate) {

    }
    webService static Id createRecord(String json) {
        return null;
    }
    webService static String getCompanyId() {
        return null;
    }
    webService static String getRecordData(String dataReceived) {
        return null;
    }
}
