/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class RkActivationXmlParser {
global class ActivationObject {
    global String activationTime;
    global String baseRainkingAppUrl;
    global String description;
    global String endPoint;
    global String status;
    global String token;
    global String version;
    global ActivationObject() {

    }
}
}
