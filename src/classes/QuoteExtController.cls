public with sharing class QuoteExtController 
{
    private Id quoteId;
    public Boolean hasError {get; set;}

    
    public QuoteExtController(ApexPages.StandardController stdController) 
    {
        quoteId = stdController.getId();
        hasError  = false;
    }
    
    public PageReference onSubmit() 
    {
        pageReference pr = new PageReference('/' + quoteId);
        Savepoint sp = Database.setSavepoint();
        
        if(quoteId!= NULL)
        {
            try
            {
                SBAA.ApprovalAPI.submit(quoteId, SBAA__Approval__c.Quote__c);
                pr.setRedirect(true);

            }
            catch(exception e)
            {
               Database.rollback(sp);
               ApexPages.addMessages(e); 
               pr = null;
               hasError = true;
            }
        }

        return pr;
   }
 
    
    public PageReference onRecall() 
    {
        if (quoteId != null) 
        {
            SBAA.ApprovalAPI.recall(quoteId, SBAA__Approval__c.Quote__c);
        }
        PageReference pr = new PageReference('/' + quoteId);
        pr.setRedirect(true);
        return pr;
    }
    
    
    
    public pageReference QuoteInformation()
    {
        return new PageReference('/' + quoteId);
    }
}