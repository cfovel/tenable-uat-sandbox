public with sharing class accounts {
/******************************************************************************
*
*   Custom Controller for Account Page
*
********************************************************************************/
    /*****************************************************************************************
    *   Variable Initialization
    ******************************************************************************************/
    public Id pageid = ApexPages.currentPage().getParameters().get('id');       // Store Account ID
    private final Account entry;                                                // Store Account Record
    private final ApexPages.standardController stdController;                   // Set-up Controller Class
    private List<OpportunityLineItem> lineItems;                                // Products for closed-won Opportunities
    private Id acctId;
    
    /*****************************************************************************************
    *   Custom Controller Initialization
    ******************************************************************************************/
    public accounts(ApexPages.StandardController stdController) {               // Controller Set-up Method
        Account entry = (Account)stdController.getRecord();                     // First get record from Page Call
        this.stdController = stdController;                                     // Set-up Controller
        lineItems = new List<OpportunityLineItem>();
        acctId = ApexPages.currentPage().getParameters().get('id');
    }   

    public List<OpportunityLineItem> getLineItems() {
        List<Opportunity> opps = new List<Opportunity>();
        opps = [SELECT Id, Name FROM Opportunity WHERE IsClosed = true AND IsWon = true AND AccountId = :acctId];
        if(!opps.isEmpty()) {
            List<Id> oppIds = new List<Id>();
            for(Opportunity opp : opps) {
                oppIds.add(opp.Id);
            }
            lineItems = [SELECT Product2.name, quantity, Description, ServiceDate, unitPrice, listPrice, Opportunity.name, 
            				Annual_Contract_Value__c, Base_Unit_Price__c
                            FROM OpportunityLineItem WHERE OpportunityId in :oppIds];
        }
            
        return lineItems;
    }
}