global class TerritoryLeadUpdate implements Schedulable {
    
    global void execute(SchedulableContext sc) {
        
        Database.executeBatch(new BatchTerritoryLeadUpdate(), 200);
        
    }
   
}