public class CreateNetSuitePO 
{
    public Contract c;
	
    public CreateNetSuitePO(ApexPages.StandardController controller){
        this.c = (Contract)controller.getRecord();
        //pageReference cpage = CreatePOInNetsuite(ApexPages.currentPage().getParameters().get('Id'));
    }
    
    //public void CreatePOInNetsuite(set<Id> ContractIds)
    public PageReference CreatePOInNetsuite(){
        String ContractId = ApexPages.currentPage().getParameters().get('id');
        set<id> contractIds = new set<id>();
        set<id> payloadIds = new set<id>();
        
        system.debug('Purchase Order Add Button was Pressed');
        //Adding Contract to PayloadId set
        payloadIds.add(ContractId);
        
        Contract con = [select id, AccountId from Contract where id = :ContractId];
        //adding vendor account to the payloadIds
        payloadIds.add(con.AccountId);
        
        list<sObject> poItemList = [select Id from Purchase_Order_Item__c where Contract__c =: ContractId ];
        //adding the PO Line Ids to the payload Id map
        for(sObject o: poItemList){
            payloadIds.add(string.ValueOf(o.get('Id')));
        }
        
        HttpResponse contractMuleEvent = mulesoftIntegrationController.mulesoftEventHandler(payloadIds, ContractId, 'purchaseOrderCreate');
		
        PageReference page = new PageReference('/' + ContractId);
        page.setRedirect(true);
        system.debug('Roy8: ' + page);
        return page;
    }
    
    
}