/**
 * Created by francoiskorb on 8/22/17.
 */

@IsTest
public with sharing class SS_LeaderboardController_test
{
	static final Integer howMany = 10;

	@IsTest
	private static void testReputationEnabled()
	{
		Boolean reputation = SS_LeaderboardController.isReputationEnabled();

		System.assertNotEquals(null, reputation);
	}

	@IsTest
	private static void testGetCandidates()
	{
		test.startTest();

		String sitePrefix = SS_LeaderboardController.getSitePrefix();
		System.assertNotEquals(null, sitePrefix);

		String networkId = Network.getNetworkId();
		String total = String.valueOf(howMany);
		List<String> data = SS_LeaderboardController.getCandidates(false, total, networkId);
		System.assertNotEquals(null, data);

		data = SS_LeaderboardController.getCandidates(true, total, networkId);
		System.assertNotEquals(null, data);

		test.stopTest();
	}


	@isTest
	private static void testCallout()
	{
		String networkId = Network.getNetworkId();

		Test.startTest();
		
		// Set mock callout class
		Test.setMock(HttpCalloutMock.class, new SS_LB_MockHttpResponseGenerator());

		List<String> idList = new List<String>();
		//String commaSepIds = String.join(idList, ',');

		String data = SS_LeaderboardController.getDataRequest(networkId,idList);
		system.assertNotEquals(null, data);

		Test.stopTest();
	}

	@IsTest
	private static void testGetData()
	{
		test.startTest();
		Test.setMock(HttpCalloutMock.class, new SS_LB_MockHttpResponseGenerator());

		String networkId = Network.getNetworkId();
		String total = String.valueOf(howMany);

		String data = SS_LeaderboardController.getData(false, total);
		system.assertNotEquals(null, data);

		test.stopTest();
	}

}