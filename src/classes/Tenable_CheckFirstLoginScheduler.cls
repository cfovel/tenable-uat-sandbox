/**
 * Created by lucassoderstrum on 5/24/18.
 */

global class Tenable_CheckFirstLoginScheduler implements Schedulable {


        global void execute(SchedulableContext sc)
        {
            Tenable_CheckFirstLogin batchClass = new Tenable_CheckFirstLogin ();
              database.executebatch(batchClass,200);   //
        }
    }