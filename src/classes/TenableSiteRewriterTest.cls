/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TenableSiteRewriterTest {

  static testMethod void decomissionedThreadTest() {
    TenableSiteRewriter testCtrl = new TenableSiteRewriter();
    PageReference pref1 = new PageReference('/success/thread/');
    PageReference pref2 = testCtrl.mapRequestUrl(pref1);
    String mappedUrl = pref2.getUrl();
    system.assertEquals(mappedUrl, '/apex/TenableSiteRedirect');
  }
  
  static testMethod void decomissionedThreadNegativeTest() {
    TenableSiteRewriter testCtrl = new TenableSiteRewriter();
    PageReference pref1 = new PageReference('/profile');
    PageReference pref2 = testCtrl.mapRequestUrl(pref1);
    system.assertEquals(pref2, null);
  }
  
  static testMethod void genericMappingTest() {
    TenableSiteRewriter testCtrl = new TenableSiteRewriter();
    List<PageReference> prefList = testCtrl.generateUrlFor(null);
  }
  
}