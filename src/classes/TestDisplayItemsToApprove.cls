/*************************************************************************************
Class: TestDisplayItemsToApprove
Author: Bharathi.M
Date: 10/03/2017
Details: This class is the test class for the batch CtrlDisplayItemsToApprove
***************************************************************************************/
@isTest(seealldata = true)
private class TestDisplayItemsToApprove
{
    static TestMethod void TestCreateCPQQuotes()
    {
        Profile p1 = [SELECT Id FROM Profile WHERE Name='Tenable - Sales Ops User']; 
        User u = new User(Alias = 'standt', Email='salesops@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p1.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='salesops@testTenable1.com');
        
        system.runAs(u)
        {
            Id recordTypeId = [SELECT ID,Name FROM RecordType WHERE SObjectType='SBQQ__Quote__c' AND Name='Draft'].Id;   
            /*
            Product2 p = new product2(name='x');    
            insert p;
            Pricebook2 pb = new pricebook2(name='AMER');
            insert pb;
            
            Account acc = new Account(name='Test CPQAccount', BillingState = 'MD', BillingCountry = 'US');
            insert acc;
            
            Contact con = new Contact(FIrstName = 'Test', LastName='Quote',AccountID = acc.Id);
            insert con;
            
            Opportunity opp = new Opportunity(AccountId = acc.Id, name='Test CPQOpp', stageName='Open', closeDate=System.Today().AddMonths(1), Pricebook2 =pb, Opp_Reg_Type__c='Channel Out');
            insert opp;
            
            SBQQ__Quote__c q = new SBQQ__Quote__c(SBQQ__Account__c = acc.Id,RecordTypeId=recordTypeId, SBQQ__Opportunity2__c = opp.Id,SBQQ__StartDate__c = System.Today() , SBQQ__EndDate__c =System.Today().addYears(1), Channel_Direct__c = 'Direct');
            insert q;
            
            SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c(SBQQ__StartDate__c = System.Today() , SBQQ__EndDate__c =System.Today().addYears(1), SBQQ__Product__c=p.Id, Units__c=1.0,SBQQ__Quote__c=q.Id);
            insert ql;
            */
            Product2 p = [SELECT ID,Name FROM Product2 WHERE name='Tenable.io Vulnerability Management' Limit 1];    
            Pricebook2 pb = [SELECT ID,Name FROM Pricebook2 WHERE name='AMER Price Book' Limit 1];              
            
            Account partner = [SELECT ID from Account WHERE RecordType.name = 'Partner Account' Limit 1];     
         
            Utils.isTest = true;
            accountTriggerHandler.enforceCountry = false;
            Account acc = new Account(Name='Test1 Inc.', BillingState='CA', BillingCountry='US', website='http://www.test1.com');
            insert acc;
            
            Opportunity opp = new Opportunity(name='test', accountid = acc.id, opp_reg_type__c = 'Channel In', StageName='Prospect', CloseDate=Date.newInstance(2015,10,10), Demo_Date__c = Date.newInstance(2020, 10, 20),Primary_Distributor__c=partner.Id);
            insert opp; 
            
            SBQQ__Quote__c q = new SBQQ__Quote__c(SBQQ__Account__c = acc.Id,RecordTypeId=recordTypeId, SBQQ__Opportunity2__c = opp.Id,SBQQ__StartDate__c = System.Today() , SBQQ__EndDate__c =System.Today().addYears(1), Channel_Direct__c = 'Channel',SBQQ__Primary__c=true,Primary_Distributor_Account__c=partner.Id,SBQQ__PriceBook__c=pb.Id);
            insert q;
            
            SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c(SBQQ__StartDate__c = System.Today() , SBQQ__EndDate__c =System.Today().addYears(1), 
                SBQQ__Product__c=p.Id, Units__c=1.0,SBQQ__Quote__c=q.Id,SBQQ__ListPrice__c=1000);
            insert ql;
            
            sbaa__ApprovalRule__c apprule = new sbaa__ApprovalRule__c(Name='Test Approval Rule', sbaa__TargetObject__c='SBQQ__Quote__c');
            insert apprule;
            
            sbaa__Approver__c app = new sbaa__Approver__c(Name = UserInfo.getUserName() , sbaa__User__c =UserInfo.getUserId());
            insert app;
            
            sbaa__Approval__c appr = new sbaa__Approval__c(sbaa__Status__c = 'Requested', sbaa__RecordField__c = 'Quote__c', sbaa__ApprovalStep__c = 10, sbaa__Rule__c=appRule.Id, sbaa__AssignedTo__c = UserInfo.getUserId(),Quote__c = q.Id );
            insert appr;
            
            CtrlDisplayItemsToApprove disp = new CtrlDisplayItemsToApprove(); 
        }
    }
    
    static TestMethod void TestCreateOldQuotes()
    {
        Profile p1 = [SELECT Id FROM Profile WHERE Name='Tenable - Sales Ops User']; 
        User u = new User(Alias = 'standt', Email='salesops@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p1.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='salesops@testTenable2.com');
        
        system.runAs(u)
        {
            Id recordTypeId = [SELECT ID,Name FROM RecordType WHERE SObjectType='SBQQ__Quote__c' AND Name='Draft'].Id;   
            Id AccRecordTypeId = [SELECT Id,Name FROM RecordType WHERE SObjectType='Account' AND Name='Partner Account'].Id;
            
            Product2 p = new product2(name='Tenable.io Container Security');    
            insert p;
            Pricebook2 pb = new pricebook2(name='test');
            insert pb;
             
            Account acc = new Account(name='Test CPQAccount', BillingState = 'MD', BillingCountry = 'US',RecordTypeId=AccRecordTypeId, Authorization_Status__c = 'Authorized Partner', Security_Center_Authorization_Level__c = 'Distribution - AMER (19%)');
            insert acc;
            
            Contact con = new Contact(FirstName = 'Test', LastName='Quote',AccountID = acc.Id);
            insert con;
            
            Opportunity opp = new Opportunity(AccountId = acc.Id, name='Test CPQOpp', stageName='Open', closeDate=Date.newInstance(2017,10,12), Pricebook2 =pb, Opp_Reg_Type__c='Channel Out');
            insert opp;
            
            Quote__c Q = new Quote__c(Opportunity__c = opp.Id, Billing_Contact__c= con.Id, Licensee_Contact__c = con.Id,Partner__c = acc.Id);
            insert Q;
            
            QuoteLine__c Ql = new QuoteLine__c(Quote__c = Q.Id,Order_Type__c = 'New', 
            Category__c = 'Products',Family__c = 'Tenable.io',Product__c = p.Id, Discount__c = 100,
            License_Type__c = 'Subscription', Unit_Type__c = '1', Container_Size__c = '5 GB',
            Quantity__c = 1, Product_SKU__c='TIO-VM',
            Start_Date__c = System.Today(),End_Date__c = System.Today() + 1);
            
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments('Submitting request for approval.');
            req1.setObjectId(Q.id);
            req1.setSubmitterId(u.Id);

            Approval.ProcessResult result = Approval.process(req1);
            
            System.assert(result.isSuccess(), 'Result Status:'+result.isSuccess());
            
            //System.assertEquals('Pending', result.getInstanceStatus(),'Instance Status'+result.getInstanceStatus());

            CtrlDisplayItemsToApprove disp = new CtrlDisplayItemsToApprove(); 
        }
    }
}