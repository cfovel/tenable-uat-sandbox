/**
 * This class contains unit tests for validating the behavior of 
 * Batch Apex closses
 */
@isTest
private class testBatchApex {
    
    @testSetup
    public static void setupData() {
        TestDataFactory.insertUsers(new List<String>{'Tenable - Support Engineer'});
        TestDataFactory.insertAccounts(10);
        TestDataFactory.insertOpportunities(1);
        TestDataFactory.insertCampaigns(3);
        TestDataFactory.insertProducts();
        TestDataFactory.insertPricebooks();
        TestDataFactory.insertOppProducts();
        
    }    
    
    @isTest
    static void testUpdateOpenCaseCount() {
        Utils.isTest = true;
        accountTriggerHandler.enforceCountry = false;
        Account a = new Account(Name='Test1 Inc.', BillingState='CA', BillingCountry='US');         // Create a test account
        insert a;
        
        Case c1 = new Case(Description='Test1 case', accountId = a.id, status='Working');
        Case c2 = new Case(Description='Test2 case', accountId = a.id, status='Pending');
        Case[] caseList = new Case[] {c1, c2};
        insert caseList;
        
        Test.StartTest();
        database.executeBatch(new baUpdateOpenCaseCount(),200);
        Test.StopTest();
        a =[SELECT Open_Case_Count__c from Account WHERE Id = :a.Id];
        System.assertEquals(a.Open_Case_Count__c, 2);
    }
    
    @isTest
    static void testMissingCaseComments() {
        Utils.isTest = true;
        accountTriggerHandler.enforceCountry = false;
        Account a = new Account(Name='Test1 Inc.', BillingState='CA', BillingCountry='US');         // Create a test account
        insert a;
        
        Case c1 = new Case(Description='Test1 case', accountId = a.id, status='Working');
        Case c2 = new Case(Description='Test2 case', accountId = a.id, status='Pending');
        Case[] caseList = new Case[] {c1,c2};
        insert caseList;
        
        String fromAddr = 'support@tenable.com';
        EmailMessage e1 = new EmailMessage(Subject='Test1 subject', TextBody = 'Test1 body', parentId=c1.id, FromAddress = fromAddr);
        EmailMessage e2 = new EmailMessage(Subject='Test2 subject', TextBody = 'Test2 body', parentId=c2.id, FromAddress = fromAddr);
        EmailMessage[] eList = new EmailMessage[] {e1, e2};
        insert eList;
        
        CaseComment cc1 = new CaseComment(ParentId = c1.Id, CommentBody = e1.TextBody, isPublished=TRUE);
        insert cc1;
        
        Datetime executeTime = (System.now()).addMinutes(1);                                        // schedule the job to run in 1 minute
        String cronExpression = Utils.GetCRONExpression(executeTime);
            
        Test.StartTest();
            database.executeBatch(new baMissingCaseComments(Date.today().format(), fromAddr),200);
        
            
            ScheduleCaseComment scheduledJob = new ScheduleCaseComment(eList);                      // Instantiate a new Scheduled Apex class
            String jobId = System.schedule('ScheduleCaseComment ' + executeTime.getTime(),cronExpression,scheduledJob);
            
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
                                NextFireTime
                                FROM CronTrigger WHERE id = :jobId];

        Test.StopTest();
        String d = date.today().format();
        DateTime dt = DateTime.parse(d + ' 12:00 AM');
        String docName = 'Log-Missing Case Comments-' + dt.format('yyyy-MM-dd HH:mm:ss');
        Document[] docs = [select Name from Document];
        System.assertEquals(1, docs.size());
        System.assertEquals(docName, docs[0].name);

        System.assertEquals(cronExpression, ct.CronExpression);                                     // Verify the expressions are the same
    
        System.assertEquals(0, ct.TimesTriggered);                                                  // Verify the job has not run
    
        System.assertEquals(String.valueOf(executeTime),String.valueOf(ct.NextFireTime));           // Verify the next time the job will run
    }
    /*
    @isTest
    static void testUpdateGroupMembers() {
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
            UserRoleId = portalRole.Id,
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'test0@test.com',
            Alias = 'batman',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        Database.insert(portalAccountOwner1);
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        List<User> users = new List<User>();
        System.runAs(thisUser) {
            //Test.startTest();
            Utils.isTest = true;
                    accountTriggerHandler.enforceCountry = false;
            //Create accounts
            Account a1 = new Account(
                Name = 'TestAccount',Edge_Authorization_Status__c = 'Platinum',
                OwnerId = portalAccountOwner1.Id
            );
            Account a2 = new Account(
                Name = 'TestAccount',Edge_Authorization_Status__c = 'Gold',
                OwnerId = portalAccountOwner1.Id
            );
            Account a3 = new Account(
                Name = 'TestAccount',Edge_Authorization_Status__c = 'Silver',
                OwnerId = portalAccountOwner1.Id
            );
            Account a4 = new Account(
                Name = 'TestAccount',Edge_Authorization_Status__c = 'Bronze',
                OwnerId = portalAccountOwner1.Id
            );
            Account a5 = new Account(
                Name = 'TestAccount',Edge_Authorization_Status__c = 'Distributor',
                OwnerId = portalAccountOwner1.Id
            );
            List<Account> accts = new List<Account>{a1, a2, a3, a4, a5};
            Database.insert(accts);
                    
            //Create contact
            Contact c1 = new Contact(
                FirstName = 'Test1',
                    Lastname = 'McTesty',
                AccountId = a1.Id,
                    Email = System.now().millisecond() + 'test1@test.com'
            );
            Contact c2 = new Contact(
                FirstName = 'Test2',
                    Lastname = 'McTesty',
                AccountId = a2.Id,
                    Email = System.now().millisecond() + 'test2@test.com'
            );
            Contact c3 = new Contact(
                FirstName = 'Test3',
                    Lastname = 'McTesty',
                AccountId = a3.Id,
                    Email = System.now().millisecond() + 'test3@test.com'
            );
            Contact c4 = new Contact(
                FirstName = 'Test4',
                    Lastname = 'McTesty',
                AccountId = a4.Id,
                    Email = System.now().millisecond() + 'test4@test.com'
            );
            Contact c5 = new Contact(
                FirstName = 'Test5',
                    Lastname = 'McTesty',
                AccountId = a5.Id,
                    Email = System.now().millisecond() + 'test5@test.com'
            );
            List<Contact> cons = new List<Contact>{c1, c2, c3, c4, c5};
            Database.insert(cons);
                    
            //Create user
            Profile portalProfile = [SELECT Id FROM Profile WHERE UserType LIKE '%Partner%' Limit 1];
            User u1 = new User(
                Username = 'BatchTest1@test.com',
                ContactId = c1.Id,
                ProfileId = portalProfile.Id,
                Alias = 'BTest1',
                Email = 'BatchTest1@test.com',
                EmailEncodingKey = 'UTF-8',
                LastName = 'McTesty',
                CommunityNickname = 'BatchTest1',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US'
            );
            User u2 = new User(
                Username = 'BatchTest2@test.com',
                ContactId = c2.Id,
                ProfileId = portalProfile.Id,
                Alias = 'BTest2',
                Email = 'BatchTest2@test.com',
                EmailEncodingKey = 'UTF-8',
                LastName = 'McTesty',
                CommunityNickname = 'BatchTest2',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US'
            );
            User u3 = new User(
                Username = 'BatchTest3@test.com',
                ContactId = c3.Id,
                ProfileId = portalProfile.Id,
                Alias = 'BTest3',
                Email = 'BatchTest3@test.com',
                EmailEncodingKey = 'UTF-8',
                LastName = 'McTesty',
                CommunityNickname = 'BatchTest3',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US'
            );
            User u4 = new User(
                Username = 'BatchTest4@test.com',
                ContactId = c4.Id,
                ProfileId = portalProfile.Id,
                Alias = 'BTest4',
                Email = 'BatchTest4@test.com',
                EmailEncodingKey = 'UTF-8',
                LastName = 'McTesty',
                CommunityNickname = 'BatchTest4',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US'
            );
            User u5 = new User(
                Username = 'BatchTest5@test.com',
                ContactId = c5.Id,
                ProfileId = portalProfile.Id,
                Alias = 'BTest5',
                Email = 'BatchTest5@test.com',
                EmailEncodingKey = 'UTF-8',
                LastName = 'McTesty',
                CommunityNickname = 'BatchTest5',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US'
            );
            users = new List<User>{u1, u2, u3, u4, u5};
            
        }
        Test.StartTest();
        Database.insert(users);
        database.executeBatch(new baUpdateGroupMembers(),2000);
        Test.StopTest();
        List<GroupMember> gms =[SELECT Group.Name from GroupMember WHERE UserOrGroupId IN :users ORDER BY UserOrGroupId];
        System.assertEquals(gms.size(), 10);
        System.assert(gms[0].Group.Name.contains('Platinum') );
        System.assert(gms[1].Group.Name.contains('Gold') );
        System.assert(gms[2].Group.Name.contains('Silver') );
    }
    */
    @isTest
    static void testContactUpdate() {
        Utils.isTest = true;
        accountTriggerHandler.enforceCountry = false;
        Account a = new Account(Name='Test1 Inc.', BillingState='CA', BillingCountry='US');         // Create a test account
        insert a;
        Contact c =                                                                                 // Create a test contact
            new Contact(lastname='test', accountid=a.id, email='test@test.com', MailingCountry='US', MailingState='CA');
        insert c;
        Lead l = new Lead(Company='Test Inc.', phone='1-211-211-1234', 
                fax='1-211-211-1234', FirstName='Bob', LastName='Smith', state='CA', 
                country='USA', Industry='Government', email='test@test.com.nospam', 
                HasOptedOutOfEmail = false);
        insert l;
        
        MarketingQuickstartSettings__c mqs1 = new MarketingQuickstartSettings__c (Name = 'ContactSaveActivity', Enabled__c = TRUE);
        MarketingQuickstartSettings__c mqs2 = new MarketingQuickstartSettings__c (Name = 'LeadSaveActivity', Enabled__c = TRUE);
        List<MarketingQuickstartSettings__c> mqsList = new List<MarketingQuickstartSettings__c>{mqs1, mqs2};
        insert mqsList;
        
        Task t1 = new Task(whoid = c.id, subject = 'test');
        Task t2 = new Task(whoid = l.id, subject = 'test2');
        List<Task> tasks = new List<Task>{t1, t2};  
        insert tasks;
        
        List<SObject> contactsAndLeads = new List<SObject>{c, l};
        Datetime executeTime = (System.now()).addMinutes(1);                                        // schedule the job to run in 1 minute
        String cronExpression = Utils.GetCRONExpression(executeTime);
            
        Test.StartTest();
            
            ScheduleContactUpdate scheduledJob = new ScheduleContactUpdate(contactsAndLeads);           // Instantiate a new Scheduled Apex class
            String jobId = System.schedule('ScheduleContactUpdate ' + executeTime.getTime(),cronExpression,scheduledJob);
            
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
                                NextFireTime
                                FROM CronTrigger WHERE id = :jobId];

        Test.StopTest();
        System.assertEquals(cronExpression, ct.CronExpression);                                     // Verify the expressions are the same
    
        System.assertEquals(0, ct.TimesTriggered);                                                  // Verify the job has not run
    
        System.assertEquals(String.valueOf(executeTime),String.valueOf(ct.NextFireTime));           // Verify the next time the job will run
    }
    
    /*
    @isTest
    static void testCampaignUpdate() {
        List<Opportunity> opps = [SELECT Id, CampaignId FROM Opportunity WHERE Name LIKE 'TestOpp%' LIMIT 1];
        List<Campaign> cmpgns = [SELECT Id FROM Campaign WHERE Name LIKE 'Test%' LIMIT 1];
        opps[0].CampaignId  = cmpgns[0].id;
        update opps;

        Datetime executeTime = (System.now()).addMinutes(1);                                        // schedule the job to run in 1 minute
        String cronExpression = Utils.GetCRONExpression(executeTime);
            
        Test.StartTest();
            
            ScheduleUpdateCampaign scheduledJob = new ScheduleUpdateCampaign();         // Instantiate a new Scheduled Apex class
            String jobId = System.schedule('ScheduleContactUpdate ' + executeTime.getTime(),cronExpression,scheduledJob);
            
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
                                NextFireTime
                                FROM CronTrigger WHERE id = :jobId];

        Test.StopTest();
        System.assertEquals(cronExpression, ct.CronExpression);                                     // Verify the expressions are the same
    
        System.assertEquals(0, ct.TimesTriggered);                                                  // Verify the job has not run
    
        System.assertEquals(String.valueOf(executeTime),String.valueOf(ct.NextFireTime));           // Verify the next time the job will run
    }
    */
    
    @isTest
    static void testAccountEscalationEmail() 
    {
        Utils.isTest = true;
        
        User u = ([SELECT Id FROM User WHERE IsActive=TRUE AND Profile.Name ='Tenable - Support Engineer' LIMIT 1]);
        Account a = new Account(Name='Account Test', BillingState='CA', BillingCountry='United States', Reason_Escalated__c = 'Dissatisfied Customer', 
                        Escalated__c = true, Escalated_By__c = u.Id, Escalation_Status__c = 'Red', Escalation_Notes__c = 'Test notes', Sales_Area__c = 'AMER' ); 
        insert a;
        
        AccountEscalationEmails__c accemails = new AccountEscalationEmails__c(Name='Testing',Email_Address__c = 'test@test.com');
        insert accemails;
        
        test.startTest();
            ContentVersion cv = new ContentVersion(Title='Escalated Accounts',ContentURL='<a target="_blank" href="http://www.google.com/');
            insert cv;
            ScheduleAccountEscalationEmail batch = new ScheduleAccountEscalationEmail();
            Id BatchProcess = Database.executeBatch(batch);
        test.stopTest();
        /*User u = ([SELECT Id FROM User WHERE IsActive=TRUE AND Profile.Name ='Tenable - Support Engineer' LIMIT 1]);
        Account a = new Account(Name='Account Test', BillingState='CA', BillingCountry='United States', Reason_Escalated__c = 'Dissatisfied Customer', 
                        Escalated__c = true, Escalated_By__c = u.Id, Escalation_Status__c = 'Red', Escalation_Notes__c = 'Test notes', Sales_Area__c = 'AMER' ); 
        insert a;

        Datetime executeTime = (System.now()).addMinutes(1);                                        // schedule the job to run in 1 minute
        String cronExpression = Utils.GetCRONExpression(executeTime);
            
        Test.StartTest();
            
            ScheduleAccountEscalationEmail scheduledJob = new ScheduleAccountEscalationEmail();         // Instantiate a new Scheduled Apex class
            String jobId = System.schedule('ScheduleAccountEscalationEmail ' + executeTime.getTime(),cronExpression,scheduledJob);
            
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
                                NextFireTime
                                FROM CronTrigger WHERE id = :jobId];

        Test.StopTest();
        System.assertEquals(cronExpression, ct.CronExpression);                                     // Verify the expressions are the same
    
        System.assertEquals(0, ct.TimesTriggered);                                                  // Verify the job has not run
    
        System.assertEquals(String.valueOf(executeTime),String.valueOf(ct.NextFireTime));           // Verify the next time the job will run
        */
    }
    
    
    @isTest
    static void testbaUpdateAny()
    {
        Utils.isTest = true;
            accountTriggerHandler.enforceCountry = false;
        List<Account> cs = new List<Account>();
        for (integer i=0; i < 10; i++)
        {
            Account c = new Account(Name='q'+i, Familiar_Name__c='X'+ i, BillingState='CA', BillingCountry='US');
            cs.add(c);   
        }
        insert cs;
        Database.BatchableContext bc;
        baUpdateAnyField ttc = new baUpdateAnyField('Select Id,Name from Account where Familiar_Name__c like \'X%\' LIMIT 10',
                                                  'Name', 'zz');
        //ID BatchprocessId = Database.executeBatch(ttc);
         ttc.execute(bc, cs);
         ttc.sendEmail(1, 0, 'cfovel@tenable.com', 'Complete');
        
         ttc = new baUpdateAnyField('Select Id,Name from Account where Familiar_Name__c like \'X%\' LIMIT 10',
                                                  'Name', '#Familiar_Name__c');
         ttc.execute(bc, cs);
         ttc.sendEmail(1, 0, 'cfovel@tenable.com', 'Complete');
         
         cs.Clear();
         cs = [Select Id,Name, Familiar_Name__c from Account where Familiar_Name__c like 'X%'];
         for (Account a : cs)
         {
            system.AssertEquals(a.Name, a.Familiar_Name__c);
         }
         // just poke
         ttc = new baUpdateAnyField('Select Id,Name from Account where Familiar_Name__c like \'X%\' LIMIT 10',
                                                  '', '');
         ttc.execute(bc, cs);
         ttc.sendEmail(1, 0, 'cfovel@tenable.com', 'Complete');  
         
         ttc = new baUpdateAnyField('Select Id,Name from Account where Familiar_Name__c like \'X%\' LIMIT 10',
                                                  'Familiar_Name__c', 'null');
         ttc.execute(bc, cs);
         ttc.sendEmail(1, 0, 'cfovel@tenable.com', 'Complete');
         
         cs.Clear();
         cs = [Select Id,Name, Familiar_Name__c from Account where Name like 'X%'];
         for (Account a : cs)
         {
            system.AssertEquals(a.Familiar_Name__c, null);
         }
    }

    @isTest
    static void testCalcAccountServiceLevel() {
    	Utils.isTest = true;
        List<Opportunity> opps = [SELECT Id, StageName FROM Opportunity WHERE Name LIKE 'TestOpp%' LIMIT 10];
        for (Opportunity opp : opps)
        {
        	opp.StageName = 'Closed - Won';
        }
        update opps;

        Datetime executeTime = (System.now()).addMinutes(1);                                        // schedule the job to run in 1 minute
        String cronExpression = Utils.GetCRONExpression(executeTime);
            
        Test.StartTest();
            database.executeBatch(new baCalcAccountServiceLevel('Start_Date__c <= TODAY AND End_Date__c >= TODAY'),200);
            
            ScheduleCalcAccountServiceLevel scheduledJob = new ScheduleCalcAccountServiceLevel();         // Instantiate a new Scheduled Apex class
            String jobId = System.schedule('ScheduleCalcAccountServiceLevel ' + executeTime.getTime(),cronExpression,scheduledJob);
            
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
                                NextFireTime
                                FROM CronTrigger WHERE id = :jobId];

        Test.StopTest();
        System.assertEquals(cronExpression, ct.CronExpression);                                     // Verify the expressions are the same
    
        System.assertEquals(0, ct.TimesTriggered);                                                  // Verify the job has not run
    
        System.assertEquals(String.valueOf(executeTime),String.valueOf(ct.NextFireTime));           // Verify the next time the job will run
    }
    
    @isTest
    static void testUpdateActivityCounters() {
		Utils.isTest = true;
		List<Contact> contacts = new List<Contact>();
		List<Lead> leads = new List<Lead>();
		User u = 
    		([SELECT ContactId, Contact.AccountId FROM User 
    		WHERE IsActive=TRUE 
    		AND Profile.Name ='Tenable - ISR User' limit 1]);
 
        Contact c = new Contact();
        c.FirstName = 'Joe';
        c.LastName = 'Smith';
        c.Email = 'jsmith@testTenable.com';
        c.MM_Most_Recent_Campaign_Source_Date__c = Date.newInstance(Date.today().year(),1,1);
        contacts.add(c);
        
        Contact c1 = new Contact();
        c1.FirstName = 'Mary';
        c1.LastName = 'Jones';
        c1.Email = 'mjones@testTenable.com';
        c1.MM_Most_Recent_Campaign_Source_Date__c = Date.newInstance(Date.today().year(),1,1);
        contacts.add(c1);
        
        insert contacts;

        Lead l = new Lead();
        l.LeadSource = 'Other';
        l.FirstName = 'Joe';
        l.LastName = 'Smith';
        l.Industry = 'Other';
        l.Status = 'New Lead';
        l.Company = 'Test Co';
        l.MM_Most_Recent_Campaign_Source_Date__c = Date.newInstance(Date.today().year(),1,1);
        leads.add(l);
        
        Lead l1 = new Lead();
        l1.LeadSource = 'Other';
        l1.FirstName = 'Mary';
        l1.LastName = 'Jones';
        l1.Industry = 'Other';
        l1.Status = 'New Lead';
        l1.Company = 'Test Co';
        l1.MM_Most_Recent_Campaign_Source_Date__c = Date.newInstance(Date.today().year(),1,1);
        leads.add(l1);
        
        insert leads;
 
        Task[] tList = new list<task>();
        Event[] eList = new list<event>();
        for(Integer i=0; i<4; i++) {
            Task t = new Task();
            t.Status = 'Not Started';
            t.Priority = 'Normal';
            t.Type = 'Scheduled Call Back';
            if(i==0) t.WhoId = contacts[0].Id;
            if(i==1) t.WhoId = leads[0].Id;
            if(i==2) t.WhoId = contacts[0].Id;
            if(i==3) t.WhoId = leads[0].Id;
            tList.add(t);
 
            Event e = new Event();
            e.StartDateTime = DateTime.now() + 7;
            e.EndDateTime = DateTime.now() + 14;
            if(i==0) e.WhoId = contacts[1].Id;
            if(i==1) e.WhoId = leads[1].Id;
            if(i==2) e.WhoId = contacts[1].Id;
            if(i==3) e.WhoId = leads[1].Id;
            eList.add(e);
        }
        System.runAs(u) {
	        insert tList;
	        insert eList;
        
        Test.StartTest();
        database.executeBatch(new baUpdateActivityCounters('Contact'),200); 
        database.executeBatch(new baUpdateActivityCounters('Lead'),200); 
        Test.StopTest();
        system.assertEquals(2, [SELECT Activity_Counter_Open__c FROM Contact WHERE Id = :c.Id].Activity_Counter_Open__c);
        system.assertEquals(2, [SELECT Activity_Counter_Open__c FROM Lead WHERE Id = :l.Id].Activity_Counter_Open__c);
        }

    }
    /*
    @isTest
    static void testUpdateTerritoryAssignment() {
        Utils.isTest = true;
        accountTriggerHandler.enforceCountry = false;
        Account parentAcct = new Account(Name='Test TA1 parent', BillingState='MI', BillingCountry='US', BillingPostalCode = '48211', Sales_Area__c = 'AMER', website='http://www.test1TA.com', NumberOfEmployees = 3000);
        insert parentAcct;
        List<TerritoryAssignmentRules__c> taList = new List<TerritoryAssignmentRules__c>();
        taList.add(new TerritoryAssignmentRules__c(Name = 'US_Enterprise_48183_48980', Start_Date__c = Date.today(), Country_Code__c = 'US - United States', Market_Segment__c = 'Enterprise', Territory__c = 'AMER : ENT : CENTRAL : NORTH CENTRAL : 4'));
        taList.add(new TerritoryAssignmentRules__c(Name = 'GB_Enterprise', Start_Date__c = Date.today(), Country_Code__c = 'GB - United Kingdom', Market_Segment__c = 'Enterprise', Territory__c = 'EMEA : ENT : NORTH EUROPE : UK : PUBLIC'));
        taList.add(new TerritoryAssignmentRules__c(Name = 'AE_Enterprise', Start_Date__c = Date.today(), Country_Code__c = 'AE - United Arab Emirates', Market_Segment__c = 'Enterprise', Territory__c = 'EMEA : ENT : MIDDLE EAST : 1'));
        taList.add(new TerritoryAssignmentRules__c(Name = 'JP_Emerging', Start_Date__c = Date.today(), Country_Code__c = 'JP - Japan', Market_Segment__c = 'Emerging', Territory__c = 'APAC : EMERGING : JAPAN'));
        insert taList;
        
        List<Account> acctList = new List<Account>();
        acctList.add(new Account(Name='Test TA1 Inc.', Site='Branch', ParentId=parentAcct.Id, BillingState='MI', BillingCountry='US', BillingPostalCode = '48211', Sales_Area__c = 'AMER', website='http://www.test1TA.com', NumberOfEmployees = 3000));
        acctList.add(new Account(Name='Test TA2 Inc.', BillingState='ABD', BillingCountry='GB', Sales_Area__c = 'EMEA', website='http://www.test2TA.com', NumberOfEmployees = 3000));
        acctList.add(new Account(Name='Test TA3 Inc.', BillingCountry='AE', BillingCity = 'Abu Dhabi', Sales_Area__c = 'EMEA', website='http://www.test3TA.com', NumberOfEmployees = 3000));
        acctList.add(new Account(Name='Test TA4 Inc.', BillingCountry='JP', Sales_Area__c = 'APAC', website='http://www.test4TA.com', NumberOfEmployees = 3000));
        insert acctList;
        
        Test.StartTest();
        database.executeBatch(new baUpdateTerritoryAssignments(new List<String>{'APAC', 'AMER', 'EMEA'}),200);
        Test.StopTest();
        Account a =[SELECT Territory__c from Account WHERE Name = 'Test TA4 Inc.' limit 1];
        System.assertEquals(a.Territory__c, 'APAC : EMERGING : JAPAN');
    }
    */
}