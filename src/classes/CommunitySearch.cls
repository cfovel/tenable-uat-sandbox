public with sharing class CommunitySearch {

    @AuraEnabled
    public static string getCommunityId() {
        return getSalesforceSiteUrl();
    }
    
    @AuraEnabled
    public static string getToken() {
        return CoveoV2.Globals.generateSearchToken(new Map<String, Object> {
            'filter' => String.join(getFilter(), ' AND ')
        });
    }
    
    public static List<String> getFilter() {
        // TODO: add filters in custom settings!
        
        List<String> filter = new List<String>();
        Id profileId=userinfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;


        String tenableUFilter = '(NOT @commontype=video OR (@coursestatus=2 AND @courseisvisibleinpkb=True))';
        if (profileName == 'Tenable - Customer Community Standard User' || profileName =='Tenable - Customer Community Super User') {
            tenableUFilter = '(NOT @commontype=video OR (@coursestatus=2 AND (@courseisvisibleinpkb=True OR @courseisvisibleincsp=True)))';
        }
        filter.add(tenableUFilter);
        
        if (profileName != 'Tenable - Customer Community Super User') {
            String caseFilter = '(NOT @objecttype==(Case,CaseComment)) ';
            filter.add(caseFilter);
        }

        String visibleKbFilter = '(NOT @sfkbid OR @sfisvisibleinpkb==True)';
        filter.add(visibleKbFilter);
        
        String removingAttachmentFilter = '(NOT @isattachment)';
        filter.add(removingAttachmentFilter);

        String removingInternalSources = '(NOT @source==("Confluence Eng","JIRA"))';
        filter.add(removingInternalSources);
        
        return filter;
    }
    

    /**
     * Returns the full site URL.
     * (Ex. https://hw-developer-edition.na3.force.com/myCommunitu)
     *
     * @return  string   The Site URL.
     */
    public static string getSalesforceSiteUrl() {
        // Will be null if we are not in a community
        String siteId = Site.getSiteId();

        // Since we can't trust Salesforce APIs to be reliable, lets query
        // the database directly to get the community URL.
        // We found that in SDO organization, the "getBaseSecureUrl()" method
        // doesn't respect the specs and also that the output can vary depending
        // on if we call it inside the "builder" vs the published community.
        // Ref. https://coveord.atlassian.net/browse/SFINT-788
        //      https://coveord.atlassian.net/browse/SFINT-791
        
        try {
            system.debug('Fetching Salesforce Site Url from Database');

            SObject ds = getDomainSiteObject(siteId);
            return 'https://' + ds.getSObject('Domain').get('Domain') + ds.get('PathPrefix');
        } catch(Exception ex) {
            system.debug('Fetching failed, using Salesforce methods');

            // In case something went wrong, we default on
            // Salesforce APIs and hope for the best.
            String baseUrl = Site.getBaseSecureUrl();
            String pathPrefix = Site.getPathPrefix();

            // We know that in SDO orgs, the pathPrefix
            // isn't always included.
            if(!baseUrl.endsWith(pathPrefix)) {
                baseUrl = baseUrl + pathPrefix;
            }

            return baseUrl;
        }
    }

    private static SObject getDomainSiteObject(String siteId) {
        // Because the Domain and DomainSite objects are only available
        // when Force.com sites are enabled, we can't make "hard" references to them.
        // We also need to filter out custom DNS entries (where LIKE '%.force.com') because
        // Salesforce doesn't allow JWT authentication otherwise.
        return Database.query('SELECT Domain.Domain, PathPrefix FROM DomainSite WHERE SiteId = :siteId AND Domain.Domain LIKE \'%.force.com\' LIMIT 1');
    }
}