public class Tenable_OnboardingWrapper {
    @auraEnabled
    public String name {get; set;}
    @auraEnabled
    public String id {get; set;}
    @auraEnabled
    public Boolean following {get; set;}
    @auraEnabled
    public String notificationFrequency {get; set;}

    public Tenable_OnboardingWrapper(String name, String id, Boolean following){ 
        this.name = name;
        this.id = id;
        this.following = following;
    }

}