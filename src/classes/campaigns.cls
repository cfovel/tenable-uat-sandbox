global class campaigns {
/******************************************************************************
*
*   Custom Controller for Adding Spreadsheet for Expenses to Campaigns
*   Copyright 2006-2014 (c) by CloudLogistix.com
*   All Rights Reserved, Modifications can only be made for your direct use and 
*   not for distribution without explicit permission from CloudLogistix.
*
********************************************************************************/
    /*****************************************************************************************
    *   Variable Initialization
    ******************************************************************************************/
    public Id pageid = ApexPages.currentPage().getParameters().get('id');       // Store Campaign ID
    private final Campaign entry;                                               // Store Campaign Record
    public List<Campaign_Expenses__c> entryList;                                // Store Campaign Expenses
    public List<CampaignMember> memberLeadList, memberContactList;              // Store related Leads and Contacts
    private final ApexPages.standardController stdController;                   // Set-up Controller Class

    /*****************************************************************************************
    *   Custom Controller Initialization
    ******************************************************************************************/
    public campaigns(ApexPages.StandardController stdController) {              // Controller Set-up Method
        Campaign entry = (Campaign)stdController.getRecord();                   // First get record from Page Call
        this.stdController = stdController;                                     // Set-up Controller
    } 

    /*****************************************************************************************
    *   Dynamic Query Routines
    ******************************************************************************************/
    public PageReference reset() {                                              // Page function to get related Campaign Expenses
        String querystring;
        querystring = 'SELECT Del__c, Cost__c, Expense_Type__c, Date__c,';
        querystring += 'Vendor__c, Description__c, Mileage__c, Payee__c, Campaign__c';
        querystring += ' FROM Campaign_Expenses__c';
        querystring += ' WHERE Campaign__c = \''+pageid;
        querystring += '\' AND IsDeleted = FALSE ORDER BY Date__c ASC';
        entryList = Database.query(querystring);                 
        return null;
    }  
    public PageReference resetMembers() {                                       // Page function to get related Campaign Members
        String querystring;
        querystring = 'SELECT Status, LeadId, Id, HasResponded, CreatedDate, ';
        querystring += 'FirstRespondedDate, ContactId, CampaignId,';
        querystring += 'Lead.OwnerId, Lead.Company, Lead.Title, Lead.FirstName, ';
        querystring += 'Lead.LastName, Lead.Email, Lead.Phone,';
        querystring += 'Lead.Owner.Name';
        querystring += ' FROM CampaignMember';
        querystring += ' WHERE CampaignId = \''+pageid;
        querystring += '\' AND LeadId != null AND Lead.IsConverted = FALSE AND ';
        querystring += 'IsDeleted = FALSE ORDER BY CreatedDate ASC LIMIT 100';
        memberLeadList = Database.query(querystring);                           // Store related Leads
        querystring = 'SELECT Status, LeadId, Id, HasResponded, CreatedDate, ';
        querystring += 'FirstRespondedDate, ContactId, CampaignId, ';
        querystring += 'Contact.Account.Name, Contact.FirstName, Contact.LastName, ';
        querystring += 'Contact.Title, Contact.Email, Contact.Phone, Contact.Owner.Name';
        querystring += ' FROM CampaignMember';
        querystring += ' WHERE CampaignId = \''+pageid;
        querystring += '\' AND ContactId != null AND IsDeleted = FALSE ORDER BY CreatedDate ASC LIMIT 100';
        memberContactList = Database.query(querystring);                        // Store related Contacts
        return null;
    }

    /*****************************************************************************************
    *   Getter/Setter's to return values to VF Page(s)
    ******************************************************************************************/        
    public List<Campaign_Expenses__c> getEntries() {                            // Called by VF Page to get related expenses
        if (entryList == null) {reset();}                                       // ... if it's empty, go call query
        return entryList;                                                       // ... then return to page
    }
    public void setEntries(List<Campaign_Expenses__c> Entries) {entryList = Entries;}
    public List<CampaignMember> getLeadMembers() {                              // Called by VF Page to get any related Lead
        if (memberLeadList == null) {resetMembers();}                           // ... to this campaign.  If it's empty, go
        return memberLeadList;                                                  // ... call query
    }
    public List<CampaignMember> getContactMembers() {                           // Called by VF Page to get any related Contacts
        if (memberContactList == null) {resetMembers();}                        // ... to this campaign.  If it's empty, go
        return memberContactList;                                               // ... call query
    }

    /*****************************************************************************************
    *   Save records for spreadsheet routines
    ******************************************************************************************/        
    public PageReference save() {
        Campaign_Expenses__c[] entriestoUpdate = new Campaign_Expenses__c[0];   // Store any expenses to be updated
        Campaign_Expenses__c[] entriestoDelete = new Campaign_Expenses__c[0];   // Store any expenses to be deleted
        
        try {
            for (Campaign_Expenses__c ce : entryList) {                         // Loop thru all related expense records
                if (ce.del__c) {entriestoDelete.add(ce);}                       // If user checked the field del__c then mark for deletion
                else {entriestoUpdate.add(ce);}                                 // ... else, add it to the list of records to update
            }
            if (entriestoDelete.size() > 0) {delete entriestoDelete;}           // Delete any entries flagged for deletion
            if (entriestoUpdate.size() > 0) {upsert entriestoUpdate;}           // Update any other records
            reset();                                                            // Cause all records to reload so that we have the latest copy
            
            PageReference newPage = new PageReference('/' + pageid);            // Cause the VF page to refresh by returning the campaign Id
            newPage.setRedirect(true);                                          // ... cause the page to refresh
            return newPage;                                                     // ... return back to VF page
        }
        catch (exception e) {return null;}                                      // If there are any issues, don't refresh so we can see error
    }

    /*****************************************************************************************
    *   Add record to spreadsheet routines
    ******************************************************************************************/
    public PageReference add() {
        entryList.add(New Campaign_Expenses__c(Campaign__c = pageid));          // Create a new blank expense record
        return null;                                                            // Don't let the page refresh just to add a record
    }

    Webservice static string clonewithmembers(String campaignid) {
        Campaign entry = 
            ([SELECT Website_URL__c, Type, Targets__c, Status, StartDate, 
                    ParentId, OwnerId, Name, Event_Type__c, Number_Of_Target_Leads__c,
                    Sponsorship_Type__c, Synopsis__c, Campaign_SubType__c,
                    IsActive, Ignore_Naming__c, ExpectedRevenue, Exclude_from_Metrics__c, 
                    ExpectedResponse, Event_Title__c, Event_State_Province__c, 
                    Event_Location__c, Event_Focus_Country__c, EndDate, 
                    Description, Campaign_Focus__c, BudgetedCost, ActualCost,
                    Sales_Region__c, Sales_Area__c
                    FROM Campaign
                    WHERE Id = :campaignid]);
        Campaign newentry = entry.clone();
        insert newentry;
        CampaignMember[] members = 
            ([SELECT Status, LeadId, Id, ContactId
                    FROM CampaignMember 
                    WHERE CampaignId = :campaignid]);
        CampaignMember[] memberstoAdd = new CampaignMember[0];
        for (CampaignMember cm : members) {
            CampaignMember cmemb = cm.clone();
            cmemb.CampaignId = newentry.id;
            memberstoAdd.add(cmemb);
        }
        if (memberstoAdd.size() > 0) {insert memberstoAdd;}
        return newentry.id;                                                     // ... return back to VF page
    }
    
    Webservice static string createcalendarevent(String campaignid) {
        Campaign entry = 
            ([SELECT Id, StartDate, EndDate, Name, Synopsis__c
                    FROM Campaign
                    WHERE Id = :campaignid]);
        String marketingcalid = utils.marketingappsettings().get('MarketingCalendarId').UserIds__c;
        Event[] existing = ([SELECT Id FROM Event WHERE WhatId = :campaignid]);

        if (existing.size() == 0) {
            if ((entry.Name != null) &&
                (entry.StartDate != null) &&
                (entry.EndDate != null)) {
                Event ev = new Event();
                ev.OwnerId = marketingcalid;
                ev.Subject = 'Marketing Event';
                ev.WhatId = entry.id;
                ev.IsAllDayEvent = true;
                ev.StartDateTime = entry.StartDate;
                ev.EndDateTime = entry.EndDate; 
                ev.Description = entry.Synopsis__c;
        
                try {
                    insert ev;
                    return 'Successfully created campaign';
                }
                catch (exception e) {
                    return e.getMessage();
                }
            }
            else {
                return 'Missing or bad campaign name, start or end dates';
            }
        }
        else {
            return 'Calendar event already exists';
        }
    }

    Webservice static string createcalendaractivity(String subject, DateTime sdate, String recordid) {

        String marketingcalid = utils.marketingappsettings().get('MarketingCalendarId').UserIds__c;

        Event[] existing = ([SELECT Id FROM Event WHERE WhatId = :recordid]);

        if (existing.size() == 0) {
            Event ev = new Event();
            ev.OwnerId = marketingcalid;
            ev.Subject = subject;
            ev.WhatId = recordid;
            ev.IsAllDayEvent = true;
            ev.StartDateTime = sdate;
            ev.EndDateTime = sdate; 
        
            try {
                insert ev;
                return 'Successfully created event';
            }
            catch (exception e) {
                return e.getMessage();
            }
        }
        else {
            return 'Calendar event already exists';
        }
    }
}