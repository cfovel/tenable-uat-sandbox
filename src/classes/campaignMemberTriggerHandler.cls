public with sharing class campaignMemberTriggerHandler {
/*******************************************************************************
*
* Campaign Trigger Handler Class
* Version 1.0a
* Copyright 2006-2014 (c) by CloudLogistix.com
* All Rights Reserved
* Your are allowed to make modifications only for yourself, not for distribution
* without permission from CloudLogistix.
*
*******************************************************************************/
	public class CampaignMemberBeforeInsertHandler implements Triggers.Handler {
		public void handle() {
			ProcessRecords(Trigger.new);
		}
	} 
	
	public class CampaignMemberBeforeUpdateHandler implements Triggers.Handler {
		public void handle() {
			ProcessRecords(Trigger.new);
		}
	} 

	public class CampaignMemberAfterInsertHandler implements Triggers.Handler {
		public void handle() {
			ProcessMetrics(Trigger.new);
		}
	} 
	
	public class CampaignMemberAfterUpdateHandler implements Triggers.Handler {
		public void handle() {
			ProcessMetrics(Trigger.new);
		}
	} 

	public class CampaignMemberAfterDeleteHandler implements Triggers.Handler {
		public void handle() {
			ProcessMetrics(Trigger.old);
		}
	} 

    public static void ProcessRecords(CampaignMember[] cmemb) {
		/*****************************************************************************************
		*	Record Pre-Processing
		******************************************************************************************/
		Set <Id> leadids = new Set <Id> ();
		
		for (CampaignMember cm : cmemb) {															// Loop thru all records being processed
			if (cm.leadid != null) {
				leadids.add(cm.leadid);
			} 
		}

		Map <Id, Lead> Leads = new Map <Id, Lead> ([SELECT Id, CreatedDate FROM Lead where id in: leadids]);
		
		/*****************************************************************************************
		*	Record Processing
		******************************************************************************************/
		for (CampaignMember cm : cmemb) {															// Loop thru all records being processed
			if ((cm.Net_New_Lead__c) ||
				(Leads != null) && 
				(Leads.get(cm.leadid) != null) &&
				((Date.valueof(Leads.get(cm.leadid).createddate).daysBetween(System.today())) < 1.0)) {
				cm.isNetNew__c = TRUE;}
		}
    }

    public static void ProcessMetrics(CampaignMember[] cmemb) {
		/*****************************************************************************************
		*	Variable Initialization
		******************************************************************************************/
		Lead[] ldstoupdate = new Lead[0];															// Store any leads we're going to udpate
		Contact[] cttoupdate = new Contact[0];														// Store any contacts we're going to update
		Set <Id> leadids = new Set <Id>();
		Set <Id> contactids = new Set <Id>();

		/*****************************************************************************************
		*	Record Pre-Processing
		******************************************************************************************/
		for (CampaignMember cm : cmemb) {															// Loop thru all records being processed
			if (cm.leadid != null) {																// Are we a lead, add to our list
				if (leadids.contains(cm.leadid) == null) {
					ldstoupdate.add(new Lead(id=cm.leadid));										// Add it to our list to be updated
				}
				leadids.add(cm.leadid);
			}
			if (cm.contactid != null) {																// Are we a contact, add to our list
				if (contactids.contains(cm.contactid) == null) {
					cttoupdate.add(new Contact(id=cm.contactid));									// Add it to our list to be udpated
				}
				contactids.add(cm.contactid);
			}
		}
		/*****************************************************************************************
		*	Record Post-Processing
		******************************************************************************************/
		if (ldstoupdate.size() > 0) {update ldstoupdate;}											// Force an update on leads
		if (cttoupdate.size() > 0) {update cttoupdate;}												// Force an update on contacts
    }

}