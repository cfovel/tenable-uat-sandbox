global without sharing class Tenable_SixMonthAlertBatch implements Database.Batchable<sObject>  {


    global Database.QueryLocator start(Database.BatchableContext BC) {
        Integer daysFromLogin = 180;
        String query;
        if (Test.isRunningTest()) {
            query = 'SELECT Id, Days_Since_Initial_Login__c, Display_6_Month_Modal__c FROM User WHERE isActive = true AND Contact.Customer_Community_User__c = True AND Days_Since_Initial_Login__c = :daysFromLogin limit 10';
        } else {
            query = 'SELECT Id, Days_Since_Initial_Login__c, Display_6_Month_Modal__c FROM User WHERE isActive = true AND Contact.Customer_Community_User__c = True AND Days_Since_Initial_Login__c = :daysFromLogin';
        }

            return Database.getQueryLocator(query);
    }


    global void execute(Database.BatchableContext BC, List <User> scope) {
        String message = [
                Select Six_Month_Message__c
                FROM Community_Chatter_Messages__mdt
        ].Six_Month_Message__c;
        if (message != null) {

            Id networkId = [
                    SELECT ID
                    FROM Network
                    WHERE Name = 'Tenable Community'
            ].Id;
            list <FeedItem> posts = new list <FeedItem>();
            for (User us:scope) {
                FeedItem post = new FeedItem(Body = message, NetworkScope = networkId, parentId = us.Id);
                posts.add(post);
            }
            system.debug('posts: ' + posts);
            insert posts;
        }
    }

    global void finish(Database.BatchableContext BC)
        {
            system.debug('Batch complete!');
        }
          
           
       }