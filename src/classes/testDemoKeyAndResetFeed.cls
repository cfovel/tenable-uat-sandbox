@isTest
public class testDemoKeyAndResetFeed {

    static accountDemoKey testAccGenerateKey;
    
    private static Account dummyAccount(){
    	
    	Utils.isTest = true;
    	
    	Account a = new Account(Name='Dell SecureWorks', BillingState='CA', BillingCountry='US', website='http://www.test1.com');
        insert a; 
        
        return a;
    }
    
    private static Contact dummyContact(Id acctId){
    	
    	Utils.isTest = true;
    	
    	Contact c = new Contact(firstname='Con',lastname='Con', accountid=acctId, email= 'ContactTenable@test.com', MailingCountry='US', MailingState='CA');
        insert c; 
        
        return c;
    }
    
    @isTest
    static void testDemoKeys() {
    	
    	Account testAccount = dummyAccount();
    	boolean isTrue;
    	Test.startTest();
    	
    	// Set mock callout class                                                       
        Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
		testAccountDemoKeyButtons(testAccount);	
        isTrue = customerUtilityController.generateDemoKey('keyReceiver=rjones%40tenable.com&productType=is_150&hostnames=&numberOfIps=64&tioSize=&customerEmail=royfranklinjones%40gmail.com&customerName=Roy%20F%20Jones&evalLength=7&accountId=0016000001AKmTcAAL');
	    Test.stopTest();
  	}
    
    private static void testAccountDemoKeyButtons(Account acc) {
    
       	User u = 
    		([SELECT Id FROM User 
    		WHERE IsActive=TRUE 
    		AND Profile.Name ='Tenable - Support Engineer' limit 1]);
    		
    	System.runAs(u) {		
       		PageReference pageRef = Page.accountDemoKeyButton;
       		Test.setCurrentPage(pageRef);
       		ApexPages.StandardController sc = new ApexPages.standardController(acc);
       		accountDemoKey  controller = new accountDemoKey(sc);
       		System.assertNotEquals(null,controller.demoKeyLoad());
    	}	
    }
    @isTest 
    static void testResetLicenceFeed() {
    	Account acct = dummyAccount();
		User u = 
    		([SELECT Id FROM User 
    		WHERE IsActive=TRUE 
    		AND Profile.Name ='Tenable - Support Engineer' limit 1]);
    		
    	Test.startTest();
		Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
		account2installbases__x mockedProd = new account2installbases__x(Id__c = acct.Id,customerID__c = '10069',  partNumber__c = 'SERV-NES', ExternalId = '10069-164995', maintenanceCode__c = 'ZZVD-93LQ-SRDM-Z4GZ');

    	resetLicenseFeed.mockedProds.add(mockedProd);
    	
		System.runAs(u) {	
			PageReference pageRef = Page.ResetLicenseFeed;
	        Test.setCurrentPage(pageRef);
	        ApexPages.currentPage().getParameters().put('id', mockedProd.Id);
	        ApexPages.StandardController sc = new ApexPages.StandardController(mockedProd);        
	        resetLicenseFeed ext = new resetLicenseFeed(sc);
	        ext.ResetFeed();
	        ext.cancel();
		}
        Test.stopTest();
    }
    
    @isTest 
    static void testResetLicenceFeedContact() {
    	Account acct = dummyAccount();
    	Contact con = dummyContact(acct.Id);
       	User u = 
    		([SELECT Id FROM User 
    		WHERE IsActive=TRUE 
    		AND Profile.Name ='Tenable - Support Engineer' limit 1]);
    		
    	Test.startTest();
		Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
		contact2installbases__x mockedProd = new contact2installbases__x(Id__c = con.Id,customerID__c = '10069', parentID__c = '3563573', partNumber__c = 'SERV-NES', ExternalId = '10069-164995', maintenanceCode__c = 'ZZVD-93LQ-SRDM-Z4GZ');

    	resetLicenseFeedContact.mockedProds.add(mockedProd);
    	
		System.runAs(u) {	
			PageReference pageRef = Page.ResetLicenseFeedContact;
	        Test.setCurrentPage(pageRef);
	        ApexPages.currentPage().getParameters().put('id', mockedProd.Id);
	        ApexPages.StandardController sc = new ApexPages.StandardController(mockedProd);        
	        resetLicenseFeedContact ext = new resetLicenseFeedContact(sc);
	        ext.ResetFeedContact();
	        ext.cancel();
		}
        Test.stopTest();
    }
    
    @isTest 
    static void testResetHostNameChangeContact() {
    	Account acct = dummyAccount();
    	Contact con = dummyContact(acct.Id);
       	User u = 
    		([SELECT Id FROM User 
    		WHERE IsActive=TRUE 
    		AND Profile.Name ='Tenable - Support Engineer' limit 1]);
    		
    	Test.startTest();
		Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
		contact2installbases__x mockedProd = new contact2installbases__x(Id__c = con.Id, customerID__c = '10069', parentID__c = '3563573', partNumber__c = 'SERV-NES', ExternalId = '10069-164995', maintenanceCode__c = 'ZZVD-93LQ-SRDM-Z4GZ');

    	resetLicenseFeedContact.mockedProds.add(mockedProd);
    	
		System.runAs(u) {	
			PageReference pageRef = Page.ResetHostNameChangeContact;
	        Test.setCurrentPage(pageRef);
	        ApexPages.currentPage().getParameters().put('id', mockedProd.Id);
	        ApexPages.StandardController sc = new ApexPages.StandardController(mockedProd);        
	        resetLicenseFeedContact ext = new resetLicenseFeedContact(sc);
	        ext.resetHostnameChangeTimerContact();
	        ext.cancel();
		}
        Test.stopTest();
    }
    
    @isTest 
    static void testResetHostnameChangeTimer() {
    	Account acct = dummyAccount();

    	User u = 
    		([SELECT Id FROM User 
    		WHERE IsActive=TRUE 
    		AND Profile.Name ='Tenable - Support Engineer' limit 1]);
    		
    	Test.startTest();
		Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
		account2installbases__x mockedProd = new account2installbases__x(Id__c = acct.Id,customerID__c = '10069',  partNumber__c = 'SERV-NES', ExternalId = '10069-164995', maintenanceCode__c = 'ZZVD-93LQ-SRDM-Z4GZ');

    	resetHostnameChangeTimer.mockedProds.add(mockedProd);
    	
		System.runAs(u) {	
			PageReference pageRef = Page.ResetHostnameChangeTimer;
	        Test.setCurrentPage(pageRef);
	        ApexPages.currentPage().getParameters().put('id', mockedProd.Id);
	        ApexPages.StandardController sc = new ApexPages.StandardController(mockedProd);        
	        resetHostnameChangeTimer ext = new resetHostnameChangeTimer(sc);
	        ext.resetHostnameChangeTimer();
	        ext.cancel();
		}
        Test.stopTest();
    }
}