public class TenableCaseCommentController {
    
    public class CaseCommentData implements Comparable{
      private String CreatorName;
      private String AccountName;
      private DateTime CreatedDate; 
      private String CreatedByName; 
      private String CreatedByFirstName; 
      private String CreatedByLastName; 
      private String ParentContactName;
      private String ParentContactAccountName;
      private String ParentOwnerName;
      private String ParentOwnerId;
      private String CreatedById;
      private String CommentBody;
      private String ParentContactId;
      public CaseCommentData(CaseCommentDataBuilder cd){
      	this.CreatorName = cd.CreatorName;
      	this.CreatedDate = cd.CreatedDate;
      	this.CreatedByName = cd.CreatedByName;
      	this.CreatedByFirstName = cd.CreatedByFirstName;
      	this.CreatedByLastName = cd.CreatedByLastName;
      	this.ParentContactName = cd.ParentContactName;
      	this.ParentContactAccountName = cd.ParentContactAccountName;
      	this.ParentOwnerName = cd.ParentOwnerName;
      	this.ParentOwnerId = cd.ParentOwnerId;
      	this.CreatedById = cd.CreatedById;
      	this.CommentBody = cd.CommentBody;
      	this.ParentContactId = cd.ParentContactId;
      }
      public void setAdditionalInfo(Contact c){
        this.AccountName = c.Account.Name;
      }
      public Integer compareTo(Object compareTo) {
        CaseCommentData compareToComment = (CaseCommentData) compareTo;
        if (CreatedDate == compareToComment.CreatedDate) return 0;
        if (CreatedDate < compareToComment.CreatedDate) return 1;
        return -1;
      }
      @AuraEnabled
      public String getCreatorName(){
        return this.CreatorName;
      }
      @AuraEnabled
      public String getAccountName(){
        return this.AccountName;
      }
      @AuraEnabled
      public DateTime getCreatedDate(){
        return this.CreatedDate;
      }
      @AuraEnabled
      public String getCreatedByName(){
        return this.CreatedByName;
      }
      @AuraEnabled
      public String getCreatedByFirstName(){
        return this.CreatedByFirstName;
      } 
      @AuraEnabled
      public String getCreatedByLastName(){
        return this.CreatedByLastName;
      }
      @AuraEnabled
      public String getParentContactName(){
        return this.ParentContactName;
      }
      @AuraEnabled
      public String getParentContactAccountName(){
        return this.ParentContactAccountName;
      }
      @AuraEnabled
      public String getParentOwnerName(){
        return this.ParentOwnerName;
      }
      @AuraEnabled
      public String getParentOwnerId(){
        return this.ParentOwnerId;
      }
      @AuraEnabled
      public String getCreatedById(){
        return this.CreatedById;
      }
      @AuraEnabled
      public String getCommentBody(){
        return this.CommentBody;
      }
      @AuraEnabled
      public String getParentContactId(){
        return this.ParentContactId;
      }
    }
    
    public class CaseCommentDataBuilder {
      private String CreatorName;
      private DateTime CreatedDate; 
      private String CreatedByName; 
      private String CreatedByFirstName; 
      private String CreatedByLastName; 
      private String ParentContactName;
      private String ParentContactAccountName;
      private String ParentOwnerName;
      private String ParentOwnerId;
      private String CreatedById;
      private String CommentBody;
      private String ParentContactId;
      public CaseCommentDataBuilder(){
      	this.ParentContactAccountName = 'N/A';
      	this.ParentContactName = '';
      }
      public CaseCommentDataBuilder CommentData(CaseComment c){
      	CreatorName = c.CreatorName;
      	CreatedDate = c.CreatedDate;
      	CreatedByName = c.CreatedBy.Name;
      	CreatedByFirstName = c.CreatedBy.FirstName;
      	CreatedByLastName = c.CreatedBy.LastName;
      	try{
      	  ParentContactName = c.Parent.Contact.Name;
      	  system.debug(c);
      	  if(!String.isBlank(c.Parent.ContactId)){
      	    ParentContactAccountName = c.Parent.Contact.Account.Name;
      	  }
      	}
      	catch(Exception err){
      	  system.debug('@@ ==> ERROR='+err.getMessage());
      	}
      	
      	ParentOwnerName = c.Parent.Owner.Name;
      	ParentOwnerId = c.Parent.OwnerId;
      	CreatedById = c.CreatedBy.Id;
      	CommentBody = c.CommentBody;
      	ParentContactId = c.Parent.ContactId;
      	return this;
      }
      public CaseCommentData build() {
        return new CaseCommentData(this);
      }
    }
    
    public TenableCaseCommentController(){
    }
    
    @AuraEnabled
    public static Boolean isCommentAllowed(String recordId){
      Boolean result = true;
      if(String.isBlank(recordId)){
      	return false;
      }
      Case c = [SELECT CreatedDate, ClosedDate FROM Case WHERE Id = :recordId];
      // disabled comments on cases closed for more than 30 days
      if(c.ClosedDate != null && c.ClosedDate.date().daysBetween(Date.today()) > 30){
        result =  false;
      }
      return result;
    }
    
    @AuraEnabled
    public static List<CaseCommentData> getCaseComments(String recordId){
      List<CaseCommentData> caseCommentList = new List<CaseCommentData>();
      if(String.isBlank(recordId)){
        return null;
      }
      Map<Id, List<CaseCommentData>> CaseCommentDataByContact = new Map<Id, List<CaseCommentData>>();
      for(CaseComment cc: [SELECT CreatorName, CreatedDate, CreatedBy.Name, CreatedBy.FirstName, CreatedBy.LastName, Parent.Contact.Name, 
                                Parent.Contact.Account.Name, Parent.Owner.Name, Parent.OwnerId, CreatedBy.Id, CreatedById, CommentBody,
                                Parent.ContactId
                         FROM CaseComment
                         WHERE ParentId = :recordId
                         Order By CreatedDate desc]){
        CaseCommentData d = new CaseCommentDataBuilder()
          .CommentData(cc)
          .build();
        List<CaseCommentData> dList = new List<CaseCommentData>();
        if(CaseCommentDataByContact.containsKey(cc.CreatedById)){
          dList = CaseCommentDataByContact.get(cc.CreatedById);
        }
        dList.add(d);
        CaseCommentDataByContact.put(cc.CreatedById, dList);
      }
      // clean up results to keep only those users with contacts
      Set<Id> INNER_JOIN_CONTACT = new Set<Id> ();
      Map<Id, Id> INNER_JOIN_USER_CONTACT = new Map<Id, Id>();
      for(User u: [SELECT Username, ContactId FROM User WHERE Id IN :CaseCommentDataByContact.keySet()]){
      	if(String.isBlank(u.ContactId)){
         caseCommentList.addAll(CaseCommentDataByContact.get(u.Id));
         CaseCommentDataByContact.remove(u.Id);
      	}
      	else{
      	  INNER_JOIN_USER_CONTACT.put(u.ContactId, u.Id);
      	}
      }
      for(Contact c: [SELECT Account.Name FROM Contact Where Id IN :INNER_JOIN_USER_CONTACT.keySet()]){
      	for(CaseCommentData d: CaseCommentDataByContact.get(INNER_JOIN_USER_CONTACT.get(c.Id))){
          d.setAdditionalInfo(c);
      	  caseCommentList.add(d);
      	}
      }
      caseCommentList.sort();
      return caseCommentList;
    } 
    
    @AuraEnabled
    public static Boolean addCaseComments(String recordId, String CaseCommentContent){
      CaseComment CustomerCaseComment = new CaseComment();
      CustomerCaseComment.CommentBody = CaseCommentContent;
      CustomerCaseComment.ParentId = recordId;
      CustomerCaseComment.IsPublished = true;
      insert CustomerCaseComment;
      return true;
    }
}