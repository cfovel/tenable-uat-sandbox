public with sharing class TerritoryAssignmentTriggerHandler
{
    public class TerritoryAssignmentBeforeInsertHandler implements Triggers.Handler
    {
        public void handle() 
        {
            UpdateTerritoryData(Trigger.new);
        }
    } 
    
    public class TerritoryAssignmentBeforeUpdateHandler implements Triggers.Handler
    {
        public void handle() 
        {
            UpdateTerritoryData(Trigger.new);
        }
    } 
    
    public static void UpdateTerritoryData(List<TerritoryAssignment__c> newAssignments)
    {
        DateTime now = DateTime.now();
    
        Map<Id, Territory__c> territories = new Map<Id, Territory__c>([SELECT Id, Name, TerritoryOwner__c, RegionName__c, RegionOwner__c, AreaName__c, AreaOwner__c, TheaterName__c, TheaterOwner__c, SegmentName__c, JobFunction__c FROM Territory__c]);
        
        if (Trigger.isBefore) {
            
            if (Trigger.isInsert) {
                
                for (TerritoryAssignment__c territoryAssignment : newAssignments) {
                    
                    territoryAssignment.TerritoryName__c = territories.get(territoryAssignment.Territory__c).Name;
                    territoryAssignment.TerritoryOwner__c = territories.get(territoryAssignment.Territory__c).TerritoryOwner__c;
                    territoryAssignment.RegionName__c = territories.get(territoryAssignment.Territory__c).RegionName__c;
                    territoryAssignment.RegionOwner__c = territories.get(territoryAssignment.Territory__c).RegionOwner__c;
                    territoryAssignment.AreaName__c = territories.get(territoryAssignment.Territory__c).AreaName__c;
                    territoryAssignment.AreaOwner__c = territories.get(territoryAssignment.Territory__c).AreaOwner__c;
                    territoryAssignment.TheaterName__c = territories.get(territoryAssignment.Territory__c).TheaterName__c;
                    territoryAssignment.TheaterOwner__c = territories.get(territoryAssignment.Territory__c).TheaterOwner__c;
                    territoryAssignment.SegmentName__c = territories.get(territoryAssignment.Territory__c).SegmentName__c;
                    territoryAssignment.JobFunction__c = territories.get(territoryAssignment.Territory__c).JobFunction__c;
    
                    if (territoryAssignment.Preview__c) {
    
                        territoryAssignment.StartAt__c = null;
                        territoryAssignment.EndAt__c = null;
    
                    } else {
    
                        if (territoryAssignment.StartAt__c <= now.addHours(-1))
                            territoryAssignment.addError('You cannot change things that have already happened.');
                        if (territoryAssignment.EndAt__c != null && territoryAssignment.EndAt__c <= now.addHours(-1))
                            territoryAssignment.addError('You cannot change things that have already happened.');
    
                    }
                    
                }
                
            } else if (Trigger.isUpdate) {
                
                for (TerritoryAssignment__c territoryAssignment : newAssignments) {
                    
                    TerritoryAssignment__c oldAssignment = (TerritoryAssignment__c)Trigger.oldMap.get(territoryAssignment.Id);
                    
                    if (!territoryAssignment.Preview__c) {
                        
                        if (territoryAssignment.EndAt__c != null && territoryAssignment.EndAt__c < now) {
                            territoryAssignment.Territory__c = null;
                            territoryAssignment.TerritoryAssignmentRule__c = null;
                            territoryAssignment.OverrideAssignment__c = null;
                        }
                        
                        if (territoryAssignment.StartAt__c != oldAssignment.StartAt__c && (territoryAssignment.StartAt__c == null || territoryAssignment.StartAt__c <= now.addHours(-1) || oldAssignment.StartAt__c == null || oldAssignment.StartAt__c <= now.addHours(-1)))
                            territoryAssignment.addError('You cannot change things that have already happened.');
                        if (territoryAssignment.EndAt__c != oldAssignment.EndAt__c && ((territoryAssignment.EndAt__c != null && territoryAssignment.EndAt__c <= now.addHours(-1)) || (oldAssignment.EndAt__c != null && oldAssignment.EndAt__c <= now.addHours(-1))))
                            territoryAssignment.addError('You cannot change things that have already happened.');
    
                    }
                    
                    if (territoryAssignment.Account__c != oldAssignment.Account__c)
                        territoryAssignment.addError('You cannot change the Account on a Territory Assignment.');
                    
                    if ((territoryAssignment.Territory__c != oldAssignment.Territory__c && territoryAssignment.Territory__c != null) || territoryAssignment.TerritoryName__c != oldAssignment.TerritoryName__c || territoryAssignment.TerritoryOwner__c != oldAssignment.TerritoryOwner__c || territoryAssignment.RegionName__c != oldAssignment.RegionName__c || territoryAssignment.RegionOwner__c != oldAssignment.RegionOwner__c || territoryAssignment.AreaName__c != oldAssignment.AreaName__c || territoryAssignment.AreaOwner__c != oldAssignment.AreaOwner__c || territoryAssignment.TheaterName__c != oldAssignment.TheaterName__c || territoryAssignment.TheaterOwner__c != oldAssignment.TheaterOwner__c || territoryAssignment.SegmentName__c != oldAssignment.SegmentName__c || territoryAssignment.JobFunction__c != oldAssignment.JobFunction__c)
                        territoryAssignment.addError('You may not change historical tracking records.');
                    
                }
                
            } 
        }
        
    }
}