public class customerUtilityController {
    
    
    @RemoteAction
    public static boolean generateDemoKey(String dataString) {
        // be careful when doing LIKE queries with '%' wildcards on both
        // sides of the query term, especially when using non-indexed
        // fields, as performance can suffer
        string jsonString = '{"datastring":"' + datastring + '"}' ;
        system.debug('Here is the dataString from Demo Key Page: ' + jsonString);
        
        boolean isSandbox = mulesoftIntegrationController.runningInASandbox();
        list<Integration_Setting__mdt> authHelperList = new list<Integration_Setting__mdt>();
        map<string, string> headerMap = new map<string, string>();
        
        
        Integration_Setting__mdt currentMuleProcess = mulesoftIntegrationController.getMuleProcess('generateDemoKey');
        authHelperList.add(currentMuleProcess);
        //getting the integration setting based on the Environment. Sandbox or Prod
        Integration_Setting__mdt envMuleProcess;
        if(isSandbox){
            envMuleProcess = mulesoftIntegrationController.getMuleProcess('Sandbox');
        }else{
            envMuleProcess = mulesoftIntegrationController.getMuleProcess('Production');
        }
        authHelperList.add(envMuleProcess);
        
        //getting the header map ready for the request
        headerMap = mulesoftIntegrationController.SetAuthorizationHeaderValues(authHelperList);
        
        system.debug('ROY6: header map ' + headerMap);
        HttpResponse theResponse = mulesoftIntegrationController.postToMulesoft(jsonString, headerMap, 'generateDemoKey', currentMuleProcess.Request_Type__c, 'application/json');
        
        system.debug('ROY7: HTTP Response ' + theResponse);
        if(theResponse.getStatusCode() == 200){
            //pageReference p = GenerateDemoKeys();
            //Deserialize the datastring into something we can work with
            List<String> frmParameters = dataString.split('&');
            Map<String,String> mapParms = new Map<String,String>();
            Map<String,String> productMapping = new Map<String,String>();
            
            //load up the mapping for products
            productMapping.put('sc_40','Security Center 4.0-4.1');
            productMapping.put('sc_42','Security Center 4.2-5.x');
            productMapping.put('pvs_36','PVS 3.6-4.2.0 (Plugin Activation codes for SCCV)');
            productMapping.put('pvs_421_1','PVS 4.2.1+ (1GB)');
            productMapping.put('pvs_421_10','PVS 4.2.1+ (10GB)');
            productMapping.put('is_150','Industrial Security (150 Assets)');
            productMapping.put('is_800','Industrial Security (800 Assets)');
            productMapping.put('is_2000','Industrial Security (2000 Assets)');
            productMapping.put('lce_4_25','LCE 4-4.4.1 (250GB/25 Silos Plugin Activation codes for SCCV)');
            productMapping.put('lce_4_103','LCE 4-4.4.1 (1TB/103 Silos Plugin Activation codes for SCCV)');
            productMapping.put('lce_4_512','LCE 4-4.4.1 (5TB/512 Silos Plugin Activation codes for SCCV)');
            productMapping.put('lce_4_1024','LCE 4-4.4.1 (10TB/1024 Silos Plugin Activation codes for SCCV)');
            productMapping.put('lce_46_103','LCE 4.6.0+ (1TB/103 Silos)');
            productMapping.put('lce_46_512','LCE 4.6.0+ (5TB/512 Silos)');
            productMapping.put('lce_46_1024','LCE 4.6.0+ (10TB/1024 Silos)');
            productMapping.put('vm','Tenable.io On-Prem (Vulnerability Management)');
            productMapping.put('was','Tenable.io On-Prem (Web Application Scanning)');
            productMapping.put('tio','Tenable.io On-Prem (VM and WAS)');
            
            for (String s : frmParameters) {
                String k = s.split('=')[0];
                String v = s.split('=').size() > 1 ? s.split('=')[1] : '';
                mapParms.put(k,v);
            }
            //list out what we need for the feed item
            String keyReceiver   = EncodingUtil.urlDecode(mapParms.get('keyReceiver'),'UTF-8');
            String customerName  = EncodingUtil.urlDecode(mapParms.get('customerName'),'UTF-8');
            String customerEmail = EncodingUtil.urlDecode(mapParms.get('customerEmail'),'UTF-8');
            String productType   = EncodingUtil.urlDecode(mapParms.get('productType'),'UTF-8');
            String ipRange       = EncodingUtil.urlDecode(mapParms.get('numberOfIps'),'UTF-8');
            String assetSize     = EncodingUtil.urlDecode(mapParms.get('tioSize'),'UTF-8');
            String evalLength    = EncodingUtil.urlDecode(mapParms.get('evalLength'),'UTF-8');
            String accountId     = EncodingUtil.urlDecode(mapParms.get('accountId'),'UTF-8');
            
            system.debug('DAN1: ' + keyReceiver + ' cn:' + customerName + ' ce:' + customerEmail + ' pt:' + productType + ' el:' + evalLength + ' aid:' + accountId);
            
            string tempBody = '';
            if(productType == 'sc_40' || productType == 'sc_42'){
               	tempBody = 'New Demo License generated for customer ' + customerName + '<' + customerEmail + '> for product ' + productMapping.get(productType) + ' with ' + ipRange + ' IPs, expiring in ' + evalLength + ' days.';
            }else if(productType == 'vm' || productType == 'was' || productType == 'tio'){
             	tempBody = 'New Demo License generated for customer ' + customerName + '<' + customerEmail + '> for product ' + productMapping.get(productType) + ' with ' + assetSize + ' assets, expiring in ' + evalLength + ' days.';    
            }else{
            	tempBody = 'New Demo License generated for customer ' + customerName + '<' + customerEmail + '> for product ' + productMapping.get(productType) + ' expiring in ' + evalLength + ' days.';    
            }
            
            
            FeedItem feedItem = new FeedItem(
                parentId = accountId, // where to post message
                body = tempBody,
                isRichText = false,
                Visibility = 'InternalUsers'
            );
            
            insert feedItem;
            
            return true;
        }else{
            // pageReference p = GenerateDemoKeys();
            return false;
        }
    }
    
}