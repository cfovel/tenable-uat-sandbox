@isTest
public with sharing class Tenable_CheckFirstLoginTest {

    @testSetup static void testSetup() {
        Utils.isTest = true;
        Contact testContact = Peak_TestUtils.createTestContact();

        testContact.Customer_Community_User__c=true;
        update testContact;
    }

    @isTest
    public static void testUpdateFirstLogin(){
        List<Contact> testContacts = [SELECT Customer_Community_User__c
                                FROM Contact
                                WHERE Email = 'standarduser@peak.com'];
        
        test.startTest();
        Utils.isTest = true;
        Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
        User testUser = Peak_TestUtils.createStandardUserNoContact();
        testUser.ContactId = testContacts[0].Id;
        insert testUser;
        Test.stopTest();

        Database.executeBatch(new Tenable_CheckFirstLogin(),10);
    }
    
    @isTest 
    public static void testCheckFirstLoginScheduler() {
        test.starttest();
        String CRON_EXP = '0 0 13 * * ?';  
        String jobId = System.schedule('testCheckFirstLoginScheduler', CRON_EXP, new Tenable_CheckFirstLoginScheduler() );

        test.stopTest(); 
    }
}