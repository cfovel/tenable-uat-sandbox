public with sharing class TheaterTriggerHandler
{
	//public static User automationUser = [SELECT Id FROM User WHERE ((FirstName = 'Automation' AND LastName = 'User') OR LastName = 'Automation User') AND IsActive = true LIMIT 1];
    //public static Map<Id, Segment__c> segments = new Map<Id, Segment__c>([SELECT Id, Name FROM Segment__c]);
    
    public class TheaterBeforeInsertHandler implements Triggers.Handler
    {
        public void handle() 
        {
            UpdateTheaterData(Trigger.new);
        }
    } 
    
    public class TheaterBeforeUpdateHandler implements Triggers.Handler
    {
        public void handle() 
        {
            UpdateTheaterData(Trigger.new);
        }
    } 
    
    public class TheaterAfterInsertHandler implements Triggers.Handler
    {
        public void handle() 
        {
            UpdateTerritoryData(Trigger.new);
        }
    } 
    
    public class TheaterAfterUpdateHandler implements Triggers.Handler
    {
        public void handle() 
        {
            UpdateTerritoryData(Trigger.new);
        }
    } 
    
    public static void UpdateTheaterData(List<Theater__c> newTheaters)
    {
        User automationUser = [SELECT Id FROM User WHERE ((FirstName = 'Automation' AND LastName = 'User') OR LastName = 'Automation User') AND IsActive = true LIMIT 1];
        
        Map<Id, Segment__c> segments = new Map<Id, Segment__c>([SELECT Id, Name FROM Segment__c]);
         
        if (Trigger.isInsert) 
        {

            for (Theater__c theater : newTheaters) 
            {
                theater.OwnerId = automationUser.Id;
                theater.SegmentName__c = (theater.Segment__c == null) ? null : segments.get(theater.Segment__c).Name;
            }

        } else if (Trigger.isUpdate) 
        {
            
            for (Theater__c Theater : newTheaters) 
            {
                Theater__c oldTheater = (Theater__c)Trigger.oldMap.get(theater.Id);
                if (theater.OwnerId != automationUser.Id) {
                    theater.OwnerId = automationUser.Id;
                }
                
                if (theater.Segment__c != oldTheater.Segment__c || theater.SegmentName__c != oldTheater.SegmentName__c) {
                    theater.SegmentName__c = (theater.Segment__c == null) ? null : segments.get(theater.Segment__c).Name;
                }
            }            
        }
    }
    
    public static void UpdateTerritoryData(List<Theater__c> newTheaters)
    {
        Set<Id> ownerIds = new Set<Id>();
        
        if (Trigger.isInsert) {
            
            for (Theater__c theater : newTheaters) {
                ownerIds.add(theater.TheaterOwner__c);
            }
            
        } else if (Trigger.isUpdate) {
            
            Map<Id, Theater__c> theaters = new Map<Id, Theater__c>();
            
            List<Area__c> areas = new List<Area__c>();
            
            for (Theater__c theater : newTheaters) {
                Theater__c oldTheater = (Theater__c)Trigger.oldMap.get(theater.Id);
                
                if (theater.TheaterOwner__c != oldTheater.TheaterOwner__c) {
                    ownerIds.add(theater.TheaterOwner__c);
                    ownerIds.add(oldTheater.TheaterOwner__c);
                }
                
                if (theater.Name != oldTheater.Name || theater.TheaterOwner__c != oldTheater.TheaterOwner__c) {
                    theaters.put(theater.Id, null);
                }
                
            }
            
            theaters = new Map<Id, Theater__c>([SELECT Id, (SELECT Id, JobFunction__c FROM Areas__r) FROM Theater__c WHERE Id IN :theaters.keySet()]);
            
            for (Theater__c theater : theaters.values()) {
            
                for (Area__c area : theater.Areas__r) {
                    Theater__c newTheater = (Theater__c)Trigger.newMap.get(theater.Id);
                    
                    areas.add(new Area__c(
                        Id = area.Id,
                        TheaterName__c = newTheater.Name,
                        TheaterOwner__c = newTheater.TheaterOwner__c
                    ));
                }
                
            }
            
            if (!areas.isEmpty())
                update areas;
            
        }
        
        if (!ownerIds.isEmpty())
            TerritoryAssignment.setUserTerritoryFuture(ownerIds);
            
    }
}