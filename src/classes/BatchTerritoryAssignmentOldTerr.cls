global class BatchTerritoryAssignmentOldTerr implements Database.Batchable<sObject> {
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([SELECT Id, Territory__c FROM Account WHERE TerritoryLookup__c = null AND Territory__c <> null]);
    }
    
    global void execute(Database.BatchableContext bc, List<Account> accounts) {

        TerritoryAssignment.setTerritoryAssignmentOldTerr(accounts);
        
        TerritoryAssignment.setAccountTerritory(accounts, true);
         
    }
    
    global void finish(Database.BatchableContext bc) {}
    
}