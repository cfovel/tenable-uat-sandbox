public with sharing class EscalatedAccounts {
/******************************************************************************
*
*   Custom Controller for Escalated Accounts
*
********************************************************************************/

	/*****************************************************************************************
	*	Variable Initialization
	******************************************************************************************/
	public List<EscAcctWrapper> escAcctList {get; set;}
	public Boolean isFirstSalesArea = true;

	public List<EscAcctWrapper> getEscAccts() {
		if (escAcctList == null) {
			isFirstSalesArea = true;
			escAcctList = new List<EscAcctWrapper>();
			String lastSalesArea = null;
			for(Account a :[SELECT Id, Name, Reason_Escalated__c, Escalation_Notes__c, Escalation_Status__c, Current_Status_Notes__c, Open_Case_Count__c, Sales_Area__c,
					Escalation_Trend__c, Escalation_Trend_Arrow__c, Escalated_By__c, Days_Escalated__c, De_Escalation_Criteria__c, LMS_Customer_Id__c
					FROM Account
					WHERE Reason_Escalated__c != '' Order By Sales_Area__c, Escalation_Status__c, Name]) {
					
				EscAcctWrapper escAcct = new EscAcctWrapper(a);
				if(a.Sales_Area__c != lastSalesArea ) {
					escAcct.salesAreaChanged = TRUE;
				}
				if (isFirstSalesArea) {
					escAcct.isFirstSalesArea = true;
					isFirstSalesArea = false;
				}
				lastSalesArea = a.Sales_Area__c;
				escAcctList.add(escAcct);
			}
		}
		return escAcctList;
	}
	
	public class EscAcctWrapper {
		public Boolean salesAreaChanged {get; set;}
		public Boolean isFirstSalesArea {get; set;}
		public Account acct {get; set;}
		
		public EscAcctWrapper(Account a) {
			acct = a;
			salesAreaChanged = false;
			isFirstSalesArea = false;
		}
	}

}