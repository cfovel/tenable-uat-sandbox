global class baDeleteDBCompany implements Database.Batchable<SObject> {
    // To run, login as Automation User, open Execute Anonymous window, and execute:
    //      database.executeBatch(new baDeleteDBCompany(),200);

    global Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator([select Id, D_B_Company__c, DUNNS_Formatted__c,Global_Ultimate_Business_Name__c
                                            FROM Fortune_1000_Company__c]);
                                            
                                            
    }

    global void execute(Database.BatchableContext context, List<Fortune_1000_Company__c> scope) {
    /*
        List<String> DUNNSNumbers = new List<String>();

        for (Fortune_1000_Company__c co : scope) {
            DUNNSNumbers.add(co.DUNNS_Formatted__c);
            //DUNNS1000Map.put(co.Global_Ultimate_DUNS_Number__c, co);
        }

        List<DandBCompany> dAndBList = new List<DandBCompany> ([SELECT Id, Name, DunsNumber, GlobalUltimateDunsNumber, FortuneRank
                                    FROM DandBCompany WHERE GlobalUltimateDunsNumber IN :DUNNSNumbers]);  // and FortuneRank <= 0]);                
        system.debug('D&BCompanies to delete count:' + dAndBList.size() + dAndBList);
        delete dAndBList;
    */                                        
    }

    global void finish(Database.BatchableContext context) {
    }
}