global class ExternalLiveAgentPreChatFormController {
    
 
  public String federalSelectOption {get;set;}
  public String emplRangeSelectOption {get;set;}
  
  public String countrySelectOption {get;set;}
  public String stateSelectOption {get;set;}
  
  public String CustomerContactId {get;set;}
  public Contact CustomerContact {get;set;}
  public Case CustomerCase {get;set;}
  
  private static final Map<String, List<String>> MAP_COUNTRY_TO_STATE_VALUES = FieldDescribeUtil.getDependentOptionsImpl(Case.State_Province__c, Case.Country__c);
  private String pCountryToStatesSerialized;
  private static Set<String> countryValues = new Set<String>(); 
  
  global ExternalLiveAgentPreChatFormController(){
    //CustomerCase = new Case();
    //setProductPicklistValues();
    Schema.DescribeFieldResult fieldResult; 
    List<Schema.PicklistEntry> ple;

    fieldResult = lead.Federal__c.getDescribe();
    ple = fieldResult.getPicklistValues();
    federalSelectOption = json.serialize (ple);
    
    fieldResult = lead.Employee_Range__c.getDescribe();
    ple = fieldResult.getPicklistValues();
    String tempStr = json.serialize (ple).removeStart('[');
    
    emplRangeSelectOption = '[{"active":true,"defaultValue":true,"label":"--None--","validFor":null,"value":null},' + tempStr;
    system.debug('@@@ empl range Select Option');

    fieldResult = case.Country__c.getDescribe();
    ple = fieldResult.getPicklistValues();

    countrySelectOption = json.serialize (ple);
    for (Schema.PicklistEntry pe : ple)
    {
        countryValues.add(pe.getValue());
    }
    
    fieldResult = case.State_Province__c.getDescribe();
    ple = fieldResult.getPicklistValues();
    
    stateSelectOption = json.serialize (ple);
  }
  
  /*
  public String countryToStateValuesMap {
        get {
            if(pCountryToStatesSerialized == null) {
                Map<String, List<String>> mapDependentPicklist = new Map<String, List<String>>();
                //String countryFieldApiName = Schema.Case.fields.Country__c.getDescribe().getName(),
                //       stateFieldApiName = Schema.Case.fields.State_Province__c.getDescribe().getName();
                //mapDependentPicklist.put(countryFieldApiName, new Map<String, List<String>>());
                // Put an empty list for each value mapped to null
                List<String> nullStateList = new List<String>();
                //mapDependentPicklist.get(countryFieldApiName).put('', nullStateList);
                
                for(String countryValue : countryValues) {
                    System.debug(LoggingLevel.DEBUG, 'countryValue: ' + countryValue);
                    mapDependentPicklist.put(countryValue, new List<String>());
                    System.debug(LoggingLevel.DEBUG, mapDependentPicklist.get(countryValue));
                    // Place State values in map
                    if(MAP_COUNTRY_TO_STATE_VALUES.containsKey(countryValue)) {
                        List<String> stateValues = MAP_COUNTRY_TO_STATE_VALUES.get(countryValue);
                        System.debug(LoggingLevel.DEBUG, stateValues);
                        if(stateValues == null) {
                            stateValues = new List<String>();
                        }
                        System.debug(LoggingLevel.DEBUG, stateValues);
                        System.debug(LoggingLevel.DEBUG, mapDependentPicklist.get(countryValue));
                        mapDependentPicklist.get(countryValue).addAll(stateValues);
                        System.debug(LoggingLevel.DEBUG, mapDependentPicklist.get(countryValue));
                    }

                    System.debug(LoggingLevel.DEBUG, mapDependentPicklist);
                }
                pCountryToStatesSerialized = JSON.serialize(mapDependentPicklist);
                
                
                
                
            }
            return pCountryToStatesSerialized;
        }
        private set;
    }
   */
   
   public String countryToStateValuesMap {
        get {
            if(pCountryToStatesSerialized == null) {
                Map<String, Map<String, Map<String, List<String>>>>  mapDependentPicklist = new Map<String, Map<String, Map<String, List<String>>>> ();
                // {"Country__c": {"United States": {"State_Province__c": ["Alabama",...]},"Algeria":{"State_Province__c":[]},...}}
                String countryFieldApiName = Schema.Case.fields.Country__c.getDescribe().getName(),
                       stateFieldApiName = Schema.Case.fields.State_Province__c.getDescribe().getName();
                mapDependentPicklist.put(countryFieldApiName, new Map<String, Map<String, List<String>>>());
                // Put an empty list for each value mapped to null
                Map<String, List<String>> nullCountryValueMap = new Map<String, List<String>>{
                    stateFieldApiName => new List<String>()
                };
                mapDependentPicklist.get(countryFieldApiName).put('', nullCountryValueMap);

                for(String countryValue : countryValues) {
                    System.debug(LoggingLevel.DEBUG, 'countryValue: ' + countryValue);
                    mapDependentPicklist.get(countryFieldApiName).put(countryValue, new Map<String, List<String>>());
                    System.debug(LoggingLevel.DEBUG, mapDependentPicklist.get(countryFieldApiName).get(countryValue));
                    // Place State values in map
                    if(MAP_COUNTRY_TO_STATE_VALUES.containsKey(countryValue)) {
                        List<String> stateValues = MAP_COUNTRY_TO_STATE_VALUES.get(countryValue);
                        System.debug(LoggingLevel.DEBUG, stateValues);
                        if(stateValues == null) {
                            stateValues = new List<String>();
                        }
                        System.debug(LoggingLevel.DEBUG, stateValues);
                        System.debug(LoggingLevel.DEBUG, mapDependentPicklist.get(countryFieldApiName));
                        System.debug(LoggingLevel.DEBUG, mapDependentPicklist.get(countryFieldApiName).get(countryValue));
                        System.debug(LoggingLevel.DEBUG, 'stateFieldApiName: ' + stateFieldApiName);
                        //mapDependentPicklist.get(countryFieldApiName).get(countryValue).addAll(stateValues);
                        mapDependentPicklist.get(countryFieldApiName).get(countryValue).put(stateFieldApiName, stateValues);
                        System.debug(LoggingLevel.DEBUG, mapDependentPicklist.get(countryFieldApiName).get(countryValue));
                    }

                    System.debug(LoggingLevel.DEBUG, mapDependentPicklist);
                }
                pCountryToStatesSerialized = JSON.serialize(mapDependentPicklist);
            }
            return pCountryToStatesSerialized;
        }
        private set;
    }
   
   
   
    public String refreshDependentPicklist() {
        System.debug(LoggingLevel.DEBUG, 'Entering ExternalLiveAgentPreChatFormController.refreshDependentPicklist...');
        Map<String, List<String>> mapCountryToStateValues = refreshState();
        
        Map<String, List<String>> returnMap = new Map<String, List<String>>();
        return '';
    }
    
    private static Map<String, List<String>> refreshState() {
        System.debug(LoggingLevel.DEBUG, 'Refreshing State_Province__c picklist...');
        return new Map<String, List<String>>();
    }
    
  
  
  
  /*
  private void setProductPicklistValues(){
    ProductSelectOptionList = '';
    List<ProductSelectOption> ProductPle = new List<ProductSelectOption>();
    
    for(String v: pleList){
      ProductPle.add(new ProductSelectOption(v, v)); 
    }
    ProductSelectOptionList = JSON.serialize(ProductPle);
  }
  
  @RemoteAction
  global static User getCustomerInfo(String CustomerContactId){
    User CustomerInfo;
    if(!String.isBlank(CustomerContactId)){
      CustomerInfo = [SELECT Username, UserType, Street, State, Profile.Name, ProfileId, Phone, Name, MobilePhone, 
                             LastName, FirstName, Email, CreatedDate, Country, CompanyName, CommunityNickname, City, 
                             Can_Create_Modify_Users__c, Alias, Contact.LMS_Customer_ID__c, Contact.AccountId, Contact.Account.Name
                      FROM User WHERE ContactId = :CustomerContactId];
    }
    return CustomerInfo;
  }
  */
}