/*************************************************************************************
Class: testFeatureRequestControllers
Author: Christine E
Date: 08/30/2018
Details: This test class is for the all feature request controllers
***************************************************************************************/

@isTest
private class testContactAndProductAssignment {	
	@isTest 
    static void testGetProductAssignment() {   	
    	string customerId = '20433';
    	Test.startTest(); 
		Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
		String result = contactAndProductAssignment.getProductAssignment(customerId);
		Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(result);
        System.assertEquals(results.get('maintenanceCode'), 'G2PB-GFTM');
        System.assertEquals(results.get('partNumber'), 'APP-SR100-3');
		Test.stopTest();
    }

	@isTest 
    static void testGetContactAssignment() {   	
    	string customerId = '20433';
    	Test.startTest(); 
		Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
		String result = contactAndProductAssignment.getContactAssignment(customerId);
		Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(result);
        System.assertEquals(results.get('name_last'), 'Guilford');
        System.assertEquals(results.get('customerContactID'), 60006);
		Test.stopTest();
    }
    
    @isTest 
    static void testGetContactsAssignedToProductAccount() {   	
    	string customerId = '20433';
    	string productId = '14';
    	Test.startTest(); 
		Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
		String result = contactAndProductAssignment.getContactsAssignedToProductAccount(customerId,productId);
		Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(result);
        System.assertEquals(results.get('nameFirst'), 'Joel');
        System.assertEquals(results.get('viewState'), 3);
		Test.stopTest();
    }
    
    @isTest 
    static void testViewContactAssignment() {   	
    	string customerId = '52333';
    	string contactId = '149488';
    	Test.startTest(); 
		Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
		String result = contactAndProductAssignment.viewContactAssignment(customerId,contactId);
		Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(result);
        System.assertEquals(results.get('lastName'), 'Oni');
        System.assertEquals(results.get('productName'), 'Nessus Professional');
		Test.stopTest();
    }
    
    @isTest 
    static void testUpdateContactProductViewState() {   	
    	string param = '[{"customerID":"52333","contactID":"149488","productInstanceID":"166745","viewState":"3","productName":"Nessus Professional","maintenaceCode":"MDXK-2F47-KAQS-NXSY","email":"joel@test.com"}]';
    	Test.startTest(); 
		Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
		Boolean result = contactAndProductAssignment.updateContactProductViewState(param,'contactAssignment');

        System.assertEquals(result, true);
		Test.stopTest();
    }
}