public class productAssignmentButtonController {
	public account2installbases__x a;
    public String productInstanceId;
    public String LMSCustomerId;
    
    public productAssignmentButtonController(ApexPages.StandardController controller)
    {
    	//Id sId = ApexPages.currentPage().getParameters().get('id');
    	
    	List<String> fields = new List<String>(); 
        fields.add('productInstanceID__c');
        fields.add('customerID__c');
    	
    	if (!Test.isRunningTest()) 
    	{
    		controller.addFields(fields);
    	}	
    	
        this.a = (account2installbases__x)controller.getRecord();
        if (a.productInstanceID__c != null)
        {
            productInstanceId = String.valueOf(a.productInstanceID__c);
        }
        LMSCustomerId = a.customerID__c;

    }
    
    public PageReference productAssignment()
    {
        String newURL = '/apex/productAssignment?customer=' + LMSCustomerId + '&product=' + productInstanceId;
        PageReference page = new PageReference(newURL);
        
        return page;
    }      
}