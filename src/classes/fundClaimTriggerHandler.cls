public with sharing class fundClaimTriggerHandler {
/*******************************************************************************
*
* Fund Claim Trigger Handler Class
* Version 1.0a
*******************************************************************************/
	public static boolean firstRun = true;
	
    public class FundClaimBeforeInsertHandler implements Triggers.Handler {
        public void handle() {
            PopulateDescriptionFromFund(Trigger.new);
        }
    } 
    
    public class FundClaimBeforeUpdateHandler implements Triggers.Handler {
        public void handle() {
            RequireRejectionComments(Trigger.new, Trigger.oldMap);
            ConvertCurrencyFields(Trigger.new, Trigger.oldMap);
        }
    } 
    
    public static void PopulateDescriptionFromFund(List<Fund_Claim__c> claims) {
        Set<Id> fundIds = new Set<Id>();
         
        for (Fund_Claim__c claim : claims) {
            fundIds.add(claim.Fund__c);
        }   
        
        Map<Id, MDF_Transaction__c> fundDescMap = new Map<Id, MDF_Transaction__c>([SELECT Id, Description__c 
                FROM MDF_Transaction__c WHERE Id IN :fundIds ]);    
        
        for (Fund_Claim__c claim : claims) {
            claim.Fund_Description__c = fundDescMap.get(claim.Fund__c).Description__c;
        }         
    }
    
    public static void RequireRejectionComments(List<Fund_Claim__c> claims, Map<Id, SObject> oldClaimMap) {
        /*****************************************************************************************
        *   Variable Initialization
        ******************************************************************************************/
        Map<Id, Fund_Claim__c> rejectedClaims = new Map<Id, Fund_Claim__c>{};
        
        for(Fund_Claim__c claim : claims)
          {
            /* 
              Get the old object record, and check if the approval status 
              field has been updated to rejected. If so, put it in a map 
              so we only have to use 1 SOQL query to do all checks.
            */
            Fund_Claim__c oldClaim = (Fund_Claim__C)oldClaimMap.get(claim.Id);
        
            if (oldClaim.Status__c != 'Rejected' 
             && claim.Status__c == 'Rejected')
            { 
              rejectedClaims.put(claim.Id, claim);  
            }
          }
           
          if (!rejectedClaims.isEmpty())  
          {
            // Get the most recent approval process instance for the object.
            // If there are some approvals to be reviewed for approval, then
            // get the most recent process instance for each object.
            List<Id> processInstanceIds = new List<Id>{};
            
            for (Fund_Claim__c claim : [SELECT (SELECT ID
                                                      FROM ProcessInstances
                                                      ORDER BY CreatedDate DESC
                                                      LIMIT 1)
                                              FROM Fund_Claim__c
                                              WHERE ID IN :rejectedClaims.keySet()])
            {
                processInstanceIds.add(claim.ProcessInstances[0].Id);
            }
              
            // Now that we have the most recent process instances, we can check
            // the most recent process steps for comments.  
            for (ProcessInstance pi : [SELECT TargetObjectId,
                                           (SELECT Id, StepStatus, Comments 
                                            FROM Steps
                                            ORDER BY CreatedDate DESC
                                            LIMIT 1 )
                                       FROM ProcessInstance
                                       WHERE Id IN :processInstanceIds
                                       ORDER BY CreatedDate DESC])   
            {                   
              if ((pi.Steps[0].Comments == null || 
                   pi.Steps[0].Comments.trim().length() == 0))
              {
                rejectedClaims.get(pi.TargetObjectId).addError(
                  'PLEASE PROVIDE A REJECTION REASON IN THE COMMENTS SECTION.');
              }
            }  
          }
    }
    
    public static void ConvertCurrencyFields(List<Fund_Claim__c> claims, Map<Id, SObject> oldClaimMap)
    {
        List<String> isoCodes = new List<String>();
        Map<String, CurrencyType> conversionMap = new Map<String, CurrencyType>();
        Boolean convCurr;
        
        // Pre-processing
        
        for (Fund_Claim__c claim : claims)
        {
            
            //  Get the old object record, and check if the claim amount has changed. If so, put it in a list 
            //  so we only have to use 1 SOQL query to get all conversion rates.
            
            convCurr = false;
            Fund_Claim__c oldClaim;
            if (oldClaimMap != null)
            {
                oldClaim = (Fund_Claim__c)oldClaimMap.get(claim.Id);
                
                if ((oldClaim != null) && (claim.Claim_Amount__c != oldClaim.Claim_Amount__c || claim.Local_Currency__c != oldclaim.Local_Currency__c || 
                   claim.Estimated_Pipeline__c != oldClaim.Estimated_Pipeline__c || oldClaim.Status__c != 'Submitted' || claim.Claim_Amount_in_USD__c == null))
                {
                    convCurr = true;
                }
            }
            if (convCurr && claim.Local_Currency__c != null && !claim.Local_Currency__c.contains('(USD)'))
            {
                isoCodes.add(claim.Local_Currency__c.substringBetween('(', ')'));
            }
        } 
        
        if (!isoCodes.isEmpty())                                                                // get conversion rates
        {   
            
            for (CurrencyType curr : [SELECT isoCode, decimalPlaces, conversionRate FROM CurrencyType
                                    WHERE isoCode in :isoCodes])
            {
                conversionMap.put(curr.isoCode, curr);
            }                       
            
        }
        
        conversionMap.put('USD', new CurrencyType(isoCode = 'USD', decimalPlaces = 2, conversionRate = 1.0));
        
        // Processing
 
        for (Fund_Claim__c claim : claims)
        {
            convCurr = false;
            Fund_Claim__c oldClaim;
            if (oldClaimMap != null)
            {
                oldClaim = (Fund_Claim__c)oldClaimMap.get(claim.Id);
                
                if ((oldClaim != null) && (claim.Claim_Amount__c != oldClaim.Claim_Amount__c || claim.Local_Currency__c != oldclaim.Local_Currency__c || 
                    claim.Estimated_Pipeline__c != oldClaim.Estimated_Pipeline__c || oldClaim.Status__c != 'Submitted' || claim.Claim_Amount_in_USD__c == null))
                {
                    convCurr = true;
                }
            }
            
            if (convCurr)
            {
            	double convRate;
            	if (claim.Local_Currency__c != null && claim.Local_Currency__c != 'Other')
            	{
                	convRate = conversionMap.get(claim.Local_Currency__c.substringBetween('(', ')')).conversionRate;
            	}	
                if (convRate == null)
                {
                    convRate = 1;
                }
            	
            	if (claim.Claim_Amount__c != null)
            	{
                	claim.Claim_Amount_in_USD__c = claim.Claim_Amount__c / convRate;
            	}
                if (claim.Estimated_Pipeline__c != null)
            	{
                	claim.Estimated_Pipeline_in_USD__c = claim.Estimated_Pipeline__c / convRate;
            	}
            }
        }       
    }  
}