global without sharing class Tenable_SixMonthAlertScheduler implements Schedulable  {

    global void execute(SchedulableContext sc)
    {
        Tenable_SixMonthAlertBatch batchClass = new Tenable_SixMonthAlertBatch ();
        //Parameters of ExecuteBatch(context,BatchSize)
        database.executebatch(batchClass,200);   //
    }
}