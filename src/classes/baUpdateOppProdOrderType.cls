global class baUpdateOppProdOrderType implements Database.Batchable<SObject> {
    // To run, login as Automation User, open Execute Anonymous window, and execute:
    //      database.executeBatch(new baUpdateOppProdOrderType(),200);

    global Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator([select Id, OpportunityId, Opportunity.Primary_Quote__c, Order_Type__c, ProductCode, Order_Category__c
                                            FROM OpportunityLineItem ]);
                                            //WHERE Opportunity.Primary_Quote__c <> null]);
    }

    global void execute(Database.BatchableContext context, List<OpportunityLineItem> scope) {
    /*
        Set<Id> oppIds = new Set<Id>();
        //Set<Id> quoteIds = new Set<Id>();
        List<OpportunityLineItem> olisToUpdate = new List<OpportunityLineItem>();
        List<OpportunityLineItem> olisNoMatch = new List<OpportunityLineItem>();
        Map <Id, List<OpportunityLineItem>> oppOliMap = new Map <Id, List<OpportunityLineItem>> (); // Map of OpportunityId to list of OpportunityLineItems                                     
        Map <Id, List<QuoteLine__c>> oppQLineMap = new Map <Id, List<QuoteLine__c>> ();             // Map of OpportunityId to list of QuoteLines                                       
        
        for (OpportunityLineItem oli : scope) {     
            oppIds.add(oli.OpportunityId);
            //if (oli.Opportunity.Primary_Quote__c != null) {
            //  quoteIds.add(oli.Opportunity.Primary_Quote__c); 
            //}                                     
            if (oppOliMap.containsKey(oli.OpportunityId)) {
                oppOliMap.get(oli.OpportunityId).add(oli);
            }
            else {
                oppOliMap.put(oli.OpportunityId,new List<OpportunityLineItem> {oli});
            }
        }   
        System.debug('@@@ oppIds count: ' + oppIds.size());
        //System.debug('@@@ quoteIds count: ' + quoteIds.size());
        
        List<QuoteLine__c> qLines = [SELECT Id, Quote__c, Quote__r.Opportunity__c, Order_Type__c, Product_SKU__c 
                                        FROM QuoteLine__c WHERE Quote__r.Opportunity__c in :oppIds];        
                                        //FROM QuoteLine__c WHERE Quote__c in :quoteIds];       
        system.debug('@@@ qlines selected: ' + qlines.size());                                                  
        for (QuoteLine__c qLine : qLines) {                                             
            if (oppQLineMap.containsKey(qLine.Quote__r.Opportunity__c)) {
                oppQLineMap.get(qLine.Quote__r.Opportunity__c).add(qLine);
            }
            else {
                oppQLineMap.put(qLine.Quote__r.Opportunity__c,new List<QuoteLine__c> {qLine});
            }
        }           
        
        Boolean matchFound = false;
        Integer matchCount = 0;
        Integer noMatchCount = 0;
        for (Id oppId : oppIds) {
            List<OpportunityLineItem> olis = oppOliMap.get(oppId);
            List<QuoteLine__c> oppQLines = oppQLineMap.get(oppId);
            for (OpportunityLineItem oli : olis) {
                matchFound = false;
                if (oppQLines != null) {
                    for (QuoteLine__c qLine : oppQLines) {
                        if((oli.ProductCode == qLine.Product_Sku__c) && (oli.Order_Type__c <= '')) {
                            oli.Order_Type__c = qLine.Order_Type__c;
                            matchFound = true;
                        }
                    }
                }
                if (matchFound) {
                    matchCount++;
                    olisToUpdate.add(oli);
                }
                else if (oli.Order_Type__c <= '') { 
                    oli.Order_Type__c = oli.Order_Category__c;
                    noMatchCount++;
                    olisNoMatch.add(oli);
                }
                
                
            }
            
        }               
        
        if (!olisToUpdate.isEmpty()) {
            system.debug('@@@ Opp Prod matched count: ' + matchCount );
            system.debug('@@@ Opp Prods matched:' + olisToUpdate);
            update olisToUpdate;
        }   
        if (!olisNoMatch.isEmpty()) {
            system.debug('@@@ Opp Prod not matched count: ' + noMatchCount);
            system.debug('@@@ Opp Prods not matched:' + olisNoMatch);
            update olisNoMatch;
        }       
    */                                        
    }

    global void finish(Database.BatchableContext context) {
    }
}