global class TenableSiteRewriter implements Site.UrlRewriter {
  
  // site url
  String THREAD_PATH_PREFIX = '/thread';
  String MESSAGE_PATH_PREFIX = '/message';
  
  // community url
  String THREAD_COMMUNITY_REWRITER = '/apex/TenableSiteRedirect';
  
  global PageReference mapRequestUrl(PageReference customerSiteUrl){
    String url = customerSiteUrl.getUrl();
    if(url.containsIgnoreCase(THREAD_PATH_PREFIX) || url.containsIgnoreCase(MESSAGE_PATH_PREFIX)){
      return new PageReference(THREAD_COMMUNITY_REWRITER);
    }
    
    return null;
  }
  
  global List<PageReference> generateUrlFor(List<PageReference> salesforceSiteUrls){
    return null;
  }
  
    
    
}