public without sharing class campaignTriggerHandler {
/*******************************************************************************
*
* Campaign Trigger Handler Class
* Version 1.0a
* Copyright 2006-2014 (c) by CloudLogistix.com
* All Rights Reserved
* Your are allowed to make modifications only for yourself, not for distribution
* without permission from CloudLogistix.
*
*******************************************************************************/
    public class CampaignBeforeUpdateHandler implements Triggers.Handler {
        public void handle() {
            ProcessUpdates(Trigger.new, Trigger.old);
        }
    } 

    public class CampaignBeforeDeleteHandler implements Triggers.Handler {
        public void handle() {
            ProcessDelete(Trigger.old);
        }
    } 

    public static void ProcessUpdates(Campaign[] camp, Campaign[] oldcamp) {
        /*****************************************************************************************
        *   Variable Initialization
        ******************************************************************************************/
/*
		Map <Id, Campaign> oldcamps = new Map <Id, Campaign>();
		Set <Id> existingsubs = new Set <Id>();
		Set <Id> campids = new Set <Id>();
		List <EntitySubscription> feedNotifications = new List <EntitySubscription>();
*/		
        /*****************************************************************************************
        *   Record Pre-Processing
        ******************************************************************************************/
/*
        for (Campaign cm : oldcamp) {
			oldcamps.put(cm.id, cm);
			campids.add(cm.id);
        }

		EntitySubscription[] existingsub = ([SELECT SubscriberId FROM EntitySubscription WHERE ParentId in :campids LIMIT 1000]);
		for (EntitySubscription es : existingsub) {
			existingsubs.add(es.SubscriberId);
		}
		GroupMember[] gms = ([SELECT UserorGroupId FROM GroupMember WHERE GroupId = '00G60000002q4tC']);
		Set <Id> groupmembers = new Set <Id>();
		for (GroupMember gm : gms) {
			if (!existingsubs.contains(gm.UserOrGroupId)) {
				groupmembers.add(gm.UserorGroupId);
			}
		}
*/
        /*****************************************************************************************
        *   Record Processing
        ******************************************************************************************/
/*
        for (Campaign cm : camp) {
            if (cm.Approval_Status__c == 'Approved') {
                cm.Campaign_Modified_Notification__c = true;
            }
            if ((oldcamps != null) &&
            	(oldcamps.get(cm.id) != null) &&
            	(oldcamps.get(cm.id).Approval_Status__c != 'Submitted') &&
            	(cm.Approval_Status__c == 'Submitted')) {
            	for (Id uid : groupmembers) {
					EntitySubscription es = new EntitySubscription();
					es.NetworkId = Network.getNetworkId();
					es.ParentId = cm.id;
					es.SubscriberId = uid;
					feedNotifications.add(es);
            	}

			}
        }

		if (feedNotifications != null) {
			insert feedNotifications;
		}
*/
    }

    public static void ProcessDelete(Campaign[] camp) {
        /*****************************************************************************************
        *   Variable Initialization
        ******************************************************************************************/

        /*****************************************************************************************
        *   Record Processing
        ******************************************************************************************/
        for (Campaign cm : camp) {                                                                  // Loop thru all records being processed
            if ((cm.NumberOfLeads > 0) || (cm.NumberOfContacts > 0)) {
                cm.name.addError('Sorry you cannot delete campaigns that have members');            // Prevent a key campaign from being deleted
            }
        }
    }


}