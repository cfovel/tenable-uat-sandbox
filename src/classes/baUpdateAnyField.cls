// Very dangerous!  Use only with permission!
// 01/19/2016 cfovel
// Execute with:
//  database.executeBatch(new baUpdateAnyField('Select Id,Name from Account where Familiar_Name__c like \'X%\'',
//                                              'Name', 'zz'), 200);
// include full query, field to change in resultset, new value
// If you want to null a field, use 'null' as value
// To just POKE the query results of an object (ie, to activate triggers), enter empty value as field name
// If you want to move the contents of one field to another, preface the fieldname with "#" in the value
//  database.executeBatch(new baUpdateAnyField('Select Id,Name, Familiar_Name__c from Account where Familiar_Name__c = \'Acme\'',
//                                              'Name', '#Familiar_Name__c'), 200);

//
global class baUpdateAnyField implements Database.Batchable<sObject>, Database.Stateful
{
    global final static String bName = 'baUpdateAnyField ';
    global Integer numChanged;
    global String Query;
    global String Field;
    global String Value;
    global Boolean bRepField;
    global Boolean bPoke;   
    
    global baUpdateAnyField(String q, String f, String v)
    {
        numChanged = 0;
        Query = q;
        Field = f;
        bRepField = false;
        bPoke = false;
        if (v != null && (v.startsWith('#') && v.length() > 1))
        {
            Value = v.substring(1);
            bRepField = true;
        }
        else Value = v;
        if (f == '' || f == null)
            bPoke = true;
     }
    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        system.debug(Query);
        return Database.getQueryLocator(Query);
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        system.debug('***In execute with ' + scope.size() + ' records');
        numChanged = numChanged + scope.size();
        if (!bPoke)
        {
            for (Sobject s : scope)
            {
                if (Value == 'null')
                    s.put(Field, null);
                else if (bRepField)
                    s.put(Field, s.get(Value));
                else
                    s.put(Field, Value);
            }
        }
        update scope;
    }
    global void finish(database.BatchableContext BC)
    {
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.email
          from AsyncApexJob where Id = :BC.getJobId()];
        sendEmail(a.TotalJobItems, a.NumberofErrors, a.CreatedBy.email,a.Status);
    }
    global void sendEmail(Integer jobs, Integer errs, String crt, String stat)
    { 
        String s = bName;
		String s2 = bName;
        if (bPoke) s2 += ' POKE ';
        else if (bRepField) s2 += 'Replaced Field by Field';
        else s2+= 'Replaced Field ' + Field;
        
        s = 'Query=' + Query;
        if (bPoke)
            s += '\nPOKING the result set only!';
        else if (bRepField)
            s += '\nReplace field (' + Field + ') with Field (' + Value + ')';
        else s += '\nReplace ' + Field + ' with ' + Value;
        system.debug('****' + s);
        //FrontierUtil.sendMsg(s2 + numChanged,
		//	s + '\nProcessed ' + jobs + ' batches with ' + errs + ' failures.  NumChanged=' + numChanged, crt);
    }
}