public class CreateNetSuiteVendor{
    
    public Account acct { get; private set; }
    private final ApexPages.standardController stdController;                   // Set-up Controller Class
    

    
    public CreateNetSuiteVendor(ApexPages.StandardController stdController){

        this.stdController = stdController;                  
        //this.stdController.addFields(new String[]{
        //  'NetSuite_Id__c',
        //  'NetSuite_Link__c',
        //  'Subsidiary__c',
        //  'Local_Currency__c'
        //});                                               // Set-up Controller
        //this.acct = (Account)stdController.getRecord();
        this.acct = [SELECT Id, NetSuite_Id__c, NetSuite_Link__c, Subsidiary__c, Local_Currency__c FROM Account WHERE Id = :stdController.getId()];
    }
    
    public PageReference CreateVendorInNetsuite(){
        String AccountId = ApexPages.currentPage().getParameters().get('id');
        PageReference page = new PageReference('/' + AccountId);
        set<id> AccountIds = new set<id>();
        set<id> payloadIds = new set<id>();
        
        system.debug('Purchase Order Add Button was Pressed');
        
        if (acct.NetSuite_Id__c != null && acct.NetSuite_Id__c != 'NO_VENDOR')
        {
            Utils.addError('A Vendor already exists in Netsuite: ' + acct.NetSuite_Link__c);
            return null;
        }
        else if (acct.Subsidiary__c == null || acct.Local_Currency__c == null)
        {
            Utils.addError('The following fields are required to create a Netsuite Vendor: Subsidiary & Local Currency');
            return null;
        }
        else
        {
            //Adding Contract to PayloadId set
            payloadIds.add(AccountId);
            
            HttpResponse contractMuleEvent = mulesoftIntegrationController.mulesoftEventHandler(payloadIds, AccountId, 'vendorCreate');
            page.setRedirect(true);
                        
            system.debug('Roy8: ' + page);
            return page;
            
        }       
    }
    
    
    public PageReference accountSyncLMSNetsuite(){
        String AccountId = ApexPages.currentPage().getParameters().get('id');
        set<id> AccountIds = new set<id>();
        set<id> payloadIds = new set<id>();
        
        system.debug('Create LMS Account Button was pressed');
        //Adding Contract to PayloadId set
        payloadIds.add(AccountId);
        
        
        HttpResponse contractMuleEvent = mulesoftIntegrationController.mulesoftEventHandler(payloadIds, AccountId, 'accountSync');
        
        PageReference page = new PageReference('/' + AccountId);
        page.setRedirect(true);
        system.debug('Roy8: ' + page);
        return page;
    }
    
    
}