public with sharing class emailMessageTriggerHandler {
/*******************************************************************************
*
* Case Email Trigger Handler Class
* Version 1.0a
* Copyright 2006-2015 (c) by CloudLogistix.com
* All Rights Reserved
* Your are allowed to make modifications only for yourself, not for distribution
* without permission from CloudLogistix.
*
*******************************************************************************/

	public class EmailMessageAfterInsertHandler implements Triggers.Handler {
		public void handle() {
			ProcessChecks(Trigger.new);
		}
	} 
	


	public static void ProcessChecks(EmailMessage[] msgs) {
		CaseComment[] commentstoAdd = new CaseComment[0];
		Set <Id> msgIds = new Set <Id>();
		//List <Id> msgIds = new List <Id>();
		EmailMessage[] msgsToRetry = new EmailMessage[0];
		List <Id> msgIdList = new List<Id>();
		String newcomment = '';

		for (EmailMessage m: msgs) {
			if ((m != null) && 
				(m.ParentId != null) && 
				(m.TextBody != null)) {
				String[] newcomments = m.TextBody.split('Original Message');

				if ((newcomments != null) &&
					(newcomments[0] != null)) {
					newcomment = newcomments[0].replace('---------------', '');						
				}

system.debug('commentLength='+newcomment.length());				
				commentstoAdd.add(new CaseComment(ParentId = m.ParentId, CommentBody = newcomment, isPublished=TRUE));
				msgIdList.add(m.Id);
			}
		}
	
		if (commentstoAdd.size() > 0) {
			try {
				system.debug('@@@ msgIdList: ' + msgIdList);
				Database.DMLOptions dmo = new Database.DMLOptions();
				dmo.allowFieldTruncation = true;
				dmo.optAllOrNone = false;
				Database.SaveResult[] srList = Database.insert (commentstoAdd, dmo);
				for (Integer i=0; i<srList.size(); i++) {
					Database.SaveResult sr = srList[i];
					system.debug('@@@ save result: ' + sr);
	    			if (!sr.isSuccess()) {													// if insert of case comment was not successful
						msgIds.add(msgIdList[i]);											// ... add it to a set to try again
					}
				}
				if (msgIds.size() > 0) {
					Map<Id, EmailMessage> msgMap = new Map<Id, EmailMessage>(msgs);
						
					for (Id msgId : msgIds) {
						msgsToRetry.add(msgMap.get(msgId));
					}
					
					RetryCaseComments(msgIds);												// Call future method to retry insert
				}
			}
			catch (exception e) {
system.debug('commentstoAdd='+commentstoAdd);				
			}
		}
	}
	
	@future
    public static void RetryCaseComments(Set <Id> msgIds) {
    	List <Id> msgIdsToRetry = new List <Id>();
		EmailMessage[] msgsToRetry = new EmailMessage[0];
		List <Id> msgIdList = new List<Id>();
    	CaseComment[] commentstoAdd = new CaseComment[0];
		String newcomment = '';
    	EmailMessage[] msgs = ([SELECT TextBody, ParentId FROM EmailMessage WHERE Id in :msgIds]);
    	for (EmailMessage m : msgs) {
    		String[] newcomments = m.TextBody.split('Original Message');

				if ((newcomments != null) &&
					(newcomments[0] != null)) {
					newcomment = newcomments[0].replace('---------------', '');						
				}
				commentstoAdd.add(new CaseComment(ParentId = m.ParentId, CommentBody = newcomment, isPublished=TRUE));
				msgIdList.add(m.Id);
    	}
    	if (commentstoAdd.size() > 0) {
			try {
				Database.DMLOptions dmo = new Database.DMLOptions();
				dmo.allowFieldTruncation = true;
				dmo.optAllOrNone = false;
				Database.SaveResult[] srList = Database.insert (commentstoAdd, dmo);
				for (Integer i=0; i<srList.size(); i++) {
					Database.SaveResult sr = srList[i];
					system.debug('@@@ save result: ' + sr);
	    			if (!sr.isSuccess()) {																		// if insert of case comment was not successful
						msgIdsToRetry.add(msgIdList[i]);														// ... add it to a set to try again
					}
				}
				if (msgIdsToRetry.size() > 0) {
					Map<Id, EmailMessage> msgMap = new Map<Id, EmailMessage>(msgs);
						
					for (Id msgId : msgIdsToRetry) {
						msgsToRetry.add(msgMap.get(msgId));
					}
					
					Datetime executeTime = (System.now()).addMinutes(1).addSeconds(30);							// schedule the job to run in 1.5 minute
					String cronExpression = Utils.GetCRONExpression(executeTime);
					
					ScheduleCaseComment scheduledJob = new ScheduleCaseComment(msgsToRetry);					// Instantiate a new Scheduled Apex class
					System.schedule('ScheduleCaseComment ' + executeTime.getTime(),cronExpression,scheduledJob);
					
				}
			}
			catch (exception e) {
				system.debug('Error inserting case comments, commentstoAdd='+commentstoAdd);				
			}
		}
    }
}