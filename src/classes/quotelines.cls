public with sharing class quotelines {
/******************************************************************************
*
*   Custom Controller for Quote Lines Layout
*   Copyright 2006-2014 (c) by CloudLogistix.com
*   All Rights Reserved, Modifications can only be made for your direct use and 
*   not for distribution without explicit permission from CloudLogistix.
*
********************************************************************************/
    /*****************************************************************************************
    *   Variable Initialization
    ******************************************************************************************/
    public Double unitPrice {get; set;}
    public Double discount {get; set;}
    public Double targetPrice {get; set;}
    public String pageid = ApexPages.currentPage().getParameters().get('id');
    public String quoteid = ApexPages.currentPage().getParameters().get('quoteid');
    public String geo = ApexPages.currentPage().getParameters().get('geo');
    public Boolean sizeRequired { get; set; }       
    public String unitType {get; set;}     
    public static List<Product2> prodList;                                  

    /*****************************************************************************************
    *   Custom Controller Initialization
    ******************************************************************************************/ 
    private final ApexPages.standardController stdController;

    public quotelines(ApexPages.StandardController stdController) {
        init();
    }
    
    public void init() {
//      loadQuote();                                                                                // Go get our Quote
        loadQuoteLine();                                                                            // Go get our QuoteLine
    }

    /*****************************************************************************************
    *   Getter/Setter's to return values to VF Page(s)
    ******************************************************************************************/ 
    public Boolean getSizeRequired()
    {
    	system.debug('@@@ getSizeRequired:' + sizeRequired);
    	return this.sizeRequired;
    }
    public void setSizeRequired(Boolean rqd)
    {
    	this.sizeRequired = rqd;
    	system.debug('@@@ setSizeRequired:' + sizeRequired);
    }
    
    public QuoteLine__c entry {get; set;}
    public QuoteLine__c getQuoteLine() {return this.entry;}                                         // Return the QuoteLine to the VF Page
    public void loadQuoteLine() {                                                                   // Load the QuoteLine
        QuoteLine__c[] qlines =                                                                     // We want to do this as an array in case there are no entries
            ([SELECT Quantity__c, Category__c, Family__c, License_Type__c, Deployment__c,
                SLA_level__c, term__c, quote__c, list_price__c, LineNumber__c,
                support_term__c, discount__c, start_date__c, end_date__c, PCI_List_Price_Override__c,
                unit_list_price__c, unit_net_price__c, order_type__c,
                extended_list_price__c, extended_net_price__c, geography__c,
                product_SKU__c, product_name__c, description__c, unit__c,
                LastModifiedDate, LastModifiedById, LastModifiedBy.Name,
                CreatedDate, CreatedById, CreatedBy.Name, Base_Unit_Price__c,
                Quote__r.Partner__c, quote__r.Partner_SC_Discount__c, quote__r.Partner_Nessus_Discount__c,
                quote__r.Service_Discount_Level__c, quote__r.Training_Discount_Level__c,
                quote__r.Dist_Svc_Discount_Level__c, quote__r.Dist_Trng_Discount_Level__c, Pricing_Term__c,
                quote__r.Distributor_Discount__c, Apply_Calculated_Price__c, Bypass_Unit_Price_Limit__c,
                Bypass_Svc_Trng_Discount_Limit__c, Container_Size__c, Tier_Size__c, Number_of_Units__c, Unit_Type__c,
                Override_New_Pricing__c, Trade_In__c
                FROM QuoteLine__c WHERE id=:pageid]); 
        geography = geo;
        if (qlines.size() > 0) {                                                                    // As long as we found an entry, save it as our quoteline
            entry = qlines[0];
            unitPrice = entry.List_Price__c;                                                        // Save our Dynamic Picklist values 
            category = entry.category__c;
            family = entry.family__c;
            unit = entry.unit__c;
            license = entry.license_type__c;
            product = entry.Product_Name__c;
            geography = entry.Geography__c;
            deployment = entry.Deployment__c;
            unitType = entry.Unit_Type__c;
        }
        else {                                                                                      // Otherwise assume we're adding a new record
            entry = new QuoteLine__c(quote__c=quoteid);     										// Create the new record
            setSizeRequired(false);                                        
        }
        
        if ((entry.Container_Size__c != null && entry.Container_Size__c.contains('More than')) || (entry.Tier_Size__c != null && entry.Tier_Size__c == 'Custom'))
		{
			setSizeRequired(true);
		}
    }

/*
    public Quote__c quote {get; set;}                                                               // Getter/Setter for the Quote for the VF Page
    public void loadQuote() {
        String querystring;
        if (quoteId != null)
        {
            querystring = 'SELECT Id, Geography_Price_List__c, Distributor__c, Partner__c, Partner_SC_Discount__c, ';
            querystring += 'Partner_Nessus_Discount__c, Distributor_Discount__c FROM Quote__c WHERE id=\''+quoteid+'\'';
            quote = Database.query(querystring);
        }
    }
*/
    /*****************************************************************************************
    *   Page Functions
    ******************************************************************************************/ 
    public PageReference save() {                                                                   // Page Save Function
        entry.category__c = category;                                                               // Set our Dynamic Picklist values
        entry.family__c = family;
        entry.license_type__c = license;
        entry.unit__c = unit;
        entry.Product_Name__c = product;
        entry.Deployment__c = deployment;
        entry.Unit_Type__c = unitType;
        
        try {                                                                                       // Try to update the record
            upsert(entry);                                                                          // Save it
        } 
        catch(exception e) {                                                                        // Had a problem
            Utils.addError('Unable to save record: '+e.getMessage());                               // Report it back to the back
            return null;                                                                            // Make sure we don't refresh the page if there's an error
        }
        return (new ApexPages.StandardController(entry)).view();                                    // Stay on the same page after refresh
   }
   
    public PageReference deleteQline() {
        if (entry.id != null) {                                                                     // Make sure we have a quote line to delete
            try {
                String returnpage = '/' + entry.quote__c;                                           // Save quote number to return to
                delete entry;                                                                       // Try deleting it
                return pageredirect(returnpage);                                                    // Return new page 
            }
            catch (exception e) {
                Utils.addError('Unable to delete record: '+e.getMessage());                         // Report error back to the page
                return null;                                                                        // Make sure we don't refresh the page if there's an error
            }
        }
        else {
            Utils.addError('Unable to delete record - missing record');                             // Problem in that we had no entry to delete
            return null;                                                                            // Make sure we don't refresh the page if there's an error
        }
    }

    public PageReference cancel() {                                                                 // Cancel Button      

        if (quoteid == null)   
        {
            quoteId = entry.quote__r.Id;
        }   
                                            
        return pageredirect('/' + quoteid);                                                         // Return new page 
    }
    
    public PageReference cloneQline() {
        entry.category__c = category;                                                               // Set our Dynamic Picklist values
        entry.family__c = family;
        entry.license_type__c = license;
        entry.unit__c = unit;
        entry.Product_Name__c = product;
        entry.Deployment__c = deployment;
        entry.Unit_Type__c = unitType;
        try {                                                                                       // Try to update the record
            upsert(entry);                                                                          // Save it
        } 
        catch(exception e) {                                                                        // Had a problem
            Utils.addError('Unable to save record: '+e.getMessage());                               // Report it back to the back
            return null;                                                                            // Make sure we don't refresh the page if there's an error
        }
    
        QuoteLine__c newentry = entry.clone(false);                                                 // Make a copy of this line item without the id
        try {
            insert newentry;                                                                        // insert the cloned copy
            return pageredirect('/' + newentry.id);                                                 // Return new page 
        }
        catch (exception e) {
            Utils.addError('Unable to clone record: '+e.getMessage());                              // Report error back to the page
            return null;                                                                            // Make sure we don't refresh the page if there's an error
        }
    }


    public PageReference newQline() {
        save();
        entry = new QuoteLine__c(quote__c=entry.quote__c);                                          // Create a new entry
        return pageredirect('/apex/quoteLine?quoteid='+entry.quote__c);                             // Point to the new page
    }

    public PageReference pageredirect(String reference) {
        PageReference newPage = new PageReference(reference);                                       // Redirect to a new page
        newPage.setRedirect(true);                                                                  // Force redirection
        return newPage;                                                                             // Return new page 
    }

    /*****************************************************************************************
    *   Dynamic Picklist Construction
    ******************************************************************************************/
    String Geography;
    String Category;
    public String getCategory() { return this.category; }
    public void setCategory(String s) {
        this.category = s;
        entry.category__c = s;
    }
    public List<SelectOption> getCategories() {

        List<SelectOption> optionList = new List<SelectOption>();

        Schema.DescribeFieldResult fieldResult = QuoteLine__c.Category__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        optionList.add(new SelectOption('', '- None -')); 
        for( Schema.PicklistEntry f : ple) {optionList.add(new SelectOption(f.getLabel(), f.getValue()));}       
        return optionList;
    }

    public List<SelectOption> buildList(List <String> listvalues, Boolean sortlist) {
        Set <String> uniquevalues = new Set <String> ();
        List <String> values = new List <String> ();
        List<SelectOption> optionList = new List<SelectOption>();
        optionList.add(new SelectOption('', '- None -'));
        String valuetoadd = null;
        for (String u : listvalues) {
            if (u != null) {valuetoadd = u;}
            if (valuetoadd != null && !uniquevalues.contains(valuetoadd)) {values.add(valuetoadd);  uniquevalues.add(valuetoadd);}
        }
        if (sortlist) {values.sort();}

        for (String u : values) {if (u != null) {optionList.add(new SelectOption(u,u));}}
        return optionList;
    }

    String family;
    public String getFamily() { return this.family; }
    public void setFamily(String s) 
    { 
    	this.family = s; 
    	entry.Family__c = s;
    }
    public List<SelectOption> getFamilies() {
        List <String> values = new List <String>();
        Product2[] prod = ([SELECT Category__c, Family 
                            FROM Product2 
                            WHERE IsActive=TRUE 
                            AND Hidden__c=FALSE 
                            AND Category__c = :category
                            AND Geography__c = :geography
                            AND Order_Category__c INCLUDES (:entry.Order_Type__c)
                            ORDER BY Family ASC]);
        for (Product2 p : prod) {values.add(p.family);}
        return buildList(values, true);
    }

    String license;
    public String getLicense() { return this.license; }
    public void setLicense(String s) { this.license = s; }
    public List<SelectOption> getLicenses() {
        List <String> values = new List <String>();
        Product2[] prod = ([SELECT Category__c, Family, License_Type__c
                            FROM Product2 
                            WHERE IsActive=TRUE 
                            AND Hidden__c=FALSE 
                            AND Category__c = :category 
                            AND Geography__c = :geography
                            AND Family = :family
                            AND Order_Category__c INCLUDES (:entry.Order_Type__c)
                            ORDER BY License_Type__c ASC]);
        for (Product2 p : prod) {values.add(p.License_Type__c);}

        return buildList(values, true);
    }

    String unit;
    public String getUnit() { return this.unit; }
    public void setUnit(String s) { this.unit = s; }
    public List<SelectOption> getUnits() {
        List <String> values = new List <String>();
        Product2[] prod = ([SELECT Category__c, Family, License_Type__c, Unit__c
                            FROM Product2 
                            WHERE IsActive=TRUE 
                            AND Hidden__c=FALSE 
                            AND Category__c = :category 
                            AND Geography__c = :geography
                            AND Family = :family
                            AND License_Type__c = :license
                            AND Order_Category__c INCLUDES (:entry.Order_Type__c)
                            ORDER BY Unit_Sort_Order__c ASC]);
        for (Product2 p : prod) {values.add(p.Unit__c);}
        return buildList(values, false);
    }

    String product;
    public String getProduct() { return this.product; }
    public void setProduct(String s) { this.product = s; }
    public List<SelectOption> getProducts() {
        List <String> values = new List <String>();
        
        System.debug('@@@ getProducts, category: ' + category + ', family: ' + family + ', deployment: ' + deployment);
        //Product2[] prod  
        prodList = ([SELECT Name, Category__c, Family, License_Type__c, Unit__c, Deployment__c, Unit_of_Measure__c
                            FROM Product2 
                            WHERE IsActive=TRUE 
                            AND Hidden__c=FALSE 
                            AND Category__c = :category 
                            AND Geography__c = :geography
                            AND Family = :family
                            AND License_Type__c = :license
                            AND Unit__c = :unit
                            AND Order_Category__c INCLUDES (:entry.Order_Type__c)
                            AND Deployment__c = :deployment
//                            AND Term_Minimum__c <= :entry.term__c
//                            AND Term_Maximum__c >= :entry.term__c
                            ORDER BY Name ASC]);
        System.debug('@@@ prodList: ' + prodList);                    
        for (Product2 p : prodList) {values.add(p.Name);}
        return buildList(values, true);
    }
    
    String deployment;
    public String getDeployment()
    {
    	if (getFamily() == 'Tenable.io')
    	{
    		if (getLicense() == 'Perpetual')
    		{
    			setDeployment('On-Prem');
    		}
    	}
    	else
    	{
    		setDeployment(null);
    	}
    	return this.deployment;
    }
    
    public void setDeployment(String s) 
    { 
    	this.deployment = s; 
    	entry.Deployment__c = s;
    }

    public List<SelectOption> getDeployments() 
    {
    	List<SelectOption> optionList = new List<SelectOption>();

        Schema.DescribeFieldResult fieldResult = QuoteLine__c.Deployment__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        optionList.add(new SelectOption('', '- None -')); 
        for( Schema.PicklistEntry f : ple) {optionList.add(new SelectOption(f.getLabel(), f.getValue()));}       
        return optionList;
    }
    
    public PageReference clearDeployment()
    {
    	setDeployment(null);
    	return null;
    }

    /*****************************************************************************************
    *   Target Price Calculator Function(s)
    ******************************************************************************************/
    public Double getTargetPrice() 
    {
        targetPrice = entry.Unit_Net_Price__c; 
        return targetPrice;
    }
    
    public void setTargetPrice(Decimal s) 
    {
        targetPrice = s;
    }

    public PageReference calcDisc() {
        if (entry.id != null) {
            entry.category__c = category;
            entry.family__c = family;
            entry.license_type__c = license;
            entry.Product_Name__c = product;
            system.debug('@@@ in CalcDisc, targetPrice = ' + targetPrice + ', unitPrice per month based on one year term = ' + unitPrice);
            if ((targetPrice >= 0) && (unitPrice <> 0) && (entry.Quantity__c <> 0)) 
            {
                if (entry.Category__c == 'Products' && entry.Product_Name__c != null && (entry.Product_Name__c.startsWith('Tenable.io Vulnerability Management') || entry.Product_Name__c.startsWith('Tenable.io Container Security')
                		|| entry.Product_Name__c == 'Industrial Security' || entry.Product_Name__c.startsWith('Tenable.io Web Application Scanning') || entry.Product_Name__c.startsWith('Tenable.io Companion License')))
                {
                    double unitPriceTIO;
                    if (entry.term__c == 0)
                    {
                    	unitPriceTIO = entry.Base_Unit_Price__c*entry.Number_of_Units__c * entry.Quantity__c;
                    }
                    else
                    {
                    	unitPriceTIO = entry.Base_Unit_Price__c*entry.Number_of_Units__c * entry.Quantity__c * entry.term__c/12;
                    }
                    discount = 100 * (1 - (targetPrice / unitPriceTio));
                }
                else if (entry.term__c == 0)
                {
                    discount = 100 * (1 - (targetPrice / (unitPrice * entry.Quantity__c)));
                } 
                else
                {
                    discount = 100 * (1 - (targetPrice / (unitPrice * entry.term__c * entry.Quantity__c)));
                }    
                entry.Discount__c = discount;
                system.debug('@@@ discount (calculated from target price) :' + discount);
                try {
                    update entry;
                    return (new ApexPages.StandardController(entry)).view();
                }
                catch (exception e) {return null;}
            }
            else {return null;}
        }
        else {return null;}
    }
    
    public PageReference calcEndDate() 
    {
    	system.debug('in calcEndDate, startDate:' + entry.Start_Date__c + ', term: ' + entry.Term__c + ', pageId:' + pageId);
    	//if ((pageId == null) && (entry.Start_Date__c != null) && 								// only when creating the quoteline, if you change the start date, endate will adjust
    	if ((entry.Start_Date__c != null) && (entry.Term__c != null) && (license != 'Perpetual'))								// only when creating the quoteline, if you change the start date, endate will adjust
    			//(entry.Start_Date__c != Date.Today()) && (entry.Term__c != null))
        {
        	Integer daysInMonth;
        	Integer additionalDays;
        	decimal fractionalMonth;
        	decimal months = Math.floor(entry.Term__c);
        	
        	date enddate = entry.start_date__c.addMonths((Integer)months);

        	fractionalMonth = entry.term__c - months;

        	if (fractionalMonth > 0)
        	{
        		daysInMonth = Date.daysInMonth(endDate.year(), endDate.month());
        		additionalDays = (Integer)Math.rint((fractionalMonth)*daysInMonth);
        		endDate = endDate.addDays(additionalDays);	
        	}
        	
        	if (entry.Pricing_Term__c == 'Daily') 
            {
        		enddate = entry.start_date__c.addDays(Math.round(entry.term__c));
            }
            entry.end_date__c = endDate.addDays(-1);
        }
        else
        {
        	entry.end_date__c = null;
        }
    	
    	return null;
    }
    
    public PageReference calcTerm() 
    {
    	system.debug('in calcTerm, startDate:' + entry.Start_Date__c +'endDate:' + entry.End_Date__c + ', term: ' + entry.Term__c + ', pageId:' + pageId);
    	//if ((pageId == null) && (entry.End_Date__c != null) && 								// only when creating the quoteline, if you change the end date, term will adjust
    	if ((entry.End_Date__c != null) && (entry.Start_Date__c != null) && (license != 'Perpetual'))								// only when creating the quoteline, if you change the end date, term will adjust	
    			//(entry.Start_Date__c != Date.Today()) && (entry.Term__c != null))
        {
        	Integer daysInMonth;
        	Integer additionalDays;
        	Integer monthsBetween;
        	Date newDate;
        	
        	//decimal fractionalMonth;
        	//decimal months = Math.floor(entry.Term__c);
        	/*
        	date enddate = entry.start_date__c.addMonths((Integer)months);

        	fractionalMonth = entry.term__c - months;

        	if (fractionalMonth > 0)
        	{
        		daysInMonth = Date.daysInMonth(endDate.year(), endDate.month());
        		additionalDays = (Integer)Math.rint((fractionalMonth)*daysInMonth);
        		endDate = endDate.addDays(additionalDays);	
        	}
        	
        	//if (isProdMatch.Product2.Pricing_Term__c == 'Daily') 
            //{
        	//	enddate = entry.start_date__c.addDays(Math.round(entry.term__c));
            //}
            entry.end_date__c = endDate.addDays(-1);
            */
            
            daysInMonth = Date.daysInMonth(entry.end_date__c.year(), entry.end_date__c.month());

            if (entry.start_date__c.day() == 1)
            {
                monthsBetween = entry.start_date__c.addDays(1).monthsBetween(entry.end_date__c.addDays(1));
            }
            else
            {
                monthsBetween = entry.start_date__c.monthsBetween(entry.end_date__c);
            }
            newDate = entry.start_date__c.addMonths(monthsBetween);
            additionalDays = newDate.daysBetween(entry.end_date__c) + 1; 
            entry.Term_in_Months__c = ((decimal)monthsBetween + (decimal)additionalDays/daysInMonth).setScale(3);         // calculate term = end date - start date in 1 decimal months

			if (entry.Pricing_Term__c == 'Annual') 
            {
                entry.term__c = entry.Term_in_Months__c;
            }
			else if (entry.Pricing_Term__c == 'Daily') 
            {
                entry.term__c = entry.start_date__c.daysBetween(entry.end_date__c) + 1;
            }
            
        }
    	
    	return null;
    }
    
    public PageReference calcContSize() 
    {
    	//system.debug('@@@ in calcContSize, containerSize:' + entry.Container_Size__c + ', in GB:' + entry.Container_Size_in_GB__c);
    	system.debug('@@@ in calcContSize, containerSize:' + entry.Container_Size__c + ', in GB:' + entry.Number_of_Units__c);
    	if (entry.Container_Size__c != null)
    	{
    		entry.Unit_Type__c = 'GBs';
    		if (entry.Container_Size__c.contains('More than'))
    		{
    			setSizeRequired(true);
    			entry.Number_of_Units__c = null;
    		}
    		else
    		{	
    			Container_Security_Unit_Price__mdt cs = [SELECT Container_Size_in_GB__c FROM Container_Security_Unit_Price__mdt WHERE Container_Size__c = :entry.Container_Size__c limit 1];
    			entry.Number_of_Units__c = cs.Container_Size_in_GB__c;
    			setSizeRequired(false);
    		}
    		system.debug('@@@ in calcContSize-2, containerSize:' + entry.Container_Size__c + ', in GB:' + entry.Number_of_Units__c);
    		
    	}
    	else
    	{
    		Utils.addError('Container Size is required with Container Security product.');     
    	}
    	
    	return null;
    }
    
    public PageReference calcTierSize() 
    {
    	//system.debug('@@@ in calcContSize, containerSize:' + entry.Container_Size__c + ', in GB:' + entry.Container_Size_in_GB__c);
    	system.debug('@@@ in calcTierSize, TierSize:' + entry.Tier_Size__c + ', in Assets:' + entry.Number_of_Units__c);
    	
    	entry.Unit_Type__c = 'Assets';
    	if (entry.Tier_Size__c != null)
    	{
    		if (entry.Tier_Size__c == 'Custom')
    		{
    			setSizeRequired(true);
    			entry.Number_of_Units__c = null;
    		}
    		else
    		{	
    			if (entry.Tier_Size__c == 'Small')
    			{
    				entry.Number_of_Units__c = 150;
    			}
    			else if (entry.Tier_Size__c == 'Medium')
    			{
    				entry.Number_of_Units__c = 1000;
    			}
    			else if (entry.Tier_Size__c == 'Large')
    			{
    				entry.Number_of_Units__c = 2500;
    			}
    				
    			setSizeRequired(false);
    		}
    		system.debug('@@@ in calcTierSize-2, tierSize:' + entry.Tier_Size__c + ', in Assets:' + entry.Number_of_Units__c);
    		
    	}
    	else
    	{
    		Utils.addError('Tier Size is required with Industrial Security product.');     
    	}
    	
    	return null;
    }
    
    public PageReference clearProduct() {
    	String s;
    	setProduct(s);
    	
    	return null;
    }
    
    public PageReference determineUnitType() 
    {
    	String s; 
    	Product2 prod = new Product2();
    	for (Product2 p : prodList)
    	{
    		if (p.Name == this.product)
    		{
    			prod = p;
    		}
    	}
    	s = prod.Unit_of_Measure__c;
    	setUnitType(s);
    	system.debug('@@@ unitType= ' + this.unitType);
    	
    	return null;
    }
    
    public String getUnitType ()
    {
    	return this.unitType;
    }
    
    public void setUnitType(String s)
    {
    	this.unitType = s;
    }
}