public with sharing class invoiceOppLineItemTriggerHandler {
/*******************************************************************************
*
* Invoice Opp Line Item Trigger Handler Class
* Version 1.0
*
*******************************************************************************/
    public class InvoiceOppLineAfterInsertHandler implements Triggers.Handler {
        public void handle() {
            ProcessAfterRecords(Trigger.new);
        }
    }


    public class InvoiceOppLineAfterUpdateHandler implements Triggers.Handler {
        public void handle() {
            ProcessAfterRecords(Trigger.new);
        }
    }
    
    public class InvoiceOppLineAfterDeleteHandler implements Triggers.Handler {
        public void handle() {
            ProcessAfterRecords(Trigger.old);
        }
    }
    
    public class InvoiceOppLineAfterUndeleteHandler implements Triggers.Handler {
        public void handle() {
            ProcessAfterRecords(Trigger.new);
        }
    }

    public static void ProcessAfterRecords(InvoiceOppLineItem__c[] records) 
    {
        Set<Id> oppIds = new Set<Id>();
         
        for (InvoiceOppLineItem__c invOLI : records)
        {
            oppIds.add(invOLI.Opportunity__c);
        }    

        List <Opportunity> oppsToUpdate = [SELECT Id, Invoiced_Amount__c, (SELECT Id, Invoice_Amount__c FROM InvoiceOppLineItems__r)
        												FROM Opportunity WHERE Id IN :oppIds];
        
        for (Opportunity opp : oppsToUpdate) 
		{                                                
        	Decimal invAmt = 0;  
            for (InvoiceOppLineItem__c invLineItem : opp.InvoiceOppLineItems__r)
            {
            	if (invLineItem.Invoice_Amount__c != null)
            	{
            		invAmt+= invLineItem.Invoice_Amount__c;
            	}
            }
            
            opp.Invoiced_Amount__c = invAmt;
        }  
        
        update oppsToUpdate;
    }      
}