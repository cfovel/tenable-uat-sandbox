global class baUpdateTerritoryNamedAccounts implements Database.Batchable<SObject> {
    // To run, login as Automation User, open Execute Anonymous window, and execute:
    //      database.executeBatch(new baUpdateTerritoryNamedAccounts(),200);

    global Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator([SELECT Id, Name, OwnerId, Territory__c, isExcludedFromRealign
            FROM Account WHERE IsExcludedFromRealign = false AND Territory__c = null AND Sales_Area__c <> null]);
    }

    global void execute(Database.BatchableContext context, List<Account> scope) {
        
        Boolean updateTerritory = FALSE;
        
        if (!utils.isTest) 
        {
           updateTerritory = utils.marketingappsettings().get('AccountUpdateTerritory').Enabled__c;
        }    
        
        if (updateTerritory)
        {
        	Boolean findTerritories = false;
	        List<Account> updates = new List<Account>();
	        
	        /*****************************************************************************************
            *   Record Pre-Processing
            ******************************************************************************************/ 
            Map<String, Territory_Named_Account__c> terrAssignMap = new Map<String, Territory_Named_Account__c>();  
            Set<String> salesAreas = new Set<String>();
            
            for (Account acct : scope)
            {
            	system.debug ('@@@ updateTerr : ' + updateTerritory + ', isExcluded: ' + acct.isExcludedFromRealign + ', Territory: ' + acct.Territory__c );
                if (updateTerritory && !acct.IsExcludedFromRealign && (acct.Territory__c == null || acct.Territory__c == ''))
                {
                    findTerritories = true;
                    salesAreas.add(acct.Sales_Area__c);
                }
            }
            
            if (findTerritories)
            {
            	String searchKey;
            	for (Territory_Named_Account__c terrAssign : [SELECT Country_Code__c, Territory_Manager__c,
                            Market_Segment__c, Territory__c, Named_Account__c FROM Territory_Named_Account__c
                            WHERE Sales_Area__c IN :salesAreas])
                {
                    if (terrAssign.Named_Account__c != null)
                    {
                    	searchKey = terrAssign.Named_Account__c;
                    	terrAssignMap.put(searchKey,terrAssign);
                    }	
                }
            }        
        	
        	
            for (Account acct : scope) 
            {
                /*****************************************
                 *     New Territory Assignment Rules    *
                 *****************************************/  
                if (updateTerritory && !acct.IsExcludedFromRealign && (acct.Territory__c == null || acct.Territory__c == ''))                                             
                {  
                    /*************************************
                     *  Find Territory Named Account match  *
                     *************************************/
                    
                    Territory_Named_Account__c terrAssign = terrAssignMap.get(acct.Id);        
                                    
                    if (terrAssign != null)                                                             // Match found on Named Account
                    {
                        acct.Territory__c = terrAssign.Territory__c.toUpperCase();
                        if (terrAssign.Territory_Manager__c != null)
                        {
                            acct.OwnerId = terrAssign.Territory_Manager__c;
                        }
                        updates.add(acct);
                    }
                }
            }
            update updates;
        }
    }

    global void finish(Database.BatchableContext context) {
    }
}