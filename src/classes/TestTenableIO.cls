@isTest
public class TestTenableIO{
    /*********************************************************************************************
    *    - Create standard customer account
    *    - Create Partner account (rectype = Partner, Autorization Status = Authorized Partner 
    *        (set discounts)
    *    - Create Distributor account (rectype = Partner, Autorization Status = Authorized Distributor 
    *        (set discounts)
    *    - Create Opportunity on the customer account
    *    - Create Quote on the Opportunity, with Partner and Distributor
    *    - Create QuoteLine on the Quote for Tenable.IO with 250 assets
    *    - Submit Quote for approval (it should auto-approve
    *    - Execute quotes.UpdateOpp (oppLineItems should be created)
    *    - Submit Opportunity for approval
    *    - Approve Opportunity
    *    - Update BaseUnitPrice on OppLineItem
    *    - Create InstallBase with end datae and oppLineItemId from above 
    *        (verify that BaseUnitPrice gets pulled from OppLineItem)
    *    - Create new Opportunity and new Quote on customer account with Add-On Tenable.io product
    *        (verify that BaseUnitPrice and End Date is pulled from Install Base
    *
    ***********************************************************************************************/
    
    @isTest
    static void testTenableIo() {
        
        Utils.isTest = true;
        
        id standardRecord, partnerRecord;
        list<Account> newAccList = new list<Account>();
        list<RecordType> recType = [Select Id, SobjectType, Name From RecordType where SobjectType = 'Account'];
        system.debug(recType);
        for(RecordType r: recType){
            if(r.Name == 'Standard Account'){
                standardRecord = r.Id;
            }else if(r.Name == 'Partner Account'){
                partnerRecord = r.Id;
            }
        }
        
        Account customerAccount = new Account(Name='Testing1 Inc.', BillingState='CA', BillingCountry='United States', RecordTypeId = standardRecord); 
        newAccList.add(customerAccount);
        
        Account partnerAccount = new Account(Name='Testing1 Inc.', BillingState='CA', BillingCountry='United States', RecordTypeId = partnerRecord,
                                            Authorization_Status__c = 'Authorized Partner'); 
        newAccList.add(partnerAccount);
        
        Account distributorAccount = new Account(Name='Testing1 Inc.', BillingState='CA', BillingCountry='United States', RecordTypeId = partnerRecord,
                                                Authorization_Status__c = 'Authorized Distributor'); 
        newAccList.add(distributorAccount);
        
        insert newAccList;
        
        Opportunity testOpp = new Opportunity(name='Test Tenable.io Opp', accountid = customerAccount.id, StageName='Proposal Submitted', CloseDate=Date.newInstance(2017,10,10));
        insert testOpp;
        
        Product2 tenableio = new Product2(searchKey__c='Products:Tenable.io:AMER:Subscription:1:Tenable.io Vulnerability Management:', Unit__c='1', Unit_Sort_Order__c=1, isActive=true, Tier_Minimum__c=-999999999, Tier_Maximum__c=999999999, Term_Minimum__c=1, Term_Maximum__c=99, RevenueScheduleType='Divide', RevenueInstallmentPeriod='Monthly', Required_Services_Quantity__c=1, Required_Product_Minimum_Quantity__c=0, ProductCode='TIO-VM', Pricing_Term__c='Annual', Order_Category__c='New;Add-on;Renewal', NumberOfRevenueInstallments=12, Name='Tenable.io Vulnerability Management', License_Type__c='Subscription', Hidden__c=false, Geography__c='AMER', GSA_Price__c=4745.00, Family='Tenable.io', Description='Tenable.io Vulnerability Management', Default_Term__c=12, Category__c='Products', CanUseRevenueSchedule=true, Approval_Need_Pricing__c=false, Approval_Need_Finance__c=false, Unit_of_Measure__c = 'Assets', Deployment__c = 'Cloud');
        insert tenableio;
        id pbookId = Test.getStandardPricebookId();
        system.debug('PRICEBOOK ID ' + pbookId);
        Pricebook2 pb = new Pricebook2(isActive=true, Name = 'Standard Price Book', id=pbookId);
        upsert pb;
        system.debug('PRICEBOOK NAME ' + pb.Name);
        PricebookEntry pEntry = new PricebookEntry(Pricebook2Id = pbookId, Product2Id = tenableio.Id, IsActive = true, UnitPrice = 51);
        insert pEntry;
        Product2 tenableioBAD = new Product2(searchKey__c='BADPRODUCTSHOULDFAIL', Unit__c='1', Unit_Sort_Order__c=1, Tier_Minimum__c=-999999999, Tier_Maximum__c=999999999, Term_Minimum__c=1, Term_Maximum__c=99, RevenueScheduleType='Divide', RevenueInstallmentPeriod='Monthly', Required_Services_Quantity__c=1, Required_Product_Minimum_Quantity__c=0, ProductCode='TIO-VM0', Pricing_Term__c='Annual', Order_Category__c='New;Add-on;Renewal', NumberOfRevenueInstallments=12, Name='Tenable.io Vulnerability Management', License_Type__c='Subscription', Hidden__c=false, Geography__c='AMER', GSA_Price__c=4745.00, Family='Tenable.io', Description='Tenable.io Vulnerability Management', Default_Term__c=12, Category__c='Products', CanUseRevenueSchedule=true, Approval_Need_Pricing__c=false, Approval_Need_Finance__c=false, Unit_of_Measure__c = 'Assets', Deployment__c = 'Cloud');
        insert tenableioBAD;
        
        test.startTest();
        //Quote__c testQuote1 = new Quote__c(Distributor__c = distributorAccount.Id,Use_VAT_Rates__c=true, Use_Standard_Discounts__c=true, ShowListPrice__c=false, ShowDiscount__c=true, Primary__c=false, Presented_Date__c=date.newInstance(2017, 02, 09), Payment_Terms__c='Net 30 Days', Payment_Method__c='Invoice', Partner__c=partnerAccount.id, Opportunity__c=testOpp.id, Geography_Price_List__c='AMER', Exclude_Term_Dates__c=false, Discount__c=16.3344856359518416974609607819764, Days_Valid__c=30, Customer_Accepted__c=false, Approval_Stage__c='Draft');
        Quote__c testQuote1 = new Quote__c(Use_VAT_Rates__c=true, Use_Standard_Discounts__c=true, ShowListPrice__c=false, ShowDiscount__c=true, Primary__c=false, Presented_Date__c=date.newInstance(2017, 02, 09), Payment_Terms__c='Net 30 Days', Payment_Method__c='Invoice', Partner__c=partnerAccount.id, Opportunity__c=testOpp.id, Geography_Price_List__c='AMER', Exclude_Term_Dates__c=false, Discount__c=16.3344856359518416974609607819764, Days_Valid__c=30, Customer_Accepted__c=false, Approval_Stage__c='Draft');
        insert testQuote1;
        
        QuoteLine__c testQuoteLine0 = new QuoteLine__c(searchKey__c='BADPRODUCTSHOULDFAIL', Unit__c='1', Term_in_Months__c=12.000, Term__c=12.000, Taxable__c=false, Start_Date__c=date.newInstance(2017, 02, 08), Shipping__c=false, Quote__c=testQuote1.Id, Quantity__c=1, Product__c=tenableioBAD.Id, Product_SKU__c='TIO-VM', Product_Name__c='Tenable.io Vulnerability Management', Order_Type__c='New', List_Price__c=2.33066666666666666666666666666667, License_Type__c='Subscription', Family__c='Tenable.io', End_Date__c=date.newInstance(2017, 02, 22), Distributor_Discount__c=24.00000000000000, Discount__c=20.00000000000000, Description__c='Tenable.io Vulnerability Management', Category__c='Products', Number_of_Units__c=250, Unit_Type__c = 'Assets');
        //catching the assertion that is there is no sku
        try {
            insert testQuoteLine0;
        } catch (DmlException e) {
            //Assert Error Message
            system.debug('CCCCCCCC ' + e.getMessage());
            System.assert( e.getMessage().contains('Insert failed. First exception on row'),
                e.getMessage() );
        }
        
        //This is the quote line that should work
        QuoteLine__c testQuoteLine2 = new QuoteLine__c(searchKey__c='Products:Tenable.io:AMER:Subscription:1:Tenable.io Vulnerability Management:', Unit__c='1', Term_in_Months__c=12.000, Term__c=12.000, Taxable__c=false, Start_Date__c=date.newInstance(2017, 02, 08), Shipping__c=false, Quote__c=testQuote1.Id, Quantity__c=1, Product__c=tenableio.Id, Product_SKU__c='TIO-VM', Product_Name__c='Tenable.io Vulnerability Management', Order_Type__c='New', List_Price__c=2.33066666666666666666666666666667, License_Type__c='Subscription', Family__c='Tenable.io', End_Date__c=date.newInstance(2017, 02, 22), Distributor_Discount__c=24.00000000000000, Discount__c=20.00000000000000, Description__c='Tenable.io Vulnerability Management', Category__c='Products', Number_of_Units__c=250, Unit_Type__c = 'Assets');
        insert testQuoteLine2;
        
        //Submitting the quote for approval
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();                        // NetSuite Sales Order was rejected - re-submit for booking
        req.setComments('Testing to see if we can submit this quote for approval');
        req.setObjectId(testQuote1.Id);
        Approval.processResult result = Approval.process(req);
        System.debug('Submitted for quote:' + result.isSuccess());
        testQuote1.Approval_Stage__c = 'Approved';
          
        //updating the quotelines to the opportunity
        PageReference pageRef = Page.quote;
        Test.setCurrentPageReference(pageRef);
        ApexPages.currentPage().getParameters().put('id', testQuote1.id);
        ApexPages.StandardController sc = new ApexPages.standardController(testQuote1);
        quotes ext = new quotes(sc);
        ext.UpdateOpp();
        
        //submitting the opportunity for approval now that it has quote lines
        //Submitting the quote for approval
        testOpp.Purchase_Order_Number__c = '234232134';
        testOpp.Primary_Quote__c = testQuote1.Id;
        system.debug('ccccccc2: ' + testOpp.Primary_Quote_Approval_Status__c);
      /*  Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();                        // NetSuite Sales Order was rejected - re-submit for booking
        req1.setComments('Testing to see if we can submit this Opportunity for approval');
        req1.setObjectId(testOpp.Id);
        Approval.processResult result1 = Approval.process(req1);
        System.debug('Submitted Opp:' + result1.isSuccess());
        System.assert(result1.isSuccess());
        
        //Approving the request in sfdc
        List<Id> newWorkItemIds = result1.getNewWorkitemIds();
        Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
        req2.setComments('Approving request.');
        req2.setAction('Approve');
        req2.setWorkitemId(newWorkItemIds.get(0));

        // Submit the request for approval  
        Approval.ProcessResult result2 =  Approval.process(req2);
        System.assert(result2.isSuccess(), 'Result Status:'+result2.isSuccess());*/
        
        test.stopTest();
    }
    
     //going to do some testing get get better coverage on opportunity trigger handler
    @isTest (seeAllData=true)
    static void testInstallBaseTrigger(){
        
        boolean isSandBox = SalesforceToJIRAController.runningInASandbox();
        set<id> oppIds = new set<id>();
        
        if(isSandBox == true){
            oppIds.add('0066000001ph5p0');
        }else{
            oppIds.add('0066000001rE3eX');
        }
        Opportunity opp = [select id, AccountId from Opportunity where id =: oppIds];
        
        list<OpportunityLineItem> oliList = [select id, quantity, Number_of_Units__c, Unit_Type__c, totalprice, description, discount_percent__c, order_category__c,
                                            order_type__c, family__c, category__c, license__c, Product2Id, term__c, start_date__c, end_date__c, line_number__c,
                                            Base_Unit_Price__c from OpportunityLineItem where opportunityId =: opp.Id];
        
        list<Install_Base__c> IBList = new list<Install_Base__c>();
        Install_Base__c ib;
        //adding the install base to the record
        
        
        Test.startTest();
        system.debug('cccccccccc4: ' + oliList);
        integer i = 0;
        for(OpportunityLineItem oli: oliList){
            oli.Base_Unit_Price__c = 2.5;
            update oli;
            ib = new Install_Base__c(Name='TIO-VM '+i,Account__c=opp.AccountId, Assets__c=500, Host_Name__c='', Installed_Version__c='',
                                    Instance_ID__c=96773, Invoice_Number__c='NIV97135630'+i, Is_Primary__c=true, Licensed_Version__c='',
                                    OppLineItemId__c=oli.Id, Order_ID__c='97135630', Other_PO_Number__c='IO_TEST_11617_3', Product__c=oli.Product2Id,
                                    Purchase_Order__c='IO_TEST_11617_3', Serial_Number__c='171701300002'+i, Size__c=500, Maintenance_State__c='maintenance',
                                    Appliance_Serial_Number__c='', Appliance_Hardware_Serial_Number__c='', User_Label__c='', UUID__c='7862dd6d-b85b-4624-92ea-72db5dad9f06' + i);
            i++;
            IBList.add(ib);
            //insert ib;
            break;
        }
        system.debug('cccccccccc5: ' + IBList);
        try{
        insert IBList;
        }catch(DmlException e){
            System.assert( e.getMessage().contains('Insert failed'),
                e.getMessage() );
        
        }
        
        Test.stopTest();
    }
    
      //going to do some testing get get better coverage on opportunity trigger handler
    @isTest (seeAllData=true)
    static void testCreateNetsuiteSalesOrder(){
        
        boolean isSandBox = SalesforceToJIRAController.runningInASandbox();
        set<id> oppIds = new set<id>();
        
        if(isSandBox == true){
            oppIds.add('0066000001rE3eX');
        }else{
            oppIds.add('0066000001rE3eX');
        }
        
        Test.startTest();
        //testing the JSON parsing class
        string testJson = '{"transactionSource":"VAR_Portal","transactionDate":"2017-02-07","startDate":"2017-02-07","shipToTier":"1","reseller":"","quoteName":"Test Quote","purchaseOrderNumber":"2717_TEST_3","portalOrderNumber":"98989898","orderType":"1","memo":"Approved","location":"1","licenseeContact":"jguilford@tenable.com","companyName":"Awesome_Company2","items":[{"revenueRecognitionStartDate":"2017-02-07","revenueRecognitionEndDate":"2018-02-06","rate":"10263.80","quantity":1,"prebill":false,"orderCategory":"New","listRate":855.3166666666667,"item":"SERV-NESM-1024","externalId":"00kg0000009VWfmAAG","discountRate":0.24,"contractStartDate":"2017-02-07","contractItemTermMonths":12,"contractEndDate":"2018-02-06","assets":0},{"revenueRecognitionStartDate":"2017-02-07","revenueRecognitionEndDate":"2018-02-06","rate":"2496.60","quantity":1,"prebill":false,"orderCategory":"New","listRate":208.05,"item":"SERV-PVS-PRO","externalId":"00kg0000009VWfrAAG","discountRate":0.24,"contractStartDate":"2017-02-07","contractItemTermMonths":12,"contractEndDate":"2018-02-06","assets":0},{"revenueRecognitionStartDate":"2017-02-07","revenueRecognitionEndDate":"2018-02-06","rate":"12080.976264","quantity":1,"prebill":false,"orderCategory":"New","listRate":1006.748022,"item":"TIO-VM","externalId":"00kg0000009VWfwAAG","discountRate":0.24,"contractStartDate":"2017-02-07","contractItemTermMonths":12,"contractEndDate":"2018-02-06","assets":666}],"externalId":"006g000000Cof7SAAR","endUser":"98989898","endDate":"2018-02-06","distributor":"","currentUserEmail":"rjones@tenable.com","contractTerm":"12.0000","billToTier":"1","billingCustomer":"43138","addresses":[{"zip":"08873-4145","type":"billing","state":"NJ","country":"US","city":"Somerset","addressee":"Shi International Corp.","address1":"290 Davidson Ave"},{"zip":"21046","type":"shipping","state":"MD","country":"US","city":"Columbia","addressee":"Test Guilford Enterprises Tenable","address1":"7021 Columbia Gateway Drive"}]}';
        
        // Set mock callout class                                                       
        Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
        
        opportunityTriggerHandler.CreateNetSuiteSalesOrders(oppIds);        
        
        //opportunityTriggerHandler.postSalesOrder(testJson, '006g000000Con90',FALSE);
        Test.stopTest();
    }
    
}