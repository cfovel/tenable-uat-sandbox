public without sharing class AccountAlertsController {
	
	private final Account acct;
	
	public AccountAlertsController(ApexPages.StandardController stdController)
	{
		this.acct = (Account)stdController.getRecord();
	}
	
	public PageReference dismiss()
	{
		//reset alert box
		acct.displayAlert__c = false;
		update acct;
		
		//refresh page
		PageReference pageRef = ApexPages.currentPage();
		pageRef.setRedirect(true);
		return pageRef;
	}
}