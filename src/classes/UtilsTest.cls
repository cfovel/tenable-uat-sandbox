/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class UtilsTest {
	/*****************************************************************************************
	*   Test Class/Coverage for Utils
	******************************************************************************************/	
    @isTest
    static void testUtils() {
    	String str1 = 'test';
		List <String> searchkeys = new List <String> {'test', 'test1'};
		
		Integer testContains = Utils.SetContains(str1, searchkeys);
		System.assertEquals(testContains, 0);
		
		Boolean testStarts = Utils.SetStartsWith(str1, searchkeys);
		System.assertEquals(testStarts, true);
			
    	String testPhone = Utils.fixphone('12345');
    	System.assertEquals(testPhone, 'fail');
    	
    	String testFormatPercent = Utils.formatPercent(.05);
    	System.assertEquals(testFormatPercent, '5.00%');
    	
    	String testShortCurr = Utils.shortCurrency(5000);
    	System.assertEquals(testShortCurr, '$5K');
    	
    	String testDateMMMMddyyyy = Utils.formatDateMMMMddyyyy(Date.newInstance(2015, 06, 17));
    	System.assertEquals(testDateMMMMddyyyy, 'June 17, 2015');
    	
    	String testDateMMddyyyy = Utils.formatDateMMddyyyy(Date.newInstance(2015, 06, 17));
    	System.assertEquals(testDateMMddyyyy, '06/17/2015');
    	
    	String testDateddMMyyyy = Utils.formatDateddMMyyyy(Date.newInstance(2015, 06, 17));
    	System.assertEquals(testDateddMMyyyy, '17/06/2015');
    	
    	Boolean testThisQuarter = Utils.thisQuarter(Date.today());
    	System.assertEquals(testThisQuarter, true);
    	
    	Boolean testThisMonth = Utils.thisMonth(Date.today());
    	System.assertEquals(testThisMonth, true);
    }

    @isTest(seeAllData=true)
    static void testDuplicatePartners() {
        // create test data
        Id rectypeid = ([SELECT Id From RecordType WHERE Name='Partner Account']).Id;	
        Utils.isTest = true;
		accountTriggerHandler.enforceCountry = false;
				
	    Account a = new Account(Name='Test1 Inc.', BillingState='CA', BillingCountry='US', website='http://www.test1.com');
	    insert a;
	    
	    Account partnerAccount = new Account(Name='Partner Test', BillingState='CA', BillingCountry='United States', recordTypeId = rectypeid); 
        insert partnerAccount;
	    
        Opportunity o = new Opportunity(name='test', accountid = a.id, StageName='Proposal Submitted', CloseDate=Date.newInstance(2015,10,10));
		insert o;
		
		OppPartner__c op1 = new OppPartner__c(Opportunity__c = o.Id, Partner__c = partnerAccount.Id);
		insert op1;     
		
		OppPartner__c op2 = new OppPartner__c(opportunity__c = o.id, partner__c =  partnerAccount.Id, 
										primary_partner__c = TRUE, partner_role__c = 'VAR/Reseller');	
		Utils.isDuplicatePartner(op2);
		
		List<OppPartner__c> opList = [SELECT Id, Primary_Partner__c, Partner_Role__c FROM OppPartner__c 
										WHERE Opportunity__c = :o.Id AND Partner__c = :partnerAccount.Id];
		
		System.assert(opList.size() == 1);
		System.assert(opList[0].Primary_Partner__c == true);
		System.assert(opList[0].Partner_Role__c == 'VAR/Reseller');

    }
}