@isTest
public class TestIntegration {
	
	static SalesforceToJIRAController testSFDCJIRA;

    @isTest
    public static void testPostSalesOrder() {    
        // create test data - Opportunity, Quote, Quote Line Items
        
        Test.startTest();
        
        // Set mock callout class                                                       
        Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
        
        // Test functionality - submitApproval - quote, UpdateOpp, submitApproval - opp, approve, 
        Test.stopTest();
    }
    
    @isTest
    private static Case[] salesforceToJIRAData(){
    	
    	Utils.isTest = true;
    	
    	Account a = new Account(Name='Dell SecureWorks', BillingState='CA', BillingCountry='US', website='http://www.test1.com');
        insert a;

        Contact c = new Contact(lastname='test', accountid=a.id, 
        	email='test@test.com', MailingCountry='US', MailingState='CA'); 
        insert c;
        
        Product2[] prodList = new Product2[0];
        prodList.add(new Product2(Name = 'TestProd', ProductCode='SERV-MSSP-VM-M', Family = 'Test', Category__c = 'test', Order_Category__c = 'Test', Pricing_Term__c = 'Net 30'));
        prodList.add(new Product2(Name = 'TestProd1', ProductCode='SERV-MSSP-WAS-M', Family = 'Test', Category__c = 'test', Order_Category__c = 'Test', Pricing_Term__c = 'Net 30'));
        prodList.add(new Product2(Name = 'TestProd2', ProductCode='SERV-MSSP-PCI-M', Family = 'Test', Category__c = 'test', Order_Category__c = 'Test', Pricing_Term__c = 'Net 30'));
        insert prodList;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry[] pbeList = new PriceBookEntry[0];
        pbeList.add(new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prodList[0].Id, UnitPrice = 100.00, IsActive = true));
        pbeList.add(new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prodList[1].Id, UnitPrice = 100.00, IsActive = true));
        pbeList.add(new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prodList[2].Id, UnitPrice = 100.00, IsActive = true));
        insert pbeList;
    	
    	Case[] testCases = new Case[0];
    	testCases.add(new Case(Priority='P3 - Medium', Subject='Testing JIRA Case', Description='More Testing JIRA', Status='On Hold', AccountId=a.Id, Product__c='SecurityCenter')); 
		testCases.add(new Case(Subject='Testing JIRA Case', Description='More Testing JIRA'));
		testCases.add(new Case(Priority='P3 - Medium', Subject='Testing JIRA Case', Description='More Testing JIRA', Status='On Hold', AccountId=a.Id, Type = 'Test'));
		testCases.add(new Case(Priority='P3 - Medium', Subject='Testing JIRA Case', Description='More Testing JIRA', Status='On Hold', AccountId=a.Id, Type = 'Test', Jira_Case_Number__c = 'CSDEV-44',Product__c = 'Channel Partner')); 
		testCases.add(new Case(Priority='P3 - Medium', Subject='Testing JIRA Case', Description='More Testing JIRA', Status='On Hold', AccountId=a.Id, Type = 'Test', Product__c = 'Nessus Cloud', Jira_Type__c = 'Query'));
		insert testCases;
		
		return testCases;
    }
    
    /*In this method we will test the functionality of the created Jira button on the salesforce case*/
    private static void testCreatJiraButton(Case cas) {
    	
        PageReference pref = Page.SalesforceToJIRAButton;
	    pref.getParameters().put('id', cas.id);
	    Test.setCurrentPage(pref);
	
	    ApexPages.StandardController sc = new ApexPages.StandardController(cas);
	
	    testSFDCJIRA = new SalesforceToJIRAController(sc);
	    
	    map<string, string> resultMap = new map<string, string>();
	    resultMap = SalesforceToJIRAController.createJiraTicket(cas.Id);
	    
	    //Updating the Jira Ticket number so that we can trigger the case trigger
	    cas.JIRA_Case_Number__c = 'CSDEV-34';
	    update cas;
	    
	    //testing the updateCaseFields method
	    boolean updateSucess = false;
	    updateSucess = SalesforceToJIRAController.updateCaseFields(cas.Id, 'Subject', 'We are changing the subject');
	    System.assertEquals(true, updateSucess);
	    
	    //testing the checkExistingRecord
	    boolean checkSucess = false;
	    checkSucess = SalesforceToJIRAController.checkExistingRecord(cas.Id);
	    System.assertEquals(true, updateSucess);
	    
	    CaseComment com = new CaseComment(CommentBody = '#jira Tesitng the Comment', ParentId = cas.Id);
	    insert com;
	
		//testSFDCJIRA.ButtonPress();
    }
    
     @isTest
    static void testCase1() {
    	
    	Case[] testCases = salesforceToJIRAData();
    	
    	Test.startTest();
    	
    	// Set mock callout class                                                       
        Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
	    testCreatJiraButton(testCases[0]);
	    
	    Test.stopTest();
  	}
    @isTest
    static void testCase2() {
    	
    	Case[] testCases = salesforceToJIRAData();
    	
	    Test.startTest();
    	
    	// Set mock callout class                                                       
        Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
	    testCreatJiraButton(testCases[1]);
	    
	    Test.stopTest();
  	}
  	@isTest
    static void testCase3() {
    	
    	Case[] testCases = salesforceToJIRAData();
    	
	    Test.startTest();
    	
    	// Set mock callout class                                                       
        Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
	    testCreatJiraButton(testCases[2]);
	    
	    Test.stopTest();
  	}
  	@isTest
    static void testCase4() {
    	
    	Case[] testCases = salesforceToJIRAData();
    	
	    Test.startTest();
    	
    	// Set mock callout class                                                       
        Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
	    testCreatJiraButton(testCases[3]);
	    
	    Test.stopTest();
  	}
  	@isTest
    static void testCase5() {
    	
    	Case[] testCases = salesforceToJIRAData();
    	
	    Test.startTest();
    	
    	// Set mock callout class                                                       
        Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
	    testCreatJiraButton(testCases[4]);
	    
	    Test.stopTest();
  	}
  	
  	//Here we are testing the evaluation trigger
  	@isTest
  	static void testEvaluationTrigger(){
  		
  		Utils.isTest = true;
    	
    	Account a = new Account(Name='Dell SecureWorks', BillingState='CA', BillingCountry='US', website='http://www.test1.com');
        insert a;

        Contact c = new Contact(lastname='test', accountid=a.id, 
        	email='test@test.com', MailingCountry='US', MailingState='CA'); 
        insert c;
  		
  		Evaluation__c conEval = new Evaluation__c(
    			Status__c               = '', 
    			Name                    = 'Tenable.io rjones@tenable.com', 
    			Marketo_Lead_Id__c      = '923142', 
    			Location_Data__c        = '',
    			Expiration_Date__c      = date.newInstance(2012,12,22), 
    			Eval_Type__c            = 'Tenable.io', 
    			Eval_Request_Type__c    = 'New', 
    			Eval_Request_Id__c      = '', 
    			Eval_Request_Date__c    = datetime.newInstance(2012,09,22,0,0,0), 
    			Eval_Product__c         = 'Tenable.io', 
    			Email__c                = 'rjones@tenable.com',
    			Date_Activation_Sent__c = date.newInstance(2012,12,22), 
    			Container_uuid__c       = '23432-5235324-21234-23234', 
    			Contact__c              = c.Id, 
    			Activation_URI__c       = 'asdfasfasfasdfasdfasdfasdfasdf', 
    			Activation_DateTime__c  = datetime.newInstance(2012,09,22,0,0,0)
    		);
    		
    		Test.startTest();
    			insert conEval;
    		Test.stopTest();
  	}
  	
    //Testing User creation and updating user record with auth0 Credentials
    @isTest
    static void testAuth0UserCreation(){
        
        Utils.isTest = true;
        
        Account a = new Account(Name='Dell SecureWorks', BillingState='CA', BillingCountry='US', website='http://www.test1.com');
        insert a;

        Contact c = new Contact(lastname='test', accountid=a.id, 
        email='test@test.com', MailingCountry='US', MailingState='CA'); 
        insert c;
        
        profile p = [select id from profile where name = 'Tenable - Customer Community Super User'];
        
        User u = new User(lastname = 'Test', firstname = 'New', username = 'testing@test.com', Alias = 'ttest',
                         IsActive = true, email = 'testing@test.com', profileId = p.Id, contactId = c.id,
                         EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US', LocaleSidKey='en_US', TimeZoneSidKey='America/Los_Angeles');
        
		Test.startTest();
        		Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
    			insert u;
    	Test.stopTest();
    }
    
 
}