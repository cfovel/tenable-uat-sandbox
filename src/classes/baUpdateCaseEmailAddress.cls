global class baUpdateCaseEmailAddress implements Database.Batchable<SObject> {
    // To run, login as Automation User, open Execute Anonymous window, and execut:
    //      database.executeBatch(new baUpdateCaseEmailAddress(),200);
    
    public static final String TRAINING_RECORD_TYPE_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Training Case').getRecordTypeId();  


    global Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator([select Id, SuppliedEmail, Customer_CC_Emails__c, Internal_CC_Emails__c, Priority, RecordTypeId from Case 
            WHERE (SuppliedEmail Like '%@%.%' and (NOT SuppliedEmail Like '%@example.com')) OR Customer_CC_Emails__c <> null OR Internal_CC_Emails__c <> null]);
    }

    global void execute(Database.BatchableContext context, List<Case> scope) {
                for (Case c : scope) 
        {
            if (c.suppliedEmail != null && !c.suppliedEmail.endsWith('example.com'))
            {   
                c.suppliedEmail = Utils.scrambleEmailAddress(c.suppliedEmail);
            }
            
            if (c.Customer_CC_Emails__c != null)
            {
                c.Customer_CC_Emails__c = null;
            }
            
            if (c.Internal_CC_Emails__c != null)
            {
                c.Internal_CC_Emails__c = null;
            }
            
            if (c.RecordTypeId == TRAINING_RECORD_TYPE_ID && c.Priority == 'P5 - New')
            {
                c.Priority = 'P4 - Minor';
            }    
            
        }
        update scope;
        /*
        Database.SaveResult[] srList  = database.update(scope, false);
        
        // Iterate through each returned result
        for (Database.SaveResult sr : srList) 
        {
            if (!sr.isSuccess()) 
            {
                System.debug('@@@ Error on update CaseId: ' + sr.getId());
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    System.debug('@@@ The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Case fields that affected this error: ' + err.getFields());
                }
            }
        }
        */
    }

    global void finish(Database.BatchableContext context) {
    }
}