@isTest
class PrepareSandboxTest {

    @isTest
    static void testSandboxPrep() {
        Utils.isTest = true;
        accountTriggerHandler.enforceCountry = false;
        
        Account a = new Account(Name='Test1 Inc.', BillingState='CA', BillingCountry='United States'); 
        insert a;

        Contact c = new Contact(lastname='test', accountid=a.id, 
            email='test@contact.com', MailingCountry='US', MailingState='CA'); 
        insert c;
        
        Lead l = new Lead(Company='Partnert Inc.', phone='211-211-1234', fax='211-211-1234', FirstName='Bob', LastName='Smith', Status='Not Contacted', email = 'test@lead.com');
        insert l;
        
        Case cs = new Case(contactid=c.id, accountid=a.id, subject='Test', origin='Email', suppliedEmail = 'test@case.com');
        insert cs;

        Test.startTest();

        Test.testSandboxPostCopyScript(
            new PrepareSandbox(), UserInfo.getOrganizationId(),
                UserInfo.getOrganizationId(), UserInfo.getOrganizationName());

        Test.stopTest();
        
        c = [SELECT Email from Contact WHERE Id = :c.Id];
        l = [SELECT Email from Lead WHERE Id = :l.Id];
        cs = [SELECT SuppliedEmail from Case WHERE Id = :cs.Id];
        
        system.assertEquals('test=contact@example.com', c.Email);
        system.assertEquals('test=lead@example.com', l.Email);
        system.assertEquals('test=case@example.com', cs.SuppliedEmail);
        
        
    }
}