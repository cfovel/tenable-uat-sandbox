public class overrideAssignmentRequestController
{
    private final Account acct;
    public OverrideAssignment__c overrideAssgn;
    public Id acctId;
    //public Map <String, String> params = new Map <String, String>(); 
    public Boolean pageError { get; set; } 
    public List<OverrideAssignment__c> overrideAssgns = new List<OverrideAssignment__c>();
    private User usr = new User();
       
    
    public overrideAssignmentRequestController(ApexPages.StandardController stdController)
    {
        //params = ApexPages.currentPage().getParameters();
        usr = ([SELECT Id, PrimaryTerritory__c FROM User WHERE Id = :Userinfo.getUserId()]);      
        pageError = FALSE;
        this.acct = (Account)stdController.getRecord();
        this.overrideAssgn = new OverrideAssignment__c(account__c = acct.Id, Approved__c = False, RequestedBy__c = UserInfo.getUserId(), 
                            RequestedAt__c = DateTime.now());       //  , Territory__c = usr.PrimaryTerritory__c);
    }
    
    public OverrideAssignment__c getOverrideAssgn ()
    {
        return overrideAssgn;
    }
    
    public PageReference save() 
    {
        DateTime timeNow = Datetime.now();
        
        if (overrideAssgn.startAt__c == null)
        {   
            overrideAssgn.startAt__c = timeNow;
        }
        
        try 
        {
                insert overrideAssgn;                                                                      
                
        }
        catch (exception e) {
            Utils.addError('Unable to request Named Account: '+e.getMessage());                         // Report error back to the page
            return null;                                                                        // Make sure we don't refresh the page if there's an error
        }
        
        
        Approval.ProcessSubmitRequest reqSubmit = new Approval.ProcessSubmitRequest();
        
        reqSubmit.setObjectId(overrideAssgn.Id);
        
        Approval.ProcessResult reqResult = Approval.process(reqSubmit); 
        
        return (new ApexPages.StandardController(acct)).view(); 
    }    
}