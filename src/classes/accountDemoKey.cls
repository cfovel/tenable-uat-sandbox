public class accountDemoKey{
    public Account a;
    public String AccountId;
    public Boolean hasCustomPermission {get; set;}
    
    public accountDemoKey(ApexPages.StandardController controller){
        this.a = (Account)controller.getRecord();
    }
    
    public PageReference demoKeyLoad(){
        AccountId = ApexPages.currentPage().getParameters().get('id');
        PageReference page;
        hasCustomPermission = FeatureManagement.checkPermission('Generate_Demo_Key_Permission');
        system.debug('@@@ hasCustomPermission: ' + hasCustomPermission);
        if (!hasCustomPermission)
        {
        	ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'You do not have permission to perform this function'));
        	return null;
        }

        
        page = new PageReference('/apex/generateDemoKey?accountId=' + AccountId);
        page.setRedirect(true);
        system.debug('Roy8: ' + page);
        return page;
    }  
    
    public PageReference cancel(){
    	return new PageReference('/' + AccountId);
    }
}