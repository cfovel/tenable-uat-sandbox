global class BatchTerritoryAttributionAccount implements Database.Batchable<sObject>, Database.Stateful {
    
    global Set<Id> territoryIds;
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        territoryIds = new Map<Id, Territory__c>([SELECT Id FROM Territory__c WHERE ChangeAttributionAccount__c = true]).keySet();
        return Database.getQueryLocator([SELECT Id FROM Account WHERE TerritoryLookup__c IN :territoryIds]);
    }
    
    global void execute(Database.BatchableContext bc, List<Account> accounts) {
        
        Map<Id, Account> accountMap = new Map<Id, Account>([SELECT Id, TerritoryLookup__r.TerritoryOwner__c, TerritoryLookup__r.Name, TerritoryLookup__r.RegionName__c, TerritoryLookup__r.AreaName__c, TerritoryLookup__r.TheaterName__c, TerritoryLookup__r.SegmentName__c FROM Account WHERE Id IN :accounts]);
        
        if (!accounts.isEmpty()) {
            for (Account acc : accounts) {
                acc.OwnerId = accountMap.get(acc.Id).TerritoryLookup__r.TerritoryOwner__c;
                acc.TerritoryName__c = accountMap.get(acc.Id).TerritoryLookup__r.Name;
                acc.RegionName__c = accountMap.get(acc.Id).TerritoryLookup__r.RegionName__c;
                acc.AreaName__c = accountMap.get(acc.Id).TerritoryLookup__r.AreaName__c;
                acc.TheaterName__c = accountMap.get(acc.Id).TerritoryLookup__r.TheaterName__c;
                acc.SegmentName__c = accountMap.get(acc.Id).TerritoryLookup__r.SegmentName__c;
            }
            update accounts;
        }
         
    }
    
    global void finish(Database.BatchableContext bc) {
        
        if (territoryIds != null && !territoryIds.isEmpty()) {
            
            List<Territory__c> territoriesUpdated = new List<Territory__c>();
            for (Id territoryId : territoryIds) {
                territoriesUpdated.add(new Territory__c(
                    Id = territoryId,
                    ChangeAttributionAccount__c = false
                ));
            }
            if (!territoriesUpdated.isEmpty())
                update territoriesUpdated;
            
        }
        
    }
    
}