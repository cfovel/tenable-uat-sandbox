global class TerritoryAssignmentPreviewRefresh implements Schedulable {
    /*************************************************************************************
       To run EVERY 15 MINUTES, login as Automation User, open Execute Anonymous window, and execute:     
       TerritoryAssignmentPreviewRefresh sch = new TerritoryAssignmentPreviewRefresh();
       system.schedule('Batch Territory Assignment Preview Refresh - job1', '0 10 * * * ?', sch);  
       system.schedule('Batch Territory Assignment Preview Refresh - job2', '0 25 * * * ?', sch);  
       system.schedule('Batch Territory Assignment Preview Refresh - job3', '0 40 * * * ?', sch);  
       system.schedule('Batch Territory Assignment Preview Refresh - job4', '0 55 * * * ?', sch);                                 
     *************************************************************************************/
    
    global void execute(SchedulableContext sc) {
        
        Database.executeBatch(new BatchTerritoryAssignmentPreviewRefresh(), 200);
        
    }
   
}