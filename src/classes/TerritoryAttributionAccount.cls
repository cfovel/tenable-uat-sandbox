global class TerritoryAttributionAccount implements Schedulable {
    /*************************************************************************************
       To run HOURLY, login as Automation User, open Execute Anonymous window, and execute:     
       String CRON_EXP = '0 12 * * * ?';
       TerritoryAttributionAccount sch = new TerritoryAttributionAccount();
       system.schedule('Hourly Batch Territory Attribution Account job', CRON_EXP, sch);                                 
     *************************************************************************************/
     
    global void execute(SchedulableContext sc) {
        
        Database.executeBatch(new BatchTerritoryAttributionAccount(), 200);
        
    }
   
}