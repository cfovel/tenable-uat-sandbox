global class baUpdateTerritoryAssignments  {	/* implements Database.Batchable<SObject> {
    // To run, *** login as Tenable System Admin User ***, open Execute Anonymous window, and execute:
    //      database.executeBatch(new baUpdateTerritoryAssignments(new List<String>{'APAC', 'LATAM', 'AMER', 'EMEA'}),200);
    //  To run with ANY Sales Area, use:
    //      database.executeBatch(new baUpdateTerritoryAssignments(),200);


    global String SalesAreaCriteria;
    global List<String> SalesAreas;
    
    global baUpdateTerritoryAssignments(List<String> saList)
    {
        SalesAreas = saList;
        SalesAreaCriteria = ' AND Sales_Area__c IN :SalesAreas';
        
    }
        
    global baUpdateTerritoryAssignments()
    {
        SalesAreaCriteria = '';
    }
            
    global Database.QueryLocator start(Database.BatchableContext context) {
        String Query = 'SELECT Id, Name, OwnerId, Territory__c, Sales_Area__c, Type, RecordTypeId, NumberOfEmployees, BillingCountry, BillingCity, BillingPostalCode, ';
        Query += 'BillingState, ShippingCountry, ShippingCity, ShippingPostalCode, ShippingState, Site, ParentId, isExcludedFromRealign, DandBCompanyId, NAICSCode, Industry ';
        Query += 'FROM Account WHERE IsExcludedFromRealign = false AND Territory__c = null';
        Query += SalesAreaCriteria;
        system.debug('@@@ query: ' + Query);
        return Database.getQueryLocator(Query);
    }

    global void execute(Database.BatchableContext context, List<Account> scope) {
        List<Account> updates = TerritoryAssignmentUpdate.updateTerritoryAssignments(scope);
        
        if (!updates.isEmpty())
        {
            update updates;
        }
    }

    global void finish(Database.BatchableContext context) {
    }
    */
}