/**
 * Created by lucassoderstrum on 5/24/18.
 */

global class Tenable_CheckFirstLogin implements Database.Batchable<sObject> {

    global Database.QueryLocator start(Database.BatchableContext BC) {
        String prof = 'Tenable - Customer Community Standard User';
        String query;
        if (Test.isRunningTest()) {
            query = 'SELECT Id, LastLoginDate, First_Login_Date__c, (SELECT NetworkId FROM NetworkMemberUsers) FROM User WHERE isActive = true AND First_Login_Date__c=Null AND Contact.Customer_Community_User__c = True limit 10';
        } else {
            query = 'SELECT Id, LastLoginDate, First_Login_Date__c, (SELECT NetworkId FROM NetworkMemberUsers) FROM User WHERE LastLoginDate !=Null AND isActive = true AND First_Login_Date__c=Null AND Contact.Customer_Community_User__c = True';
        }
        return Database.getQueryLocator(query);
    }

        global void execute(Database.BatchableContext BC, List <User> scope) {
            String message = [
                    Select Initial_Message__c
                    FROM Community_Chatter_Messages__mdt
            ].Initial_Message__c;
            if (message != null) {
                Id networkId = [
                        SELECT ID
                        FROM Network
                        WHERE Name = 'Tenable Community'
                ].Id;
                
                list <User> usersToUpdate = new list <User>();
                list <FeedItem> posts = new list <FeedItem>();
                for (User us:scope) {
                    for(NetworkMember networkMember : us.NetworkMemberUsers){
                        if(networkMember.NetworkId == networkId){
                            FeedItem post = new FeedItem(Body = message, NetworkScope = networkId, parentId = us.Id);
                            posts.add(post);
                        }
                    }
                    us.First_Login_Date__c = system.today();
                    usersToUpdate.add(us);
                }
                system.debug('posts: ' + posts);
                insert posts;
                update usersToUpdate;
            }
        }

    global void finish(Database.BatchableContext BC)
    {
        system.debug('Batch complete!');
    }

}