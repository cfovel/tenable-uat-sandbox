public class territoryAssignmentsApplyController
{
    private final Territory__c terr;
    public Boolean pageError { get; set; } 
    public List<Account> accounts { get; set; }
    public DateTime now = DateTime.now();

    public territoryAssignmentsApplyController(ApexPages.StandardController stdController)
    {
        //params = ApexPages.currentPage().getParameters();   
        pageError = FALSE;

        Profile p = [select name from Profile where id = :UserInfo.getProfileId()];
        if (p.name == null || (p.name !='System Administrator' && p.name !='Tenable - System Administrator' && p.name != 'Tenable - Sales Ops User'))
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Preview Territory Assignments is only available to Sales Ops users.'));
            pageError = TRUE;
        }
        else
        {
            this.terr = (Territory__c)stdController.getRecord();
        }     
        
        List<Id> accIds = new List<Id>();
        for (TerritoryAssignment__c territoryAssignment : [SELECT Id, Account__c FROM TerritoryAssignment__c WHERE RecordType.DeveloperName = 'TerritoryAssignmentRule' AND Territory__c = :terr.Id AND (Preview__c = true OR (StartAt__c <= :now AND (EndAt__c = null OR EndAt__c > :now)))]) {
            if (territoryAssignment.Account__c != null)
                accIds.add(territoryAssignment.Account__c);
        }
        
        accounts = [SELECT Id, name, TerritoryLookup__c FROM Account WHERE Id IN: accIds];
             
    }
    
    public PageReference apply() 
    {
        TerritoryAssignment assignment = new TerritoryAssignment();
        
        assignment.start();
        assignment.setTerritoryAssignment(terr, true);
        assignment.finish();
        
        TerritoryAssignment.setAccountTerritory(accounts, false);
        
        return (new ApexPages.StandardController(terr)).view(); 
    }
}