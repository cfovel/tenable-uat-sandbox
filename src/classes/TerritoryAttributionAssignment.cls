global class TerritoryAttributionAssignment implements Schedulable {
    /*************************************************************************************
       To run HOURLY, login as Automation User, open Execute Anonymous window, and execute:     
       String CRON_EXP = '0 15 * * * ?';
       TerritoryAttributionAssignment sch = new TerritoryAttributionAssignment();
       system.schedule('Hourly Batch Territory Attribution Assignment job', CRON_EXP, sch);                                 
     *************************************************************************************/
     
    global void execute(SchedulableContext sc) {
        
        Database.executeBatch(new BatchTerritoryAttributionAssignment(), 200);
        
    }
   
}