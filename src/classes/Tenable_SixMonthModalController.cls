public with sharing class Tenable_SixMonthModalController {

    @auraEnabled
    public static User getUserRecord(){
        system.debug('test user');
        User currentUser = [SELECT Display_6_Month_Modal__c, FirstName, LastName, Onboarding_Complete__c
        FROM User
        WHERE Id = :userinfo.getuserId() limit 1];

        return currentUser;
    }

    @auraEnabled
    public static void finishModal(User communityUser){
        communityUser.Display_6_Month_Modal__c = false;
        update communityUser;
    }

    @auraEnabled
    public static String getGroupUrl(){
        Id communityNetworkId = [SELECT Id
                                 FROM Network
                                 WHERE Name = 'Tenable Community'].Id;

        List<CollaborationGroup> groups = [Select Id
                                    FROM CollaborationGroup
                                    WHERE Name = 'Community Corner' AND
                                    NetworkId = :communityNetworkId];
        if(groups != null && groups.size() > 0){
            String grpId = groups[0].Id;
            return grpId;
        }
        return null;
    }
}