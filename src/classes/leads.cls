public with sharing class leads {
/******************************************************************************
*
*	Custom Controller for Custom Lead Page Layout
*	Copyright 2006-2014 (c) by CloudLogistix.com
*	All Rights Reserved, Modifications can only be made for your direct use and 
*	not for distribution without explicit permission from CloudLogistix.
*
********************************************************************************/
	/*****************************************************************************************
	*	Variable Initialization
	******************************************************************************************/
	public Id pageid = ApexPages.currentPage().getParameters().get('id');							// Store Lead ID
	public Boolean pageError = FALSE;																// Store Page Load Error
	public Lead entry {get; set;}																	// Store Lead Record
	public Boolean editmode {get; set;}
	public Boolean newlead;
	private final ApexPages.standardController stdController;										// Set-up Controller Class

	/*****************************************************************************************
	*	Custom Controller Initialization
	******************************************************************************************/
    public leads(ApexPages.StandardController stdController) {										// Controller Set-up Method
        Lead entry = (Lead)stdController.getRecord();												// First get record from Page Call
        this.stdController = stdController;															// Set-up Controller
		init();																						// Call init to load all relevant data
	}
	
	public void init() {																			// Page Initialization
		editmode = false;																			// set initial mode as view
		if (ApexPages.currentPage().getParameters() != null) {	 									// Make sure we have parameters
			if ((pageid != null) &&																	// ... and a lead id
			(ApexPages.currentPage().getParameters().get('id') != null) && 							// ... and the lead id isn't blank
			(ApexPages.currentPage().getParameters().get('id') != '')) {							// ... or empty
				newlead = false;
	 			loadLead();																			// Load Lead Record
			}
		}
		else {																						// Oops, problem with the URL
			pageError = TRUE;																		// Return an error to the VF Page
//			Utils.addError('Problem with URL, You have attempted to load a lead without the correct parameters.');
		}
		if (entry == null) {editmode = true;}
	}

	/*****************************************************************************************
	*	Getter/Setter's to return values to VF Page(s)
	******************************************************************************************/        
	public Boolean getPageError() {return pageError;}												// Return that we had an error to the VF Page

	public Lead getLead() {return this.entry;}														// Return Lead record to VF Page
	public void loadLead() {																		// Called by Controller init to get Lead Record
		String querystring;
		querystring = 'SELECT id,Website,Title,SystemModstamp,Street,Status,State,Salutation,';		// Standard Fields
		querystring += 'RecordTypeId,RecordType.Name,PostalCode,Phone,OwnerId,NumberOfEmployees,';
		querystring += 'Name,MobilePhone,MasterRecordId,LeadSource,LastName,';
		querystring += 'LastModifiedDate,LastModifiedById,LastActivityDate,IsUnreadByOwner,';
		querystring += 'IsDeleted,IsConverted,Industry,HasOptedOutOfEmail,FirstName,Fax,';
		querystring += 'EmailBouncedReason,EmailBouncedDate,Email,DoNotCall,Description,';
		querystring += 'CreatedDate,CreatedById,Country,Company,City,AnnualRevenue ';
		querystring += 'FROM Lead ';
		querystring += 'WHERE id = \''+pageid+'\'';
		entry = Database.query(querystring);
		this.leadstatus = entry.status;
	}

	/********************************************************
	*   Page Reference Functions
	********************************************************/
	public PageReference edit() {
		editmode = true;
        return null;	
	}

	public PageReference submitApproval() {
		try {
			PageReference newPage = new PageReference('/'+entry.id);
			Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
			req1.setObjectId(entry.id);
			Approval.ProcessResult result = Approval.process(req1);
			return newPage;
		}
		catch(exception e) {
           ApexPages.addMessages(e);
           return null;
       }
	}
	
	/*****************************************************************************************
	*	Dynamic Picklist Construction
	******************************************************************************************/
	public String leadstatus;
	public String getLeadStatus() { return this.leadstatus; }
	public void setLeadStatus(String s) { this.leadstatus = s; }
	public List<SelectOption> getLeadStatuses() {
		List<SelectOption> optionList = new List<SelectOption>();
		optionList.add(new SelectOption('Not Contacted','Not Contacted'));
		optionList.add(new SelectOption('Contact Attempted','Not Contacted'));
		optionList.add(new SelectOption('Contacted','Contacted'));
		return optionList;
	}

	//@isTest (seeAllData=TRUE)
	@isTest
    static void testLeadPage() {
		//LeadHistory ldid = ([SELECT Lead.IsConverted, Lead.Id, LeadId 
		//	FROM LeadHistory WHERE Lead.IsConverted=FALSE LIMIT 1]);
		//Lead lead = ([SELECT id FROM Lead WHERE id=:ldid.leadid]);
		Utils.isTest = true;
        leadTriggerHandler.enforceCountry = false;
		Lead lead = new Lead(Company='Test Inc.', phone='555-555-1234', 
        	fax='555-555-1234', FirstName='Bobby', LastName='Jones', 
        	Industry='Accessories', email='bjonestest@test.com', 
        	HasOptedOutOfEmail = true);
        insert lead;

        PageReference pageRef = Page.lead;
		Test.setCurrentPage(pageRef);        
        System.currentPageReference().getParameters().put('id',lead.id);
        ApexPages.StandardController sc = new ApexPages.standardController(lead);
        leads ext = new leads(sc);
		ext.getLead();
		ext.getPageError();
		ext.getLead();
		ext.edit();
		ext.setLeadStatus(ext.getLeadStatus());
		ext.getLeadStatuses();
		ext.submitApproval();

        Lead ld = new Lead(Company='Test Inc.', phone='211-211-1234', 
        	fax='211-211-1234', FirstName='Bob', LastName='Smith', 
        	Industry='Accessories', email='test@test.com', 
        	HasOptedOutOfEmail = true);
        insert ld;
        PageReference pageRef2 = Page.lead;
		Test.setCurrentPage(pageRef2);        
        ApexPages.StandardController sc2 = new ApexPages.standardController(ld);
        leads ext2 = new leads(sc2);
		ext2.init();
        System.currentPageReference().getParameters().put('id',ld.id);
		ext2.init();
		ext2.getPageError();
    }

}