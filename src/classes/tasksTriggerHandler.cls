public without sharing class tasksTriggerHandler {
/*******************************************************************************
*
* Task Trigger Handler Class
* Version 1.0a
* Copyright 2006-2015 (c) by CloudLogistix.com
* All Rights Reserved
* Your are allowed to make modifications only for yourself, not for distribution
* without permission from CloudLogistix.
*
*******************************************************************************/
	/*****************************************************************************************
    *   Constant Declaration
    ******************************************************************************************/
    public static Boolean SaveContactActivities = FALSE;
    public static Boolean SaveLeadActivities = FALSE;

    public class taskBeforeInsertHandler implements Triggers.Handler {
        public void handle() {
			Map <Id, User> partners = new Map <Id, User>
				([SELECT Id, UserType, CompanyName
				FROM User 
				WHERE UserType = 'PowerPartner'
				AND IsActive = TRUE]);
            for (SObject so : Trigger.new) {
                Task t = (task) so;
				if ((t.subject != null) &&
					(t.subject.contains('Registration - Partner - '))) {
					String pid = t.subject.substring(25);
					if (partners.get(pid) != null) {
						String pname = partners.get(pid).CompanyName;
						t.subject = 'Registration - Partner - ' + pname;
					}
				}
            }
        }
    } 

    public class taskBeforeDeleteHandler implements Triggers.Handler {
        public void handle() {
            for (SObject so : Trigger.old) {
                Task t = (task) so;
                if ((!string.valueof(Userinfo.getProfileId()).contains('00e600000017SAc')) &&
                	(string.valueof(t.createdbyid).contains('00560000001T5Tz'))) {
                	t.subject.addError('Sorry but you cannot delete tasks created by marketing');
                }
            }
		}
	}
	
	public class taskAfterInsertHandler implements Triggers.Handler {
        public void handle() {
            Set <Id> contactids = new Set <Id>();
            for (SObject so : Trigger.new) {
                Task t = (task) so;
                if ((t != null) && 
                    (t.WhoId != null) &&
                    ((String.valueof(t.WhoId).startswith('003')) ||
                    (String.valueof(t.WhoId).startswith('00Q')))) {
                    contactids.add(t.WhoId);
                }
            }
            tasktracking(contactids);
            CountTasks(Trigger.new);
        }
    } 
    
    public class taskAfterUpdateHandler implements Triggers.Handler {
        public void handle() {
            Set <Id> contactids = new Set <Id>();
            for (SObject so : Trigger.new) {
                Task t = (task) so;
                if ((t != null) && 
                    (t.WhoId != null) &&
                    ((String.valueof(t.WhoId).startswith('003')) ||
                    (String.valueof(t.WhoId).startswith('00Q')))) {
                    contactids.add(t.WhoId);
                }
            }
            tasktracking(contactids);
            CountTasks(Trigger.new);
        }
    } 

    public class taskAfterDeleteHandler implements Triggers.Handler {
        public void handle() {
            Set <Id> contactids = new Set <Id>();
            for (SObject so : Trigger.old) {
                Task t = (task) so;
                if ((t != null) && 
                    (t.WhoId != null) &&
                    ((String.valueof(t.WhoId).startswith('003')) ||
                    (String.valueof(t.WhoId).startswith('00Q')))) {
                    contactids.add(t.WhoId);
                }
            }
            tasktracking(contactids);
            CountTasks(Trigger.old);
        }
    } 

    public class taskAfterUndeleteHandler implements Triggers.Handler {
        public void handle() {
            Set <Id> contactids = new Set <Id>();
            for (SObject so : Trigger.new) {
                Task t = (task) so;
                if ((t != null) && 
                    (t.WhoId != null) &&
                    ((String.valueof(t.WhoId).startswith('003')) ||
                    (String.valueof(t.WhoId).startswith('00Q')))) {
                    contactids.add(t.WhoId);
                }
            }
            tasktracking(contactids);
            CountTasks(Trigger.new);
        }
    } 
    
    public static void CountTasks(List<Task> tasks) {
    	//Update Activity Count
	    ActivityUtils au = new ActivityUtils(tasks);
	    au.updateContactActivityCount();
	    au.updateLeadActivityCount();
    
    /*
	    List<Task> ltask1 = new List<Task>();
	    //public id oppid;
	    Set<Id> contactIds = new Set<Id>();
	    Set<Id> leadIds = new Set<Id>();
	    integer openCount = 0;   
	    integer closedCount = 0;
	    for(Task t : tasks){
	        oppid = t.WhatId;
	        system.debug('oppid'+oppid);
	        
	    }
		ltask1 = [select id,Status from task where whatid=:oppid];
	    system.debug('oppsize'+ltask1.size());
	    
	    
	    for(task t:ltask1){
	        if(t.Status!='Completed'){
	            inp = inp+1;
	        } else{
	            inr = inr +1;
	        }                
	    }
	    List<Opportunity> opp = new List<opportunity>();	
	    List<opportunity> op = [select id from Opportunity where id = :oppid];
	    system.debug('oppsize'+op.size());
	    for(opportunity o: op){
	        o.Open_Tasks__c = inp;
	        o.Closed_Tasks__c = inr;
	        opp.add(o);  
	    }
	    if(opp.size()>0){
	    	update opp;
		}        
	    system.debug('No of Tasks'+inp+inr);     
	*/      
	}

@future
    /*****************************************************************************************
    *   Update Contact First/Most Recent tasks 
    ******************************************************************************************/
    public static void tasktracking(Set <Id> contactids) {

        /*****************************************************************************************
        *   Constant Initialization
        ******************************************************************************************/ 
        if (!utils.isTest) 
        {
        	SaveContactActivities = utils.marketingappsettings().get('ContactSaveActivity').Enabled__c;
        	SaveLeadActivities = utils.marketingappsettings().get('LeadSaveActivity').Enabled__c;
        }

        /*****************************************************************************************
        *   Variable Initialization
        ******************************************************************************************/ 
        Contact[] firstcontactstoupd = new Contact[0];
        Contact[] lastcontactstoupd = new Contact[0];
        Lead[] firstleadstoupd = new Lead[0];
        Lead[] lastleadstoupd = new Lead[0];
        Map <Id, Task> firstMatches = new Map <Id, Task>();
        Map <Id, Task> lastMatches = new Map <Id, Task>();
        Map <String, Integer> activityStats = new Map <String, Integer>();
        List <SObject> contactsToRetry = new List <SObject>();
        List <Id> contactIdsToRetry = new List <Id>();
        List <Id> firstCtIdList = new List<Id>();
        List <Id> lastCtIdList = new List<Id>();
        List <Id> firstLdIdList = new List<Id>();
        List <Id> lastLdIdList = new List<Id>();
    
        /*****************************************************************************************
        *   Record Pre-Processing
        ******************************************************************************************/ 
        Contact[] contacts = ([SELECT Id, AccountId, MM_Most_Recent_Activity__c, 
                            MM_Most_Recent_Activity_Date__c, MM_First_Activity__c,
                            MM_First_Activity_Id__c, MM_Most_Recent_Activity_Id__c, 
                            MM_First_Activity_Date__c 
                            FROM Contact
                            WHERE Id in :contactids]);
        Lead[] leads = ([SELECT Id, Company, MM_Most_Recent_Activity__c, 
                            MM_Most_Recent_Activity_Date__c, MM_First_Activity__c,
                            MM_First_Activity_Id__c, MM_Most_Recent_Activity_Id__c, 
                            MM_First_Activity_Date__c 
                            FROM Lead
                            WHERE Id in :contactids]);

        Task[] AllTasks = ([SELECT Id, WhoId, Subject, CreatedDate FROM Task
                            WHERE Subject != null 
                            AND IsDeleted != TRUE
                            AND WhoId in :contactids
                            ORDER BY CreatedDate 
                            ALL ROWS]);
        AggregateResult[] tasksummary = ([SELECT Count(Id) totals, whoid, IsClosed FROM TASK
                            WHERE IsDeleted != TRUE
                            AND WhoId in :contactids
                            GROUP BY WhoId, IsClosed]);
                            
        for (Task t : AllTasks) {
            if ((firstMatches == null) ||
                (firstMatches.get(t.whoid) == null)) { 
                firstMatches.put(t.whoid, t);
            }
            lastMatches.put(t.whoid, t);
        }

		String recordid = '';
		Integer totals = 0;
		String isclosed = '';
		for (Integer i=0; i < tasksummary.size(); i++) {											// Loop thru the metrics retrieved
			recordid = String.valueof(tasksummary[i].get('WhoId'));									// Save our WhoId
			isclosed = String.valueof(tasksummary[i].get('IsClosed'));								// Is this for Open or Closed Activities
			totals = Integer.valueof(String.valueof(tasksummary[i].get('totals')));					// And Totals
			activityStats.put(recordid + ':' + isclosed, totals);
		}

        /*****************************************************************************************
        *   Record Processing
        ******************************************************************************************/
        if (SaveContactActivities) {
            for (Contact c : contacts) {
                c.MM_First_Activity_Id__c = null;
                c.MM_First_Activity__c = null;
                c.MM_First_Activity_Date__c = null;
                c.MM_Most_Recent_Activity_Id__c = null;
                c.MM_Most_Recent_Activity__c = null;
                c.MM_Most_Recent_Activity_Date__c = null;
                c.Number_of_Activities__c = 0;
                c.Number_of_Open_Activities__c = 0;
                if (activityStats != null) {
                	if (activityStats.get(c.id+':true') != null) {
                		c.Number_of_Activities__c = activityStats.get(c.id+':true');
                	}
                	if (activityStats.get(c.id+':false') != null) {
                		c.Number_of_Open_Activities__c = activityStats.get(c.id+':false');
                		c.Number_of_Activities__c += activityStats.get(c.id+':false');
                	}
				}
                Contact ct = new Contact(id=c.id, MM_First_Activity_Id__c = c.MM_First_Activity_Id__c,
                                        MM_First_Activity__c = c.MM_First_Activity__c,
                                        MM_First_Activity_Date__c = c.MM_First_Activity_Date__c,
                                        Number_of_Activities__c = c.Number_of_Activities__c,
                                        Number_of_Open_Activities__c = c.Number_of_Open_Activities__c);
                Contact lct = new Contact(id=c.id, MM_Most_Recent_Activity_Id__c = c.MM_Most_Recent_Activity_Id__c,
                                        MM_Most_Recent_Activity__c = c.MM_Most_Recent_Activity__c,
                                        MM_Most_Recent_Activity_Date__c = c.MM_Most_Recent_Activity_Date__c,
                                        Number_of_Activities__c = c.Number_of_Activities__c,
                                        Number_of_Open_Activities__c = c.Number_of_Open_Activities__c);
                if ((firstMatches != null) &&
                    (firstMatches.size() > 0) &&
                    (firstMatches.get(c.id) != null) &&
                    (firstMatches.get(c.id).subject != null) &&
                    (c.MM_First_Activity_Id__c != firstMatches.get(c.id).id)) {
                    ct.MM_First_Activity_Id__c = firstMatches.get(c.id).id;
                    ct.MM_First_Activity__c = firstMatches.get(c.id).subject;
                    ct.MM_First_Activity_Date__c = firstMatches.get(c.id).createddate;
                }
                if ((lastMatches != null) &&
                    (lastMatches.size() > 0) &&
                    (lastMatches.get(c.id) != null) &&
                    (lastMatches.get(c.id).subject != null) &&
                    (c.MM_Most_Recent_Activity_Id__c != lastMatches.get(c.id).id)) {
                    lct.MM_Most_Recent_Activity_Id__c = lastMatches.get(c.id).id;
                    lct.MM_Most_Recent_Activity__c = lastMatches.get(c.id).subject;
                    lct.MM_Most_Recent_Activity_Date__c = lastMatches.get(c.id).createddate;
                }
                firstcontactstoupd.add(ct);
                firstCtIdList.add(ct.Id);
                lastcontactstoupd.add(lct);
                lastCtIdList.add(lct.Id);
            }
        }

        if (SaveLeadActivities) {
            for (Lead l : leads) {
                l.MM_First_Activity_Id__c = null;
                l.MM_First_Activity__c = null;
                l.MM_First_Activity_Date__c = null;
                l.MM_Most_Recent_Activity_Id__c = null;
                l.MM_Most_Recent_Activity_Id__c = null;
                l.MM_Most_Recent_Activity_Id__c = null;
                l.Number_of_Activities__c = 0;
                l.Number_of_Open_Activities__c = 0;
                if (activityStats != null) {
                	if (activityStats.get(l.id+':true') != null) {
                		l.Number_of_Activities__c = activityStats.get(l.id+':true');
                	}
                	if (activityStats.get(l.id+':false') != null) {
                		l.Number_of_Open_Activities__c = activityStats.get(l.id+':false');
                		l.Number_of_Activities__c += activityStats.get(l.id+':false');
                	}
				}
                Lead ld = new Lead(id=l.id, MM_First_Activity_Id__c = l.MM_First_Activity_Id__c,
                                        MM_First_Activity__c = l.MM_First_Activity__c,
                                        MM_First_Activity_Date__c = l.MM_First_Activity_Date__c,
                                        Number_of_Activities__c = l.Number_of_Activities__c,
                                        Number_of_Open_Activities__c = l.Number_of_Open_Activities__c);
                Lead lld = new Lead(id=l.id, MM_Most_Recent_Activity_Id__c = l.MM_Most_Recent_Activity_Id__c,
                                        MM_Most_Recent_Activity__c = l.MM_Most_Recent_Activity__c,
                                        MM_Most_Recent_Activity_Date__c = l.MM_Most_Recent_Activity_Date__c,
                                        Number_of_Activities__c = l.Number_of_Activities__c,
                                        Number_of_Open_Activities__c = l.Number_of_Open_Activities__c);
                if ((firstMatches != null) &&
                    (firstMatches.size() > 0) &&
                    (firstMatches.get(l.id) != null) &&
                    (firstMatches.get(l.id).subject != null) &&
                    (l.MM_First_Activity_Id__c != firstMatches.get(l.id).id)) {
                    ld.MM_First_Activity_Id__c = firstMatches.get(l.id).id;
                    ld.MM_First_Activity__c = firstMatches.get(l.id).subject;
                    ld.MM_First_Activity_Date__c = firstMatches.get(l.id).createddate;
                }
                if ((lastMatches != null) &&
                    (lastMatches.size() > 0) &&
                    (lastMatches.get(l.id) != null) &&
                    (lastMatches.get(l.id).subject != null) &&
                    (l.MM_Most_Recent_Activity_Id__c != lastMatches.get(l.id).id)) {
                    lld.MM_Most_Recent_Activity_Id__c = lastMatches.get(l.id).id;
                    lld.MM_Most_Recent_Activity__c = lastMatches.get(l.id).subject;
                    lld.MM_Most_Recent_Activity_Date__c = lastMatches.get(l.id).createddate;
                }
                firstleadstoupd.add(ld);
                firstLdIdList.add(ld.Id);
                lastleadstoupd.add(lld);
                lastLdIdList.add(lld.Id);
            }
        }
        
        /*****************************************************************************************
        *   Record Post-Processing
        ******************************************************************************************/
        if (firstcontactstoupd.size() > 0) {
        	//update firstcontactstoupd;
        	try {
				Database.DMLOptions dmo = new Database.DMLOptions();
				dmo.optAllOrNone = false;
				Database.SaveResult[] srList = Database.update (firstcontactstoupd, dmo);
				for (Integer i=0; i<srList.size(); i++) {
					Database.SaveResult sr = srList[i];
					system.debug('@@@ save result: ' + sr);
	    			if (!sr.isSuccess()) {																		// if update of contact was not successful
						contactIdsToRetry.add(firstCtIdList[i]);												// ... add it to a set to try again
					}
				}
			}
			catch (exception e) {
				system.debug('Error updating contacts, firstContactsToUpd='+firstcontactstoupd);				
			}
        }
        if (lastcontactstoupd.size() > 0) {
        	//update lastcontactstoupd;
        	try {
				Database.DMLOptions dmo = new Database.DMLOptions();
				dmo.optAllOrNone = false;
				Database.SaveResult[] srList = Database.update (lastcontactstoupd, dmo);
				for (Integer i=0; i<srList.size(); i++) {
					Database.SaveResult sr = srList[i];
					system.debug('@@@ save result: ' + sr);
	    			if (!sr.isSuccess()) {																		// if update of contact was not successful
						contactIdsToRetry.add(lastCtIdList[i]);													// ... add it to a set to try again
					}
				}
			}
			catch (exception e) {
				system.debug('Error updating contacts, lastContactsToUpd='+lastcontactstoupd);				
			}
        }
        if (firstleadstoupd.size() > 0) {
        	//update firstleadstoupd;
        	try {
				Database.DMLOptions dmo = new Database.DMLOptions();
				dmo.optAllOrNone = false;
				Database.SaveResult[] srList = Database.update (firstleadstoupd, dmo);
				for (Integer i=0; i<srList.size(); i++) {
					Database.SaveResult sr = srList[i];
					system.debug('@@@ save result: ' + sr);
	    			if (!sr.isSuccess()) {																		// if update of lead was not successful
						contactIdsToRetry.add(firstLdIdList[i]);												// ... add it to a set to try again
					}
				}
			}
			catch (exception e) {
				system.debug('Error updating leads, firstLeadsToUpd='+firstleadstoupd);				
			}
        }
        if (lastleadstoupd.size() > 0) {
        	//update lastleadstoupd;
        	try {
				Database.DMLOptions dmo = new Database.DMLOptions();
				dmo.optAllOrNone = false;
				Database.SaveResult[] srList = Database.update (lastleadstoupd, dmo);
				for (Integer i=0; i<srList.size(); i++) {
					Database.SaveResult sr = srList[i];
					system.debug('@@@ save result: ' + sr);
	    			if (!sr.isSuccess()) {																		// if update of lead was not successful
						contactIdsToRetry.add(lastLdIdList[i]);													// ... add it to a set to try again
					}
				}
			}
			catch (exception e) {
				system.debug('Error updating leads, lastLeadsToUpd='+lastleadstoupd);				
			}
        }
        
        if (contactIdsToRetry.size() > 0) {
			Map<Id, Contact> contactMap = new Map<Id, Contact>(contacts);
			Map<Id, Lead> leadMap = new Map<Id, Lead>(leads);
				
			for (Id ctId : contactIdsToRetry) {
				if (String.valueOf(ctId).startsWith('003')) {
					contactsToRetry.add(contactMap.get(ctId));
				}
				else {
					contactsToRetry.add(leadMap.get(ctId));
				}
			}
			
			Datetime executeTime = (System.now()).addMinutes(1).addSeconds(30);								// schedule the job to run in 1.5 minute
			String cronExpression = Utils.GetCRONExpression(executeTime);
			
			ScheduleContactUpdate scheduledJob = new ScheduleContactUpdate(contactsToRetry);					// Instantiate a new Scheduled Apex class
			System.schedule('ScheduleContactUpdate ' + executeTime.getTime(),cronExpression,scheduledJob);
			
		}
    }
}