public class overrideAssignmentRemoveController
{
    //public OverrideAssignment__c overrideAssgn = new List<OverrideAssignment__c>();
    public Id acctId;
    public Map <String, String> params = new Map <String, String>(); 
    public Boolean pageError { get; set; } 
    public List<OverrideAssignment__c> overrideAssgns = new List<OverrideAssignment__c>();
    public static DateTime now = DateTime.now();
    public String returnURL;
       
    
    public overrideAssignmentRemoveController(ApexPages.StandardSetController stdSetController)
    {
        params = ApexPages.currentPage().getParameters();
        pageError = FALSE;

        Profile p = [select name from Profile where id = :UserInfo.getProfileId()];
        if (p.name == null || (p.name !='System Administrator' && p.name !='Tenable - System Administrator' && p.name != 'Tenable - Sales Ops User'))
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please contact Sales Ops to remove Named Accounts.'));
            pageError = TRUE;
        }
        else
        {
            if (params != null)
            {
                if (params.get('id') != null)
                {
                    acctId = params.get('id'); 
                    getoverrideAssgns();
                }
                if (params.get('retURL') != null)
                {
                    returnURL = params.get('retURL');
                }
            }
        }
    }
    
    
    public List<OverrideAssignment__c> getoverrideAssgns ()
    {
        if (overrideAssgns.isEmpty())
        {
            overrideAssgns = new List<OverrideAssignment__c>([SELECT Id, Name, Approved__c, Territory__c, StartAt__c, EndAt__c FROM OverrideAssignment__c WHERE Account__c = :acctId AND StartAt__c <= :now AND
                                (EndAt__c = null OR EndAt__c > :now)]);
        }
        
        system.debug('@@@ currentOverrideAssignments: ' + overrideAssgns);
        
        if (overrideAssgns.isEmpty())
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There are no current Named Account records for this Account.'));
            pageError = TRUE;
        }    
            
        return overrideAssgns;
    }
    
    
    public PageReference remove() 
    {
        //List<OverrideAssignment__c> overrideAssgnsToUpDate = new List<OverrideAssignment__c>();
        
        for (OverrideAssignment__c na : overrideAssgns)
        {
            if (na.Approved__c && na.StartAt__c <= now && (na.endAt__c == null || na.endAt__c > now))
            {
                na.endAt__c = now;
                //overrideAssgnsToUpdate.add(na);
                system.debug('@@@ endAt: ' + na.endAt__c);
            }
        }

        try 
        {
                //String returnpage = '/' + entry.quote__c;                                           // Save quote number to return to
                update overrideAssgns;  //ToUpdate;                                                                       // Try deleting it
                //return pageredirect(returnpage);                                                    // Return new page 
                return (new PageReference(returnURL)); 
        }
        catch (exception e) {
            Utils.addError('Unable to remove Named Account records: '+e.getMessage());                         // Report error back to the page
            return null;                                                                        // Make sure we don't refresh the page if there's an error
        }
    }    
    

    /*
    public PageReference save() 
    {
        List<OverrideAssignment__c> overrideAssgnsToUpsert = new List<OverrideAssignment__c>();
        DateTime timeNow = Datetime.now();
        
        for (OverrideAssignment__c na : overrideAssgns)
        {
            if (na.Approved__c && na.StartAt__c <= Datetime.now() && (na.endAt__c == null || na.endAt__c > Datetime.now()))
            {
                na.endAt__c = timeNow;
                overrideAssgnsToUpsert.add(na);
                system.debug('@@@ endAt: ' + na.endAt__c);
            }
        }
        
        overrideAssgn.startAt__c = timeNow.addSeconds(1);
        system.debug('@@@ startAt: ' + overrideAssgn.startAt__c);
        overrideAssgnsToUpsert.add(overrideAssgn);
        
        try 
        {
                //String returnpage = '/' + entry.quote__c;                                           // Save quote number to return to
                upsert overrideAssgnsToUpsert;                                                                       // Try deleting it
                //return pageredirect(returnpage);                                                    // Return new page 
                return (new ApexPages.StandardController(overrideAssgn)).view(); 
        }
        catch (exception e) {
            Utils.addError('Unable to upsert Named Account records: '+e.getMessage());                         // Report error back to the page
            return null;                                                                        // Make sure we don't refresh the page if there's an error
        }
    }    
    */
}