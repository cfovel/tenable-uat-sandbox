global class TenableCommunityFooterController {
    
    global class FooterRendering{
        public boolean ShowCollab {get;set;}
        public boolean ShowCases {get;set;}
        public boolean isSuccess {get;set;}
        public string errMsg {get;set;}
        public FooterRendering(boolean ShowCollab, boolean ShowCases){
            this.isSuccess = true;
            this.ShowCollab = ShowCollab;
            this.ShowCases = ShowCases;
        }
        public FooterRendering setRenderingError(String msg){
            this.isSuccess = false;
            this.errMsg = msg;
            return this;
        }
    }
    
    public TenableCommunityFooterController(){}
    
    /**
     *  Evaluates the information on the contact record and gives back visibility conditions that apply to certain sections
     *
     *  -Unauthenticated:
     *    Customer_Community_User__c = False
     *  -Authenticated:
     *    Customer_Community_User__c = True
     *  -Unpaid Support:
     *    Customer_Community_User__c = True & Support_Entitled_User__c = False
     *  -Paid Support:
     *    Customer_Community_User__c = True & Support_Entitled_User__c = True
     *
     *  @return serialized instance of FooterRendering
     *
     */
    @AuraEnabled
    global static String getFooterConditions() {
      //Unauthenticated - DO NOT show Cases or Collaborate
      Boolean ShowCases = false;
      Boolean ShowCollab = false;
      try{
          // Customer_Community_User__c = True & Support_Entitled_User__c = False
          User usr = [Select ContactId, Profile.Name, ProfileId from User where Id=: UserInfo.getUserId()];
          if(!isRestrictedNavigationUser(usr)){
            ShowCases = true;
            ShowCollab = true;
          }
          else if(usr.ContactId !=null){
            Contact CustomerContact = [SELECT Id, Name, Email, Phone, Phone_Support__c, Chat_Support__c, Customer_Community_User__c, Support_Entitled_User__c FROM Contact WHERE Id =:usr.ContactId];
            // Authenticated
            if(CustomerContact.Customer_Community_User__c){
              ShowCollab = true;
              //Paid Support - Show all
              if(CustomerContact.Customer_Community_User__c & CustomerContact.Support_Entitled_User__c){
                ShowCases = true;
              }else if(CustomerContact.Customer_Community_User__c & !CustomerContact.Support_Entitled_User__c){
                //Unpaid support - DO NOT show Cases
                ShowCases = false;
              }
            }
          }
      }
      catch(Exception err){
        FooterRendering f = new FooterRendering(false,false).setRenderingError(err.getMessage());
      }
      return System.JSON.serialize(new FooterRendering(ShowCollab,ShowCases));
    }
    
    private static Boolean isRestrictedNavigationUser(User u){
          Boolean resVal = true;
          for(Community_Full_Navigation__c n: [SELECT Disabled__c, Profile_Name__c, User_License__c FROM Community_Full_Navigation__c]){
            if (n.Profile_Name__c == u.Profile.Name && !n.Disabled__c) return false;
          }
          return resVal;
    }
}