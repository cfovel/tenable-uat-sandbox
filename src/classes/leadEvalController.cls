public class leadEvalController {
	public Id leadId;		
	public Boolean emptyElines = TRUE;	
	private final Lead leadInfo;
	public List<leadEvalWrapper> leadEvalList {get; set;}
	
	private final ApexPages.standardController stdController;										// Set-up page controller
	
	public leadEvalController(ApexPages.StandardController stdController)
	{
		if ((ApexPages.currentPage().getParameters() != null) &&									// Make sure we have parameters
        	(ApexPages.currentPage().getParameters().get('id') != null) && 							// and we're passing in a lead id value
            (ApexPages.currentPage().getParameters().get('id') != '')) {
            this.stdController = stdController;
			this.leadId = ApexPages.currentPage().getParameters().get('id');						// Save the lead id parameter
			
			this.leadInfo = [SELECT Has_Activated_Nessus_Eval__c,Date_Nessus_Eval_Order_Conf_Sent__c,Date_Nessus_Eval_Activation_Sent__c,
				Date_of_Nessus_Eval_Activation__c,Date_Nessus_Eval_Expires__c, Has_Activated_PVS_Eval__c,Date_PVS_Eval_Order_Conf_Sent__c,Date_PVS_Eval_Activation_Sent__c,
				Date_of_PVS_Eval_Activation__c,Date_PVS_Eval_Expires__c, Date_Nessus_Ent_Eval_Activ_Sent__c,Date_Nessus_Ent_Eval_Expires__c,Date_Nessus_Ent_Eval_Requested__c,
				Date_Nessus_Ent_Cloud_Eval_Activ_Sent__c,Date_Nessus_Ent_Cloud_Eval_Expires__c,Date_Nessus_Ent_Cloud_Eval_Requested__c,Nessus_Cloud_Eval_Product__c,Nessus_Ent_Cloud_Eval_Approval__c,
				Nessus_Ent_Cloud_Eval_Start_Date__c,Nessus_Ent_Eval_Approval__c,Nessus_Enterprise_Eval_Denial__c,Nessus_Enterprise_Cloud_Eval_Denial__c,Nessus_Activation_Code__c,
				Nessus_Manager_Eval_Product__c,Nessus_Ent_Eval_Start_Date__c,Has_Activated_Nessus_Ent_Eval__c,Has_Activated_Nessus_Ent_Cloud_Eval__c,Eval_Request_Id__c 
				FROM Lead  WHERE Id = :this.leadId];
				
			init();																					// Go initialize all the other values we want for the VF Page
		}
	}
	
	public void init()                                                                             // Page Initialization
	{
		this.leadEvalList = new List<leadEvalWrapper>();
		LeadEvalWrapper leadEval = new LeadEvalWrapper();
		
		/*********************** Populate Nessus Professional Evaluation *****************************/

		if (leadInfo.Date_Nessus_Eval_Order_Conf_Sent__c != null)
		{
			leadEval.evalRequestDate = leadInfo.Date_Nessus_Eval_Order_Conf_Sent__c.format();
		}
		
		if (leadInfo.Date_Nessus_Eval_Activation_Sent__c != null)
		{
			leadEval.evalActivSentDate = leadInfo.Date_Nessus_Eval_Activation_Sent__c.format();
		}
		
		if (leadInfo.Date_of_Nessus_Eval_Activation__c != null)
		{
			leadEval.evalActivStartDate = leadInfo.Date_of_Nessus_Eval_Activation__c.format('MM/dd/yyyy hh:mm a');
		}
		
		if (leadInfo.Date_Nessus_Eval_Expires__c != null)
		{
			leadEval.evalExpireDate = leadInfo.Date_Nessus_Eval_Expires__c.format();
		}
		
		//leadEval.activationCode = leadInfo.Nessus_Activation_Code__c;
		
		if (!evalIsEmpty(leadEval)) {
			leadEval.evalType = 'Nessus Professional';
			this.leadEvalList.add(leadEval);
		}
			
		/*************************** Populate PVS Evaluation *********************************/
		leadEval = new LeadEvalWrapper();

		if (leadInfo.Date_PVS_Eval_Order_Conf_Sent__c != null)
		{
			leadEval.evalRequestDate = leadInfo.Date_PVS_Eval_Order_Conf_Sent__c.format();
		}
		
		if (leadInfo.Date_PVS_Eval_Activation_Sent__c != null)
		{
			leadEval.evalActivSentDate = leadInfo.Date_PVS_Eval_Activation_Sent__c.format();
		}
		
		if (leadInfo.Date_of_PVS_Eval_Activation__c != null)
		{
			leadEval.evalActivStartDate = leadInfo.Date_of_PVS_Eval_Activation__c.format('MM/dd/yyyy hh:mm a');
		}
		
		if (leadInfo.Date_PVS_Eval_Expires__c != null)
		{
			leadEval.evalExpireDate = leadInfo.Date_PVS_Eval_Expires__c.format();
		}

		if (!evalIsEmpty(leadEval)) {
			leadEval.evalType = 'PVS';
			this.leadEvalList.add(leadEval);
		}
		
		/*************************** Populate Nessus Manager Evaluation *********************************/
		leadEval = new LeadEvalWrapper();
		
		if (leadInfo.Date_Nessus_Ent_Eval_Requested__c != null)
		{
			leadEval.evalRequestDate = leadInfo.Date_Nessus_Ent_Eval_Requested__c.format();
		}


		if (leadInfo.Date_Nessus_Ent_Eval_Activ_Sent__c != null)
		{
			leadEval.evalActivSentDate = leadInfo.Date_Nessus_Ent_Eval_Activ_Sent__c.format();
		}
		
		if (leadInfo.Nessus_Ent_Eval_Start_Date__c != null)
		{
			leadEval.evalActivStartDate = leadInfo.Nessus_Ent_Eval_Start_Date__c.format();
		}
		
		if (leadInfo.Date_Nessus_Ent_Eval_Expires__c != null)
		{
			leadEval.evalExpireDate = leadInfo.Date_Nessus_Ent_Eval_Expires__c.format();
		}

		leadEval.approval = leadInfo.Nessus_Ent_Eval_Approval__c;
		leadEval.product = leadInfo.Nessus_Manager_Eval_Product__c;
		//leadEval.activationCode = leadInfo.Nessus_Activation_Code__c;

		if (!evalIsEmpty(leadEval)) {
			leadEval.evalType = 'Nessus Manager';
			this.leadEvalList.add(leadEval);
		}
			
		/*************************** Populate Nessus Manager Evaluation *********************************/
		leadEval = new LeadEvalWrapper();
		
		if (leadInfo.Date_Nessus_Ent_Cloud_Eval_Requested__c != null)
		{
			leadEval.evalRequestDate = leadInfo.Date_Nessus_Ent_Cloud_Eval_Requested__c.format();
		}

		if (leadInfo.Date_Nessus_Ent_Cloud_Eval_Activ_Sent__c != null)
		{
			leadEval.evalActivSentDate = leadInfo.Date_Nessus_Ent_Cloud_Eval_Activ_Sent__c.format();
		}
		
		if (leadInfo.Nessus_Ent_Cloud_Eval_Start_Date__c != null)
		{
			leadEval.evalActivStartDate = leadInfo.Nessus_Ent_Cloud_Eval_Start_Date__c.format();
		}
		
		if (leadInfo.Date_Nessus_Ent_Cloud_Eval_Expires__c != null)
		{
			leadEval.evalExpireDate = leadInfo.Date_Nessus_Ent_Cloud_Eval_Expires__c.format();
		}

		leadEval.approval = leadInfo.Nessus_Ent_Cloud_Eval_Approval__c;
		leadEval.product = leadInfo.Nessus_Cloud_Eval_Product__c;
		//leadEval.activationCode = leadInfo.Nessus_Activation_Code__c;

		if (!evalIsEmpty(leadEval)) {
			leadEval.evalType = 'Nessus Cloud';
			this.leadEvalList.add(leadEval);
		}
			
		if (this.leadEvalList.size() > 0 ) 
		{
			emptyElines = false;
		}
	}	

	
	public Boolean getelinesIsEmpty() 
	{
		return emptyElines;
	}  
	
	public Boolean evalIsEmpty(LeadEvalWrapper eval) 
	{
		boolean isEmpty = true;
		if (eval.evalRequestDate > '' || eval.evalActivSentDate > '' || eval.evalActivStartDate > '' ||
			eval.evalExpireDate > '' || eval.approval > '' || eval.product > '') {
			isEmpty = false;
		}
		return isEmpty;
	}  

	public List<LeadEvalWrapper> getLeadEvalList() 
	{
		return this.leadEvalList;
	}
	
	public class LeadEvalWrapper 
	{
		public String evalType {get; set;}
		public String evalRequestDate {get; set;}
		public String evalActivSentDate {get; set;}
		public String evalActivStartDate {get; set;}
		public String evalExpireDate {get; set;}
		public String approval {get; set;}
		public String product {get; set;}
		//public String activationCode {get; set;}
	}
}