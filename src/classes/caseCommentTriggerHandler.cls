public with sharing class caseCommentTriggerHandler {
/*******************************************************************************
*
* Case Email Trigger Handler Class
* Version 1.0a
* Copyright 2006-2015 (c) by CloudLogistix.com
* All Rights Reserved
* Your are allowed to make modifications only for yourself, not for distribution
* without permission from CloudLogistix.
*
*******************************************************************************/

    public class CaseCommentAfterInsertHandler implements Triggers.Handler {
        public void handle() {
           // ProcessChecks(Trigger.new);
        }
    }
    
    public class CaseCommentAfterUpdateHandler implements Triggers.Handler {
        public void handle() {
          //  checkNewComments(Trigger.new, Trigger.old);
        }
    }  
    
    
    public static void ProcessChecks(CaseComment[] comments) {
      /*Set<Id> commentIds = new Set<Id>();
        for (CaseComment c: comments) {
      commentIds.add(c.Id);
        }
        
        if (!commentIds.isEmpty()) {
          SalesforceToJIRAController.CreateJiraCaseComments(commentIds);
        }*/
    }
    
    public static void checkNewComments(CaseComment[] newComments, CaseComment[] oldComments) {
    /*  string newComment, oldComment;
      Set<Id> commentIds = new Set<Id>();
      map<id, CaseComment> newMap = new map<id, CaseComment>();
      map<id, CaseComment> oldMap = new map<id, CaseComment>();
        for(CaseComment v: oldComments){
          oldMap.put(v.id, v);
        }
        //looping through the new comments to see if the new comment has #sfdc and the old comment does not. If this is true we will move it over to jira
        for(CaseComment c: newComments){
          newComment = c.commentBody;
          oldComment = oldMap.get(c.id).commentBody;
          if(newComment.contains('#nojira') && !oldComment.contains('#nojira') && (c.Parent.Jira_Case_Number__c != null && c.Parent.Jira_Case_Number__c != '')){
            system.debug('Am I here');
            commentIds.add(c.Id);  
          }
        }
        
        if (!commentIds.isEmpty()) {
          SalesforceToJIRAController.CreateJiraCaseComments(commentIds);
        }*/
    }
    
    public class jsonWrapper {
      public String text { get; set; }
      public String createdBy { get; Set; }
    }
    
}