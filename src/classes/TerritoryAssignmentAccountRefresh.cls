global class TerritoryAssignmentAccountRefresh implements Schedulable {
    /*************************************************************************************
       To run EVERY 15 MINUTES, login as Automation User, open Execute Anonymous window, and execute:     
       TerritoryAssignmentAccountRefresh sch = new TerritoryAssignmentAccountRefresh();
       system.schedule('Batch Territory Assignment Account Refresh - job1', '0 02 * * * ?', sch);  
       system.schedule('Batch Territory Assignment Account Refresh - job2', '0 17 * * * ?', sch);  
       system.schedule('Batch Territory Assignment Account Refresh - job3', '0 32 * * * ?', sch);  
       system.schedule('Batch Territory Assignment Account Refresh - job4', '0 47 * * * ?', sch);                                 
     *************************************************************************************/
    
    global void execute(SchedulableContext sc) {
        
        Database.executeBatch(new BatchTerritoryAssignmentAccountRefresh(), 200);
        
    }
   
}