global class baCalcAccountServiceLevel implements Database.Batchable<SObject> {
	// To run, login as Tenable System Admin, open Execute Anonymous window, and execute:
	//		database.executeBatch(new baCalcAccountServiceLevel('Start_Date__c <= TODAY AND End_Date__c >= TODAY'),200);  // for the first run
	// OR   database.executeBatch(new baCalcAccountServiceLevel('(((Start_Date__c = TODAY OR (Start_Date__c < TODAY AND (Opportunity.CloseDate = YESTERDAY OR Opportunity.CloseDate = TODAY))) AND End_Date__c >= TODAY) OR End_Date__c = YESTERDAY)'),200);   // for subsequent daily runs
		
	global String Query;
	global Id stdAcctRecTypeId = Schema.Sobjecttype.Account.getRecordTypeInfosByName().get('Standard Account').getRecordTypeId();

	global baCalcAccountServiceLevel(String dateCriteria)
    {
    	Query = 'SELECT Id, Quantity, Start_Date__c, End_Date__c, Service_Level__c, Opportunity.AccountId FROM OpportunityLineItem WHERE Service_Level__c != null AND Opportunity.Account.RecordTypeId = :stdAcctRecTypeId AND Opportunity.isClosed = true AND Opportunity.isWon = true AND ' + dateCriteria;
    }
    
    global Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator(Query);
    }

    global void execute(Database.BatchableContext context, List<OpportunityLineItem> scope) {
		
        Map<Id,Account> acctsToUpdate = new Map<Id,Account>();
        Map<String,OpportunityLineItem> acctProdOliMap = new Map<String,OpportunityLineItem>();
        //Set<Id> acctIds = new Set<Id>();
        
        for (OpportunityLineItem oli : scope) 
	    {
	    	//acctIds.add(oli.Opportunity.AccountId);
	    	acctsToUpdate.put(oli.Opportunity.AccountId, new Account(Id = oli.Opportunity.AccountId, Account_Service_Level__c = null));
	    	system.debug('oppLineItem: ' + oli);
	    }
	    
        for (Opportunity opp : [SELECT Id, AccountId, (SELECT Id, Quantity, OpportunityId, Product2Id, Service_Level__c, Opportunity.AccountId FROM OpportunityLineItems WHERE Service_Level__c != null AND Start_Date__c <= Today and End_Date__c >= TODAY) FROM Opportunity WHERE AccountId IN :acctsToUpdate.keySet() AND isClosed = true and isWon = true])
    	{
	        /*
	        Account acct = acctsToUpdate.get(opp.AccountId);
			if (acct == null)
			{
				acct = new Account(Id = opp.AccountId, Account_Service_Level__c = null);
			}
			acctsToUpdate.put(opp.AccountId, acct);
			*/
			
	        for (OpportunityLineItem oli : opp.OpportunityLineItems) 
	        {
	        	/*
	        	acct = acctsToUpdate.get(opp.AccountId);
				if (acct.Account_Service_Level__c == null || (oli.Service_Level__c != null && (oli.Service_Level__c.Contains('Premium') || (oli.Service_Level__c == 'Advanced' && (acct.Account_Service_Level__c == null || acct.Account_Service_Level__c == 'Standard'))))) 
				{
					acct.Account_Service_Level__c = oli.Service_Level__c;
				}
				acctsToUpdate.put(opp.AccountId, acct);
				system.debug('Acct to Update: ' + acct);
				*/
				String acctProd = opp.AccountId + '-' + oli.Product2Id;
				OpportunityLineItem oliSum = acctProdOliMap.get(acctProd);
				if (oliSum == null)
				{
					oliSum = oli;
				}
				else
				{
					oliSum.Quantity+=oli.Quantity;
				}
				acctProdOliMap.put(acctProd,oliSum);
				
	        }
        }
        
        for (OpportunityLineItem oli : acctProdOliMap.values())
        {
        	if (oli.Quantity > 0)
        	{
        		Account acct = acctsToUpdate.get(oli.Opportunity.AccountId);
				if (acct.Account_Service_Level__c == null || (oli.Service_Level__c != null && (oli.Service_Level__c.Contains('Elite') || (oli.Service_Level__c == 'Advanced' && (acct.Account_Service_Level__c == null || acct.Account_Service_Level__c == 'Standard'))))) 
				{
					acct.Account_Service_Level__c = oli.Service_Level__c;
				}
				acctsToUpdate.put(oli.Opportunity.AccountId, acct);
				system.debug('Acct to Update: ' + acct);
        	}
        }
        update acctsToUpdate.values();
    }

    global void finish(Database.BatchableContext context) {
    }   
}