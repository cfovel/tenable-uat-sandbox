@isTest
public class Tenable_OnboardingControllerTest {

    @testSetup public static void setup() {
        system.debug('in setup');
        Utils.isTest = true;

        list <network> testnetworkList = [SELECT Id
        FROM Network
        WHERE Name = 'Tenable Community'];
        Id networkId = testnetworkList[0].Id;

        test.startTest();
        Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
        User testUser = Peak_TestUtils.createStandardUser();
        insert testUser;
        test.stopTest(); 

        CollaborationGroup colGroup = new CollaborationGroup(
                Name = 'Test Chatter Group',
                CollaborationType = 'Public',
                CanHaveGuests = False,
                isArchived = false,
                isAutoArchiveDisabled = false,
                networkId = networkId
        );
        CollaborationGroup colGroup1 = new CollaborationGroup(
                Name = 'Test Chatter Group2',
                CollaborationType = 'Public',
                CanHaveGuests = False,
                isArchived = false,
                isAutoArchiveDisabled = false,
                networkId = networkId
        );
        CollaborationGroup colGroup3 = new CollaborationGroup(
                Name = 'Test Chatter Group3',
                CollaborationType = 'Public',
                CanHaveGuests = False,
                isArchived = false,
                isAutoArchiveDisabled = false,
                networkId = networkId
        );
        CollaborationGroup colGroup4 = new CollaborationGroup(
                Name = 'Test Chatter Group4',
                CollaborationType = 'Public',
                CanHaveGuests = False,
                isArchived = false,
                isAutoArchiveDisabled = false,
                networkId = networkId
        );
        insert new List<CollaborationGroup> {colGroup, colGroup1, colGroup3, colGroup4};

        Topic testTopic = new Topic(
                Name = 'Test Topic 1',
                Description = 'Test Topic1 Description',
                networkId = networkId

        );
        insert testTopic;
        Topic testTopic2 = new Topic(
                Name = 'Test Topic 2',
                Description = 'Test Topic2 Description',
                networkId = networkId
        );
        insert testTopic2;
    }

    @isTest
    public static void testGetOnboardingComplete() {
        User testUser = [Select Id, Onboarding_Complete__c from User where Email = 'standarduser@peak.com' Limit 1];
        testUser.Onboarding_Complete__c = True;
        update testUser;
        system.runAs(testUser) {
            User testUser1 = Tenable_OnboardingController.getUserRecord();
            system.assert(testUser1.Onboarding_Complete__c == True);
        }
    }

    @isTest
    public static void testUpdateUserNames() {
        User testUser = [Select Id, Onboarding_Complete__c from User where Email = 'standarduser@peak.com' Limit 1];
        testUser.FirstName ='TestChange';
        testUser.LastName = 'LastNameChange';
        system.runAs(testUser) {
            Tenable_OnboardingController.updateUserNames(testUser);
            User testUserName = [Select Id, FirstName from User where Email = 'standarduser@peak.com' Limit 1];
            system.assert(testUserName.FirstName == 'TestChange');
        }
    }
    
    @isTest
    public static void testGetEmailPreference(){
        List<User> users = [Select Id, Onboarding_Complete__c from User where Email = 'standarduser@peak.com' Limit 1];
        if(users != null && users.size() > 0){
            User testUser = users[0];
            system.runAs(testUser) {
                Tenable_OnboardingController.getEmailPreference();
            }
        }
    }

    @isTest
    public static void testUpdatePreferences() {
        List<User> users = [Select Id, Onboarding_Complete__c from User where Email = 'standarduser@peak.com' Limit 1];
        if(users != null && users.size() > 0){
            User testUser = users[0];
            system.runAs(testUser) {
                Tenable_OnboardingController.updatePreferences(False);
                    NetworkMember testUserName = [SELECT PreferencesDisableAllFeedsEmail FROM NetworkMember WHERE MemberId = :testUser.Id Limit 1];
                    //system.assert(testUserName.PreferencesDisableAllFeedsEmail == True);
                    Tenable_OnboardingController.updatePreferences(True);
                    NetworkMember testUserName2 = [SELECT PreferencesDisableAllFeedsEmail FROM NetworkMember WHERE MemberId = :testUser.Id Limit 1];
                    //system.assert(testUserName2.PreferencesDisableAllFeedsEmail == False);
            }
        }
    }

    @isTest
    public static void testInsertGroupMember() {
        User testUser = [Select Id, Onboarding_Complete__c from User where Email = 'standarduser@peak.com' Limit 1];

        Id group1 = [Select Id from CollaborationGroup where Name = 'Test Chatter Group' limit 1].Id;
        Id group2 = [Select Id from CollaborationGroup where Name = 'Test Chatter Group2' limit 1].Id;
        Id group3 = [Select Id from CollaborationGroup where Name = 'Test Chatter Group3' limit 1].Id;
        Id group4 = [Select Id from CollaborationGroup where Name = 'Test Chatter Group4' limit 1].Id;

        system.runAs(testUser) {
            Tenable_OnboardingController.insertGroupMember(group1, 'Daily', True, group2, 'Weekly', True,
                                                            group3, 'Daily', true, group4, 'Daily', true);

            list <CollaborationGroupMember> memberList = [SELECT Id
                                                        FROM CollaborationGroupMember
                                                        WHERE CollaborationGroupId = :group1 AND memberId = :testUser.Id];

            system.assert(memberList.size() == 1);
            
            Tenable_OnboardingController.insertGroupMember(group1, 'Daily', false, group2, 'Weekly', false,
                                                            group3, 'Daily', false, group4, 'Daily', false);


        }
    }

    @isTest
    public static void testCompleteSlide() {
        User testUser = [Select Id from User where Email = 'standarduser@peak.com' Limit 1];
        system.runAs(testUser) {
            Tenable_OnboardingController.completeSlide('Welcome');
            Tenable_OnboardingController.completeSlide('Profile');
            Tenable_OnboardingController.completeSlide('Notification');
            Tenable_OnboardingController.completeSlide('Topic');
            Tenable_OnboardingController.completeSlide('Group');
            Tenable_OnboardingController.completeSlide('Tours');
            Tenable_OnboardingController.completeSlide('Done');

            User testUserName = [Select Completed_Tours_Slide__c, Onboarding_Complete__c from User where Email = 'standarduser@peak.com' Limit 1];
            system.assert(testUserName.Completed_Tours_Slide__c == True);
            system.assert(testUserName.Onboarding_Complete__c == True);
        }
    }

    @isTest
    public static void testUpdateReputationPoints() {
        User testUser = [Select Id from User where Email = 'standarduser@peak.com' Limit 1];
        system.runAs(testUser) {
            Tenable_OnboardingController.updateReputationPoints();
        }
    }


    @isTest
    public static void testGetTopics() {
        User testUser = [Select Id from User where Email = 'standarduser@peak.com' Limit 1];
        list <Id> topicIds = new list <Id>();
        for (Topic top: [Select Id from Topic where Name = 'Test Topic 1' OR Name = 'Test Topic 2']){
            topicIds.add(top.Id);
        }
        system.runAs(testUser) {
            list <Tenable_OnboardingWrapper> topicWrapper = Tenable_OnboardingController.getTopics(topicIds);
            system.assert(topicWrapper.size() == 2);
        }
    }

    @isTest
    public static void testGetGroups() {
        User testUser = [Select Id from User where Email = 'standarduser@peak.com' Limit 1];
        list <Id> groupIds = new list <Id>();
        for (CollaborationGroup grp: [Select Id from CollaborationGroup where Name = 'Test Chatter Group' OR Name = 'Test Chatter Group2']){
            groupIds.add(grp.Id);
        }
        system.runAs(testUser) {
        	Tenable_OnboardingController.insertGroupMember(groupIds[0], 'Daily', True, groupIds[1], 'Weekly', True,
                                                            null, null, false, null, null, false);
            list <Tenable_OnboardingWrapper> groupWrapper = Tenable_OnboardingController.getGroups(groupIds);
            system.assert(groupWrapper.size() == 2);
        }
    }

    @isTest
    public static void testGetGroup() {
        User testUser = [Select Id from User where Email = 'standarduser@peak.com' Limit 1];
        list <Id> groupIds = new list <Id>();
        for (CollaborationGroup grp: [Select Id from CollaborationGroup where Name = 'Test Chatter Group' OR Name = 'Test Chatter Group2']){
            groupIds.add(grp.Id);
        }
        system.runAs(testUser) {
        	Tenable_OnboardingController.insertGroupMember(groupIds[0], 'Daily', True, groupIds[1], 'Weekly', True,
                                                            null, null, false, null, null, false);
            Tenable_OnboardingWrapper groupWrapper = Tenable_OnboardingController.getGroup(groupIds[0]);
            system.assert(groupWrapper.id == groupIds[0]);
        }
    }
    
    @isTest
    public static void testRemoveGroupMember(){
        User testUser = [Select Id from User where Email = 'standarduser@peak.com' Limit 1];
        list <Id> groupIds = new list <Id>();
        for (CollaborationGroup grp: [Select Id from CollaborationGroup where Name = 'Test Chatter Group' OR Name = 'Test Chatter Group2']){
            groupIds.add(grp.Id);
        }
        system.runAs(testUser) {
        	Tenable_OnboardingController.insertGroupMember(groupIds[0], 'Daily', True, groupIds[1], 'Weekly', True,
                                                            null, null, false, null, null, false);
			Tenable_OnboardingController.removeGroupMember(groupIds[0]);
        }
    }

    @isTest
    public static void testFollowTopic() {
        String topicId =  [Select Id from Topic where Name = 'Test Topic 1' limit 1].Id;

        Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
        Test.startTest();
        Tenable_OnboardingController.followTopic(topicId);
        Test.stopTest();
        list <EntitySubscription> userFollow = [SELECT Id FROM EntitySubscription WHERE parentId = :topicId AND subscriberId = :UserInfo.getUserId() limit 1];
        system.assert(userFollow.size() == 1);
    }

    @isTest
    public static void testUnfollowTopic() {
        String topicId =  [Select Id from Topic where Name = 'Test Topic 1' limit 1].Id;
        Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
        Test.startTest();
        Tenable_OnboardingController.followTopic(topicId);

        Tenable_OnboardingController.unfollowTopic(topicId);
        Test.stopTest();
        list <EntitySubscription> userFollow = [SELECT Id FROM EntitySubscription WHERE parentId = :topicId AND subscriberId = :UserInfo.getUserId() limit 1];
        system.assert(userFollow.size() == 0);
    }


}