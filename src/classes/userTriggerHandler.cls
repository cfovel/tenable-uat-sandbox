public with sharing class userTriggerHandler 
{
/*******************************************************************************
*
* User Trigger Handler Class
* Version 1.0a
* Copyright 2006-2014 (c) by CloudLogistix.com
* All Rights Reserved
* Your are allowed to make modifications only for yourself, not for distribution
* without permission from CloudLogistix.
*
*******************************************************************************/
    //Contstant declaration
    public static String ClientCredentials;
    public static Blob headerValue;
    public static String authorizationHeader;
    public static String endPoint;
    public static boolean sendToSandbox = runningInASandbox();
    
    
    public class UserAfterInsertHandler implements Triggers.Handler 
    {
        public void handle() 
        {
            ProcessChecks(Trigger.new);
            auth0UserCreation(trigger.new);
        }
    } 
    
    public class UserAfterUpdateHandler implements Triggers.Handler 
    {
        public void handle() 
        {
            ProcessCommunityUsers(trigger.new);
        }
    } 


    public static void ProcessChecks(User[] users) 
    {
        /*****************************************************************************************
        *   Variable Initialization
        ******************************************************************************************/
        Set <Id> userids = new Set <Id>();
        for (User u : users) 
        { 
            // Check for new Partner Record or Customer Community Record
            if (u.UserType != null && (u.UserType.contains('Partner') || u.UserType.contains('High Volume Portal') || u.UserType.contains('CspLitePortal'))) 
            {                                                           // Check to see if we're adding a partner or customer community user record
                userids.add(u.id);
            }
        }

        if (userids.size() > 0) 
        {
            system.debug('The value of userids is : ' + userids);
            ProcessRecords(userids);
            ProcessPartnerGroups(userids);
        }
    }
    
    public static void auth0UserCreation(User[] users) 
    {
        /*****************************************************************************************
        *   Variable Initialization
        ******************************************************************************************/
        Set <Id> userids                   = new Set <Id>();
        set<id> conIds                     = new set<id>();
        list<contact> conlist              = new list<contact>();
        list<createUserWrapper> auth0Users = new list<createUserWrapper>();
        map<id, contact> conMap            = new map<id, contact>();
        contact tempCon                    = new contact();
        createUserWrapper auth0User;
        a_metadata ameta;
        u_metadata umeta;
        string jsonString;
        
        for (User i : users){ 
            //creating a set of contact Ids so that we can queury them and create a map
            conIds.add(i.contactId);
        }
        
        //quering list of all contacts related to the user records
        conList = [select id, LMS_Customer_Contact_ID__c , LMS_Customer_ID__c, Account_Country__c, Account.Name from contact where id =: conIds];
        if(conList.size() > 0){
            for(contact c: conList){
               conMap.put(c.id, c); 
            }
        }
        
        for(User u: users){
            // Check for new Customer Record
            // 
            system.debug('ROY auth0 Start: ' + u);
            if ((u.UserType.contains('High Volume Portal') || u.UserType.contains('CspLitePortal')) && (u.FederationIdentifier == null || u.FederationIdentifier == '')){ 
                
                //create app metadata object
                ameta = new a_metadata();
                ameta.given_name  = u.FirstName;
                ameta.family_name = u.LastName;
                if(u.contactId != null){
                    tempCon = conMap.get(u.contactId);
                    system.debug('ROY auth0 check contact: ' + tempCon);
                    ameta.lms_customer_id  = tempCon.LMS_Customer_ID__c;
                    ameta.lms_country_name = tempCon.Account_Country__c;
                    ameta.lms_company_name = tempCon.Account.Name;
                    ameta.docebo_branch_id = 'customer';
                }
                
                //creating user_metadata object
                umeta = new u_metadata();
                umeta.requires_username = false;
                
                //getting together the rest of the auth0 fields
                auth0User = new createUserWrapper();
                auth0User.connection     = 'Username-Password-Authentication';
                auth0User.email          = u.email;
                auth0User.name           = u.FirstName + ' ' + u.LastName;
                auth0User.password       = randomString();
                auth0User.email_verified = false;
                auth0User.app_metadata   = ameta;
                auth0User.user_metadata  = umeta;
                
                system.debug('ROY auth0 0 authuser: ' + auth0User);
                
                
                jsonString = JSON.serialize(auth0User);
                system.debug('ROY auth0 0.5 JSON String: ' + jsonString);
                
                //calling future method to make callout to Auth0
                try{
                auth0Callout(jsonString, u.Id);
                }catch(exception e){
                    system.debug('ROY auth0 1: ' + e);
                }
            }
        }
    }
    
    public static void ProcessCommunityUsers(User[] users) 
    {
        /*****************************************************************************************
        *   Variable Initialization
        ******************************************************************************************/
        List <Id> inactiveContactIds = new List<Id>();
        List <Id> activeContactIds = new List<Id>();
        for (User u : users) 
        { 
        	system.debug('@@@ user: ' + u);
            // Check for new Partner Record or Customer Community Record
            if (u.UserType != null && (u.UserType.contains('High Volume Portal') || u.UserType.contains('CspLitePortal'))) 
            {

                User oldUser = (User)Trigger.oldMap.get(u.Id);
                system.debug('@@@ old user: ' + oldUser);
                if (u.isActive == false && oldUser.isActive == true)										// if user is deactivated, uncheck customer community user flag
                {
                	if(oldUser.ContactId != null)// && u.ContactId != oldUser.ContactId)
                	{
                		inactiveContactIds.add(oldUser.ContactId);
                	}
                }
	            if (u.isActive == true && oldUser.isActive == false)										// if user is activated, check customer community user flag
                {
                	if(u.ContactId != null)
                	{
                		activeContactIds.add(u.ContactId);
                	}
                }
            }
        }
        
        if (!activeContactIds.isEmpty()) 
        {
            UpdateCommunityContactsActive(activeContactIds);
        }

        if (!inactiveContactIds.isEmpty()) 
        {
            UpdateCommunityContactsInactive(inactiveContactIds);
        }
    }
    
    @future
    public static void UpdateCommunityContactsActive(Id[] contactIds) 
    {
        /*****************************************************************************************
        *   Variable Initialization
        ******************************************************************************************/
        List <Contact> contactstoUpdate = new List <Contact>();
        for (Id contactId : contactIds) 
        { 
			contactsToUpdate.add(new Contact(Id = contactId, Customer_Community_User__c = TRUE));
        }

        if (!contactsToUpdate.isEmpty()) 
        {
            update contactsToUpdate;
        }
    }
    
    @future
    public static void UpdateCommunityContactsInactive(Id[] contactIds) 
    {
        /*****************************************************************************************
        *   Variable Initialization
        ******************************************************************************************/
        List <Contact> contactstoUpdate = new List <Contact>();
        for (Id contactId : contactIds) 
        { 
			contactsToUpdate.add(new Contact(Id = contactId, Customer_Community_User__c = FALSE));
        }

        if (!contactsToUpdate.isEmpty()) 
        {
            update contactsToUpdate;
        }
    }
    
    @future(callout=true)
    public static void auth0Callout(string jsonString, id userId){
        
        if (sendToSandbox)
        {
            // set Net Suite Sandbox credentials
            SetSandboxAuthorizationHeaderValues();
        }
        else
        {    
            SetAuthorizationHeaderValues();
        }
       
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        List<String> sendTo = new List <String>{'cfovel@tenable.com', 'rjones@tenable.com', 'bmuniswamy@tenable.com'};

        Http h = new Http();
        HttpRequest req = new HttpRequest();
        
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Authorization',authorizationHeader);
        req.setEndpoint(endpoint + 'security/' + userId + '/createUser');
        req.setMethod('POST');
        req.setTimeout(60000);
        req.setBody(jsonString);
        HttpResponse response = h.send(req);                                                            // send http request
        system.debug('ROY auth0 2: createUser response:' + response);
        if (response.getStatusCode() != 200) {                                        // send email if there is an error
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setReplyTo('sfdc@tenable.com');
            mail.setSenderDisplayName('Salesforce Automation');
            mail.setSubject('Error Creating Sales Order in NetSuite');
            String body = 'There was an error creating a auth0 user.  Salesforce User Id: ' + 
                userId + '.  Response Code: ' + response.getStatusCode();
            mail.setToAddresses(sendTo);
            mail.setHtmlBody(body);
            mails.add(mail);
            try{
                Messaging.sendEmail(mails);
            }
            catch(Exception e){
                system.debug('ROY auth0 3 exception sending email:'+e);
            }
        }
    }

@future
    public static void ProcessRecords(Set <Id> userids) 
    {
        /*****************************************************************************************
        *   Variable Initialization
        ******************************************************************************************/
        Set <Id> acctsbeingupdated = new Set <Id>();
        List <Contact> contactstoUpdate = new List <Contact>();
        List <AccountTeamMember> teamMembers = new List <AccountTeamMember>();
        List <CollaborationGroupMember> chatterGroupMembers = new List <CollaborationGroupMember>();
        String PartnerGroup, PartnerAMERGroup, PartnerEMEAGroup, PartnerAPACGroup, TIPUserGroup;

        /*****************************************************************************************
        *   Record Pre-Processing
        ******************************************************************************************/
        List <String> groups = new List <String> {'All Partners', 'All AMER Partners', 'All EMEA Partners', 'All APAC Partners', 'TIP Collaboration Group'};
        List <CollaborationGroup> chatterGroups = ([SELECT id, Name FROM CollaborationGroup WHERE Name in :groups]);
        for (CollaborationGroup cg : chattergroups) 
        {
            if (cg.Name == 'All Partners') {PartnerGroup = cg.Id;}
            if (cg.Name == 'All AMER Partners') {PartnerAMERGroup = cg.Id;}
            if (cg.Name == 'All EMEA Partners') {PartnerEMEAGroup = cg.Id;}
            if (cg.Name == 'All APAC Partners') {PartnerAPACGroup = cg.Id;}
            //if (cg.Name == 'TIP Collaboration Group') {TipUserGroup = cg.Id;}
        }
        
        User[] users = ([Select Id, UserType, AccountId, ContactId, Theatre__c, Profile.Name FROM User WHERE Id in :userids]);

        /*****************************************************************************************
        *   Record Processing
        ******************************************************************************************/
        for (User u : users) 
        {  
            // Check for new Partner Record
            if (u.UserType.contains('Partner')) 
            {
                if (u.AccountId != null) 
                {                                                          // Add to Account record
                    TeamMembers.add(new AccountTeamMember(userId = u.Id, AccountId = u.AccountId, TeamMemberRole = 'Partner'));
                    acctsbeingupdated.add(u.AccountId);
                }
                if (u.ContactId != null) 
                {
                    contactstoUpdate.add(new Contact(id = u.ContactId, isPortalUser__c = TRUE));
                }
                
                if(u.Profile.Name != 'Tenable - APAC Agency Community User')   
                {
                    system.debug('The value of u.Profile.Name inside the processrecords is : ' + u.Profile.Name);                                                                                 
                    // Add to proper chatter groups based on Area   
                    // Add to the all Partners Group
                    chatterGroupMembers.add(new CollaborationGroupMember(CollaborationGroupId=PartnerGroup, MemberId = u.Id));
                    
                    // Add to the all Partners AMER Group
                    if (u.Theatre__c == 'AMER') 
                    {
                        chatterGroupMembers.add(                                                        
                            new CollaborationGroupMember(CollaborationGroupId=PartnerAMERGroup, MemberId = u.Id));                  
                    }
                    
                    // Add to the all Partners EMEA Group
                    if (u.Theatre__c == 'EMEA') 
                    {
                        chatterGroupMembers.add(new CollaborationGroupMember(CollaborationGroupId=PartnerEMEAGroup, MemberId = u.Id));                  
                    }
                    
                    // Add to the all Partners APAC Group
                    if (u.Theatre__c == 'APAC') 
                    {
                        chatterGroupMembers.add(new CollaborationGroupMember(CollaborationGroupId=PartnerAPACGroup, MemberId = u.Id));                  
                    }
                }
            }
            else if (u.ContactId != null && (u.UserType.contains('High Volume Portal') || u.UserType.contains('CspLitePortal'))) 
            {
                contactstoUpdate.add(new Contact(id = u.ContactId, Customer_Community_User__c = TRUE));
            }
        }

        /*****************************************************************************************
        *   Record Post-Processing
        ******************************************************************************************/
        if(!Utils.isTest) 
        {
            system.debug('The value of chatterGroupMembers.size is : ' + ChatterGroupMembers.size());
            if (chatterGroupMembers.size() > 0) 
            {
                system.debug('The value of chatterGroupMembers is : ' + ChatterGroupMembers);
                insert chatterGroupMembers;
            }
        
            if (TeamMembers.size() > 0) 
            {
                insert TeamMembers;
                // Now we need to grab them back to fix access level
                List <AccountShare> shares = ([SELECT Id, AccountAccessLevel FROM AccountShare WHERE AccountId IN :acctsbeingupdated
                                               AND RowCause = 'Team']);
                
                // Loop thru the records and ...
                for (AccountShare share : shares) 
                {                                                     
                    share.AccountAccessLevel = 'Edit';                                                  // give the partner read/write access
                }
                update shares;                                                                          // Go back and update them
            }
        
            if (contactstoUpdate.size() > 0) 
            {
                update contactstoUpdate;
            }
        }
    }
    
    @future
    public static void ProcessPartnerGroups(Set <Id> userids) 
    {
        /*****************************************************************************************
        *   Variable Initialization
        ******************************************************************************************/
        List <GroupMember> groupMembers = new List <GroupMember>();
        String AllPartnersGroup,AllPartnersAMERGroup,AllPartnersEMEAGroup,AllPartnersAPACGroup,PlatinumPartnerGroup, GoldPartnerGroup, SilverPartnerGroup, BronzePartnerGroup, DistributorPartnerGroup,TIPUserGroup;

        /*****************************************************************************************
        *   Record Pre-Processing
        ******************************************************************************************/        
        List<String> partnerGroupNames = new List<String> {'All Partners','All Partners (APAC)','All Partners (AMER)','All Partners (EMEA)','All Platinum Partners', 'All Gold Partners', 'All Silver Partners', 'All Bronze Partners', 'All Distributor Partners', 'All TIP Users'};
        List<Group> partnerGroups = new List<Group>([SELECT Id, Name FROM Group WHERE Name in :partnerGroupNames]);
        for (Group pg : partnerGroups) 
        {
            if (pg.Name == 'All Platinum Partners') {PlatinumPartnerGroup = pg.Id;}
            if (pg.Name == 'All Gold Partners') {GoldPartnerGroup = pg.Id;}
            if (pg.Name == 'All Silver Partners') {SilverPartnerGroup = pg.Id;}
            if (pg.Name == 'All Bronze Partners') {BronzePartnerGroup = pg.Id;}
            if (pg.Name == 'All Distributor Partners') {DistributorPartnerGroup = pg.Id;}
            if (pg.Name == 'All Partners') {AllPartnersGroup = pg.Id;}
            if (pg.Name == 'All Partners (APAC)') {AllPartnersAPACGroup = pg.Id;}
            if (pg.Name == 'All Partners (AMER)') {AllPartnersAMERGroup = pg.Id;}
            if (pg.Name == 'All Partners (EMEA)') {AllPartnersEMEAGroup = pg.Id;}
            //if (pg.Name == 'All TIP Users') {TIPUserGroup = pg.Id;}
        }
        
        User[] users = ([Select Id, UserType, AccountId, ContactId, Theatre__c, Contact.Account.Edge_Authorization_Status__c,Profile.Name FROM User WHERE Id in :userids]);
        
        /*****************************************************************************************
        *   Record Processing
        ******************************************************************************************/
        for (User u : users) 
        { 
            system.debug('The value of u.Profile.Name before the if ProcessPartnerGroups is : ' + u.Profile.Name) ;
            if(u.Profile.Name != 'Tenable - APAC Agency Community User')
            {
                if(u.UserType.contains('Partner'))
                {
                    groupMembers.add(new GroupMember(GroupId = AllPartnersGroup, UserOrGroupId = u.Id));
                    
                    if (u.Theatre__c == 'AMER') 
                    {
                        groupMembers.add(new GroupMember(GroupId = AllPartnersAMERGroup, UserOrGroupId = u.Id));
                    }
                    
                    if(u.Theatre__c == 'APAC')
                    {
                        groupMembers.add(new GroupMember(GroupId = AllPartnersAPACGroup, UserOrGroupId = u.Id));
                    }
                    
                    if(u.Theatre__c == 'EMEA')
                    {
                        groupMembers.add(new GroupMember(GroupId = AllPartnersEMEAGroup, UserOrGroupId = u.Id));
                    }
                }
                system.debug('The value of u.Profile.Name inside processpartnersgroups is : ' + u.Profile.Name);
                // Check for new Partner Record // Check to see if we're adding a partner user record
                if (u.UserType.contains('Partner') && u.Contact.Account.Edge_Authorization_Status__c  != null) 
                {   
                    // Add user to group based on Edge Authorization Status                                                                                                             
                    if (u.Contact.Account.Edge_Authorization_Status__c == 'Platinum') 
                    {                                       
                        groupMembers.add(new GroupMember(GroupId = PlatinumPartnerGroup, UserOrGroupId = u.Id));
                    }
                    else if (u.Contact.Account.Edge_Authorization_Status__c == 'Gold') 
                    {
                        groupMembers.add(new GroupMember(GroupId = GoldPartnerGroup, UserOrGroupId = u.Id));
                    }
                    else if (u.Contact.Account.Edge_Authorization_Status__c == 'Silver') 
                    {
                        groupMembers.add(new GroupMember(GroupId = SilverPartnerGroup, UserOrGroupId = u.Id));
                    }
                    else if (u.Contact.Account.Edge_Authorization_Status__c == 'Bronze') 
                    {
                        groupMembers.add(new GroupMember(GroupId = BronzePartnerGroup, UserOrGroupId = u.Id));
                    }
                    else if (u.Contact.Account.Edge_Authorization_Status__c == 'Distributor') 
                    {
                        groupMembers.add(new GroupMember(GroupId = DistributorPartnerGroup, UserOrGroupId = u.Id));
                    }
                }
                
                //if(u.Profile.Name == 'Tenable - Technology Partner Community User')
                //{
                    //groupMembers.add(new GroupMember(GroupId = TIPUserGroup, UserOrGroupId = u.Id));
                //}
            }
        }

        /*****************************************************************************************
        *   Record Post-Processing
        ******************************************************************************************/
        if (groupMembers.size() > 0) 
        {
            system.debug('The value of groupMembers is : ' + groupMembers.size());
            insert groupMembers;
        }
    }
    
    //Added by BM on 09/29/2016 to use metadatatypes
    public Static void SetAuthorizationHeaderValues()
    {   
        for(Integration_Setting__mdt is : [SELECT Environment_Ind__c, Client_ID__c,Client_Secret__c,Email_Address__c,Endpoint_URL__c
                FROM Integration_Setting__mdt order by Environment_Ind__c asc limit 1])
        {
            ClientCredentials = is.Client_ID__c + ':' + is.Client_Secret__c;
            headerValue = Blob.valueOf(ClientCredentials);
            authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            endpoint = is.Endpoint_URL__c;
        }
    }
    
    public Static void SetSandboxAuthorizationHeaderValues()
    {   
        for(Integration_Setting__mdt is : [SELECT Environment_Ind__c, Client_ID__c,Client_Secret__c,Email_Address__c,Endpoint_URL__c
                FROM Integration_Setting__mdt WHERE Label = 'Sandbox' limit 1])
        {
            ClientCredentials = is.Client_ID__c + ':' + is.Client_Secret__c;
            headerValue = Blob.valueOf(ClientCredentials);
            authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            endpoint = is.Endpoint_URL__c;
        }
    }
    
    public static Boolean runningInASandbox() 
    {
        return [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
    }
    
    public static String randomString(){
        String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        String randStr = '';
        while (randStr.length() < 3) {
           Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
           randStr += chars.substring(idx, idx+1);
        }
        
        chars = '0123456789';
        while (randStr.length() < 6) {
           Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
           randStr += chars.substring(idx, idx+1);
        }
        
        chars = 'abcdefghijklmnopqrstuvwxyz';
        while (randStr.length() < 13) {
           Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
           randStr += chars.substring(idx, idx+1);
        }
        
        chars = '!@#$%&*';
        while (randStr.length() < 16) {
           Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
           randStr += chars.substring(idx, idx+1);
        }
        
        //system.debug('random string: ' + randStr);
        return randStr; 
    }
    
    
    public class createUserWrapper {
        public String connection {get; set;}
        public String email { get; set; }
        public String name { get; Set; }
        public String password { get; set; }
        public Boolean email_verified { get; set; }
        public a_metadata app_metadata { get; set; }
        public u_metadata user_metadata { get; set; }
    }
    
    public class a_metadata {
        public String given_name { get; set; }
        public String family_name { get; set; }
        public String lms_customer_id {get; set;}
        public String lms_country_name { get; set; }
        public String lms_company_name { get; set; }
        public String docebo_branch_id { get; set; }
    }
    
    public class u_metadata {
        public boolean requires_username { get; set; }
    }
}