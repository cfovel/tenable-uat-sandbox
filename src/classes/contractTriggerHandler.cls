public with sharing class contractTriggerHandler {
/*******************************************************************************
*
* Contract Trigger Handler Class
* Version 1.0a
*******************************************************************************/
    public class ContractBeforeInsertHandler implements Triggers.Handler {
        public void handle() {
            ConvertCurrencyFields(Trigger.new, null);
        }
    }
    
    public class ContractBeforeUpdateHandler implements Triggers.Handler {
        public void handle() {
            ConvertCurrencyFields(Trigger.new, Trigger.oldMap); 
        }
    }
    
    public class ContractAfterUpdateHandler implements Triggers.Handler {
        public void handle() {
            CreateNetSuitePO(Trigger.new, Trigger.oldMap); 
        }
    }
    


    public static void ConvertCurrencyFields(List<Contract> contracts, Map<Id, SObject> oldContractMap)
    {
        List<String> isoCodes = new List<String>();
        Map<String, CurrencyType> conversionMap = new Map<String, CurrencyType>();
        Boolean amountChanged;
        
        // Pre-processing

        for (Contract contract : contracts)
        {
            //Get the old object record, and check if the requested amount has changed. If so, put it in a list 
            //so we only have to use 1 SOQL query to get all conversion rates.
            
            amountChanged = false;
            Contract oldContract;
            if (oldContractMap != null)
            {
                oldContract = (Contract)oldContractMap.get(contract.Id);

                if ((oldContract != null) && (contract.Contract_Value__c != oldContract.Contract_Value__c || contract.Local_Currency__c != oldContract.Local_Currency__c || 
                            contract.Contract_Value_calc__c == null))
                {
                    amountChanged = true;
                }
            }
                
            if ((trigger.isInsert || amountChanged) && contract.Local_Currency__c != null && !contract.Local_Currency__c.contains('(USD)'))
            {
                isoCodes.add(contract.Local_Currency__c.substringBetween('(', ')'));
            }
        } 
        
        if (!isoCodes.isEmpty())                                                                // get conversion rates
        {   
            
            for (CurrencyType curr : [SELECT isoCode, decimalPlaces, conversionRate FROM CurrencyType
                                    WHERE isoCode in :isoCodes])
            {
                conversionMap.put(curr.isoCode, curr);
            }                       
            
        }
        
        conversionMap.put('USD', new CurrencyType(isoCode = 'USD', decimalPlaces = 2, conversionRate = 1.0));
        
        // Processing
 
        for (Contract contract : contracts)
        {
            amountChanged = false;
            Contract oldContract;
            if (oldContractMap != null)
            {
                oldContract = (Contract)oldContractMap.get(contract.Id);
                if ((oldContract != null) && (contract.Contract_Value__c != oldContract.Contract_Value__c || contract.Local_Currency__c != oldContract.Local_Currency__c || 
                                contract.Contract_Value_calc__c == null))
                {
                    amountChanged = true;
                }
            }
            
            if (trigger.isInsert || amountChanged)
            {
                double convRate;
                if (contract.Local_Currency__c != null && contract.Local_Currency__c != 'Other')
                {
                    convRate = conversionMap.get(contract.Local_Currency__c.substringBetween('(', ')')).conversionRate;
                }
                
                if (convRate == null)
                {
                    convRate = 1;
                }
                if (contract.Contract_Value__c != null)
                {
                    contract.Contract_Value_calc__c = contract.Contract_Value__c / convRate;
                }
            }
        }
    }  
    
    public static void CreateNetSuitePO(List<Contract> contracts, Map<Id, SObject> oldContractMap)
    {
    	//set<id> contractIds = new set<id>();
        
        Map<Id, Set<Id>> payloadMap = new Map<Id, Set<Id>>();
        
        for (Contract contract : contracts)
        {
           	Set<id> payloadIds = new Set<id>();
        	Contract oldContract;
            if (oldContractMap != null)
            {
                oldContract = (Contract)oldContractMap.get(contract.Id);
            }
            
            if(oldContract != null && oldContract.Status != contract.Status && contract.Status == 'Signed' && 
            		contract.Payment_Method__c != 'Credit Card' && contract.MDF__c != true && contract.NetSuite_PO_Id__c == null)
            {
            	//contractIds.add(contract.Id);
            	payloadIds.add(contract.Id);
            	payloadIds.add(contract.AccountId);
            	payloadMap.put(contract.Id, payloadIds);
            }
        }        
        //Adding Contract to PayloadId set
        //payloadIds.add(ContractId);
        
        //Contract con = [select id, AccountId from Contract where id = :ContractId];
        //adding vendor account to the payloadIds
        //payloadIds.add(con.AccountId);
        
        //List<sObject> poItemList = [select Id from Purchase_Order_Item__c where Contract__c IN :contractIds ];
        //adding the PO Line Ids to the payload Map
        //for(sObject o: poItemList){
        //    payloadIds.add(string.ValueOf(o.get('Id')));
        //}
        
        for (Contract con :  [select Id, (select Id from Purchase_Order_Items__r) from Contract where Id IN :payloadMap.keyset() ])
        {
        	Set<id> payloadIds = payloadMap.get(con.Id);
        	for (Purchase_Order_Item__c poi : con.Purchase_Order_Items__r)
        	{
        		payloadIds.add(poi.Id);
        	}	
        }
        
        
        for (ProcessInstance pi : [SELECT Id, TargetObjectId, (SELECT Id, StepStatus,Actor.Name, ActorId, Comments FROM StepsAndWorkitems where StepStatus = 'Approved' ORDER BY SystemModStamp)
											FROM ProcessInstance WHERE TargetObjectId IN :payloadMap.keyset()])
		{
			Set<id> payloadIds = payloadMap.get(pi.TargetObjectId);
        	for (ProcessInstanceHistory pih : pi.StepsAndWorkitems)
        	{
        		payloadIds.add(pih.ActorId);
        		system.debug('@@@ Approver name:' + pih.Actor.Name);
        	}	
		}									
        
        if (!payloadMap.isEmpty())
        {
        	for (Id conId : payloadMap.keyset())
        	{
        		//Boolean contractMuleEvent = mulesoftIntegrationController.mulesoftEventHandler(payloadMap.get(conId), conId, 'purchaseOrderCreate');
        		if (!utils.isTest) 
    			{
        			mulesoftIntegrationController.futureMulesoftEventHandler(payloadMap.get(conId), conId, 'purchaseOrderCreate');
    			}
        	}
        }	
		
    }
}