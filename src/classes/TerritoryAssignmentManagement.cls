global class TerritoryAssignmentManagement  implements Schedulable {
    /*************************************************************************************
       To run HOURLY-00, login as Automation User, open Execute Anonymous window, and execute:     
       String CRON_EXP = '0 0 * * * ?';
       TerritoryAssignmentManagement sch = new TerritoryAssignmentManagement();
       system.schedule('Hourly Batch Territory Assignment Management job', CRON_EXP, sch);                                 
     *************************************************************************************/
    
    global void execute(SchedulableContext sc) {
        
        Database.executeBatch(new BatchTerritoryAssignmentManagement(), 200);
        
    }
   
}