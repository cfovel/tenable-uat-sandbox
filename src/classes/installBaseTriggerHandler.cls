public class installBaseTriggerHandler {
/*******************************************************************************
* Install Base Trigger Handler Class
* Version 1.0a
*******************************************************************************/
    public class InstallBaseBeforeInsertHandler implements Triggers.Handler 
    {
        public void handle() 
        {
            PopulateFieldsFromOppProd(Trigger.new);
        }
    } 
    
    public static void PopulateFieldsFromOppProd(List<Install_Base__c> ibList) 
    {
        Set<Id> oppProdIds = new Set<Id>();
        for (Install_Base__c ib : ibList) 
        {
            oppProdIds.add(ib.oppLineItemId__c);
        } 
        
        if(!oppProdIds.isEmpty())
        {  
            Map<Id, OpportunityLineItem> oppProdMap = new Map<Id, OpportunityLineItem>([SELECT Id, Base_Unit_Price__c, Deployment__c 
                    FROM OpportunityLineItem WHERE Id IN :oppProdIds ]);    
            
            for (Install_Base__c ib : ibList) 
            {
                OpportunityLineItem OppLItem = oppProdMap.get(ib.oppLineItemId__c);
                if(OppLItem != NULL)
                {
                    if(OppLItem.Base_Unit_Price__c != NULL)
                    {
                        ib.Base_Unit_Price__c = OppLItem.Base_Unit_Price__c;
                    }
                    
                    ib.Deployment__c = OppLItem.Deployment__c;
                }
            }
        }         
    }
}