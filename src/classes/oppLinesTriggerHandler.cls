public with sharing class oppLinesTriggerHandler {
/*******************************************************************************
*
* Opportunity Products Trigger Handler Class
* Version 1.0a
* Copyright 2006-2016 (c) by CloudLogistix.com
* All Rights Reserved
* Your are allowed to make modifications only for yourself, not for distribution
* without permission from CloudLogistix.
*
*******************************************************************************/

    public static Map <Id, PricebookEntry> priceBookEntries;
    
    public class OppLinesBeforeInsertHandler implements Triggers.Handler {
        public void handle() {
            ProcessBeforeRecords(Trigger.new, Trigger.oldMap);
        }
    }


    public class OppLinesBeforeUpdateHandler implements Triggers.Handler {
        public void handle() {
            ProcessBeforeRecords(Trigger.new, Trigger.oldMap);
        }
    }

    public class OppLinesAfterInsertHandler implements Triggers.Handler {
        public void handle() {
            ProcessAfterRecords(Trigger.new);
        }
    }


    public class OppLinesAfterUpdateHandler implements Triggers.Handler {
        public void handle() {
            ProcessAfterRecords(Trigger.new);
            
        }
    }

    public static void ProcessBeforeRecords(OpportunityLineItem[] records, Map<Id, SObject> oldOppLineItemMap) {
        /*****************************************************************************************
        *   Variable Initialization
        ******************************************************************************************/
        Set <Id> pbeIds = new Set <Id>();
        Set <Id> legacyProdIds = new Set <Id>();
        Set <Id> oppIds = new Set <Id>();
        Set <Id> oppLineIds = new Set <Id>();
        Map<Id,Id> prodReqdByProdMap = new Map<Id,Id>();
        Map<String,Id> oppProdOppLineItemMap = new Map<String,Id>();
        Decimal ACV = 0.0;
        
        /*****************************************************************************************
        *   Record Pre-Processing
        ******************************************************************************************/
        for (OpportunityLineItem ol : records) {                                                    // Loop thru the OppLines we're adding
            pbeIds.add(ol.PricebookEntryId);
            oppIds.add(ol.OpportunityId);
            oppLineIds.add(ol.Id);
        }
        
        for (Opportunity opp : [SELECT Id, (SELECT Id, Legacy_SKU_Lookup__c FROM OpportunityLineItems) FROM Opportunity WHERE Id IN :oppIds])
        {
            for (OpportunityLineItem ol : opp.OpportunityLineItems)
            {
                legacyProdIds.add(ol.Legacy_SKU_Lookup__c);
                oppProdOppLineItemMap.put(opp.Id + ':' + ol.Legacy_SKU_Lookup__c, ol.Id);
            }
        }
        system.debug('@@@ legacyProdIds: ' + legacyProdIds);
        system.debug('@@@ oppProdOppLineItemMap: ' + oppProdOppLineItemMap);
        for (Product2 prod : [SELECT Id, Category__c, Required_Product__c FROM Product2 WHERE Id IN :legacyProdIds])
        {
            if (prod.Required_Product__c != null)
            {
                prodReqdByProdMap.put(prod.Required_Product__c, prod.Id);
            }
        }
        system.debug('@@@ prodReqdByProdMap: ' + prodReqdByProdMap);

        priceBookEntries = new Map <Id, PricebookEntry>
            ([SELECT Id, Product2.Pricing_Term__c, Product2Id, Product2.Family, Product2.SOW_Needed__c,
                Product2.Category__c, Product2.Order_Category__c, Product2.Pre_Billed__c
            FROM PricebookEntry 
            WHERE Id IN :pbeIds]);
            
        
        Map<Id, OpportunityLineItem> oppLineIdMap = new Map<Id, OpportunityLineItem>([Select Id, Opportunity.Name, Opportunity.IsWon, SBQQ__QuoteLine__c, 
            SBQQ__QuoteLine__r.SBQQ__ListPrice__c, SBQQ__QuoteLine__r.Units__c, SBQQ__QuoteLine__r.SBQQ__ListTotal__c,SBQQ__QuoteLine__r.Legacy_SKU_Lookup__r.Category__c,SBQQ__QuoteLine__r.SBQQ__NetTotal__c,
            SBQQ__QuoteLine__r.Legacy_SKU_Lookup__r.License_Type__c,SBQQ__QuoteLine__r.Default_Distributor_Medallion_Discount__c,
            SBQQ__QuoteLine__r.Default_Distributor_Channel_Discount__c,Opportunity.Primary_Distributor__c, Opportunity.Primary_Partner__c,
            SBQQ__QuoteLine__r.Default_Partner_Channel_Discount__c,SBQQ__QuoteLine__r.Discount_Differential__c, SBQQ__QuoteLine__r.Subscription_Term_Calculated__c,
            SBQQ__QuoteLine__r.Unit_of_Measure__c, SBQQ__QuoteLine__r.SBQQ__StartDate__c, SBQQ__QuoteLine__r.SBQQ__EndDate__c, SBQQ__QuoteLine__r.Order_Category_2__c,
            SBQQ__QuoteLine__r.Default_Partner_Medallion_Discount__c
            from OpportunityLineItem where Id in :oppLineIds]);

        /*****************************************************************************************
        *   Record Processing
        ******************************************************************************************/
        for (OpportunityLineItem ol : records) {                                                    // Loop thru the OppLines we're adding
            OpportunityLineItem lineItem = oppLineIdMap.get(ol.Id);
            
            PricebookEntry pbe = priceBookEntries.get(ol.pricebookentryid);
            
            if (trigger.isInsert)
            {
                ACV = 0.0;
                if (ol.TotalPrice != null)
                {
                    ACV = ol.TotalPrice;
                }
                else
                {
                    if (ol.Discount == null)
                    {
                        ACV = ol.UnitPrice*ol.Quantity;
                    }
                    else
                    {
                        ACV = (ol.UnitPrice-(ol.UnitPrice*ol.Discount))*ol.Quantity;
                    }
                }
                
                if (ol.term__c == null && ol.Start_Date__c != null && ol.End_Date__c != null)
                {
                    ol.term__c = Utils.calcTermInMonths(ol.Start_Date__c,ol.End_Date__c);
                }
    
                
                if ((ol.Term__c != null) &&
                    (ol.Term__c != 0) &&
                    (ol.TotalPrice != null)) {
                    ACV = (ol.TotalPrice / ol.Term__c) * ol.Term__c;
                
                    if (pbe != null && (pbe.Product2.Pricing_Term__c == 'Monthly' ||
                        pbe.Product2.Pricing_Term__c == 'Annual')) {
                        if (ol.Term__c > 12) 
                        {
                            ACV = (ol.TotalPrice / ol.Term__c) * 12;
                        }
                    }
                    if (pbe != null && pbe.Product2.Pricing_Term__c == 'Daily') {
                        if (ol.Term__c > 365) {ACV = (ol.TotalPrice / ol.Term__c) * 365;}
                    }
                }
                ol.Annual_Contract_Value__c = ACV;
                
    
                if (pbe != null && (ol.Family__c == null || ol.Family__c == '')) {
                    ol.Family__c = pbe.Product2.Family;
                }
                if (pbe != null && (ol.Category__c == null || ol.Category__c == '')) {
                    ol.Category__c = pbe.Product2.Category__c;
                }
                if (pbe != null && (ol.Order_Category__c == null || ol.Order_Category__c == '')) {
                    ol.Order_Category__c = pbe.Product2.Order_Category__c;
                }
            }
            
            if (pbe != null)
            {
                ol.Pre_Billed__c = pbe.Product2.Pre_Billed__c;
                ol.SOW_Needed__c = pbe.Product2.SOW_Needed__c;
            }           
            
            //Move the logic related to SBQQ_QuoteLine__c fields from process builder/workflow to trigger
            if (lineItem != null){
                Boolean isQuoteLineChanged = false;
            
                if (oldOppLineItemMap != null && ol.SBQQ__QuoteLine__c != null){
                    OpportunityLineItem oldOppLineItem = (OpportunityLineItem)oldOppLineItemMap.get(ol.Id); 
                    isQuoteLineChanged = ol.SBQQ__QuoteLine__c != oldOppLineItem.SBQQ__QuoteLine__c;
                }
                
                //Update Opportunity Line Item if the Quote Line field value is populated or changed and the Opportunity is not ClosedWon
                if ((ol.SBQQ__QuoteLine__c != null || isQuoteLineChanged) && (!lineItem.Opportunity.IsWon)) {
                    if (lineItem.SBQQ__QuoteLine__r.SBQQ__ListPrice__c != null && lineItem.SBQQ__QuoteLine__r.Units__c != null && lineItem.SBQQ__QuoteLine__r.Units__c != 0) {
                        ol.Base_Unit_Price__c = lineItem.SBQQ__QuoteLine__r.SBQQ__ListPrice__c /lineItem.SBQQ__QuoteLine__r.Units__c;
                    }
                    
                    ol.CPQ_List_Price__c = lineItem.SBQQ__QuoteLine__r.SBQQ__ListTotal__c;
                    ol.Category__c = lineItem.SBQQ__QuoteLine__r.Legacy_SKU_Lookup__r.Category__c;
                    
                    //update Discount Percent field
                    if (lineItem.SBQQ__QuoteLine__r.SBQQ__NetTotal__c != null && lineItem.SBQQ__QuoteLine__r.SBQQ__ListTotal__c != null && 
                            lineItem.SBQQ__QuoteLine__r.SBQQ__ListTotal__c != 0){
                        ol.Discount_Percent__c = (1 - (lineItem.SBQQ__QuoteLine__r.SBQQ__NetTotal__c /lineItem.SBQQ__QuoteLine__r.SBQQ__ListTotal__c)) * 100;
                    }
                    else{
                        ol.Discount_Percent__c = 0;
                    }
                    
                    ol.License__c = lineItem.SBQQ__QuoteLine__r.Legacy_SKU_Lookup__r.License_Type__c;
                    ol.Number_of_Units__c = lineItem.SBQQ__QuoteLine__r.Units__c;
                    
                    //update Partner Discount field
                    if (lineItem.SBQQ__QuoteLine__r.SBQQ__NetTotal__c != null && lineItem.SBQQ__QuoteLine__r.SBQQ__ListTotal__c != 0 &&
                            lineItem.SBQQ__QuoteLine__r.SBQQ__ListTotal__c != 0){
                        if (lineItem.Opportunity.Name.contains('NFR') && ((1 - (lineItem.SBQQ__QuoteLine__r.SBQQ__NetTotal__c /lineItem.SBQQ__QuoteLine__r.SBQQ__ListTotal__c)) * 100 == 100)){
                            ol.Partner_Discount__c = 0;
                        }
                        else if (lineItem.SBQQ__QuoteLine__r.Default_Distributor_Medallion_Discount__c != null){
                            ol.Partner_Discount__c = lineItem.SBQQ__QuoteLine__r.Default_Distributor_Medallion_Discount__c;
                        }
                        else if (lineItem.SBQQ__QuoteLine__r.Default_Partner_Medallion_Discount__c != null){
                            ol.Partner_Discount__c = lineItem.SBQQ__QuoteLine__r.Default_Partner_Medallion_Discount__c;
                        }
                        else{
                            ol.Partner_Discount__c = 0;
                        }
                    }
                    else{
                        ol.Partner_Discount__c = 0;
                    }
                    
                    //update registered deal discount
                    if (lineItem.SBQQ__QuoteLine__r.SBQQ__ListTotal__c != 0 && lineItem.SBQQ__QuoteLine__r.Default_Distributor_Channel_Discount__c != null && 
                            lineItem.Opportunity.Primary_Distributor__c != null){
                        ol.Registered_Deal_Discount__c = lineItem.SBQQ__QuoteLine__r.Default_Distributor_Channel_Discount__c;
                    }
                    else if (lineItem.SBQQ__QuoteLine__r.SBQQ__ListTotal__c != 0 && lineItem.Opportunity.Primary_Distributor__c == null && lineItem.Opportunity.Primary_Partner__c != null
                                && lineItem.SBQQ__QuoteLine__r.Default_Partner_Channel_Discount__c != 0) {
                        ol.Registered_Deal_Discount__c = lineItem.SBQQ__QuoteLine__r.Default_Partner_Channel_Discount__c;
                    }
                    else {
                        ol.Registered_Deal_Discount__c = 0;
                    }
                    
                    //update discrentionary discount 
                    Decimal discountPercent = (ol.Discount_Percent__c != null ? ol.Discount_Percent__c : 0);
                    Decimal partnerDiscount = (ol.Partner_Discount__c != null ? ol.Partner_Discount__c : 0);
                    Decimal registeredDealDiscount = (ol.Registered_Deal_Discount__c != null ? ol.Registered_Deal_Discount__c : 0);
                    Decimal discretionaryDiscount = discountPercent - partnerDiscount - registeredDealDiscount;
                    
                    ol.Discretionary_Discount__c = discretionaryDiscount.SetScale(10);
                    
                    ol.Term__c = lineItem.SBQQ__QuoteLine__r.Subscription_Term_Calculated__c;
                    ol.Unit_Type__c = lineItem.SBQQ__QuoteLine__r.Unit_of_Measure__c;
                    ol.Start_Date__c = lineItem.SBQQ__QuoteLine__r.SBQQ__StartDate__c;
                    ol.End_Date__c = lineItem.SBQQ__QuoteLine__r.SBQQ__EndDate__c;
                    ol.Order_Category__c = lineItem.SBQQ__QuoteLine__r.Order_Category_2__c;
                }
            }
            
            // Replace process builder - ACV TCV Updates on OLI
            if (ol.ACV_New__c == null || ol.ACV_New__c != ol.ACV_New_Calc__c)
            {
                ol.ACV_New__c = ol.ACV_New_Calc__c;
            }
            if (ol.ACV_Renewal__c == null || ol.ACV_Renewal__c != ol.ACV_Renewal_Calc__c)
            {
                ol.ACV_Renewal__c = ol.ACV_Renewal_Calc__c;
            }
            if (ol.TCV_New__c == null || ol.TCV_New__c != ol.TCV_New_Calc__c)
            {
                ol.TCV_New__c = ol.TCV_New_Calc__c;
            }
            if (ol.TCV_Renewal__c == null || ol.TCV_Renewal__c != ol.TCV_Renewal_Calc__c)
            {
                ol.TCV_Renewal__c = ol.TCV_Renewal_Calc__c;
            }
            
            system.debug('@@@ legacy sku id:' + ol.Legacy_SKU_Lookup__c + ', get map: ' + prodReqdByProdMap.get(ol.Legacy_SKU_Lookup__c));
            if (prodReqdByProdMap.get(ol.Legacy_SKU_Lookup__c) != null)
            {
                ol.Required_By__c = oppProdOppLineItemMap.get(ol.OpportunityId + ':' + prodReqdByProdMap.get(ol.Legacy_SKU_Lookup__c));
            }
        }
        
    }

    public static void ProcessAfterRecords(OpportunityLineItem[] records) {
        /*****************************************************************************************
        *   Variable Initialization
        ******************************************************************************************/
        OpportunityLineItemSchedule[] olstoadd = new OpportunityLineItemSchedule[0];                // Used to store the records we're going to add
        Set <String> oppids = new Set <String>();                                                   // Used to store the Opportunities we're processing
        
        /*****************************************************************************************
        *   Record Pre-Processing
        ******************************************************************************************/
        /*For (OpportunityLineItem oli : records) {                                                   // Loop thru and get all the OppLines
            oppids.add(oli.id);                                                                     // Save in our list for processing
        }
        
        OpportunityLineItem[] opplines = 
            ([SELECT id, Opportunityid, totalprice, Opportunity.CloseDate, Quantity, 
                    Pricebookentryid, Pricebookentry.Product2.name, PricebookEntry.Product2Id,
                    License__c, Category__c, Family__c, Term__c, Start_Date__c, End_Date__c
            FROM OpportunityLineItem
            WHERE id in :oppids
            AND Discount != 100.0]);
        
        OpportunityLineItemSchedule[] olstodel =                                                    // Go get all related schedule records
            ([SELECT id FROM OpportunityLineItemSchedule 
            WHERE OpportunityLineItemId in :oppids]);
        delete olstodel;  */                                                                          // Delete them since we'll add them anew
        
        /*****************************************************************************************
        *   Record Processing
        ******************************************************************************************/
        /*for (OpportunityLineItem ol : opplines) {                                                   // Loop thru the OppLines we're adding
            PricebookEntry pbe = priceBookEntries.get(ol.pricebookentryid);         
            Date firststartdate = ol.start_date__c;                                                 // Set the first period
            Decimal term = ol.Term__c;                                                              // Save the line item term
            if (term == 0) {                                                                        // Perpetual deal
                term = 12;                                                                          // Spread it out over 12 months
            }
            
            if (pbe != null && pbe.Product2.Pricing_Term__c == 'Daily' && 
                ol.start_date__c != null && ol.end_date__c != null) 
            {
                term = Utils.calcTermInMonths(ol.start_date__c, ol.end_date__c);
            }
            
            for (Integer i=0; i < term; i++) {                                                      // Loop thru the number of periods in our term
                OpportunityLineItemSchedule ols = new OpportunityLineItemSchedule();                // Create a schedule record
                ols.opportunitylineitemid = ol.id;                                                  // Link to the Opportunity Line Item
                ols.type = 'Revenue';                                                               // Schedule Type is Revenue
                ols.revenue  = ol.totalprice / term;                                                // The Revenue is the unit price divided by the term 
                system.debug('The value of ol.TotalPrice is : ' + ol.TotalPrice);
                system.debug('The value of ol.Term__c is : ' + ol.Term__c);
            
                //ols.revenue  = ol.totalprice / ol.term__c;
                
                system.debug('The value of ols.revenue is : ' + ols.revenue);
                ols.scheduledate = firststartdate;                                                  // Set the period date
                firststartdate = firststartdate.addMonths(1);                                       // Add one to the month
                olstoadd.add(ols);                                                                  // Add the schedule record to be added
            }
        }
        insert olstoadd; */                                                                           // Insert the schedule records
    }
   
}