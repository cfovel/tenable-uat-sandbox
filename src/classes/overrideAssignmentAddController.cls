public class overrideAssignmentAddController
{
    private final OverrideAssignment__c namedAcct;
    public Id acctId;
    public Map <String, String> params = new Map <String, String>(); 
    public Boolean pageError { get; set; } 
    public List<OverrideAssignment__c> namedAccts = new List<OverrideAssignment__c>();
       
    
    public overrideAssignmentAddController(ApexPages.StandardController stdController)
    {
        params = ApexPages.currentPage().getParameters();
        pageError = FALSE;
        this.namedAcct = (OverrideAssignment__c)stdController.getRecord();
        namedAcct.approved__c = true;
        namedAcct.ApprovedBy__c = UserInfo.getUserId();
        namedAcct.ApprovedAt__c = DateTime.now();
        Profile p = [select name from Profile where id = :UserInfo.getProfileId()];
        if (p.name == null || (p.name !='System Administrator' && p.name !='Tenable - System Administrator' && p.name != 'Tenable - Sales Ops User'))
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please contact Sales Ops to add Named Account.'));
            pageError = TRUE;
        }
        else
        {
            if ((params != null) &&  (params.get('CF00Nn00000017rxt_lkid') != null)) 
            {
                acctId = params.get('CF00Nn00000017rxt_lkid'); 
            }
            
            if (acctId != null)
            {
                namedAccts = getCurrentOverrideAssignments();
                for (OverrideAssignment__c na : namedAccts)
                {
                    if (na.StartAt__c > Datetime.now())
                    {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Cannot add Named Account when there is an existing future Named Account.'));
                        pageError = TRUE;
                        break;
                    }
                }
            }
        }
    }
    
    public List<OverrideAssignment__c> getCurrentOverrideAssignments ()
    {
        namedAccts = [SELECT Id, Name, Approved__c, StartAt__c, EndAt__c FROM OverrideAssignment__c WHERE Account__c = :acctId ];
        
        return namedAccts;
    }
    /*
    public PageReference save() 
    {
        List<OverrideAssignment__c> namedAcctsToUpsert = new List<OverrideAssignment__c>();
        DateTime timeNow = Datetime.now();
        
        for (OverrideAssignment__c na : namedAccts)
        {
            if (na.Approved__c && na.StartAt__c <= Datetime.now() && (na.endAt__c == null || na.endAt__c > Datetime.now()))
            {
                na.endAt__c = timeNow;
                namedAcctsToUpsert.add(na);
                system.debug('@@@ endAt: ' + na.endAt__c);
            }
        }
        
        namedAcct.startAt__c = timeNow.addSeconds(1);
        system.debug('@@@ startAt: ' + namedAcct.startAt__c);
        namedAcctsToUpsert.add(namedAcct);
        
        try 
        {
                //String returnpage = '/' + entry.quote__c;                                           // Save quote number to return to
                upsert namedAcctsToUpsert;                                                                       // Try deleting it
                //return pageredirect(returnpage);                                                    // Return new page 
                return (new ApexPages.StandardController(namedAcct)).view(); 
        }
        catch (exception e) {
            Utils.addError('Unable to upsert Named Account records: '+e.getMessage());                         // Report error back to the page
            return null;                                                                        // Make sure we don't refresh the page if there's an error
        }
    }    
    */
}