/*************************************************************************************
Class: TestFeatureRequestTriggerHandler
Author: Christine E
Date: 08/29/2018
Details: This test class is for the trigger class TestFeatureRequestTriggerHandler
***************************************************************************************/

@isTest
private class TestFeatureRequestTriggerHandler {

    @isTest static void createFeatureRequest() {
        Utils.isTest = true;
        
        Account a = new Account(Name='Dell SecureWorks', BillingState='CA', BillingCountry='US', website='http://www.test1.com');
        insert a;
        
        Feature_Request__c fr = new Feature_Request__c(
       		Name = 'Test Feature Request 001',
       		Status__c = 'Pending',
        	Product_Group__c = 'Tenable.io Container Security',
        	Business_Problem__c = 'Feature Request Unit Test'
        );
        
        Test.startTest();
        	Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
        	insert fr;
        	
        	Account_Feature_Request__c afr = new Account_Feature_Request__c();
        
	        afr.Account__c = a.Id;
	        afr.Feature_Request__c = fr.Id;
	        insert afr;
        Test.stopTest();
        
        Feature_Request__c freq = [Select Id, Initial_Requester__c from Feature_Request__c where Id = :fr.Id];        
        System.assertEquals(a.Id, freq.Initial_Requester__c);
    }
}