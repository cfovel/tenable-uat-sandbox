public with sharing class Custom_Field_Creation {
    
    public static void createCustomField(string fieldType, string objectName, string label, integer precision, integer scale){
    	string reqBody;
    	list<String> labelList = label.split(' ');
		string fullName = '';
		for (String x : labelList)
		{
		    fullName += x.substring(0,1).toUpperCase()+x.substring(1,x.length()) + ' ';
		}
		fullName = fullName.removeEnd(' ');
		label = fullName;
		fullName =fullName.replace(' ', '_');
    	
    	fullName = objectName + '.' + fullName + '__c';
    	
    	system.debug('Full Name of Custom Field: ' + fullName);
    	
    	
    	if(fieldType == 'Text'){
    		reqBody = '{"FullName":"'+fullName+'","Metadata":{"externalId":false,"label":"'+label+'","length":255,"required":false,"trackHistory":false,"type":"'+fieldType+'","unique":false}}';
    	}else if(fieldType == 'Number'){
    		reqBody = '{"FullName":"'+fullName+'","Metadata":{"externalId":false,"label":"'+label+'","precision": '+precision+',"scale": '+scale+',"required":false,"trackHistory":false,"type":"'+fieldType+'","unique":false}}';	
    	}else if(fieldType == 'DateTime' || fieldType == 'Date'){
    		reqBody = '{"FullName":"'+fullName+'","Metadata":{"externalId":false,"label":"'+label+'","required":false,"trackHistory":false,"type":"'+fieldType+'","unique":false}}';	
    	}else if(fieldType == 'Checkbox'){
    		reqBody = '{"FullName":"'+fullName+'","Metadata":{"externalId":false,"label":"'+label+'","required":false,"trackHistory":false,"type":"'+fieldType+'","unique":false,"defaultValue":"false"}}';	
    	}
    	
    	system.debug('requestBody: ' + reqBody);
    	
    	HttpRequest req = new HttpRequest();
		req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID());
		req.setHeader('Content-Type', 'application/json');
		String domainUrl=URL.getSalesforceBaseUrl().toExternalForm();
		system.debug('********domainUrl:'+domainUrl);
		req.setEndpoint(domainUrl+'/services/data/v36.0/tooling/sobjects/CustomField');
		req.setBody(reqBody);
		req.setMethod('POST');
		
		Http h = new Http();
		HttpResponse res = h.send(req);
		system.debug(res.getBody()); 
		    
    }
    
    public static void queryCustomField(string qryString){
    	
    	string restOfEndPoint = '/services/data/v28.0/tooling/query/?q=Select+id,DeveloperName,Metadata+from+CustomField+where+TableEnumOrId=+\'01Iq00000009k1P\'';
    	
    	///////Query for customFields on tenable io usage log object
		HttpRequest req = new HttpRequest();
		req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID());
		req.setHeader('Content-Type', 'application/json');
		String domainUrl=URL.getSalesforceBaseUrl().toExternalForm();
		system.debug('********domainUrl:'+domainUrl);
		req.setEndpoint(domainUrl + qryString);
		req.setMethod('GET');
		
		Http h = new Http();
		HttpResponse res = h.send(req);
		system.debug(res.getBody());
    	
    }
    
}

//Execute Anonymous for createField
/*
string objectName = 'Tenable_io_Usage_Log__c';
string label = 'users total';
string fieldType = 'Number';
integer precision = 18;
integer scale = 0;

Custom_Field_Creation.createCustomField(fieldType, objectName, label, precision, scale);
*/

//Execute Anonymous for query custom field
/*
string reqBody = '/services/data/v28.0/tooling/query/?q=Select+id,DeveloperName,Metadata+from+CustomField+where+TableEnumOrId=+\'01Iq00000009k1P\'';
Custom_Field_Creation.queryCustomField(reqBody);
*/

//All the Attributes that could be on the custom field
/*
			"Metadata": {
				"caseSensitive": null,
				"customDataType": null,
				"defaultValue": "false",
				"deleteConstraint": null,
				"deprecated": null,
				"description": null,
				"displayFormat": null,
				"displayLocationInDecimal": null,
				"escapeMarkup": null,
				"externalDeveloperName": null,
				"externalId": false,
				"formula": null,
				"formulaTreatBlanksAs": null,
				"inlineHelpText": null,
				"isFilteringDisabled": null,
				"isNameField": null,
				"isSortingDisabled": null,
				"label": "Pendo Feature Asset Management",
				"length": null,
				"maskChar": null,
				"maskType": null,
				"picklist": null,
				"populateExistingRows": null,
				"precision": null,
				"readOnlyProxy": null,
				"referenceTargetField": null,
				"referenceTo": null,
				"relationshipLabel": null,
				"relationshipName": null,
				"relationshipOrder": null,
				"reparentableMasterDetail": null,
				"required": null,
				"restrictedAdminField": null,
				"scale": null,
				"startingNumber": null,
				"stripMarkup": null,
				"summarizedField": null,
				"summaryFilterItems": null,
				"summaryForeignKey": null,
				"summaryOperation": null,
				"trackFeedHistory": null,
				"trackHistory": false,
				"type": "Checkbox",
				"unique": null,
				"urls": null,
				"visibleLines": null,
				"writeRequiresMasterRead": null
			}
*/