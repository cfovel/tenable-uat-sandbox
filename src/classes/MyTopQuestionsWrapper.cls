public without sharing class MyTopQuestionsWrapper {
    @AuraEnabled public String tTitle {get; set;}
    @AuraEnabled public String tBody {get; set;}
    @AuraEnabled public String tType {get; set;}
    @AuraEnabled public String tRecordId {get; set;}
    @AuraEnabled public Date tDate {get; set;}
}