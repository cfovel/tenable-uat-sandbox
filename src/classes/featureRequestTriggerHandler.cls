public class featureRequestTriggerHandler {
    public class FeatureRequestBeforeUpdateHandler implements Triggers.Handler 
    {
        public void handle() 
        {
            SetInitialRequester(Trigger.newMap); 
        }
    } 
    
    public class FeatureRequestAfterInsertHandler implements Triggers.Handler 
    {
        public void handle() 
        {
            FeatureRequestSyncWithJira(Trigger.new, null);
        }
    } 
    
    public class FeatureRequestAfterUpdateHandler implements Triggers.Handler 
    {
        public void handle() 
        {
            FeatureRequestSyncWithJira(Trigger.new, Trigger.oldMap); 
        }
    } 
    
    public static void SetInitialRequester(Map<Id, SObject> newReqMap)
    {
    	List<Id> frIds = new List<Id>();
    	for (Feature_Request__c req : (List<Feature_Request__c>)newReqMap.values())
        {
	    	if (req.Initial_Requester__c == null)
	        {
	        	frIds.add(req.Id);
	        }
        }
        
        if (!frIds.isEmpty())
        {
        	for (Account_Feature_Request__c afr : [SELECT Account__c, Feature_Request__c FROM Account_Feature_Request__c WHERE Feature_Request__c IN :frIds ORDER BY CreatedDate])
        	{
        		Feature_Request__c fr = (Feature_Request__c)newReqMap.get(afr.Feature_Request__c);
        		if (fr.Initial_Requester__c == null)
        		{
        			fr.Initial_Requester__c = afr.Account__c;
        		}
        	}
        }
    }   
    
    public static void FeatureRequestSyncWithJira(List<Feature_Request__c> requests, Map<Id, SObject> oldRequestMap)
    {
        Map<Id, Set<Id>> payloadMap = new Map<Id, Set<Id>>();
        Map<Id, Set<Id>> payloadUpdateMap = new Map<Id, Set<Id>>();

        for (Feature_Request__c req : requests)
        {
            Set<id> payloadIds = new Set<id>();
            Feature_Request__c oldRequest;
            if (oldRequestMap != null)
            {
                oldRequest = (Feature_Request__c)oldRequestMap.get(req.Id);
            }
            system.debug('@@@ req: ' + req);
            system.debug('@@@ oldRequest: ' + oldRequest);
            /*
            if(oldAccount != null && oldAccount.Customer_Info_Id__c != null && oldAccount.NetSuite_Id__c != null && (oldAccount.Name != acct.Name || oldAccount.Type != acct.Type || oldAccount.Subsidiary__c != acct.Subsidiary__c || oldAccount.LMS_Customer_Id__c != acct.LMS_Customer_Id__c || oldAccount.Customer_Info_Id__c != acct.Customer_Info_Id__c|| 
                oldAccount.BillingStreet != acct.BillingStreet || oldAccount.BillingCity != acct.BillingCity || oldAccount.BillingState != acct.BillingState || oldAccount.BillingCountry != acct.BillingCountry || oldAccount.BillingPostalCode != acct.BillingPostalCode || 
                oldAccount.Phone != acct.Phone || oldAccount.isInactive__c != acct.isInactive__c || oldAccount.Hours_Between_Resets__c != acct.Hours_Between_Resets__c || oldAccount.Contacts_Allowed__c != acct.Contacts_Allowed__c || oldAccount.Fax != acct.Fax || 
                oldAccount.Website != acct.Website))
            {
                payloadIds.add(acct.Id);
                payloadMap.put(acct.Id, payloadIds);
            }
            */
            
            if (Trigger.isInsert || (oldRequest != null && oldRequest.Number_of_Accounts__c != req.Number_of_Accounts__c))
            {
                payloadIds.add(req.Id);
                if (Trigger.isInsert)
                {
                	payloadMap.put(req.Id, payloadIds);
                }
                else
                {
                	payloadUpdateMap.put(req.Id, payloadIds);
                }
            }
        }        
        system.debug('@@@ payloadMap: ' + payloadMap);    
        system.debug('@@@ payloadUpdateMap: ' + payloadUpdateMap);    
    
    
        if (!payloadMap.isEmpty() && payloadMap.size() <= 10)
        {
            for (Id reqId : payloadMap.keyset())
            {
                mulesoftIntegrationController.futureMulesoftEventHandler(payloadMap.get(reqId), reqId, 'featureRequest');
            }
        }   
        
        if (!payloadUpdateMap.isEmpty() && payloadMap.size() <= 10)
        {
            for (Id reqId : payloadUpdateMap.keyset())
            {
                mulesoftIntegrationController.futureMulesoftEventHandler(payloadUpdateMap.get(reqId), reqId, 'featureRequestUpdate');
            }
        }   
    } 
}