global class baUpdateFortune1000 implements Database.Batchable<SObject> {
    // To run, login as Automation User, open Execute Anonymous window, and execute:
    //      database.executeBatch(new baUpdateFortune1000(),200);

    global Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator([select Id, D_B_Company__C, New_D_B_Company__c, DUNNS_Formatted__c,Global_Ultimate_Business_Name__c
                                            FROM Fortune_1000_Company__c]);
                                            
                                            
    }

    global void execute(Database.BatchableContext context, List<Fortune_1000_Company__c> scope) {
    /*
        List<String> DUNNSNumbers = new List<String>();
        //Map <Id, List<Fortune_1000_Company__c>> DUNNS1000Map = new Map <Id, List<Fortune_1000_Company__c>> ();      // Map of DUNNS number to Fortune 1000 Company
        Map <String, DandBCompany> DUNNSDandBMap = new Map <String, DandBCompany> ();     // Map of DUNNS number to D&B Company
        for (Fortune_1000_Company__c co : scope) {
            DUNNSNumbers.add(co.DUNNS_Formatted__c);
            //DUNNS1000Map.put(co.Global_Ultimate_DUNS_Number__c, co);
        }
        system.debug('Fortune 1000 Dunns:' + DUNNSNumbers);

        List<DandBCompany> dAndBList = new List<DandBCompany> ([SELECT Id, DunsNumber, GlobalUltimateDunsNumber, FortuneRank
                                    FROM DandBCompany WHERE GlobalUltimateDunsNumber IN :DUNNSNumbers]);  // and FortuneRank <= 0]);        
        for (DandBCompany dbCo : dAndBList) {
            DUNNSDandBMap.put(dbCo.GlobalUltimateDunsNumber, dbCo);
            system.debug('D&B DUNS: ' + dbCo.GlobalUltimateDunsNumber);
        }           
        
        for (Fortune_1000_Company__c co : scope) {
            DandBCompany dbCo = DUNNSDandBMap.get(co.DUNNS_Formatted__c);
            
            if (dbCo != null) {
                co.New_D_B_Company__c = dbCo.Id;
                //co.D_B_Company__c = dbCo.Id;
            }
            else {
                system.debug('D&B Company not found for DUNNS:' + co.DUNNS_Formatted__c + ', Co: ' + co.Global_Ultimate_Business_Name__c);
            }
        }                                           
         
        update scope;
    */                                        
    }

    global void finish(Database.BatchableContext context) {
    }
}