public with sharing class productTriggerHandler {
/*******************************************************************************
*
* Product Trigger Handler Class
* Version 1.0a
* Copyright 2006-2016 (c) by CloudLogistix.com
* All Rights Reserved
* Your are allowed to make modifications only for yourself, not for distribution
* without permission from CloudLogistix.
*
*******************************************************************************/
	public class ProductBeforeInsertHandler implements Triggers.Handler {
		public void handle() {
			ProcessBeforeRecords(Trigger.new);
		}
	} 

	public class ProductBeforeUpdateHandler implements Triggers.Handler {
		public void handle() {
			ProcessBeforeRecords(Trigger.new);
		}
	} 

	public static void ProcessBeforeRecords(Product2[] prods) {
	    /*****************************************************************************************
	    *   Variable Initialization
	    ******************************************************************************************/
	    String searchKey;                                                           				// Store the search key we're creating
		Set <Id> prodids = new Set <Id>();															// Save id's for x-refing
		Map <Id, Decimal> GSA = new Map <Id, Decimal>();											// Save GSA SKU's
	
	    /*****************************************************************************************
	    *   Record Pre-Processing
	    ******************************************************************************************/
	    for (Product2 pr : prods) {                                           						// Loop thru all products being added
			if (pr.GSA_Equivalent__c != null) {
				prodids.add(pr.GSA_Equivalent__c);												// Found a GSA SKU to go get pricing for
			}
	    }

		PricebookEntry[] GSASKUs = 
			([SELECT product2id, unitprice
			FROM PricebookEntry
			WHERE Product2id IN :prodids]);
		for (PricebookEntry prb : GSASKUs) {
			GSA.put(prb.product2id, prb.unitprice);
		}

	    /*****************************************************************************************
	    *   Record Processing
	    ******************************************************************************************/
	    for (Product2 pr : prods) {                                           						// Loop thru all products being added
	        searchKey = Utils.fixnullString(pr.category__c) + ':';                  				// Create our search key starting with category
	        searchKey += Utils.fixnullString(pr.family) + ':';                      				// ... add family
	        searchKey += Utils.fixnullString(pr.geography__c) + ':';                				// ... add geo
	        searchKey += Utils.fixnullString(pr.license_type__c) + ':';             				// ... add license type
	        searchKey += Utils.fixnullString(pr.Unit__c) + ':';             						// ... add unit
	        searchKey += Utils.fixnullString(pr.name) + ':';             							// ... add name
	        pr.searchKey__c = searchkey;                                            				// Store in the record
			if ((pr.GSA_Equivalent__c != null) &&
				(GSA.size() != null) &&
				(GSA.get(pr.GSA_Equivalent__c) != null)) {
				pr.GSA_Price__c = GSA.get(pr.GSA_Equivalent__c);
			}
	    } 
	}
}