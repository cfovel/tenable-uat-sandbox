global class TestResponseGenerator implements HttpCalloutMock {

    global HTTPResponse respond(HTTPRequest req) {
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setStatusCode(200);
        
        if (req.getMethod() == 'POST' && req.getEndpoint().contains('salesOrder')) {
            res.setBody('{"results":[],"status":"OK"}');
        }else if(req.getMethod() == 'GET' && req.getEndpoint().contains('productCalculators')){
            res.setBody('{"usageData":[{"container":{"uuid":"6dba5011-1ad9-422b-ab4a-a99a0e015e8e","numericId":123120,"siteName":"us-2a","region":"US"},"hosts":10,"applications":50,"pciSubmissions":1000}],"products":[{"product":"Hosts","quantity":"50","rate":"178.15"},{"product":"Applications","quantity":"100","rate":"10.57"},{"product":"Submissions","quantity":"500","rate":"520.00"}]}');
        }else if(req.getMethod() == 'POST' && req.getEndpoint().contains('createUser')){
            res.setStatusCode(200);
            res.setBody('{"email":"john.doe@gmail.com","email_verified":false,"username":"johndoe","phone_number":"+199999999999999","phone_verified":false,"user_id":"usr_5457edea1b8f33391a000004","created_at":"","updated_at":"","identities":[{"connection":"Initial-Connection","user_id":"5457edea1b8f22891a000004","provider":"auth0","isSocial":false}],"app_metadata":{"given_name":"Roy","family_name":"Jones","lms_customer_id":"9897879","lms_country_name":"United States","lms_company_name":"Tenable - Internal Development","docebo_branch_id":"customer"},"user_metadata":{"requires_username":false},"picture":"","name":"","nickname":"","multifactor":[""],"last_ip":"","last_login":"","logins_count":0,"blocked":false,"given_name":"","family_name":""}');
        }else if (req.getMethod() == 'POST' && req.getEndpoint().contains('comments')) {
            res.setBody('{"success":true,"commentId":null,"issueNumber":"CSDEV-17"}');
        }else if (req.getMethod() == 'POST' && req.getEndpoint().contains('tns-salesforce-sb.cloudhub.io/1.0/issues/issues')) {
            res.setBody('{"success":true}');
        }else if (req.getMethod() == 'GET' && req.getEndpoint().contains('statistics?date')) {
            res.setBody('[{"container_uuid":"90b89808-914b-4afa-bded-c74b740c7379","measurement_date":"2017-03-18","measurement_timestamp":"2017-03-18T02:15:00.08Z","container_region":"dev","container_site_name":"us-2b","container_name":"Demo Container - jreeder","container_id":197,"container_creation_date":"2017-01-04T23:13:02Z","enabled_features":["analytics","indexing_v2","reporting","suggest_feature","state"],"license_assets":2048,"license_agents":512,"license_scanners":5,"license_users":10,"license_domains":["demo.tenable"],"license_expiration_date":"2030-01-01T00:00:00Z","license_is_expired":false,"license_modification_date":"2017-01-23T18:25:03Z","users_total":1,"users_api_total":1,"users_active_30_days":0,"users_active_60_days":1,"users_active_90_days":1,"users_domain_total":1,"users_last_login_date":"2017-02-01T15:15:34Z","agents_total":0,"agents_active":0,"scanners_total":2,"scanners_active":0},{"container_uuid":"73a7ced3-7150-4bfa-a0a2-2f562d208d2d","measurement_date":"2017-03-18","measurement_timestamp":"2017-03-18T02:15:00.08Z","container_region":"dev","container_site_name":"us-2b","container_name":"Demo Container - creid","container_id":173,"container_creation_date":"2017-01-04T23:10:23Z","enabled_features":["analytics","indexing_v2","reporting","suggest_feature","state"],"license_assets":2048,"license_agents":512,"license_scanners":5,"license_users":10,"license_domains":["demo.tenable"],"license_expiration_date":"2029-12-31T05:00:00Z","license_is_expired":false,"license_modification_date":"2017-01-31T18:50:11Z","users_total":1,"users_api_total":1,"users_active_30_days":0,"users_active_60_days":1,"users_active_90_days":1,"users_domain_total":1,"users_last_login_date":"2017-02-14T21:35:29Z","agents_total":0,"agents_active":0,"scanners_total":2,"scanners_active":0},{"container_uuid":"b46d843d-dd2a-41e0-b4be-95b201cd8a96","measurement_date":"2017-03-18","measurement_timestamp":"2017-03-18T02:15:00.08Z","container_region":"dev","container_site_name":"us-2b","container_name":"Demo Container - bprime","container_id":169,"container_creation_date":"2017-01-04T23:09:56Z","enabled_features":["analytics","indexing_v2","reporting","suggest_feature","state"],"license_assets":2048,"license_agents":512,"license_scanners":5,"license_users":10,"license_domains":["demo.tenable"],"license_expiration_date":"2029-12-31T05:00:00Z","license_is_expired":false,"license_modification_date":"2017-01-31T18:48:41Z","users_total":1,"users_api_total":1,"users_active_30_days":1,"users_active_60_days":1,"users_active_90_days":1,"users_domain_total":1,"users_last_login_date":"2017-03-17T20:39:35Z","agents_total":0,"agents_active":0,"scanners_total":2,"scanners_active":0}]');
        }else if (req.getMethod() == 'POST' && req.getEndpoint().contains('/dbconnections/change_password')) {
            res.setHeader('Content-Type', 'application/x-www-form-urlencoded');
            res.setStatusCode(249);
            res.setBody('?client_id = kIto4LOCjTTzHlFZHjIp8Pd7BHOp6krv&email=test@test.com&connection=Username-Password-Authentication');
        }else if(req.getMethod() == 'POST' && req.getEndpoint().contains('vendor')){
            res.setStatusCode(200);
            res.setBody('{"success": true,"id": "80021000000B5lSAAS"}');
        }else if(req.getMethod() == 'POST' && req.getEndpoint().contains('purchaseOrder')){
            res.setStatusCode(200);
            res.setBody('{"success": true,"id": "80021000000B5lSAAS"}');
        }else if (req.getMethod() == 'POST' && req.getEndpoint().contains('installbase/demoKey')) {
            res.setStatusCode(200);
            res.setBody('{"success": true,"id": "80021000000B5lSAAS"}');
        }
        else if(req.getMethod() == 'GET' && req.getEndpoint().contains('account/products')){
            res.setBody('{"maintenanceCode":"G2PB-GFTM","productID":14,"productSize":1,"companyName":"Test Guilford Enterprises Tenable","hostname":"","partNumber":"APP-SR100-3","maintenanceEndDate":1521331200000,"maintenanceID":160304,"productName":"Appliance","productInstanceID":90732,"customerID":20433}');
        }
        else if(req.getMethod() == 'GET' && req.getEndpoint().contains('account/contacts')){
            res.setBody('{"address_address":"joel@guil4d.net","customerContactID":60006,"name_last":"Guilford","name":"Test Guilford Enterprises Tenable","name_first":"Joel","customerID":20433}');
        }
        else if(req.getMethod() == 'GET' && req.getEndpoint().contains('account/assigned')){
            res.setBody('{"nameFirst":"Joel","partNumber":"APP-SR100-3","viewState":3,"maintenanceID":160304,"customerID":20433,"productInstanceID":90732,"maintenanceCode":"G2PB-GFTM","productID":14,"customerContactID":60006,"productSize":1,"companyName":"Test Guilford Enterprises Tenable","emailAddress":"joel@guil4d.net","hostname":"","nameLast":"Guilford","maintenanceEndDate":1521331200000,"productName":"Appliance"}');
        }
        else if(req.getMethod() == 'GET' && req.getEndpoint().contains('account/view')){
            res.setBody('{"partNumber":"SERV-NES","viewState":3,"maintenanceID":322558,"customerID":52333,"productInstanceID":166745,"lastName":"Oni","maintenanceCode":"MDXK-2F47-KAQS-NXSY","productID":5,"productSize":0,"firstName":"Oluwaseyi","companyName":"Sterling Bank","hostname":"","maintenanceEndDate":1556755200000,"productName":"Nessus Professional"}');
        }
        else if(req.getMethod() == 'POST' && req.getEndpoint().contains('account/update/viewstate')){
            res.setStatusCode(200);
            res.setBody('{"success": true}');
        }else if(req.getMethod() == 'GET' && req.getEndpoint().contains('tenableio/container/')){
            res.setStatusCode(200);
            res.setBody('{"agents":-1,"ips":-1,"scanners":-1,"expiration_date":1542430800,"users":10,"evaluation":true,"apps":{"consec":{"mode":"eval","expiration_date":1542430800},"pci":{"mode":"basic"},"was":{"mode":"eval","expiration_date":1542430800},"vm":{"mode":"eval","expiration_date":1538577053}}}');
        }else if(req.getMethod() == 'POST' && req.getEndpoint().contains('tenableio/container/')){
            res.setStatusCode(200);
            res.setBody('{"agents":-1,"ips":-1,"scanners":-1,"expiration_date":1542430800,"users":10,"evaluation":true,"apps":{"consec":{"mode":"eval","expiration_date":1542430800},"pci":{"mode":"basic"},"was":{"mode":"eval","expiration_date":1542430800},"vm":{"mode":"eval","expiration_date":1538577053}}}');
        }else if(req.getMethod() == 'POST' && req.getEndpoint().contains('docebo/notification/')){
            res.setStatusCode(200);
            res.setBody('{"success": true}');
        }
        return res;
    }
}