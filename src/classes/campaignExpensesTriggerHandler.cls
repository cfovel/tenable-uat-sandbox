public with sharing class campaignExpensesTriggerHandler {
/*******************************************************************************
*
* Campaign Expenses Trigger Handler Class
* Version 1.0a
* Copyright 2006-2013 (c) by CloudLogistix.com
* All Rights Reserved
* Your are allowed to make modifications only for yourself, not for distribution
* without permission from CloudLogistix.
*
*******************************************************************************/
	/*****************************************************************************************
	*
	Constant Declaration
	******************************************************************************************/
	/*****************************************************************************************
	*
	Variable Initialization
	******************************************************************************************/
	
	/*****************************************************************************************
	*
	Common Functions
	******************************************************************************************/
	public static void ProcessUpdates(Campaign_Expenses__c[] records) {
		/*****************************************************************************************
		*
		Variable Initialization
		******************************************************************************************/
		Set <Id> campaignIDs = new Set <Id>();
		// Set declaration for all campaign ids to process
		/*****************************************************************************************
		*
		Record Processing
		******************************************************************************************/
		if (records != null) {																		// If we have records to process
			for (Campaign_Expenses__c ce : records) {												// ... then
				campaignIDs.add(ce.campaign__c);													// ... go save all the campaign ids to update
			}
		}
		/*****************************************************************************************
		*
		Campaign Record Processing
		******************************************************************************************/
		Campaign[] campaignIdToUpdate = new Campaign[0];											// List to store campaign records we're updating
		Campaign_Expenses__c[] celines = ([SELECT Cost__c, Campaign__c								// Go get all related records
			FROM Campaign_Expenses__c 
			WHERE Campaign__c IN : campaignIDs]);
		for (Id campaignID : campaignIDs) {															// Loop thru all the Campaign's we're updating
			Decimal tempTotal = 0.0;																// Reset total to zero each time
			for (Campaign_Expenses__c ce : celines) { 												// loop thru expenses
				if (ce.campaign__c == campaignId) {													// Does the campaign match the current loop one?
					tempTotal += ce.Cost__c;														// ... Increment total up by costs
				}
			}
			campaignIdToUpdate.add(
			new Campaign(id=campaignID, ActualCost=tempTotal));										// Save record for updating
		}
		if (campaignIdToUpdate.size()>0) {update campaignIdToUpdate;}								// Push updates back out into Campaign Records
	}
	
	
	/*****************************************************************************************
	*
	Trigger Functions
	******************************************************************************************/
	public class CampaignExpensesAfterInsertHandler implements Triggers.Handler {
		public void handle() {
			/*****************************************************************************************
			*
			Record Processing
			******************************************************************************************/
			ProcessUpdates(Trigger.new);
		}
	} 
	public class CampaignExpensesAfterUpdateHandler implements Triggers.Handler {
		public void handle() {
			/*****************************************************************************************
			*
			Record Processing
			******************************************************************************************/
			ProcessUpdates(Trigger.new);
		}
	} 
	
	public class CampaignExpensesAfterDeleteHandler implements Triggers.Handler {
		public void handle() {
			/*****************************************************************************************
			*
			Record Processing
			******************************************************************************************/
			ProcessUpdates(Trigger.old);
		}
	} 
	public class CampaignExpensesAfterUndeleteHandler implements Triggers.Handler {
		public void handle() {
			/*****************************************************************************************
			*
			Record Processing
			******************************************************************************************/
			ProcessUpdates(Trigger.new);
		}
	}

}