/*************************************************************************************
Class: CtrlAskAQuestion
Author: Bharathi.M
Details: This controller provides functionality to the AskAQuestion Lightning component.
It is used to ask a question on the community.
History : Created 11/6/2017 (98% code coverage)
***************************************************************************************/
public class CtrlAskAQuestion 
{
    @auraEnabled
    public static map<Id,String> GetPostTovalues()
    {
        map<Id,String> options = new map<Id,String>();
        list<Customer_Support_Community_Topics__c> lstTopics = new list< Customer_Support_Community_Topics__c >(
            [SELECT Name,Topic_Name__c,Topic_Type__c,Community_Id__c FROM Customer_Support_Community_Topics__c 
             WHERE Topic_Type__c='Navigational' AND Active__c = TRUE ORDER BY Topic_Name__c ASC]);
        
        for(Customer_Support_Community_Topics__c c: lstTopics)
        {
            if(!options.containsKey(c.Name))
            {
                options.put(c.Name,c.Topic_Name__c);
            }
        }
        
        return options;
    }
    
    @AuraEnabled
    public static String GetUserId()
    {
        Profile p = [SELECT Name,UserLicense.Name FROM Profile WHERE Id=: userinfo.getProfileId()];
        return p.UserLicense.Name;
    }
    
    @auraEnabled
    public static map<Id,String> GetTopicvalues()
    {
        map<Id,String> options = new map<Id,String>();
        list<Customer_Support_Community_Topics__c> lstTopics = new list< Customer_Support_Community_Topics__c >(
            [SELECT Name,Topic_Name__c,Topic_Type__c,Community_Id__c FROM Customer_Support_Community_Topics__c 
             WHERE Topic_Type__c='Product' ORDER BY Topic_Name__c ASC]);
        
        for(Customer_Support_Community_Topics__c c: lstTopics)
        {
            if(!options.containsKey(c.Name))
            {
                options.put(c.Name,c.Topic_Name__c);
            }
        }
        
        return options;
    }
    
    @AuraEnabled
    public static Id InsertQuestionRecords(string PostId, string Question, string Details, string TopicId, String FileTitle,String FilePath, String FileData)
    {
        Id FeedItemId;
        Map<String, Customer_Support_Community_Topics__c> allTopics = Customer_Support_Community_Topics__c.getAll();
        list<TopicAssignment> lstta = new list<TopicAssignment>();
        string CommunityId;
        
        try
        {
            if(Userinfo.getUserId() != NULL && Userinfo.getUserId() != '')
            {
                if(!allTopics.isEmpty())
                {
                    if(allTopics.containsKey(PostId))
                    {
                        Customer_Support_Community_Topics__c cs = allTopics.get(PostId);
                        CommunityId = cs.Community_Id__c;
                    }
                }
                //create and insert post
                FeedItem post = new FeedItem();
                post.Body = Details; 
                post.ParentId = Userinfo.getUserId(); //UserId of the user creating this
                post.Title = Question; //Need to add what the user typed in the question text box
                post.Type = 'QuestionPost';
                post.IsRichText = true;
                post.NetworkScope = CommunityId; //CommunityId from custom settings
                insert post;
                
                FeedItemId = post.Id;
                
                if(PostId != NULL)
                {
                    lstta.add(new TopicAssignment(TopicId  = PostId,NetworkId = CommunityId,EntityId = FeedItemId));
                }
                
                if(TopicId != NULL)
                {
                    lstta.add(new TopicAssignment(TopicId  = TopicId,NetworkId = CommunityId ,EntityId = FeedItemId));
                }
                
                if(!lstta.isEmpty())
                {
                    insert lstta;
                }
                
                if(FilePath != '' && FilePath != NULL && FileTitle != NULL && FileTitle != '')
                {
                    ContentVersion cv = new ContentVersion();
                    //cv.versionData = Blob.valueOf(FilePath);
                    system.debug('**FILE VESRION DATA '+FileData);
                    system.debug('File title **'+FileTitle);
                    //cv.versionData = Blob.valueOf(FileData);
                    cv.versionData= EncodingUtil.base64Decode(FileData);
                    cv.title=FileTitle;
                    cv.PathOnClient=FilePath;
                    insert cv;
                    system.debug('Contentversion'+cv);
                    
                    Id cvId = [SELECT Id FROM ContentVersion WHERE Title=: FileTitle LIMIT 1].Id;
                    
                    FeedAttachment fattach = new FeedAttachment();
                    fattach.FeedEntityId = FeedItemId;
                    fattach.Type = 'content';
                    fattach.RecordId = cv.Id; //This should have the contentVersionId
                    
                    insert fattach;	
                    system.debug('** Feed attachement'+fattach);
                }
            }
            else
            {
                return 'You need to be logged in to submit a question';
            }
        }
        catch(exception e)
        {
            system.debug('There was an error and the error is : ' + e.getMessage());
        }
        
        return FeedItemId;
    }
}