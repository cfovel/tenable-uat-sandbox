public without sharing class Tenable_OnboardingController {
   @auraEnabled
    public static User getUserRecord(){
        system.debug('test user');
        User currentUser = [SELECT Completed_Groups_Slide__c, Completed_Notification_Slide__c, Completed_Profile_Slide__c, Completed_Topics_Slide__c,
                                    Completed_Tours_Slide__c, Completed_Welcome_Slide__c, FirstName, LastName, Onboarding_Complete__c
                                   FROM User 
                                   WHERE Id = :userinfo.getuserId() limit 1];
        
        return currentUser;   
    }
    
    @auraEnabled
    public static void updateUserNames(User currentUser){
        User objUser = new User(Id = currentUser.Id, FirstName = currentUser.FirstName, LastName = currentUser.LastName);
        update objUser;
    }
    
    @auraEnabled
    public static boolean getEmailPreference(){
        Id networkId = Network.getNetworkId();
	 	List<NetworkMember> networkMembers = [Select PreferencesDisableAllFeedsEmail
                                FROM NetworkMember
                                WHERE MemberId = :userInfo.getUserId() AND networkId = :networkId];
        if(networkMembers != null && networkMembers.size() > 0){
            NetworkMember member = networkMembers[0];
            if(member != null){
                return member.PreferencesDisableAllFeedsEmail;
            }
        }
        return false;
    }
    
    @auraEnabled
    public static void updatePreferences(Boolean decision){
        Id networkId = Network.getNetworkId();
        List<NetworkMember> networkMembers = [Select PreferencesDisableAllFeedsEmail
                                FROM NetworkMember
                                WHERE MemberId = :userInfo.getUserId() AND networkId = :networkId];
        if(networkMembers != null && networkMembers.size() > 0){
            NetworkMember member = networkMembers[0];
            if(!decision){
                system.debug('in false');
                member.PreferencesDisableAllFeedsEmail=true;
            } else if(decision){
                system.debug('in true');
                member.PreferencesDisableAllFeedsEmail=false;
            }
            update member;
            system.debug('updated!!');
        }
    }

    
        @auraEnabled
    public static void insertGroupMember(String groupOneId, String groupOneEmail, Boolean groupOneJoined, String groupTwoId, String groupTwoEmail, Boolean groupTwoJoined,
                                        String groupThreeId, String groupThreeEmail, Boolean groupThreeJoined, String groupFourId, String groupFourEmail, Boolean groupFourJoined){
                system.debug('groupOneEmail: ' + groupOneEmail);
            system.debug('groupOneEmail: ' + groupTwoEmail);
            system.debug('groupOneEmail: ' + groupThreeEmail);
            system.debug('groupOneEmail: ' + groupFourEmail);
            list <String> groupIds = new list <String>{groupOneId, groupTwoId, groupThreeId, groupFourId};
            list <CollaborationGroupMember> membershipsToDelete = new list <CollaborationGroupMember>();

            set <String> existingMembershipIds = new set <String>();
            Map <String, CollaborationGroupMember> membershipGroupToMemberMap = new Map <String, CollaborationGroupMember>();
            for (CollaborationGroupMember member : [Select Id, CollaborationGroupId
                                                    FROM CollaborationGroupMember
                                                    WHERE memberId = :userInfo.getUserId() AND
                                                    CollaborationGroupId in :groupIds]){
                existingMembershipIds.add(member.CollaborationGroupId);
                membershipGroupToMemberMap.put(member.CollaborationGroupId, member);
            }



            // delete membership for each group if it exists in the db but the joined attribute was changed to false in the component
            if (existingMembershipIds.size() > 0){
                if(!groupOneJoined && existingMembershipIds.contains(groupOneId))
                    membershipsToDelete.add(membershipGroupToMemberMap.get(groupOneId));
                if(!groupTwoJoined && existingMembershipIds.contains(groupTwoId))
                    membershipsToDelete.add(membershipGroupToMemberMap.get(groupTwoId));
                if(!groupThreeJoined && existingMembershipIds.contains(groupThreeId))
                    membershipsToDelete.add(membershipGroupToMemberMap.get(groupThreeId));
                if(!groupFourJoined && existingMembershipIds.contains(groupFourId))
                    membershipsToDelete.add(membershipGroupToMemberMap.get(groupFourId));

                system.debug('membershipsToDelete: ' + membershipsToDelete);
                delete membershipsToDelete;
            }

            list <CollaborationGroupMember> collabGroups = new list <CollaborationGroupMember>();
            Id userId = userInfo.getUserId();
            Map <String, String> notificationMap = new Map<String, String>{'Daily'=>'D','Weekly'=>'W','Never'=>'N','On Every Post'=>'P'};


            system.debug('membershipGroupToMemberMap: ' + notificationMap);
            system.debug('notificationmap: ' + notificationMap.get('Daily'));
            system.debug('notificationmap: ' + notificationMap.get('Weekly'));
            system.debug('notificationmap: ' + notificationMap.get('Never'));
            system.debug('notificationmap: ' + notificationMap.get('On Every Post'));
            // add membership for each group if joined attribute was changed to true in the component
            if(groupOneJoined && !existingMembershipIds.contains(groupOneId)){
                CollaborationGroupMember newMember = new CollaborationGroupMember();
                newMember.CollaborationGroupId = groupOneId;
                newMember.MemberId = userId;
                system.debug('1: ' + groupOneEmail);
                system.debug('membershipGroupToMemberMap: ' + notificationMap);
                system.debug('map test: ' + notificationMap.get(groupOneEmail));
                newMember.NotificationFrequency = notificationMap.get(groupOneEmail);
                collabGroups.add(newMember);
            }
            if(groupTwoJoined && !existingMembershipIds.contains(groupTwoId)){
                CollaborationGroupMember newMember = new CollaborationGroupMember();
                newMember.CollaborationGroupId = groupTwoId;
                newMember.MemberId = userId;
                newMember.NotificationFrequency = notificationMap.get(groupTwoEmail);
                system.debug('2: ' + groupTwoEmail);
                system.debug('membershipGroupToMemberMap: ' + notificationMap);
                system.debug('map test: ' + notificationMap.get(groupOneEmail));
                collabGroups.add(newMember);
            }
            if(groupThreeJoined && !existingMembershipIds.contains(groupThreeId)){
                CollaborationGroupMember newMember = new CollaborationGroupMember();
                newMember.CollaborationGroupId = groupThreeId;
                newMember.MemberId = userId;
                newMember.NotificationFrequency = notificationMap.get(groupThreeEmail);
                system.debug('3: ' + groupThreeEmail);
                system.debug('membershipGroupToMemberMap: ' + notificationMap);
                system.debug('map test: ' + notificationMap.get(groupOneEmail));
                collabGroups.add(newMember);
            }
            if(groupFourJoined && !existingMembershipIds.contains(groupFourId)){
                CollaborationGroupMember newMember = new CollaborationGroupMember();
                newMember.CollaborationGroupId = groupFourId;
                newMember.MemberId = userId;
                newMember.NotificationFrequency = notificationMap.get(groupFourEmail);
                system.debug('4: ' + groupFourEmail);
                system.debug('membershipGroupToMemberMap: ' + notificationMap);
                system.debug('map test: ' + notificationMap.get(groupOneEmail));
                collabGroups.add(newMember);
            }

            if (collabGroups.size() > 0){
                insert collabGroups;
            }


    }
    
        @auraEnabled
    public static void completeSlide(String slide){
        String queryString;
        Map <String, String> slideMap = new Map <String, String>{'Welcome'=>'Completed_Welcome_Slide__c', 'Profile'=>'Completed_Profile_Slide__c', 'Notification'=>'Completed_Notification_Slide__c',
            													'Topic'=>'Completed_Topics_Slide__c', 'Group'=>'Completed_Groups_Slide__c', 'Tours'=>'Completed_Tours_Slide__c'};
        String slideName = slideMap.get(slide);
        String userId = UserInfo.getUserId();

            if(slide != 'Done') {
                queryString = 'Select ' + slideName + ', Onboarding_Complete__c FROM User WHERE Id = :userId';
            } else {
                queryString = 'Select Onboarding_Complete__c FROM User WHERE Id = :userId';
            }
        User userRecord = Database.query(queryString);

            // complete slide checkbox as 'Save & Next' is hit.  These checkboxes keep track of the user's progress, and also
            // direct the user to the last uncompleted slide when this component is next initalized
        if(slide=='Welcome'){  
            userRecord.Completed_Welcome_Slide__c=True;
        } else if(slide=='Profile'){  
            userRecord.Completed_Profile_Slide__c=True;
        } else if(slide=='Notification'){  
            userRecord.Completed_Notification_Slide__c=True;
        } else if(slide=='Topic'){  
            userRecord.Completed_Topics_Slide__c=True;
        } else if(slide=='Group'){  
            userRecord.Completed_Groups_Slide__c=True;
        } else if(slide=='Tours'){  
            userRecord.Completed_Tours_Slide__c=True;
        } else if (slide=='Done'){
            userRecord.Onboarding_Complete__c=True;
            userRecord.Onboarding_Complete_Date__c = System.today();
        }
            system.debug('userrecord: ' +  userRecord);
        update userRecord;
    }
    
    @auraEnabled
    public static void updateReputationPoints(){
       networkMember userNetworkMember = [SELECT Id, memberId, ReputationPoints 
                                          FROM NetworkMember 
                                          WHERE MemberId = :userInfo.getUserId() limit 1]; 
            userNetworkMember.ReputationPoints += 10;
         	update userNetworkMember;
    }
    
    @auraEnabled
    public static list <Tenable_OnboardingWrapper> getTopics(list <Id> topicIds ){
        Id networkId = Network.getNetworkId();
        List<Tenable_OnboardingWrapper> topicWrappers = new List<Tenable_OnboardingWrapper>();
        Set <Id> topicsAlreadyFollowedIds = new Set <Id>();
        for (EntitySubscription member : [SELECT parentId 
                                          FROM EntitySubscription 
                                          WHERE SubscriberId = :userInfo.getUserId() AND NetworkId = :networkId limit 500]){
            topicsAlreadyFollowedIds.add(member.parentId);
        }
        
       for (Topic top : [Select Id, Name
                        FROM Topic 
                        WHERE Id in :topicIds]){

            Tenable_OnboardingWrapper wrapper = new Tenable_OnboardingWrapper(top.Name, top.Id, false);

            if(topicsAlreadyFollowedIds.contains(top.Id)){
                wrapper.following = True;
            }
            topicWrappers.add(wrapper);
        }
        return topicWrappers;
    }
    
     @auraEnabled
    public static list <Tenable_OnboardingWrapper> getGroups(list <Id> groupIds ){
        Map <String, String> notificationMap = new Map<String, String>{'D'=>'Daily','W'=>'Weekly','N'=>'Never','P'=>'On Every Post'};
            
        List<Tenable_OnboardingWrapper> groupWrappers = new List<Tenable_OnboardingWrapper>();
        Set <String> groupsAlreadyJoinedIds = new Set <String>();
        Map<String, String> mapGroupNotificationFrequency = new Map<String, String>();
        for (CollaborationGroupMember grp : [SELECT NotificationFrequency, CollaborationGroupId  
                                          FROM CollaborationGroupMember  
                                          WHERE MemberId = :userInfo.getUserId()]){
            groupsAlreadyJoinedIds.add(grp.CollaborationGroupId);
			mapGroupNotificationFrequency.put(grp.CollaborationGroupId, grp.NotificationFrequency);
        }
        
       for (CollaborationGroup grp : [Select Id, Name
                        FROM CollaborationGroup 
                        WHERE Id in :groupIds]){

            Tenable_OnboardingWrapper wrapper = new Tenable_OnboardingWrapper(grp.Name, grp.Id, false);
            if(groupsAlreadyJoinedIds.contains(grp.Id)){
                wrapper.following = True;
                wrapper.notificationFrequency = notificationMap.get(mapGroupNotificationFrequency.get(grp.Id));
            }
            groupWrappers.add(wrapper);
        }
        return groupWrappers; //
    }

    @auraEnabled
    public static Tenable_OnboardingWrapper getGroup(Id groupId){
        Map <String, String> notificationMap = new Map<String, String>{'D'=>'Daily','W'=>'Weekly','N'=>'Never','P'=>'On Every Post'};
        system.debug('groupId: ' + groupId);
        Boolean following = false;
        Set <String> groupsAlreadyJoinedIds = new Set <String>();
        Map<String, String> mapGroupNotificationFrequency = new Map<String, String>();
        for (CollaborationGroupMember grp : [SELECT CollaborationGroupId, NotificationFrequency
                                                FROM CollaborationGroupMember
                                                WHERE MemberId = :userInfo.getUserId() AND
                                                CollaborationGroupId = :groupId]){
            groupsAlreadyJoinedIds.add(grp.CollaborationGroupId);
			mapGroupNotificationFrequency.put(grp.CollaborationGroupId, grp.NotificationFrequency);
        }

        List<CollaborationGroup> grpList = [Select Id, Name
                                        FROM CollaborationGroup
                                        WHERE Id = :groupId];
        if(grpList != null && grpList.size() > 0){
            CollaborationGroup grp = grpList[0];
            if(groupsAlreadyJoinedIds.contains(grp.Id)){
                following = True;
            }

            Tenable_OnboardingWrapper wrapper = new Tenable_OnboardingWrapper(grp.Name, grp.Id, following);
            if(mapGroupNotificationFrequency.containsKey(grp.Id)){
                wrapper.notificationFrequency = notificationMap.get(mapGroupNotificationFrequency.get(grp.Id));            
            }
            return wrapper;
        }
        return null;
    }

    @auraEnabled
    public static void removeGroupMember(Id groupId){

        list <CollaborationGroupMember> grps = [SELECT CollaborationGroupId
                                        FROM CollaborationGroupMember
                                        WHERE MemberId = :userInfo.getUserId() AND
                                        CollaborationGroupId = :groupId];

        if(grps.size() > 0) {
            CollaborationGroupMember grpMember = grps[0];
            delete grpMember;
        }
    }

    @AuraEnabled
    public static void followTopic(String topicId) {
        Id networkId = Network.getNetworkId();
        list <EntitySubscription> memberIds = [SELECT parentId, subscriberId
                                               FROM EntitySubscription
                                               WHERE subscriberId = :userInfo.getUserId() AND parentId = :topicId AND NetworkId = :networkId
                                               limit 500];

        system.debug('memberIds: ' + memberIds);

        if (memberIds.size() == 0) {
            EntitySubscription subscription = new EntitySubscription();
            subscription.ParentId = topicId;
            subscription.SubscriberId = UserInfo.getUserId();
            subscription.networkId = networkId;
            insert subscription;
            
            System.debug('SetTopicNotificationPreferences:' + topicId);
            setTopicNotificationPreferences(networkId, UserInfo.getUserId(), UserInfo.getSessionId(), topicId);
           
        } else {
            system.debug('already following, no subscription');
        }
    }

    @AuraEnabled
    public static void unfollowTopic(String topicId) {
        Id networkId = Network.getNetworkId();
        list <EntitySubscription> memberIds = [SELECT Id, parentId, subscriberId
                                               FROM EntitySubscription
                                               WHERE subscriberId = :userInfo.getUserId()
                                               AND parentId = :topicId
                                               AND NetworkId = :networkId
                                               LIMIT 500];

        system.debug('memberIds ' + memberIds);
        if (memberIds.size() > 0) {
            EntitySubscription subscriptionToDelete = memberIds[0];
            delete subscriptionToDelete;
        }
    }
    
    @future(callout=true)
    private static void setTopicNotificationPreferences(String networkId, String userId, String sessionId, String topicId){
		HttpRequest req  = new HttpRequest();
		String baseUrl   = System.Url.getSalesforceBaseUrl().toExternalForm().toLowerCase();
		String url       = baseUrl + '/services/data/v40.0/connect/communities/'+networkId+'/chatter/subscriptions/notification/' + topicId+ '/members/' + userId;
		req.setEndpoint(url);
		String userSessionId = sessionId;

		req.setHeader('Authorization', 'OAuth ' + userSessionId);
        req.setHeader('content-type','application/Json');
		req.setMethod('POST');

        String body = '{"notificationFrequency":"EachPost"}';
        req.setBody(body);
        
		Http h = new Http();

        HttpResponse res = h.send(req);
        
    }
}