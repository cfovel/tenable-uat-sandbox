public class TestHelper {
  
  public static String CONTACT_EMAIL = 'tenable.unit.test@tenable.com';
  public static String CONTACT_PHONE = '2015750001';
  
    public enum PortalType {CspLitePortal}
    
    public static User getCommunityUser(Id ContactId, PortalType portalType, Boolean doInsert){
        /* Get any profile for the given type.*/
        Profile p = [SELECT id FROM profile WHERE usertype = :portalType.name() limit 1];
        String testemail = 'tenable.unit.test@tenable.com.usr';
        User pu = new User(profileId = p.id, username = testemail, email = testemail, 
                           emailencodingkey = 'UTF-8', localesidkey = 'en_US', 
                           languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles', 
                           alias='cspu', lastname='lastname', contactId = ContactId);
        if(doInsert) {
            Database.insert(pu);
        }
        return pu;
    }
    
    public static Contact createContact(Id AccountId, Boolean doInsert){
		Contact c = new Contact();
		c.FirstName = 'Tenable';
		c.LastName = 'UnitTest';
		c.LeadSource = 'Advertising';
		c.Phone = CONTACT_PHONE;
		c.MobilePhone = CONTACT_PHONE;
		c.Email = CONTACT_EMAIL;
		c.AccountId = AccountId; 
		c.Customer_Community_User__c = true;
        if(doInsert) {
            Database.insert(c);
        }
        return c;
    }
    
    public static Case createCase(Id AccountId, Id ContactId, Boolean doInsert){
      Case customerCase = new Case();
      customerCase.Subject = 'Tenable Case Subject';
      customerCase.Description = 'Tenable unit test case description';
      customerCase.ContactId = ContactId;
      customerCase.AccountId = AccountId;
      if(doInsert) {
        Database.insert(customerCase);
      }
      return customerCase;
    }
    
}