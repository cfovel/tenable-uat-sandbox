global class BatchTerritoryAssignmentAcctRefreshDebug implements Database.Batchable<sObject> {
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([SELECT Id FROM Account WHERE Name LIKE 'CDF Terr%' ]);
    }
    
    global void execute(Database.BatchableContext bc, List<Account> accounts) {
        TerritoryAssignment.setAccountTerritory(accounts, false);
    }
    
    global void finish(Database.BatchableContext bc) {}
    
}