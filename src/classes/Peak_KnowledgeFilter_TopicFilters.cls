/**
 * Created by 7Summits on 8/1/17.
 */

public with sharing class Peak_KnowledgeFilter_TopicFilters {

    //Pull this out and make static to prevent rerunning through all of our topics to get ids
    private static Set<Id> navigationalTopicSubTopicIds = new Set<Id>();

    public static List<Peak_KnowledgeFilter_TopicWrapper> getFiltersListByIds(List<String> topicIds){
        String network = System.Network.getNetworkId();

        if(Test.isRunningTest()){
            List<Network> networks = [SELECT id FROM Network];
            network = networks[0].id;
        }

        // get navigational topics
        List<Peak_KnowledgeFilter_TopicWrapper> topicWrappers = getManagedTopics(network, topicIds);

        System.debug('----------------------> RESULTS' + topicWrappers + topicIds);


        return topicWrappers;
    }
//    public static List<Peak_KnowledgeFilter_TopicWrapper> getFiltersListByIds(List<String> topicIds){
//        String network = System.Network.getNetworkId();
//
//        if(Test.isRunningTest()){
//            List<Network> networks = [SELECT id FROM Network];
//            network = networks[0].id;
//        }
//
//        // get navigational topics
//        ConnectApi.ManagedTopicCollection topics = getManagedTopics(network, topicIds);
//
//        // prepare topic wrappers
//        List<Peak_KnowledgeFilter_TopicWrapper> topicWrappers = prepareTopicWrappers(topics.managedTopics);
//
//        // filter results by topics Ids passed in
//        List<Peak_KnowledgeFilter_TopicWrapper> results = filterTopicWrappersById(topicWrappers, topicIds);
//
//        System.debug('----------------------> RESULTS' + results + topicIds);
//
//
//        return results;
//    }

    private static List<Peak_KnowledgeFilter_TopicWrapper> prepareTopicWrappers(List<ConnectApi.ManagedTopic> managedTopicList){
        List<Peak_KnowledgeFilter_TopicWrapper> topicWrappers = new List<Peak_KnowledgeFilter_TopicWrapper>();

        // iterate all the topics
        for (ConnectApi.ManagedTopic managedTopic : managedTopicList) {
            Peak_KnowledgeFilter_TopicWrapper wrapper = new Peak_KnowledgeFilter_TopicWrapper(managedTopic.topic.name, managedTopic.topic.Id);
            navigationalTopicSubTopicIds.add(wrapper.topicId);

            //recursively jump down children to build parent heirarchy
            if(!managedTopic.children.isEmpty()) {
                List<Peak_KnowledgeFilter_TopicWrapper> childTopicWrappers = prepareTopicWrappers(managedTopic.children);
                wrapper.childTopicList = childTopicWrappers;
            }

            topicWrappers.add(wrapper);
        }

        return topicWrappers;
    }



//    private static Peak_KnowledgeFilter_TopicWrapper findTopicWrapperById(Peak_KnowledgeFilter_TopicWrapper topicWrapper, String topicId){
//
//        if(topicWrapper.topicId == topicId){
//            return topicWrapper;
//        }
//
//        else if(!topicWrapper.childTopicList.isEmpty()){
//            for (Peak_KnowledgeFilter_TopicWrapper childTopicWrapper : topicWrapper.childTopicList) {
//                return findTopicWrapperById(childTopicWrapper, topicId);
//            }
//        }
//
//        return null;
//    }

    private static List<Peak_KnowledgeFilter_TopicWrapper> getManagedTopics(String network, List<String> topicIds){
        ConnectApi.ManagedTopicCollection topics;
        if(network != '') {
            topics = ConnectApi.ManagedTopics.getManagedTopics(network, ConnectApi.ManagedTopicType.Navigational, topicIds, 3);
            return prepareTopicWrappers(topics.managedTopics);
        }

        return null;
    }

}