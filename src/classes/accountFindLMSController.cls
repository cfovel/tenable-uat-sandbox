public with sharing class accountFindLMSController {
/******************************************************************************
*
*   Custom Controller for AccountFindLMS Page
*
********************************************************************************/
    /*****************************************************************************************
    *   Variable Initialization
    ******************************************************************************************/
    public Id pageid = ApexPages.currentPage().getParameters().get('id');       // Store Account ID
    private final Account entry;                                                // Store Account Record
    //private Contact primaryContact { get; set; }
    private final ApexPages.standardController stdController;                   // Set-up Controller Class
    private Id acctId;
    public Account acct { get; set; }
    public PageReference pageRef = ApexPages.currentPage();
    
    public Paginate paginater {get;set;}
    public List<CustomerSearchResult> csrList{get;set;}
    public List<List<CustomerSearchResult>> fullCsrList{get;set;}
    
    //public List<CustomerSearchResult> searchResults = new List<CustomerSearchResult>();
    public Boolean searched { get; set; }
    public Boolean resultsFound { get; set; }
    public Boolean pageError { get; set; }
    //public List<String> emailAddrList { get; set; }
    public String emailDomains { get; set; }
    private Map<String,Id> domainMap = new Map<String,Id>();
    private Set<id> payloadIds = new Set<id>();
    
    //private integer count=1;   //to track the function calling
    //private integer counter=0;   //to keep track of offset
    //private integer list_size=25; //to set the page size to show the rows/records
    //public integer total_size; //used to show user the total size of the list   
    
    /*****************************************************************************************
    *   Custom Controller Initialization
    ******************************************************************************************/
    public accountFindLMSController(ApexPages.StandardController stdController)                // Controller Set-up Method
    //public accountFindLMSController()                // Controller Set-up Method
    {
        acctId = ApexPages.currentPage().getParameters().get('id');
        if (!Test.isRunningTest())
        {
            stdController.addFields(new List<String>{'Id', 'Type', 'Phone', 'Name', 'Subsidiary__c','LMS_Customer_Id__c', 'Customer_Info_Id__c', 'BillingStreet', 'BillingCity', 'BillingState', 'BillingCountry', 'BillingPostalCode', 'DandBCompanyId', 'Territory__c'});
        }
        this.stdController = stdController;    
        entry = (Account)stdController.getRecord();                     // First get record from Page Call
        //acct = [SELECT Id, Name FROM Account WHERE Id = :acctId LIMIT 1];
        system.debug('@@@ constructor - entry: ' + entry);
        searched = false;
        resultsFound = false;
        pageError = false;
        //searchResults.clear();
        
        // get unique email domains
        emailDomains = '';
        
        if (entry.LMS_Customer_Id__c != null)
        {
            Utils.addError('This Account has a Customer ID.');
            pageError = true;
        }
        else
        {
            for (Contact con : [SELECT Id, Email FROM Contact WHERE AccountId = :acctId])
            {
                if (con.Email != null && con.email.substringAfter('@') != null)
                {
                    String domain = con.email.substringAfter('@');
                    if (!domainMap.containsKey(domain))
                    {
                        domainMap.put(domain,con.Id);
                        emailDomains = emailDomains + domain + ', ';
                    }
                }
                //payloadids.add(con.Id);
                //emailAddrList.add(con.email);
                
            }
            
            if (emailDomains.length() > 0)
            {
                emailDomains = emailDomains.trim().removeEnd(',');
            }
        }
        searchSFDC();
        
    }   
    
    
    public PageReference previousPage(){
        this.paginater.decrement();
        return changeData();
    }
    
    public PageReference nextPage(){
        this.paginater.increment();
        return changeData();
    }

    public PageReference updatePage(){
        this.paginater.updateNumbers();
        return changeData();
    }
    
    public PageReference changeData(){
        this.csrList = this.fullCsrList.get(this.paginater.index);
        return null;
    }
    
    /*
    public List<CustomerSearchResult> populateData(){
        List<CustomerSearchResult> customCsrList = new List<CustomerSearchResult>();
        for(Integer i = 1; i < 50; i++){
            customCsrList.add(new CustomerSearchResult(i, 'Name:  ' + String.valueOf(i)));
        }
        return customCsrList;
    }
    */
    
/*
    public PageReference searchLMS() 
    {
        
        String AccountId = ApexPages.currentPage().getParameters().get('id');
        payloadIds.add(AccountId);
        
        
        //searchResults.clear();
        
        // Add contacts with unique email domains
        
        payloadIds.addAll(domainMap.values());
        
        system.debug('@@@ AccountId - ' + AccountId);
        system.debug('@@@ payloadIds - ' + payloadIds);
        
        //Get the data we need to paginate
        List<CustomerSearchResult> resultsList = new List<CustomerSearchResult>();
        
        
        // Call Mulesoft to get matching accounts in LMS
        
        HttpResponse response = mulesoftIntegrationController.mulesoftEventHandler(payloadIds, AccountId, 'accountLMSSearch');
        
        if (response.getStatusCode() != 200) 
        {
            Utils.addError('There was an unexpected response from this request: ' + response.getStatusCode() + ' - ' + response.getStatus());
            return null;
        } 
        else 
        {
            System.debug('Successful response: ' + response.getBody());
            
            //searchResults = (List<CustomerSearchResult>)JSON.deserialize(response.getBody(),List<CustomerSearchResult>.class);
            resultsList = (List<CustomerSearchResult>)JSON.deserialize(response.getBody(),List<CustomerSearchResult>.class);
        
            searched = true;
            
            
            if (!resultsList.isEmpty()) {
                resultsFound = true;
            }
            //total_size = searchResults.size();
        }
        
        
        
        
        //Set the page size
        Integer pageSize = 25;
        
        //Create a new instance of Paginate passing in the overall size of 
        //the list of data and the page size you want
        this.paginater = new Paginate(resultsList.size(), pageSize);

        //These lists hold the data
        this.fullCsrList = new List<List<CustomerSearchResult>>();
        this.csrList = new List<CustomerSearchResult>();
        
        //Break out the full list into a list of lists
        if(resultsList.size() > 0){
            List<CustomerSearchResult> tempCSR = new List<CustomerSearchResult>();          
            Integer i = 0;
            for(CustomerSearchResult csr : resultsList){
                tempCSR.add(csr);
                i++;
                if(i == pageSize){
                    this.fullCsrList.add(tempCSR);
                    tempCSR = new List<CustomerSearchResult>();
                    i = 0;
                }
            }
            if(!tempCSR.isEmpty()){
                this.fullCsrList.add(tempCSR);
            }
            
            //Gets the correct list of data to show on the page
            this.csrList = this.fullCsrList.get(this.paginater.index);
        }
        
        
        return pageRef;
    }
*/
    
     public PageReference searchSFDC() 
    {
        
        //String AccountId = ApexPages.currentPage().getParameters().get('id');
        //payloadIds.add(AccountId);
        
        
        //searchResults.clear();
        
        // Add contacts with unique email domains
        
        //payloadIds.addAll(domainMap.values());
        
        //system.debug('@@@ AccountId - ' + AccountId);
        //system.debug('@@@ payloadIds - ' + payloadIds);
        
        //Get the data we need to paginate
        List<CustomerSearchResult> resultsList = new List<CustomerSearchResult>();
        
        /*
        // Call Mulesoft to get matching accounts in LMS
        
        HttpResponse response = mulesoftIntegrationController.mulesoftEventHandler(payloadIds, AccountId, 'accountLMSSearch');
        
        if (response.getStatusCode() != 200) 
        {
            Utils.addError('There was an unexpected response from this request: ' + response.getStatusCode() + ' - ' + response.getStatus());
            return null;
        } 
        else 
        {
            System.debug('Successful response: ' + response.getBody());
            
            //searchResults = (List<CustomerSearchResult>)JSON.deserialize(response.getBody(),List<CustomerSearchResult>.class);
            resultsList = (List<CustomerSearchResult>)JSON.deserialize(response.getBody(),List<CustomerSearchResult>.class);
        
            searched = true;
            
            
            if (!resultsList.isEmpty()) {
                resultsFound = true;
            }
            //total_size = searchResults.size();
        }
        */
        // Search for matches on Account Name, D & B Company, and Email Domain (on Contacts) in SFDC
        
        String acctName = entry.Name;
        String queryString;
        acctName = acctName.removeStartIgnoreCase('The ');
        acctName = acctName.removeEnd('.');
        acctName = acctName.removeEndIgnoreCase(' Inc');
        acctName = acctName.removeEndIgnoreCase(' Co');
        acctName = acctName.removeEndIgnoreCase(' Corp');
        acctName = acctName.removeEndIgnoreCase(' Incorporated');
        acctName = acctName.removeEndIgnoreCase(' Company');
        acctName = acctName.removeEndIgnoreCase(' Corporation');
        acctName = acctName.removeEnd(',');
        acctName = String.escapeSingleQuotes(acctName);
        system.debug('@@@ Acct Name: ' + acctName);
        
        
        String dbCoId = entry.DandBCompanyId;
        String accountId = ApexPages.currentPage().getParameters().get('id');
        
        Set<Id> acctIds = new Set<Id>();
        for (Contact con : [SELECT AccountId FROM Contact WHERE AccountId <> null AND Email_Domain__c IN :domainMap.keySet() ])
        {
            acctIds.add(con.AccountId);
        }
        
        queryString = 'SELECT Id, Name, LMS_Customer_Id__c, Customer_Info_Id__c, BillingStreet, BillingCity, BillingState, BillingCountry, BillingPostalCode, DandBCompanyId, DandBCompany.Name, Territory__c ';
        //queryString += 'FROM Account WHERE Id != :accountId AND (Name LIKE \'%' + acctName + '%\'';
        queryString += 'FROM Account WHERE Id != :accountId AND (Name LIKE \'' + acctName + '%\' OR Name LIKE \'% ' + acctName + '%\'';
        if (entry.DandBCompanyId != null)
        {
            queryString += ' OR DandBCompanyId = :dbCoId';
        }
        
        if (!acctIds.isEmpty())
        {
            queryString += ' OR Id IN :acctIds';
        }
        
        queryString += ')';
        
        system.debug('@@@ constructor - entry: ' + entry);
        system.debug('@@@ queryString: ' + queryString); 
        List<Account> acctList = Database.query(queryString);
        
        for (Account acct : acctList)
        {
            resultsList.add(new CustomerSearchResult(acct,acctName,dbCoId));
        }
        
        searched = true;
             
        if (!resultsList.isEmpty()) {
            resultsFound = true;
        }
        
        //Set the page size
        Integer pageSize = 25;
        
        //Create a new instance of Paginate passing in the overall size of 
        //the list of data and the page size you want
        this.paginater = new Paginate(resultsList.size(), pageSize);

        //These lists hold the data
        this.fullCsrList = new List<List<CustomerSearchResult>>();
        this.csrList = new List<CustomerSearchResult>();
        
        //Break out the full list into a list of lists
        if(resultsList.size() > 0){
            List<CustomerSearchResult> tempCSR = new List<CustomerSearchResult>();          
            Integer i = 0;
            for(CustomerSearchResult csr : resultsList){
                tempCSR.add(csr);
                i++;
                if(i == pageSize){
                    this.fullCsrList.add(tempCSR);
                    tempCSR = new List<CustomerSearchResult>();
                    i = 0;
                }
            }
            if(!tempCSR.isEmpty()){
                this.fullCsrList.add(tempCSR);
            }
            
            //Gets the correct list of data to show on the page
            this.csrList = this.fullCsrList.get(this.paginater.index);
        }
        
        
        return pageRef;
    }
    
    /*
    public PageReference selectCustId() 
    {
        // Update account with LMS Id from selected row, then return to account page
        String LMS_Id;
        Decimal Cust_Info_Id;
        Integer count = 0;
        for (CustomerSearchResult csr : csrList)
        {
            if (csr.selected)
            {
                count++;
                LMS_Id = csr.customerID;
                Cust_Info_Id = csr.customerInfoID;
            }
        }
        if (count == 1)
        {
            // Search for existing accounts in Salesforce with the selected Customer Id.  If found, go to Merge screen
            List<Account> accounts = [SELECT Id, Name FROM Account WHERE LMS_Customer_Id__c = :LMS_Id];
            if(accounts != null && accounts.size() > 0)
            {
                pageRef = new PageReference('/merge/accmergewizard.jsp?srch=' + accounts[0].Name);
                pageRef.setRedirect(true);
            }
            else
            {
                Account acct = new Account(Id = acctId, LMS_Customer_Id__c = LMS_Id, Customer_Info_ID__c = Cust_Info_Id);
                update acct;
                pageRef = new ApexPages.StandardController(acct).view();
            }
            //pageRef = Page.accounts;
            //pageRef.getParameters().put('id', acctId);
            
            
        }
        else
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select one and only one matching customer.'));
        }
        
        
        return pageRef;
    }
    */
    public PageReference createCustId() 
    {
        // Check that all required fields on account are populated.  If not, return error
        //if ( String.isBlank(Account.Name) || String.isBlank(Account.LMS_Customer_Id__c) || String.isBlank(Account.BillingStreet) || String.isBlank(Account.BillingCity) ||
        //          String.isBlank(Account.BillingState) || String.isBlank(Account.BillingCountry)|| String.isBlank(Account.BillingPostalCode))
        system.debug('@@@ Account entry: ' + entry);
        if ( entry.Name == null || entry.Type == null || entry.Subsidiary__c == null || entry.Phone == null || entry.BillingStreet == null || entry.BillingCity == null ||
                    entry.BillingState == null || entry.BillingCountry == null || entry.BillingPostalCode == null)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please populate all required fields on Account before creating Account in Netsuite: Name, Type, Subsidiary, Phone, complete Billing Address.'));
            return pageRef;
        }
        
        // send reequest to create new customer in LMS, get LMS Id back, update account in Salesforce, return to account page
        Account acct = new Account(Id = acctId);
        //pageRef = new ApexPages.StandardController(acct).view();
        ApexPages.StandardController sc = new ApexPages.StandardController(acct);
        CreateNetSuiteVendor cnsv = new CreateNetSuiteVendor(sc);
        pageRef = cnsv.accountSyncLMSNetsuite();
        //pageRef = Page.accounts;
        //pageRef.getParameters().put('id', acctId);
        return pageRef;
    }
    
    //public List<CustomerSearchResult> getSearchResults () {
    //    return searchResults;
    //}
    
    
    
    public PageReference Beginning() {  //when the user clicked the beginning button
        //counter=0;
        //cnd=[select First_Name__c, Last_Name__c,City__c from Candidate__c ORDER BY Name limit 10 ];
        this.paginater.pageNum = 1;
        updatePage();
        return null;
    }

    
    public PageReference End() {      //user clicked the End button
        //counter = total_size - math.mod(total_size, list_size);
         //cnd=[select First_Name__c, Last_Name__c,City__c from Candidate__c ORDER BY Name limit 10 offset 20];
        this.paginater.pageNum = this.paginater.totalPage; 
        updatePage(); 
        return null;
    }
    
     public Boolean getDisabledPrevious() {           //this will disable the previous and beginning buttons
        //if(counter>0)
        if (this.paginater.pageNum == 1)
             return true;
         else 
             return false;
        
    }


     public Boolean getDisabledNext() {            //this will disable the next and end buttons
        //if (counter + list_size < total_size) 
        if (this.paginater.pageNum == paginater.totalPage)
            return true; 
        else 
            return false;
     
    }
    /*
     public Integer getTotal_size() {
      return total_size;
   }
    public Integer getPageNumber() {
        return counter/list_size + 1;
    }

    public Integer getTotalPages() {
        if (math.mod(total_size, list_size) > 0) {
         return total_size/list_size + 1;
      } 
      else {
         return (total_size/list_size);
      }
    }
    
    */
    
    /*
    public String getEmailDomains ()
    {
        //emailAddrList = new List<String>();
        emailDomains = '';
        acctId = ApexPages.currentPage().getParameters().get('id');
        
        for (Contact con : [SELECT Id, Email FROM Contact WHERE AccountId = :acctId])
        {
            String domain = con.email.substringAfter('@');
            if (!domainMap.containsKey(domain))
            {
                domainMap.put(domain,con.Id);
                emailDomains = emailDomains + domain + ', ';
            }
            //payloadids.add(con.Id);
            //emailAddrList.add(con.email);
            
        }
        
        if (emailDomains.length() > 0)
        {
            emailDomains = emailDomains.trim().removeEnd(',');
        }
        return emailDomains;
    }
    */
    /*
    public class CustomerSearchResult {
        public Boolean selected { get; set;}
        public String company_name { get; set; }
        public String customerID { get; set; }
        public Decimal customerInfoID { get; set; }
        public String company_mailing_address { get; set; }
        public String company_mailing_city { get; set; }
        public String company_mailing_state { get; set; }
        public String company_mailing_country { get; set; }
        public String company_mailing_zip { get; set; }
        public String name { get; set; }
        public String email_address { get; set; }
        public String searchSource { get; set; }
    }
    */
    public class CustomerSearchResult 
    {
        public Account acct { get; set;}
        public String matchSource { get; set;}
        
        public CustomerSearchResult(Account a, String acctName, Id dbCoId)
        {
            this.acct = a;
            if (a.Name.contains(acctName))
            {
                this.matchSource = 'Account Name';
            }
            else if (a.DandBCompanyId != null && a.DandBCompanyId == dbCoId)
            {
                this.matchSource = 'D & B Company';
            }
            else
            {
                this.matchSource = 'Contact Email Domain';
            }
            
        }
    }
}