/*************************************************************************************
Class: TestCtrlAskAQuestionLightningComponent
Author: Bharathi.M
Details: This test class provides code coverage for the CtrlAskAQuestion controller
History : Created 11/14/2017 (98% code coverage)
***************************************************************************************/
@isTest
public class TestCtrlAskAQuestionLightningComponent {
    
    static Testmethod void createcustomsettings()
    {
        map<string,topic> maptopicinfo = new map<string,topic>();
        list<Topic> lstTopics = new list<Topic>();
        test.startTest();
            lstTopics.add(new Topic(Name='Test1',Description='This is a navigational topic',NetworkId=Network.getNetworkId()));
            lstTopics.add(new Topic(Name='Test2',Description='This is a navigational topic',NetworkId=Network.getNetworkId()));
            lstTopics.add(new Topic(Name='Test3',Description='This is a product topic',NetworkId=Network.getNetworkId()));
            lstTopics.add(new Topic(Name='Test4',Description='This is a product topic',NetworkId=Network.getNetworkId()));
            
            insert lstTopics;
            
            lstTopics = [SELECT Id,Name,NetworkId From Topic];
            if(!lstTopics.isEmpty())
            {
                for(Topic t : lstTopics)
                {
                    maptopicinfo.put(t.Name,t);
                }
            }
            
            list<Customer_Support_Community_Topics__c> lstCustSettings = new list<Customer_Support_Community_Topics__c>();
            Topic t = maptopicinfo.get('Test1');
            lstCustSettings.add(new Customer_Support_Community_Topics__c(Topic_Name__c = 'Installation',Topic_Type__c ='Navigational',Community_Id__c = t.NetworkId,Name=t.Id));
            Topic tp = maptopicinfo.get('Test2');
            lstCustSettings.add(new Customer_Support_Community_Topics__c(Topic_Name__c = 'Tenable.io',Topic_Type__c ='Product',Community_Id__c = t.NetworkId,Name=tp.Id));
            insert lstCustSettings;
        test.stopTest();
    }
    
    static testmethod void InsertFeedItems()
    {
        createcustomsettings();
        CtrlAskAQuestion.GetPostTovalues();
        CtrlAskAQuestion.GetTopicvalues();
        string PostId = [SELECT Id,Name FROM Customer_Support_Community_Topics__c WHERE Topic_Name__c = 'Installation' LIMIT 1].Name;
        string TopicId = [SELECT Id,Name FROM Customer_Support_Community_Topics__c WHERE Topic_Name__c = 'Tenable.io' LIMIT 1].Name;
        string Question = 'This is a test class question';
        string Details = '<b>This is to test rich text field</b>';
        string FileTitle = 'Test.txt';
        string FilePath = 'This is a test';
        string FileData ='';
        CtrlAskAQuestion.InsertQuestionRecords(PostId, Question, Details, TopicId, FileTitle, FilePath, FileData);
    }
}