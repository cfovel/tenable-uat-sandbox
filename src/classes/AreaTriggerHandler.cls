public with sharing class AreaTriggerHandler
{
	//public static User automationUser = [SELECT Id FROM User WHERE ((FirstName = 'Automation' AND LastName = 'User') OR LastName = 'Automation User') AND IsActive = true LIMIT 1];
    //public static Map<Id, Segment__c> segments = new Map<Id, Segment__c>([SELECT Id, Name FROM Segment__c]);
    //public static Map<Id, Theater__c> theaters = new Map<Id, Theater__c>([SELECT Id, Name, TheaterOwner__c FROM Theater__c]);
	
    public class AreaBeforeInsertHandler implements Triggers.Handler
    {
        public void handle() 
        {
            UpdateAreaData(Trigger.new);
        }
    } 
    
    public class AreaBeforeUpdateHandler implements Triggers.Handler
    {
        public void handle() 
        {
            UpdateAreaData(Trigger.new);
        }
    } 
    
    public class AreaAfterInsertHandler implements Triggers.Handler
    {
        public void handle() 
        {
            UpdateTerritoryData(Trigger.new);
        }
    } 
    
    public class AreaAfterUpdateHandler implements Triggers.Handler
    {
        public void handle() 
        {
            UpdateTerritoryData(Trigger.new);
        }
    } 
    
    public static void UpdateAreaData(List<Area__c> newAreas)
    {
        User automationUser = [SELECT Id FROM User WHERE ((FirstName = 'Automation' AND LastName = 'User') OR LastName = 'Automation User') AND IsActive = true LIMIT 1];
        Map<Id, Segment__c> segments = new Map<Id, Segment__c>([SELECT Id, Name FROM Segment__c]);
        Map<Id, Theater__c> theaters = new Map<Id, Theater__c>([SELECT Id, Name, TheaterOwner__c FROM Theater__c]);
        
        if (Trigger.isInsert) 
        {

            for (Area__c area : newAreas) 
            {
                area.OwnerId = automationUser.Id;
                area.TheaterName__c = (area.Theater__c == null) ? null : theaters.get(area.Theater__c).Name;
                area.TheaterOwner__c = (area.Theater__c == null) ? null : theaters.get(area.Theater__c).TheaterOwner__c;
                area.SegmentName__c = (area.Segment__c == null) ? null : segments.get(area.Segment__c).Name;
            }

        } else if (Trigger.isUpdate) 
        {
            
            for (Area__c area : newAreas) 
            {
                Area__c oldArea = (Area__c)Trigger.oldMap.get(area.Id);
                
                if (area.OwnerId != automationUser.Id) 
                {
                    area.OwnerId = automationUser.Id;
                }
                if (area.Theater__c != oldArea.Theater__c || area.TheaterName__c != oldArea.TheaterName__c || area.TheaterOwner__c != oldArea.TheaterOwner__c) {
                    area.TheaterName__c = (area.Theater__c == null) ? null : theaters.get(area.Theater__c).Name;
                    area.TheaterOwner__c = (area.Theater__c == null) ? null : theaters.get(area.Theater__c).TheaterOwner__c;
                }
                
                if (area.Segment__c != oldArea.Segment__c || area.SegmentName__c != oldArea.SegmentName__c) {
                    area.SegmentName__c = (area.Segment__c == null) ? null : segments.get(area.Segment__c).Name;
                }
            }            
        }
    }
    
    public static void UpdateTerritoryData(List<Area__c> newAreas)
    {
        Set<Id> ownerIds = new Set<Id>();
        
        if (Trigger.isInsert) {
            
            for (Area__c area : newAreas) {
                ownerIds.add(area.AreaOwner__c);
            }
            
        } else if (Trigger.isUpdate) {
            
            Map<Id, Area__c> areas = new Map<Id, Area__c>();
            
            List<Region__c> regions = new List<Region__c>();
            
            for (Area__c area : newAreas) {
                Area__c oldArea = (Area__c)Trigger.oldMap.get(area.Id);
                
                if (area.AreaOwner__c != oldArea.AreaOwner__c) {
                    ownerIds.add(area.AreaOwner__c);
                    ownerIds.add(oldArea.AreaOwner__c);
                }
                
                if (area.Name != oldArea.Name || area.AreaOwner__c != oldArea.AreaOwner__c || area.TheaterName__c != oldArea.TheaterName__c || area.TheaterOwner__c != oldArea.TheaterOwner__c) {
                    areas.put(area.Id, null);
                }
                
            }
            
            areas = new Map<Id, Area__c>([SELECT Id, (SELECT Id, JobFunction__c FROM Regions__r) FROM Area__c WHERE Id IN :areas.keySet()]);
            
            for (Area__c area : areas.values()) {
                Area__c newArea = (Area__c)Trigger.newMap.get(area.Id);
                
                for (Region__c region : area.Regions__r) {
                    regions.add(new Region__c(
                        Id = region.Id,
                        AreaName__c = newArea.Name,
                        AreaOwner__c = newArea.AreaOwner__c,
                        TheaterName__c = newArea.TheaterName__c,
                        TheaterOwner__c = newArea.TheaterOwner__c
                    ));
                }
                
            }
            
            if (!regions.isEmpty())
                update regions;
            
        }
        
        if (!ownerIds.isEmpty())
            TerritoryAssignment.setUserTerritoryFuture(ownerIds);
            
    }
}