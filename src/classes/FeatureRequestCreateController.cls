public class FeatureRequestCreateController {
    public Feature_Request__c fr { get; private set; }
    public Account_Feature_Request__c afr { get; private set; }
    private ApexPages.standardController sc;                   // Set-up Controller Class
    private Id frId;
    private Id acctId;
    private Account acct;
    private PageReference pg;
    
    public FeatureRequestCreateController(ApexPages.StandardController sc)
    {
    	this.sc = sc;
        //requestId = ApexPages.currentPage().getParameters().get('id');
        acctId = ApexPages.currentPage().getParameters().get('acctId');
        fr = new Feature_Request__c();
        afr = new Account_Feature_Request__c(Account__c = acctId);
        pg = new PageReference('/' + acctId);
        pg.setRedirect(true);
        //stdController.addFields(new List<String>{'Name'});
        //this.stdController = stdController;    
        //request = (Feature_Request__c)stdController.getRecord();                     // First get record from Page Call
        //if (acctId != null)
        //{
        //    acct = [SELECT Name FROM Account WHERE Id = :acctId limit 1];
        //}
        //System.debug('@@@ requestId: ' + requestId +', acctId: ' + acctId + ', Account: ' + acct);
    }
    
    public PageReference save() {
        try
        {
        	insert fr;
        	
        	if (fr.Id != null)
        	{
        		afr.Feature_Request__c = fr.Id;
        		insert afr;
        	}
        
        }
        catch (Exception e){}
        
        return pg;
        
    }
    public pageReference cancel()
    {
        return pg;
    }    
}