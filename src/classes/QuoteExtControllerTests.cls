@isTest
private class QuoteExtControllerTests {
    
    testMethod static void testSubmit() {
    	Utils.isTest = true;
        accountTriggerHandler.enforceCountry = false;
            
    	Account a = new Account(Name='Test1 Inc.', BillingState='CA', BillingCountry='US',  IsExcludedFromRealign = true);         // Create a test account
        insert a;

        Contact c =                                                                                 // Create a test contact
            new Contact(lastname='test', accountid=a.id, email='test@test.com', MailingCountry='US', MailingState='CA');
        insert c;

        Opportunity o =                                                                             // Create a test opportunity
            new Opportunity(name='test', accountid=a.id, StageName='Discovery',  CloseDate=Date.newInstance(System.now().year(),System.now().month(),System.now().day()));
        insert o;
                           
        SBQQ__Quote__c quote = new SBQQ__Quote__c(SBQQ__Account__c = a.Id, SBQQ__Opportunity2__c = o.Id, Discount_Justification_Explanation__c = 'test justification', Justification_checkbox_1__c = true, Channel_Direct__c = 'Direct');
        insert quote;
        
        Test.startTest();
        QuoteExtController con = new QuoteExtController(new ApexPages.StandardController(quote));
        con.onSubmit();
        quote = [SELECT ApprovalStatus__c FROM SBQQ__Quote__c WHERE Id = :quote.Id LIMIT 1];
        con.QuoteInformation();
        Test.stopTest();
        
        //System.assertEquals('Approved', quote.ApprovalStatus__c);
    }
    
    testMethod static void testRecall() {
        SBQQ__Quote__c quote = new SBQQ__Quote__c(Discount_Justification_Explanation__c = 'test justification', Justification_checkbox_1__c = true, Channel_Direct__c = 'Direct');
        insert quote;
        
        Test.startTest();
        QuoteExtController con = new QuoteExtController(new ApexPages.StandardController(quote));
        con.onRecall();
        quote = [SELECT ApprovalStatus__c FROM SBQQ__Quote__c WHERE Id = :quote.Id LIMIT 1];
        Test.stopTest();
        
        System.assertEquals('Recalled', quote.ApprovalStatus__c);
    }
}