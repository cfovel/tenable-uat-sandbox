public class EvalExtensionButtonController {
    public Evaluation__c eval;
    public String EvalId;
    public Boolean hasCustomPermission {get; set;}
    
    public EvalExtensionButtonController(ApexPages.StandardController controller){
        List<String> fields = new List<String>(); 
        fields.add('Container_uuid__c');
        fields.add('Enterprise_Pause__c');
        
        if (!Test.isRunningTest()) 
        {
            controller.addFields(fields);
        }   
        
        this.eval = (Evaluation__c)controller.getRecord();        
    }  
    
    public PageReference evalExtension()
    {
        EvalId = ApexPages.currentPage().getParameters().get('id');
        PageReference page;
        hasCustomPermission = FeatureManagement.checkPermission('Eval_Extension');
        system.debug('@@@ hasCustomPermission: ' + hasCustomPermission);
        if (!hasCustomPermission)
        {
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'You do not have permission to perform this function'));
          return null;
        }
        
        String newURL = '/apex/EvalExtensionPage?id=' + evalId + '&uuid=' + eval.Container_uuid__c + '&enterprisePause=' + eval.Enterprise_Pause__c + '&userId=' + UserInfo.getUserId();
        page = new PageReference(newURL);
        page.setRedirect(true);
        system.debug('@@@ page: ' + page);
        return page;
    }   
    
    public PageReference evalUnpause()
    {
        EvalId = ApexPages.currentPage().getParameters().get('id');
        PageReference page;
        hasCustomPermission = FeatureManagement.checkPermission('Eval_Unpause');
        system.debug('@@@ hasCustomPermission: ' + hasCustomPermission);
        if (!hasCustomPermission)
        {
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'You do not have permission to perform this function'));
          return null;
        }else if(!eval.Enterprise_Pause__c){
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'This eval has already been re-activated. Please contact your SE manager for any further extensions.'));
          return null;
        }
        
        String newURL = '/apex/EvalExtensionPage?id=' + evalId + '&uuid=' + eval.Container_uuid__c + '&enterprisePause=' + eval.Enterprise_Pause__c + '&userId=' + UserInfo.getUserId();
        page = new PageReference(newURL);
        page.setRedirect(true);
        system.debug('@@@ page: ' + page);
        return page;
    }   
}