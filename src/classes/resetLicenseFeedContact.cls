public class resetLicenseFeedContact{
    public contact2installbases__x a;
    public String licenseId;
    public Boolean hasCustomPermission {get; set;}
    
    @TestVisible private static List<contact2installbases__x> mockedProds = new List<contact2installbases__x>();
    
    public resetLicenseFeedContact(ApexPages.StandardController controller){
        if(Test.isRunningTest()) 
    	{
            if (mockedProds.size() > 0)
            {
            	 this.a = mockedProds[0];
            	 licenseId = a.Id;
            }	 
        }
        else
        {
	        this.a = (contact2installbases__x)controller.getRecord();
	        licenseId = ApexPages.currentPage().getParameters().get('id');
        }
        
        if(ApexPages.currentPage().getURL().contains('Hostname'))
        {
        	hasCustomPermission = FeatureManagement.checkPermission('Reset_Hostname_Timer');
        }
        else
        {
        	hasCustomPermission = FeatureManagement.checkPermission('Reset_Feed');
        }
        system.debug('@@@ hasCustomPermission: ' + hasCustomPermission);
        if (!hasCustomPermission)
        {
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'You do not have permission to perform this function'));
        }
    }
    
    public PageReference resetFeedContact(){
        system.debug('Does this work: ' );
        
        PageReference page;
        
        contact2installbases__x ib;
        if(Test.isRunningTest()) 
    	{
            if (mockedProds.size() > 0)
            {
            	 ib = mockedProds[0];
            }
        }
        else
        {
			ib = [select id, Id__c, Id__r.accountId, productID__c, partNumber__c, ExternalId, maintenanceCode__c from contact2installbases__x where id =: licenseId];
        }
        	
        string licenseCode = ib.maintenanceCode__c;
        string xId = ib.ExternalId;
        string accountId;
        if(Test.isRunningTest()) 
    	{
    		accountId = ib.id__c;
    	}
    	else
    	{
        	accountId = ib.id__r.AccountId;
    	}
        User currentUser = [SELECT Id, UserType, Name FROM User WHERE Id = :UserInfo.getUserId() limit 1]; 
        string feedBody = 'Reset Feed was sent by @' + currentUser.Name + ' for the following product: ';
        string productURL = ib.partNumber__c + ' ExternalId: ' + ib.ExternalId;
        system.debug('Does this work: ' + licenseCode);
        
        FeedItem feedItem = new FeedItem(parentId = accountId, 
                            body = feedBody + productURL, 
                            isRichText = false, 
                            Visibility = 'InternalUsers');
        system.debug('Does this work: ' + feedItem);
        insert feedItem;                    

        resetFeed(licenseCode, null);
        
        //page = new PageReference('/x/contact2installbases__x/' + xId);
        page = new PageReference('/' + licenseId);
        page.setRedirect(true);
        system.debug('Roy8: ' + page);
        return page;
    }
    
    public PageReference cancel(){
    	return new PageReference('/' + licenseId);
    }
    
    @future(callout=true)
    public static void resetFeed(string licenseCode, string productInstanceId){
    
        boolean isSandbox = mulesoftIntegrationController.runningInASandbox();
        list<Integration_Setting__mdt> authHelperList = new list<Integration_Setting__mdt>();
        map<string, string> headerMap = new map<string, string>();
        
        //getting the integration setting based on the Environment. Sandbox or Prod
        Integration_Setting__mdt envMuleProcess;
        if(isSandbox){
            envMuleProcess = mulesoftIntegrationController.getMuleProcess('Sandbox');
        }else{
            envMuleProcess = mulesoftIntegrationController.getMuleProcess('Production');
        }
        authHelperList.add(envMuleProcess);
        
        //getting the header map ready for the request
        headerMap = mulesoftIntegrationController.SetAuthorizationHeaderValues(authHelperList);
        string epSuffix;
        string theProcess;
        if(productInstanceId == null){
        	epSuffix = 'installbase/' + licenseCode + '/feed/reset';
            theProcess = 'resetLicenseFeed';
        }else{
            epSuffix = 'installbase/' + licenseCode + '/timer/hostname/' + productInstanceId;
            theProcess = 'resetHostnameChangeTimer';
        }
        headerMap.put('endpoint', envMuleProcess.Endpoint_URL__c + epSuffix);
        system.debug('ROY6: header map ' + headerMap);
        
        
        
        HttpResponse theResponse = mulesoftIntegrationController.postToMulesoft('', headerMap, 'resetHostnameChangeTimer', 'POST', 'application/json'); 
        
        system.debug('ROY7: HTTP Response ' + theResponse);
    
    }  
    
    public PageReference resetHostnameChangeTimerContact(){

        PageReference page;
        
        contact2installbases__x ib;
        if(Test.isRunningTest()) 
    	{
            if (mockedProds.size() > 0)
            {
            	 ib = mockedProds[0];
            }
        }
        else
        {
        	ib = [select id, id__c, id__r.accountId, partNumber__c, ExternalId, maintenanceCode__c, productInstanceID__c from contact2installbases__x where id =: licenseId];
        }
        	
        string licenseCode = ib.maintenanceCode__c;
        string xId = ib.ExternalId;
        string productInstanceId = String.valueOf(ib.productInstanceID__c);
        string accountId;
        if(Test.isRunningTest()) 
    	{
    		accountId = ib.id__c;
    	}
    	else
    	{
        	accountId = ib.id__r.AccountId;
    	}
        
        User currentUser = [SELECT Id, UserType, Name FROM User WHERE Id = :UserInfo.getUserId() limit 1]; 
		string feedBody = 'Hostname Timer was reset by ' + currentUser.Name + ' for the following product: ' + ib.partNumber__c + ' ExternalId: ' + ib.ExternalId;
        
        system.debug('License Code: ' + licenseCode);
        system.debug('Product Instance ID: ' + productInstanceId);
        
        FeedItem feedItem = new FeedItem(parentId = accountId, 
        					body = feedBody, 
        					isRichText = false, 
        					Visibility = 'InternalUsers');
        insert feedItem;					

        resetFeed(licenseCode, productInstanceId);
        
        //page = new PageReference('/x/contact2installbases__x/' + xId);
        page = new PageReference('/' + licenseId);
        page.setRedirect(true);
        return page;
    }
}