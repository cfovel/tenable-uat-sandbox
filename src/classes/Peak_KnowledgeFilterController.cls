/**
 * Created by 7Summits on 7/19/17.
 */

public with sharing class Peak_KnowledgeFilterController {
    @TestVisible
    private static final String CUSTOM_SETTING = 'Customer_Support_Community_Topics__c';
    @TestVisible
    private static final String SETTING_ID_FIELD = 'Name';
    @TestVisible
    private static final String SETTING_NAME_FIELD = 'Topic_Name__c';
    @TestVisible
    private static final String SETTING_TYPE_FIELD = 'Topic_Type__c';
    @TestVisible
    private static final String SETTING_TOPIC_TYPE_VALUE = 'Product';

    @AuraEnabled
    public static Peak_Response getArticleTypes(){
        Map<String, Schema.SObjectType> describedItems = Schema.getGlobalDescribe();
        Peak_Response peakResponse = new Peak_Response();
        try {
            for (String key : describedItems.keySet()) {
                if (key.endsWith('kav')) {
                    Peak_ContentObject articleType = new Peak_ContentObject();
                    articleType.apiName = String.valueOf(describedItems.get(key));
                    articleType.title = Schema.getGlobalDescribe().get(key).getDescribe().getLabel();
                    peakResponse.peakResults.add(articleType);
                }
            }
            System.debug(peakResponse.peakResults);
        } catch (Exception e) {
            peakResponse.success = false;
            peakResponse.messages.add(e.getMessage());
        }
        return peakResponse;
    }
    @AuraEnabled
    public static Peak_Response getFilteredTopics(){
        String filterQuery = 'SELECT Id, ' + SETTING_ID_FIELD + ', ' + SETTING_NAME_FIELD + ' FROM ' + CUSTOM_SETTING + ' WHERE ' + SETTING_TYPE_FIELD + ' = :SETTING_TOPIC_TYPE_VALUE' + ' ORDER BY Topic_Name__c asc';
        Peak_Response peakResponse = new Peak_Response();
        try {
            for (SObject filteredTopic : Database.query(filterQuery)){
                Peak_ContentObject filteredTopics = new Peak_ContentObject();
                filteredTopics.contentID = (Id)filteredTopic.get(SETTING_ID_FIELD);
                filteredTopics.title = (String)filteredTopic.get(SETTING_NAME_FIELD);
                peakResponse.peakResults.add(filteredTopics);
            }

            System.debug(peakResponse.peakResults);
        } catch (Exception e) {
            peakResponse.success = false;
            peakResponse.messages.add(e.getMessage());
        }
        return peakResponse;
    }

    @AuraEnabled
    public static List<Peak_KnowledgeFilter_TopicWrapper> getNavigationalTopics(List<String> topicIds) {
        List<Peak_KnowledgeFilter_TopicWrapper> filters = Peak_KnowledgeFilter_TopicFilters.getFiltersListByIds(topicIds);
        return filters;
    }

    /**
        Since you can't pass complex objects into APEX from a
        controller so couldn't pass a List<List<String>> so doing
        it this way with topicFilterOne, topicFilterTwo, topicFilterThree, topicFilterFour
     */
    @AuraEnabled
    public static Peak_KnowledgeFilter_SearchResults doSearch(String searchTerm, List<String> topicFilterOne, List<String> topicFilterTwo, List<String> topicFilterThree, List<String> topicFilterFour, List<String> topicFilterFive, String selectedType, String orderByField, String orderByDirection, List<String> excludedTopics, String topicDetailId) {
        Set<String> filterOne = new Set<String>(topicFilterOne);
        Set<String> filterTwo = new Set<String>(topicFilterTwo);
        Set<String> filterThree = new Set<String>(topicFilterThree);
        Set<String> filterFour = new Set<String>(topicFilterFour);
        Set<String> filterFive = new Set<String>(topicFilterFive);
        Set<String> excluded = new Set<String>(excludedTopics);
        filterOne.removeAll(excluded);
        filterTwo.removeAll(excluded);
        filterThree.removeAll(excluded);
        filterFour.removeAll(excluded);
        System.debug('excluded ===== ' + excluded);
        Peak_KnowledgeFilter_SearchResults searchResult = Peak_KnowledgeFilter_Search.doSearch(searchTerm, filterOne, filterTwo, filterThree, filterFour, filterFive, selectedType, orderByField, orderByDirection, excluded, topicDetailId);

        return searchResult;
    }
//    @AuraEnabled
//    public static Peak_KnowledgeFilter_SearchResults doSearch(String searchTerm, List<String> topicFilterOne, List<String> topicFilterTwo, List<String> topicFilterThree, List<String> topicFilterFour, List<String> topicFilterFive, String selectedType, String orderByField, String orderByDirection) {
//        Peak_KnowledgeFilter_SearchResults searchResult = Peak_KnowledgeFilter_Search.doSearch(searchTerm, topicFilterOne, topicFilterTwo, topicFilterThree, topicFilterFour, topicFilterFive, selectedType, orderByField, orderByDirection);
//
//        return searchResult;
//    }


}