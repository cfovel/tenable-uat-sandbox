public class resetLicenseFeed{
    public account2installbases__x a;
    public String confirmationWarning {get; set;}
    public String licenseId;
    public Boolean hasCustomPermission {get; set;}
    
    @TestVisible private static List<account2installbases__x> mockedProds = new List<account2installbases__x>();
    
    
    public resetLicenseFeed(ApexPages.StandardController controller){
    	system.debug('@@@ resetLicenseFeed constructor');
    	if(Test.isRunningTest()) 
    	{
            if (mockedProds.size() > 0)
            {
            	 this.a = mockedProds[0];
            	 licenseId = a.Id;
            }	 
        }
        else
        {
	        this.a = (account2installbases__x)controller.getRecord();
	        licenseId = ApexPages.currentPage().getParameters().get('id');
        }
        hasCustomPermission = FeatureManagement.checkPermission('Reset_Feed');
        system.debug('@@@ hasCustomPermission: ' + hasCustomPermission);
        if (!hasCustomPermission)
        {
        	ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'You do not have permission to perform this function'));
        }
        
    }
    /*
    public void clearConfirmationWarning(){
	   confirmationWarning = '';
	   system.debug('@@@ clearConfirmationWarning' );
	}
	*/
    public PageReference resetFeed(){
        
        system.debug('@@@ Does this work: licenseId= ' + licenseId );
        confirmationWarning = '';
        PageReference page;
        
        
        /*if (!hasCustomPermission)
        {
        	page = new PageReference('/apex/PermissionErrorMessage');
        	page.setRedirect(true);
        	return page;
        }*/
        
        account2installbases__x ib;
        if(Test.isRunningTest()) 
    	{
            if (mockedProds.size() > 0)
            {
            	 ib = mockedProds[0];
            }
        }
        else
        {
        	ib = [select id, id__c, partNumber__c, ExternalId, maintenanceCode__c from account2installbases__x where id =: licenseId];
        }	 
        string licenseCode = ib.maintenanceCode__c;
        string xId = ib.ExternalId;
        string accountId = ib.id__c;
        User currentUser = [SELECT Id, UserType, Name FROM User WHERE Id = :UserInfo.getUserId() limit 1]; 
		string feedBody = 'Reset Feed was sent by ' + currentUser.Name + ' for the following product: ' + ib.partNumber__c + ' ExternalId: ' + ib.ExternalId;
        system.debug('@@@ Does this work: ' + licenseCode);
        
        FeedItem feedItem = new FeedItem(parentId = accountId, 
        					body = feedBody, 
        					isRichText = false, 
        					Visibility = 'InternalUsers');
        insert feedItem;					

        resetFeedCallout(licenseCode);
        
        page = new PageReference('/' + licenseId);
        page.setRedirect(true);
        system.debug('Roy8: ' + page);
        return page;
    }
    
    public PageReference cancel(){
    	return new PageReference('/' + licenseId);
    }
    
    @future(callout=true)
    public static void resetFeedCallout(string licenseCode){
    
        boolean isSandbox = mulesoftIntegrationController.runningInASandbox();
        list<Integration_Setting__mdt> authHelperList = new list<Integration_Setting__mdt>();
        map<string, string> headerMap = new map<string, string>();
        
        //getting the integration setting based on the Environment. Sandbox or Prod
        Integration_Setting__mdt envMuleProcess;
        if(isSandbox){
            envMuleProcess = mulesoftIntegrationController.getMuleProcess('Sandbox');
        }else{
            envMuleProcess = mulesoftIntegrationController.getMuleProcess('Production');
        }
        authHelperList.add(envMuleProcess);
        
        //getting the header map ready for the request
        headerMap = mulesoftIntegrationController.SetAuthorizationHeaderValues(authHelperList);
        string epSuffix = 'installbase/' + licenseCode + '/feed/reset';
        headerMap.put('endpoint', envMuleProcess.Endpoint_URL__c + epSuffix);
        
        system.debug('ROY6: header map ' + headerMap);
        HttpResponse theResponse = mulesoftIntegrationController.postToMulesoft('', headerMap, 'resetLicenseFeed', 'POST', 'application/json');
        
        system.debug('ROY7: HTTP Response ' + theResponse);
    
    }  
}