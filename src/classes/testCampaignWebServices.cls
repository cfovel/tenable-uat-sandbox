/**
 * This class contains unit tests for validating the behavior webservices in Campaigns.
 */
@isTest(seeAllData=true)
private class testCampaignWebServices {

    static testMethod void testCreateCalendarEvent() {
        Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        
        Utils.isTest = true;
        Campaign c1 = new Campaign(name = 'TestCampaign1', event_title__c='TestEvent', isActive = TRUE, type='Event',  event_location__c='Test Location', event_state_province__c='CA', 
                startdate=System.today(), EndDate = Date.today().addDays(7), event_focus_country__c='US', Description='TestDesc', event_type__c='Session', Number_Of_Target_Leads__c=100, Sales_Area__c='AMER',
                Sponsorship_Type__c='Industry', Synopsis__c='Test synopsis', Exclude_from_Metrics__c = true,Campaign_SubType__c = 'other'); 
        insert c1;
        
        Lead l = new Lead(Company='Test Inc.', FirstName='Bob', LastName='Smith', 
                leadsource = 'webinar', lead_source_detail__c = 'test',
                email='test@test1.gov', HasOptedOutOfEmail = true, country='US', state='CA');
        insert l;
        
        CampaignMember cm1 = new CampaignMember(CampaignId=c1.id, leadid=l.id);
            insert cm1;
        
        String calEvent = campaigns.createCalendarEvent(c1.Id);
        System.assert(calEvent =='Successfully created campaign');
        
        String calActivity = campaigns.createCalendarActivity('Test Subject', DateTime.now(), c1.Id);
        System.assert(calActivity == 'Calendar event already exists' );
        
        String cloneMembers = campaigns.cloneWithMembers(c1.Id);
        System.assert(cloneMembers != null);
        
        Campaign c2 = new Campaign(name = 'TestCampaign2', event_title__c='TestEvent', isActive = TRUE, type='Event', event_location__c='Test Location', event_state_province__c='CA', 
                startdate=System.today(), EndDate = Date.today().addDays(7), event_focus_country__c='US', Description='TestDesc', event_type__c='Session', Number_Of_Target_Leads__c=100, Sales_Area__c='AMER',
                Sponsorship_Type__c='Industry', Synopsis__c='Test synopsis', Exclude_from_Metrics__c = true,Campaign_SubType__c = 'other'); 
        insert c2;
        
        calActivity = campaigns.createCalendarActivity('Test Subject', DateTime.now(), c2.Id);
        System.assert(calActivity == 'Successfully created event'  );
        
        calEvent = campaigns.createCalendarEvent(c2.Id);
        System.assert(calEvent == 'Calendar event already exists');
   
    }

    
    private class WebServiceMockImpl implements WebServiceMock {
        public void doInvoke(
            Object stub,
            Object request,
            Map<String, Object> response,
            String endpoint,
            String soapAction,
            String requestName,
            String responseNS,
            String responseName,
            String responseType) 
            {
                throw new TestException(); 
            }   
    }
    
    public class TestException extends Exception{ }
}