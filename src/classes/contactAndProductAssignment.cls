public class contactAndProductAssignment {
    
    
    @RemoteAction
    public static string getProductAssignment(String customerId){
     
        boolean isSandbox = mulesoftIntegrationController.runningInASandbox();
        list<Integration_Setting__mdt> authHelperList = new list<Integration_Setting__mdt>();
        map<string, string> headerMap = new map<string, string>();
        
       	//getting the integration setting based on the Environment. Sandbox or Prod
        Integration_Setting__mdt envMuleProcess;
        if(isSandbox){
            envMuleProcess = mulesoftIntegrationController.getMuleProcess('Sandbox');
        }else{
            envMuleProcess = mulesoftIntegrationController.getMuleProcess('Production');
        }
        authHelperList.add(envMuleProcess);
        
        //getting the header map ready for the request
        //headerMap = mulesoftIntegrationController.SetAuthorizationHeaderValues(authHelperList);
        //temp header
        //headerMap.put('Authorization', 'Basic OGZiYTE5YTliNWRhNGQ4MjhjOTRmZjkzMzRmYjlhMTc6NDNjZjM1MTNkYjVmNGQzODg1QTgzMDM4QThDQjYxNTk=');
        
        system.debug('Roy 1 header map ' + headerMap);
        string epSuffix = 'getproductsforaccount?customer=' + customerId;
        headerMap.put('endpoint', 'https://tns-licensing-sb.cloudhub.io/1.0/' + epSuffix);
        //Call out to get license data
        system.debug('ROY2: header map ' + headerMap);
        HttpResponse theResponse = mulesoftIntegrationController.postToMulesoft(null, headerMap, 'getProductAssignments', 'GET', 'application/json');
	    system.debug('Roy3: reponse body: ' + theResponse.getBody());
       
        string theBody = theResponse.getBody();//EncodingUtil.urlDecode(theResponse.getBody(), 'UTF-8');
       /* Map<String, Object> theBody = (Map<String, Object>) JSON.deserializeUntyped(theResponse.getBody());
        system.debug('Roy4: reponse body: ' + theBody);
        */
       // theBody = '{"test":"value"}';
        //JSONParser parser = JSON.createParser(theBody);

		return theBody; 
    }
    
    @RemoteAction
    public static string getContactAssignment(String customerId){
     
        boolean isSandbox = mulesoftIntegrationController.runningInASandbox();
        list<Integration_Setting__mdt> authHelperList = new list<Integration_Setting__mdt>();
        map<string, string> headerMap = new map<string, string>();
        
       	//getting the integration setting based on the Environment. Sandbox or Prod
        Integration_Setting__mdt envMuleProcess;
        if(isSandbox){
            envMuleProcess = mulesoftIntegrationController.getMuleProcess('Sandbox');
        }else{
            envMuleProcess = mulesoftIntegrationController.getMuleProcess('Production');
        }
        authHelperList.add(envMuleProcess);
        
        //getting the header map ready for the request
        //headerMap = mulesoftIntegrationController.SetAuthorizationHeaderValues(authHelperList);
        //temp header
        headerMap.put('Authorization', 'Basic OGZiYTE5YTliNWRhNGQ4MjhjOTRmZjkzMzRmYjlhMTc6NDNjZjM1MTNkYjVmNGQzODg1QTgzMDM4QThDQjYxNTk=');
        
        system.debug('Roy 1 header map ' + headerMap);
        string epSuffix = 'getcontactsforaccount?customer=' + customerId;
        headerMap.put('endpoint', 'https://tns-licensing-sb.cloudhub.io/1.0/' + epSuffix);
        //Call out to get license data
        system.debug('ROY2: header map ' + headerMap);
        HttpResponse theResponse = mulesoftIntegrationController.postToMulesoft(null, headerMap, 'getContactAssignment', 'GET', 'application/json');
	    system.debug('Roy3: reponse body: ' + theResponse.getBody());
       
        string theBody = theResponse.getBody();//EncodingUtil.urlDecode(theResponse.getBody(), 'UTF-8');
       /* Map<String, Object> theBody = (Map<String, Object>) JSON.deserializeUntyped(theResponse.getBody());
        system.debug('Roy4: reponse body: ' + theBody);
        */
        //JSONParser parser = JSON.createParser(theBody);

		return theBody; 
    }
    
    @RemoteAction
    public static string getContactsAssignedToProductAccount(String customerId, String productId){
     
        boolean isSandbox = mulesoftIntegrationController.runningInASandbox();
        list<Integration_Setting__mdt> authHelperList = new list<Integration_Setting__mdt>();
        map<string, string> headerMap = new map<string, string>();
        
       	//getting the integration setting based on the Environment. Sandbox or Prod
        Integration_Setting__mdt envMuleProcess;
        if(isSandbox){
            envMuleProcess = mulesoftIntegrationController.getMuleProcess('Sandbox');
        }else{
            envMuleProcess = mulesoftIntegrationController.getMuleProcess('Production');
        }
        authHelperList.add(envMuleProcess);
        
        //getting the header map ready for the request
        //headerMap = mulesoftIntegrationController.SetAuthorizationHeaderValues(authHelperList);
        //temp header
        headerMap.put('Authorization', 'Basic OGZiYTE5YTliNWRhNGQ4MjhjOTRmZjkzMzRmYjlhMTc6NDNjZjM1MTNkYjVmNGQzODg1QTgzMDM4QThDQjYxNTk=');
        
        system.debug('getContactsAssignedToProductAccount header map ' + headerMap);
        string epSuffix = 'contactsassignedtoproductaccount?customer=' + customerId + '&product=' + productId;
        headerMap.put('endpoint', 'https://tns-licensing-sb.cloudhub.io/1.0/' + epSuffix);
        //Call out to get license data
        system.debug('ROY2: header map ' + headerMap);
        HttpResponse theResponse = mulesoftIntegrationController.postToMulesoft(null, headerMap, 'getContactsAssignedToProductAccount', 'GET', 'application/json');
	    system.debug('Roy3: reponse body: ' + theResponse.getBody());
       
        string theBody = theResponse.getBody();//EncodingUtil.urlDecode(theResponse.getBody(), 'UTF-8');

		return theBody; 
    }
    
    @RemoteAction
    public static string viewContactAssignment(String customerId, String contactId){
     
        boolean isSandbox = mulesoftIntegrationController.runningInASandbox();
        list<Integration_Setting__mdt> authHelperList = new list<Integration_Setting__mdt>();
        map<string, string> headerMap = new map<string, string>();
        
       	//getting the integration setting based on the Environment. Sandbox or Prod
        Integration_Setting__mdt envMuleProcess;
        if(isSandbox){
            envMuleProcess = mulesoftIntegrationController.getMuleProcess('Sandbox');
        }else{
            envMuleProcess = mulesoftIntegrationController.getMuleProcess('Production');
        }
        authHelperList.add(envMuleProcess);
        
        //getting the header map ready for the request
        //headerMap = mulesoftIntegrationController.SetAuthorizationHeaderValues(authHelperList);
        //temp header
        //headerMap.put('Authorization', 'Basic OGZiYTE5YTliNWRhNGQ4MjhjOTRmZjkzMzRmYjlhMTc6NDNjZjM1MTNkYjVmNGQzODg1QTgzMDM4QThDQjYxNTk=');
        
        system.debug('Roy 1 header map ' + headerMap);
        string epSuffix = '';
        if (contactId != null){
        	epSuffix = 'viewcontacttoproduct?customer=' + customerId+ '&contact=' + contactId;
        }else{
        	epSuffix = 'viewcontacttoproduct?customer=' + customerId + '&onlyproducts=true';
        }
        
        headerMap.put('endpoint', 'https://tns-licensing-sb.cloudhub.io/1.0/' + epSuffix);
        //Call out to get license data
        system.debug('ROY2: header map ' + headerMap);
        HttpResponse theResponse = mulesoftIntegrationController.postToMulesoft(null, headerMap, 'getViewContactAssignments', 'GET', 'application/json');
	    system.debug('Roy3: reponse body: ' + theResponse.getBody());
       
        string theBody = theResponse.getBody();//EncodingUtil.urlDecode(theResponse.getBody(), 'UTF-8');
       /* Map<String, Object> theBody = (Map<String, Object>) JSON.deserializeUntyped(theResponse.getBody());
        system.debug('Roy4: reponse body: ' + theBody);
        */
       // theBody = '{"test":"value"}';
        //JSONParser parser = JSON.createParser(theBody);

		return theBody; 
    }
    
    @RemoteAction
    public static boolean updateContactProductViewState(string stringObject){
     	
        system.debug('updateContactProductViewState Database Sync: ' + stringObject);
        
        boolean isSandbox = mulesoftIntegrationController.runningInASandbox();
        list<Integration_Setting__mdt> authHelperList = new list<Integration_Setting__mdt>();
        map<string, string> headerMap = new map<string, string>();
        
       	//getting the integration setting based on the Environment. Sandbox or Prod
        Integration_Setting__mdt envMuleProcess;
        if(isSandbox){
            envMuleProcess = mulesoftIntegrationController.getMuleProcess('Sandbox');
        }else{
            envMuleProcess = mulesoftIntegrationController.getMuleProcess('Production');
        }
        authHelperList.add(envMuleProcess);
        
        system.debug('Roy 1 header map ' + headerMap);
        string epSuffix = '';
        epSuffix = 'updatecontactproductviewstate';
        
        
        headerMap.put('endpoint', 'https://tns-licensing-sb.cloudhub.io/1.0/' + epSuffix);
        //Call out to get license data
        system.debug('ROY2: header map ' + headerMap);
        //mulesoftIntegrationController.postToMulesoft Parameters are (jsonData, authorization and endpoint header map, processName, RequestType, content Type)
        HttpResponse theResponse = mulesoftIntegrationController.postToMulesoft(stringObject, headerMap, 'SyncToDatabase', 'POST', 'application/json');
	    system.debug('Roy3: reponse body: ' + theResponse.getBody());
        
		return true; 
    }

}