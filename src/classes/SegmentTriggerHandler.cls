public with sharing class SegmentTriggerHandler
{
	//public static User automationUser = [SELECT Id FROM User WHERE ((FirstName = 'Automation' AND LastName = 'User') OR LastName = 'Automation User') AND IsActive = true LIMIT 1];
	
    public class SegmentBeforeInsertHandler implements Triggers.Handler
    {
        public void handle() 
        {
            UpdateSegmentData(Trigger.new);
        }
    } 
    
    public class SegmentBeforeUpdateHandler implements Triggers.Handler
    {
        public void handle() 
        {
            UpdateSegmentData(Trigger.new);
        }
    } 
    
    public class SegmentAfterUpdateHandler implements Triggers.Handler
    {
        public void handle() 
        {
            UpdateTerritoryData(Trigger.new);
        }
    } 
    
    public static void UpdateSegmentData(List<Segment__c> newSegments)
    {
        User automationUser = [SELECT Id FROM User WHERE ((FirstName = 'Automation' AND LastName = 'User') OR LastName = 'Automation User') AND IsActive = true LIMIT 1];
        
        if (Trigger.isInsert) 
        {

            for (Segment__c segment : newSegments) 
            {
                segment.OwnerId = automationUser.Id;
            }

        } else if (Trigger.isUpdate) 
        {
            
            for (Segment__c segment : newSegments) 
            {

                if (segment.OwnerId != automationUser.Id) 
                {
                    segment.OwnerId = automationUser.Id;
                }

            }            
        }
    }
    
    public static void UpdateTerritoryData(List<Segment__c> newSegments)
    {
        Map<Id, Segment__c> segments = new Map<Id, Segment__c>();
            
        List<Theater__c> theaters = new List<Theater__c>();
        List<Area__c> areas = new List<Area__c>();
        List<Region__c> regions = new List<Region__c>();
        List<Territory__c> territories = new List<Territory__c>();
        
        for (Segment__c segment : newSegments) 
        {
            Segment__c oldSegment = (Segment__c)Trigger.oldMap.get(segment.Id);
            if (segment.Name != oldSegment.Name) 
            {
                segments.put(segment.Id, null);
            }
            
        }
        
        segments = new Map<Id, Segment__c>([SELECT Id, (SELECT Id FROM Theaters__r), (SELECT Id FROM Areas__r), (SELECT Id FROM Regions__r), (SELECT Id FROM Territories__r) FROM Segment__c WHERE Id IN :segments.keySet()]);
        
        for (Segment__c segment : segments.values()) 
        {
            Segment__c newSegment;
            for (Theater__c theater : segment.Theaters__r) 
            {
                newSegment = (Segment__c)Trigger.newMap.get(segment.Id);
                theaters.add(new Theater__c(
                    Id = theater.Id,
                    SegmentName__c = newSegment.Name
                ));
            }
        
            for (Area__c area : segment.Areas__r) 
            {
                newSegment = (Segment__c)Trigger.newMap.get(segment.Id);
                areas.add(new Area__c(
                    Id = area.Id,
                    SegmentName__c = newSegment.Name
                ));
            }
        
            for (Region__c region : segment.Regions__r) 
            {
                newSegment = (Segment__c)Trigger.newMap.get(segment.Id);
                regions.add(new Region__c(
                    Id = region.Id,
                    SegmentName__c = newSegment.Name
                ));
            }
        
            for (Territory__c territory : segment.Territories__r) 
            {
                newSegment = (Segment__c)Trigger.newMap.get(segment.Id);
                territories.add(new Territory__c(
                    Id = territory.Id,
                    SegmentName__c = newSegment.Name
                ));
            }
            
        }
        
        if (!theaters.isEmpty())
            update theaters;

        if (!areas.isEmpty())
            update areas;

        if (!regions.isEmpty())
            update regions;

        if (!territories.isEmpty())
            update territories;
            
    }
}