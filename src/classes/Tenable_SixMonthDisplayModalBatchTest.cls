@isTest

public with sharing class Tenable_SixMonthDisplayModalBatchTest {

    @isTest
    public static void testSixMonthDisplayModal(){
        Utils.isTest = true;

        test.startTest();
        Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
        User testUser = Peak_TestUtils.createStandardUser();
        insert testUser;
        test.stopTest();

        Contact testContact = [SELECT Customer_Community_User__c
        FROM Contact
        WHERE Email = 'standarduser@peak.com'];

        testContact.Customer_Community_User__c=true;
        update testContact;

        System.runAs(new User(Id = UserInfo.getUserId())) {
            testUser.First_Login_Date__c = system.today() - 180;
            testUser.Onboarding_Complete__c = true;
            update testUser;
            Database.executeBatch(new Tenable_SixMonthDisplayModalBatch(), 10);
        }

         User testUserAfter = [Select Id, Days_Since_Initial_Login__c, Display_6_Month_Modal__c, Six_Month_Modal_Displayed_Date__c
                                FROM User
                                WHERE Id = :testUser.Id limit 1];
    }
    
    @isTest 
    public static void testSixMonthDisplayModalScheduler() {
        test.starttest();
        String CRON_EXP = '0 0 13 * * ?';  
        String jobId = System.schedule('testSixMonthDisplayModal', CRON_EXP, new Tenable_SixMonthDisplayModalScheduler() );

        test.stopTest(); 
	}

}