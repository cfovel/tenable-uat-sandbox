/*************************************************************************************
Class: TestEvalTriggerHandler
Author: Bharathi.M
Date: 03/21/2017
Details: This test class is for the trigger class EvaluationTriggerHandler
History: Copied Roy's test method from TestIntegration Class
***************************************************************************************/

@isTest
private class TestEvalTriggerHandler 
{

    static testMethod void testEvaluationTrigger()
    {
        Utils.isTest = true;
        
        Account a = new Account(Name='Dell SecureWorks', BillingState='CA', BillingCountry='US', website='http://www.test1.com');
        insert a;

        Contact c = new Contact(lastname='test', accountid=a.id, 
            email='test@test.com', MailingCountry='US', MailingState='CA'); 
        insert c;
        
        Evaluation__c conEval = new Evaluation__c(
                Status__c               = '', 
                Name                    = 'Tenable.io rjones@tenable.com', 
                Marketo_Lead_Id__c      = '923142', 
                Location_Data__c        = '',
                Expiration_Date__c      = date.newInstance(2012,12,22), 
                Eval_Type__c            = 'Tenable.io', 
                Eval_Request_Type__c    = 'New', 
                Eval_Request_Id__c      = '', 
                Eval_Request_Date__c    = datetime.newInstance(2012,09,22,0,0,0), 
                Eval_Product__c         = 'Tenable.io', 
                Email__c                = 'rjones@tenable.com',
                Date_Activation_Sent__c = date.newInstance(2012,12,22), 
                Container_uuid__c       = '23432-5235324-21234-23234', 
                Contact__c              = c.Id, 
                Activation_URI__c       = 'asdfasfasfasdfasdfasdfasdfasdf', 
                Activation_DateTime__c  = datetime.newInstance(2012,09,22,0,0,0)
            );
            
            Test.startTest();
                insert conEval;
            Test.stopTest();
    }
    
    static testmethod void UpdateEvals()
    {
                Utils.isTest = true;
        
        Account a = new Account(Name='Dell SecureWorks', BillingState='CA', BillingCountry='US', website='http://www.test1.com');
        insert a;

        Contact c = new Contact(lastname='test', accountid=a.id, 
            email='test@test.com', MailingCountry='US', MailingState='CA'); 
        insert c;
        
        Evaluation__c conEval = new Evaluation__c(
                Status__c               = 'Activated', 
                Name                    = 'Tenable.io rjones@tenable.com', 
                Marketo_Lead_Id__c      = '923142', 
                Location_Data__c        = '',
                Expiration_Date__c      = date.newInstance(2012,12,22), 
                Eval_Type__c            = 'Tenable.io', 
                Eval_Request_Type__c    = 'New', 
                Eval_Request_Id__c      = '', 
                Eval_Request_Date__c    = datetime.newInstance(2012,09,22,0,0,0), 
                Eval_Product__c         = 'Tenable.io', 
                Email__c                = 'rjones@tenable.com',
                Date_Activation_Sent__c = date.newInstance(2012,12,22), 
                Container_uuid__c       = '23432-5235324-21234-23234', 
                Contact__c              = c.Id, 
                Activation_URI__c       = 'asdfasfasfasdfasdfasdfasdfasdf', 
                Activation_DateTime__c  = datetime.newInstance(2012,09,22,0,0,0)
            );
            
            Test.startTest();
                insert conEval;
            Test.stopTest();
            
            Evaluation__c eval = [SELECT Id, Container_UUID__c FROM Evaluation__c WHERE Id=: conEval.Id];
            
            eval.Container_UUID__c = '23432-5235324-21234-23235';
            
            Update eval;
    }
}