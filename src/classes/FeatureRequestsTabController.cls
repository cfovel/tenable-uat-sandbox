public class FeatureRequestsTabController
{
    //public OverrideAssignment__c overrideAssgn = new List<OverrideAssignment__c>();
    
    public Integer size {get; set;} 
    public Integer noOfRecords {get; set;} 
    public Feature_Request__c entry {get; set;}
    public String searchText {get; set;}
    private String query;
    private String whereClause;
    public Boolean searched { get; set; }
    public Boolean resultsFound { get; set; }
    public Id acctId { get; set; }
    public Account acct { get; set;}
    //private Account acct;
    List<FeatureRequestWrapper> requests {get;set;}
    
     
    public ApexPages.StandardSetController setCon {
    	
        get {
            if(setCon == null) {           
            	resultsFound = false;     
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
                      query + whereClause));
                setCon.setPageSize(size);  
                noOfRecords = setCon.getResultSize();
                system.debug('@@@ noOfRecords: ' + noOfRecords);
                if (noOfRecords > 0)
                {
                	resultsFound = true;
                }
            }            
            return setCon;
        }
        set;
    }
    /*
    public String getAddToAcctURL (Feature_Request__c fr) 
    {
		String URL;
		URL = '/aAH/e?CF00N210000016K7N=' + acct.name + '&CF00N210000016K7N_lkid='+ acct.Id + '&CF00N210000016K7O=' + fr.request.Name + '&CF00N210000016K7O_lkid=' + fr.request.Id + '&saveURL=%2F' + acct.Id + '&retURL=%2F' + acct.Id;
		return URL;
    }
    */
    public FeatureRequestsTabController()
    {
    	searched = false;
    	entry = new Feature_Request__c();
    	query = 'select id,Name,Status__c, Product_Group__c, Category__c, Business_Problem__c from Feature_Request__c';
    	size = 25;
      	acctId = ApexPages.currentPage().getParameters().get('id');
      	if (acctId != null)
        {
        	acct = [SELECT Name FROM Account WHERE Id = :acctId limit 1];
        }
        System.debug('@@@ acctId: ' + acctId + ', Account: ' + acct);
    }
    
    public FeatureRequestsTabController(ApexPages.StandardSetController stdSetController)
    {
    	searched = false;
    	entry = new Feature_Request__c();
    	query = 'select id,Name,Status__c, Product_Group__c, Category__c, Business_Problem__c, Number_of_Accounts__c, CreatedDate from Feature_Request__c';
    	size = 25;
      	acctId = ApexPages.currentPage().getParameters().get('id');
      	if (acctId != null)
        {
        	acct = [SELECT Name FROM Account WHERE Id = :acctId limit 1];
        }
        System.debug('@@@ acctId: ' + acctId + ', Account: ' + acct);
    }
    
    public FeatureRequestsTabController(ApexPages.StandardController stdController)
    {
    	searched = false;
    	entry = new Feature_Request__c();
    	query = 'select id,Name,Status__c, Product_Group__c, Category__c, Business_Problem__c, Number_of_Accounts__c, CreatedDate from Feature_Request__c';
    	size = 25;
      	acctId = ApexPages.currentPage().getParameters().get('id');
    }
    

    public PageReference search() 
    {
        whereClause = ' WHERE Id NOT IN (SELECT Feature_Request__c FROM Account_Feature_Request__c WHERE Account__c = :acctId) AND';
        searched=false;
        List<String> criteria = new List<String>();
        String productGroup;
        String catg;
        String status;
        
        if (entry.Product_Group__c != null)
        {
        	productGroup = entry.Product_Group__c;
        	criteria.add(' Product_Group__c = \'' + productGroup + '\' AND');
        }
        
        if (entry.Category__c != null)
        {
        	catg = entry.Category__c;
        	criteria.add(' Category__c = \'' + catg + '\' AND');
        }
        
        /*
        if (entry.Status__c != null)
        {
        	status = entry.Status__c;
        	criteria.add(' Status__c = \'' + status + '\' AND');
        }
        */
        
        if (searchText != '')
        {
        	//criteria.add(' (Name LIKE \'%' + searchText + '%\'' + ' OR');
        	//criteria.add(' Business_Problem__c LIKE \'%' + searchText + '%\')');
        	criteria.add(' (Name LIKE \'%' + searchText + '%\')');
        }
        
        if (!criteria.isEmpty())
        {
        	//boolean first = true;
        	for (String crit : criteria)
        	{
        		/*
        		if (first)
        		{
        			first = false;
        			whereClause = ' WHERE';
        		}
        		*/
        		whereClause += crit;
        	}
        	
        }
        whereClause = whereClause.removeEnd(' AND');
        
        System.debug('@@@ query: ' + query);
        System.debug('@@@ whereClause: ' + whereClause);
        setCon = null;
        setCon.getRecords();
        searched = true;
        return null;
    }    
    
    // Initialize setCon and return a list of records    
     
    public List<FeatureRequestWrapper> getRequests() 
    {
    	//return (List<Feature_Request__c>) setCon.getRecords();
    	requests = new List<FeatureRequestWrapper>();
        for (Feature_Request__c req : (List<Feature_Request__c>)setCon.getRecords())
            requests.add(new FeatureRequestWrapper(req));

        return requests;
    }
    
    /*
    public PageReference CreateAccountFeatureRequests()
    {
    	Integer count = 0;
    	List<Account_Feature_Request__c> afrList = new List<Account_Feature_Request__c>();
    	PageReference pageref;
    	
        for (FeatureRequestWrapper frw : requests)
        {
            if (frw.selected)
            {
            	count++;
                afrList.add(new Account_Feature_Request__c(Account__c = acctId, Feature_Request__c = frw.request.Id));
            }
        }
        if (count >= 1)
        {
            insert afrList;
            
            //pageRef = Page.accounts;
            //pageRef.getParameters().put('id', acctId);
            
            pageRef = new ApexPages.StandardController(new Account(Id=acctId)).view();
        }
        else
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select at least one feature request to associate with account.'));
            pageref = null;
        }
        
        
        return pageRef;
    	
        Map<string,schema.Sobjecttype> gd = schema.getGlobalDescribe();
        String strKeyPrefix = gd.get('Account_Feature_Request__c').getDescribe().getKeyPrefix();
        String requestName = request.Name;
    
        PageReference newPage = new PageReference('/' + strKeyPrefix + '/e');
        //newPage.getParameters().put('CF00N21000001598c','FR 1');
        //newPage.getParameters().put('CF00N21000001598c_lkid','aAD21000000HUK9');
        //newPage.getParameters().put('CF00N210000015988','N H S Borders');
        //newPage.getParameters().put('CF00N210000015988_lkid','0016000001JK2HS');
        newPage.getParameters().put('CF00N21000001598c',request.Name);
        newPage.getParameters().put('CF00N21000001598c_lkid',(String)requestId);
        newPage.getParameters().put('CF00N210000015988',acct.Name);
        newPage.getParameters().put('CF00N210000015988_lkid',(String)acctId);
        
        newPage.setRedirect(true);
        return newPage;
        
    }
    */
    
    public PageReference CreateNewFeatureRequest()
    {
    	PageReference pageref = new PageReference('/apex/FeatureRequestCreate?acctId=' + acctId) ;
    	pageRef.setRedirect(true);
        
        return pageRef;

    }
    
    public class FeatureRequestWrapper 
    {
        public Boolean selected { get; set;}
        public Feature_Request__c request { get; set;}
        
        public FeatureRequestWrapper()
        {
        	request = new Feature_Request__c();
        	selected = false;
        }
        
        public FeatureRequestWrapper(Feature_Request__c fr)
        {
        	request = fr;
        	selected = false;
        }
    }    

}