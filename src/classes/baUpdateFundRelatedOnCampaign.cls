global class baUpdateFundRelatedOnCampaign implements Database.Batchable<SObject> {
    // To run, login as Automation User, open Execute Anonymous window, and execut:
    //      database.executeBatch(new baUpdateFundRelatedOnCampaign(),200);

    global Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator([select Id from Campaign order by Name]);
    }

    global void execute(Database.BatchableContext context, List<Campaign> scope) {
    /*
        Set<Campaign> campaigns = new Set<Campaign> ();
        
        Boolean related;
        // for (AggregateResult ar : [
        //        select Associated_Campaign__c a, count(Associated_Campaign__c) c
        //        from MDF_Transaction__c
        //        where Associated_Campaign__c in :scope
        //        group by Associated_Campaign__c
        //        ]) { 
        for (MDF_Transaction__c fund : [SELECT Id, Associated_Campaign__C FROM MDF_Transaction__c
                                        WHERE Associated_Campaign__c IN :scope])  {        
    
            campaigns.add(new Campaign(
                    Id = fund.Associated_Campaign__c,
                    Fund_Related__c = true
                    ));  
        }
        List<Campaign> updates = new List<Campaign> (campaigns);
        system.debug('@@@ updates: '  + updates);      
        update updates;
    */    
    }

    global void finish(Database.BatchableContext context) {
    }
}