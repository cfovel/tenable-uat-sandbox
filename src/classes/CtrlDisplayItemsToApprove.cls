/*************************************************************************************
Class: CtrlDisplayItemsToApprove
Author: Bharathi.M
Date: 09/26/2017
Details: This class has all the controller logic for the DisplayItemsToApprove VF component.
Test Coverage: 92% (Code Coverage)
***************************************************************************************/
public class CtrlDisplayItemsToApprove 
{
    public list<ProcessInstanceWorkitem> lstPwItems {get;set;}
    public list<sbaa__Approval__c> lstApprovals {get;set;}
    public list<ApprovalListItemWrapper> lstAppList {get;set;}
    public integer ApprovalListSize {get;set;}

    
    public CtrlDisplayItemsToApprove()
    {
        GetApprovalListItemWrapperItems();
    }
    
    public list<ApprovalListItemWrapper> GetApprovalListItemWrapperItems()
    {
        set<Id> InstanceIds = new set<Id>();
        map<Id,ProcessInstanceStep> mapProcessInstanceStep = new map<Id,ProcessInstanceStep>();
        map<Id,String> mapApprovals = new map<Id,String>();
        String CreatedDateValue;
        string LastActorInfo;
        lstAppList = new list<ApprovalListItemWrapper>();
        ApprovalListSize=0;
        
        
        set<id> actorIds=new set<id>{UserInfo.getUserId()};
        Integer setSize = 0;
        
        while (setSize < actorIds.size())
        {
        	setSize = actorIds.size();
	        for(Group grouprec:[SELECT Id, type, relatedId, (select UserOrGroupId from GroupMembers where UserOrGroupId IN :actorIds ) FROM Group])				// WHERE type = 'Queue' OR type = 'Regular' OR relatedId = :UserInfo.getUserRoleId()])	
	        {
	          //system.debug('@@@@@grouprec'+grouprec.Id + ', relatedId: ' + grouprec.relatedId + ', type: ' + grouprec.type +', groupmembers '+grouprec.groupmembers);
	            if(grouprec.groupmembers!=NULL &&  grouprec.groupmembers.size()>0 )
	            {
	        		
	              actorIds.add(grouprec.Id);
	          	}
	          	else if (grouprec.relatedId  == UserInfo.getUserRoleId())
	          	{
	          		actorIds.add(grouprec.Id);
	          	}
	        }
	        system.debug('@@@@@ actorIds: ' + actorIds);
        }
        
        lstApprovals = new list<sbaa__Approval__c>([SELECT Id,sbaa__Approver__c,Quote__c,Quote__r.Name,
                                                    sbaa__Approver__r.Name,CreatedDate
                                                    FROM sbaa__Approval__c 
                                                    WHERE (sbaa__AssignedTo__c IN :actorIds OR sbaa__AssignedGroupId__c IN :actorIds) 
                                                    AND sbaa__Status__c = 'Requested'
                                                    ]);
                                                    
        list<sbaa__Approval__c> lstApprovedApps = new list<sbaa__Approval__c>([SELECT Id, sbaa__ApprovedBy__r.Name,Quote__c 
                                                                               FROM sbaa__Approval__c 
                                                                               //WHERE (Quote__c != null) AND (Quote__r.SBQQ__Opportunity2__c != null) AND (Quote__r.ApprovalStatus__c != 'Approved') AND (Quote__r.SBQQ__Opportunity2__r.StageName != 'Closed - Won') AND sbaa__Status__c = 'Approved' 
                                                                               WHERE (Quote__r.ApprovalStatus__c != 'Approved') AND (Quote__r.SBQQ__Opportunity2__r.StageName != 'Closed - Won') AND sbaa__Status__c = 'Approved'                                                                                
                                                                               ORDER BY LastModifiedDate ASC]);
        
                                                        
                                                                                               
        lstPwItems = new list<ProcessInstanceWorkitem>([SELECT Id, ActorId, ElapsedTimeInDays, ProcessInstanceId, Actor.Name,
                                                        ProcessInstance.TargetObjectId,
                                                        ProcessInstance.TargetObject.Name,
                                                        ProcessInstance.TargetObject.Type,
                                                        ProcessInstance.LastActorId, OriginalActorId,
                                                        ProcessInstance.LastActor.Name,CreatedDate,
                                                        OriginalActor.Name, ProcessInstance.Status 
                                                        FROM ProcessInstanceWorkitem
                                                        WHERE ActorId IN :actorIds
                                                        AND ProcessInstance.Status = 'Pending'
                                                        ORDER BY CreatedDate DESC]);
        
        for(ProcessInstanceWorkitem pw: lstpwItems)
        {
            InstanceIds.add(pw.ProcessInstance.Id);
        }
        
        
        list<ProcessInstanceStep> lstProcessInstanceStep = new list<ProcessInstanceStep>([SELECT Id,ActorId,Actor.Name,
                                                                ProcessInstance.Id 
                                                                FROM ProcessInstanceStep
                                                                WHERE 
                                                                (StepStatus = 'Approved' OR
                                                                StepStatus = 'Started')
                                                                AND ProcessInstance.Id in : InstanceIds
                                                                ORDER BY CreatedDate DESC]);                                                 

        for(ProcessInstanceStep pi: lstProcessInstanceStep)
        {
            if(!mapProcessInstanceStep.containsKey(pi.ProcessInstance.Id))
            {
                mapProcessInstanceStep.put(pi.ProcessInstance.Id,pi);
            }
        }
        
        for(sbaa__Approval__c app: lstApprovedApps)
        {
            if(!mapApprovals.containsKey(app.Quote__c))
            {
               mapApprovals.put(app.Quote__c,app.sbaa__ApprovedBy__r.Name);
            }
        }
        
        for(ProcessInstanceWorkitem pw: lstpwItems)
        {
            ProcessInstanceStep pi = mapProcessInstanceStep.get(pw.ProcessInstance.Id);
            LastActorInfo = pi != NULL ? pi.Actor.Name : '';
            CreatedDateValue = pw.CreatedDate.format('MM/dd/yyyy hh:mm a');

            lstAppList.add(new ApprovalListItemWrapper(pw.ProcessInstance.TargetObjectId,pw.ProcessInstance.TargetObject.Name,pw.ProcessInstance.TargetObject.Type.substringBefore('_'),LastActorInfo,pw.Id,true,CreatedDateValue));
        }
        
        for(sbaa__Approval__c app: lstApprovals)
        {
            LastActorInfo = mapApprovals.get(app.Quote__c) != NULL ? mapApprovals.get(app.Quote__c) : '';
            CreatedDateValue = app.CreatedDate.format('MM/dd/yyyy hh:mm a');
            lstAppList.add(new ApprovalListItemWrapper(app.Quote__c,app.Quote__r.Name,'Quote',LastActorInfo,app.Id,false,CreatedDateValue));
        }
        
        ApprovalListSize = lstAppList.size();
        return lstAppList;
    }
    
    public class ApprovalListItemWrapper
    {
        public string RelatedTo {get;set;}
        public string Type {get;set;}
        public string MostRecentApprover {get;set;}
        public string TargetObjectId {get;set;}
        public boolean isStandard {get;set;}
        public string ProcessId {get;set;}
        public string DateSubmitted {get;set;}
        
        public ApprovalListItemWrapper(string ObjId, string ObjRelatedTo, string ObjType, string ObjRecentApprover,string ObjpwId,boolean ObjisStandard,string ObjCreatedDate)
        {
            this.IsStandard = ObjisStandard;
            this.TargetObjectId = ObjId;
            this.RelatedTo=ObjRelatedTo;
            this.Type = ObjType;
            this.MostRecentApprover= ObjRecentApprover; 
            this.ProcessId = ObjpwId;
            //system.debug('@@@@ The values are : ' + this.TargetObjectId + '  ' + this.RelatedTo + '  ' + this.Type + '   ' + this.MostRecentApprover);
            this.DateSubmitted = ObjCreatedDate;
            
        }
    }
}