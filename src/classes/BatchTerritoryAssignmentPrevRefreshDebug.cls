global class BatchTerritoryAssignmentPrevRefreshDebug implements Database.Batchable<sObject> {
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([SELECT Id FROM Account WHERE Name LIKE 'CDF Terr%' ORDER BY BillingCountry]);
    }
    
    global void execute(Database.BatchableContext bc, List<Account> accounts) {

        TerritoryAssignment assignment = new TerritoryAssignment();
        
        assignment.start();
        assignment.setTerritoryAssignmentPreview(accounts, true);
        assignment.finish();
         
    }
    
    global void finish(Database.BatchableContext bc) {}
    
}