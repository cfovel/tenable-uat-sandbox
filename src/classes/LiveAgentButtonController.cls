public with sharing class LiveAgentButtonController {
    
    @AuraEnabled
    public static Contact getContact(Id contactId) {
      Contact CustomerContact = null;
      User usr = [Select ContactId from User where Id=: UserInfo.getUserId()];
      if(usr.ContactId !=null){
        CustomerContact = [SELECT Id, Name, Email, Phone, Chat_Support__c, Customer_Community_User__c, AccountId FROM Contact WHERE Id =:usr.ContactId];
      }else{
        CustomerContact = new Contact(FirstName = 'No Contact');
      }
      return CustomerContact;
    }
    
}