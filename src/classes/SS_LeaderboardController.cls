public without sharing class SS_LeaderboardController {

	@AuraEnabled
	public static Boolean isReputationEnabled()
	{
		Boolean enabled = false;

		for (Network nw : [SELECT OptionsReputationEnabled FROM Network])
		{
			enabled = nw.OptionsReputationEnabled;
		}

		System.debug('Reputation: ' + enabled);

		return enabled;
	}

	@AuraEnabled
	public static String getData(Boolean hideInternal, String recordNum){
		String networkId    = Network.getNetworkId();
		List<String> idList = getCandidates(hideInternal, recordNum, networkId);
		String commaSepIds  = String.join(idList, ',');

		return getDataRequest(networkId, idList);
	}

	public static String getDataRequest(String networkId, List<String> idList) {
		List<ConnectApi.UserDetail> userDetailList = new List<ConnectApi.UserDetail>();

		for (String userId : idList){
			ConnectApi.UserDetail userDetail = ConnectApi.ChatterUsers.getUser(networkId, userId); userDetailList.add(userDetail);
        }

		return JSON.serialize(userDetailList);
	}

	public static List<String> getCandidates(Boolean hideInternal, String recordNum, String networkId) {
		List<String> idList = new List<String>();
		Integer recordLimit = Integer.valueOf(recordNum);

		if (hideInternal){
			for(NetworkMember per : [
					SELECT ReputationPoints, MemberId
					FROM NetworkMember n
					WHERE NetworkId = :networkId AND n.Member.IsActive = TRUE AND n.Member.UserType != 'Standard'
					ORDER BY ReputationPoints DESC
					LIMIT :recordLimit]) { idList.add(per.MemberId);}
		} else {
			for (NetworkMember per : [
					SELECT ReputationPoints, MemberId
					FROM NetworkMember n
					WHERE NetworkId = :networkId AND n.Member.IsActive = TRUE
					ORDER BY ReputationPoints DESC
					LIMIT :recordLimit]) { idList.add(per.MemberId);}
		}

		return idList;
	}

	@AuraEnabled
	public static String getSitePrefix(){
		return System.Site.getPathPrefix();
	}
}