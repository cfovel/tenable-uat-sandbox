global class ScheduleUpdateCampaign { 	// implements Schedulable {
	/*************************************************************************************
	 * To run, login as Automation User, open Execute Anonymous window, and execute:     *
	 *	ScheduleUpdateCampaign.scheduleCampaignJob();                                    *
	 *************************************************************************************/
/*
	public static String sched = '0 00 00,12 * * ?';  //Every Day at Noon and Midnight 

    global static String scheduleCampaignJob() {
        ScheduleUpdateCampaign SC = new ScheduleUpdateCampaign(); 
        return System.schedule('Campaign Update Job', sched, SC);
    }
	global void execute(SchedulableContext sc) {
	   	baUpdateCampaignInfluencedOpportunities baCampaign = new baUpdateCampaignInfluencedOpportunities(); 
	   	database.executebatch(baCampaign);
	}
*/	
}