global class ScheduleCalcAccountServiceLevel implements Schedulable {
	/*************************************************************************************
	 * To run, login as Tenable System Admin, open Execute Anonymous window, and execute:     *
	 *	ScheduleCalcAccountServiceLevel.scheduleAccountServiceLevelJob();                                    *
	 *************************************************************************************/

	public static String sched = '0 00 00 * * ?';  //Every Day at Midnight 
    public static String dateCriteria = '(((Start_Date__c = TODAY OR (Start_Date__c < TODAY AND (Opportunity.CloseDate = YESTERDAY OR Opportunity.CloseDate = TODAY))) AND End_Date__c >= TODAY) OR End_Date__c = YESTERDAY)';

    global static String scheduleAccountServiceLevelJob() {
        ScheduleCalcAccountServiceLevel SC = new ScheduleCalcAccountServiceLevel(); 
        return System.schedule('Account Service Level Update Job', sched, SC);
    }
	global void execute(SchedulableContext sc) {
	   	baCalcAccountServiceLevel baAcctSvcLvl = new baCalcAccountServiceLevel(dateCriteria); 
	   	database.executebatch(baAcctSvcLvl);
	}	
}