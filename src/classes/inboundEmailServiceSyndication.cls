global class inboundEmailServiceSyndication implements Messaging.InboundEmailHandler {

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();

		String body = email.plainTextBody;
system.debug('body='+body);
		body = body.replace('<mailto:', '');
		body = body.replace('>','');
system.debug('body='+body);
		String[] fields = body.split('/n');
system.debug('fields='+fields);

for (Integer i=0; i < fields.size(); i++ ) {
	system.debug('field['+string.valueof(i)+']='+fields[i]+', '+fields[i].length());
}
		Lead ld = new Lead();
		ld.FirstName = fields[0];
		ld.LastName = fields[1];
		ld.Company = fields[2];
		ld.Title = fields[3];
		ld.Role__c = fields[4];
		ld.City = fields[5];
		ld.State = fields[6];
		ld.Country = fields[7];
		ld.Phone = fields[8];
		ld.OwnerId = Utils.convertId18to15(String.valueof(fields[9]).trim());
		ld.PID__c = fields[9].trim();
		ld.Email = fields[10];
		ld.Notes__c = fields[11];
		ld.LeadSource = 'Partner';
		ld.Lead_Source_Detail__c = 'Content Syndication';
		upsert ld;
			
        return result;
    }

}