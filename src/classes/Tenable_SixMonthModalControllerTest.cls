@isTest

public with sharing class Tenable_SixMonthModalControllerTest {

    @isTest
    public static void testGetUserRecord(){
        Utils.isTest = true;
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
        User testUser = Peak_TestUtils.createStandardUser();
        insert testUser;
        test.stopTest();

        System.runAs(new User(Id = UserInfo.getUserId())) {
            testUser.Display_6_Month_Modal__c = true;
            update testUser;
        }

        system.runas(testUser){
            User communityUser = Tenable_SixMonthModalController.getUserRecord();
            system.assert(communityUser.Display_6_Month_Modal__c==true);
        }
    }

    @isTest
    public static void testFinishModal(){
        Utils.isTest = true;
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
        User testUser = Peak_TestUtils.createStandardUser();
        insert testUser;
        test.stopTest(); 

        System.runAs(new User(Id = UserInfo.getUserId())) {
            testUser.Display_6_Month_Modal__c = true;
            update testUser;
        }

        system.runas(testUser){
            Tenable_SixMonthModalController.finishModal(testUser);
            list <user> userResult = [SELECT Display_6_Month_Modal__c FROM User WHERE Id = :testUser.Id];
            system.assert(userResult[0].Display_6_Month_Modal__c == false);
        }
    }

    @isTest (seeAllData=true)
    public static void testGetGroupURL(){
        
        test.startTest();
        Tenable_SixMonthModalController.getGroupUrl();
        test.stopTest();
    }
}