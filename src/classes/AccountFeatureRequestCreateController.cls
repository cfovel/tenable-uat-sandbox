public class AccountFeatureRequestCreateController {
    public Account_Feature_Request__c afr { get; private set; }
    private ApexPages.standardController sc;                   // Set-up Controller Class
    private Id frId;
    public Id acctId {get; set;}
    private Account acct;
    
    public AccountFeatureRequestCreateController(ApexPages.StandardController sc)
    {
        this.sc = sc;
        acctId = ApexPages.currentPage().getParameters().get('acctId');
        frId = ApexPages.currentPage().getParameters().get('frId');
        afr = new Account_Feature_Request__c(Account__c = acctId, Feature_Request__c = frId);
        //stdController.addFields(new List<String>{'Name'});
        //this.stdController = stdController;    
        //request = (Feature_Request__c)stdController.getRecord();                     // First get record from Page Call
        //if (acctId != null)
        //{
        //    acct = [SELECT Name FROM Account WHERE Id = :acctId limit 1];
        //}
        //System.debug('@@@ requestId: ' + requestId +', acctId: ' + acctId + ', Account: ' + acct);
    }
    
    public pageReference save()
    {
        if (acctId == null)
        {
        	List<Account_Feature_Request__c> afrList = new List<Account_Feature_Request__c>([SELECT Id FROM Account_Feature_Request__c WHERE Feature_Request__c = :frId AND Account__c = :afr.Account__c]);
        	
        	if (!afrList.isEmpty())
        	{
        		ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'This Feature Request has already been added to this account.'));
        		return null;
        	}
        }
        
        try
        {
            insert afr;
        }
        catch (Exception e) {}
        
        pageReference pg;
    	if (acctId != null)
    	{
    		pg = new PageReference('/' + acctId);
    	}
    	else
    	{
    		pg = new PageReference('/' + frId);
    	}
    	pg.setRedirect(true);
        return pg;
    }        
    
    public pageReference cancel()
    {
    	pageReference pg;
    	if (acctId != null)
    	{
    		pg = new PageReference('/' + acctId);
    	}
    	else
    	{
    		pg = new PageReference('/' + frId);
    	}
    	pg.setRedirect(true);
        return pg;
    }        
}