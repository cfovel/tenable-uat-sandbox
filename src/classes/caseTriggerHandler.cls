public with sharing class caseTriggerHandler {
/*******************************************************************************
*
* Case Trigger Handler Class
* Version 1.0a
* Copyright 2006-2015 (c) by CloudLogistix.com
* All Rights Reserved
* Your are allowed to make modifications only for yourself, not for distribution
* without permission from CloudLogistix.
*
*******************************************************************************/
    public static ManageCaseCount mcc = new ManageCaseCount();
    public static List<Case> caseOpenChangedList = new List<Case>();
    public static final String STANDARD_RECORD_TYPE_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Standard Case').getRecordTypeId();
    public static final String COMMUNITY_RECORD_TYPE_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Community Case').getRecordTypeId();
    public static List<Case_Flag_Setting__mdt> caseFlagSettings = new List<Case_Flag_Setting__mdt>([SELECT DeveloperName, Status__c, Priority__c, CaseTimeOffset1__c, CaseTimeOffset2__c
                FROM Case_Flag_Setting__mdt WHERE Active__c = true]);
    
    public static UpdateContactSurvey ucs = new UpdateContactSurvey();
    
    public class CaseBeforeInsertHandler implements Triggers.Handler {
        public void handle() {
            LoopChecker(Trigger.new);
            PopulateAccountContact(null, Trigger.new);
            SetCaseFlags(null, Trigger.new);
        }
    } 
    
    public class CaseBeforeUpdateHandler implements Triggers.Handler {
        public void handle() {
            PopulateAccountContact(trigger.oldMap, Trigger.new);
            SetCaseFlags(Trigger.oldMap, Trigger.new);
        }
    } 

    public class CaseAfterInsertHandler implements Triggers.Handler {
        public void handle() {
            ProcessCaseTeams(Trigger.new);
            mcc.UpdateOpenCaseCount(null, Trigger.new);
        }
    } 
    
    public class CaseAfterUpdateHandler implements Triggers.Handler {
        public void handle() {
            ProcessCaseTeams(Trigger.new);
            if (!caseOpenChangedList.isEmpty())
            {
                mcc.UpdateOpenCaseCount(trigger.oldMap, caseOpenChangedList);
            }
            ucs.ProcessContactUpdates(trigger.oldMap, trigger.new);
            //ProcessJIRATicketNeeds(Trigger.new, trigger.oldMap);
        }
    } 
    
    public class CaseAfterDeleteHandler implements Triggers.Handler {
        public void handle() {
            mcc.UpdateOpenCaseCount(null, Trigger.old);
        }
    } 
    
    public class CaseAfterUndeleteHandler implements Triggers.Handler {
        public void handle() {
            mcc.UpdateOpenCaseCount(null, Trigger.new);
        }
    } 


    public static void LoopChecker(Case[] cases) {
    
    Set <String> searchstr = new Set <String>();
    Set <String> foundstr = new Set <String>();

        for (Case c : cases) {
            if ((c.Subject != null) &&
                (c.SuppliedEmail != null) &&
                (c.SuppliedEmail.contains('tenable.com') != true)) {
                searchstr.add(c.Subject + ':' + c.SuppliedEmail);
            }
        }


        DateTime LastHour = System.now().addminutes(-5);
        
        Case[] potentialcases = 
            ([SELECT id, searchstr__c
            FROM Case 
            WHERE searchstr__c IN :searchstr
            AND CreatedDate >= :lasthour]);
            
        for (Case c : potentialcases) {
            foundstr.add(c.searchstr__c);
        }
        
        for (Case c : cases) {
            if (foundstr.contains(c.searchstr__c)) {c.subject.addError('Email Loop - Not creating case');}
        }
            
    }
    
    public static void PopulateAccountContact(Map<Id, SObject> oldCasesMap, Case[] cases) 
    {
        Set<Id> contactIds = new Set<Id>();
        
        for (Case c : cases) 
        {
            contactIds.add(c.ContactId);
        }
        
        Map<Id,Contact> contactMap = new Map<id,Contact>([SELECT Id, AccountId FROM Contact WHERE Id IN :contactIds]);
        
        for (Case c : cases)
        {
            c.Contact_Account_Case_Account_Match__c = null;
            if (c.ContactId != null)
            {
                Id contactAcctId = contactMap.get(c.ContactId).AccountId;
                if (contactAcctId == c.AccountId)
                {
                    c.Contact_Account_Case_Account_Match__c = c.ContactId;
                }
            }
            
            if (trigger.isUpdate) 
            {
                Case oldCase = (Case)oldCasesMap.get(c.Id);
                if (c.Status != oldCase.Status)
                {
                    caseOpenChangedList.add(c);
                }
            }
        }
    }
    
    public static void SetCaseFlags(Map<Id, SObject> oldCasesMap, Case[] cases) 
    {
        Map<String,Case_Flag_Setting__mdt> cfsMap = new Map<String,Case_Flag_Setting__mdt>();
        
        for (Case_Flag_Setting__mdt cfs : caseFlagSettings)
        {
            cfsMap.put(cfs.DeveloperName,cfs);
            
        }
        
        
        // replace workflow - Case:Enable Case Flags
        for (Case c : cases)
        {
            Case oldCase;
            if (Trigger.isUpdate)
            {
                 oldCase = (Case)oldCasesMap.get(c.Id);
            }
            if ((c.RecordTypeId == STANDARD_RECORD_TYPE_ID || c.RecordTypeId == COMMUNITY_RECORD_TYPE_ID) && 
                (Trigger.isInsert || (oldCase != null && oldCase.RecordTypeId != STANDARD_RECORD_TYPE_ID && oldCase.RecordTypeId != COMMUNITY_RECORD_TYPE_ID )))
            {
                c.FLAGS__Enable_Case_Flags__c = true;
                //c.RecordTypeId = STANDARD_RECORD_TYPE_ID;     // This line could replace the workflow: Case: Update Record Type of Community Case
                if (c.Status == 'On Hold' || c.Status == 'Escalated' || c.Status == 'Suspended' || c.Status == 'Pending Close' || c.Status == 'Closed' )
                {
                    c.FLAGS__ViewedFlag__c = null;
                }
                else
                {
                    c.FLAGS__ViewedFlag__c = datetime.now();
                }
                
            }
        }
        
        // replace process builder
        for (Case c : cases)
        {
            
            if ((c.RecordTypeId == STANDARD_RECORD_TYPE_ID || c.RecordTypeId == COMMUNITY_RECORD_TYPE_ID) && c.FLAGS__Enable_Case_Flags__c == true)
            {
                String caseFlagKey = c.Status.replace(' ','_') + '_Priority_' + c.Priority.substring(0,2);
                Case_Flag_Setting__mdt cfs = cfsMap.get(caseFlagKey);
                
                if (cfs == null)
                {
                    caseFlagKey = c.Status.replace(' ','_') + '_Priority_' + 'Any';
                    cfs = cfsMap.get(caseFlagKey);
                }
                
                if (cfs != null)
                {
                    c.FLAGS__CaseTimeOffset1__c = cfs.CaseTimeOffset1__c;
                    c.FLAGS__CaseTimeOffset2__c = cfs.CaseTimeOffset2__c;
                }
                
                Case oldCase;
                if (Trigger.isUpdate)
                {
                     oldCase = (Case)oldCasesMap.get(c.Id);
                }
                
                if (oldCase != null && ((c.Status == 'Customer Updated' && oldCase.Status == 'Working') || (oldCase.Status == 'Customer Updated' && c.Status == 'Working')))
                {
                    if (c.FLAGS__ViewedFlag__c == null)
                    {
                        c.FLAGS__ViewedFlag__c = datetime.now();
                    }   
                }
                // This is to handle exception for ootb logic that clears flag when outbound email is sent
                else if (oldCase != null && c.FLAGS__ViewedFlag__c == null && oldCase.FLAGS__ViewedFlag__c != null && (c.Status == 'Pending Customer' || c.Status == 'Pending Engineering'))
                {
                    c.FLAGS__ViewedFlag__c = datetime.now();
                }
                else if (c.Status == 'On Hold' || c.Status == 'Escalated' || c.Status == 'Suspended' || c.Status == 'Pending Close' || c.Status == 'Closed' )
                {
                    c.FLAGS__ViewedFlag__c = null;
                }
                else if (oldCase != null && c.Status != oldCase.Status && c.Status != 'Closed')
                {
                    c.FLAGS__ViewedFlag__c = datetime.now();
                }
                
            
            }
            
        }   
            
            /*
            if (c.RecordTypeId == STANDARD_RECORD_TYPE_ID && c.FLAGS__Enable_Case_Flags__c == true && (c.Status == 'On Hold' || c.Status == 'Escalated' || c.Status == 'Pending Close' ))
            {
                c.FLAGS__CaseTimeOffset1__c = null;
                c.FLAGS__CaseTimeOffset2__c = null;
            }
            else if (c.RecordTypeId == STANDARD_RECORD_TYPE_ID && c.FLAGS__Enable_Case_Flags__c == true && c.Status == 'New')
            {
                if (c.Priority == 'P1 - Critical' )
                {
                    c.FLAGS__CaseTimeOffset1__c = null;
                    c.FLAGS__CaseTimeOffset2__c = 0;
                }
                else if (c.Priority == 'P2 - High' )
                {
                    c.FLAGS__CaseTimeOffset1__c = 2;
                    c.FLAGS__CaseTimeOffset2__c = 4;
                }
                else if (c.Priority == 'P3 - Medium' )
                {
                    c.FLAGS__CaseTimeOffset1__c = 10;
                    c.FLAGS__CaseTimeOffset2__c = 12;
                }
                else if (c.Priority == 'P4 - Informational' )
                {
                    c.FLAGS__CaseTimeOffset1__c = 22;
                    c.FLAGS__CaseTimeOffset2__c = 24;
                }
            }
            else if (c.RecordTypeId == STANDARD_RECORD_TYPE_ID && c.FLAGS__Enable_Case_Flags__c == true && c.Status == 'Working')
            {
                if (c.Priority == 'P1 - Critical' )
                {
                    c.FLAGS__CaseTimeOffset1__c = 0;
                    c.FLAGS__CaseTimeOffset2__c = 2;
                }
                else if (c.Priority == 'P2 - High' )
                {
                    c.FLAGS__CaseTimeOffset1__c = 4;
                    c.FLAGS__CaseTimeOffset2__c = 6;
                }
                else if (c.Priority == 'P3 - Medium' )
                {
                    c.FLAGS__CaseTimeOffset1__c = 20;
                    c.FLAGS__CaseTimeOffset2__c = 24;
                }
                else if (c.Priority == 'P4 - Informational' )
                {
                    c.FLAGS__CaseTimeOffset1__c = 40;
                    c.FLAGS__CaseTimeOffset2__c = 48;
                }
            }
            else if (c.RecordTypeId == STANDARD_RECORD_TYPE_ID && c.FLAGS__Enable_Case_Flags__c == true && c.Status == 'Pending Customer')
            {
                if (c.Priority == 'P1 - Critical' )
                {
                    c.FLAGS__CaseTimeOffset1__c = 20;
                    c.FLAGS__CaseTimeOffset2__c = 24;
                }
                else if (c.Priority == 'P2 - High' )
                {
                    c.FLAGS__CaseTimeOffset1__c = 40;
                    c.FLAGS__CaseTimeOffset2__c = 48;
                }
                else if (c.Priority == 'P3 - Medium' )
                {
                    c.FLAGS__CaseTimeOffset1__c = 64;
                    c.FLAGS__CaseTimeOffset2__c = 72;
                }
                else if (c.Priority == 'P4 - Informational' )
                {
                    c.FLAGS__CaseTimeOffset1__c = 88;
                    c.FLAGS__CaseTimeOffset2__c = 96;
                }
            }
            else if (c.RecordTypeId == STANDARD_RECORD_TYPE_ID && c.FLAGS__Enable_Case_Flags__c == true && c.Status == 'Customer Updated')
            {
                if (c.Priority == 'P1 - Critical' )
                {
                    c.FLAGS__CaseTimeOffset1__c = 0;
                    c.FLAGS__CaseTimeOffset2__c = 2;
                }
                else if (c.Priority == 'P2 - High' )
                {
                    c.FLAGS__CaseTimeOffset1__c = 4;
                    c.FLAGS__CaseTimeOffset2__c = 6;
                }
                else if (c.Priority == 'P3 - Medium' )
                {
                    c.FLAGS__CaseTimeOffset1__c = 20;
                    c.FLAGS__CaseTimeOffset2__c = 24;
                }
                else if (c.Priority == 'P4 - Informational' )
                {
                    c.FLAGS__CaseTimeOffset1__c = 40;
                    c.FLAGS__CaseTimeOffset2__c = 48;
                }
            }
            else if (c.RecordTypeId == STANDARD_RECORD_TYPE_ID && c.FLAGS__Enable_Case_Flags__c == true && c.Status == 'Pending Engineering')
            {
                if (c.Priority == 'P1 - Critical' )
                {
                    c.FLAGS__CaseTimeOffset1__c = 1;
                    c.FLAGS__CaseTimeOffset2__c = 2;
                }
                else if (c.Priority == 'P2 - High' )
                {
                    c.FLAGS__CaseTimeOffset1__c = 12;
                    c.FLAGS__CaseTimeOffset2__c = 14;
                }
                else if (c.Priority == 'P3 - Medium' )
                {
                    c.FLAGS__CaseTimeOffset1__c = 48;
                    c.FLAGS__CaseTimeOffset2__c = 52;
                }
                else if (c.Priority == 'P4 - Informational' )
                {
                    c.FLAGS__CaseTimeOffset1__c = 168;
                    c.FLAGS__CaseTimeOffset2__c = 176;
                }
            }
            
        } */
    }
    

    public static void ProcessJIRATicketNeeds(Case[] newCases, map<Id, SObject> oldCasesMap){
       /* SObject tempCase;
        set<id> idSet = new set<id>();
        set<id> jiraSet = new set<id>();
        map<id, string> upserMap = new map<id, string>();
        map<string, string> fieldMap = new map<string, string>();
        for(SObject c: newCases){
            tempCase = oldCasesMap.get(c.Id);
            string jiraNumber   = string.valueOf(tempCase.get('Jira_Case_Number__c'));
            string thisNumber   = string.valueOf(c.get('Jira_Case_Number__c'));
            string jiraStatus   = string.valueOf(tempCase.get('Status'));
            string jiraPriority = string.valueOf(tempCase.get('Priority')); 
            if(c.get('Jira_Case_Number__c') != null){
                if(thisNumber.startsWithIgnoreCase('cs')){
                    if(jiraNumber != string.valueOf(c.get('Jira_Case_Number__c'))){
                        idSet.add(string.valueOf(c.get('LastModifiedById')));
                        jiraSet.add(string.valueOf(c.get('Id')));
                    }
                    if(jiraStatus != string.valueOf(c.get('Status'))){
                        idSet.add(string.valueOf(c.get('LastModifiedById')));
                        jiraSet.add(string.valueOf(c.get('Id')));
                    }
                    if(jiraPriority != string.valueOf(c.get('Priority'))){
                        idSet.add(string.valueOf(c.get('LastModifiedById')));
                        jiraSet.add(string.valueOf(c.get('Id')));
                    }
                }
            }
        }
        system.debug('Roy 21');
        if(jiraSet.size() > 0){
            SalesforceToJIRAController.UpdateJIRATicket(jiraSet, idSet);
        }   */
    }
    
    public static void ProcessCaseTeams(Case[] cases) {

        CaseTeamTemplate[] ctt = new CaseTeamTemplate[0];
        CaseTeamTemplateRecord[] caseTeamToAdd = new CaseTeamTemplateRecord[0];

        ctt = 
            ([SELECT Id 
            FROM CaseTeamTemplate
            WHERE Name = 'PCI Team' LIMIT 1]);


        for (Case c : cases) {
            if ((c != null) && 
                (c.OwnerId != null) &&
                (ctt.size() > 0) &&
                (ctt[0] != null) &&
                (ctt[0].id != null) &&
                (String.valueof(c.OwnerId).startswith('00G60000002SbwW'))) {
                caseTeamToAdd.add(new CaseTeamTemplateRecord(TeamTemplateId = ctt[0].id, ParentId = c.id));             
            }
        }

        if (caseTeamToAdd.size() > 0) {
            try {
                insert caseTeamToAdd;}
            catch(exception e) {
            }
        }
    }
    
    
    
    public without sharing class ManageCaseCount {
        public void UpdateOpenCaseCount(Map<Id, SObject> oldCasesMap, Case[] cases) {
            List<Account> accounts = new List<Account>();                                                           // hold list of all related accounts for the cases being processed
            List<Id> acctIds = new List<Id>();
            
            for(Case c : cases) {
                acctIds.add(c.AccountId);
            }
            accounts = [SELECT Id, Open_Case_Count__c, (SELECT Id FROM Cases WHERE isClosed <> true)                // select all affected accounts, and their related open cases
                         FROM Account WHERE id IN :acctIds];
            
            if(!accounts.isEmpty()) {
                List<Account> acctsToUpdate = new List<Account>();      
                for(Account a : accounts) {                                                                         // for all affected accounts
                    if(a.Open_Case_Count__c != a.Cases.size()) {                                                    // ... if the number of open cases has changed
                        a.Open_Case_Count__c = a.Cases.size();                                                      // ... updated the open case count field
                        acctsToUpdate.add(a);                                                                       // ... add the account to list to be updated
                    }
                }
                if(!acctsToUpdate.isEmpty()) {
                    Utils.isTest = true;
                    update acctsToUpdate;                                                                           // Update the accounts
                }
            }
        }
    }
    
    public without sharing class UpdateContactSurvey {
        public void ProcessContactUpdates(map<Id, SObject> oldCasesMap, Case[] newCases)
        {
            system.debug('@@@ caseTriggerHander - after update-ProcessContactUpdates');
            Map<Id,Contact> contactsToUpdate = new Map<Id,Contact>();
            
            for (Case c : newCases) 
            {
                Case oldCase = new Case();
                if (oldCasesMap != null) 
                {
                    oldCase = (Case)oldCasesMap.get(c.Id);
                }
                
                if ((c.contactId != null) && (oldCase.GF_cSat_Survey_Sent__c == null) && (c.GF_cSat_Survey_Sent__c != null))
                {
                    contactsToUpdate.put(c.contactId,new Contact(Id= c.ContactId, GetFeedback_cSat_Last_Requested_On__c = datetime.now()));
                }
            }
            
            if (!contactsToUpdate.isEmpty())
            {
                system.debug('@@@ caseTriggerHander - after update-contactsToUpdate:' + contactsToUpdate);
                update contactsToUpdate.values();
            }
        }
    }
}