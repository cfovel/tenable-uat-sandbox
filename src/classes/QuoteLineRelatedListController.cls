public with sharing class QuoteLineRelatedListController {
/******************************************************************************
*
*   Custom Controller for Quote Line Related List
*
********************************************************************************/
    /*****************************************************************************************
    *   Variable Initialization
    ******************************************************************************************/
    public Id pageid = ApexPages.currentPage().getParameters().get('id');       // Store Quote ID
    private final SBQQ__Quote__c	 entry;                                     // Store Quote Record
    private final ApexPages.standardController stdController;                   // Set-up Controller Class
    private List<SBQQ__QuoteLine__c> lineItems;                                	// Products for closed-won Opportunities
    private Id quoteId;
    
    /*****************************************************************************************
    *   Custom Controller Initialization
    ******************************************************************************************/
    public QuoteLineRelatedListController(ApexPages.StandardController stdController) {               	// Controller Set-up Method
        SBQQ__Quote__c	 entry = (SBQQ__Quote__c)stdController.getRecord();                     				// First get record from Page Call
        this.stdController = stdController;                                     						// Set-up Controller
        lineItems = new List<SBQQ__QuoteLine__c>();
        quoteId = ApexPages.currentPage().getParameters().get('id');
    }   

    public List<SBQQ__QuoteLine__c> getLineItems() {

        lineItems = [SELECT Id, Name, SBQQ__Product__c, SBQQ__Product__r.Name, SBQQ__Number__c, Legacy_SKU_PR__c, Units__c, SBQQ__Quantity__c, Total_Distributor_Line_Disc__c, SBQQ__NetTotal__c, 
        					SBQQ__StartDate__c, SBQQ__EndDate__c, Order_Category_2__c
                            FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c = :quoteId];

            
        return lineItems;
    }
}