global class convertLead {
/******************************************************************************
*
*   Custom Controller for Lead Conversions
*   Copyright 2006-2015 (c) by CloudLogistix.com
*   All Rights Reserved, Modifications can only be made for your direct use and 
*   not for distribution without explicit permission from CloudLogistix.
*
********************************************************************************/

    /*****************************************************************************************
    *   Constant Declaration
    ******************************************************************************************/
    String DefaultLeadStatus = 'Qualified';
    String DefaultStageName = 'Suspect';

    /*****************************************************************************************
    *   Variable Initialization
    ******************************************************************************************/
    public Id pageid = ApexPages.currentPage().getParameters().get('id');       // Store Lead ID
    public Lead entry {get; set;}                                               // Store Lead Record
    public Boolean pageerror = false;                                           // Used to control rendering if error
    public Boolean getPageError() {
        return pageerror;
    }                                                                           // Used to control rendering if error
    private final ApexPages.standardController stdController;                   // Set-up Controller Class

    /*****************************************************************************************
    *   Custom Controller Initialization
    ******************************************************************************************/
    public convertLead(ApexPages.StandardController stdController) {            // Controller Set-up Method
        Lead entry = (Lead)stdController.getRecord();                           // First get record from Page Call
        this.stdController = stdController;                                     // Set-up Controller
        this.ldstatus = DefaultLeadStatus;
        doUpdate();
    }

    public void doUpdate() {
        this.pageerror = false;
        this.numAccounts = 0;
        this.numContacts = 0;
        getAccounts();
        getContacts();      
    }

    /********************************************************
    *   Conversion Functions
    ********************************************************/
    /*****************************************************************************************
    *   Dynamic Picklist Construction
    ******************************************************************************************/
    public String leadstatus;
    public String getLeadStatus() { return this.leadstatus; }
    public void setLeadStatus(String s) {
        account = null;
        contact = null;
        oppty = null;
        dummyopp = new Opportunity(closedate = System.today());
        leadstatus = s;
        doUpdate();
    }
    public List<SelectOption> getLeadStatuses() {
        List<SelectOption> optionList = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = 
            Lead.Status.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple) {
            optionList.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return optionList;
    }

    String username;
    public String getUser() { return this.username; }
    public void setUser(String s) { this.username = s; }
    public List <SelectOption> getUsers() {
        List <SelectOption> optionList = new List <SelectOption>();
        User[] usrs = 
            ([SELECT Id, Name
            FROM User 
            WHERE (Profile.Name LIKE '%Sales%' 
            OR Profile.Name LIKE '%ISR%'
            OR Profile.Name LIKE '%Partner Manager%') 
            AND Id != :Userinfo.getUserId() 
            AND IsActive=TRUE 
            ORDER BY Name ASC]);

        optionList.add(new SelectOption(Userinfo.getUserId(), Userinfo.getName()));

        for (User u : usrs) {
            optionList.add(new SelectOption(u.id,u.name));
        }
        return optionList;
    }

    String account;
    public Integer numAccounts = 0;
    public Integer getnumAccounts() {return this.numAccounts;}
    public String getAccount() { return this.account; }
    public void setAccount(String s) { this.account = s; }
    public List <SelectOption> getAccounts() {
        List <SelectOption> optionList = new List <SelectOption>();
        Lead ld = 
            ([SELECT Id, Name, Company 
            FROM Lead 
            WHERE Id = :ApexPages.currentPage().getParameters().get('id')]);
        String searchstr = '%' + ld.company + '%';
        Account[] acct = 
            ([SELECT Id, Name 
            FROM Account 
            WHERE Name LIKE :searchstr
            LIMIT 1000]);
        this.numAccounts = acct.size();
        
        if (this.numAccounts == 1000) {                      //Salesforce’s SelectOption List Limit
            searchstr = ld.company + '%';
            acct = ([SELECT Id, Name 
              FROM Account 
              WHERE Name LIKE :searchstr
              LIMIT 1000]);
            this.numAccounts = acct.size();
        }
        
        if (this.numAccounts == 1000) {                      //Salesforce’s SelectOption List Limit
            searchstr = ld.company + ' %';
            String searchstr2 = ld.company + '-%';
            acct = ([SELECT Id, Name 
              FROM Account 
              WHERE Name LIKE :searchstr OR Name LIKE :searchstr2
              LIMIT 1000]);
            this.numAccounts = acct.size();
        }
        
        if (((this.ldstatus == 'Add to Existing Account') || 
            (this.ldstatus == 'Merge Duplicate')) && 
            (acct.size() == 0)) {
            ApexPages.Message msgErr = new ApexPages.Message(ApexPages.Severity.ERROR, 'No existing account matches for this lead'); 
            ApexPages.addMessage(msgErr);
            this.pageerror=true;
        }
        else {
            if (this.ldstatus == DefaultLeadStatus) {
                optionList.add(new SelectOption('', 'Create New Account - '+ld.company));
                this.numAccounts++;
            }
            for (Account a : acct) {
                if (account == null) {account = a.id;}
                optionList.add(new SelectOption(a.id,'Add to existing account - '+a.name));
            }
        }
        return optionList;
    }

    String contact;
    public Integer numContacts = 0;
    public Integer getnumContacts() {return this.numContacts;}
    public String getContact() { return this.contact; }
    public void setContact(String s) { this.contact = s; }
    public List <SelectOption> getContacts() {
        List <SelectOption> optionList = new List <SelectOption>();
        Lead ld = 
            ([SELECT Id, Name, Company
            FROM Lead
            WHERE Id = :ApexPages.currentPage().getParameters().get('id')]);
        if (this.account != null) {
            String searchstr = '%' + ld.name + '%';
            Contact[] cntct = 
                ([SELECT Id, Name 
                FROM Contact 
                WHERE Name LIKE :searchstr 
                AND AccountId = :this.account]);
            this.numContacts = cntct.size();

            if ((this.ldstatus == 'Merge Duplicate') && 
                (cntct.size() == 0)) {
                ApexPages.Message msgErr = new ApexPages.Message(ApexPages.Severity.ERROR, 'No existing contact matches for this lead'); 
                ApexPages.addMessage(msgErr);
                this.pageerror=true;
            }
            else {
                if ((this.ldstatus == DefaultLeadStatus) || 
                    (this.ldstatus == 'Add to Existing Account')) {
                    optionList.add(new SelectOption('', 'Create New Contact - '+ld.name));
                    this.numContacts++;
                }
                for (Contact c : cntct) {
                    optionList.add(new SelectOption(c.id,'Add to existing contact - '+c.name));
                }
            }
        }
        else {
            if (this.numAccounts > 0) {
                optionList.add(new SelectOption('', 'Create New Contact - '+ld.name));
            }
        }
        return optionList;
    }

    String contactrole;
    public String getContactRole() { return this.contactrole; }
    public void setContactRole(String s) { this.contactrole = s; }
    public List <SelectOption> getContactRoles() {
        List <SelectOption> optionList = new List <SelectOption>();
        Schema.DescribeFieldResult cr = OpportunityContactRole.Role.getDescribe();
        for (Integer i = 0; i < cr.getPickListValues().size(); i++) {
            optionList.add(new SelectOption(cr.getPicklistValues()[i].getValue(),cr.getPicklistValues()[i].getValue()));
        }
        return optionList;
    }

    String oppty;
    public String getOppty() { return this.oppty; }
    public void setOppty(String s) { this.oppty = s; }
    public List <SelectOption> getOpportunities() {
        List <SelectOption> optionList = new List <SelectOption>();
        if (this.account != null) {
            String searchstr = '%' + this.account + '%';
            Opportunity[] opps = 
                ([SELECT Id, Name 
                FROM Opportunity 
                WHERE AccountID = :this.account
                AND IsClosed=FALSE]);
    
            optionList.add(new SelectOption('', 'Create New Opportunity'));
    
            for (Opportunity o : opps) {
                optionList.add(new SelectOption(o.id,'Add to existing opportunity - '+o.name));
            }
        }
        return optionList;
    }

    String ldstatus;
    public String getStatus() { return this.ldstatus; }
    public void setStatus(String s) { this.ldstatus = s; }
    public List <SelectOption> getStatuses() {
        Map <String, String> table = new Map <String, String>();
        String ProfileId = UserInfo.getProfileId();
        table.put(DefaultLeadStatus,'Qualified Lead');
        table.put('Merge Duplicate','Lead is a duplicate of an existing contact');
        table.put('Add to Existing Account', 'Lead is a contact for an existing account');
        
        List <SelectOption> optionList = new List <SelectOption>();

        LeadStatus[] cs = 
            ([SELECT Id, MasterLabel 
            FROM LeadStatus 
            WHERE IsConverted=true]);

        for (LeadStatus ls : cs) {
            if (table.get(ls.masterlabel) != null) {
                optionList.add(new SelectOption(ls.masterlabel, table.get(ls.masterlabel)));
            }
        }
        return optionList;
    }

    public Opportunity dummyopp = new Opportunity(closedate = System.today());
    public Opportunity getdummyopp() {return this.dummyopp;}
    public void setdummyopp(Opportunity o) {this.dummyopp = o;}

    public PageReference convertLead() {
        PageReference newPage = new PageReference('/apex/convertleadwizard?id=' + ApexPages.currentPage().getParameters().get('id'));
        newPage.setRedirect(true);
        return newPage;     
    }

    public PageReference convert() {
        String convstring;
        Id oppid;
        Boolean stageupdate = false;
        Date oppclosedate;
        Lead[] leadstoupdate = new Lead[0];

        Lead ld =                                                                                   // Go get the lead record we're converting
                ([SELECT Id, OwnerId, Country, Company, CreatedDate, Owner.Name,
                Title, State, NumberOfEmployees, Name, Industry, Product_Interest__c,
                Partner_Acceptance_Date__c, Partner_Lead_Status__c,
                Deal_Registration_Id__c, Deal_Reg_Eligibility__c, Exclude_from_Workflow__c
                FROM Lead 
                WHERE Id = :ApexPages.currentPage().getParameters().get('id')]);

        ld.Exclude_from_Workflow__c = TRUE;
        ld.SQL_Date__c = System.today();
        if (ld.Partner_Lead_Status__c == 'Assigned to SDR' || ld.Partner_Lead_Status__c == 'In Nessus Pro Eval Queue' ||
                ld.Partner_Lead_Status__c == 'In EMEA Regional Queue') {
            ld.Partner_Lead_Status__c = 'Upsell';
        }
        
        update ld;

        User[] usr = 
                ([SELECT AccountId, Id FROM User WHERE id = :ld.ownerid AND UserType LIKE '%Partner%']);


        Database.LeadConvert lc = new database.LeadConvert();                                       // Get ready to convert lead
        lc.setLeadId(ApexPages.currentPage().getParameters().get('id'));                            // Point to lead record
        lc.setConvertedStatus(this.ldstatus);                                                       // Set Lead status to our choice from VF Page
        
        if ((account != null) &&                                                                    // Do we have an account we're converting into
            (account != '')) {
            lc.setAccountId(account);                                                               // Point to that account
            if ((contact != null) && 
                (contact != '')) {                                                                  // Do we have a contact we're point to
                lc.setContactId(contact);                                                           // Point to that contact
            }
        }

        lc.setOwnerId(username);                                                                    // Point to the new owner
        lc.setDoNotCreateOpportunity(false);                                                        // Default create an opportunity

        if ((oppty != null) &&                                                                      // Are we pointing to an existing opp
            (oppty != '')) {
            lc.setDoNotCreateOpportunity(true);                                                     // Yes then don't create an opportunity
            stageupdate = true;
            Opportunity newopp = ([SELECT CloseDate FROM Opportunity WHERE id = :oppty]);
            oppclosedate = newopp.closedate;
        }

        Database.LeadConvertResult lcr = Database.convertLead(lc);                                  // Perform the conversion
        oppid = lcr.getOpportunityId();                                                             // ... get the new opp id

        if ((this.oppty != null) &&                                                                 // Did we point to an existing opportunity
            (this.oppty != '')) {
            oppid = this.oppty;                                                                     // If so, get the opp id
        }


        if (contactrole != null) {                                                                  // If we are adding a contactrole
            OpportunityContactRole[] ocr =                                                          // Go see if we alreaddy have that contact on the opp
                ([SELECT Id, Role 
                FROM OpportunityContactRole
                WHERE OpportunityId = :oppid 
                AND ContactId = :lcr.getContactId() 
                AND Role = '']);

            if (ocr.size() > 0) {
                ocr[0].role = contactrole;
                update ocr[0];
            }
            else {
                OpportunityContactRole oc = 
                    new OpportunityContactRole(contactid = lcr.getContactId(), role = contactrole, opportunityid = oppid);
                insert oc;
            }
        }

        Boolean updateopp = false;

        String dealregid = '';
        Boolean dealregeligible = false;
        Date dealregdate;
        String leadinterest = '';

        if (ld.Deal_Registration_Id__c != null) {dealregid = ld.Deal_Registration_Id__c;}
        if (ld.Deal_Reg_Eligibility__c) {dealregeligible = TRUE;}
        if (ld.Product_Interest__c != null) {leadinterest = ld.Product_Interest__c;}
        if (ld.Partner_Acceptance_Date__c != null) {dealregdate = ld.Partner_Acceptance_Date__c;}

        Opportunity existingopp = 
            ([SELECT id, Deal_Registration_Id__c, Registered_Deal__c, Deal_Registration_Date__c, Initial_Lead_Interest__c
            FROM Opportunity WHERE Id = :oppid]);
            
        if (existingopp != null) {
            if ((existingopp.Deal_Registration_Id__c != null) && (dealregid == '')) {dealregid = existingopp.Deal_Registration_Id__c;}
            if ((existingopp.Registered_Deal__c != null) && (!dealregeligible)) {dealregeligible = existingopp.Registered_Deal__c;}
            if ((existingopp.Deal_Registration_Date__c != null) && (dealregdate == null)) {dealregdate = existingopp.Deal_Registration_Date__c;}
            if ((existingopp.Initial_lead_Interest__c != null) && (leadinterest == '')) {leadinterest = existingopp.Initial_lead_Interest__c;}
        }
        
        Opportunity opp = 
            new Opportunity(id=oppid, 
                Deal_Registration_Id__c = dealregid,
                Registered_Deal__c = dealregeligible,
                Deal_Registration_Date__c = dealregdate,
                Initial_lead_Interest__c = leadinterest);

        if ((usr.size() > 0) &&
            (usr[0] != null) &&
            (usr[0].AccountId != null)) {
            OppPartner__c op = new OppPartner__c(opportunity__c = oppid, partner__c = usr[0].AccountId, 
                    primary_partner__c = TRUE, primary_distributor__c = FALSE, partner_role__c = 'VAR/Reseller');   
            if(!Utils.isDuplicatePartner(op)) {                                                 // if this partner does not already exists for this opportunity, add it                             
                insert op;
            }   
            opp.Deal_Registration_Partner__c = usr[0].AccountId;
        }

        if (stageupdate) {
            opp.stagename = DefaultStageName;
            opp.closedate = oppclosedate;
        }

        Id newoppid = oppid;                                                                        // Default point to new oppid
        if ((oppty != null) && 
            (oppty != '')) {
            opp.closedate = this.dummyopp.closedate;
            newoppid = oppty;
        }

        update opp;


        PageReference newPage;                                                                      // Go create a new page to return to
        newPage = new PageReference('/'+newoppid);                                                  // Point to the new opportunity id
        newPage.setRedirect(true);                                                                  // Redirect to the new page

        return newPage;
    }

}