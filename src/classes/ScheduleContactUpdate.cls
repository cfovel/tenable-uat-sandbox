public class ScheduleContactUpdate implements Schedulable {

	public List<SObject> contactsAndLeads;
	
	public ScheduleContactUpdate (List<SObject> contactsAndLeads) {
		this.contactsAndLeads = contactsAndLeads;
	}
	
	public void execute(SchedulableContext context) {
		CronTrigger cron = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                FROM CronTrigger WHERE Id = :context.getTriggerId()];
                
        /*****************************************************************************************
        *   Constant Initialization
        ******************************************************************************************/ 
        Boolean SaveContactActivities = utils.marketingappsettings().get('ContactSaveActivity').Enabled__c;
        Boolean SaveLeadActivities = utils.marketingappsettings().get('LeadSaveActivity').Enabled__c;

        /*****************************************************************************************
        *   Variable Initialization
        ******************************************************************************************/ 
        Contact[] firstcontactstoupd = new Contact[0];
        Contact[] lastcontactstoupd = new Contact[0];
        Lead[] firstleadstoupd = new Lead[0];
        Lead[] lastleadstoupd = new Lead[0];
        Map <Id, Task> firstMatches = new Map <Id, Task>();
        Map <Id, Task> lastMatches = new Map <Id, Task>();
        Map <String, Integer> activityStats = new Map <String, Integer>();
    
        /*****************************************************************************************
        *   Record Pre-Processing
        ******************************************************************************************/ 
        /*Contact[] contacts = ([SELECT Id, AccountId, MM_Most_Recent_Activity__c, 
                            MM_Most_Recent_Activity_Date__c, MM_First_Activity__c,
                            MM_First_Activity_Id__c, MM_Most_Recent_Activity_Id__c, 
                            MM_First_Activity_Date__c 
                            FROM Contact
                            WHERE Id in :contacts]);
        Lead[] leads = ([SELECT Id, Company, MM_Most_Recent_Activity__c, 
                            MM_Most_Recent_Activity_Date__c, MM_First_Activity__c,
                            MM_First_Activity_Id__c, MM_Most_Recent_Activity_Id__c, 
                            MM_First_Activity_Date__c 
                            FROM Lead
                            WHERE Id in :contacts]); */
        List<Contact> contacts = new List<Contact>();
        List<Lead> leads = new List<Lead>();    
        List<Id> contactLeadIds = new List<Id>();                
        for (SObject so : contactsAndLeads) {
        	if (String.valueOf(so.Id).startsWith('003') ) {
        		contacts.add((Contact)so);
        	}
        	else {
        		leads.add((Lead)so);
        	}
        	contactLeadIds.add(so.Id);
        }                    

        Task[] AllTasks = ([SELECT Id, WhoId, Subject, CreatedDate FROM Task
                            WHERE Subject != null 
                            AND IsDeleted != TRUE
                            AND WhoId in :contactLeadIds
                            ORDER BY CreatedDate 
                            ALL ROWS]);
        AggregateResult[] tasksummary = ([SELECT Count(Id) totals, whoid, IsClosed FROM TASK
                            WHERE IsDeleted != TRUE
                            AND WhoId in :contactLeadIds
                            GROUP BY WhoId, IsClosed]);
                            
        for (Task t : AllTasks) {
            if ((firstMatches == null) ||
                (firstMatches.get(t.whoid) == null)) { 
                firstMatches.put(t.whoid, t);
            }
            lastMatches.put(t.whoid, t);
        }

		String recordid = '';
		Integer totals = 0;
		String isclosed = '';
		for (Integer i=0; i < tasksummary.size(); i++) {											// Loop thru the metrics retrieved
			recordid = String.valueof(tasksummary[i].get('WhoId'));									// Save our WhoId
			isclosed = String.valueof(tasksummary[i].get('IsClosed'));								// Is this for Open or Closed Activities
			totals = Integer.valueof(String.valueof(tasksummary[i].get('totals')));					// And Totals
			activityStats.put(recordid + ':' + isclosed, totals);
		}

        /*****************************************************************************************
        *   Record Processing
        ******************************************************************************************/
        if (SaveContactActivities) {
            for (Contact c : contacts) {
                c.MM_First_Activity_Id__c = null;
                c.MM_First_Activity__c = null;
                c.MM_First_Activity_Date__c = null;
                c.MM_Most_Recent_Activity_Id__c = null;
                c.MM_Most_Recent_Activity__c = null;
                c.MM_Most_Recent_Activity_Date__c = null;
                c.Number_of_Activities__c = 0;
                c.Number_of_Open_Activities__c = 0;
                if (activityStats != null) {
                	if (activityStats.get(c.id+':true') != null) {
                		c.Number_of_Activities__c = activityStats.get(c.id+':true');
                	}
                	if (activityStats.get(c.id+':false') != null) {
                		c.Number_of_Open_Activities__c = activityStats.get(c.id+':false');
                		c.Number_of_Activities__c += activityStats.get(c.id+':false');
                	}
				}
                Contact ct = new Contact(id=c.id, MM_First_Activity_Id__c = c.MM_First_Activity_Id__c,
                                        MM_First_Activity__c = c.MM_First_Activity__c,
                                        MM_First_Activity_Date__c = c.MM_First_Activity_Date__c,
                                        Number_of_Activities__c = c.Number_of_Activities__c,
                                        Number_of_Open_Activities__c = c.Number_of_Open_Activities__c);
                Contact lct = new Contact(id=c.id, MM_Most_Recent_Activity_Id__c = c.MM_Most_Recent_Activity_Id__c,
                                        MM_Most_Recent_Activity__c = c.MM_Most_Recent_Activity__c,
                                        MM_Most_Recent_Activity_Date__c = c.MM_Most_Recent_Activity_Date__c,
                                        Number_of_Activities__c = c.Number_of_Activities__c,
                                        Number_of_Open_Activities__c = c.Number_of_Open_Activities__c);
                if ((firstMatches != null) &&
                    (firstMatches.size() > 0) &&
                    (firstMatches.get(c.id) != null) &&
                    (firstMatches.get(c.id).subject != null) &&
                    (c.MM_First_Activity_Id__c != firstMatches.get(c.id).id)) {
                    ct.MM_First_Activity_Id__c = firstMatches.get(c.id).id;
                    ct.MM_First_Activity__c = firstMatches.get(c.id).subject;
                    ct.MM_First_Activity_Date__c = firstMatches.get(c.id).createddate;
                }
                if ((lastMatches != null) &&
                    (lastMatches.size() > 0) &&
                    (lastMatches.get(c.id) != null) &&
                    (lastMatches.get(c.id).subject != null) &&
                    (c.MM_Most_Recent_Activity_Id__c != lastMatches.get(c.id).id)) {
                    lct.MM_Most_Recent_Activity_Id__c = lastMatches.get(c.id).id;
                    lct.MM_Most_Recent_Activity__c = lastMatches.get(c.id).subject;
                    lct.MM_Most_Recent_Activity_Date__c = lastMatches.get(c.id).createddate;
                }
                firstcontactstoupd.add(ct);
                lastcontactstoupd.add(lct);
            }
        }

        if (SaveLeadActivities) {
            for (Lead l : leads) {
                l.MM_First_Activity_Id__c = null;
                l.MM_First_Activity__c = null;
                l.MM_First_Activity_Date__c = null;
                l.MM_Most_Recent_Activity_Id__c = null;
                l.MM_Most_Recent_Activity_Id__c = null;
                l.MM_Most_Recent_Activity_Id__c = null;
                l.Number_of_Activities__c = 0;
                l.Number_of_Open_Activities__c = 0;
                if (activityStats != null) {
                	if (activityStats.get(l.id+':true') != null) {
                		l.Number_of_Activities__c = activityStats.get(l.id+':true');
                	}
                	if (activityStats.get(l.id+':false') != null) {
                		l.Number_of_Open_Activities__c = activityStats.get(l.id+':false');
                		l.Number_of_Activities__c += activityStats.get(l.id+':false');
                	}
				}
                Lead ld = new Lead(id=l.id, MM_First_Activity_Id__c = l.MM_First_Activity_Id__c,
                                        MM_First_Activity__c = l.MM_First_Activity__c,
                                        MM_First_Activity_Date__c = l.MM_First_Activity_Date__c,
                                        Number_of_Activities__c = l.Number_of_Activities__c,
                                        Number_of_Open_Activities__c = l.Number_of_Open_Activities__c);
                Lead lld = new Lead(id=l.id, MM_Most_Recent_Activity_Id__c = l.MM_Most_Recent_Activity_Id__c,
                                        MM_Most_Recent_Activity__c = l.MM_Most_Recent_Activity__c,
                                        MM_Most_Recent_Activity_Date__c = l.MM_Most_Recent_Activity_Date__c,
                                        Number_of_Activities__c = l.Number_of_Activities__c,
                                        Number_of_Open_Activities__c = l.Number_of_Open_Activities__c);
                if ((firstMatches != null) &&
                    (firstMatches.size() > 0) &&
                    (firstMatches.get(l.id) != null) &&
                    (firstMatches.get(l.id).subject != null) &&
                    (l.MM_First_Activity_Id__c != firstMatches.get(l.id).id)) {
                    ld.MM_First_Activity_Id__c = firstMatches.get(l.id).id;
                    ld.MM_First_Activity__c = firstMatches.get(l.id).subject;
                    ld.MM_First_Activity_Date__c = firstMatches.get(l.id).createddate;
                }
                if ((lastMatches != null) &&
                    (lastMatches.size() > 0) &&
                    (lastMatches.get(l.id) != null) &&
                    (lastMatches.get(l.id).subject != null) &&
                    (l.MM_Most_Recent_Activity_Id__c != lastMatches.get(l.id).id)) {
                    lld.MM_Most_Recent_Activity_Id__c = lastMatches.get(l.id).id;
                    lld.MM_Most_Recent_Activity__c = lastMatches.get(l.id).subject;
                    lld.MM_Most_Recent_Activity_Date__c = lastMatches.get(l.id).createddate;
                }
                firstleadstoupd.add(ld);
                lastleadstoupd.add(lld);
            }
        }
        
        /*****************************************************************************************
        *   Record Post-Processing
        ******************************************************************************************/
        if (firstcontactstoupd.size() > 0) {
        	try {
        		Database.DMLOptions dmo = new Database.DMLOptions();
				dmo.optAllOrNone = false;
				Database.SaveResult[] srList = Database.update (firstcontactstoupd, dmo);
			}
			catch (exception e) {
				system.debug('Error updating contacts, firstcontactstoupd='+firstcontactstoupd);				
			}
        }
        if (lastcontactstoupd.size() > 0) {
        	try {
        		Database.DMLOptions dmo = new Database.DMLOptions();
				dmo.optAllOrNone = false;
				Database.SaveResult[] srList = Database.update (lastcontactstoupd, dmo);
			}
			catch (exception e) {
				system.debug('Error updating contacts, lastcontactstoupd='+lastcontactstoupd);				
			}
        }
        if (firstleadstoupd.size() > 0) {
        	try {
        		Database.DMLOptions dmo = new Database.DMLOptions();
				dmo.optAllOrNone = false;
				Database.SaveResult[] srList = Database.update (firstleadstoupd, dmo);
			}
			catch (exception e) {
				system.debug('Error updating leads, firstleadstoupd='+firstleadstoupd);				
			}
        }
        if (lastleadstoupd.size() > 0) {
        	try {
        		Database.DMLOptions dmo = new Database.DMLOptions();
				dmo.optAllOrNone = false;
				Database.SaveResult[] srList = Database.update (lastleadstoupd, dmo);
			}
			catch (exception e) {
				system.debug('Error updating leads, lastleadstoupd='+lastleadstoupd);				
			}
        }        
	}
}