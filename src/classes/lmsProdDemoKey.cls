public class lmsProdDemoKey{
    public account2installbases__x a;
    public String licenseId;
    public Boolean hasCustomPermission {get; set;}
    
    public lmsProdDemoKey(ApexPages.StandardController controller){
        this.a = (account2installbases__x)controller.getRecord();
    }
    
    public PageReference generateKey(){
        licenseId = ApexPages.currentPage().getParameters().get('id');
        hasCustomPermission = FeatureManagement.checkPermission('Generate_Demo_Key_Permission');
        system.debug('@@@ hasCustomPermission: ' + hasCustomPermission);
        if (!hasCustomPermission)
        {
        	//page = new PageReference('/apex/PermissionErrorMessage');
        	//page.setRedirect(true);
        	//return page;
        	ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'You do not have permission to perform this function'));
        	return null;
        }
       
        system.debug('querying the product values: ' );
        account2installbases__x ib = [select id, ExternalId, maintenanceCode__c, Id__c, hostname__c, productSize__c, productName__c from account2installbases__x where id =: licenseId];
        system.debug('After LMS Product query: ' + ib);
        string productName;
        string xId          = ib.ExternalId;
        string hostName     = ib.hostname__c;
        decimal productSize = ib.productSize__c;
        system.debug('After LMS Product query: ' + ib.productName__c);
        
        PageReference page;
        
        
        String pageRefString = '/apex/generateDemoKey?';
        
        if(ib.productName__c != null){
            if(ib.productName__c.contains('SecurityCenter')){
                productName = 'sc_42';
                pageRefString += 'productSize=' + productSize + '&productName=' + productName + '&hostName=' + hostName + '&externalId=' + xId;
            }else if(ib.productName__c.contains('Industrial Security')){
                productName = 'is_150';
                pageRefString += 'productName=' + productName + '&externalId=' + xId;
            }else if(ib.productName__c.contains('Log Correlation Engine')){
                productName = 'lce_46_1024';
                pageRefString += 'productName=' + productName + '&externalId=' + xId;
            }else if(ib.productName__c.contains('Tenable.io')){
                productName = 'vm';
                pageRefString += 'size=' + productSize + '&productName=' + productName + '&externalId=' + xId;
            }else if(ib.productName__c.contains('Passive Scanner')){
                productName = 'pvs_421_10';
                pageRefString += 'productName=' + productName + '&hostName=' + hostName + '&externalId=' + xId;
            }
            pageRefString += '&accountId=' + ib.Id__c;
        }
        
        //urlEncoding string
        String encoded = EncodingUtil.urlEncode(pageRefString, 'UTF-8');
        

        page = new PageReference(pageRefString);
        page.setRedirect(true);
        system.debug('Roy8: ' + page);
        return page;
    }
    
    public PageReference cancel(){
    	return new PageReference('/' + licenseId);
    }
   
}