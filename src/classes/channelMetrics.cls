global with sharing class channelMetrics {
/******************************************************************************
*
*	Custom Controller for Metrics to Accounts
*	Copyright 2006-2015 (c) by CloudLogistix.com
*	All Rights Reserved, Modifications can only be made for your direct use and 
*	not for distribution without explicit permission from CloudLogistix.
*
********************************************************************************/


    /*****************************************************************************************
    *   Variable Initialization
    ******************************************************************************************/
    public List<Data> leadscreated = new List <Data>();
    public List<Data> partnerpassedleads = new List <Data>();
    public List<Data> recycledleads = new List <Data>();
	public List<Data> toberecycledleads = new List <Data>();
    public List<Data> partnersubmittedleads = new List <Data>();
	public List<Data> partnercreatedpipeline = new List <Data>();
	public List<Data> partnerpipeline = new List <Data>();
	public List<Data> partnerbookings = new List <Data>();
	public List<Data> partnerdealreg = new List <Data>();
	public List<Data> partnerlosses = new List <Data>();   
	public List<Data> partnermdfsubmitted = new List <Data>();
	public List<Data> partnermdfapproved = new List <Data>();
	public List<Data> partnermdfclaimssubmitted = new List <Data>();
	public List<Data> partnermdfclaimsapproved = new List <Data>();
    
    public Set <Id> userids = new Set <Id>();
	/*****************************************************************************************
	*	Variable Initialization
	******************************************************************************************/
	public Id pageid = ApexPages.currentPage().getParameters().get('id');		// Store Account ID
	private final Account entry;												// Store Account Record
	private final ApexPages.standardController stdController;					// Set-up Controller Class

	/*****************************************************************************************
	*	Custom Controller Initialization
	******************************************************************************************/
    public channelMetrics(ApexPages.StandardController stdController) {			// Controller Set-up Method
        Account entry = (Account)stdController.getRecord();						// First get record from Page Call
        this.stdController = stdController;										// Set-up Controller
        loadUserIds();
		loadMetrics();
    } 

    /*****************************************************************************************
    *   Wrapper Class definitions
    ******************************************************************************************/
    global class Data {                                                                				// Wrapper class to store metric info
        public String name {get; set;}																// Store the period these metrics are for
        public Decimal data1 {get; set;}															// Store the overall metric for the period
        public Data(String name, Decimal data1) {
			this.name = name;
			this.data1 = data1;
		}
    }

	/*****************************************************************************************
	*	Getter/Setter's to return values to VF Page(s)
	******************************************************************************************/ 
	public Id getAccountId() {return pageid;}
	
	public List<Data> getLeadsYTD() {return leadscreated;}
	public List<Data> getpassedYTD() {return partnerpassedleads;}
	public List<Data> getrecycledYTD() {return recycledleads;}
	public List<Data> gettoberecycledYTD() {return toberecycledleads;}
	public List<Data> getsubmittedYTD() {return partnersubmittedleads;}

	public List<Data> getpartnerCreatedPipeline() {return partnercreatedpipeline;}
	public List<Data> getpartnerPipeline() {return partnerpipeline;}
	public List<Data> getpartnerBookings() {return partnerbookings;}
	public List<Data> getpartnerDealReg() {return partnerdealreg;}
	public List<Data> getpartnerLosses() {return partnerlosses;}

	public List<Data> getpartnerMDFSubmitted() {return partnermdfsubmitted;}
	public List<Data> getpartnerMDFApproved() {return partnermdfapproved;}
	public List<Data> getpartnerMDFClaimsSubmitted() {return partnermdfclaimssubmitted;}
	public List<Data> getpartnerMDFClaimsApproved() {return partnermdfclaimsapproved;}

	/*****************************************************************************************
	*	Go Create the Metrics
	******************************************************************************************/ 
	public void loadUserIds() {
		User[] usrs = ([SELECT Id FROM User WHERE AccountId = :pageid]);		
		for (User u : usrs) {userids.add(u.id);}
	}

	public void loadMetrics() {
		Map <String, PartnerMetrics__c> pm = PartnerMetrics__c.getAll(); 
		leadscreated = getMetric('SELECT ' + pm.get('leadscreated').Select__c + ' FROM ' + pm.get('leadscreated').From__c + 
				' WHERE ' + pm.get('leadscreated').Where__c + ' GROUP BY ' + pm.get('leadscreated').Group__c + 
				' ORDER BY ' + pm.get('leadscreated').Order__c);
		partnerpassedleads = getMetric('SELECT ' + pm.get('partnerpassedleads').Select__c + ' FROM ' + pm.get('partnerpassedleads').From__c + 
				' WHERE ' + pm.get('partnerpassedleads').Where__c + ' GROUP BY ' + pm.get('partnerpassedleads').Group__c + 
				' ORDER BY ' + pm.get('partnerpassedleads').Order__c);
		recycledleads = getMetric('SELECT ' + pm.get('recycledleads').Select__c + ' FROM ' + pm.get('recycledleads').From__c + 
				' WHERE ' + pm.get('recycledleads').Where__c + ' GROUP BY ' + pm.get('recycledleads').Group__c + 
				' ORDER BY ' + pm.get('recycledleads').Order__c);
		toberecycledleads = getMetric('SELECT ' + pm.get('toberecycledleads').Select__c + ' FROM ' + pm.get('toberecycledleads').From__c + 
				' WHERE ' + pm.get('toberecycledleads').Where__c + ' GROUP BY ' + pm.get('toberecycledleads').Group__c + 
				' ORDER BY ' + pm.get('toberecycledleads').Order__c);
		partnersubmittedleads = getMetric('SELECT ' + pm.get('partnersubmittedleads').Select__c + ' FROM ' + pm.get('partnersubmittedleads').From__c + 
				' WHERE ' + pm.get('partnersubmittedleads').Where__c + ' GROUP BY ' + pm.get('partnersubmittedleads').Group__c + 
				' ORDER BY ' + pm.get('partnersubmittedleads').Order__c);
		partnercreatedpipeline = getMetric('SELECT ' + pm.get('partnercreatedpipeline').Select__c + ' FROM ' + pm.get('partnercreatedpipeline').From__c + 
				' WHERE ' + pm.get('partnercreatedpipeline').Where__c + ' GROUP BY ' + pm.get('partnercreatedpipeline').Group__c + 
				' ORDER BY ' + pm.get('partnercreatedpipeline').Order__c);
		partnerpipeline = getMetric('SELECT ' + pm.get('partnerpipeline').Select__c + ' FROM ' + pm.get('partnerpipeline').From__c + 
				' WHERE ' + pm.get('partnerpipeline').Where__c + ' GROUP BY ' + pm.get('partnerpipeline').Group__c + 
				' ORDER BY ' + pm.get('partnerpipeline').Order__c);
		partnerbookings = getMetric('SELECT ' + pm.get('partnerbookings').Select__c + ' FROM ' + pm.get('partnerbookings').From__c + 
				' WHERE ' + pm.get('partnerbookings').Where__c + ' GROUP BY ' + pm.get('partnerbookings').Group__c + 
				' ORDER BY ' + pm.get('partnerbookings').Order__c);
		partnerlosses = getMetric('SELECT ' + pm.get('partnerlosses').Select__c + ' FROM ' + pm.get('partnerlosses').From__c + 
				' WHERE ' + pm.get('partnerlosses').Where__c + ' GROUP BY ' + pm.get('partnerlosses').Group__c + 
				' ORDER BY ' + pm.get('partnerlosses').Order__c);
		partnerdealreg = getMetric('SELECT ' + pm.get('partnerdealreg').Select__c + ' FROM ' + pm.get('partnerdealreg').From__c + 
				' WHERE ' + pm.get('partnerdealreg').Where__c + ' GROUP BY ' + pm.get('partnerdealreg').Group__c + 
				' ORDER BY ' + pm.get('partnerdealreg').Order__c);
		partnermdfsubmitted = getMetric('SELECT ' + pm.get('partnermdfsubmitted').Select__c + ' FROM ' + pm.get('partnermdfsubmitted').From__c + 
				' WHERE ' + pm.get('partnermdfsubmitted').Where__c + ' GROUP BY ' + pm.get('partnermdfsubmitted').Group__c + 
				' ORDER BY ' + pm.get('partnermdfsubmitted').Order__c);
		partnermdfapproved = getMetric('SELECT ' + pm.get('partnermdfapproved').Select__c + ' FROM ' + pm.get('partnermdfapproved').From__c + 
				' WHERE ' + pm.get('partnermdfapproved').Where__c + ' GROUP BY ' + pm.get('partnermdfapproved').Group__c + 
				' ORDER BY ' + pm.get('partnermdfapproved').Order__c);
		partnermdfclaimssubmitted = getMetric('SELECT ' + pm.get('partnermdfclaimssubmitted').Select__c + ' FROM ' + pm.get('partnermdfclaimssubmitted').From__c + 
				' WHERE ' + pm.get('partnermdfclaimssubmitted').Where__c + ' GROUP BY ' + pm.get('partnermdfclaimssubmitted').Group__c + 
				' ORDER BY ' + pm.get('partnermdfclaimssubmitted').Order__c);
		partnermdfclaimsapproved = getMetric('SELECT ' + pm.get('partnermdfclaimsapproved').Select__c + ' FROM ' + pm.get('partnermdfclaimsapproved').From__c + 
				' WHERE ' + pm.get('partnermdfclaimsapproved').Where__c + ' GROUP BY ' + pm.get('partnermdfclaimsapproved').Group__c + 
				' ORDER BY ' + pm.get('partnermdfclaimsapproved').Order__c);

	}


	public List <Data> getMetric(string querystring) {

		AggregateResult[] periodsummary; 															// Store the raw metrics
		List <Data> returndata = new List <Data>();

		/*****************************************************************************************
		*	Record Pre-Processing
		******************************************************************************************/
		periodsummary = Database.query(querystring);												// Go get the results

		/*****************************************************************************************
		*	Record Processing
		******************************************************************************************/
		for (Integer i=0; i < periodsummary.size(); i++) {											// Loop thru the metrics retrieved
			String Period = '';
			Decimal Count = 0.0;																	// Zero out our metrics placeholder
			String month = '0' + String.valueof(periodsummary[i].get('month'));
			period = String.valueof(periodsummary[i].get('year'))+'-'+month.substring(month.length()-2);
			if (String.valueof(periodsummary[i].get('Totals')) != null) {							// Do we have results for this period?
				Count = Decimal.valueof(String.valueof(periodsummary[i].get('Totals')));			// Yes, so convert them into Decimal
			}
			returndata.add(new Data(Period, Count));
		}
		return returndata;
	}

}