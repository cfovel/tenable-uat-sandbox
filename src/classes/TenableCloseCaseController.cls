public without sharing class TenableCloseCaseController {
  
  public class CaseStatus {
  	private string status;
  	private boolean reopenEligible;
  	
  	public CaseStatus(CaseStatusBuilder builder){
  	  this.status = builder.getStatus();
  	  this.reopenEligible = builder.getReopenEligible();
  	}
  	
  	@AuraEnabled
  	public String getStatus(){
  		return this.status;
  	}
  	
  	@AuraEnabled
  	public Boolean getReopenEligible(){
  		return this.reopenEligible;
  	}
  }
  
  public class CaseStatusBuilder {
  	private string status;
  	private boolean reopenEligible;
    public CaseStatusBuilder(){
    }
    public String getStatus(){
    	return status;
    }
    public Boolean getReopenEligible(){
    	return reopenEligible;
    }
    public CaseStatusBuilder setStatus(String status){
    	this.status = status;
    	return this;
    }
    public CaseStatusBuilder setReopenEligible(Boolean reopenEligible){
    	this.reopenEligible = reopenEligible;
    	return this;
    }
    public CaseStatusBuilder caseRecord(Case c){
    	Boolean isReopenEligible = c.ClosedDate != null && c.ClosedDate.date().daysBetween(Date.today()) <= 30;
    	return setStatus(c.Status)
    	 .setReopenEligible(isReopenEligible);
    }
    public CaseStatus build(){
    	return new CaseStatus(this);
    }
  }
  
  public TenableCloseCaseController(){}
  
  @AuraEnabled
  public static CaseStatus getCaseStatus(Id recordId){
  	if(String.isBlank(recordId)){
  		return null;
  	}
    List<Case> cL = [SELECT ClosedDate, Status FROM Case WHERE Id = :recordId];
    if(cL != null && !cL.isEmpty()){
      return new CaseStatusBuilder()
        .caseRecord(cL[0])
        .build();
    }
    return null;
  } 
  
  @AuraEnabled
  public static Case reopenCaseWithComments(Id recordId, String CaseCommentContent){
    // read case record to close
    Case c = [SELECT CaseNumber, Status, Subject, Description FROM Case WHERE Id = :recordId];
    // create a case comment while reopening
    CaseComment Comment = new CaseComment();
    Comment.CommentBody = CaseCommentContent;
    Comment.ParentId = c.Id;
    insert Comment;
    return c;
  }
  
  @AuraEnabled
  public static Case closeCaseWithComments(Id recordId, String CaseCommentContent, String ResolutionDetail){
  	Savepoint sp;
    try{
      sp = Database.setSavepoint();
      // read case record to close
      Case c = [SELECT CaseNumber, Status, Subject, Description FROM Case WHERE Id = :recordId];
      // create a case comment while closing
      CaseComment Comment = new CaseComment();
      Comment.CommentBody = CaseCommentContent;
      Comment.ParentId = c.Id;
      insert Comment;
      // close this case
      c.Status = 'Closed';
      c.Resolution_Detail__c = ResolutionDetail;
      update c;
      // if everything worked fine then return true
      return c;
    }
	catch(Exception err){
	  // rollback any transactions that might have occured before this error happened
	  Database.rollback(sp);
	  // give back the error message for the client to handle
      throw new AuraHandledException('Error while closing this case. '+err.getMessage());
	}
  }
    
}