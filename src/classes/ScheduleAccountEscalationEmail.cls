global class ScheduleAccountEscalationEmail implements Database.Batchable<sobject>, Schedulable 
{
     public string query = 'SELECT Id, Email_Address__c From AccountEscalationEmails__c ' + (Test.isRunningTest() ? ' limit 49' : '');
     public static boolean hasErrors = false;
     public static list<string> lstErrs = new list<string>();
 
    
     global ScheduleAccountEscalationEmail()
     {
        
     }
    
     global Database.QueryLocator start(Database.BatchableContext bc)
     {
       return Database.getQueryLocator(query);
     }
    
     global void execute (SchedulableContext context) 
     {
        Database.executeBatch(new ScheduleAccountEscalationEmail());
     }
    
     global void execute(Database.BatchableContext bc, List<sObject> scope)
     {
        Id TargetObjectId = [SELECT Id, Name FROM User WHERE Name=:'Automation User'].Id;
        Id EmailTemplateId = [SELECT Id, Name FROM EmailTemplate WHERE Name=: 'Weekly Escalation Report'].Id;
        Id WorkspaceId = [SELECT Id, Name FROM ContentWorkspace WHERE Name=: 'Account Escalation Attachments'].Id;
        ContentVersion cv = [Select Id,Title,FirstPublishLocationId,VersionData from ContentVersion where title=:'Escalated Accounts' and islatest=:true]; 
       
        List<String> emailAddrs = new List<String>();
        
        for (sObject sc: scope) 
        {
            AccountEscalationEmails__c ae = (AccountEscalationEmails__c) sc;
            emailAddrs.add(ae.email_address__c);
        }
        
        PageReference ref = Page.EscalatedAccounts;
        Blob b;
        Blob b1;
        
        if (Test.IsRunningTest()) 
        {
            b = Blob.valueOf('UNIT.TEST');
            b1= Blob.valueOf('UNIT.TEST');
        }
        else 
        {
            b = ref.getContentAsPDF();
            
            if(cv != NULL)
            {
                b1 = cv.VersionData;
            }
        }
        
        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
        efa.setFileName('EscalatedAccounts.pdf');
        efa.setBody(b);
        Messaging.EmailFileAttachment efa1 = new Messaging.EmailFileAttachment();
        efa1.setFileName('CorpEscalatedAccountsList.pdf');
        efa1.setBody(b1);
        
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        // test: '00XP0000000QctO', prod: '00X60000005AQEi'
        //email.setTemplateId('00X60000005AQEi');                                                 
        //email.setTargetObjectId('00560000001T5Tz');
        email.setTemplateId(EmailTemplateId);
        email.setTargetObjectId(TargetObjectId);
        email.setSaveAsActivity(false);
        email.setSenderDisplayName('Salesforce Automation');
        email.setReplyTo('bizplatforms@tenable.com');
        email.setToAddresses(emailAddrs);
        email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa,efa1});
        
        try
        {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
        }
        catch (Exception e) 
        {
            hasErrors = true;
            lstErrs.add(e.getMessage());
        }
     }

    
     global void finish(Database.BatchableContext bc)
     {
        if(!test.isRunningTest())
        {
            if(hasErrors)
            {
                string htmlBody = 'The following errors occured during processing: <br/>';
                
                for(string s: lstErrs)
                {
                    htmlBody += s + '<br/>';
                }
                
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                string[] toAddress = new string[] {'bizplatforms@tenable.com'};
                mail.setToAddresses(toAddress);
                mail.setReplyTo('bmuniswamy@tenable.com');
                mail.setSenderDisplayName('ScheduleAccountEscalation Batch');
                mail.setSubject('ScheduleAccountEscalation Batch Completed With Errors');
                mail.setHtmlBody(htmlbody);
                
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }
        }
     }
}