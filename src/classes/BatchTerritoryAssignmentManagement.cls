global class BatchTerritoryAssignmentManagement implements Database.Batchable<sObject> {
    
    DateTime now = DateTime.now();
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([SELECT Id FROM TerritoryAssignment__c WHERE EndAt__c < :now AND Territory__c <> null]);
    }
    
    global void execute(Database.BatchableContext bc, List<TerritoryAssignment__c> territoryAssignments) {
        
        if (!territoryAssignments.isEmpty()) {
            update territoryAssignments;
        }
         
    }
    
    global void finish(Database.BatchableContext bc) {}
    
}