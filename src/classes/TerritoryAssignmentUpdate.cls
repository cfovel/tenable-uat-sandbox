public class TerritoryAssignmentUpdate {
/*******************************************************************************
*
* Global class for updating Territory and OwnerId on Accounts
* Version 1.0
*
* Called by AccountTriggerHandler (when Custom Setting MarketingQuickStartSettings.AccountUpdateTerritory = true)
*   and baUpdateTerritoryAssignments (batch apex)
* 
*******************************************************************************/
	/*
    public static List<Account> updateTerritoryAssignments(List<Account> accts)
    {
        final String PARTNER_RECORD_TYPE_ID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner Account').getRecordTypeId();
        final String VENDOR_RECORD_TYPE_ID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor Account').getRecordTypeId();
        Boolean findTerritories = false;
        List<Account> updates = new List<Account>();
        Map <String, ISOCountryCodes__c> countries = utils.countries();                    // Define Map based on the Country Name 
        Map <String, PostalCodes__c> states = utils.states();                             // Define Map based on the State Name 
        

        //   Record Pre-Processing
        //   Possible search criteria in priority order:
        //       Country Code, Market Segment, Postal Code (5-digits), NAICS Code (first 2 digits)   - used for AMER - US
        //       Country Code, Market Segment, Postal Code range - used for AMER - US
        //       Country Code, Market Segment, NAICS Code (6-digits) - used for EMEA - UK
        //       Country Code, Market Segment, Postal Code (first 2 digits) - used for EMEA - UK
        //       Country Code, Market Segment, State/Province - used for APAC = China (full name of state), AMER - Canada (state 2-char abbreviation)
        
        Map<String,TerritoryAssignmentRules__c> terrAssignRulesMap = TerritoryAssignmentRules__c.getAll();  
        Map<String,TerritoryAssignmentRules__c> terrAssignMap = new Map<String,TerritoryAssignmentRules__c>();
        List<TerritoryAssignmentRules__c> terrAssignPostalRanges = new List<TerritoryAssignmentRules__c>();
        List<String> terrAssignToRemove = new List<String>();
        Map<Id, DandBCompany> acctDandBMap = new Map <Id, DandBCompany>();
        Map<Id, DandBCompany> acctDomUltMap = new Map <Id, DandBCompany>();
        Map<Id, DandBCompany> acctGlobalUltMap = new Map <Id, DandBCompany>();
        Map<Id, DandBCompany> acctParentDandBMap = new Map <Id, DandBCompany>();
        Map<Id, Account> acctParentMap = new Map <Id, Account>();
        Map<Id, List<Id>> DandBIdAcctIdsMap = new Map <Id, List<Id>>();
        Map<String, List<Id>> DomUltDunsAcctIdsMap = new Map <String, List<Id>>();
        Map<String, List<Id>> GlobalUltDunsAcctIdsMap = new Map <String, List<Id>>();
        Map<String, List<Id>> ParentDunsAcctIdsMap = new Map <String, List<Id>>();
        Map<Id, List<Id>> parentIdAcctIdsMap = new Map <Id, List<Id>>();
        Boolean skipTerrAssign;
            
        for (Account acct : accts)
        {
            skipTerrAssign = false;
            if ((acct.Sales_Area__c != 'APAC') &&                                                                           // Do not update Territory for Partner accounts, except for APAC
                (acct.RecordTypeId == PARTNER_RECORD_TYPE_ID || acct.RecordTypeId == VENDOR_RECORD_TYPE_ID ||acct.Name.endsWithIgnoreCase('NFR') ||
                 acct.Type == 'Partner' || acct.Type == 'Prospect Partner' || acct.Type == 'Vendor' || acct.Type == 'Press'))
            {
                skipTerrAssign = true;
            }
			
            if (!acct.IsExcludedFromRealign && (acct.Territory__c == null || acct.Territory__c == '') && !skipTerrAssign)
            {
                findTerritories = true;
                if (acct.DandBCompanyId != null)
                {
                    if (DandBIdAcctIdsMap.containsKey(acct.DandBCompanyId)) {
                        DandBIdAcctIdsMap.get(acct.DandBCompanyId).add(acct.Id);
                    }
                    else {
                        DandBIdAcctIdsMap.put(acct.DandBCompanyId,new List<Id> {acct.Id});
                    }
                }
                
                if (acct.Site == 'Branch' && acct.ParentId != null)
                {
                    if (parentIdAcctIdsMap.containsKey(acct.ParentId)) {
                        parentIdAcctIdsMap.get(acct.ParentId).add(acct.Id);
                    }
                    else {
                        parentIdAcctIdsMap.put(acct.ParentId,new List<Id> {acct.Id});
                    }
                }
            }
        }
        
        if (findTerritories)
        {
            TerritoryAssignmentRules__c terrAssign = new TerritoryAssignmentRules__c();
            for (String taKey : TerrAssignRulesMap.keySet())
            {
                terrAssign = TerrAssignRulesMap.get(taKey);
                if (terrAssign.Start_Date__c <= Date.today() && (terrAssign.End_Date__c == null || terrAssign.End_Date__c > Date.today()))
                {
                    if (terrAssign.Postal_Start__c != null && terrAssign.Postal_Start__c.isNumeric() && 
                        terrAssign.Postal_End__c != null && terrAssign.Postal_End__c.isNumeric())
                    {
                        terrAssignPostalRanges.add(terrAssign);
                    }
                    else
                    {
                        terrAssignMap.put(taKey,terrAssign);
                    }
                }
            }   
                
            if (!DandBIdAcctIdsMap.isEmpty())
            {
                for (DandBCompany DandBCo : [SELECT Id, DunsNumber, DomesticUltimateDunsNumber, GlobalUltimateDunsNumber, ParentOrHqDunsNumber, EmployeesTotal,
                    GlobalUltimateTotalEmployees, EmployeesHere, PostalCode, PrimaryNAICS, Country, City, State, LocationStatus
                    FROM DandBCompany WHERE Id IN :DandBIdAcctIdsMap.keySet()])
                {
                    List<Id> acctIds = DandBIdAcctIdsMap.get(DandBCo.Id);
                    for (Id acctId : acctIds)
                    {
                        acctDandBMap.put(acctId, DandBCo);

                        if ((DandBCo.DomesticUltimateDunsNumber != null) && (DandBCo.DomesticUltimateDunsNumber != '') && (DandBCo.DomesticUltimateDunsNumber != '000000000'))
                        {
                            if (DomUltDunsAcctIdsMap.containsKey(DandBCo.DomesticUltimateDunsNumber)) {
                                DomUltDunsAcctIdsMap.get(DandBCo.DomesticUltimateDunsNumber).add(acctId);
                            }
                            else {
                                DomUltDunsAcctIdsMap.put(DandBCo.DomesticUltimateDunsNumber,new List<Id> {acctId});
                            }
                        }
                            
                        if ((DandBCo.GlobalUltimateDunsNumber != null) && (DandBCo.GlobalUltimateDunsNumber != '') && (DandBCo.GlobalUltimateDunsNumber != '000000000'))
                        {
                            if (GlobalUltDunsAcctIdsMap.containsKey(DandBCo.GlobalUltimateDunsNumber)) {
                                GlobalUltDunsAcctIdsMap.get(DandBCo.GlobalUltimateDunsNumber).add(acctId);
                            }
                            else {
                                GlobalUltDunsAcctIdsMap.put(DandBCo.GlobalUltimateDunsNumber,new List<Id> {acctId});
                            }
                        }
                            
                        if ((DandBCo.LocationStatus == '2') && (DandBCo.ParentOrHqDunsNumber != null) && (DandBCo.ParentOrHqDunsNumber != '') && (DandBCo.ParentOrHqDunsNumber != '000000000'))
                        {
                            if (ParentDunsAcctIdsMap.containsKey(DandBCo.ParentOrHqDunsNumber)) {
                                ParentDunsAcctIdsMap.get(DandBCo.ParentOrHqDunsNumber).add(acctId);
                            }
                            else {
                                ParentDunsAcctIdsMap.put(DandBCo.ParentOrHqDunsNumber,new List<Id> {acctId});
                            }
                        }
                    }
                }
            }
                
            if (!DomUltDunsAcctIdsMap.isEmpty() || !GlobalUltDunsAcctIdsMap.isEmpty() || !ParentDunsAcctIdsMap.isEmpty())
            {
                List<String> relatedDuns = new List<String>(DomUltDunsAcctIdsMap.keySet());
                relatedDuns.addAll(GlobalUltDunsAcctIdsMap.keySet());
                relatedDuns.addAll(ParentDunsAcctIdsMap.keySet());
                
                for (DandBCompany DandBCo : [SELECT Id, DunsNumber, DomesticUltimateDunsNumber, GlobalUltimateDunsNumber, ParentOrHqDunsNumber, EmployeesTotal, 
                    GlobalUltimateTotalEmployees, EmployeesHere, PostalCode, PrimaryNAICS, Country, City, State, LocationStatus
                    FROM DandBCompany WHERE DunsNumber IN :relatedDuns])                      
                {
                    List<Id> acctIds = DomUltDunsAcctIdsMap.get(DandBCo.DunsNumber);
                    if (acctIds != null && !acctIds.isEmpty())
                    {
                        for (Id acctId : acctIds)
                        {
                            acctDomUltMap.put(acctId, DandBCo);
                        }
                    }
                    
                    List<Id> acctIds2 = GlobalUltDunsAcctIdsMap.get(DandBCo.DunsNumber);
                    if (acctIds2 != null && !acctIds2.isEmpty())
                    {
                        for (Id acctId2 : acctIds2)
                        {
                            acctGlobalUltMap.put(acctId2, DandBCo);
                        }
                    }
                    
                    List<Id> acctIds3 = ParentDunsAcctIdsMap.get(DandBCo.DunsNumber);
                    if (acctIds3 != null && !acctIds3.isEmpty())
                    {
                        for (Id acctId3 : acctIds3)
                        {
                            acctParentDandBMap.put(acctId3, DandBCo);
                        }
                    }
                }
            }    
            
            if (!parentIdAcctIdsMap.isEmpty())
            {
                for (Account parentAcct : [SELECT Id, BillingPostalCode, ShippingPostalCode, NumberOfEmployees, BillingCountry, BillingCity, BillingState, 
                    ShippingCountry, ShippingCity, ShippingState, NAICSCode, Territory__c, OwnerId, Industry
                        FROM Account WHERE Id IN :parentIdAcctIdsMap.keySet()])
                {
                    List<Id> acctIds = parentIdAcctIdsMap.get(parentAcct.Id);
                    if (acctIds != null && !acctIds.isEmpty())
                    {
                        for (Id acctId : acctIds)
                        {
                            acctParentMap.put(acctId, parentAcct);
                        }
                    }   
                }       
            }


	    	//   Record Processing

	        for (Account acct : accts) 
	        {
	         	//     New Territory Assignment Rules   
	            
	            skipTerrAssign = false;
	            if ((acct.Sales_Area__c != 'APAC') &&                                                                           // Do not update Territory for Partner accounts, except for APAC
	                (acct.RecordTypeId == PARTNER_RECORD_TYPE_ID || acct.RecordTypeId == VENDOR_RECORD_TYPE_ID ||acct.Name.endsWithIgnoreCase('NFR') ||
	                 acct.Type == 'Partner' || acct.Type == 'Prospect Partner' || acct.Type == 'Vendor' || acct.Type == 'Press'))
	            {
	                skipTerrAssign = true;
	            }    
	                                          
	            if (!acct.IsExcludedFromRealign && (acct.Territory__c == null || acct.Territory__c == '') && !skipTerrAssign)                                             
	            {
	                String mktSeg;
	                String NAICSCode;
	                Double nbrEmployees = 0;
	                String postalCode;
	                String countryCode;
	                String city;
	                String state;
	                String industryClass;
	                
	                DandBCompany domUltCo = acctDomUltMap.get(acct.Id);
	                
	                DandBCompany globalUltCo = acctGlobalUltMap.get(acct.Id);
	                
	                DandBCompany DandBCo = acctDandBMap.get(acct.Id);
	                
	                DandBCompany parentDandBCo = acctParentDandBMap.get(acct.Id);
	                
	                Account parentAcct = acctParentMap.get(acct.Id);
	                
	                industryClass = acct.Industry;
	                    
	                if (DandBCo != null && DandBCo.PostalCode != null && DandBCo.PostalCode != '')                      // find postal code
	                {
	                    postalCode = DandBCo.PostalCode;
	                }
	                else
	                {
	                    if (acct.BillingPostalCode != null) 
	                    {
	                        postalCode = acct.BillingPostalCode;
	                    }
	                    else if (acct.ShippingPostalCode != null)
	                    {
	                        postalCode = acct.ShippingPostalCode;
	                    }
	                }    
	                    
	                if (DandBCo != null && DandBCo.Country != null && countries.get(DandBCo.Country) != null) {         // find country                           
	                    countryCode = countries.get(DandBCo.Country).Country_Code__c;
	                }
	                else
	                {
	                    if (acct.BillingCountry != null) 
	                    {
	                        countryCode = acct.BillingCountry;
	                    }
	                    else if (acct.ShippingCountry != null)
	                    {
	                        countryCode = acct.ShippingCountry;
	                    }
	                }
	                
	                if (DandBCo != null && DandBCo.City != null)                                                        // find city
	                {
	                    city = DandBCo.City;
	                }
	                else
	                {
	                    if (acct.BillingCity != null) 
	                    {
	                        city = acct.BillingCity;
	                    }
	                    else if (acct.ShippingCity != null)
	                    {
	                        city = acct.ShippingCity;
	                    }
	                }
	                    
	                if (countryCode == 'CA' && DandBCo != null && DandBCo.State != null &&
	                                (states.get(countryCode + ':' + DandBCo.State.tolowercase()) != null)) 
	                {
	                    state = states.get(countryCode + ':' + DandBCo.State.tolowercase()).State_Code__c;
	                }
	                else if (countryCode != 'CA' && DandBCo != null && DandBCo.State != null) 
	                {
	                    state = DandBCo.State;
	                }
	                else
	                {
	                    if (acct.BillingState != null) 
	                    {
	                        state = acct.BillingState;
	                    }
	                    else if (acct.ShippingState != null)
	                    {
	                        state = acct.ShippingState;
	                    }
	                }
	                    
	                if (DandBCo != null && DandBCo.LocationStatus == '2')                                   // if Branch location, use postal code from Parent or Domestic Ultimate
	                         
	                {
	                    if (parentDandBCo != null)
	                    {
	                        if (parentDandBCo.PostalCode != null && parentDandBCo.PostalCode != '')
	                        {
	                            postalCode = parentDandBCo.PostalCode;
	                        }
	                        
	                        if (parentDandBCo.Country != null && countries.get(parentDandBCo.Country) != null) {                                      
	                            countryCode = countries.get(parentDandBCo.Country).Country_Code__c;
	                        }
	                        
	                        if (parentDandBCo.City != null)
	                        {
	                            city = parentDandBCo.City;
	                        }
	                        
	                        if (countryCode == 'CA' && parentDandBCo != null && parentDandBCo.State != null &&
	                                (states.get(countryCode + ':' + parentDandBCo.State.tolowercase()) != null)) 
	                        {
	                            state = states.get(countryCode + ':' + parentDandBCo.State.tolowercase()).State_Code__c;
	                        }
	                        else if (countryCode != 'CA' && parentDandBCo.State != null)
	                        {
	                            state = parentDandBCo.State;
	                        }
	                    }
	                        
	                    if (domUltCo != null && (postalCode == null || countryCode == null || city == null || state == null))
	                    {
	                        if (postalCode == null && domUltCo.PostalCode != null && domUltCo.PostalCode != '')
	                        {
	                            postalCode = domUltCo.PostalCode;
	                        }
	                        
	                        if (countryCode == null && domUltCo.Country != null && countries.get(domUltCo.Country) != null) {                                      
	                            countryCode = countries.get(domUltCo.Country).Country_Code__c;
	                        }
	                        
	                        if (city == null && domUltCo.City != null)
	                        {
	                            city = domUltCo.City;
	                        }
	                        
	                        if (countryCode == 'CA' && domUltCo != null && domUltCo.State != null &&
	                                (states.get(countryCode + ':' + domUltCo.State.tolowercase()) != null)) 
	                        {
	                            state = states.get(countryCode + ':' + domUltCo.State.tolowercase()).State_Code__c;
	                        }
	                        else if (countryCode != 'CA' && state == null && domUltCo.State != null)
	                        {
	                            state = domUltCo.State;
	                        }
	                    }
	                }
	                    
	                if (acct.Site == 'Branch' && acct.ParentId != null && (postalCode == null || countryCode == null || city == null || state == null))
	                {
	                    if (parentAcct != null)                                                                     // get postal code from parent account
	                    {
	                        if (countryCode == null && parentAcct.BillingCountry != null) 
	                        {
	                            countryCode = parentAcct.BillingCountry;
	                        }
	                        
	                        if (countryCode == null && parentAcct.ShippingCountry != null)
	                        {
	                            countryCode = parentAcct.ShippingCountry;
	                        }
	                        
	                        if (postalCode == null && parentAcct.BillingPostalCode != null) 
	                        {
	                            postalCode = parentAcct.BillingPostalCode;
	                        }
	                        
	                        if (postalCode == null && parentAcct.ShippingPostalCode != null)
	                        {
	                            postalCode = parentAcct.ShippingPostalCode;
	                        }
	                        
	                        if (city == null && parentAcct.BillingCity != null) 
	                        {
	                            city = parentAcct.BillingCity;
	                        }
	                        
	                        if (city == null && parentAcct.ShippingCity != null)
	                        {
	                            city = parentAcct.ShippingCity;
	                        }
	                            
	                        if (state == null && parentAcct.BillingState != null) 
	                        {
	                            state = parentAcct.BillingState;
	                        }
	                        
	                        if (state == null && parentAcct.ShippingState != null)
	                        {
	                            state = parentAcct.ShippingState;
	                        }
	                    }                                                                               
	                }
	
	                if (globalUltCo != null)                                                    // find number of Employees
	                {
	                    if (globalUltCo.GlobalUltimateTotalEmployees > nbrEmployees)
	                    {
	                        nbrEmployees = globalUltCo.GlobalUltimateTotalEmployees;
	                    }
	                    if (globalUltCo.EmployeesTotal > nbrEmployees)
	                    {
	                        nbrEmployees = globalUltCo.EmployeesTotal;
	                    }
	                    if (globalUltCo.EmployeesHere > nbrEmployees)
	                    {
	                        nbrEmployees = globalUltCo.EmployeesHere;
	                    }
	                }
	                
	                if (domUltCo != null) 
	                {
	                    if (domUltCo.GlobalUltimateTotalEmployees > nbrEmployees)
	                    {
	                        nbrEmployees = domUltCo.GlobalUltimateTotalEmployees;
	                    }
	                    if (domUltCo.EmployeesTotal > nbrEmployees)
	                    {
	                        nbrEmployees = domUltCo.EmployeesTotal;
	                    }
	                    if (domUltCo.EmployeesHere > nbrEmployees)
	                    {
	                        nbrEmployees = domUltCo.EmployeesHere;
	                    }
	                }
	                    
	                if (DandBCo != null)
	                {
	                    if (DandBCo.GlobalUltimateTotalEmployees > nbrEmployees)
	                    {
	                        nbrEmployees = DandBCo.GlobalUltimateTotalEmployees;
	                    }
	                    if (DandBCo.EmployeesTotal > nbrEmployees)
	                    {
	                        nbrEmployees = DandBCo.EmployeesTotal;
	                    }
	                    if (DandBCo.EmployeesHere > nbrEmployees)
	                    {
	                        nbrEmployees = DandBCo.EmployeesHere;
	                    }
	                }
	                
	                if (parentAcct != null && parentAcct.NumberOfEmployees > nbrEmployees)
	                {
	                    nbrEmployees = parentAcct.NumberOfEmployees;
	                }
	                
	                if (nbrEmployees <= 0 && acct.NumberOfEmployees > 0)
	                {
	                    nbrEmployees = acct.NumberOfEmployees;
	                }
	                    
	                if (acct.Sales_Area__c == 'APAC')                                           // For APAC, do not use nbr employess for Mkt Seg
	                {
	                    if (acct.Type != null && acct.Type.contains('Partner'))
	                    {
	                        mktSeg = 'Channel';
	                    }
	                    else if (countryCode == 'JP' || countryCode == 'CN' || countryCode == 'HK' || countryCode == 'MN' || countryCode == 'MO' || countryCode == 'TW')
	                    {
	                        mktSeg = 'Emerging';
	                    }
	                    else
	                    {
	                        mktSeg = 'Mid-Market';
	                    }
	                }
	                else if (acct.Sales_Area__c == 'LATAM')
	                {
	                    mktSeg = 'Mid-Market';
	                }
	                else
	                {
	                    if (nbrEmployees >= 2500 )                                              // Enterprise
	                    {
	                        mktSeg = 'Enterprise';      
	                    }
	                    else                                                                    // Mid-Market
	                    {
	                        mktSeg = 'Mid-Market';
	                    }
	                }
	                    
	                if (DandBCo != null && DandBCo.PrimaryNAICS != null && DandBCo.PrimaryNAICS != '')                  // get NAICS Code
	                {
	                    NAICSCode = DandBCo.PrimaryNAICS;
	                }
	                else if (domUltCo != null && domUltCo.PrimaryNAICS != null && domUltCo.PrimaryNAICS != '') 
	                {
	                    NAICSCode = domUltCo.PrimaryNAICS;
	                }
	                else if (globalUltCo != null && globalUltCo.PrimaryNAICS != null && globalUltCo.PrimaryNAICS != '') 
	                {
	                    NAICSCode = globalUltCo.PrimaryNAICS;
	                }
	                else if (parentDandBCo != null && parentDandBCo.PrimaryNAICS != null && parentDandBCo.PrimaryNAICS != '') 
	                {
	                    NAICSCode = parentDandBCo.PrimaryNAICS;
	                }
	                else if (parentAcct != null && parentAcct.NAICSCode != null && parentAcct.NAICSCode != '')
	                {
	                    NAICSCode = parentAcct.NAICSCode;
	                }
	                else
	                {
	                    NAICSCode = acct.NAICSCode;
	                }
	                        
	                 
	                 //  Find Territory Assignment match  
	                 
	                                    
	                system.debug ('@@@ postal code: ' + postalCode);
	                system.debug ('@@@ mktg segment: ' + mktSeg);   
	                
	                                         
	                if (acct.Site == 'Branch' && parentAcct != null && parentAcct.Territory__c != null)                      // if parent of branch has territory, use same territory and owner
	                {
	                    acct.Territory__c = parentAcct.Territory__c;
	                    if (parentAcct.OwnerId != null)
	                    {
	                        acct.OwnerId = parentAcct.OwnerId;
	                    }
	                    updates.add(acct);
	                }
	                else
	                {
	                    String searchKey = countryCode;                                                 // create search keys
	                    String searchKeyPost2 = countryCode;
	                    String searchKeyPost5 = countryCode;
	                    String searchKeyNAICS6 = countryCode;
	                    String searchKeyPostNAICS2 = countryCode;
	                    String searchKeyState = countryCode;
	                    String searchKeyIndustry = countryCode;                                         // does not include market segment, employee count does not matter
	                    String searchKeyMktSegIndustry = countryCode;                               
	                    String searchKeyNAICS3 = countryCode;
	                    String searchKeyNAICS4 = countryCode;
	                    
	                    if (mktSeg != null)
	                    {
	                        searchKey = searchKey + '_' + mktSeg;
	                        searchKeyPost2 = searchKeyPost2 + '_' + mktSeg;
	                        searchKeyPost5 = searchKeyPost5 + '_' + mktSeg;
	                        searchKeyNAICS6 = searchKeyNAICS6 + '_' + mktSeg;
	                        searchKeyPostNAICS2 = searchKeyPostNAICS2 + '_' + mktSeg;
	                        searchKeyState = searchKeyState + '_' + mktSeg;
	                        searchKeyNAICS3 = searchKeyNAICS3 + '_' + mktSeg;
	                        searchKeyNAICS4 = searchKeyNAICS4 + '_' + mktSeg;
	                        searchKeyMktSegIndustry = searchKeyMktSegIndustry + '_' + mktSeg;
	                    }
	                    
	                    if (postalCode != null) 
	                    {
	                    	postalCode = postalCode.toUpperCase();
	                        if (postalCode.length() >= 5)
	                        {
	                            searchKeyPost2 = searchkeyPost2 + '_' + postalCode.substring(0,2);
	                            searchKeyPost5 = searchkeyPost5 + '_' + postalCode.substring(0,5);
	                            searchKeyPostNAICS2 = searchkeyPostNAICS2 + '_' + postalCode.substring(0,5);
	                        }
	                        else if (postalCode.length() >= 2)
	                        {
	                            searchKeyPost2 = searchkeyPost2 + '_' + postalCode.substring(0,2);
	                        }
	                    }
	                    
	                    if (NAICSCode != null && NAICSCode.length() >= 2)
	                    {
	                        searchKeyNAICS6 = searchkeyNAICS6 + '_' + NAICSCode;
	                        searchKeyPostNAICS2 = searchkeyPostNAICS2 + '_' + NAICSCode.substring(0,2);
	                        searchKeyNAICS3 = searchkeyNAICS3 + '_' + NAICSCode.substring(0,3);
	                        searchKeyNAICS4 = searchkeyNAICS4 + '_' + NAICSCode.substring(0,4);
	                    }
	                    
	                    if (industryClass != null)
	                    {
	                        if (industryClass.containsIgnoreCase('Government'))
	                        {
	                            industryClass = 'Government';
	                        }
	                        else if (industryClass.containsIgnoreCase('Education'))
	                        {
	                            industryClass = 'Education';
	                        }
	                        else if (industryClass.containsIgnoreCase('Healthcare') || industryClass.containsIgnoreCase('Health care'))
	                        {
	                            industryClass = 'Healthcare';
	                        }
	                        
	                        if (industryClass == 'Healthcare')
	                        {
	                            searchKeyMktSegIndustry = searchKeyMktSegIndustry + '_' + industryClass;
	                        }
	                        else
	                        {
	                            searchKeyIndustry = searchkeyIndustry + '_' + industryClass;
	                        }
	                    }
	                    
	                    if ((countryCode == 'CN' || countryCode == 'CA') && state != null)                                           // China and Canada - search by state/province
	                    {
	                        searchKeyState = searchKeyState + '_' + state.toUpperCase();
	                    }
	                    
	                    system.debug('@@@ SearchKeyPostNAICS2 : ' + searchKeyPostNAICS2);
	                    system.debug('@@@ SearchKeyNAICS6 : ' + searchKeyNAICS6);
	                    system.debug('@@@ SearchKeyPost5 : ' + searchKeyPost5);
	                    system.debug('@@@ SearchKeyPost2 : ' + searchKeyPost2);
	                    system.debug('@@@ SearchKeyState : ' + searchKeyState);
	                    system.debug('@@@ SearchKey : ' + searchKey);
	                    
	                    //TerritoryAssignmentRules__c terrAssign = terrAssignMap.get(searchKeyPostNAICS2);        // search for postal code/Naics prefix used for NY City
	                    terrAssign = terrAssignMap.get(searchKeyPostNAICS2);        							// search for postal code/Naics pr
	                    
	                    if (terrAssign == null && countryCode == 'US' && postalCode != null  && postalCode.length() >=5 )                                 
	                    {
	                        for (TerritoryAssignmentRules__c ta : terrAssignPostalRanges)                       // search for postal code range match - Used for US
	                        {
	                            if (ta.Country_Code__c.substring(0,2) == countryCode && ta.Market_Segment__c == mktSeg && 
	                                    postalCode.substring(0,5) >= ta.Postal_Start__c && postalCode.substring(0,5) <= ta.Postal_End__c )
	                            {
	                                terrAssign = ta;
	                                break;
	                            }
	                        }
	                    }
	                    
	                    if (terrAssign == null)                                                                 // search for Industry - used for UK Government and Education
	                    {
	                        terrAssign = terrAssignMap.get(searchKeyIndustry);
	                    }
	                    
	                    if (terrAssign == null)                                                                 // search for Mkt Seg and Industry - used for UK Healthcare
	                    {
	                        terrAssign = terrAssignMap.get(searchKeyMktSegIndustry);
	                    }
	                    
	                    if (terrAssign == null)                                                                 // search for full Naics code - used for UK Enterprise
	                    {
	                        terrAssign = terrAssignMap.get(searchKeyNAICS6);
	                    }
	                    
	                    if (terrAssign == null)                                                                 // search for first 4 digits of Naics code - used for UK Mid-Market
	                    {
	                        terrAssign = terrAssignMap.get(searchKeyNAICS4);
	                    }
	                    
	                    if (terrAssign == null)                                                                 // search for first 3 digits of Naics code - used for UK Mid-Market
	                    {
	                        terrAssign = terrAssignMap.get(searchKeyNAICS3);
	                    }
	                    
	                    if (terrAssign == null)                                                                 // search for 2-digits of postal code
	                    {
	                        terrAssign = terrAssignMap.get(searchKeyPost2);
	                    }
	                    
	                    if (terrAssign == null)                                                                 // search for state/province
	                    {
	                        terrAssign = terrAssignMap.get(searchKeyState);
	                    }
	                    
	                    if (terrAssign == null)                                                                 // search for country/mkt segment
	                    {
	                        terrAssign = terrAssignMap.get(searchKey);
	                    }
	                    
	                    system.debug('@@@ Terr Assign found: ' + terrAssign);
	                    
	                    if (terrAssign != null)                                                     			// match found
	                    {
							if (terrAssign.Territory__c == 'EMEA : ENT : NORTH EUROPE : UK : PUBLIC' && (industryClass == null ||           // exceptions for UK Public - only Governmnent or Healthcare                                                     
	                           (!industryClass.containsIgnoreCase('Healthcare')   &&  !industryClass.containsIgnoreCase('Health care')  &&  
	                           	!industryClass.containsIgnoreCase('Government'))))            
	                        {
	                            continue;
	                        }
	                        else
	                        {
	                            acct.Territory__c = terrAssign.Territory__c.toUpperCase();
	                            if (terrAssign.Territory_Manager_Id__c != null)
	                            {
	                                acct.OwnerId = terrAssign.Territory_Manager_Id__c;
	                            }
	                            if (countryCode == 'AE' && city != null && city.equalsIgnoreCase('Abu Dhabi'))             // exception for Abu Dhabi
	                            {
	                                acct.Territory__c = 'EMEA : ENT : MIDDLE EAST : 2';
	                            }
	                            updates.add(acct);
	                        }
	                    }
	                }
	            }
	        }
    	}
        return updates;
    }
    */
}