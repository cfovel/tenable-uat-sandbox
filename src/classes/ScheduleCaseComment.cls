public class ScheduleCaseComment implements Schedulable {
	public List<EmailMessage> msgs;
	
	public ScheduleCaseComment (List<EmailMessage> msgs) {
		this.msgs = msgs;
	}
	
	public void execute(SchedulableContext context) {
		CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                FROM CronTrigger WHERE Id = :context.getTriggerId()];
		
		CaseComment[] commentstoAdd = new CaseComment[0];
		List <Id> msgIds = new List <Id>();
		EmailMessage[] msgsToRetry = new EmailMessage[0];
		List <Id> msgIdList = new List<Id>();
		String newcomment = '';
    	for (EmailMessage m : msgs) {
    		String[] newcomments = m.TextBody.split('Original Message');

			if ((newcomments != null) &&
				(newcomments[0] != null)) {
				newcomment = newcomments[0].replace('---------------', '');						
			}
			commentstoAdd.add(new CaseComment(ParentId = m.ParentId, CommentBody = newcomment, isPublished=TRUE));
			msgIdList.add(m.Id);
    	}
    	if (commentstoAdd.size() > 0) {
			try {
				Database.DMLOptions dmo = new Database.DMLOptions();
				dmo.allowFieldTruncation = true;
				dmo.optAllOrNone = false;
				Database.SaveResult[] srList = Database.insert (commentstoAdd, dmo);
			}
			catch (exception e) {
				system.debug('Error inserting case comments, commentstoAdd='+commentstoAdd);				
			}
		}
	}

}