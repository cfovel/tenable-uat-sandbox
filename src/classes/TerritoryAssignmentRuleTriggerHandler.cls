public with sharing class TerritoryAssignmentRuleTriggerHandler
{
    public class TerritoryAssignmentRuleBeforeUpdateHandler implements Triggers.Handler
    {
        public void handle() 
        {
            UpdateTerritoryData(Trigger.new);
        }
    } 
    
    public class TerritoryAssignmentRuleBeforeDeleteHandler implements Triggers.Handler
    {
        public void handle() 
        {
            UpdateTerritoryData(Trigger.new);
        }
    } 
    
    public static void UpdateTerritoryData(List<TerritoryAssignmentRule__c> newRules)
    {
        List<TerritoryAssignment__c> territoryAssignmentsToDelete = new List<TerritoryAssignment__c>();
    
        if (Trigger.isBefore) 
        {
            
            if (Trigger.isUpdate) 
            {
                TerritoryAssignmentRule__c oldRule;
                
                for (TerritoryAssignmentRule__c territoryAssignmentRule : newRules) 
                {
                    oldRule = (TerritoryAssignmentRule__c)Trigger.oldMap.get(territoryAssignmentRule.Id);
					if (territoryAssignmentRule.Country__c != oldRule.Country__c || territoryAssignmentRule.State__c != oldRule.State__c || territoryAssignmentRule.PostalCode__c != oldRule.PostalCode__c || territoryAssignmentRule.NAICSCode__C != oldRule.NAICSCode__c || territoryAssignmentRule.Industry__c != oldRUle.Industry__c || territoryAssignmentRule.EmployeeMinimum__c != oldRule.EmployeeMinimum__c || territoryAssignmentRule.EmployeeMaximum__c != oldRule.EmployeeMaximum__c || territoryAssignmentRule.Channel__c != oldRule.Channel__c)
                        territoryAssignmentRule.addError('You may not update active rules.');
                    
                }
                
            } 
            else if (Trigger.isDelete) 
            {
                
                for (TerritoryAssignment__c territoryAssignment : [SELECT Id FROM TerritoryAssignment__c WHERE TerritoryAssignmentRule__c IN :Trigger.oldMap.keySet() AND Preview__c = true]) 
                {
                    territoryAssignmentsToDelete.add(territoryAssignment);
                }
                
            }
            
        }
        
        if (!territoryAssignmentsToDelete.isEmpty())
            delete territoryAssignmentsToDelete;
        
    }
}