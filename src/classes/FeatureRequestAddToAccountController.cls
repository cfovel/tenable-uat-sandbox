public class FeatureRequestAddToAccountController
{
	public Feature_Request__c request { get; private set; }
    private final ApexPages.standardController stdController;                   // Set-up Controller Class
    private Id requestId;
    private Id acctId;
    private Account acct;
    
    public FeatureRequestAddToAccountController(ApexPages.StandardController stdController)
    {
        requestId = ApexPages.currentPage().getParameters().get('id');
        acctId = ApexPages.currentPage().getParameters().get('acctId');
        this.stdController = stdController;    
        request = (Feature_Request__c)stdController.getRecord();                     // First get record from Page Call
        if (acctId != null)
        {
        	acct = [SELECT Name FROM Account WHERE Id = :acctId limit 1];
        }
        System.debug('@@@ requestId: ' + requestId +', acctId: ' + acctId + ', Account: ' + acct);
        
   
    }
    
    public pagereference CreateAccountFeatureRequest()
    {
        Map<string,schema.Sobjecttype> gd = schema.getGlobalDescribe();
        String strKeyPrefix = gd.get('Account_Feature_Request__c').getDescribe().getKeyPrefix();
    
        PageReference newPage = new PageReference('/apex/AccountFeatureRequestCreate?frId=' + requestId);
    
        newPage.setRedirect(true);
        return newPage;
    }
    
}