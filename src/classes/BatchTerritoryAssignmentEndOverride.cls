global class BatchTerritoryAssignmentEndOverride implements Database.Batchable<sObject> {
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([SELECT Id FROM Account WHERE isExcludedFromRealign != true AND Territory__c <> null]);
    }
    
    global void execute(Database.BatchableContext bc, List<Account> accounts) {

        TerritoryAssignment.endOverrideAssignmentOldTerr(accounts);       
    }
    
    global void finish(Database.BatchableContext bc) {}
    
}