global class baUpdateGroupMembers implements Database.Batchable<SObject> {
	// To run, login as Automation User, open Execute Anonymous window, and execute:
	//		database.executeBatch(new baUpdateGroupMembers(),200);

    global Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator([Select Id, AccountId, ContactId, Theatre__c, Contact.Account.Edge_Authorization_Status__c 
        			FROM User WHERE UserType LIKE '%Partner%' ]);
    }

    global void execute(Database.BatchableContext context, List<User> scope) {
    	List <GroupMember> groupMembersToAdd = new List <GroupMember>();
        String PlatinumPartnerGroup, GoldPartnerGroup, SilverPartnerGroup, BronzePartnerGroup, DistributorPartnerGroup;

        /*****************************************************************************************
        *   Record Pre-Processing
        ******************************************************************************************/        
        List<String> partnerGroupNames = new List<String> {'All Platinum Partners', 'All Gold Partners', 'All Silver Partners', 'All Bronze Partners', 'All Distributor Partners'};
        List<Group> partnerGroups = new List<Group>([SELECT Id, Name FROM Group WHERE Name in :partnerGroupNames]);
        for (Group pg : partnerGroups) {
        	if (pg.Name == 'All Platinum Partners') {PlatinumPartnerGroup = pg.Id;}
        	if (pg.Name == 'All Gold Partners') {GoldPartnerGroup = pg.Id;}
        	if (pg.Name == 'All Silver Partners') {SilverPartnerGroup = pg.Id;}
        	if (pg.Name == 'All Bronze Partners') {BronzePartnerGroup = pg.Id;}
        	if (pg.Name == 'All Distributor Partners') {DistributorPartnerGroup = pg.Id;}
        }
        
        /*****************************************************************************************
        *   Record Processing
        ******************************************************************************************/
        List<GroupMember> groupMembers = new List<GroupMember>([SELECT Id, GroupId,										// Get existing groupMember records for these users
				UserOrGroupId FROM GroupMember WHERE GroupId IN :partnerGroups AND 
							UserOrGroupId IN :scope]);
							
		if (!groupMembers.isEmpty()) {
			try {
				delete groupMembers;																					// Delete these users from old groups
			}
			catch (DMLException e) {}						
		}	
        
        for (User u : scope) {  
			if (u.Contact.Account.Edge_Authorization_Status__c  != null) {            									// Check to see if we're adding a partner user record                   	                                         										
            	if (u.Contact.Account.Edge_Authorization_Status__c == 'Platinum') {										// Add user to group based on Edge Authorization Status
            		groupMembersToAdd.add(new GroupMember(GroupId = PlatinumPartnerGroup, UserOrGroupId = u.Id));
            	}
            	else if (u.Contact.Account.Edge_Authorization_Status__c == 'Gold') {
            		groupMembersToAdd.add(new GroupMember(GroupId = GoldPartnerGroup, UserOrGroupId = u.Id));
            	}
            	else if (u.Contact.Account.Edge_Authorization_Status__c == 'Silver') {
            		groupMembersToAdd.add(new GroupMember(GroupId = SilverPartnerGroup, UserOrGroupId = u.Id));
            	}
            	else if (u.Contact.Account.Edge_Authorization_Status__c == 'Bronze') {
            		groupMembersToAdd.add(new GroupMember(GroupId = BronzePartnerGroup, UserOrGroupId = u.Id));
            	}
            	else if (u.Contact.Account.Edge_Authorization_Status__c == 'Distributor') {
            		groupMembersToAdd.add(new GroupMember(GroupId = DistributorPartnerGroup, UserOrGroupId = u.Id));
            	}
            }
        }

        /*****************************************************************************************
        *   Record Post-Processing
        ******************************************************************************************/
        
        if (groupMembersToAdd.size() > 0) {
        	try {
				insert groupMembersToAdd;																				// Delete these users from old groups
			}
			catch (DMLException e) {}		
        }
    }

    global void finish(Database.BatchableContext context) {
    }
}