/**
 * Created by lucassoderstrum on 6/5/18.
 */

public with sharing class Tenable_ProfileProgressController {

    @auraEnabled
    public static Tenable_ProfileProgressWrapper getProgress(){
        system.debug('test user');
        User currentUser = [SELECT Completed_Groups_Slide__c, Completed_Notification_Slide__c, Completed_Profile_Slide__c,
                Completed_Tours_Slide__c, Completed_Welcome_Slide__c, Completed_Topics_Slide__c, Onboarding_Complete__c
                FROM User
                WHERE Id = :userinfo.getuserId() limit 1];

        Tenable_ProfileProgressWrapper wrapper = new Tenable_ProfileProgressWrapper(
                currentUser.Completed_Welcome_Slide__c, currentUser.Completed_Profile_Slide__c, currentUser.Completed_Notification_Slide__c,
        currentUser.Completed_Topics_Slide__c, currentUser.Completed_Groups_Slide__c, currentUser.Completed_Tours_Slide__c
        );

        system.debug('wrapper: ' + wrapper);

        return wrapper;
    }

    @auraEnabled
    public static User getUserRecord(){
        system.debug('test user');
        User currentUser = [SELECT Completed_Groups_Slide__c, Completed_Notification_Slide__c, Completed_Profile_Slide__c, Completed_Topics_Slide__c,
                Completed_Tours_Slide__c, Completed_Welcome_Slide__c, FirstName, LastName, Onboarding_Complete__c
        FROM User
        WHERE Id = :userinfo.getuserId() limit 1];

        return currentUser;
    }

}