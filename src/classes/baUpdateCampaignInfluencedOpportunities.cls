global class baUpdateCampaignInfluencedOpportunities {		// implements Database.Batchable<SObject> {
	// To run, login as Automation User, open Execute Anonymous window, and execute:
	//		database.executeBatch(new baUpdateCampaignInfluencedOpportunities(),200);

	//global List <Report> reportList = [SELECT Id,DeveloperName FROM Report where 
	//		    DeveloperName = 'Active_Campaigns_with_Influenced_Opps'];
	//global String reportId = (String)reportList.get(0).get('Id');
	//global String reportId  = '00OP0000000f0aB';  // this is the reportId in the Test sandbox
	
/*	
	global String reportId  = '00O60000004jzIG';	// this is the report Id in production
    global Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator([SELECT Id, Num_Influenced_Opportunities__c, Total_Value_Influenced_Opportunities__c, 
        			NumberOfOpportunities, AmountAllOpportunities FROM Campaign WHERE isActive = TRUE]);
    }

    global void execute(Database.BatchableContext context, List<Campaign> scope) {
		
		Decimal numInfluencedOpps;
		Decimal valInfluencedOpps;
		Map<String, Reports.ReportFactWithSummaries> rptMap = new Map<String, Reports.ReportFactWithSummaries>(); 					// create a map to hold the report data by campaign Id
		Reports.ReportResults results = Reports.ReportManager.runReport(reportId, false);
		String factMapKey;
		Reports.Dimension dim = results.getGroupingsDown();
		List<Reports.GroupingValue> groupings = dim.getGroupings();
		for(Reports.GroupingValue grouping : groupings) {
			factMapKey = grouping.getKey() + '!T';
    		rptMap.put(String.valueOf(grouping.getValue()), (Reports.ReportFactWithSummaries)results.getFactMap().get(factMapKey)); // populate the map
    	}
		
        Campaign[] updates = new Campaign[] {};
        for (Campaign c : scope) {
        	numInfluencedOpps = 0;
        	valInfluencedOpps = 0;
			Reports.ReportFactWithSummaries factSum = rptMap.get(c.Id);
			if (factSum != null) {
				numInfluencedOpps = (Decimal)factSum.getAggregates()[1].getValue() - c.NumberOfOpportunities;  
				valInfluencedOpps = (Decimal)factSum.getAggregates()[0].getValue() - c.AmountAllOpportunities;    
			}
			if (c.Num_Influenced_Opportunities__c != numInfluencedOpps  ||
					c.Total_Value_Influenced_Opportunities__c != valInfluencedOpps ) {
				c.Num_Influenced_Opportunities__c = numInfluencedOpps;
				c.Total_Value_Influenced_Opportunities__c = valInfluencedOpps;
	            updates.add(c);
			}   
        }
        update updates;
    }

    global void finish(Database.BatchableContext context) {
    }
*/    
}