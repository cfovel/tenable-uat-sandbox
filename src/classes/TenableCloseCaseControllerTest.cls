/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TenableCloseCaseControllerTest {

    static testMethod void testGetCaseStatus() {
      // set the callout response mock
      Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
      // This flag is used by most of the code
      utils.isTest = true;
      //Account
	  Account a = new Account(name='Tenable Unit Test');
	  insert a;
      // Contact
      Contact c = TestHelper.createContact(a.Id, true);
	  // Community user
	  User u = TestHelper.getCommunityUser(c.Id, TestHelper.PortalType.CspLitePortal, true);
	  
      Case customerCase = new Case();
      customerCase.Subject = 'Tenable Case Subject';
      customerCase.Description = 'Tenable unit test case description';
      customerCase.ContactId = c.Id;
      customerCase.AccountId = a.Id;
      insert customerCase;
      
      Test.startTest();
      TenableCloseCaseController ctrl = new TenableCloseCaseController();
      TenableCloseCaseController.getCaseStatus(null);
      TenableCloseCaseController.CaseStatus CustomerCaseStatus = TenableCloseCaseController.getCaseStatus(customerCase.Id);
      String status = CustomerCaseStatus.getStatus();
      Boolean isReopenEligible = CustomerCaseStatus.getReopenEligible();
      Test.stopTest();
    }
    
    static testMethod void testReopenCaseWithComments(){
      // set the callout response mock
      Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
      // This flag is used by most of the code
      utils.isTest = true;
      //Account
	  Account a = new Account(name='Tenable Unit Test');
	  insert a;
      // Contact
      Contact c = TestHelper.createContact(a.Id, true);
	  
      Case customerCase = new Case();
      customerCase.Subject = 'Tenable Case Subject';
      customerCase.Description = 'Tenable unit test case description';
      customerCase.ContactId = c.Id;
      customerCase.AccountId = a.Id;
      insert customerCase;
      customerCase.Status = 'Closed';
      customerCase.Resolution_Detail__c = 'Problem was solved for the customer';
      update customerCase;
      
      Test.startTest();
      case reOpenedCase = TenableCloseCaseController.reopenCaseWithComments(customerCase.Id, 'Opening case back with comments');
      Test.stopTest();
      
    }
    
    static testMethod void testCloseCaseWithComments(){
      // This flag is used by most of the code
      utils.isTest = true;
      User AdminUser = [SELECT Username FROM User WHERE Id = :UserInfo.getUserId()];
      Account a;
      Contact c;
      User u;
      Case customerCase;
      system.runAs(AdminUser){
        //Account
	    a = new Account(name='Tenable Unit Test');
	    insert a;
        // Contact
        c = TestHelper.createContact(a.Id, true);
        customerCase = new Case();
        customerCase.Subject = 'Tenable Case Subject';
        customerCase.Description = 'Tenable unit test case description';
        customerCase.ContactId = c.Id;
        customerCase.AccountId = a.Id;
        insert customerCase;
      }
	  

      Test.startTest();
      case closedCase = TenableCloseCaseController.closeCaseWithComments(customerCase.Id, 'Opening case back with comments', 'Closed by Customer');
      Test.stopTest();
    }
}