global class baUpdateContactEmailAddress implements Database.Batchable<SObject> {
    // To run, login as Automation User, open Execute Anonymous window, and execut:
    //      database.executeBatch(new baUpdateContactEmailAddress(),200);

    global Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator([select Id, Name, Email from Contact WHERE Email Like '%@%.%' and (NOT Email Like '%@example.com')]);
    }

    global void execute(Database.BatchableContext context, List<Contact> scope) {
    
        Utils.isTest = true;
        contactTriggerHandler.enforceCountry = false;
        
        for (Contact c : scope) 
        {   
            c.email = Utils.scrambleEmailAddress(c.email);
        }
        
        update scope;
    }

    global void finish(Database.BatchableContext context) {
    }
}