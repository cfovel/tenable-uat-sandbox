global class baMissingCaseComments implements Database.Batchable<SObject> {
	// To run, login as Automation User, open Execute Anonymous window, and execute:
	//		database.executeBatch(new baMissingCaseComments('01/01/2016', 'support@tenable.com'),200);

    global String query;
    global String fromDate;																// MM/DD/YYYY
    global String fromAddr;																// prod - support@tenable.com, sandbox - <support@tenable.com>
    global String dt2;
    global DateTime dt;
    
    
    global baMissingCaseComments(String fromDate, String fromAddr)
    {
        dt = DateTime.parse(fromDate + ' 12:00 AM');
        dt2 = String.valueOf(dt).replace(' ','T') + '.000Z';
        query = 'Select id, createdDate, ParentId, Subject from EmailMessage WHERE CreatedDate >= ' + dt2 + 
        			' AND isDeleted = false AND FromAddress = \'' + fromAddr + '\' AND ParentId  <> null';		
        system.debug('@@@ query: ' + query);						
    }
    
    global Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext context, List<EmailMessage> scope) {
    	List<Id> parentIds = new List<Id>();
    	Logger logger = new Logger();
		for (EmailMessage msg : scope) {
			if(String.valueOf(msg.parentId).startsWith('500')) {
				parentIds.add( msg.parentId);
			}
		}	
		List<CaseComment> ccs = [Select id, CreatedDate, ParentId from CaseComment WHERE isDeleted = false AND
								CreatedDate >= :dt AND
								ParentId IN :parentIds];
																			
		List<EmailMessage> msgsWithoutComments = new List<EmailMessage>();		
		for (EmailMessage msg : scope) {
			
		    //System.debug('@@@ Msg: ' + msg);
			Boolean commentFound = false;
			                  
		    for (CaseComment cc : ccs) {
		        if(cc.ParentId == msg.ParentId && cc.CreatedDate >= msg.CreatedDate.addMinutes(-1) && cc.CreatedDate <= msg.CreatedDate.addMinutes(1)) {
		            commentFound = true;
		            break;
		        }
		    }
		    if (!commentFound) {
		        msgsWithoutComments.add(msg);
		        System.debug('@@@ Msg not found: ' + msg);
		        try {
			    logger.info(String.valueOf(msg));
				} finally {
				    logger.flush('Log-Missing Case Comments-' + System.today());
				}
		    }	
		    
						
		}
        System.debug('@@@ Missing comments count: ' + msgsWithoutComments.size());
        try {
	    logger.info('Missing comments count: ' + msgsWithoutComments.size());
		} finally {
		    logger.flush('Log-Missing Case Comments-' + System.today());
		}
        
        
		
		
        /*																					// if case comment is inserted, createdDate will be different from emailMessage
        if (msgsWithoutComments.size() > 0) {
        	CaseComment[] commentstoAdd = new CaseComment[0];
			String newcomment = '';
			for (EmailMessage m : msgsWithoutComments) {
    			String[] newcomments = m.TextBody.split('Original Message');

				if ((newcomments != null) &&
					(newcomments[0] != null)) {
					newcomment = newcomments[0].replace('---------------', '');						
				}
		
				commentstoAdd.add(new CaseComment(ParentId = m.ParentId, CommentBody = newcomment, isPublished=TRUE));
	    	}	
	    	if (commentstoAdd.size() > 0) {
				try {
					system.debug('@@@ case comments to add:' + commentsToAdd);
					Database.DMLOptions dmo = new Database.DMLOptions();
					dmo.allowFieldTruncation = true;
					dmo.optAllOrNone = false;
					Database.SaveResult[] srList = Database.insert (commentstoAdd, dmo);
				}
				catch (exception e) {
system.debug('Error inserting case comments, commentstoAdd='+commentstoAdd);				
				}
			}
        }
        */
    }

    global void finish(Database.BatchableContext context) {
    }
}