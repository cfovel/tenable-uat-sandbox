public without sharing class FeaturedTopicsController {
	public static list<FeaturedTopicsWrapper> topWrappersList = new list<FeaturedTopicsWrapper>();
	public static map<Id, TopicAssignment> topicAssignmentMap = new map<Id, TopicAssignment>();
	
    @AuraEnabled
    public static list<FeaturedTopicsWrapper> getQuestionsAndArticles(String jsonTopicIds) {
        topWrappersList = new list<FeaturedTopicsWrapper>();
        topicAssignmentMap = new map<Id, TopicAssignment>();
        map<Id, Integer> knowArtCountMap = new map<Id, Integer>();
        map<Id, Integer> questionCountMap = new map<Id, Integer>();
        
        system.debug('jsonTopicIds value: ' + jsonTopicIds);
        
        // Get the list of Topic Ids.
        list<String> topicIds = (list<String>)System.JSON.deserialize(jsonTopicIds, list<String>.class);
        
        system.debug('topicIds value: ' + topicIds);
        
        for(TopicAssignment ta: [select EntityId, EntityType, TopicId
        							from TopicAssignment 
        							where TopicId in :topicIds
        							order by CreatedDate desc]){
        	String entId = ta.EntityId;
        	
        	if(entId.startsWithIgnoreCase('ka')){
        		// This is a Knowledge Article. Only store the two most recent Knowledge Articles per TopicId.
        		topicAssignmentMap.put(ta.EntityId, ta);
        	}
        	
        	if(ta.EntityType == 'FeedItem'){
        		// This is a QuestionPost (FeedItem).
        		if(questionCountMap.get(ta.TopicId) != null){
        			if(questionCountMap.get(ta.TopicId) == 1){
        				questionCountMap.put(ta.TopicId, 2);
        				
        				topicAssignmentMap.put(ta.EntityId, ta);
        			} else if(questionCountMap.get(ta.TopicId) == 2){
        				questionCountMap.put(ta.TopicId, 3);
        				
        				topicAssignmentMap.put(ta.EntityId, ta);
        			} else if(questionCountMap.get(ta.TopicId) == 3){
        				questionCountMap.put(ta.TopicId, 4);
        				
        				topicAssignmentMap.put(ta.EntityId, ta);
        			} else if(questionCountMap.get(ta.TopicId) == 4){
        				questionCountMap.put(ta.TopicId, 5);

        				topicAssignmentMap.put(ta.EntityId, ta);
        			} else if(questionCountMap.get(ta.TopicId) == 5){
        				questionCountMap.put(ta.TopicId, 6);

        				topicAssignmentMap.put(ta.EntityId, ta);
        			}
        		} else{
        			// This is the first QuestionPost (FeedItem) added for this TopicId.
        			questionCountMap.put(ta.TopicId, 1);
        			
        			topicAssignmentMap.put(ta.EntityId, ta);
        		}
        	}
        }
        
        system.debug('topicAssignmentMap value: ' + topicAssignmentMap);
        
        // Find the QuestionPosts.
        for(FeedItem fi: [select CreatedDate, Type, Title, Body 
        						from FeedItem 
        						where Type = 'QuestionPost' and Id in :topicAssignmentMap.keySet()
        						order by CreatedDate desc]){
        	// This is a Question.
        	FeaturedTopicsWrapper tempTopWrapper = new FeaturedTopicsWrapper();
        	
        	tempTopWrapper.topicId = topicAssignmentMap.get(fi.Id).TopicId;
			tempTopWrapper.tTitle = fi.Title;
			tempTopWrapper.tBody = fi.Body;
			tempTopWrapper.tType = 'Question';
			tempTopWrapper.tRecordId = fi.Id;
			tempTopWrapper.tRecordURL = Label.Community_URL + 'feed/' + fi.Id;
			tempTopWrapper.tDate = Date.newinstance(fi.CreatedDate.year(), fi.CreatedDate.month(), fi.CreatedDate.day());
			
			topWrappersList.add(tempTopWrapper);
        }
        
        system.debug('topWrappersList first value: ' + topWrappersList);
        
        // Find the Knowledge Articles.
        for(KnowledgeArticleVersion ka: [select CreatedDate, KnowledgeArticleId, Summary, Title
        									from KnowledgeArticleVersion 
        									where Id in :topicAssignmentMap.keySet() and IsLatestVersion = true and PublishStatus = 'Online'
        									order by CreatedDate desc]){
        	// This is a KnowledgeArticleVersion. Only store the three most recent Knowledge Articles per TopicId.
        	String topicId = topicAssignmentMap.get(ka.Id).TopicId;
        	
        	system.debug('ka value: ' + ka);
        	
        	if(knowArtCountMap.get(topicId) != null){
        		if(knowArtCountMap.get(topicId) == 1){
        			knowArtCountMap.put(topicId, 2);
        			
        			addTopicWrapper(ka);
        		}
        		else if(knowArtCountMap.get(topicId) == 2){
        			// Update the value to 2 so this is the last Knowledge Article added for this TopicId.
        			knowArtCountMap.put(topicId, 3);
					
        			addTopicWrapper(ka);
        		}
        	} else{
        		// This is the first Knowledge Article added for this TopicId.
        		knowArtCountMap.put(topicId, 1);
        		
        		addTopicWrapper(ka);
        	}
        }
        
        system.debug('topWrappersList second value: ' + topWrappersList);
        
        return topWrappersList;
    }
    
    @AuraEnabled
    public static list<FeaturedTopicsWrapper> getQuestionsAndArticlesByTab(String jsonTopicIds) {
        topWrappersList = new list<FeaturedTopicsWrapper>();
        topicAssignmentMap = new map<Id, TopicAssignment>();
        map<Id, Integer> knowArtCountMap = new map<Id, Integer>();
        map<Id, Integer> questionCountMap = new map<Id, Integer>();
        
        // Get the list of Topic Ids.
        list<String> topicIds = (list<String>)System.JSON.deserialize(jsonTopicIds, list<String>.class);
        
        system.debug('topicIds value: ' + topicIds);
        
        for(TopicAssignment ta: [select EntityId, EntityType, TopicId 
        							from TopicAssignment 
        							where TopicId in :topicIds
        							order by CreatedDate desc]){
        	String entId = ta.EntityId;
        	
        	if(entId.startsWithIgnoreCase('ka')){
        		// This is a Knowledge Article. Only store the two most recent Knowledge Articles per TopicId.
        		topicAssignmentMap.put(ta.EntityId, ta);
        	}
        	
        	if(ta.EntityType == 'FeedItem'){
        		// This is a QuestionPost (FeedItem).
        		if(questionCountMap.get(ta.TopicId) != null){
        			if(questionCountMap.get(ta.TopicId) == 1){
        				questionCountMap.put(ta.TopicId, 2);
        				
        				topicAssignmentMap.put(ta.EntityId, ta);
        			} else if(questionCountMap.get(ta.TopicId) == 2){
        				questionCountMap.put(ta.TopicId, 3);
        				
        				topicAssignmentMap.put(ta.EntityId, ta);
        			} else if(questionCountMap.get(ta.TopicId) == 3){
        				questionCountMap.put(ta.TopicId, 4);
        				
        				topicAssignmentMap.put(ta.EntityId, ta);
        			} else if(questionCountMap.get(ta.TopicId) == 4){
        				questionCountMap.put(ta.TopicId, 5);
        				
        				topicAssignmentMap.put(ta.EntityId, ta);
        			} else if(questionCountMap.get(ta.TopicId) == 5){
        				questionCountMap.put(ta.TopicId, 6);
        				
        				// This is the last QuestionPost (FeedItem) added for this TopicId.
        				topicAssignmentMap.put(ta.EntityId, ta);
        			}
        		} else{
        			// This is the first QuestionPost (FeedItem) added for this TopicId.
        			questionCountMap.put(ta.TopicId, 1);
        			
        			topicAssignmentMap.put(ta.EntityId, ta);
        		}
        	}
        }
        
        system.debug('knowArtCountMap value: ' + knowArtCountMap);
        system.debug('topicAssignmentMap value: ' + topicAssignmentMap);
        
        // Find the QuestionPosts.
        for(FeedItem fi: [select CreatedDate, Type, Title, Body 
        						from FeedItem 
        						where Type = 'QuestionPost' and Id in :topicAssignmentMap.keySet()
        						order by CreatedDate desc]){
        	// This is a Question.
        	FeaturedTopicsWrapper tempTopWrapper = new FeaturedTopicsWrapper();
        	
        	tempTopWrapper.topicId = topicAssignmentMap.get(fi.Id).TopicId;
			tempTopWrapper.tTitle = fi.Title;
			tempTopWrapper.tBody = fi.Body;
			tempTopWrapper.tType = 'Question';
			tempTopWrapper.tRecordId = fi.Id;
			tempTopWrapper.tRecordURL = Label.Community_URL + 'feed/' + fi.Id;
			tempTopWrapper.tDate = Date.newinstance(fi.CreatedDate.year(), fi.CreatedDate.month(), fi.CreatedDate.day());
			
			topWrappersList.add(tempTopWrapper);
        }
        
        system.debug('topWrappersList first value: ' + topWrappersList);
        
        // Find the Knowledge Articles.
        for(KnowledgeArticleVersion ka: [select CreatedDate, KnowledgeArticleId, Summary, Title
        									from KnowledgeArticleVersion 
        									where Id in :topicAssignmentMap.keySet() and IsLatestVersion = true and PublishStatus = 'Online'
        									order by CreatedDate desc]){
        	// This is a KnowledgeArticleVersion. Only store the three most recent Knowledge Articles per TopicId.
        	String topicId = topicAssignmentMap.get(ka.Id).TopicId;
        	
        	system.debug('ka value: ' + ka);
        	
        	if(knowArtCountMap.get(topicId) != null){
        		if(knowArtCountMap.get(topicId) == 1){
        			knowArtCountMap.put(topicId, 2);
        			
        			addTopicWrapper(ka);
        		} else if(knowArtCountMap.get(topicId) == 2){
        			knowArtCountMap.put(topicId, 3);
					
        			addTopicWrapper(ka);
        		} else if(knowArtCountMap.get(topicId) == 3){
        			knowArtCountMap.put(topicId, 4);
					
        			addTopicWrapper(ka);
        		} else if(knowArtCountMap.get(topicId) == 4){
        			knowArtCountMap.put(topicId, 5);
					
        			addTopicWrapper(ka);
        		} else if(knowArtCountMap.get(topicId) == 5){
        			// Update the value to 6 so this is the last Knowledge Article added for this TopicId.
        			knowArtCountMap.put(topicId, 6);
					
        			addTopicWrapper(ka);
        		}
        	} else{
        		// This is the first Knowledge Article added for this TopicId.
        		knowArtCountMap.put(topicId, 1);
        		
        		addTopicWrapper(ka);
        	}
        }
        
        system.debug('topWrappersList second value: ' + topWrappersList);
        
        return topWrappersList;
    }
    
    public static void addTopicWrapper(KnowledgeArticleVersion kav){
    	FeaturedTopicsWrapper tempTopWrapper = new FeaturedTopicsWrapper();
    	
    	tempTopWrapper.topicId = topicAssignmentMap.get(kav.Id).TopicId;
		tempTopWrapper.tTitle = kav.Title;
		tempTopWrapper.tBody = kav.Summary;
		tempTopWrapper.tType = 'Article';
		tempTopWrapper.tRecordId = kav.Id;
		tempTopWrapper.tRecordURL = Label.Community_URL + 'article/' + kav.Id;
		tempTopWrapper.tDate = Date.newinstance(kav.CreatedDate.year(), kav.CreatedDate.month(), kav.CreatedDate.day());
		
		topWrappersList.add(tempTopWrapper);
    }
}