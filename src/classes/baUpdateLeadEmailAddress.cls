global class baUpdateLeadEmailAddress implements Database.Batchable<SObject> {
    // To run, login as Automation User, open Execute Anonymous window, and execut:
    //      database.executeBatch(new baUpdateLeadEmailAddress(),200);

    global Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator([select Id, Name, Email,Alt_Email__c, Channel_Managers_Email__c, CM_Managers_Email__c, Executive_Sponsor_Email_Address__c, Primary_Finance_AP_Email_Address__c, Primary_Marketing_Email_Address__c, Primary_Sales_Email_Address__c, Primary_Technical_Email_Address__c, SDRs_Email__c, Shipped_From_Email_Address__c, TM_Managers_Email__c  
                 FROM Lead WHERE isConverted = false and ((Email Like '%@%.%' and (NOT Email Like '%@example.com')) OR (Alt_Email__c Like '%@%.%' and (NOT Alt_Email__c Like '%@example.com'))
                 OR (Channel_Managers_Email__c Like '%@%.%' and (NOT Channel_Managers_Email__c Like '%@example.com'))  OR (CM_Managers_Email__c Like '%@%.%' and (NOT CM_Managers_Email__c Like '%@example.com'))  OR (Executive_Sponsor_Email_Address__c Like '%@%.%' and (NOT Executive_Sponsor_Email_Address__c Like '%@example.com'))
                 OR (Primary_Finance_AP_Email_Address__c Like '%@%.%' and (NOT Primary_Finance_AP_Email_Address__c Like '%@example.com'))  OR (Primary_Marketing_Email_Address__c Like '%@%.%' and (NOT Primary_Marketing_Email_Address__c Like '%@example.com'))  OR (Primary_Sales_Email_Address__c Like '%@%.%' and (NOT Primary_Sales_Email_Address__c Like '%@example.com'))
                 OR (Primary_Technical_Email_Address__c Like '%@%.%' and (NOT Primary_Technical_Email_Address__c Like '%@example.com'))  OR (SDRs_Email__c Like '%@%.%' and (NOT SDRs_Email__c Like '%@example.com'))  OR (Shipped_From_Email_Address__c Like '%@%.%' and (NOT Shipped_From_Email_Address__c Like '%@example.com')) OR (TM_Managers_Email__c Like '%@%.%' and (NOT TM_Managers_Email__c Like '%@example.com')))]);
    }

    global void execute(Database.BatchableContext context, List<Lead> scope) {
        
        Utils.isTest = true;
        leadTriggerHandler.enforceCountry = false;
        
        for (Lead ld : scope) 
        {
            if (ld.email != null && !ld.email.endsWith('@example.com'))
            {
                system.debug('@@@ Name: ' + ld.name + ', email before: ' + ld.email);
                ld.email = Utils.scrambleEmailAddress(ld.email);
                system.debug('@@@ Name: ' + ld.name + ', email after: ' + ld.email);
            }
            
            if (ld.Alt_Email__c != null && !ld.Alt_Email__c.endsWith('@example.com'))
            {
                ld.Alt_Email__c = Utils.scrambleEmailAddress(ld.Alt_Email__c);
            }
            
            if (ld.Channel_Managers_Email__c != null && !ld.Channel_Managers_Email__c.endsWith('@example.com'))
            {
                ld.Channel_Managers_Email__c = Utils.scrambleEmailAddress(ld.Channel_Managers_Email__c);
            }
            
            if (ld.CM_Managers_Email__c != null && !ld.CM_Managers_Email__c.endsWith('@example.com'))
            {
                ld.CM_Managers_Email__c = Utils.scrambleEmailAddress(ld.CM_Managers_Email__c);
            }
            
            if (ld.Executive_Sponsor_Email_Address__c != null && !ld.Executive_Sponsor_Email_Address__c.endsWith('@example.com'))
            {
                ld.Executive_Sponsor_Email_Address__c = Utils.scrambleEmailAddress(ld.Executive_Sponsor_Email_Address__c);
            }
            
            if (ld.Primary_Finance_AP_Email_Address__c != null && !ld.Primary_Finance_AP_Email_Address__c.endsWith('@example.com'))
            {
                ld.Primary_Finance_AP_Email_Address__c = Utils.scrambleEmailAddress(ld.Primary_Finance_AP_Email_Address__c);
            }
            
            if (ld.Primary_Marketing_Email_Address__c != null && !ld.Primary_Marketing_Email_Address__c.endsWith('@example.com'))
            {
                ld.Primary_Marketing_Email_Address__c = Utils.scrambleEmailAddress(ld.Primary_Marketing_Email_Address__c);
            }
            
            if (ld.Primary_Sales_Email_Address__c != null && !ld.Primary_Sales_Email_Address__c.endsWith('@example.com'))
            {
                ld.Primary_Sales_Email_Address__c = Utils.scrambleEmailAddress(ld.Primary_Sales_Email_Address__c);
            }
            
            if (ld.Primary_Technical_Email_Address__c != null && !ld.Primary_Technical_Email_Address__c.endsWith('@example.com'))
            {
                ld.Primary_Technical_Email_Address__c = Utils.scrambleEmailAddress(ld.Primary_Technical_Email_Address__c);
            }
            
            if (ld.SDRs_Email__c != null && !ld.SDRs_Email__c.endsWith('@example.com'))
            {
                ld.SDRs_Email__c = Utils.scrambleEmailAddress(ld.SDRs_Email__c);
            }
            
            if (ld.Shipped_From_Email_Address__c != null && !ld.Shipped_From_Email_Address__c.endsWith('@example.com'))
            {
                ld.Shipped_From_Email_Address__c = Utils.scrambleEmailAddress(ld.Shipped_From_Email_Address__c);
            }
            
            if (ld.TM_Managers_Email__c != null && !ld.TM_Managers_Email__c.endsWith('@example.com'))
            {
                ld.TM_Managers_Email__c = Utils.scrambleEmailAddress(ld.TM_Managers_Email__c);
            }
        }
        
        update scope;
    }

    global void finish(Database.BatchableContext context) {
    }
}