public with sharing class oppPartnerTriggerHandler {
/*******************************************************************************
*
* OppPartner Trigger Handler Class
*
*******************************************************************************/
	public class OppPartnerBeforeInsertHandler implements Triggers.Handler {
		public void handle() {
			ProcessBeforeInsertRecords(Trigger.new);
		}
	} 

	public class OppPartnerAfterInsertHandler implements Triggers.Handler {
		public void handle() {
			ProcessAfterRecords(null,Trigger.new);
		}
	} 

	public class OppPartnerAfterUpdateHandler implements Triggers.Handler {
		public void handle() {
			ProcessAfterRecords(Trigger.oldMap,Trigger.new);
		}
	} 
	
	public class OppPartnerAfterUndeleteHandler implements Triggers.Handler {
		public void handle() {
			ProcessAfterRecords(null,Trigger.new);
		}
	} 

	public static void ProcessBeforeInsertRecords(OppPartner__c[] partners) {
	    /*****************************************************************************************
	    *   Variable Initialization
	    ******************************************************************************************/
	    Map<Id,List<OppPartner__c>> oppPartnerMap = new Map<Id, List<OppPartner__c>>();
	    Map<Id,List<OppPartner__c>> oldOppPartnerMap = new Map<Id, List<OppPartner__c>>();
	    Set<OppPartner__c> dupPartners = new Set<OppPartner__c>();
	   
	    /*****************************************************************************************
	    *   Record Processing
	    ******************************************************************************************/
	    for (OppPartner__c partner : partners) {                                           					// Loop thru all partners being added or updated    	
	    	OppPartner__c op = new OppPartner__c (Opportunity__c = partner.Opportunity__c, 
	    											Partner__c = partner.Partner__c);
    		if(oppPartnerMap.containsKey(partner.Opportunity__c)) {
    			oppPartnerMap.get(partner.Opportunity__c).add(op);											//  save the opportunity id and all new OppPartners in a map
    		}
    		else {
    			oppPartnerMap.put(partner.Opportunity__c,new List<OppPartner__c> {op});
    		}																								
	    } 
	    

	    List<OppPartner__c> oldPartners = new List<OppPartner__c>([SELECT Opportunity__c, Partner__c, 		// select oll oppPartner records into a list, 
	    		Id, Primary_Partner__c, Primary_Distributor__c FROM OppPartner__c 
	    		WHERE Opportunity__c IN :oppPartnerMap.keySet()]);											// where opportunity is in the map keyset
	    		
	    for (OppPartner__c partner : oldPartners) {                                           				// Loop thru all existing partners
    		if(oldOppPartnerMap.containsKey(partner.Opportunity__c)) {
    			oldOppPartnerMap.get(partner.Opportunity__c).add(partner);									//  save the opportunity id and all existing OppPartners in a map
    		}
    		else {
    			oldOppPartnerMap.put(partner.Opportunity__c,new List<OppPartner__c> {partner});
    		}																								
	    } 
	    		
	    for (OppPartner__c partner : partners) {      
	    	List<OppPartner__c> newOppPartners = oppPartnerMap.get(partner.Opportunity__c);
	    	List<OppPartner__c> oldOppPartners = oldOppPartnerMap.get(partner.Opportunity__c);
	    	newOppPartners.sort();
	    	Integer partnerCount = 0;
	    	if (oldOppPartners != null) {
		    	for (OppPartner__c op : oldOppPartners) {
		    		if (partner.Partner__c == op.Partner__c) {												// Check to see if this partner already exists on this opportunity
		    			partnerCount++;
		    		}
		    	}
	    	}
	    	if (partnerCount > 0) {
	    		partner.addError('This partner already exists on this opportunity.');						// If it does, put an error on this trigger record
	    	}
	    	else {
	    		partnerCount = 0;
	    		Boolean dupFound = false;
		    	for (OppPartner__c op : newOppPartners) {													// Else, check to see if the same partner is being added more that once in a batch
		    		system.debug('New OppPartner:' + op);
		    		system.debug('Partner: ' + partner.Opportunity__c + partner.Partner__c);
		    		if (partner.Partner__c == op.Partner__c ) {	
		    			partnerCount++;
		    		}
		    	}
		    	if (partnerCount > 1) {
		    		dupPartners.add(partner);		
		    	}
	    	}
	    }
	    for (OppPartner__c dup : dupPartners) {																// Loop through the duplicates, and after the first occurence is found, put an error on the dups
	    	Boolean matchFound = false;
	    	
	    	for (OppPartner__c partner : partners) {
	    		if(partner.Opportunity__c == dup.Opportunity__c && partner.Partner__c == dup.Partner__c) {
	    			if (matchFound) {
	    				partner.addError('Cannot add the same partner to an opportunity more than once.');
	    			}
	    			else {
	    				matchFound = true;
	    			}
	    		}
	    	}
	    }	
	}

	public static void ProcessAfterRecords(Map<Id, SObject> oldPartnersMap, OppPartner__c[] partners) {
	    /*****************************************************************************************
	    *   Variable Initialization
	    ******************************************************************************************/
	    Map<Id,OppPartner__c> resellerMap = new Map<Id, OppPartner__c>();
	    Map<Id,OppPartner__c> distributorMap = new Map<Id, OppPartner__c>();
	    /*****************************************************************************************
	    *   Record Processing
	    ******************************************************************************************/
	    
	    for (OppPartner__c partner : partners) {                                           					// Loop thru all partners being added or updated
	    	if (trigger.isUpdate) {																			// If update, check for change in primary partner or distributor
	    		OppPartner__c oldPartner = (OppPartner__c)oldPartnersMap.get(partner.Id);
	    		if ((partner.Primary_Partner__c != oldPartner.Primary_Partner__c) && (partner.Primary_Partner__c)) {
	    			resellerMap.put(partner.Opportunity__c, partner);										//  save the opportunity id and OppPartner in reseller map,
	    		}
	    		if ((partner.Primary_Distributor__c != oldPartner.Primary_Distributor__c) && (partner.Primary_Distributor__c)) {
	    			distributorMap.put(partner.Opportunity__c, partner);									//  save the opportunity id and OppPartner in distributor map,
	    		}
	    	}
	    	else if (partner.Primary_Partner__c) {															// if it is primary partner (reseller),
	    		resellerMap.put(partner.Opportunity__c, partner);											//  save the opportunity id and OppPartner in reseller map,
	    	}	
	    	else if (partner.Primary_Distributor__c) {														// if it is primary distributor, 
	    		distributorMap.put(partner.Opportunity__c, partner);										//  save the opportunity id and OppPartner in distributor map,
	    	}																								//  since each opportunity should only have one primary partner
	    } 																									//  and one primary distributor
	    System.debug('@@@ oppParnter after update, resellerMap : ' + resellerMap + ', DistributorMap: ' + distributorMap);
    	List<OppPartner__c> partnersToUpdate = new List<OppPartner__c>();
	    List<OppPartner__c> partnerList = new List<OppPartner__c>([SELECT Id, Primary_Partner__c,				// select oll oppPartner records into a list, 
	    		Primary_Distributor__c, Opportunity__c, Partner__c FROM OppPartner__c 
	    		WHERE (Opportunity__c IN :resellerMap.keySet() OR Opportunity__c IN :distributorMap.keySet())	// where opportunity in map.keyset
	    		AND (Primary_Partner__c = TRUE OR Primary_Distributor__c = TRUE)]);								// and primary partner or primary distributor is true
	    System.debug('@@@ partnerList:' + partnerList);		
	    for (OppPartner__c p : partnerList) {
	    	OppPartner__c reseller = resellerMap.get(p.Opportunity__c);										// get the new primary reseller for the opportunity
	    	OppPartner__c distributor = distributorMap.get(p.Opportunity__c);								// get the new primary distributor for the opportunity	
	    	if (reseller != null && p.Id != reseller.Id && p.Primary_Partner__c == true) {					// if this is not the primary reseller being added or updated
	    		p.Primary_Partner__c = FALSE;	
	    		partnersToUpdate.add(p);
	    	}
	    	if (distributor != null && p.Id != distributor.Id && p.Primary_Distributor__c == true) {		// if this is not the primary distributor being added or updated
	    		p.Primary_Distributor__c = FALSE;															// set primary to false 
	    		partnersToUpdate.add(p);
	    	}
	    }		
	    System.debug('@@@ partnersToUpdate:' + partnersToUpdate);		
	    if (!partnersToUpdate.isEmpty()) {
	    	List<OppPartner__c> partnersToUpdate2 = new List<OppPartner__c>();
	    	for (OppPartner__c op1 : partnersToUpdate) {
	    		Boolean match = false;
	    		for (OppPartner__c op2 : partners) {
	    			if (op1.Id == op2.id ) {
	    				match = true;
	    			}
	    		}																							// only update partners that are not in the original trigger list
	    		if (!match) {
	    			partnersToUpdate2.add(op1);
	    		}
	    	}
	    	
		    update partnersToUpdate2;																		// update this list of partners
	    }
	    
	    Map<Id,Opportunity> oppsToUpdate = new Map<Id,Opportunity>();
	    for (Id oppId : resellerMap.keySet()) 																// for all opps in reseller map, 
	    {	
	    	Opportunity opp = oppsToUpdate.get(oppId);
	    	if (opp == null)
	    	{
	    		opp = new Opportunity(Id = oppId, 
	    		Primary_Partner__c = resellerMap.get(oppId).Partner__c);									// set primary partner on opp to this partner
	    	}
	    	else
	    	{
	    		opp.Primary_Partner__c = resellerMap.get(oppId).Partner__c;									// set primary partner on opp to this partner
	    	}
	    		
	    	if (distributorMap.get(oppId) != null) 
	    	{
	    		if (opp.Primary_Partner__c == distributorMap.get(oppId).Partner__c) 						// if the same partner was the distributor
	    		{
	    			opp.Primary_Distributor__c = null;														//   set primary distributor on opp to null
	    		}
	    	}					
					
	    	oppsToUpdate.put(oppId,opp);
	    }
	    System.debug('@@@ oppsToUpdate - reseller' + oppsToUpdate);
	    for (Id oppId : distributorMap.keySet()) {															// for all opps in distributor map, 
	    	Opportunity opp = oppsToUpdate.get(oppId);
	    	if (opp == null)
	    	{
	    		opp = new Opportunity(Id = oppId, 
	    		Primary_Distributor__c = distributorMap.get(oppId).Partner__c);								// set primary distributor on opp to this partner
	    	}
	    	else
	    	{
	    		opp.Primary_Distributor__c = distributorMap.get(oppId).Partner__c;
	    	}
	    	
	    	if (resellerMap.get(oppId) != null) {	
		    	if (opp.Primary_Distributor__c == resellerMap.get(oppId).Partner__c) {							// if the same partner was the reseller
	    			opp.Primary_Partner__c = null;																//   set primary reseller on opp to null
	    		}		
	    	}
	    	oppsToUpdate.put(oppId,opp);
	    }
	    System.debug('@@@ oppsToUpdate - distrubutor' + oppsToUpdate);
	    update oppsToUpdate.values();																				// update this list of opportunities
		    
	}
}