/**
 * This class contains unit tests for validating the behavior of inboundEmailServiceSyndication class.
 */
@isTest(seeAllData = true)
private class inboundEmailServiceSyndicationTest {

    static testMethod void inboundEmailTest() {
        // Create a new email, envelope object and Attachment
	   Messaging.InboundEmail email = new Messaging.InboundEmail();
	   Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();

	   User u = 
    		([SELECT id 
    		FROM User 
    		WHERE isActive=TRUE 
    		AND Profile.Name != 'System Administrator' 
    		AND UserType = 'Standard'
    		LIMIT 1]);
	
	   email.subject = 'test';
	   email.plainTextBody = '<mailto: John /n Doe > /n Company1 /n Title1 /n Consultant /n City1 /n NY /n US /n 585-555-1212 /n ';
	   email.plainTextBody += String.valueOf(u.Id) + ' /n test@tenable.com /n Notes1 ';
	   env.fromAddress = 'user@acme.com';
	
	   // call the class and test it with the data in the testMethod
	   InboundEmailServiceSyndication emailServiceObj = new InboundEmailServiceSyndication();
	   emailServiceObj.handleInboundEmail(email, env );        
    }
}