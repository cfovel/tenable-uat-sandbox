public with sharing class ExtFundDetails 
{
    public boolean hasErrors {get;set;}
    public boolean isEdit {get;set;}
    private ApexPages.StandardController ctrl;
    public boolean emptyLines {get;set;}
    public boolean POPAcknowledged {get;set;}
    public MDF_Transaction__c fund;
    public String proofOfPerformance {get; set;}
    
    public ExtFundDetails(ApexPages.StandardController ctrl) 
    {
        this.ctrl = ctrl;
        isEdit = false;
        if(ApexPages.currentPage().getParameters() != NULL && ApexPages.currentPage().getParameters().get('Id') != NULL)
        {
            emptyLines = true;
             fund = [SELECT Activity_Type__c, POP_Acknowledged__c FROM MDF_Transaction__c 
                                WHERE Id = : ApexPages.currentPage().getParameters().get('Id')];
            POPAcknowledged = fund.POP_Acknowledged__c;
            GetPOPInfo();
        }
    }
    
    public void GetPOPInfo()                                                                             // Page Initialization
    {
        this.proofOfPerformance = '';
        String filterLogic = '';
        Map<Integer, String> popItemMap = new Map<Integer, String>();
        
        for(Activity_POP_Setting__mdt aps : [SELECT Activity_Type__c, Line_Number__c, Proof_of_Performance__c, Qualifier__c, Filter_Logic__c
                FROM Activity_POP_Setting__mdt WHERE Activity_Type__c = :fund.Activity_Type__c order by Line_Number__c])
        {
            if (aps.Line_Number__c == 1 && aps.Filter_Logic__c > '')
            {
                filterLogic = aps.Filter_Logic__c;
            }
            
            String popItem = aps.Proof_of_Performance__c;
            if (aps.Qualifier__c > '')
            {
                 popItem = popItem + ' ' + aps.Qualifier__c;
            }   
             
            popItemMap.put(Integer.valueOf(aps.Line_Number__c), popItem);
        }
        
        if (filterLogic <= '')
        {
            for(Integer i = 1; i <= popItemMap.size(); i++)
            {
                filterLogic += String.valueOf(i);
                if (i < popItemMap.size())
                {
                    filterLogic += ' AND ';
                }
            }
        }
        
        proofOfPerformance = filterLogic;
        
        for(Integer i = 1; i <= popItemMap.size(); i++)
        {
            if(proofOfPerformance.contains(String.valueOf(i))){
                proofOfPerformance = proofOfPerformance.replace(String.valueOf(i),popItemMap.get(i));
            }
        }
        system.debug('The value of this.proofOfPerformance is : ' + this.proofOfPerformance);   
        if (this.proofOfPerformance > '' && !fund.POP_Acknowledged__c) 
        {
            emptyLines = false;
        }
    }   
    
    public pageReference EditDetails()
    {
        isEdit = true;
        return null;
    }
    
    public pageReference CancelDetails()
    {
        isEdit = false;
        return null;
    }
    
    public pageReference SaveDetails()
    {
        isEdit=false;
        pageReference pref;
        
        pref = ctrl.save();
        return pref;
    }
    
    public pageReference UpdatePOPDetails()
    {
        pageReference pref;
        fund.POP_Acknowledged__c = True;
        update fund;
        emptyLines = true;
        return pref;
    }
}