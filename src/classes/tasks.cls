global with sharing class tasks {
/******************************************************************************
*
*   Custom Controller for Task functions
*   Copyright 2006-2013 (c) by CloudLogistix.com
*   All Rights Reserved, Modifications can only be made for your direct use and 
*   not for distribution without explicit permission from CloudLogistix.
*
********************************************************************************/

	/*****************************************************************************************
	*	Variable Initialization
	******************************************************************************************/
	public Id pageid = ApexPages.currentPage().getParameters().get('id');
	public Task entry {get; set;}
	public Lead lead {get; set;}
	public Contact contact {get; set;}

	/*****************************************************************************************
	*	Custom Controller Initialization
	******************************************************************************************/
    public tasks() {
		entry = 
			([SELECT Id, WhoId, 
			CreatedById, CreatedBy.Name, CreatedDate,
			LastModifiedById, LastModifiedBy.Name, LastModifiedDate
			FROM Task
			WHERE Id = :pageid
			ALL ROWS]);
        init();
	}

 	public void init() {
		if (entry.WhoId != null) {
			if (String.valueof(entry.WhoId).startsWith('00Q')) {
				lead =
					([SELECT Id, Name, Title, Role__c, Email, HasOptedOutOfEmail, Phone, Extension__c
					FROM Lead
					WHERE Id = :entry.whoid]);
			}
			if (String.valueof(this.entry.WhoId).startsWith('003')) {
				contact = 
					([SELECT Id, Name, Title, Role__c, Email, HasOptedOutOfEmail, Phone, Extension__c
					FROM Contact
					WHERE Id = :entry.whoid]);
			}
		}
 	}

	@isTest(seeAllData=TRUE)
	static void testTaskPage() {
		Lead ld = 
			new Lead(lastname='test', firstname='test', company='test', country='GB',
					extension__c = '1234', phone='123123', status='Not Contacted', 
					leadsource='SFDC-TD|Change Control');
		insert ld;
		Contact ct = 
			new Contact(lastname='test', firstname='test', mailingcountry='GB', 
					extension__c = '1234', phone='123123', 
					leadsource='SFDC-TD|Change Control');
		insert ct;
		Task tsk1 = 
			new Task(whoid = ld.id, subject = 'test');
		insert tsk1;

		PageReference pageRef = new PageReference('Page.tasks');
        System.currentPageReference().getParameters().put('id',tsk1.id);        
//        ApexPages.StandardController sc = new ApexPages.standardController(tsk1);
		tasks ext = new tasks();
        ext.init();
	}

}