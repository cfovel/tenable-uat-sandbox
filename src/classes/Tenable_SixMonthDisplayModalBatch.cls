global without sharing class Tenable_SixMonthDisplayModalBatch implements Database.Batchable<sObject>  {

    global Database.QueryLocator start(Database.BatchableContext BC) {
        Integer daysFromLogin = 180;
        String query;
        if (Test.isRunningTest()) {
            query = 'SELECT Id, Days_Since_Initial_Login__c, Display_6_Month_Modal__c FROM User WHERE ' +
                '((Onboarding_Complete__c = True AND Days_Since_Initial_Login__c = :daysFromLogin) OR (Onboarding_Complete__c = True AND Days_Since_Initial_Login__c > :daysFromLogin AND Six_Month_Modal_Displayed_Date__c = null)) ' +
                'AND isActive = true AND Contact.Customer_Community_User__c = True limit 10';
        } else {
            query = 'SELECT Id, Days_Since_Initial_Login__c, Display_6_Month_Modal__c FROM User WHERE ' +
                '((Onboarding_Complete__c = True AND Days_Since_Initial_Login__c = :daysFromLogin) OR (Onboarding_Complete__c = True AND Days_Since_Initial_Login__c > :daysFromLogin AND Six_Month_Modal_Displayed_Date__c = null)) ' +
                'AND isActive = true AND Contact.Customer_Community_User__c = True';
        }

            return Database.getQueryLocator(query);
    }


    global void execute(Database.BatchableContext BC, List <User> scope) {
        Id networkId = [
            SELECT ID
            FROM Network
            WHERE Name = 'Tenable Community'
        ].Id;
        list <User> usersToUpdate = new list <User>();
        for (User us:scope) {
            us.Display_6_Month_Modal__c = True;
            us.Six_Month_Modal_Displayed_Date__c = System.Now();
            usersToUpdate.add(us);
        }
        update usersToUpdate;
    }

    global void finish(Database.BatchableContext BC)
        {
            system.debug('Batch complete!');
        }
}