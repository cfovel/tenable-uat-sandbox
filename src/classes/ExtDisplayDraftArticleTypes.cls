/*************************************************************************************
Class: ExtDisplayDraftArticleTypes
Author: Bharathi.M
Date: 08/29/2017
Details: This class is an extension for the DisplayDraftArticleTypes 
         and DisplayDraftArticleTypesInExcel VF pages (100% Code Coverage)
***************************************************************************************/
public with sharing class ExtDisplayDraftArticleTypes 
{
    public list<selectOption> ObjNames {get;set;}
    public map<string,string> describeMap = new map<string,string>();
    public string ObjectName {get;set;}
    public list<sObject> lstDraftDocInfo{get;set;}
    
    public ExtDisplayDraftArticleTypes(ApexPages.StandardController controller) 
    {
        system.debug('The value of Objectname is : ' + ObjectName);
        GetObjNames();
    }
    
    // Create DescribeMap
    public void GetObjNames() 
    {
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        Set<String> keySet = gd.keySet();
        ObjNames = new list<selectOption>();
        
        for (String key : keySet) 
        {
            Schema.SObjectType objectType = gd.get(key);
            if (key.endsWith('kav') || key.endsWith('kb')) 
            {
                ObjNames.add(new selectOption(objectType.getDescribe().getName(),objectType.getDescribe().getLabel()));
            }
            
        } 
    }
    
    public void GetDraftDocumentInfo()
    {
        lstDraftDocInfo = new list<sObject>() ;
        string Query = 'SELECT Id, ArticleNumber, Validationstatus, PublishStatus, articlecreatedby.name, articlecreatedby.email, ';
        Query += 'CreatedDate, Lastmodifieddate, Title, Applies_To__c From ' + ObjectName + ' WHERE PublishStatus = \'Draft\'';
        
        System.debug('The value of Query is : ' + Query);
        lstDraftDocInfo = database.query(Query);
    }
    
    public pageReference OpenPdfDocument()
    {
        Pagereference pageref = new pagereference('/apex/DisplayDraftArticleTypesInExcel');
        return pageref;
    }
}