@isTest
private class testControllerClasses {
/******************************************************************************
*
*   Test Classes for any Controller Classes
*   Copyright 2006-2014 (c) by CloudLogistix.com
*   All Rights Reserved, Modifications can only be made for your direct use and 
*   not for distribution without explicit permission from CloudLogistix.
*
********************************************************************************/

    /*****************************************************************************************
    *   Test Coverage Classes for Campaigns
    ******************************************************************************************/
    @isTest (SeeAllData=true)
    static void testCampaignExpenses() {
        Account a = new Account(Name='Test1 Inc.', BillingState='CA', BillingCountry='United States'); 
        insert a;

        Contact c = new Contact(lastname='test', accountid=a.id, 
            email='test@test.com', MailingCountry='US', MailingState='CA'); 
        insert c;

        Opportunity o = new Opportunity(name='test', accountid=a.id, 
            StageName='06 - Conditionally Agreed / Closing the', CloseDate=Date.newInstance(2010,10,10)); 
        insert o; 

        Campaign[] camp = new Campaign[0];
        camp.add(new Campaign(name = 'Test Campaign 1', event_title__c='Test', isActive = TRUE, type='Event', event_location__c='Test Location', event_state_province__c='CA', 
                startdate=System.today(), event_focus_country__c='US', Description='TestDesc', event_type__c='Session', Number_Of_Target_Leads__c=100, Sales_Area__c='AMER',
                Sponsorship_Type__c='Industry', Synopsis__c='Test synopsis')); 
        camp.add(new Campaign(name = 'Test Campaign 2', event_title__c='Test', isActive = TRUE, type='Event', event_location__c='Test Location', event_state_province__c='CA', 
                startdate=System.today(), event_focus_country__c='US', Description='TestDesc', event_type__c='Session', Number_Of_Target_Leads__c=100, Sales_Area__c='AMER',
                Sponsorship_Type__c='Industry', Synopsis__c='Test synopsis')); 
        camp.add(new Campaign(name = 'Test Campaign 3', event_title__c='Test', isActive = TRUE, type='Event', event_location__c='Test Location', event_state_province__c='CA', 
                startdate=System.today(), event_focus_country__c='US', Description='TestDesc', event_type__c='Session', Number_Of_Target_Leads__c=100, Sales_Area__c='AMER',
                Sponsorship_Type__c='Industry', Synopsis__c='Test synopsis')); 
        insert camp;
        
        CampaignMember[] cmembs = new CampaignMember[0];
        cmembs.add(new CampaignMember(campaignid=camp[0].id, contactid=c.id, status='test'));
        insert cmembs;
        update cmembs;

        Campaign[] camp2 = new Campaign[0];
        camp2.add(new Campaign(name = 'Test Campaign A', event_title__c='Test Campaign A', type='Event', event_location__c='Test Location', event_state_province__c='CB', 
                startdate=System.today(), event_focus_country__c='us', Description='TestDesc', event_type__c='Session', Number_Of_Target_Leads__c=100, Sales_Area__c='AMER',
                Sponsorship_Type__c='Industry', Synopsis__c='Test synopsis')); 
        camp2.add(new Campaign(name = 'Test Campaign B', event_title__c='Test Campaign B', type='Event', event_location__c='Test Location', event_state_province__c='CA', 
                startdate=System.today(), event_focus_country__c='UU', Description='TestDesc', event_type__c='Session', Number_Of_Target_Leads__c=100, Sales_Area__c='AMER',
                Sponsorship_Type__c='Industry', Synopsis__c='Test synopsis')); 
        camp2.add(new Campaign(name = 'Test Campaign C', event_title__c='1234567890123456789012345678901234567890123456789012345678901234567890', type='Event', event_location__c='LET US REALLY MAKE IT REALLY REALLY REALLY LONG', event_state_province__c='CA', 
                startdate=System.today(), event_focus_country__c='US', Description='TestDesc', event_type__c='Session', Number_Of_Target_Leads__c=100, Sales_Area__c='AMER',
                Sponsorship_Type__c='Industry', Synopsis__c='Test synopsis')); 
        
        try {
            insert camp2;
        }
        catch (exception e) {}

        Campaign_Expenses__c[] expenses = new Campaign_Expenses__c[0];
        expenses.add(new Campaign_Expenses__c(Cost__c = 10.0, Campaign__c = camp[0].id));
        expenses.add(new Campaign_Expenses__c(Cost__c = 10.0, Campaign__c = camp[1].id));
        insert expenses;

        campaigns.clonewithmembers(camp[0].id);
        delete cmembs;

        ApexPages.StandardController sc = new ApexPages.StandardController(camp[0]);
        ApexPages.currentPage().getParameters().put('id', camp[0].id);
        campaigns ext = new campaigns(sc);
        /*****************************************************************************************
        *   Function calls for test coverage
        ******************************************************************************************/ 
        ext.getEntries();
        ext.getLeadMembers();
        ext.memberContactList = null;
        ext.getContactMembers();
        ext.reset();
        ext.resetMembers();
        ext.getEntries();
        ext.save();
        ext.add();
        ext.getLeadMembers();
        ext.getContactMembers();
        ext.setEntries(expenses);
        expenses[0].del__c = true;
        update expenses;
        delete expenses;
        undelete expenses;
        delete camp;
        ext.save();
        ext.reset();
        ext.getEntries();
    }

    /*****************************************************************************************
    *   Test Class for Community Pages
    ******************************************************************************************/
    @isTest(seeAllData=TRUE)
    static void testCommunityPage() {
        User u = 
            ([SELECT ContactId, Contact.AccountId FROM User 
            WHERE IsActive=TRUE 
            AND Profile.Name ='Tenable - Partner Community User' 
            AND Contact.AccountId <> null
            AND Contact.Account.KimbleOne__BusinessUnitTradingEntity__c = null
            //AND Contact.Account.Territory__c = null
            LIMIT 1]);
        User u2 = 
            ([SELECT ContactId, Contact.AccountId FROM User 
            WHERE IsActive=TRUE 
            AND Profile.Name ='Tenable - Partner Manager User' 
            LIMIT 1]);  
            
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
            
        Lead[] leadsToAdd = new Lead[0];
        leadsToAdd.add(
            new Lead(Company='Partner Test Inc.', phone='211-211-1234', fax='211-211-1234', FirstName='Bob', LastName='Smith', Status='Not Contacted', ownerid=u.id));
        leadsToAdd.add(
            new Lead(Company='Partner Test Inc.2', phone='211-211-1234', fax='211-211-1234', FirstName='Bob', LastName='Smith', Status='Contacted Attempted', ownerid=u.id));
        leadsToAdd.add(
            new Lead(Company='Partner Test Inc.2', phone='211-211-1234', fax='211-211-1234', FirstName='Bob', LastName='Smith', Status='Contacted', ownerid=u.id));
        leadsToAdd.add(
            new Lead(Company='Partner Test Inc.2', phone='211-211-1234', fax='211-211-1234', FirstName='Bob', LastName='Smith', Status='Dead', ownerid=u.id));
        insert leadsToAdd;

        MDF_Transaction__c[] fundstoAdd = new MDF_Transaction__c[0];
        fundstoAdd.add(
            new MDF_Transaction__c(Total_Activity_Cost_in_USD__c = 1000.0, Status__c = 'Open', Prospect_Type__c = 'Commercial', 
                                Prospect_Focus__c = 'New', Partner__c = u.Contact.AccountId, Number_of_Leads_Target__c = 100.0, 
                                Number_of_Leads_Actual__c = 20.0, Requested_Amount_in_USD__c = 200.0, Location__c = 'Anywhere', 
                                Industry_Focus__c = 'Test', Date__c = System.today(), Local_Currency__c = 'United States Dollars (USD)',
                                Activity_Type__c = 'Test', Activity_Category__c = 'Test', Lead_Follow_Up_Consent__c = 'Yes'));
        fundstoAdd.add(
                    new MDF_Transaction__c(Total_Activity_Cost_in_USD__c = 1000.0, Status__c = 'Submitted', Prospect_Type__c = 'Commercial', 
                                Prospect_Focus__c = 'New', Partner__c = u.Contact.AccountId, Number_of_Leads_Target__c = 100.0, 
                                Number_of_Leads_Actual__c = 20.0, Requested_Amount_in_USD__c = 200.0, Location__c = 'Anywhere', 
                                Industry_Focus__c = 'Test', Date__c = System.today(), Local_Currency__c = 'United States Dollars (USD)',
                                Activity_Type__c = 'Test', Activity_Category__c = 'Test', Lead_Follow_Up_Consent__c = 'Yes'));
        fundstoAdd.add(
                    new MDF_Transaction__c(Total_Activity_Cost_in_USD__c = 1000.0, Status__c = 'Approved', Prospect_Type__c = 'Commercial', 
                                Prospect_Focus__c = 'New', Partner__c = u.Contact.AccountId, Number_of_Leads_Target__c = 100.0, 
                                Number_of_Leads_Actual__c = 20.0, Requested_Amount_in_USD__c = 200.0, Location__c = 'Anywhere', 
                                Industry_Focus__c = 'Test', Date__c = System.today(), Local_Currency__c = 'United States Dollars (USD)',
                                Activity_Type__c = 'Test', Activity_Category__c = 'Test', Lead_Follow_Up_Consent__c = 'Yes'));
        insert fundstoAdd;

        System.runas(u) {
            PageReference pageRef = Page.partnerCommunitySideBar;
            Test.setCurrentPage(pageRef);
            partnerCommunityClasses ext = new partnerCommunityClasses();
            ext.init();
            ext.getPRMUser();
            ext.getPRMAccount();
            ext.getPRMContact();
            ext.getPRMManager();
            ext.getNumOpenLeads();
            ext.getNumUnreadLeads();
            ext.getNumPendingLeads();
            ext.getNumAcceptedLeads();
            ext.getNumOpenOpps();
            ext.getValueOpenOpps();
            ext.getNumWonOpps();
            ext.getValueWonOpps();
            ext.getFundsAvail();
            ext.getFundsPending();
            ext.getFundsApproved();
            ext.getPVSPrograms();
            ext.getSCPrograms();
            ext.getSCCVPrograms();
            ext.getNessusPrograms();
            ext.getPSPrograms();
            ext.getLCEPrograms();
            ext.getOtherPrograms();
            ext.getNumOpenCases();
            ext.getNumClosedCases();
            ext.getNumPendingCases();
            ext.getNumNewCases();
            ext.getnewKB();
            
            Test.stopTest();
        }
    }
    
    /*****************************************************************************************
    *   Test Class for Funds and Fund Claims
    ******************************************************************************************/
    
    @isTest(seeAllData=TRUE)
    static void testFundAndClaimPages() {
       /* User u = 
            ([SELECT ContactId, Contact.AccountId FROM User 
            WHERE IsActive=TRUE 
            AND Profile.Name ='Tenable - Partner Community User' 
            AND Contact.AccountId <> null
            AND Contact.Account.KimbleOne__BusinessUnitTradingEntity__c = null
            //AND Contact.Account.Territory__c = null
            LIMIT 1]);
            
        User u2 = 
            ([SELECT ContactId, Contact.AccountId FROM User 
            WHERE IsActive=TRUE 
            AND Profile.Name ='Tenable - Partner Manager User' 
            LIMIT 1]);  */

        Map<Id,Id> MDFUserAcctIdMap = new Map<Id,Id>();
        Map<Id,Id> MDFUserAcctIdTeamMap = new Map<Id,Id>();
        
        // Get all Partner Community Users that have MDF Visibility for their assigned Account
        for (PermissionSetAssignment psa : [SELECT AssigneeId, Assignee.ContactId, Assignee.Contact.AccountId, Assignee.Contact.Account.KimbleOne__BusinessUnitTradingEntity__c FROM PermissionSetAssignment WHERE PermissionSet.Label = 'PartnerCommunityMDFVisibility'
                                            AND Assignee.IsActive=TRUE AND Assignee.Profile.Name ='Tenable - Partner Community User' AND Assignee.ManagerId <> null AND Assignee.Contact.AccountId <> null
                                            AND Assignee.Channel_Marketing_Manager__c <> null AND Assignee.Contact.Account.KimbleOne__BusinessUnitTradingEntity__c = null])
        {
            MDFUserAcctIdMap.put(psa.AssigneeId, psa.Assignee.Contact.AccountId);
 
        }
        
        // Get those users that have edit access on the account team
        for(AccountTeamMember atm : [SELECT Id, AccountId, UserId, TeamMemberRole, AccountAccessLevel 
                    FROM AccountTeamMember WHERE AccountId In :MDFUserAcctIdMap.values() AND UserId IN :MDFUserAcctIdMap.keySet() AND AccountAccessLevel = 'Edit' AND isDeleted = false])  
        {
            Id acctId = MDFUserAcctIdMap.get(atm.UserId);

            if(acctId == atm.AccountId)
            {
                MDFUserAcctIdTeamMap.put(atm.UserId,atm.AccountId);
            }
        }
        
        if (!MDFUserAcctIdTeamMap.isEmpty())
        {          
            Id userId = new List<id>(MDFUserAcctIdTeamMap.keySet())[0];
            Id acctId = MDFUserAcctIdTeamMap.get(userId);   
            
            User u = ([SELECT ContactId, Contact.AccountId FROM User WHERE Id = :userId LIMIT 1]);
            
            
			
            Campaign c1 = new Campaign(name = 'TestCampaign', event_title__c='TestEvent', isActive = TRUE, type='Event', event_location__c='Test Location', event_state_province__c='CA', 
                    startdate=System.today(), event_focus_country__c='US', Description='TestDesc', event_type__c='Session', Number_Of_Target_Leads__c=100, Sales_Area__c='AMER',
                    Sponsorship_Type__c='Industry', Synopsis__c='Test synopsis'); 
            insert c1;  
            
            MDF_Transaction__c[] fundstoAdd = new MDF_Transaction__c[0];
            fundstoAdd.add(
                new MDF_Transaction__c(Total_Activity_Cost_in_USD__c = 1000.0, Status__c = 'Open', Prospect_Type__c = 'Commercial', Contact_for_Lead_Updates__c = 'Test',
                                    Prospect_Focus__c = 'New', Partner__c = u.Contact.AccountId, Number_of_Leads_Target__c = 100.0, 
                                    Number_of_Leads_Actual__c = 20.0, Requested_Amount_in_USD__c = 200.0, Location__c = 'Anywhere', 
                                    Industry_Focus__c = 'Test', Date__c = System.today(), Local_Currency__c = 'United States Dollars (USD)',
                                    Associated_Campaign__c = c1.Id, Activity_Type__c = 'Events', Activity_Category__c = 'Test', Lead_Follow_Up_Consent__c = 'Yes'));
            fundstoAdd.add(
                        new MDF_Transaction__c(Total_Activity_Cost_in_USD__c = 1000.0, Status__c = 'Submitted', Prospect_Type__c = 'Commercial', Contact_for_Lead_Updates__c = 'Test',
                                    Prospect_Focus__c = 'New', Partner__c = u.Contact.AccountId, Number_of_Leads_Target__c = 100.0, 
                                    Number_of_Leads_Actual__c = 20.0, Requested_Amount_in_USD__c = 200.0, Location__c = 'Anywhere', 
                                    Industry_Focus__c = 'Test', Date__c = System.today(), Local_Currency__c = 'United States Dollars (USD)',
                                    Activity_Type__c = 'Advertising', Activity_Category__c = 'Test', Lead_Follow_Up_Consent__c = 'Yes'));
            fundstoAdd.add(
                        new MDF_Transaction__c(Total_Activity_Cost_in_USD__c = 1000.0, Status__c = 'Approved', Prospect_Type__c = 'Commercial', Contact_for_Lead_Updates__c = 'Test',
                                    Prospect_Focus__c = 'New', Partner__c = u.Contact.AccountId, Number_of_Leads_Target__c = 100.0, 
                                    Number_of_Leads_Actual__c = 20.0, Requested_Amount_in_USD__c = 200.0, Location__c = 'Anywhere', 
                                    Industry_Focus__c = 'Test', Date__c = System.today(), Local_Currency__c = 'United States Dollars (USD)',
                                    Activity_Type__c = 'Other', Activity_Category__c = 'Test', Lead_Follow_Up_Consent__c = 'Yes'));
            insert fundstoAdd;
            
            Test.startTest();
			Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
            
            System.runas(u) {
                PageReference pageRef = Page.fundInstructions;
                pageRef.getParameters().put('id', fundsToAdd[0].id);
                Test.setCurrentPage(pageRef);
                fundInstructionsController ext = new fundInstructionsController(new ApexPages.StandardController (fundsToAdd[0]));
                ext.init();
                ext.updatePOP();
                ext.getPOPEmpty();
                ext.getPOPAcknowledged();
            }   
            Test.stopTest();
            
            // Approve the fund
            Approval.ProcessResult reqResult;  
            Approval.ProcessSubmitRequest testReq;
    
                
            System.runAs(u)
            {
                testReq = new Approval.ProcessSubmitRequest();
                testReq.setObjectId(fundsToAdd[0].Id);
                
            }   
            
            reqResult = Approval.process(testReq);
            
            // approve the submitted request, through all approval steps
            Approval.ProcessWorkitemRequest testApp = new Approval.ProcessWorkitemRequest();
            testApp.setComments ('');
            testApp.setAction   ('Approve');
            testApp.setWorkitemId(reqResult.getNewWorkitemIds()[0]);
            
            Approval.ProcessResult testAppResult =  Approval.process(testApp);
            
            
            while (testAppResult.getInstanceStatus() != 'Approved')
            {
                testApp.setWorkItemId(testAppResult.getNewWorkitemIds()[0]);
                testAppResult =  Approval.process(testApp);
            }
            
            System.assertEquals('Approved', testAppResult.getInstanceStatus());
            MDF_Transaction__c fund = [SELECT Id, Tenable_P_O_Number__c, Status__c FROM MDF_Transaction__c WHERE id = :fundsToAdd[0].Id];
            fund.Tenable_P_O_Number__c = '12345';
            
            
    
            update fund;
            
            // Create Fund Claim
            System.runAs(u) {
                Fund_Claim__c testFC = new Fund_Claim__c(Fund__c = fund.Id, Claim_Amount__c = 100, POP_Loaded__c = true);
                insert testFC; 
                
                PageReference pageRef = Page.fundClaimInstructions;
                pageRef.getParameters().put('id', testFC.id);
                Test.setCurrentPage(pageRef);
                fundClaimInstructionsController ext = new fundClaimInstructionsController(new ApexPages.StandardController (testFC));
                ext.init();
                ext.updatePOP();
                ext.getPOPEmpty();
            }
            
            
        }
    }
    
    
    /*****************************************************************************************
    *   Test Class for Convert Lead
    ******************************************************************************************/
    @isTest(seeAllData=TRUE)
    static void testConvertLead() {
        User u = 
            ([SELECT ContactId, Contact.AccountId FROM User 
            WHERE IsActive=TRUE 
            AND Profile.Name ='Tenable - Partner Community User' 
            LIMIT 1]);

        Lead leadToAdd = new Lead(Company='Partner Test', phone='211-211-1234', fax='211-211-1234', FirstName='Bob', LastName='Smith', Status='Not Contacted', ownerid=u.id);
        insert leadToAdd;
        
        Account a = new Account(Name='Partner Test', BillingState='CA', BillingCountry='United States'); 
        insert a;
        
        Contact c = new Contact(lastname='test', accountid=a.id, 
            email='test@test.com', MailingCountry='US', MailingState='CA'); 
        insert c;

        Opportunity o = new Opportunity(name='test', accountid=a.id, 
            StageName='06 - Conditionally Agreed / Closing the', CloseDate=Date.newInstance(2015,10,30)); 
        insert o; 
        
        PageReference pageRef = Page.convertLeadWizard;
        pageRef.getParameters().put('id', leadToAdd.id);
        Test.setCurrentPage(pageRef);
        convertLead ext = new convertLead(new ApexPages.StandardController (leadToAdd));
        ext.setLeadStatus('Merge Duplicates');
        String lStatus = ext.getLeadStatus();
        System.assert(lStatus == 'Merge Duplicates');
        ext.getLeadStatuses();
        ext.setUser(String.valueOf(u.id));
        ext.getUser();
        ext.getUsers();
        ext.setAccount(String.valueOf(a.id));
        ext.getAccount();
        ext.getAccounts();
        ext.getNumAccounts();
        ext.setContact(String.valueOf(c.id));
        ext.getContact();
        ext.getContacts();
        ext.getNumContacts();
        ext.setContactRole('Evaluator');
        String cr = ext.getContactRole();
        System.assert(cr == 'Evaluator');
        ext.getContactRoles();
        ext.setOppty(String.valueOf(o.id));
        ext.getOppty();
        ext.getOpportunities();
        ext.setStatus('Qualified');
        String stat = ext.getStatus();
        System.assert(stat == 'Qualified');
        ext.getStatuses();
        ext.getAccounts();
        ext.setDummyOpp(o);
        ext.getDummyOpp();
        
        test.startTest();
            ext.convertLead();
            ext.convert();
        test.stopTest();
    }
    
    /*****************************************************************************************
    *   Test for Account Alerts Controller
    ******************************************************************************************/
    @isTest(seeAllData=TRUE)
    static void testAccountAlerts() {
        Account a = new Account(Name='Account Test', BillingState='CA', BillingCountry='United States'); 
        insert a;
        
        PageReference pageRef = Page.AccountAlerts;
        pageRef.getParameters().put('id', a.id);
        Test.setCurrentPage(pageRef);
        AccountAlertsController ext = new AccountAlertsController(new ApexPages.StandardController (a));
        ext.dismiss();
        
    }
    
    /*****************************************************************************************
    *   Test for EscalatedAccounts Controller
    ******************************************************************************************/
    @isTest(seeAllData=TRUE)
    static void testEscalatedAccountsController() {
        User u = 
            ([SELECT Id FROM User 
            WHERE IsActive=TRUE 
            AND Profile.Name ='Tenable - Support Engineer' 
            LIMIT 1]);
        Account a = new Account(Name='Account Test', BillingState='CA', BillingCountry='United States', Reason_Escalated__c = 'Dissatisfied Customer', 
                        Escalated__c = true, Escalated_By__c = u.Id, Escalation_Status__c = 'Red', Escalation_Notes__c = 'Test notes', Sales_Area__c = 'AMER' ); 
        insert a;
        PageReference pageRef = Page.EscalatedAccounts;
        Test.setCurrentPage(pageRef);
        
        EscalatedAccounts ea = new EscalatedAccounts();
        ea.getEscAccts();
        system.assert(ea.escAcctList.size() > 0);
    }
    
    /*****************************************************************************************
    *   Test for Accounts Controller
    ******************************************************************************************/
    @isTest(seeAllData=TRUE)
    static void testAccountsController() {
        User u = 
            ([SELECT Id FROM User 
            WHERE IsActive=TRUE 
            AND Profile.Name ='Tenable - Support Engineer' 
            LIMIT 1]);
        Account a = new Account(Name='Account Test', BillingState='CA', BillingCountry='United States', Reason_Escalated__c = 'Dissatisfied Customer', 
                        Escalated__c = true, Escalated_By__c = u.Id, Escalation_Status__c = 'Red', Escalation_Notes__c = 'Test notes', Sales_Area__c = 'AMER' ); 
        insert a;
        PageReference pageRef = Page.Accounts;
        Test.setCurrentPage(pageRef);
        
        Accounts ext = new Accounts(new ApexPages.StandardController (a));
        system.assertEquals(ext.getLineItems().size(),0);
    }
    
    /*****************************************************************************************
    *   Test for Account Experience Dashboard Controller
    ******************************************************************************************/
    @isTest(seeAllData=TRUE)
    static void testAccountExperienceDashboard() {
        Account a = new Account(Name='Account Test', BillingState='CA', BillingCountry='United States'); 
        insert a;
        
        Opportunity o =                                                                             // Create a test opportunity
            new Opportunity(name='test', accountid=a.id, StageName='Closed - Won',  CloseDate=Date.newInstance(System.now().year(),System.now().month(),System.now().day()));
        insert o;
        
        Pricebook2 stdPb = ([SELECT Id FROM Pricebook2 WHERE isStandard=TRUE limit 1]);             // Go get the standard pricebook
    
        Product2 prod = new Product2(name='test', geography__c = 'AMER', category__c='Products', Tier_Minimum__c=0, Tier_Maximum__c=4, 
                    family='Services', license_type__c='Test', unit__c='Each', isactive=true, description='test', CanUseRevenueSchedule=true, ProductCode = 'test');
        insert prod;
        
        PriceBookEntry pbe = new PriceBookEntry(Product2Id = prod.Id, PriceBook2Id = stdPb.Id, isActive = true, unitPrice = 200);
        insert pbe;
        
        OpportunityLineItem oli = new OpportunityLineItem(opportunityid = o.id, quantity = 2, pricebookentryid = pbe.Id, unitPrice = pbe.unitPrice);
        insert oli;            
        
        PageReference pageRef = Page.AccountExperienceDashboard;
        pageRef.getParameters().put('id', a.id);
        Test.setCurrentPage(pageRef);
        AccountExperienceDashboardController ext = new AccountExperienceDashboardController(new ApexPages.StandardController (a));
        ext.getSvcTrngProducts();
        
    }
}