public with sharing class fundTriggerHandler {
/*******************************************************************************
*
* Fund Trigger Handler Class
* Version 1.0a
*******************************************************************************/
    public class FundBeforeInsertHandler implements Triggers.Handler {
        public void handle() {
            RequireProofOfPerformance(Trigger.new);
            ConvertCurrencyFields(Trigger.new, null);
        }
    }
    
    public class FundBeforeUpdateHandler implements Triggers.Handler {
        public void handle() {
            RequireAssociatedCampaign(Trigger.newMap, Trigger.oldMap);
            RequireRejectionComments(Trigger.new, Trigger.oldMap);
            RequireProofOfPerformance(Trigger.new);
            ConvertCurrencyFields(Trigger.new, Trigger.oldMap); 
        }
    }
    
    public class FundAfterInsertHandler implements Triggers.Handler {
        public void handle() {
            UpdateRelatedCampaign(Trigger.new, null);
        }
    } 
    
    public class FundAfterUpdateHandler implements Triggers.Handler {
        public void handle() {
            UpdateRelatedCampaign(Trigger.new, Trigger.oldMap);
            UpdateFundClaim(Trigger.newMap, Trigger.oldMap);
        }
    } 
    
    public class FundAfterDeleteHandler implements Triggers.Handler {
        public void handle() {
            /*****************************************************************************************
            *
            Record Processing
            ******************************************************************************************/
            UpdateRelatedCampaign(Trigger.old, null);
        }
    } 
    public class FundAfterUndeleteHandler implements Triggers.Handler {
        public void handle() {
            /*****************************************************************************************
            *
            Record Processing
            ******************************************************************************************/
            UpdateRelatedCampaign(Trigger.new, null);
        }
    }
    
    
    public static void RequireAssociatedCampaign(Map<Id, SObject> newFundMap, Map<Id, SObject> oldFundMap) {
        /*****************************************************************************************
        *   Variable Initialization
        ******************************************************************************************/
        List<Id> campaignIds = new List<Id>();
        
        for(MDF_Transaction__c fund : (List<MDF_Transaction__c>)newFundMap.values())
        {
            /* 
              Get the old object record, and check if the approval status 
              field has been updated to 'In Review'. If so, put it in a map 
              so we only have to use 1 SOQL query to do all checks.
            */
            MDF_Transaction__c oldFund = (MDF_Transaction__c)oldFundMap.get(fund.Id);
            
            if (oldFund.Status__c != 'In Review' && fund.Status__c == 'In Review' && fund.Associated_Campaign__c == null)
            { 
              fund.addError('ASSOCIATED CAMPAIGN IS REQUIRED PRIOR TO PPM APPROVAL.');
            }
            
            if (oldFund.Associated_Campaign__c != fund.Associated_Campaign__c) {
                campaignIds.add(fund.Associated_Campaign__c);
            }
        }
        if (!campaignIds.isEmpty()) {
            Map<Id, Campaign> campaigns = new Map<Id, Campaign>([SELECT Id, Fund_Related__c FROM Campaign WHERE Id IN :campaignIds]);
            for(MDF_Transaction__c fund : (List<MDF_Transaction__c>)newFundMap.values()) {
                MDF_Transaction__c oldFund = (MDF_Transaction__c)oldFundMap.get(fund.Id);
                if (oldFund.Associated_Campaign__c != fund.Associated_Campaign__c) {
                    Campaign c = campaigns.get(fund.Associated_Campaign__c);
                    if (c != null && c.Fund_Related__c) { 
                        fund.addError('FUND MAY NOT BE RELATED TO A CAMPAIGN WHICH HAS A FUND RELATED ALREADY.');
                    }
                }
            }
        }
    }

    public static void RequireRejectionComments(List<MDF_Transaction__c> funds, Map<Id, SObject> oldFundMap) {
        /*****************************************************************************************
        *   Variable Initialization
        ******************************************************************************************/
        Map<Id, MDF_Transaction__c> rejectedFunds = new Map<Id, MDF_Transaction__c>{};
        
        for(MDF_Transaction__c fund : funds)
        {
            /* 
              Get the old object record, and check if the approval status 
              field has been updated to rejected. If so, put it in a map 
              so we only have to use 1 SOQL query to do all checks.
            */
            MDF_Transaction__c oldFund = (MDF_Transaction__c)oldFundMap.get(fund.Id);
        
            if (oldFund.Status__c != 'Rejected' 
             && fund.Status__c == 'Rejected')
            { 
              rejectedFunds.put(fund.Id, fund);  
            }
        }
           
        if (!rejectedFunds.isEmpty())  
        {
            // Get the most recent approval process instance for the object.
            // If there are some approvals to be reviewed for approval, then
            // get the most recent process instance for each object.
            List<Id> processInstanceIds = new List<Id>{};
            
            for (MDF_Transaction__c fund : [SELECT (SELECT ID
                                                      FROM ProcessInstances
                                                      ORDER BY CreatedDate DESC
                                                      LIMIT 1)
                                              FROM MDF_Transaction__c
                                              WHERE ID IN :rejectedFunds.keySet()])
            {
                processInstanceIds.add(fund.ProcessInstances[0].Id);
            }
              
            // Now that we have the most recent process instances, we can check
            // the most recent process steps for comments.  
            for (ProcessInstance pi : [SELECT TargetObjectId,
                                           (SELECT Id, StepStatus, Comments 
                                            FROM Steps
                                            ORDER BY CreatedDate DESC
                                            LIMIT 1 )
                                       FROM ProcessInstance
                                       WHERE Id IN :processInstanceIds
                                       ORDER BY CreatedDate DESC])   
            {                   
                if ((pi.Steps[0].Comments == null || 
                     pi.Steps[0].Comments.trim().length() == 0))
                {
                  rejectedFunds.get(pi.TargetObjectId).addError(
                    'PLEASE PROVIDE A REJECTION REASON IN THE COMMENTS SECTION.');
                }
            }  
        }
    }
    
    public static void RequireProofOfPerformance(List<MDF_Transaction__c> funds) {
        List<Activity_POP_Setting__mdt> apsList = new List<Activity_POP_Setting__mdt>([SELECT Activity_Type__c
                                                                            FROM Activity_POP_Setting__mdt]);
                
        for (MDF_Transaction__c fund : funds) {
            fund.POP_Required__c = false;
            for (Activity_POP_Setting__mdt aps : apsList) {
                if (fund.Activity_Type__c == aps.Activity_Type__c) {
                    fund.POP_Required__c = true;
                    break;
                }
            }
        }       
    }
    
    public static void UpdateRelatedCampaign(List<MDF_Transaction__c> funds, Map<Id, SObject> oldFundMap) {
        /*****************************************************************************************
        *   Variable Initialization
        ******************************************************************************************/
        List<Campaign> campaignsToUpdate = new List<Campaign>();
        List<Campaign> unassociatedCampaigns = new List<Campaign>();
        
        for(MDF_Transaction__c fund : funds)
        {
            /* 
              Get the old object record, and check if the associated campaign has changed. If so, put it in a map 
              so we only have to use 1 SOQL query to do all checks.
            */
            MDF_Transaction__c oldFund = new MDF_Transaction__c();
            if (oldFundMap != null) {
                oldFund = (MDF_Transaction__c)oldFundMap.get(fund.Id);
            }
            if (oldFund.Associated_Campaign__c !=  fund.Associated_Campaign__c)
            { 
                if (oldFund.Associated_Campaign__c != null) {
                    campaignsToUpdate.add(new Campaign(Id = oldFund.Associated_Campaign__c, Fund_Related__c = false));  
                    unassociatedCampaigns.add(new Campaign(Id = oldFund.Associated_Campaign__c, Fund_Related__c = false)); 
                }
                if (fund.Associated_Campaign__c != null) {
                    if (trigger.isDelete) {
                        campaignsToUpdate.add(new Campaign(Id = fund.Associated_Campaign__c, Fund_Related__c = false));  
                        unassociatedCampaigns.add(new Campaign(Id = oldFund.Associated_Campaign__c, Fund_Related__c = false)); 
                    }
                    else {
                        campaignsToUpdate.add(new Campaign(Id = fund.Associated_Campaign__c, Fund_Related__c = true));  
                    }
                }

            }
        }
           
        if (!campaignsToUpdate.isEmpty())  
        {
            if (!unassociatedCampaigns.isEmpty()) {                                                         // check to see if these campaigns are associated to any funds
                List<MDF_Transaction__c> associatedFunds = [SELECT Id, Associated_Campaign__c from MDF_Transaction__c 
                                                WHERE Associated_Campaign__c IN :unassociatedCampaigns];
                if (!associatedFunds.isEmpty()) {
                    Integer i = 0;
                    for (MDF_Transaction__c fund : associatedFunds) {
                        Boolean found = false;
                        for (MDF_Transaction__c f : funds) {
                            if (f.Id == fund.Id && (trigger.isDelete || trigger.isUpdate)) {
                                found = true;
                            }
                        }
                        if (found) {
                            associatedFunds.remove(i);
                        }
                        i++;
                    }
                    for (MDF_Transaction__c fund : associatedFunds) {
                        for (Campaign c: campaignsToUpdate) {
                            if (c.Id == fund.Associated_Campaign__c) {
                                c.Fund_Related__c = true;
                            }
                        }
                    }
                }
            }
            
            update campaignsToUpdate;
        }    
    }    
    
    public static void UpdateFundClaim(Map<Id, SObject> fundMap, Map<Id, SObject> oldFundMap) {
        /*****************************************************************************************
        *   Variable Initialization
        ******************************************************************************************/
        List<Fund_Claim__c> claimsToUpdate = new List<Fund_Claim__c>();
        Map<Id, MDF_Transaction__c> fundsChanged = new Map<Id, MDF_Transaction__c>();
        
        for(MDF_Transaction__c fund : (List<MDF_Transaction__c>)fundMap.values())
        {
            /* 
              Get the old object record, and check if the description or Activity Type has changed. If so, put it in a map 
              so we only have to use 1 SOQL query to do all checks.
            */
            MDF_Transaction__c oldFund = (MDF_Transaction__c)oldFundMap.get(fund.Id);

            if ((oldFund.Description__c !=  fund.Description__c) || (oldFund.Activity_Type__c !=  fund.Activity_Type__c))
            { 
                fundsChanged.put(fund.Id, fund);
            }
        }
           
        if (!fundsChanged.isEmpty())  
        {
            claimsToUpdate = [SELECT Id, Fund__c, Fund_Description__c, Fund_Activity_Type__c from Fund_Claim__c
                                WHERE Fund__c IN :fundsChanged.keySet()];
            
            if (!claimsToUpdate.isEmpty())
            {
                for (Fund_Claim__c claim : claimsToUpdate)
                {
                    MDF_Transaction__c fund = (MDF_Transaction__c)fundMap.get(claim.Fund__c);
                    MDF_Transaction__c oldFund = (MDF_Transaction__c)oldFundMap.get(claim.Fund__c);
                    if (fund.Activity_Type__c != oldFund.Activity_Type__c)
                    {
                        claim.POP_Loaded__c = False;
                    }
                    claim.Fund_Description__c = fundsChanged.get(claim.Fund__c).Description__c;
                }    
                update claimsToUpdate;
            }    
        }    
    }   
    
    public static void ConvertCurrencyFields(List<MDF_Transaction__c> funds, Map<Id, SObject> oldFundMap)
    {
        List<String> isoCodes = new List<String>();
        Map<String, CurrencyType> conversionMap = new Map<String, CurrencyType>();
        Boolean amountChanged;
        
        // Pre-processing
        
        for (MDF_Transaction__c fund : funds)
        {
            //Get the old object record, and check if the requested amount has changed. If so, put it in a list 
            //so we only have to use 1 SOQL query to get all conversion rates.
            
            system.debug('@@@ FundTriggerHandler - Status:' + fund.Status__c);
            amountChanged = false;
            MDF_Transaction__c oldFund;
            if (oldFundMap != null)
            {
            	oldFund = (MDF_Transaction__c)oldFundMap.get(fund.Id);
            	system.debug('@@@ old Status:' + oldfund.Status__c);
            	if ((oldFund != null) && (fund.MDF_Amount__c != oldFund.MDF_Amount__c || fund.Local_Currency__c != oldFund.Local_Currency__c || 
            				fund.Total_Activity_Cost__c != oldFund.Total_Activity_Cost__c || fund.Requested_Amount_in_USD__c == null))
            	{
            		amountChanged = true;
            	}
            }
            	
            if ((trigger.isInsert || amountChanged) && fund.Local_Currency__c != null && !fund.Local_Currency__c.contains('(USD)'))
            {
                isoCodes.add(fund.Local_Currency__c.substringBetween('(', ')'));
            }
        } 
        
        if (!isoCodes.isEmpty())                                                                // get conversion rates
        {   
            
            for (CurrencyType curr : [SELECT isoCode, decimalPlaces, conversionRate FROM CurrencyType
                                    WHERE isoCode in :isoCodes])
            {
                conversionMap.put(curr.isoCode, curr);
            }                       
            
        }
        
        conversionMap.put('USD', new CurrencyType(isoCode = 'USD', decimalPlaces = 2, conversionRate = 1.0));
        
        // Processing
 
        for (MDF_Transaction__c fund : funds)
        {
            amountChanged = false;
            MDF_Transaction__c oldFund;
            if (oldFundMap != null)
            {
            	oldFund = (MDF_Transaction__c)oldFundMap.get(fund.Id);
            	if ((oldFund != null) && (fund.MDF_Amount__c != oldFund.MDF_Amount__c || fund.Local_Currency__c != oldFund.Local_Currency__c || 
            					fund.Total_Activity_Cost__c != oldFund.Total_Activity_Cost__c || fund.Requested_Amount_in_USD__c == null))
            	{
            		amountChanged = true;
            	}
            }
            
            if (trigger.isInsert || amountChanged)
            {
            	double convRate;
            	if (fund.Local_Currency__c != null && fund.Local_Currency__c != 'Other')
            	{
            		convRate = conversionMap.get(fund.Local_Currency__c.substringBetween('(', ')')).conversionRate;
            	}
            	
	            if (convRate == null)
	            {
	                convRate = 1;
	            }
	            if (fund.MDF_Amount__c != null)
	            {
            		fund.Requested_Amount_in_USD__c = fund.MDF_Amount__c / convRate;
	            }
	            if (fund.Total_Activity_Cost__c != null)
	            {
            		fund.Total_Activity_Cost_in_USD__c = fund.Total_Activity_Cost__c / convRate;
	            }
            }
        }
    }  
}