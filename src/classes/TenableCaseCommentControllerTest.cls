/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TenableCaseCommentControllerTest {

    static testMethod void testIsCommentAllowed() {
      // This flag is used by most of the code
      utils.isTest = true;
      //Account
      Account a = new Account(name='Tenable Unit Test');
      insert a;
      // Contact
      Contact c = TestHelper.createContact(a.Id, true);
      
      Case customerCase = TestHelper.createCase(a.Id, c.Id, true);
      
      Test.startTest();
      TenableCaseCommentController ctrl = new TenableCaseCommentController();
      Boolean isCommentAllowed = TenableCaseCommentController.isCommentAllowed(customerCase.Id);
      Test.stopTest();
    }
    
    static testMethod void testGetCaseComments(){
      // This flag is used by most of the code
      utils.isTest = true;
      //Account
      Account a = new Account(name='Tenable Unit Test');
      insert a;
      // Contact
      Contact c = TestHelper.createContact(a.Id, true);
      
      Case customerCase = TestHelper.createCase(a.Id, c.Id, true);

      Test.startTest();
      List< TenableCaseCommentController.CaseCommentData> CaseCommentList = TenableCaseCommentController.getCaseComments(customerCase.Id);
      Test.stopTest();
    }
    
    static testMethod void addCaseComments(){
      // This flag is used by most of the code
      utils.isTest = true;
      //Account
      Account a = new Account(name='Tenable Unit Test');
      insert a;
      // Contact
      Contact c = TestHelper.createContact(a.Id, true);
      system.debug('*************** printing out contact ......addCaseComments().....');
      system.debug(c);
	  // Community user
      List< TenableCaseCommentController.CaseCommentData> CaseCommentList;
      Case customerCase = TestHelper.createCase(a.Id, c.Id, true);

      Boolean addCaseCommentSuccess1 = TenableCaseCommentController.addCaseComments(customerCase.Id, 'Adding a case comment test 1');
      Boolean addCaseCommentSuccess2 = TenableCaseCommentController.addCaseComments(customerCase.Id, 'Adding a case comment test 2');
      CaseComment CustomerCaseComment = new CaseComment();
      CustomerCaseComment.CommentBody = 'Adding a case comment test 3';
      CustomerCaseComment.CreatedDate = Datetime.Now().addDays(-1);
      CustomerCaseComment.ParentId = customerCase.Id;
      CustomerCaseComment.IsPublished = true;
      insert CustomerCaseComment;
      // sortable conditions 0, 1, -1
      CaseCommentList = TenableCaseCommentController.getCaseComments(customerCase.Id);
      
      Test.startTest();
      for(TenableCaseCommentController.CaseCommentData cd: CaseCommentList){
       String str1 = cd.getCreatorName();
       String str2 = cd.getAccountName();
       DateTime dt1 = cd.getCreatedDate();
       String str3 = cd.getCreatedByName();
       String str4 = cd.getCreatedByFirstName();
       String str5 = cd.getCreatedByLastName();
       String Str6 = cd.getParentContactName();
       String str7 = cd.getParentContactAccountName();
       String str8 = cd.getParentOwnerName();
       String str9 = cd.getParentOwnerId();
       String str10 = cd.getCreatedById();
       String str11 = cd.getCommentBody();
       String str12 = cd.getParentContactId();
      }
      Test.stopTest();
    }
}