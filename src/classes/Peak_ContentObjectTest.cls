@IsTest(SeeAllData=true)
private class Peak_ContentObjectTest {
	
    static void testSetup() {
        Utils.isTest = true;
       	accountTriggerHandler.enforceCountry = false;
       	Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
        
        String profId = [select Id from Profile where Name = 'Tenable - Customer Community Standard User' limit 1].Id;
        
        // Find User.
        User testUser = [select Id from User where ProfileId = :profId and isActive = true  limit 1];
    }

    @isTest
    public static void testPeak_ContentObject() {

        List<Contact> testContacts = [SELECT Id from Contact where Email = :Peak_TestConstants.STANDARD_EMAIL];
		
        Utils.isTest = true;
       	accountTriggerHandler.enforceCountry = false;
        Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
        
        String profId = [select Id from Profile where Name = 'Tenable - Customer Community Standard User' limit 1].Id;
        
        // Find User.
        User testUser = [select Id from User where ProfileId = :profId and isActive = true  limit 1];
		
        Peak_ContentObject peakContentObject = new Peak_ContentObject();
        peakContentObject.contentID = testUser.Id;
        peakContentObject.title = Peak_TestConstants.FIRSTNAME;
        peakContentObject.description = Peak_TestConstants.TEST_DESCRIPTION;
        peakContentObject.fullDescription = Peak_TestConstants.TEST_DESCRIPTION;
        peakContentObject.attachments = new List<Attachment>();
        peakContentObject.url = Peak_TestConstants.TEST_URL;
        peakContentObject.featured = false;
        peakContentObject.bannerImage = '';
        peakContentObject.avatar = '';
        peakContentObject.commentCount = 1;
        peakContentObject.commentUrl = '';
        peakContentObject.dateTimeField = DateTime.newInstance(2011, 11, 18, 3, 3, 3);
        peakContentObject.dateField = Peak_TestConstants.TODAY;

        system.assertEquals(peakContentObject.title,Peak_TestConstants.FIRSTNAME);
    }
}