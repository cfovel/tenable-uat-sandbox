global class LiveAgentPreChatFormController {
	
    private static final Map<Id, RecordType> CASE_RECORDTYPE_MAP = new Map<Id, RecordType>([
        SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'Case'
    ]);
    // Default Record Type for current User requires reading the
    // Profile from Metadata API. Timeout occurs because response takes
    // so long, so setting here
    private static final String DEFAULT_CASE_RECORDTYPE_NAME = 'Community_Case';
    
    // Metadata API access requires authenticated user
    // Not available on Force.com Sites, so Record Type visibilities
    // for Product__c is hard-coded in this label
    private static final Set<String> SET_PRODUCT_VALUES = new Set<String>(Label.TechnicalSupport_CaseProduct_Values.split(','));
    private static final Set<String> SET_PRODUCT_VERSION_EXCLUDED_VALUES = new Set<String>(Label.TechnicalSupport_CaseProductVersion_ExcludedValues.split(','));
    
    private static Map<String, List<String>> MAP_PRODUCT_TO_PRODUCTVERSION_VALUES = FieldDescribeUtil.getDependentOptionsImpl(Case.Product_Version__c, Case.Product__c);
    private static final Map<String, List<String>> MAP_PRODUCT_TO_OPERATIONSYSTEM_VALUES = FieldDescribeUtil.getDependentOptionsImpl(Case.Operation_System__c, Case.Product__c);
    
    private String pProductSelectOptionList;
    private String pProductVersionSelectOptionList;
    private String pOperationSystemSelectOptionList;
    private String pProductToDependentValuesSerialized;

    
    private static RecordType defaultRecordType {
        get {
            for(Id rtId : CASE_RECORDTYPE_MAP.keySet()) {
                RecordType rt = CASE_RECORDTYPE_MAP.get(rtId);
                if(rt.DeveloperName == DEFAULT_CASE_RECORDTYPE_NAME) {
                    return rt;
                }
            }
            System.debug(LoggingLevel.ERROR, 'Record type not found for ' + DEFAULT_CASE_RECORDTYPE_NAME);
            return null;
        }
        private set;
    }
    
    public String customerContactId {get;set;}
    public Contact customerContact {get;set;}
    public Case customerCase {get;set;}
    
    global LiveAgentPreChatFormController(){
  	    this.customerCase = new Case(
            // Record Type does not get set until insert, so
            // manually setting here first
            RecordTypeId = defaultRecordType.Id
        );
        System.debug('customerCase.RecordTypeId: ' + customerCase.RecordTypeId);
    }

    /*
    public String productSelectOptionList {
        get{
            List<Schema.PicklistEntry> allProductPicklistEntries = Case.Product__c.getDescribe().getPicklistValues();
            List<Schema.PicklistEntry> availableProductPicklistEntries = new List<Schema.PicklistEntry>();
            for(Schema.PicklistEntry productPle : allProductPicklistEntries) {
                if(SET_PRODUCT_VALUES.contains(productPle.getValue())){
                    availableProductPicklistEntries.add(productPle);
                }
            }
            return JSON.serialize(availableProductPicklistEntries);
        }
        private set;
    }
    
    public String productVersionSelectOptionList {
        get{
            System.debug(LoggingLevel.DEBUG, 'Entering LiveAgentPreChatFormController.productVersionSelectOptionList getter');
            System.debug(LoggingLevel.DEBUG, 'this.customerCase.Product__c: ' + this.customerCase.Product__c);
            Map<String, List<String>> mapProductToVersionValues = FieldDescribeUtil.getDependentOptionsImpl(Case.Product_Version__c, Case.Product__c);
            System.debug(LoggingLevel.DEBUG, 'mapProductToVersionValues: ' + mapProductToVersionValues);
            for (String controllingFieldValue : MAP_PRODUCT_TO_PRODUCTVERSION_VALUES.keySet()) {
                this.caseProductToCaseProductVersionEntries.put(controllingFieldValue, mapProductToVersionValues.get(controllingFieldValue));
            }
            System.debug('this.caseProductToCaseProductVersionEntries: ' + this.caseProductToCaseProductVersionEntries);
            Set<String> availableVersionValues = this.customerCase.Product__c == null ? new Set<String>() : new Set<String>(mapProductToVersionValues.get(this.customerCase.Product__c));
            System.debug('availableVersionValues: ' + availableVersionValues);
            List<Schema.PicklistEntry> allProductVersionPicklistEntries = Case.Product_Version__c.getDescribe().getPicklistValues();
            List<Schema.PicklistEntry> availableProductVersionPicklistEntries = new List<Schema.PicklistEntry>();
            for(Schema.PicklistEntry productVersionPle : allProductVersionPicklistEntries) {
                // Filter based on results from dependent picklists
                if(availableVersionValues.contains(productVersionPle.getValue())) {
                    availableProductVersionPicklistEntries.add(productVersionPle);
                }
            }
            return JSON.serialize(availableProductVersionPicklistEntries);
        }
        private set;
    }

    public String operationSystemSelectOptionList {
        get{
            System.debug(LoggingLevel.DEBUG, 'Entering LiveAgentPreChatFormController.operationSystemSelectOptionList getter');
            System.debug(LoggingLevel.DEBUG, 'this.customerCase.Product__c: ' + this.customerCase.Product__c);
            Map<String, List<String>> mapProductToOperationSystemValues = FieldDescribeUtil.getDependentOptionsImpl(Case.Operation_System__c, Case.Product__c);
            System.debug(LoggingLevel.DEBUG, 'mapProductToOperationSystemValues: ' + mapProductToOperationSystemValues);
            Set<String> availableOperationSystemValues = this.customerCase.Product__c == null ? new Set<String>() : new Set<String>(mapProductToOperationSystemValues.get(this.customerCase.Product__c));
            System.debug('availableOperationSystemValues: ' + availableOperationSystemValues);
            List<Schema.PicklistEntry> allOperationSystemPicklistEntries = Case.Operation_System__c.getDescribe().getPicklistValues();
            List<Schema.PicklistEntry> availableOperationionPicklistEntries = new List<Schema.PicklistEntry>();
            for(Schema.PicklistEntry productVersionPle : allOperationSystemPicklistEntries) {
                // Filter based on results from dependent picklists
                if(availableOperationSystemValues.contains(productVersionPle.getValue())) {
                    availableOperationionPicklistEntries.add(productVersionPle);
                }
            }
            return JSON.serialize(availableOperationionPicklistEntries);
        }
        private set;
    }
	*/
    
    public String productToDependentValuesMap {
        get {
            if(pProductToDependentValuesSerialized == null) {
                Map<String, Map<String, Map<String, List<String>>>> mapDependentPicklists = new Map<String, Map<String, Map<String, List<String>>>>();
                String productFieldApiName = Schema.Case.fields.Product__c.getDescribe().getName(),
                       productVersionFieldApiName = Schema.Case.fields.Product_Version__c.getDescribe().getName(),
                       operationSystemFieldApiName = Schema.Case.fields.Operation_System__c.getDescribe().getName();
                mapDependentPicklists.put(productFieldApiName, new Map<String, Map<String, List<String>>>());
                // Put an empty list for each value mapped to null
                Map<String, List<String>> nullProductValueMap = new Map<String, List<String>>{
                    productVersionFieldApiName => new List<String>(),
                    operationSystemFieldApiName => new List<String>()
                };
                mapDependentPicklists.get(productFieldApiName).put('', nullProductValueMap);
                
                // Remove excluded values from ProductVersion picklist
                Map<String,List<Integer>> prodVersionsToRemove = new Map<String,List<Integer>>();
                for (String prod : MAP_PRODUCT_TO_PRODUCTVERSION_VALUES.keyset())
                {
                	//List<String> versions = MAP_PRODUCT_TO_PRODUCTVERSION_VALUES.get(prod);
                	List<String> newVersions = new List<String>();
                	for (String version : MAP_PRODUCT_TO_PRODUCTVERSION_VALUES.get(prod))
                	{
                		if (!SET_PRODUCT_VERSION_EXCLUDED_VALUES.contains(version))
                		{
                			newVersions.add(version);
                		}
                	}
                	MAP_PRODUCT_TO_PRODUCTVERSION_VALUES.put(prod,newVersions);

                }
                
                for(String productValue : SET_PRODUCT_VALUES) {
                    System.debug(LoggingLevel.DEBUG, 'productValue: ' + productValue);
                    mapDependentPicklists.get(productFieldApiName).put(productValue, new Map<String, List<String>>());
                    System.debug(LoggingLevel.DEBUG, mapDependentPicklists.get(productFieldApiName).get(productValue));
                    // Place Product Version values in map
                    if(MAP_PRODUCT_TO_PRODUCTVERSION_VALUES.containsKey(productValue)) {
                        List<String> productVersionValues = MAP_PRODUCT_TO_PRODUCTVERSION_VALUES.get(productValue);
                        System.debug(LoggingLevel.DEBUG, productVersionValues);
                        if(productVersionValues == null) {
                            productVersionValues = new List<String>();
                        }
                        System.debug(LoggingLevel.DEBUG, productVersionValues);
                        System.debug(LoggingLevel.DEBUG, mapDependentPicklists.get(productFieldApiName));
                        System.debug(LoggingLevel.DEBUG, mapDependentPicklists.get(productFieldApiName).get(productValue));
                        System.debug(LoggingLevel.DEBUG, 'productVersionFieldApiName: ' + productVersionFieldApiName);
                        mapDependentPicklists.get(productFieldApiName).get(productValue).put(productVersionFieldApiName, productVersionValues);
                        System.debug(LoggingLevel.DEBUG, mapDependentPicklists.get(productFieldApiName).get(productValue));
                    }
                    
                    // Place Operation System values in map
                    if(MAP_PRODUCT_TO_OPERATIONSYSTEM_VALUES.containsKey(productValue)) {
                        List<String> operationSystemValues = MAP_PRODUCT_TO_OPERATIONSYSTEM_VALUES.get(productValue);
                        System.debug(LoggingLevel.DEBUG, 'operationSystemValues: ' + operationSystemValues);
                        if(operationSystemValues == null) {
                            operationSystemValues = new List<String>();
                        }
                        System.debug(LoggingLevel.DEBUG, 'operationSystemValues: ' + operationSystemValues);
                        mapDependentPicklists.get(productFieldApiName).get(productValue).put(operationSystemFieldApiName, operationSystemValues);
                    }
                    System.debug(LoggingLevel.DEBUG, mapDependentPicklists);
                }
                pProductToDependentValuesSerialized = JSON.serialize(mapDependentPicklists);
            }
            return pProductToDependentValuesSerialized;
            //return '{"Product__c":{"Account Management":{"Product_Version__c":["NA"],"Operation_System__c":[]},"LCE Clients/Monitors":{"Product_Version__c":["3.6.x","4.0.x","4.2.x","4.4.x","4.6.x","Other"],"Operation_System__c":["AIX","Debian/Kali"]}}}';
        }
        private set;
    }
    
    public String refreshDependentPicklists() {
        System.debug(LoggingLevel.DEBUG, 'Entering LiveAgentPreChatFormController.refreshDependentPicklists...');
        Map<String, List<String>> mapProductToProductVersionValues = refreshProductVersion();
        
        Map<String, List<String>> mapProductToOperationSystemValues = refreshOperationSystem();
        Map<String, Map<String, List<String>>> returnMap = new Map<String, Map<String, List<String>>>();
        return '';
    }
    
    private static Map<String, List<String>> refreshProductVersion() {
        System.debug(LoggingLevel.DEBUG, 'Refreshing Product_Version__c picklist...');
        return new Map<String, List<String>>();
    }
    
    private static Map<String, List<String>> refreshOperationSystem() {
        System.debug(LoggingLevel.DEBUG, 'Refreshing Operation_System__c picklist...');
        return new Map<String, List<String>>();
    }
    
    @RemoteAction
    global static User getCustomerInfo(String customerContactId){
  	    User customerInfo;
        if(!String.isBlank(customerContactId)){
            customerInfo = [
                SELECT Username, UserType, Street, State, Profile.Name, ProfileId, Phone, Name, MobilePhone, 
  	                   LastName, FirstName, Email, CreatedDate, Country, CompanyName, CommunityNickname, City, 
  	                   Can_Create_Modify_Users__c, Alias, Contact.LMS_Customer_ID__c, Contact.AccountId, Contact.Account.Name
                FROM User 
                WHERE ContactId = :customerContactId
            ];
        }
        return customerInfo;
    }
}