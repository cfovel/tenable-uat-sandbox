public with sharing class dashboardMetricClasses {

    public List <Event> getLast5Events() {
        return ([SELECT Type, Subject, Id, WhatId, What.Name, Description, ActivityDate FROM Event
                WHERE ActivityDate < :System.today()
                AND OwnerID = '02360000001ce5x'
                ORDER BY ActivityDate DESC
                LIMIT 5]);
    }

    public List <Event> getNext5Events() {
        return ([SELECT Type, Subject, Id, WhatId, What.Name, Description, ActivityDate FROM Event
                WHERE ActivityDate >= :System.today()
                AND OwnerID = '02360000001ce5x'
                ORDER BY ActivityDate ASC
                LIMIT 5]);
    }
}