@isTest
public with sharing class Peak_BaseControllerTest {

    @testSetup static void testSetup() {
        Utils.isTest = true;
        Contact testContact = Peak_TestUtils.createTestContact();
    }

    @isTest
    public static void testGetSitePrefix() {
        system.assert(Peak_BaseController.getSitePrefix() != null);
    }

    // Test finding a user
    @isTest
    public static void testUser(){
        List<Contact> testContacts = [SELECT Id from Contact where Email = :Peak_TestConstants.STANDARD_EMAIL];

        test.startTest();
        Utils.isTest = true;
        Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
        User testUser = Peak_TestUtils.createStandardUserNoContact();
        testUser.ContactId = testContacts[0].Id;
        insert testUser;
        Test.stopTest();

        System.runAs(testUser) {
            System.assertEquals(false,Peak_BaseController.isGuestUser());
        }
    }

    // Test running as a guest
    @isTest
    public static void testGuest(){
        // Set up and run as guest user 
        User guestUser = Peak_TestUtils.createGuestUser();

        System.runAs(guestUser) {
            System.assertEquals(true,Peak_BaseController.isGuestUser());
        }
    }

}