public with sharing class quoteTriggerHandler {
/*******************************************************************************
*
* Quote Trigger Handler Class
* Version 1.0a
* Copyright 2006-2014 (c) by CloudLogistix.com
* All Rights Reserved
* Your are allowed to make modifications only for yourself, not for distribution
* without permission from CloudLogistix.
*
*******************************************************************************/
    public class QuoteBeforeInsertHandler implements Triggers.Handler {
        public void handle() {
            ProcessBeforeRecords(Trigger.new);
        }
    } 

    public class QuoteBeforeUpdateHandler implements Triggers.Handler {
        public void handle() {
//          ProcessBeforeRecords(Trigger.new, Trigger.old);
        }
    } 

    public class QuoteAfterUpdateHandler implements Triggers.Handler {
        public void handle() {
//          ProcessAfterRecords(Trigger.new, Trigger.old);
        }
    } 

    public static void ProcessBeforeRecords(Quote__c[] quotes) {
        /*****************************************************************************************
        *   Variable Initialization
        ******************************************************************************************/
        String pricelist = 'AMER';
        
        /*****************************************************************************************
        *   Record Pre-Processing
        ******************************************************************************************/
        User usr = ([SELECT Id, Price_List_Access__c FROM User WHERE Id = :Userinfo.getUserId()]);      

        if ((usr != null) &&
            (usr.Price_List_Access__c != null)) {
            pricelist = usr.Price_List_Access__c.left(4);
        }

        /*****************************************************************************************
        *   Record Processing
        ******************************************************************************************/
        for (Quote__c q : quotes) {
            if (q.Geography_Price_List__c == null) {
                q.Geography_Price_List__c = pricelist;
            }
        }
    }

}