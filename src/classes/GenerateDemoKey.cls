public class GenerateDemoKey{
    
    public Account acct { get; private set; }
    private final ApexPages.standardController stdController;                   // Set-up Controller Class
    

    
    public GenerateDemoKey(ApexPages.StandardController stdController){

        this.stdController = stdController;                  
        //this.stdController.addFields(new String[]{
        //  'NetSuite_Id__c',
        //  'NetSuite_Link__c',
        //  'Subsidiary__c',
        //  'Local_Currency__c'
        //});                                               // Set-up Controller
        //this.acct = (Account)stdController.getRecord();
        this.acct = [SELECT Id, NetSuite_Id__c, NetSuite_Link__c, Account_ID_18_Digit__c, LMS_Customer_ID__c, Subsidiary__c, Local_Currency__c FROM Account WHERE Id = :stdController.getId()];
    }
    
    public PageReference GenerateDemoKeys(){
        String AccountId   = ApexPages.currentPage().getParameters().get('id');
        Account acct = [SELECT Id, NetSuite_Id__c, NetSuite_Link__c, Account_ID_18_Digit__c, LMS_Customer_ID__c, Subsidiary__c, Local_Currency__c FROM Account WHERE Id = :AccountId];
        String sfdcId      = acct.Account_ID_18_Digit__c;
        String customerId  = acct.LMS_Customer_ID__c;
        String userName    = UserInfo.getUserEmail();
        PageReference page = new PageReference('/' + AccountId);
        string pURL = 'https://integrate.tenable.com/tst/provisioning/lmsadmin/demokey.html?';
        map<string, string> queryString = new map<string,String>();
        queryString.put('sfdcId',sfdcId);
        queryString.put('customerId',customerId);
        queryString.put('userName',userName);
        string tempVal = '';
        system.debug('before loop: ' + queryString);
        for(string s: queryString.keySet()){
            tempVal = queryString.get(s);
            system.debug('In Loop: ' + tempVal + ' ' + s);
            if(tempVal != null && tempVal != ''){
                pURL += s + '=' + tempVal + '&';
            }
        } 
        pURL = pURL.removeEnd('&');
 
        system.debug('Generate Demo Key: ' + pURL);
        
        
        PageReference pageRef = new PageReference(pURL);
        pageRef.getHeaders().put('Authorization', 'Basic bXVsZXNvZnQtdXNlcjpfVCNrZ18lQ0FtP3BuOSEz');
        pageRef.getHeaders().put('X-SFDC-Access-Token', 'Basic bXVsZXNvZnQtdXNlcjpfVCNrZ18lQ0FtP3BuOSEz');
        pageRef.setRedirect(true);
        return pageRef;      
    }    
    
}