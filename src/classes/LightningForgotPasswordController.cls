global class LightningForgotPasswordController 
{
    public static String Username;
    public static String ClientCredentials;
    public static String ClientId;
    public static Blob headerValue;
    public static String authorizationHeader;
    public static String ContentType;
    public static String EndPoint;  
    public String EndpointBody;
    
    /*public LightningForgotPasswordController() 
{
}*/
    
    @AuraEnabled
    public static void forgotPassword(String username)
    {
        try
        {
            futurePostUserInfo(username);
        }
        catch(exception e)
        {
            system.debug('There was an error : ' + e.getMessage());
        }
    }
    
    public Static void SetAuthorizationHeaderValues()
    {
        Boolean IsSandbox = [Select Id,IsSandbox From Organization].IsSandbox;   
        
        for(Integration_Setting__mdt is : [SELECT DeveloperName,Environment_Ind__c, Client_ID__c,Client_Secret__c,Email_Address__c,Endpoint_URL__c
                                           FROM Integration_Setting__mdt order by Environment_Ind__c])
        {
            if(IsSandbox && is.DeveloperName == 'Sandbox')
            {
                ClientCredentials = is.Client_ID__c + ':' + is.Client_Secret__c;
                headerValue = Blob.valueOf(ClientCredentials);
                authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                endpoint = is.Endpoint_URL__c;
            }
            else if(!IsSandbox && is.DeveloperName == 'Production')
            {
                ClientCredentials = is.Client_ID__c + ':' + is.Client_Secret__c;
                headerValue = Blob.valueOf(ClientCredentials);
                authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                endpoint = is.Endpoint_URL__c;
            }
            else if(IsSandbox && is.DeveloperName == 'Auth0_Sandbox')
            {
                ClientId = is.Client_ID__c;
                ClientCredentials = is.Client_ID__c + ':' + is.Client_Secret__c;
                headerValue = Blob.valueOf(ClientCredentials);
                authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                endpoint = is.Endpoint_URL__c;
            }
            else if(!IsSandbox && is.DeveloperName == 'Auth0_Production')
            {
                ClientId = is.Client_ID__c;
                ClientCredentials = is.Client_ID__c + ':' + is.Client_Secret__c;
                headerValue = Blob.valueOf(ClientCredentials);
                authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                endpoint = is.Endpoint_URL__c;
            }
        }
    }
    
    
    public static HttpResponse postUserName(String UserName) 
    {
        
        SetAuthorizationHeaderValues();
        String jsonString = 'client_id=' + EncodingUtil.urlEncode(ClientId, 'UTF-8') + '&email=' + EncodingUtil.urlEncode(UserName, 'UTF-8') + '&connection=' + EncodingUtil.urlEncode('Username-Password-Authentication', 'UTF-8');
        
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        List<String> sendTo = new List <String>{'bizplatforms@tenable.com'};
            
            Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        //req.setHeader('Authorization',authorizationHeader);       
        req.setEndpoint(endpoint);
        req.setMethod('POST');
        req.setTimeout(60000);
        req.setBody('?' + jsonString);
        
        HttpResponse response = h.send(req);
        
        if (response.getStatusCode() != 200) 
        {                                        
            // send email if there is an error
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setReplyTo('bizplatforms@tenable.com');
            mail.setSenderDisplayName('Salesforce Automation');
            mail.setSubject('Error Sending Email To the User..');
            String body = 'There was an error sending the Auth0 email to the user: .  Response Code: ' + response.getStatusCode();
            mail.setToAddresses(sendTo);
            mail.setHtmlBody(body);
            mails.add(mail);
            try
            {
                if(!test.isRunningTest())
                {
                    Messaging.sendEmail(mails);
                }
            }
            catch(Exception e)
            {
                system.debug('@@ exception sending email: '+ e.getMessage());
            }
        }
        
        return response;
    }
    
    @future (callout=true)
    public static void futurePostUserInfo(String UserName)
    {
        postUserName(UserName);
    }
    
    //Old forgotpassword method
    /*public static String forgotPassword(String username, String checkEmailUrl) 
    {
    try {
    Site.forgotPassword(username);
    ApexPages.PageReference checkEmailRef = new PageReference(checkEmailUrl);
    if(!Site.isValidUsername(username)) {
    return Label.Site.invalid_email;
    return null;
    }
    //aura.redirect(checkEmailRef);
    return null;
    }
    catch (Exception ex) {
    return ex.getMessage();
    }
    }*/
}