global class BatchTerritoryAssignmentNoGeoDebug implements Database.Batchable<sObject> {
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([SELECT Id FROM Account WHERE Name LIKE 'CDF Terr%' AND TerritoryGeographic__c = null AND (DandBCompanyId != null OR BillingCountry != null OR BillingState != null OR          
        BillingPostalCode != null OR ShippingCountry != null OR ShippingState != null OR ShippingPostalCode != null) ORDER BY BillingCountry]);
    }
    
    global void execute(Database.BatchableContext bc, List<Account> accounts) {

        TerritoryAssignment assignment = new TerritoryAssignment();
        
        assignment.start();
        assignment.setTerritoryAssignmentPreview(accounts, false);
        assignment.finish();
        
        assignment.start();
        assignment.setTerritoryAssignment(accounts, false);
        assignment.finish();
         
    }
    
    global void finish(Database.BatchableContext bc) {}
    
}