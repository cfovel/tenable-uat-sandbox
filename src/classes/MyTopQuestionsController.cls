public without sharing class MyTopQuestionsController {
    
    @AuraEnabled
    public static List<MyTopQuestionsWrapper> getMyTopQuestions(Integer numberOfQuestions) {
        List<MyTopQuestionsWrapper> topWrappersList = new List<MyTopQuestionsWrapper>();
        Map<Id, TopicAssignment> topicAssignmentMap = new Map<Id, TopicAssignment>();
        
        system.debug('numberOfQuestions value: ' + numberOfQuestions);
        
        String userId = UserInfo.getUserId();

        system.debug('USER ID: ' + userId);

        for(FeedItem fi: [SELECT Id, CreatedById, CreatedDate, Type, Title, Body FROM FeedItem WHERE CreatedById = :userId AND Type = 'QuestionPost' ORDER BY CreatedDate ASC LIMIT :numberOfQuestions]){
            Integer i = 0;
            
            system.debug('RETURNING QUESTIONS:' + fi);
            
            // Check to see if this is the current User's Question and only display the first 5.
            if(i < numberOfQuestions){
                MyTopQuestionsWrapper tempTopWrapper = new MyTopQuestionsWrapper();

                tempTopWrapper.tTitle = fi.Title;
                tempTopWrapper.tBody = fi.Body;
                tempTopWrapper.tType = 'Question';
                tempTopWrapper.tRecordId = fi.Id;
                tempTopWrapper.tDate = Date.newinstance(fi.CreatedDate.year(), fi.CreatedDate.month(), fi.CreatedDate.day());
                
                topWrappersList.add(tempTopWrapper);
                
                i++;
            }
        }
        
        system.debug('topWrappersList first value: ' + topWrappersList);
        
        return topWrappersList;
    }
}