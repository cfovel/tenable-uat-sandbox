public with sharing class auditLogController {

	/*****************************************************************************************
	*	Constant Declaration
	******************************************************************************************/
/*
	public static Boolean AuditEnabled = FALSE;
	public static Boolean ignoreUser = FALSE;
	public static String AuditIgnoreUserIds = '';
	public static String AuditIgnoreProfileIds = '';
	public Map <String, AuditSettings__c> appsettings = AuditSettings__c.getAll();
	
	public auditLogController() {
		AuditEnabled = appsettings.get('AuditLogging').Enabled__c;
		AuditIgnoreUserIds = appsettings.get('AuditIgnoreUserIds').Ids__c;
		AuditIgnoreProfileIds = appsettings.get('AuditIgnoreProfileIds').Ids__c;
		if ((AuditIgnoreProfileIds != null) &&
			(AuditIgnoreProfileIds.contains(String.valueof(UserInfo.getProfileId()).substring(0,15)))) {
			ignoreUser = TRUE;
		}
		if ((AuditIgnoreUserIds != null) &&
			(AuditIgnoreUserIds.contains(String.valueof(UserInfo.getUserId()).substring(0,15)))) {
			ignoreUser = TRUE;
		}
	}
	
	
	public PageReference saveAuditRecord() {
		Audit_Log__c[] newrecstoAdd = new Audit_Log__c[0];
		if ((AuditEnabled) && (!ignoreUser)) {
			newrecstoAdd.add(
				new Audit_Log__c(IP_Address__c = getipAddress(), 
								Page_Viewed__c = getReferer(), 
								Browser__c = getuserAgentInfo(),
								Object__c = getObject(),
								Record_Id__c = getRecordId(),
								User__c = Userinfo.getUserId()));
			if (newrecstoAdd.size() > 0) {
				insert newrecstoAdd;
			}
		}
		return null;
	}
	
	public String getuserAgentinfo() {
	    String userAgent =
	    ApexPages.currentPage().getHeaders().get('USER-AGENT');
	    return userAgent;
	}
	
	public String getipAddress() {
	
	    String ipAddress;
	    Map <String, String> mapHeaders = ApexPages.currentPage().getHeaders();  
	    
	    if(mapHeaders != null) {
	        ipAddress = mapHeaders.get('True-Client-IP');
	        if (ipAddress == null) {
	            ipAddress = mapHeaders.get('X-Salesforce-SIP');
	        }
	    }
	    return ipAddress; 
	}
	
	public String getReferer() {
	    return ApexPages.currentPage().getHeaders().get('referer');
	}

	public Id getRecordId() {
		return ApexPages.currentPage().getParameters().get('Id');
	}

	public String getObject() {
		String stype = '';
		if (getRecordId() != null) {
			stype = getRecordId().getSObjectType().getDescribe().getName();
		}
		return stype;
	}
*/
}