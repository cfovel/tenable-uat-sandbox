global class BatchTerritoryAttributionAssignment implements Database.Batchable<sObject>, Database.Stateful {
    
    global final DateTime now;
    global Set<Id> territoryIds;
    
    global BatchTerritoryAttributionAssignment() {
        now = DateTime.now();
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        territoryIds = new Map<Id, Territory__c>([SELECT Id FROM Territory__c WHERE ChangeAttributionAssignment__c = true]).keySet();
        return Database.getQueryLocator([SELECT Id FROM TerritoryAssignment__c WHERE Territory__c IN :territoryIds AND (EndAt__c = null OR EndAt__c > :now)]);
    }
    
    global void execute(Database.BatchableContext bc, List<TerritoryAssignment__c> territoryAssignments) {
        
        Map<Id, TerritoryAssignment__c> territoryAssignmentMap = new Map<Id, TerritoryAssignment__c>([SELECT Id, RecordTypeId, TerritoryAssignmentRule__c, OverrideAssignment__c, Account__c, Territory__c, OwnerId, JobFunction__c, StartAt__c, EndAt__c, Preview__c, TerritoryName__c, TerritoryOwner__c, RegionName__c, RegionOwner__c, AreaName__c, AreaOwner__c, TheaterName__c, TheaterOwner__c, SegmentName__c, Territory__r.Name, Territory__r.OwnerId, Territory__r.RegionName__c, Territory__r.RegionOwner__c, Territory__r.AreaName__c, Territory__r.AreaOwner__c, Territory__r.TheaterName__c, Territory__r.TheaterOwner__c, Territory__r.SegmentName__c FROM TerritoryAssignment__c WHERE Id IN :territoryAssignments]);
        
        List<TerritoryAssignment__c> assignmentsToInsert = new List<TerritoryAssignment__c>();
        List<TerritoryAssignment__c> assignmentsToUpdate = new List<TerritoryAssignment__c>();
        List<TerritoryAssignment__c> assignmentsToDelete = new List<TerritoryAssignment__c>();

        TerritoryAssignment__c thisTerritoryAssignment = null;
        
        if (!territoryAssignments.isEmpty()) {
            for (TerritoryAssignment__c territoryAssignment : territoryAssignments) {
                thisTerritoryAssignment = territoryAssignmentMap.get(territoryAssignment.Id);
                if (thisTerritoryAssignment.TerritoryName__c != thisTerritoryAssignment.Territory__r.Name || thisTerritoryAssignment.TerritoryOwner__c != thisTerritoryAssignment.Territory__r.OwnerId || thisTerritoryAssignment.RegionName__c != thisTerritoryAssignment.Territory__r.RegionName__c || thisTerritoryAssignment.RegionOwner__c != thisTerritoryAssignment.Territory__r.RegionOwner__c || thisTerritoryAssignment.AreaName__c != thisTerritoryAssignment.Territory__r.AreaName__c || thisTerritoryAssignment.AreaOwner__c != thisTerritoryAssignment.Territory__r.AreaOwner__c || thisTerritoryAssignment.TheaterName__c != thisTerritoryAssignment.Territory__r.TheaterName__c || thisTerritoryAssignment.TheaterOwner__c != thisTerritoryAssignment.Territory__r.TheaterOwner__c || thisTerritoryAssignment.SegmentName__c != thisTerritoryAssignment.Territory__r.SegmentName__c) {
                    assignmentsToInsert.add(new TerritoryAssignment__c(
                        RecordTypeId = thisTerritoryAssignment.RecordTypeId,
                        TerritoryAssignmentRule__c = thisTerritoryAssignment.TerritoryAssignmentRule__c,
                        OverrideAssignment__c = thisTerritoryAssignment.OverrideAssignment__c,
                        Account__c = thisTerritoryAssignment.Account__c,
                        Territory__c = thisTerritoryAssignment.Territory__c,
                        OwnerId = thisTerritoryAssignment.OwnerId,
                        StartAt__c = (thisTerritoryAssignment.Preview__c) ? null : (thisTerritoryAssignment.StartAt__c == null || thisTerritoryAssignment.StartAt__c <= now) ? now : thisTerritoryAssignment.StartAt__c,
                        EndAt__c = (thisTerritoryAssignment.Preview__c) ? null : thisTerritoryAssignment.EndAt__c,
                        Preview__c = thisTerritoryAssignment.Preview__c
                    ));
                    if (thisTerritoryAssignment.Preview__c) {
                        assignmentsToDelete.add(territoryAssignment);
                    } else {
                        assignmentsToUpdate.add(new TerritoryAssignment__c(
                            Id = territoryAssignment.Id,
                            EndAt__c = now
                        ));
                        
                    }
                }
            }

            if (!assignmentsToDelete.isEmpty())
                delete assignmentsToDelete;

            if (!assignmentsToUpdate.isEmpty())
                update assignmentsToUpdate;
            
            if (!assignmentsToInsert.isEmpty())
                insert assignmentsToInsert;

        }
         
    }
    
    global void finish(Database.BatchableContext bc) {
        
        if (territoryIds != null && !territoryIds.isEmpty()) {
            
            List<Territory__c> territoriesUpdated = new List<Territory__c>();
            for (Id territoryId : territoryIds) {
                territoriesUpdated.add(new Territory__c(
                    Id = territoryId,
                    ChangeAttributionAssignment__c = false
                ));
            }
            if (!territoriesUpdated.isEmpty())
                update territoriesUpdated;
            
        }
        
    }
  
}