@isTest
public class CommunitySearch_Test {

    static testMethod void test_getCommunityId() {
        System.assertNotEquals('RandomString', CommunitySearch.getCommunityId());
    }
    
    static testMethod void test_getToken(){
	
        Test.StartTest();
        
        String token = CommunitySearch.getToken();
        System.AssertEquals(null, token);

        Test.StopTest();
    }
}