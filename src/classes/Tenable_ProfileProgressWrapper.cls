/**
 * Created by lucassoderstrum on 6/5/18.
 */

public with sharing class Tenable_ProfileProgressWrapper {

    @auraEnabled
    public Boolean completedWelcome {get; set;}
    @auraEnabled
    public Boolean completedProfile {get; set;}
    @auraEnabled
    public Boolean completedNotifications {get; set;}
    @auraEnabled
    public Boolean completedTopics {get; set;}
    @auraEnabled
    public Boolean completedGroups {get; set;}
    @auraEnabled
    public Boolean completedTour{get; set;}  //

    public Tenable_ProfileProgressWrapper(Boolean completedWelcome, Boolean completedProfile, Boolean completedNotifications,
                                        Boolean completedTopics, Boolean completedGroups, Boolean completedTour){
        this.completedWelcome = completedWelcome;
        this.completedProfile = completedProfile;
        this.completedNotifications = completedNotifications;
        this.completedTopics = completedTopics;
        this.completedGroups = completedGroups;
        this.completedTour = completedTour;
    }


}