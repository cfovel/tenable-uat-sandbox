@isTest
private class ActivityUtilsTest {
 
    static testMethod void mainTest() {
    	Utils.isTest = true;
    	User u = 
    		([SELECT ContactId, Contact.AccountId FROM User 
    		WHERE IsActive=TRUE 
    		AND Profile.Name ='Tenable - ISR User' limit 1]);
 
        Contact c = new Contact();
        c.FirstName = 'Joe';
        c.LastName = 'Smith';
        c.Email = 'test@test.com';
        insert c;

        Lead l = new Lead();
        l.LeadSource = 'Other';
        l.FirstName = 'Joe';
        l.LastName = 'Smith';
        l.Industry = 'Other';
        l.Status = 'New Lead';
        l.Company = 'Test Co';
        insert l;
 
        Task[] tList = new list<task>();
        Event[] eList = new list<event>();
        for(Integer i=0; i<2; i++) {
            Task t = new Task();
            t.Status = 'Not Started';
            t.Priority = 'Normal';
            t.Type = 'Scheduled Call Back';
            if(i==0) t.WhoId = c.Id;
            if(i==1) t.WhoId = l.Id;
            tList.add(t);
 
            Event e = new Event();
            e.StartDateTime = DateTime.now() + 7;
            e.EndDateTime = DateTime.now() + 14;
            if(i==0) e.WhoId = c.Id;
            if(i==1) e.WhoId = l.Id;
            eList.add(e);
        }
        System.runAs(u) {
	        insert tList;
	        insert eList;
        
        

			system.assertEquals(2, [SELECT Activity_Counter_Open__c FROM Contact WHERE Id = :c.Id].Activity_Counter_Open__c);
        	system.assertEquals(2, [SELECT Activity_Counter_Open__c FROM Lead WHERE Id = :l.Id].Activity_Counter_Open__c);
 
        	//Delete some activities and run assertions again
           	delete tList;
            system.assertEquals(1, [SELECT Activity_Counter_Open__c FROM Contact WHERE Id = :c.Id].Activity_Counter_Open__c);
            system.assertEquals(1, [SELECT Activity_Counter_Open__c FROM Lead WHERE Id = :l.Id].Activity_Counter_Open__c);
        }    
 
    }
 
}