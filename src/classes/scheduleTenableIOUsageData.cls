global class scheduleTenableIOUsageData implements Schedulable {
   global void execute(SchedulableContext sc) {
      BatchUpdateTenableioUsageLog  batch = new BatchUpdateTenableioUsageLog ('');
	  database.executeBatch(batch, 150);
   }
}

/*
scheduleTenableIOUsageDate m = new scheduleTenableIOUsageDate();
String sch = '0 0 23 * * ?';
String jobID = system.schedule('Tenable.io Usage Data', sch, m);
*/