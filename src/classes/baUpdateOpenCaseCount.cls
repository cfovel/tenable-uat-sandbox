global class baUpdateOpenCaseCount implements Database.Batchable<SObject> {
	// To run, login as Automation User, open Execute Anonymous window, and execut:
	//		database.executeBatch(new baUpdateOpenCaseCount(),200);

    global Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator([select Id, Open_Case_Count__c from Account order by Name]);
    }

    global void execute(Database.BatchableContext context, List<Account> scope) {
        Account[] updates = new Account[] {};
        Map<Id, Account> acctMap = new Map<Id, Account>();
        for (Account acct : scope)
        {
        	acctMap.put(acct.Id,acct);
        }
        for (AggregateResult ar : [
                select AccountId a, count(Id) c
                from Case
                where AccountId in :scope AND isClosed <> true
                group by AccountId
                ]) {
            Id acctId = (Id) ar.get('a');    	
            Account acct = new Account(
                    Id = acctId,
                    Open_Case_Count__c = (Decimal) ar.get('c')
                    );
            Account oldAcct = acctMap.get(acctId);
            if (oldAcct.Open_Case_Count__c != acct.Open_Case_Count__c)
            {            	
	            updates.add(new Account(
	                    Id = (Id) ar.get('a'),
	                    Open_Case_Count__c = (Decimal) ar.get('c')
	                    ));
	            system.debug('@@@ ar: ' + ar);  
            }      
        }
        update updates;
    }

    global void finish(Database.BatchableContext context) {
    }
}