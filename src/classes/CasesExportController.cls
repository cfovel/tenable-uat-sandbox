public class CasesExportController {
    @AuraEnabled
    public static list<Case> getOpenCases() {
    	Id standardCaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Standard Case').getRecordTypeId();
    	
    	system.debug('standardCaseRecordTypeId value: ' + standardCaseRecordTypeId);
    	
        list<Case> myOpenCasesList = [select CaseNumber, Priority, Status, Subject, CreatedDate, Contact_Name__c
	        							from Case 
	        							where IsClosed = false and RecordTypeId = :standardCaseRecordTypeId
	        							limit 1000];
	    
	    system.debug('myOpenCasesList value: ' + myOpenCasesList);
        
        return myOpenCasesList;
    }
    
    @AuraEnabled
    public static list<Case> getClosedCases() {
    	Id standardCaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Standard Case').getRecordTypeId();
    	
    	system.debug('standardCaseRecordTypeId value: ' + standardCaseRecordTypeId);
    	
        list<Case> myClosedCasesList = [select CaseNumber, Priority, Status, Subject, CreatedDate, Contact_Name__c
		        							from Case 
		        							where IsClosed = true and RecordTypeId = :standardCaseRecordTypeId
		        							limit 1000];
        
        System.debug('myClosedCasesList value: ' + myClosedCasesList);
        
        return myClosedCasesList;
    }
}