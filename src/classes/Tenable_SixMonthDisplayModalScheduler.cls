global without sharing class Tenable_SixMonthDisplayModalScheduler implements Schedulable  {
    
    global void execute(SchedulableContext sc)
    {
        Tenable_SixMonthDisplayModalBatch batchClass = new Tenable_SixMonthDisplayModalBatch();
        //Parameters of ExecuteBatch(context,BatchSize)
        database.executebatch(batchClass,200);   //
    }
}