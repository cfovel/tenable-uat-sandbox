@isTest
private class mulesoftIntegrationControllerTests {
    @isTest
    static void testCreateVendorNoSubsid() 
    {
        Utils.isTest = true;
        accountTriggerHandler.enforceCountry = false;
        Id RecordTypeIdVendor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor Account').getRecordTypeId();
            
        Account a = new Account(Name='Test1 Vendor', recordtypeid=RecordTypeIdVendor, BillingState='CA', BillingCountry='US',  IsExcludedFromRealign = true);         // Create a test account
        insert a;
        
        PageReference pref = Page.CreateNetSuiteVendor;
        pref.getParameters().put('id', a.id);
        Test.setCurrentPage(pref);
    
        ApexPages.StandardController sc = new ApexPages.StandardController(a);
        
        Test.startTest();
        
        // Set mock callout class                                                       
        Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
        
        CreateNetSuiteVendor cv = new CreateNetSuiteVendor(sc);
        PageReference result = cv.CreateVendorInNetSuite();
        
        Test.stopTest();
        System.assertEquals(null, result);
        
    }    
    @isTest
    static void testCreateVendorAlreadyExists() 
    {
        Utils.isTest = true;
        accountTriggerHandler.enforceCountry = false;
        Id RecordTypeIdVendor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor Account').getRecordTypeId();
            
        Account a = new Account(Name='Test1 Vendor', recordtypeid=RecordTypeIdVendor, BillingState='CA', BillingCountry='US', NetSuite_Id__c = '12345', IsExcludedFromRealign = true);         // Create a test account
        insert a;
        
        PageReference pref = Page.CreateNetSuiteVendor;
        pref.getParameters().put('id', a.id);
        Test.setCurrentPage(pref);
    
        ApexPages.StandardController sc = new ApexPages.StandardController(a);
        
        Test.startTest();
        
        // Set mock callout class                                                       
        Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
        
        CreateNetSuiteVendor cv = new CreateNetSuiteVendor(sc);
        PageReference result = cv.CreateVendorInNetSuite();

        Test.stopTest();
        System.assertEquals(null, result);
        
    }    
    @isTest
    static void testCreateVendor() 
    {
        Utils.isTest = true;
        accountTriggerHandler.enforceCountry = false;
        Id RecordTypeIdVendor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor Account').getRecordTypeId();
            
        Account a = new Account(Name='Test1 Vendor', recordtypeid=RecordTypeIdVendor, BillingState='CA', BillingCountry='US',  
                    Subsidiary__c = 'US (Tenable Network Security, Inc.)', Local_Currency__c = 'United States Dollars (USD)', IsExcludedFromRealign = true);         // Create a test account
        insert a;
        
        PageReference pref = Page.CreateNetSuiteVendor;
        pref.getParameters().put('id', a.id);
        Test.setCurrentPage(pref);
    
        ApexPages.StandardController sc = new ApexPages.StandardController(a);
        
        Test.startTest();
        
        // Set mock callout class                                                       
        Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
        
        CreateNetSuiteVendor cv = new CreateNetSuiteVendor(sc);
        PageReference result = cv.CreateVendorInNetSuite();
        Test.stopTest();
        
        System.assertNotEquals(null, result); 
        
    }    
    
    @isTest
    static void testCreatePO() 
    {
        Utils.isTest = true;
        accountTriggerHandler.enforceCountry = false;
        Id RecordTypeIdVendorAcct = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor Account').getRecordTypeId();
        Id RecordTypeIdVendorCon = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Vendor Contract').getRecordTypeId();
            
        Account a = new Account(Name='Test1 Vendor', recordtypeid=RecordTypeIdVendorAcct, BillingState='CA', BillingCountry='US',  
                    Subsidiary__c = 'US (Tenable Network Security, Inc.)', Local_Currency__c = 'United States Dollars (USD)', IsExcludedFromRealign = true);         // Create a test account
        insert a;
        
        Contract c = new Contract(Name='Test1 Contract', recordtypeid=RecordTypeIdVendorCon, AccountId = a.Id, Description ='Test', 
                Local_Currency__c = 'United States Dollars (USD)', Renew_or_extend_approved_contract__c = 'Yes', Expense_already_included_in_budget__c = 'Yes',
                Vendor_Contract_Uploaded__c = 'Yes', New_Vendor_Form_Uploaded__c = 'Yes',   Start_Date__c = Date.today(),   End_Date__c = Date.today().addYears(1),
                Status = 'Signed', Receiving_Employee__c = UserInfo.getUserId(), Director_Approver__c = UserInfo.getUserId(), Date_Needed__c = Date.today(), Contract_Value__c = 10000, Payment_Method__c = 'ACH' );         
        insert c;
        
        Purchase_Order_Item__c p = new Purchase_Order_Item__c(Contract__c = c.Id, Department__c = 'Sales', Item_Category__c = 'Mktg Ads - Digital', Qty__c = 1, Description__c = 'Test',
                                        Price_Unit__c = 100);
        insert p;                               
        
        PageReference pref = Page.CreateNetSuitePO;
        pref.getParameters().put('id', c.id);
        Test.setCurrentPage(pref);
    
        ApexPages.StandardController sc = new ApexPages.StandardController(c);
        
        Test.startTest();
        
        // Set mock callout class                                                       
        Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
        
        CreateNetSuitePO po = new CreateNetSuitePO(sc);
        PageReference result = po.CreatePOInNetSuite();
        Test.stopTest();
        
        System.assertNotEquals(null, result); 
        
    }    
    
    @isTest
    static void testAccountSync() 
    {
        Utils.isTest = true;
        accountTriggerHandler.enforceCountry = false;
        Id RecordTypeIdStandardAcct = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Account').getRecordTypeId();
            
        Account a = new Account(Name='Test1 Co.', recordtypeid=RecordTypeIdStandardAcct, BillingStreet = '123 Main St', BillingCity = 'Los Angeles', BillingState='CA', BillingPostalCode = '90210', BillingCountry='US', Type = 'Customer', Phone = '888-888-8888', 
                    Subsidiary__c = 'Tenable, Inc.', Local_Currency__c = 'United States Dollars (USD)', IsExcludedFromRealign = true);         // Create a test account
        insert a;
        
        
        Account a1 = new Account(Name='Test1', recordtypeid=RecordTypeIdStandardAcct, BillingStreet = '123 Main St', BillingCity = 'Los Angeles', BillingState='CA', BillingPostalCode = '90210', BillingCountry='US', Type = 'Customer', Phone = '888-888-8888', 
                    Subsidiary__c = 'Tenable, Inc.', Local_Currency__c = 'United States Dollars (USD)', IsExcludedFromRealign = true);         // Create a test account
        
        insert a1;
        
        Account a2 = new Account(Name='Test2', recordtypeid=RecordTypeIdStandardAcct, BillingStreet = '123 Main St', BillingCity = 'Los Angeles', BillingState='CA', BillingPostalCode = '90210', BillingCountry='US', Type = 'Customer', Phone = '888-888-8888', 
                    Subsidiary__c = 'Tenable, Inc.', Local_Currency__c = 'United States Dollars (USD)', IsExcludedFromRealign = true);         // Create a test account
        
        insert a2;
                            
        Contact c = new Contact(lastname='test', accountid=a.id, email='test@test.com', MailingCountry='US', MailingState='CA'); 
        insert c;
        
        Contact c1 = new Contact(lastname='test2', accountid=a2.id, email='test2@test.com', MailingCountry='US', MailingState='CA'); 
        insert c1;
        
        PageReference pref = Page.AccountFindLMS;
        pref.getParameters().put('id', a.id);
        Test.setCurrentPage(pref);
    
        ApexPages.StandardController sc = new ApexPages.StandardController(a);
        AccountFindLMSController ext = new AccountFindLMSController(sc);
        
        Test.startTest();
        
        // Set mock callout class                                                       
        Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
        ext.Beginning();
        ext.End();
        ext.getDisabledPrevious();
        ext.getDisabledNext();
        PageReference result = ext.createCustId();
        //CreateNetSuiteVendor cv = new CreateNetSuiteVendor(sc);
        //PageReference result = cv.accountSyncLMSNetsuite();
        Test.stopTest();
        
        System.assertNotEquals(null, result); 
        
    }    
}