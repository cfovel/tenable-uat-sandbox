/**
 * This class contains unit tests for fundTriggerHandler
 */
@isTest(SeeAllData=true)
public with sharing class fundTriggerTest {
    /*
        For this first test, create an object for approval, then
        simulate rejeting the approval with an added comment for explanation.
        
        The rejection should be processed normally without being interrupted.
    */
    private static testmethod void testRejectionWithComment()
    {
        
        // Generate sample work item using utility method.
        Id testWorkItemId = generateAndSubmitObject();
	    Test.startTest();  
        	Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());    
	        // Reject the submitted request, providing a comment.
	        Approval.ProcessWorkitemRequest testRej = new Approval.ProcessWorkitemRequest();
	        
	        testRej.setComments('Rejecting request with a comment.');
	        testRej.setAction  ('Reject');
	        testRej.setWorkitemId(testWorkItemId);
	              
            // Process the rejection
            try 
            {
            	Approval.ProcessResult testRejResult =  Approval.process(testRej);
            	// Verify the rejection results
            	System.assert(testRejResult.isSuccess(), 'Rejections that include comments should be permitted');
        		System.assertEquals('Rejected', testRejResult.getInstanceStatus(), 
          				'Rejections that include comments should be successful and instance status should be Rejected');
            }
            Catch (DMLException e){}
        Test.stopTest();
        
        
        
    }
    
    /*
        For this test, create an object for approval, then reject the request, mark the approval status as pending, then
        without a comment explaining why. The rejection should be halted, and
        and an apex page message should be provided to the user.
    */
    private static testmethod void testRejectionWithoutComment()
    {
        
        // Generate sample work item using utility method.
        Id testWorkItemId = generateAndSubmitObject();
	   	Test.startTest();   
        	Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());     
	        // Reject the submitted request, without providing a comment.
	        Approval.ProcessWorkitemRequest testRej = new Approval.ProcessWorkitemRequest();
	        testRej.setComments('');
	        testRej.setAction  ('Reject');      
	        testRej.setWorkitemId(testWorkItemId);
             
            // Attempt to process the rejection
            try
            {
                Approval.ProcessResult testRejResult =  Approval.process(testRej);
                system.assert(false, 'A rejection with no comment should cause an exception');
            }
            catch(DMLException e)
            {
                system.assertEquals('PLEASE PROVIDE A REJECTION REASON IN THE COMMENTS SECTION.', 
                                    e.getDmlMessage(0), 
                  'error message should be PLEASE PROVIDE A REJECTION REASON IN THE COMMENTS SECTION.'); 
            }
        Test.stopTest();
    }
    
    /*
        When an approval is approved instead of rejected, a comment is not required, 
        mark the approval status as pending, then ensure that this functionality still holds together.
    */
    private static testmethod void testApprovalWithoutComment()
    {
        
        // Generate sample work item using utility method.
        Id testWorkItemId = generateAndSubmitObject();
        Test.startTest();  
        	Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());      
	    
	        // approve the submitted request, without providing a comment.
	        Approval.ProcessWorkitemRequest testApp = new Approval.ProcessWorkitemRequest();
	        testApp.setComments ('');
	        testApp.setAction   ('Approve');
	        testApp.setWorkitemId(testWorkItemId);
	    
	        // Process the approval
	        Approval.ProcessResult testAppResult =  Approval.process(testApp);
    	Test.stopTest();
        
        // Verify the approval results
        System.assert(testAppResult.isSuccess(), 
                     'Approvals that do not include comments should still be permitted');
        System.assertEquals('Pending', testAppResult.getInstanceStatus(), 
           'All approvals should be successful and result in an instance status of Pending');
    }
    
    /*
        Utility method for creating single object, and submitting for approval.
        
        The method should return the Id of the work item generated as a result of the submission.
    */
    private static Id generateAndSubmitObject()
    {
        /*User u = 
    		([SELECT ContactId, Contact.AccountId FROM User 
    		WHERE IsActive=TRUE 
    		AND Profile.Name ='Tenable - Partner Community User' 
    		AND Contact.AccountId <> null
    		AND Contact.Account.KimbleOne__BusinessUnitTradingEntity__c = null
    		LIMIT 1]);*/
    	/*	
    	Map<Id,Id> MDFUserAcctIdMap = new Map<Id,Id>();
    	Map<Id,Id> MDFUserAcctIdTeamMap = new Map<Id,Id>();
    	
    	// Get all Partner Community Users that have MDF Visibility for their assigned Account
    	for (PermissionSetAssignment psa : [SELECT AssigneeId, Assignee.ContactId, Assignee.Contact.AccountId, Assignee.Contact.Account.KimbleOne__BusinessUnitTradingEntity__c FROM PermissionSetAssignment WHERE PermissionSet.Label = 'PartnerCommunityMDFVisibility'
    										AND Assignee.IsActive=TRUE AND Assignee.Profile.Name ='Tenable - Partner Community User' AND Assignee.ManagerId <> null AND Assignee.Contact.AccountId <> null
    										AND Assignee.Channel_Marketing_Manager__c <> null AND Assignee.Contact.Account.KimbleOne__BusinessUnitTradingEntity__c = null])
    	{
    		MDFUserAcctIdMap.put(psa.AssigneeId, psa.Assignee.Contact.AccountId);
 
    	}
    	
    	// Get those users that have edit access on the account team
    	for(AccountTeamMember atm : [SELECT Id, AccountId, UserId, TeamMemberRole, AccountAccessLevel 
                    FROM AccountTeamMember WHERE AccountId In :MDFUserAcctIdMap.values() AND UserId IN :MDFUserAcctIdMap.keySet() AND AccountAccessLevel = 'Edit' AND isDeleted = false])  
        {
        	Id acctId = MDFUserAcctIdMap.get(atm.UserId);

    		if(acctId == atm.AccountId)
        	{
        		MDFUserAcctIdTeamMap.put(atm.UserId,atm.AccountId);
        	}
        }  

        if (!MDFUserAcctIdTeamMap.isEmpty())
        {          
    		Id userId = new List<id>(MDFUserAcctIdTeamMap.keySet())[0];
    		Id acctId = MDFUserAcctIdTeamMap.get(userId);	
    	
	    	Campaign c1 = new Campaign(name = 'TestCampaign', event_title__c='TestEvent', isActive = TRUE, type='Event', event_location__c='Test Location', event_state_province__c='CA', 
	            	startdate=System.today(), event_focus_country__c='US', Description='TestDesc', event_type__c='Session', Number_Of_Target_Leads__c=100, Sales_Area__c='AMER',
	            	Sponsorship_Type__c='Industry', Synopsis__c='Test synopsis'); 
			insert c1;	
	    		
	        MDF_Transaction__c fund = new MDF_Transaction__c(Total_Activity_Cost_in_USD__c = 1000.0, Status__c = 'Open', Prospect_Type__c = 'Commercial', 
									Prospect_Focus__c = 'New', Partner__c = acctId, Number_of_Leads_Target__c = 100.0, 
									Number_of_Leads_Actual__c = 20.0, Requested_Amount_in_USD__c = 200.0, Location__c = 'Anywhere', Local_Currency__c = 'United States Dollars (USD)',
									Industry_Focus__c = 'Test', Date__c = System.today(), Associated_Campaign__c = c1.Id,
									Activity_Type__c = 'Test', Activity_Category__c = 'Test');
	        insert fund;
	        Approval.ProcessSubmitRequest testReq = new Approval.ProcessSubmitRequest();
	        testReq.setObjectId(fund.Id);
	        Approval.ProcessResult reqResult = Approval.process(testReq);
	        
	        System.assert(reqResult.isSuccess(),'Unable to submit new fund record for approval');
	        
	        return reqResult.getNewWorkitemIds()[0];
        }
        
        return null;
        */
        User u = 
        ([SELECT ContactId, Contact.AccountId FROM User 
        WHERE IsActive=TRUE 
        AND Profile.Name ='Tenable - Partner Community User' 
        AND id = '00560000005F5NyAAK'
        //AND Theatre__c = 'APAC' 
            //AND ManagerId <> null
            //AND Contact.AccountId <> null
        //AND Contact.Account.KimbleOne__BusinessUnitTradingEntity__c = null
        LIMIT 1]);
      
      	Campaign c1 = new Campaign(name = 'TestCampaign', event_title__c='TestEvent', isActive = TRUE, type='Event', event_location__c='Test Location', event_state_province__c='CA', 
              startdate=System.today(), event_focus_country__c='US', Description='TestDesc', event_type__c='Session', Number_Of_Target_Leads__c=100, Sales_Area__c='AMER',
              Sponsorship_Type__c='Industry', Synopsis__c='Test synopsis'); 
    	insert c1;  
        
        MDF_Transaction__c fund = new MDF_Transaction__c(Total_Activity_Cost_in_USD__c = 1000.0, Status__c = 'Open', Prospect_Type__c = 'Commercial', 
                Prospect_Focus__c = 'New', Partner__c = u.Contact.AccountId, Number_of_Leads_Target__c = 100.0, 
                Number_of_Leads_Actual__c = 20.0, Requested_Amount_in_USD__c = 200.0, Location__c = 'Anywhere', Local_Currency__c = 'United States Dollars (USD)',
                Industry_Focus__c = 'Test', Date__c = System.today(), Associated_Campaign__c = c1.Id,
                Activity_Type__c = 'Test', Activity_Category__c = 'Test', Lead_Follow_Up_Consent__c = 'Yes');
        insert fund;

        Approval.ProcessResult reqResult;  
        Approval.ProcessSubmitRequest testReq;

            
        System.runAs(u)
        {
          testReq = new Approval.ProcessSubmitRequest();
          testReq.setObjectId(fund.Id);
          
        }  
        
        reqResult = Approval.process(testReq);
        
        return reqResult.getNewWorkitemIds()[0];
      
    }
}