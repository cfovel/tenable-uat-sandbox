global class ScheduleUpdateTerritory implements Schedulable {
    /*************************************************************************************
     * To run, login as Automation User, open Execute Anonymous window, and execute:     *
     *  ScheduleUpdateTerritory.scheduleTerritoryJob();                                    *
     *************************************************************************************/
    public static String sched = '0 00 5 * * ?';  //Every Day at 5 am (after data.com clean job, which runs at 3 am)

    global static String scheduleTerritoryJob() {
        ScheduleUpdateTerritory SC = new ScheduleUpdateTerritory(); 
        return System.schedule('Territory Update Job', sched, SC);
    }
    global void execute(SchedulableContext sc) {
        baUpdateTerritoryAssignments baTerritory = new baUpdateTerritoryAssignments(); 
        database.executebatch(baTerritory);
    }
}