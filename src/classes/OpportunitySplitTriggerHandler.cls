public with sharing class OpportunitySplitTriggerHandler
{
    static map<string,Territory__c> mapTerritoryInfo = new map<string,Territory__c>();
    public static Map<Id,Profile> adminProfiles = new Map<Id,Profile>([Select Id from Profile where name LIKE '%System Administrator%']);
     
    public static void GetTerritoryInfo()
    {
        for(Territory__c tr: [SELECT Id,Name,Active__c,Area__c,Region__c,Segment__c,Sub_Segment__c,Territory_Manager__c,Territory_Name__c,Theatre__c FROM Territory__c WHERE Active__c = TRUE])
        {
            if(!mapTerritoryInfo.containsKey(tr.Territory_Name__c))
            {
                mapTerritoryInfo.put(tr.Territory_Name__c,tr);
            }
        }           
    }
    
    public class OpportunitySplitBeforeInsertHandler implements Triggers.Handler
    {
        public void handle() 
        {
            UpdateTerritoryData(Trigger.new);
        }
    } 
    
    public class OpportunitySplitBeforeUpdateHandler implements Triggers.Handler
    {
        public void handle() 
        {
            UpdateTerritoryData(Trigger.new);
        }
    } 
    
    public static void UpdateTerritoryData(List<OpportunitySplit> newOppSplits)
    {        
        //if(!utils.HasRunOpportunitySplitTriggerHandler)
        //{
            if (mapTerritoryInfo.isEmpty())
            {
                GetTerritoryInfo();
            }

            system.debug('@@@ Inside Oppsplit Trigger Handler');
            for(OpportunitySplit os: newOppSplits)
            {
                if (os.SplitOwnerID != os.Opportunity_Owner__c)
                {
                    if(os.Territory__c != NULL)
                    {
                        if(!mapTerritoryInfo.isEmpty())
                        {
                            if(mapTerritoryInfo.containsKey(os.Territory__c))
                            {
                                Territory__c tr = mapTerritoryInfo.get(os.Territory__c);
                                
                                os.Area__c = tr.Area__c;
                                os.Region__c = tr.Region__c;
                                os.Theater__c = tr.Theatre__c;
                                os.Segment__c = tr.Segment__c;
                                os.Sub_Segment__c = tr.Sub_Segment__c;
                            }
                            else if (!adminProfiles.keyset().contains(Userinfo.getProfileId()))
                            {
                                os.Territory__c.addError('Territory does not exist on Territory Table');
                            }
                        }
                    }
                    else
                    {
                        os.Area__c = null;
                        os.Region__c = null;
                        os.Theater__c = null;
                        os.Segment__c = null;
                        os.Sub_Segment__c = null;
                    }
                }
            }
            //utils.HasRunOpportunitySplitTriggerHandler = true;
        //}
    }
}