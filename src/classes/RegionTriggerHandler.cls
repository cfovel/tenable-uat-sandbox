public with sharing class RegionTriggerHandler
{
	//public static User automationUser = [SELECT Id FROM User WHERE ((FirstName = 'Automation' AND LastName = 'User') OR LastName = 'Automation User') AND IsActive = true LIMIT 1];
    //public static Map<Id, Segment__c> segments = new Map<Id, Segment__c>([SELECT Id, Name FROM Segment__c]);
    //public static Map<Id, Area__c> areas = new Map<Id, Area__c>([SELECT Id, Name, AreaOwner__c, TheaterName__c, TheaterOwner__c FROM Area__c]);
        
    public class RegionBeforeInsertHandler implements Triggers.Handler
    {
        public void handle() 
        {
            UpdateRegionData(Trigger.new);
        }
    } 
    
    public class RegionBeforeUpdateHandler implements Triggers.Handler
    {
        public void handle() 
        {
            UpdateRegionData(Trigger.new);
        }
    } 
    
    public class RegionAfterInsertHandler implements Triggers.Handler
    {
        public void handle() 
        {
            UpdateTerritoryData(Trigger.new);
        }
    } 
    
    public class RegionAfterUpdateHandler implements Triggers.Handler
    {
        public void handle() 
        {
            UpdateTerritoryData(Trigger.new);
        }
    } 
    
    public static void UpdateRegionData(List<Region__c> newRegions)
    {
        User automationUser = [SELECT Id FROM User WHERE ((FirstName = 'Automation' AND LastName = 'User') OR LastName = 'Automation User') AND IsActive = true LIMIT 1];
        Map<Id, Segment__c> segments = new Map<Id, Segment__c>([SELECT Id, Name FROM Segment__c]);
        Map<Id, Area__c> areas = new Map<Id, Area__c>([SELECT Id, Name, AreaOwner__c, TheaterName__c, TheaterOwner__c FROM Area__c]);
        
        if (Trigger.isInsert) 
        {

            for (Region__c region : newRegions) 
            {
                region.OwnerId = automationUser.Id;
                region.AreaName__c = (region.Area__c == null) ? null : areas.get(region.Area__c).Name;
                region.AreaOwner__c = (region.Area__c == null) ? null : areas.get(region.Area__c).AreaOwner__c;
                region.TheaterName__c = (region.Area__c == null) ? null : areas.get(region.Area__c).TheaterName__c;
                region.TheaterOwner__c = (region.Area__c == null) ? null : areas.get(region.Area__c).TheaterOwner__c;
                region.SegmentName__c = (region.Segment__c == null) ? null : segments.get(region.Segment__c).Name;
            }

        } else if (Trigger.isUpdate) 
        {
            
            for (Region__c region : newRegions) 
            {
                Region__c oldRegion = (Region__c)Trigger.oldMap.get(region.Id);
                
                if (region.OwnerId != automationUser.Id) 
                {
                    region.OwnerId = automationUser.Id;
                }
                
                if (region.Area__c != oldRegion.Area__c || region.AreaName__c != oldRegion.AreaName__c || region.AreaOwner__c != oldRegion.AreaOwner__c || region.TheaterName__c != oldRegion.TheaterName__c || region.TheaterOwner__c != oldRegion.TheaterOwner__c) {
                    region.AreaName__c = (region.Area__c == null) ? null : areas.get(region.Area__c).Name;
                    region.AreaOwner__c = (region.Area__c == null) ? null : areas.get(region.Area__c).AreaOwner__c;
                    region.TheaterName__c = (region.Area__c == null) ? null : areas.get(region.Area__c).TheaterName__c;
                    region.TheaterOwner__c = (region.Area__c == null) ? null : areas.get(region.Area__c).TheaterOwner__c;
                }
                
                if (region.Segment__c != oldRegion.Segment__c || region.SegmentName__c != oldRegion.SegmentName__c) {
                    region.SegmentName__c = (region.Segment__c == null) ? null : segments.get(region.Segment__c).Name;
                }
            }            
        }
    }
    
    public static void UpdateTerritoryData(List<Region__c> newRegions)
    {
        Set<Id> ownerIds = new Set<Id>();
        
        if (Trigger.isInsert) {
            
            for (Region__c region : newRegions) {
                ownerIds.add(region.RegionOwner__c);
            }
            
        } else if (Trigger.isUpdate) {
            
            Map<Id, Region__c> regions = new Map<Id, Region__c>();
            
            List<Territory__c> territories = new List<Territory__c>();
            
            for (Region__c region : newRegions) {
                Region__c oldRegion = (Region__c)Trigger.oldMap.get(region.Id);
                
                if (region.RegionOwner__c != oldRegion.RegionOwner__c) {
                    ownerIds.add(region.RegionOwner__c);
                    ownerIds.add(oldRegion.RegionOwner__c);
                }
                
                if (region.Name != oldRegion.Name || region.RegionOwner__c != oldRegion.RegionOwner__c || region.AreaName__c != oldRegion.AreaName__c || region.AreaOwner__c != oldRegion.AreaOwner__c || region.TheaterName__c != oldRegion.TheaterName__c || region.TheaterOwner__c != oldRegion.TheaterOwner__c) {
                    regions.put(region.Id, null);
                }
                
            }
            
            regions = new Map<Id, Region__c>([SELECT Id, (SELECT Id, JobFunction__c FROM Territories__r) FROM Region__c WHERE Id IN :regions.keySet()]);
            
            for (Region__c region : regions.values()) {
                Region__c newRegion = (Region__c)Trigger.newMap.get(region.Id);
                
                for (Territory__c terr : region.Territories__r) {
                    territories.add(new Territory__c(
                        Id = terr.Id,
                        RegionName__c = newRegion.Name,
                        RegionOwner__c = newRegion.RegionOwner__c,
                        AreaName__c = newRegion.AreaName__c,
                        AreaOwner__c = newRegion.AreaOwner__c,
                        TheaterName__c = newRegion.TheaterName__c,
                        TheaterOwner__c = newRegion.TheaterOwner__c
                    ));
                }
                
            }
            
             if (!territories.isEmpty())
                update territories;
            
        }
        
        if (!ownerIds.isEmpty())
            TerritoryAssignment.setUserTerritoryFuture(ownerIds);
            
    }
}