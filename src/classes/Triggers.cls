public class Triggers {
/******************************************************************************
*
*	Standard Trigger Handler Classes
*	Copyright 2006-2014 (c) by CloudLogistix.com
*	All Rights Reserved, Modifications can only be made for your direct use and 
*	not for distribution without explicit permission from CloudLogistix.
*
********************************************************************************/

	public enum Evt {
		afterdelete, afterinsert, afterundelete,
		afterupdate, beforedelete, beforeinsert, beforeupdate	
	}
	public interface Handler {
		void handle();			
	} 
	
	Map<String, List<Handler>> eventHandlerMapping = new Map<String, List<Handler>>();
	
	public Triggers bind(Evt event, Handler eh) {
		List<Handler> handlers = eventHandlerMapping.get(event.name());
		if (handlers == null) {
			handlers = new List<Handler>();
			eventHandlerMapping.put(event.name(), handlers);
		}
		handlers.add(eh);
		return this;
	}
	
	public void manage() {
		Evt ev = null;
		if(Trigger.isInsert && Trigger.isBefore){
			ev = Evt.beforeinsert;
		} else if(Trigger.isInsert && Trigger.isAfter){
			ev = Evt.afterinsert;
		} else if(Trigger.isUpdate && Trigger.isBefore){
			ev = Evt.beforeupdate;
		} else if(Trigger.isUpdate && Trigger.isAfter){
			ev = Evt.afterupdate;
		} else if(Trigger.isDelete && Trigger.isBefore){
			ev = Evt.beforedelete;
		} else if(Trigger.isDelete && Trigger.isAfter){
			ev = Evt.afterdelete;
		} else if(Trigger.isundelete){
			ev = Evt.afterundelete;				
		}
		List<Handler> handlers = eventHandlerMapping.get(ev.name());
		if (handlers != null && !handlers.isEmpty()) {
			for (Handler h : handlers) {
				h.handle();
			}
		}
	}
}