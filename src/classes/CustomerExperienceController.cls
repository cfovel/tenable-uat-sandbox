public with sharing class CustomerExperienceController {
/******************************************************************************
*
*   Controller Extension for Customer Experience Dashboard
*
********************************************************************************/

    private Account acct;   
    public Boolean emptyProducts = TRUE;                                                              // Used to report back if we have no products
    public Boolean getProductsIsEmpty() {return emptyProducts;}                                        
    
    public CustomerExperienceController(ApexPages.StandardController stdController)
    {
        this.acct = (Account)stdController.getRecord();
    }
    
    /*****************************************************************************************
    *   Variable Initialization
    ******************************************************************************************/
    public List<ProductWrapper> svcTrngList {get; set;}

    public List<ProductWrapper> getSvcTrngProducts() {
        if (svcTrngList == null) {
            svcTrngList = new List<ProductWrapper>();
            for(Opportunity opp :[SELECT Id, (SELECT Opportunity.CloseDate, Quantity, Product2.Name FROM OpportunityLineItems WHERE Product2.Family IN ('Services', 'Training Credit'))
                    FROM Opportunity
                    WHERE AccountId = :acct.Id AND StageName = 'Closed - Won' Order By CloseDate desc]) 
            {
                for (OpportunityLineItem oli : opp.OpportunityLineItems)
				{    
                	ProductWrapper svcTrng = new ProductWrapper(oli);
                	svcTrngList.add(svcTrng);
				}
            }
        }
        if (svcTrngList != null && svcTrngList.size() > 0 ) 
        {
        	emptyProducts = false;
        }
        return svcTrngList;
    }
    
    public class ProductWrapper {
        public Date CloseDate {get; set;}
        public String ProductName {get; set;}
        public Decimal Quantity {get; set;}
        public Opportunity opp {get; set;}
        
        public ProductWrapper(OpportunityLineItem oli) {
			CloseDate = oli.Opportunity.CloseDate;
			ProductName = oli.Product2.Name;
			Quantity = oli.Quantity;
		}
    }
}