/*************************************************************************************
Class: testFeatureRequestControllers
Author: Christine E
Date: 08/30/2018
Details: This test class is for the all feature request controllers
***************************************************************************************/

@isTest
private class testFeatureRequestControllers {
    
    @testSetup
    static void SetupData(){
        utils.isTest = true;
        
        Test.startTest();
        
        Account a = new Account(Name='Dell SecureWorks', BillingState='CA', BillingCountry='US', website='http://www.test1.com');
        insert a;
        
        Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
        List<Feature_Request__c> lfr = new List<Feature_Request__c>();
        
        Feature_Request__c fr = new Feature_Request__c(Name = 'Test Feature Request 001',Status__c = 'Pending',Product_Group__c = 'Tenable.io Container Security',
            Business_Problem__c = 'Feature Request Unit Test'); 
        lfr.add(fr);
        
        fr = new Feature_Request__c(Name = 'Test Feature Request 002',Status__c = 'Pending',Product_Group__c = 'Tenable.io Platform',
            Business_Problem__c = 'Feature Request Unit Test 2');
        lfr.add(fr);
            
        fr = new Feature_Request__c(Name = 'Test Feature Request 003',Status__c = 'Pending',Product_Group__c = 'Security Center',
            Business_Problem__c = 'Feature Request Unit Test 3');
        lfr.add(fr);
            
        fr = new Feature_Request__c(Name = 'Test Feature Request 004',Status__c = 'Pending',Product_Group__c = 'Tenable.io Web Apps Scanning',
            Business_Problem__c = 'Feature Request Unit Test 4');
        lfr.add(fr);
        
        Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
        
        insert lfr;
        
        Feature_Request__c frObj = [SELECT Id FROM Feature_Request__c WHERE Name='Test Feature Request 001' LIMIT 1];
        Account_Feature_Request__c afr = new Account_Feature_Request__c(Account__c = a.Id, Feature_Request__c = frObj.Id);
        
        insert afr;
        
        Test.stopTest();
    }

    @isTest 
    static void testFeatureRequestAddToAccount() {
        Test.startTest();
        
        Account acct = [SELECT Id FROM Account WHERE Name='Dell SecureWorks' LIMIT 1];
        Feature_Request__c frObj = [SELECT Id FROM Feature_Request__c WHERE Name='Test Feature Request 001' LIMIT 1];

        PageReference pageRef = Page.FeatureRequestAddToAccount;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', frObj.id);
        ApexPages.currentPage().getParameters().put('acctId', acct.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(frObj);        
        FeatureRequestAddToAccountController ext = new FeatureRequestAddToAccountController(sc);
        ext.CreateAccountFeatureRequest();
        
        Test.stopTest();
    }
    
    @isTest
    static void testFeatureRequestTabController(){
        Test.startTest();
        
        Account acct = [SELECT Id FROM Account WHERE Name='Dell SecureWorks' LIMIT 1];
        List<Account_Feature_Request__c> lafr = [SELECT Id from Account_Feature_Request__c where Account__c = :acct.Id];
        
        PageReference pageRef = Page.FeatureRequestsTab;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', acct.Id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(new Account_Feature_Request__c());
        FeatureRequestsTabController frc = new FeatureRequestsTabController();
        FeatureRequestsTabController scext = new FeatureRequestsTabController(sc);
        scext.entry = new Feature_Request__c(Name='Test 001', Product_Group__c='Security Center',Category__c='Other');
        scext.search();
        
        ApexPages.StandardSetController setCon = new ApexPages.StandardSetController(lafr);
        setCon.setSelected(lafr);
        FeatureRequestsTabController ext = new FeatureRequestsTabController(setCon);
        
        ext.size = 10;
        System.assertEquals(10, ext.size);
        
        ext.searchText = 'Test';
        System.assertEquals('Test', ext.searchText);
        
        ext.search();
        ext.getRequests();
        ext.CreateNewFeatureRequest();
        
        Test.stopTest();
        
    }
    
    @isTest
    static void testAccountFeatureRequestCreateController(){
        Test.startTest();
        
        Account acct = [SELECT Id FROM Account WHERE Name='Dell SecureWorks' LIMIT 1];
        Feature_Request__c frObj = [SELECT Id FROM Feature_Request__c WHERE Name='Test Feature Request 002' LIMIT 1];

        PageReference pageRef = Page.AccountFeatureRequestCreate;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('acctId', acct.Id);
        ApexPages.currentPage().getParameters().put('frId', frObj.Id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(new Account_Feature_Request__c());        
        AccountFeatureRequestCreateController ext = new AccountFeatureRequestCreateController(sc);
        Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
        PageReference resSave = ext.save();
        System.assertNotEquals(null, resSave);
        
        PageReference resCancel = ext.cancel();
        System.assertNotEquals(null, resCancel);
        
        Test.stopTest();
    }
    
    @isTest
    static void testAccountFeatureRequestCreateWithoutAccountId(){
        Test.startTest();
        
        Account acct = [SELECT Id FROM Account WHERE Name='Dell SecureWorks' LIMIT 1];
        Feature_Request__c frObj = [SELECT Id FROM Feature_Request__c WHERE Name='Test Feature Request 001' LIMIT 1];

        PageReference pageRef = Page.AccountFeatureRequestCreate;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('frId', frObj.Id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(new Account_Feature_Request__c());        
        AccountFeatureRequestCreateController ext = new AccountFeatureRequestCreateController(sc);
        Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());

        PageReference resSave = ext.save();
        System.assertNotEquals(null, resSave);
        
        PageReference resCancel = ext.cancel();
        System.assertNotEquals(null, resCancel);
        
        Test.stopTest();
    }
    
    @isTest
    static void testFeatureRequestCreateController(){
        Test.startTest();
        
        Account acct = [SELECT Id FROM Account WHERE Name='Dell SecureWorks' LIMIT 1];
        
        PageReference pageRef = Page.FeatureRequestCreate;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('acctId', acct.Id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(new Feature_Request__c());        
        FeatureRequestCreateController ext = new FeatureRequestCreateController(sc);

        Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
        PageReference resSave = ext.save();
        System.assertNotEquals(null, resSave);
        
        PageReference resCancel = ext.cancel();
        System.assertNotEquals(null, resCancel);
        
        Test.stopTest();
    }
     
    @isTest
    static void testAccountFeatureRequestEditController(){
        Test.startTest();
        
        Account acct = [SELECT Id FROM Account WHERE Name='Dell SecureWorks' LIMIT 1];
        Feature_Request__c frObj = [SELECT Id FROM Feature_Request__c WHERE Name='Test Feature Request 001' LIMIT 1];
        Account_Feature_Request__c afrObj = [SELECT Id FROM Account_Feature_Request__c WHERE Feature_Request__c = :frObj.Id LIMIT 1];

        PageReference pageRef = Page.AccountFeatureRequestEdit;
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(afrObj);        
        AccountFeatureRequestEditController ext = new AccountFeatureRequestEditController(sc);
        
        Test.stopTest();
    }
}