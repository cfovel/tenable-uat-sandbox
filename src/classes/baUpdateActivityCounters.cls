global class baUpdateActivityCounters implements Database.Batchable<SObject> {
    // To run, login as Tenable System Admin, open Execute Anonymous window, and execute:
    //       database.executeBatch(new baUpdateActivityCounters('Contact'),200);  // for the Contacts
    // AND   database.executeBatch(new baUpdateActivityCounters('Lead'),200);  // for the Leads
            
    global String Query;
    global String objectType;
    global List<String> profileIds = new List<String>(); 
    
    global baUpdateActivityCounters(String objectType)
    {
        this.objectType = objectType;
        
        Query = 'SELECT Id, (SELECT Id, WhoId FROM Events WHERE CreatedBy.ProfileId IN :profileIds LIMIT 1), (SELECT Id, WhoId FROM Tasks WHERE CreatedBy.ProfileId IN :profileIds LIMIT 1) FROM ' + objectType + ' WHERE MM_Most_Recent_Campaign_Source_Date__c = THIS_YEAR';
        
        profileIds.add('00e600000017WAvAAM');                   // Tenable - ISR (with Linkedin) User
        profileIds.add('00e600000017VgKAAU');                   // Tenable - ISR User
        profileIds.add('00ef2000001BgwOAAS');                   // Tenable - Sales CSM User
        profileIds.add('00e60000000izjZAAQ');                   // Tenable - Sales Executive User
        profileIds.add('00e600000017Rc6AAE');                   // Tenable - Sales Management User
    }
    
    global Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator(Query);
    }

    global void execute(Database.BatchableContext context, List<sObject> scope) {
        
        List<sObject> eventsOrTasks = new List<sObject>();
        Map<Id,sObject> whoIdActivity = new Map<Id,sObject>();
        
        if (objectType == 'Contact')
        {    
            for (Contact s : (List<Contact>)scope)
            {
                if (!s.events.isEmpty()) 
                {
                    eventsOrTasks.add(s.events[0]);
                }
                else if (!s.tasks.isEmpty())
                {
                    eventsOrTasks.add(s.tasks[0]);
                }
            }
        }
        else if (objectType == 'Lead')
        {    
            for (Lead s : (List<Lead>)scope)
            {
                if (!s.events.isEmpty()) 
                {
                    eventsOrTasks.add(s.events[0]);
                }
                else if (!s.tasks.isEmpty())
                {
                    eventsOrTasks.add(s.tasks[0]);
                }
            }
        }
        
        ActivityUtils au = new ActivityUtils(eventsOrTasks);
        au.updateContactActivityCount();
        au.updateLeadActivityCount();

    }

    global void finish(Database.BatchableContext context) {
    }   
}