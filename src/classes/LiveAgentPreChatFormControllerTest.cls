/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class LiveAgentPreChatFormControllerTest {

    static testMethod void myUnitTest() {
      LiveAgentPreChatFormController ctrl = new LiveAgentPreChatFormController();
      String CustomerContactId = ctrl.CustomerContactId;

      // set the callout response mock
      Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
      // This flag is used by most of the code
      utils.isTest = true;
      //Account
	  Account a = new Account(name='Tenable Unit Test');
	  insert a;
      // Contact
      Contact c = TestHelper.createContact(a.Id, true);
      ctrl.CustomerContact = c;
	  // Community user
	  User u = TestHelper.getCommunityUser(c.Id, TestHelper.PortalType.CspLitePortal, true);
      
      Test.startTest();
      LiveAgentPreChatFormController.getCustomerInfo(c.Id);
      Test.stopTest();
      
    }
    
    static testMethod void externalLiveAgentTest() {
      ExternalLiveAgentPreChatFormController ctrl = new ExternalLiveAgentPreChatFormController();

      Test.startTest();
      String ctry = ctrl.countryToStateValuesMap;
      ctrl.refreshDependentPicklist();
      Test.stopTest();
      
    }
}