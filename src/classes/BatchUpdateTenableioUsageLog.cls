/*************************************************************************************
Class: BatchUpdateTenableioUsageLog
Author: Roy.J
Date: 03/08/2017
Details: This batch gets all the contacts that are updated in a day and 
pushes the data to the Tenable.io Usage Log object.  The push will be
based on the container UUID
***************************************************************************************/
global class BatchUpdateTenableioUsageLog implements Database.Batchable<sobject>/*, Schedulable*/, Database.AllowsCallouts, Database.Stateful
{
   /* public static string query = 'SELECT TIO_Assets_Scanned_90_Days__c, TIO_Assets_Scanned_Total__c, TIO_Assets_Total__c, TIO_License_Assets__c, ' +
                                 'TIO_License_Users__c, TIO_Users_Total__c, UUID__c, Account__r.TIO_License_Assets__c, ' +
                                 'Account__r.TIO_Assets_Scanned_Total__c, Account__r.TIO_Assets_Scanned_90_Days__c FROM ' +
                                 'Install_Base__c WHERE UUID__c != null' + (Test.isRunningTest() ? ' limit 49' : '');
    public static string query2 = 'SELECT TIO_Assets_Scanned_90_Days__c, TIO_Assets_Scanned_Total__c, TIO_Assets_Total__c, TIO_License_Assets__c, ' +
                                 'TIO_License_Users__c, TIO_Users_Total__c, container_uuid__c FROM ' +
                                 'Evaluation__c WHERE container_uuid__c != null' + (Test.isRunningTest() ? ' limit 49' : '');*/
    public list<usageLogStatistics> logStats {get; set;}
    public set<string> updateFieldSet {get; set;}
    public map<String, Schema.SObjectField> TenableioUsageLogFieldTypeMap = Schema.SObjectType.Tenable_io_Usage_Log__c.fields.getMap();
    public list<sObject> firstScope = new list<sObject>();
    public list<sObject> secondScope = new list<sObject>();
    private String usageDate;
    public map<Id, string> exceptionMap {get; set;}
    
    global BatchUpdateTenableioUsageLog(String runDate)
    {
        //query = query ;
        //query2 = query2;
        //system.debug('TTTTTTTTTTTTTTTTT: ' + runDate);
        //Query += '  WHERE SystemModstamp =: CurrentDate Contact WHERE LastModifiedDate =: CurrentDate' + (Test.isRunningTest() ? ' limit 49' : '');
        usageDate = runDate;
        System.debug('@@@@@@@@@@0: ' + usageDate);
    }
    
    
    global list<sObject> start(Database.BatchableContext bc)
    {
        
        if(!test.isRunningTest())
        {
            firstScope = [SELECT TIO_Assets_Scanned_90_Days__c, TIO_Assets_Scanned_Total__c, TIO_Assets_Total__c, 
                                        TIO_License_Assets__c, TIO_License_Users__c, TIO_Users_Total__c, UUID__c,  
                                        Account__r.TIO_License_Assets__c, Account__r.TIO_Assets_Scanned_Total__c, 
                                        Account__r.TIO_Assets_Scanned_90_Days__c FROM Install_Base__c WHERE UUID__c != null and End_Date__c > today];
            secondScope = [SELECT TIO_Assets_Scanned_90_Days__c, TIO_Assets_Scanned_Total__c, Expiration_Date__c, TIO_Assets_Total__c, 
                                    TIO_License_Assets__c, TIO_License_Users__c, TIO_Users_Total__c, container_uuid__c FROM 
                                    Evaluation__c WHERE (container_uuid__c != null) and ((Eval_Type__c = 'Tenable.io VM') or (Eval_Type__c = 'Tenable.io CONSEC;VM;WAS')) 
                           								and (Expiration_Date__c > today) and (Activation_DateTime__c != null)];
        }
        
        if(test.isRunningTest())
        {
            system.debug('Inside test.isrunning test');
            firstScope = [SELECT TIO_Assets_Scanned_90_Days__c, TIO_Assets_Scanned_Total__c, TIO_Assets_Total__c, 
                                        TIO_License_Assets__c, TIO_License_Users__c, TIO_Users_Total__c, UUID__c, 
                                        Account__r.TIO_License_Assets__c, Account__r.TIO_Assets_Scanned_Total__c, 
                                        Account__r.TIO_Assets_Scanned_90_Days__c FROM Install_Base__c WHERE UUID__c != null limit 5];
            secondScope = [SELECT TIO_Assets_Scanned_90_Days__c, TIO_Assets_Scanned_Total__c, TIO_Assets_Total__c, 
                                    TIO_License_Assets__c, TIO_License_Users__c, TIO_Users_Total__c, container_uuid__c FROM 
                                    Evaluation__c WHERE container_uuid__c != null limit 5];
            system.debug('The size of firstscope and secondscope is : ' + firstscope.size() +  '   ' + secondscope.size());
        }
        
        list<sObject> currentScope = new list<sObject>();
        currentScope.addAll(firstScope);
        currentScope.addAll(secondScope);
        System.debug('@@@@@@@@@@1: ' + usageDate);
        system.debug('@@@@ list size ' + currentScope.size());
        
        return currentScope;
    }
    
   //global void execute(SchedulableContext ctx)
   //{
    //    Database.executeBatch(new BatchUpdateTenableioUsageLog(), 150);
   //}
    
    global void execute(Database.BatchableContext bc, List<sObject> scope)
    {
        system.debug('@@@@@@@@2: ' + usageDate);
        system.debug('@@@@@ Scope Size ' + scope.size());
        //usageDate = runDate;
        
        map<string, sObject> installBaseMap    = new map<string, sObject>();
        list<Tenable_io_Usage_Log__c> tioUList = new list<Tenable_io_Usage_Log__c>();//UPdate list for Insall base, account, and evals
        list<sObject> objUpdateList            = new list<sObject>();
        list<sObject> installObjUpdateList     = new list<sObject>();
        list<Account> accUpdateList            = new list<Account>();
        map<id, Account> accMap                = new map<id, Account>();
        map<id, sObject> evalMap               = new map<id, sObject>();
        map<id, sObject> ibMap                 = new map<id, sObject>();
        set<id> accIdSet                       = new set<id>();
        set<string> uuidSet                    = new set<string>();
        string containerString = '';
        Tenable_io_Usage_Log__c tempTIOU;
        SObject ibase;
        Account acc;
        logStats = new list<usageLogStatistics>();
        usageLogStatistics newLog = new usageLogStatistics();
        exceptionMap = new map<Id, string>();
        
        //adding UUID and mapping to Install Base and Eval objects
        string objType;
        for(sObject so: scope){
            objType = string.valueOf(so.getSobjectType());
            system.debug('WHAT IS THE OBJECT TYPE: ' + objType);
            if(objType == 'Install_Base__c'){
                system.debug('Install Base: ' + string.valueOf(so.get('UUID__c')));
            containerString += '&container=' + string.valueOf(so.get('UUID__c'));
            installBaseMap.put(string.valueOf(so.get('UUID__c')), so);
            }else if(objType == 'Evaluation__c'){
                system.debug('Evaluation: ' + string.valueOf(so.get('container_uuid__c')));
                containerString += '&container=' + string.valueOf(so.get('container_uuid__c'));
                installBaseMap.put(string.valueOf(so.get('container_uuid__c')), so);
            }
        }
            
        //Here I am doing the callout that will get all the us all the tenable.io Usage data.  I would like to pass today's date and all the containers in the parameters
        HTTPResponse messageres   = getTenablioUsage(containerString);
        
        logStats                  = (list<usageLogStatistics>)JSON.deserialize( messageres.getBody(), list<usageLogStatistics>.class);
        system.debug('#######  ' + logStats);
        
        //Here I am iterating through the list of all the container information that we have. 
        for(usageLogStatistics l: logStats){
            
            //only looping through the fields if the record is in our scope
            if(installBaseMap.get(l.container_uuid) != null){
            system.debug('#######1  ' + l);
                system.debug('@@@ Usage Date: ' + usageDate);
                //Creating a map so that I can get all the JSON key pairs so I know which fields to update
                Map<String, Object> mapOfFields = (Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(l));
                updateFieldSet = mapOfFields.keyset();
        
                //creating a new Tenable_io_Usage_Log__c object so that we can add it to our list
                tempTIOU = new Tenable_io_Usage_Log__c();
                string tempField, tempString;
                Schema.DisplayType fieldType;
                double tempNumber;
                date tempDate;
                dateTime tempDateTime;
                boolean tempBoolean;
            
            
                //Here I am iterating through the field list for each record so that we can create the Tenable.io Usage Log objct
                for(string s: updateFieldSet){
                    tempField = s + '__c';
                    if(tempField == 'container_uuid__c'){
                        tempField = 'UUID__c';
                    }
                    if(tempField == 'assets_licensed_last_90_excluding_deleted__c'){
                        tempField = 'Assets_Licensed_Last_90_Excluding_Delete__c';
                    }
                    fieldType = getFieldType(tempField);
                    if(fieldType == null || mapOfFields.get(s) == null){
                        system.debug('!!!!!!!!: ' + tempField + ', ' + mapOfFields.get(s) + ', ' + l.measurement_date + ', ' + l.container_uuid);
                    }else if(string.ValueOf(fieldType) == 'DOUBLE' ){
                    
                        tempNumber = double.valueOf(mapOfFields.get(s));
                        tempTIOU.put(tempField, tempNumber);
                        
                    }else if(string.ValueOf(fieldType) == 'BOOLEAN'){
                        tempBoolean = boolean.valueOf(mapOfFields.get(s));
                        tempTIOU.put(tempField, tempBoolean);
                    }else if(string.ValueOf(fieldType) == 'Date'){
                        tempString = string.valueOf(mapOfFields.get(s));
                        tempDate = Date.valueOf(tempString);
                        tempTIOU.put(tempField, tempDate);
                    }else if(string.ValueOf(fieldType) == 'DateTime'){
                        tempString = string.valueOf(mapOfFields.get(s));
                        tempString = tempString.replaceAll('T', ' ');
                        tempDateTime = dateTime.valueOf(tempString);
                        tempTIOU.put(tempField, tempDateTime);
                    }else{
                        tempString = String.valueOf(mapOfFields.get(s));
                        tempTIOU.put(tempField, tempString);
                    } 
                }
                
                //Now I need to update the corresponding Account and INstall Base OBjects
                acc   = new Account();
                //ibase = Schema.getGlobalDescribe().get(objType).newSObject(); 
                
                //updating the necessary field in each
                SObject obj = installBaseMap.get(l.container_uuid);
                objType = string.valueOf(obj.getSobjectType());
                
                if(objType == 'Install_Base__c'){
                    String accId = (String) obj.get('Account__c');
                    system.debug('ACCNAME ' + accId);
                    acc.put('Id', accId);
                
                    //now that we have looped through the field list I am going to set the Id of the install base object
                    tempTIOU.Install_Base__c = installBaseMap.get(l.container_uuid).Id;
                }else{
                    tempTIOU.Evaluation__c = installBaseMap.get(l.container_uuid).Id;
                }
                //setting the Id for the object we are updating
                String ibId = (String) obj.get('Id');
                obj.put('Id', ibId);
                if (l.users_total != null)
                {
                    obj.put('TIO_Users_Total__c', l.users_total);
                }
                else
                {
                    obj.put('TIO_Users_Total__c', 0);
                }
                
                if(l.license_users != null)
                {
                    obj.put('TIO_License_Users__c', l.license_users);
                }
                else
                {
                    obj.put('TIO_License_Users__c', 0);
                }
                if(l.license_assets != null)
                {
                    obj.put('TIO_License_Assets__c', l.license_assets);
                    if(objType == 'Install_Base__c')
                    {
                        acc.put('TIO_License_Assets__c', l.license_assets);
                    }
                }else{
                    obj.put('TIO_License_Assets__c', 0);
                    if(objType == 'Install_Base__c')
                    {
                        acc.put('TIO_License_Assets__c', 0);
                    }
                }
                if(l.Assets_Total != null)
                {
                    obj.put('TIO_Assets_Total__c', l.assets_total);
                }
                else
                {
                    obj.put('TIO_Assets_Total__c', 0);
                }
                if(l.Assets_Scanned_Total != null)
                {
                    obj.put('TIO_Assets_Scanned_Total__c', l.assets_scanned_total);
                    if(objType == 'Install_Base__c'){
                        acc.put('TIO_Assets_Scanned_Total__c', l.assets_scanned_total);
                    }
                }
                else
                {
                    obj.put('TIO_Assets_Scanned_Total__c', 0);
                    if(objType == 'Install_Base__c'){
                        acc.put('TIO_Assets_Scanned_Total__c', 0);
                    }
                }
                
                if(l.Assets_Scanned_90_Days != null)
                {
                    obj.put('TIO_Assets_Scanned_90_Days__c', l.assets_scanned_90_days);
                    if(objType == 'Install_Base__c'){
                        acc.put('TIO_Assets_Scanned_90_Days__c', l.assets_scanned_90_days);
                    }
                }
                else
                {
                    obj.put('TIO_Assets_Scanned_90_Days__c', 0);
                    if(objType == 'Install_Base__c'){
                        acc.put('TIO_Assets_Scanned_90_Days__c', 0);
                    }
                }
                if(l.Scan_Last_Scan_Date != null)
                {
                    obj.put('TIO_Scan_Last_Scan_Date__c', l.scan_last_scan_date);
                }
                else
                {
                    obj.put('TIO_Scan_Last_Scan_Date__c', '0');
                }
                //Mapping new fields to the install base object
                if(l.assets_licensed_total != null){
                    if(objType == 'Install_Base__c'){
                    	obj.put('TIO_Assets_Licensed_Total__c', l.assets_licensed_total);
                    }
                }
                
                if(l.assets_licensed_last_90 != null){
                    if(objType == 'Install_Base__c'){
                    	obj.put('TIO_Assets_Licensed_Last_90__c', l.assets_licensed_last_90);
                        acc.put('TIO_Assets_Licensed_Last_90__c', l.assets_licensed_last_90);
                    }
                }
                
                if(l.assets_licensed_last_90_excluding_deleted != null){
                    if(objType == 'Install_Base__c'){
                    	obj.put('TIO_Assets_Licensed_Last_90_Ex_Delete__c', l.assets_licensed_last_90_excluding_deleted);
                   		acc.put('TIO_Assets_Licensed_Last_90_Ex_Delete__c', l.assets_licensed_last_90_excluding_deleted);
                    }
                }
               
                //adding fields to the corresponding account Object 
                updateFieldSet.clear();
                tioUList.add(tempTIOU);
                if(objType == 'Install_Base__c'){
                    accMap.put(acc.id, acc);
                    ibMap.put(obj.Id, obj);
                    //installObjUpdateList.add(obj);
                    system.debug('****** Install Base Obj: '+ obj);
                    system.debug('****** list: ' + objUpdateList);
                }else{
                    //objUpdateList.add(obj);
                    evalMap.put(obj.Id, obj);
                }
            }/*else{
                system.debug('&&&&&&&& The UUID Was not in the scope');
            }*/
        }
        system.debug('@@@@@@@2.5 Usage Date: ' + usageDate);
        system.debug('*********1: ' + tioUList);
        system.debug('*********2: ' + evalMap.Values());
        system.debug('*********3: ' + ibMap.Values());
        system.debug('*********4: ' + accMap.Values());
         
        String errorBody = '';
        
        Database.SaveResult[] srList  = database.insert(tioUList, false);
        // Iterate through each returned result
        for (Database.SaveResult sr : srList) {
            if (!sr.isSuccess()) {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    exceptionMap.put(sr.getId(),err.getMessage() + ' ' + err.getMessage());
                }
            }
        }
        Database.SaveResult[] srList2 = database.update(evalMap.Values(), false);
        // Iterate through each returned result
        for (Database.SaveResult sr : srList2) {
            if (!sr.isSuccess()) {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    exceptionMap.put(sr.getId(),err.getMessage() + ' ' + err.getMessage());
                }
            }
        }
        Database.SaveResult[] srList3 = database.update(ibMap.Values(), false);
        // Iterate through each returned result
        for (Database.SaveResult sr : srList3) {
            if (!sr.isSuccess()) {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    exceptionMap.put(sr.getId(),err.getMessage() + ' ' + err.getMessage());
                }
            }
        }
        Database.SaveResult[] srList4 = database.update(accMap.Values(), false);
        // Iterate through each returned result
        for (Database.SaveResult sr : srList4) {
            if (!sr.isSuccess()) {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    exceptionMap.put(sr.getId(),err.getMessage() + ' ' + err.getMessage());
                }
            }
        }
       
        /* }catch (exception e){
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            List<String> sendTo = new List <String>{'cfovel@tenable.com', 'rjones@tenable.com', 'bmuniswamy@tenable.com'};
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setReplyTo('sfdc@tenable.com');
            mail.setSenderDisplayName('Salesforce Automation');
            mail.setSubject('Error Retrieving Tenabl.io Usage Statistics');
            String body = 'There was an error when inserting records into usage records into SFDC: ' + e;
            mail.setToAddresses(sendTo);
            mail.setHtmlBody(body);
            mails.add(mail);
        }*/
        
    }
    
    global void finish(Database.BatchableContext bc){
        integer o;
        system.debug('$$$$$$$$0: Exceptions ' + exceptionMap.size());
        for(Id i: exceptionMap.keySet()){
            o++;
            system.debug('$$$$$$$$' + o + ': Exceptions ' + i + ' and ' + exceptionMap.get(i));
        }
    }
    
    public map<string, string> SetAuthorizationHeaderValues(){   
        map<string, string> callOutMap = new map<string, string>();
        
        for(Integration_Setting__mdt is : [SELECT Environment_Ind__c, Client_ID__c,Client_Secret__c,Email_Address__c,Endpoint_URL__c
                FROM Integration_Setting__mdt order by Environment_Ind__c asc limit 1]){
            string ClientCredentials = is.Client_ID__c + ':' + is.Client_Secret__c;
            Blob headerValue = Blob.valueOf(ClientCredentials);
            string authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            callOutMap.put('authorizationHeader', authorizationHeader);
            string endpoint = is.Endpoint_URL__c;
            callOutMap.put('endpoint', endpoint);
        }
        
        return callOutMap;
    }
    
    public HttpResponse getTenablioUsage(string containerString){
        map<string, string> callOutMap = SetAuthorizationHeaderValues();
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        List<String> sendTo = new List <String>{'cfovel@tenable.com', 'rjones@tenable.com', 'bmuniswamy@tenable.com'};
        string todayDate;
        system.debug('@@@@@@@@3 Usage Date: ' + usageDate);
        if(usageDate == null || usageDate == ''){
            todayDate = string.valueOf(date.today());
        }else{
            todayDate = usageDate;
        }
        system.debug('&&&&: ' + containerString);
        //String endPoint = callOutMap.get('endpoint') + 'tenableio/statistics?' + containerString + 'date=' + todayDate;
        //String endPoint = callOutMap.get('endpoint') + 'tenableio/statistics?container=90b89808-914b-4afa-bded-c74b740c7379&date=' + todayDate;
        //String endPoint = callOutMap.get('endpoint') + 'tenableio/statistics?date=' + todayDate;
        String endPoint = 'https://svc.cloud.tenable.com/usage/v1/statistics?date=' + todayDate + containerString;

        system.debug('@@@ endpoint: ' + endPoint);  
    
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        
        req.setHeader('Content-Type', 'application/json');
        //req.setHeader('Authorization',callOutMap.get('authorizationHeader'));
        req.setHeader('X-ApiKeys','accessKey=Z4CZCjKUnxNJnVdRVShCd9h1zEJazvw6U3CGTFhB;secretKey=WmWEiW8uqPhHzQu1dm5zHw1FBsipKhaQ7l0cCrrA');
        req.setEndpoint(endPoint);
        req.setMethod('GET');
        req.setTimeout(90000);
        HttpResponse response = h.send(req);                                                            // send http request
        system.debug('@@@ Get date response:' + response.getBody());
        if (response.getStatusCode() != 200) {                                                          // send email if there is an error
         /*   Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setReplyTo('sfdc@tenable.com');
            mail.setSenderDisplayName('Salesforce Automation');
            mail.setSubject('Error Retrieving Tenabl.io Usage Statistics');
            String body = 'There was an error creating a NetSuite Sales order from Opportunity Id: ' + 
                oppId + '.  Response Code: ' + response.getStatusCode();
            mail.setToAddresses(sendTo);
            mail.setHtmlBody(body);
            mails.add(mail);*/
            try{
               // Messaging.sendEmail(mails);
               system.debug('@@@ Status code not equale to 200');
            }
            catch(Exception e){
                system.debug('@@ exception sending email:'+e);
            }
        }
        return response;
    }
    
    
    //THis is the object we are going to convert our json string into
    public class usageLogStatistics {
        public String container_uuid {get; set;}
        public String measurement_date {get; set;}
        public String measurement_timestamp {get; set;}
        public String container_region {get; set;}
        public String container_site_name {get; set;}
        public String container_partner_id {get; set;}
        public String container_name {get; set;}
        public Integer container_id {get; set;}
        public String container_creation_date {get; set;}
        public List<String> enabled_features {get; set;}
        public Integer license_assets {get; set;}
        public Integer license_agents {get; set;}
        public Integer license_scanners {get; set;}
        public Integer license_users {get; set;}
        public List<String> license_domains {get; set;}
        public String license_expiration_date {get; set;}
        public Boolean license_is_expired {get; set;}
        public String license_modification_date {get; set;}
        public Integer users_total {get; set;}
        public Integer users_api_total {get; set;}
        public Integer users_active_30_days {get; set;}
        public Integer users_active_60_days {get; set;}
        public Integer users_active_90_days {get; set;}
        public Integer users_domain_total {get; set;}
        public String users_last_login_date {get; set;}
        public Integer assets_total {get; set;}
        public Integer assets_scanned_total {get; set;}
        public Integer assets_scanned_30_days {get; set;}
        public Integer assets_scanned_60_days {get; set;}
        public Integer assets_scanned_90_days {get; set;}
        public Integer agents_total {get; set;}
        public Integer agents_active {get; set;}
        public Integer scanners_total {get; set;}
        public Integer scanners_active {get; set;}
        public Integer scans_discovery_total {get; set;}
        public Integer scans_discovery_30_days {get; set;}
        public Integer scans_discovery_60_days {get; set;}
        public Integer scans_discovery_90_days {get; set;}
        public Integer scans_total {get; set;} 
        public Integer scans_30_days {get; set;}
        public Integer scans_60_days {get; set;}
        public Integer scans_90_days {get; set;}
        public String scan_last_scan_date {get; set;}
        public Integer scans_pci_total {get; set;}
        public Integer scans_pci_30_days {get; set;}
        public Integer scans_pci_60_days {get; set;}
        public Integer scans_pci_90_days {get; set;}
        public String scans_pci_last_scan_date {get; set;}
        public Integer scans_storage_size_mb {get; set;}
        public integer assets_was_scanned_90_days {get;set;}
        public String scans_discovery_last_scan_date {get;set;}
        public Integer assets_licensed_total {get; set;}
        public Integer assets_licensed_last_90 {get; set;}
        public Integer assets_licensed_last_90_excluding_deleted  {get; set;}
        
    }
    
   /*We are getting the schema map here to check the field type to make sure that we are converting the JSON values to the correct fields. */
   public Schema.DisplayType getFieldType(String fieldName){
        try{
        Schema.SObjectField field = TenableioUsageLogFieldTypeMap.get(fieldName);
        Schema.DisplayType FldType = field.getDescribe().getType();
        return FldType;
        }catch(exception e){
            system.debug('THis field needs a match: ' + fieldName);
            return null;
        }
   }
}