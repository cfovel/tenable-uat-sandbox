/*************************************************************************************
Class: TestExtDisplayDraftArticleTypes
Author: Bharathi.M
Date: 09/01/2017
Details: This class is the test class for the class ExtDisplayDraftArticleTypes
***************************************************************************************/
@isTest(seealldata = false)
private class TestExtDisplayDraftArticleTypes 
{

    static testMethod void TestGettingKnowledgeArticles() 
    {
        How_To__Kav howtoarticle1 = new How_to__Kav(Title='test1',summary='xyz',urlname='xyz');
        How_To__Kav howtoarticle2 = new How_to__Kav(Title='test2',summary='xyz',urlname='xyz');
        
        test.StartTest();
            ApexPages.StandardController ctrl = new ApexPages.StandardController(howtoarticle1);
            ExtDisplayDraftArticleTypes extctrl = new ExtDisplayDraftArticleTypes(ctrl);
            extctrl.ObjectName = 'How_To__Kav';
            extctrl.GetDraftDocumentInfo();
            extctrl.OpenPdfDocument();
        test.StopTest();
    }
}