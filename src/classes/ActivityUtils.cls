public class ActivityUtils {
 
    //config
    String openCountField = 'Activity_Counter_Open__c'; //this field must be added to each object we're updating
    String closedCountField = 'Activity_Counter_Closed__c'; //this field must be added to each object we're updating
 
    //state
    //set<id> accountIds;
    set<id> contactIds;
    //set<id> opportunityIds;
    set<id> leadIds;
 
    public ActivityUtils(sObject[] records) {
        //accountIds = new set<id>();
        contactIds = new set<id>();
        //opportunityIds = new set<id>();
        leadIds = new set<id>();
        captureWhatAndWhoIds(records);
        //addAccountIdsFromRlatedObjects();
    }
 
    public void updateContactActivityCount() {
        if(contactIds.size() == 0) return;
        //updateActivityCount('Contact','WhoId', getStringFromIdSet(contactIds));
        updateActivityCount('Contact', contactIds);
    }
    
    public void updateLeadActivityCount() {
        if(leadIds.size() == 0) return;
        //updateActivityCount('Lead','WhoId', getStringFromIdSet(leadIds));
        updateActivityCount('Lead',leadIds);
    }
    //private void updateActivityCount(String objToUpdate, String queryFld, String updateIds) {
    private void updateActivityCount(String objToUpdate, Set<Id> updateIds) {
        //List<Task> tasks = new List<Task>();
        //List<Event> events = new List<Event>();
        //List<sObject> sObjects = new List<sObject>();
        Map<Id,sObject> sObjectMap = new Map<Id,sObject>();
        //String profileIds = new String(getStringFromIdSet(new Set<String>{'00e600000017WAvAAM', '00e600000017VgKAAU', '00ef2000001BgwOAAS', '00e60000000izjZAAQ', '00e600000017Rc6AAE'}));
        //String strQuery = 'SELECT Id FROM ' + objToUpdate + ' WHERE Id IN (' + updateIds + ')';
        //String strQuery;
        Decimal openCount = 0;   
	    Decimal closedCount = 0;
	    //List<String> profileIds = new List<String>{'00e600000017WAvAAM', '00e600000017VgKAAU', '00ef2000001BgwOAAS', '00e60000000izjZAAQ', '00e600000017Rc6AAE'};
	    
	    List<String> profileIds = new List<String>(); 
	    profileIds.add('00e600000017WAvAAM');					// Tenable - ISR (with Linkedin) User
	    profileIds.add('00e600000017VgKAAU');					// Tenable - ISR User
	    profileIds.add('00ef2000001BgwOAAS');					// Tenable - Sales CSM User
	    profileIds.add('00e60000000izjZAAQ');					// Tenable - Sales Executive User
	    profileIds.add('00e600000017Rc6AAE');					// Tenable - Sales Management User
	    
	    //String profileIds = getStringFromIdSet(profiles);

        /*
        //for(sObject so : database.query(strQuery)) {
        //	strQuery = 'SELECT Id, CreatedBy.ProfileId, Status, WhoId FROM Task WHERE WhoId IN (' + updateIds + ') AND CreatedBy.ProfileId IN :profileIds';
        	//strQuery = 'SELECT Id, CreatedBy.ProfileId, Status, WhoId FROM Task WHERE WhoId IN :updateIds AND CreatedBy.ProfileId IN :profileIds';
	        //for (Task t : database.query(strQuery))
	        for (Task t : [SELECT Id, Status, WhoId FROM Task WHERE WhoId IN :updateIds AND CreatedBy.ProfileId IN :profileIds])
	        {
	        	//openCount = 0;
	        	//closedCount = 0;
	        	sObject so = sObjectMap.get(t.WhoId);
	        	if (so == null)
	        	{
	        		so = createObject(objToUpdate, t.WhoId); 
	        		so.put(openCountField, 0);
            		so.put(closedCountField, 0);
	        	}
	        	if(t.Status!='Completed')
	        	{
		            openCount = (decimal)so.get(openCountField) + 1;
		        } 
		        else
		        {
		            closedCount = (decimal)so.get(closedCountField) + 1;
		        }    
		        so.put(openCountField,openCount);
		        so.put(closedCountField,closedCount);
		        sObjectMap.put(t.WhoId,so);
	        }
	        //strQuery = 'SELECT Id, EndDateTime, WhoId FROM Event WHERE WhoId IN (' + updateIds + ') AND CreatedBy.ProfileId IN :profileIds';
	        //strQuery = 'SELECT Id, EndDateTime, WhoId FROM Event WHERE WhoId IN :updateIds AND CreatedBy.ProfileId IN :profileIds';
	        for (Event e : [SELECT Id, EndDateTime, WhoId FROM Event WHERE WhoId IN :updateIds AND CreatedBy.ProfileId IN :profileIds])
	        {
	        	//openCount = 0;
	        	//closedCount = 0;
	        	sObject so = sObjectMap.get(e.WhoId);
	        	if (so == null)
	        	{
	        		so = createObject(objToUpdate, e.WhoId); 
	        		so.put(openCountField, 0);
            		so.put(closedCountField, 0);
	        	}
	        	if(e.EndDateTime > DateTime.now())
	        	{
		            openCount = (decimal)so.get(openCountField) + 1;
		        } 
		        else
		        {
		            closedCount = (decimal)so.get(closedCountField) + 1;
		        }    
		        so.put(openCountField,openCount);
		        so.put(closedCountField,closedCount);
		        sObjectMap.put(e.WhoId,so);
	        }
	        
	        //sObject obj = createObject(objToUpdate, so.Id);
            //obj.put(openCountField, openCount);
            //obj.put(closedCountField, closedCount);
            //sObjects.add(obj);
        //}
        
        update sObjectMap.values();
        
        */
        
        /*
        string strQuery = 'SELECT Id, (SELECT Id FROM OpenActivities) FROM ' + objToUpdate + ' WHERE Id IN (' + updateIds + ')';
        sObject[] sobjects = new list<sobject>();
        for(sObject so : database.query(strQuery)) {
            OpenActivity[] oActivities = so.getSObjects('OpenActivities');
            Integer openActivityCount = oActivities == null ? 0 : oActivities.size();
            sObject obj = createObject(objToUpdate, so.Id);
            obj.put(openCountField, openActivityCount);
            sobjects.add(obj);
            system.debug('openActivityCount: ' + openActivityCount);
        }
        update sobjects;
        */
        
        
        string strQuery = 'SELECT Id, (SELECT Id, EndDateTime FROM Events WHERE CreatedBy.ProfileId IN :profileIds), (SELECT Id, Status FROM Tasks WHERE CreatedBy.ProfileId IN :profileIds) FROM ' + objToUpdate + ' WHERE Id IN :updateIds';
        sObject[] sobjects = new list<sobject>();
        for(sObject so : database.query(strQuery)) {
            //OpenActivity[] oActivities = so.getSObjects('OpenActivities');
            //Integer openActivityCount = oActivities == null ? 0 : oActivities.size();
            openCount = 0;
	        closedCount = 0;
	        //List<Event> events = so.getSObjects('Events');
	        //List<Task> tasks = so.getSObjects('Tasks');
            sObject obj = createObject(objToUpdate, so.Id);
            for (Task t : so.getSObjects('Tasks'))
            {
            	if(t.Status!='Completed')
	        	{
		            openCount+=1;
		        } 
		        else
		        {
		            closedCount+=1;
		        }    
            }
            for (Event e : so.getSObjects('Events'))
            {
            	if(e.EndDateTime > DateTime.now())
	        	{
		            openCount+=1;
		        } 
		        else
		        {
		            closedCount+=1;
		        }    
            }
            obj.put(openCountField, openCount);
            obj.put(closedCountField, closedCount);
            sobjects.add(obj);
        }
        update sobjects;
    }
 
    private void captureWhatAndWhoIds(sObject[] objects) {
        for(sObject o : objects) {
            //Id whatId = (Id)o.get('WhatId');
            Id whoId = (Id)o.get('WhoId');
            //if(whatId != null) {
            //    String objectName = getObjectNameFromId(whatId);
            //    if(objectName == 'account') accountIds.add(whatId);
            //    if(objectName == 'opportunity') opportunityIds.add(whatId);
            //}
            if(whoId != null) {
                String objectName = getObjectNameFromId(whoId);
                if(objectName == 'contact') contactIds.add(whoId);
                if(objectName == 'lead') leadIds.add(whoId);
            }
        }
    }
    
    /*
    public void updateAccountActivityCount() {
        if(accountIds.size() == 0) return;
        updateActivityCount('Account','WhatId', getStringFromIdSet(accountIds));
    }
    
    public void updateOpportunityActivityCount() {
        if(opportunityIds.size() == 0) return;
        updateActivityCount('Opportunity','WhatId', getStringFromIdSet(opportunityIds));
    }
 
    private void addAccountIdsFromRlatedObjects() {
        for(Opportunity o : [SELECT AccountId FROM Opportunity WHERE Id IN :opportunityIds]) accountIds.add(o.AccountId);
        for(Contact c : [SELECT AccountId FROM Contact WHERE Id IN :contactIds]) accountIds.add(c.AccountId);
    }
    */
 
    private String getObjectNameFromId(Id objId) {
        String preFix = String.valueOf(objId).left(3).toLowercase();
        //if(prefix == '001') return 'account';
        if(prefix == '003') return 'contact';
        //if(prefix == '006') return 'opportunity';
        if(prefix == '00q') return 'lead';
        return '';
    }
    
    private String getStringFromIdSet(set<id> idSet) {
        string idString = '';
        for(Id i : idSet) idString+= '\'' + i + '\',';
        return idString == '' ? idString : idString.left(idString.length()-1); //If idString contains some ids we want to ensure we strip out the last comma
    }
 
    
 
    //The main part of the method below was taken from //Taken from http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_dynamic_dml.htm
    //However we've modified this to accept an object id
    private sObject createObject(String typeName, Id objId) {
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(typeName);
        if (targetType == null) {
            // throw an exception
            throw new applicationException('Invalid sObject Type');
        }
 
        // Instantiate an sObject with the type passed in as an argument
        //  at run time.
        return targetType.newSObject(objId);
    }
    
    public class applicationException extends Exception {}
 
}