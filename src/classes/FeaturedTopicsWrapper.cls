public without sharing class FeaturedTopicsWrapper {
    @AuraEnabled public String topicId {get; set;}
    @AuraEnabled public String tTitle {get; set;}
    @AuraEnabled public String tBody {get; set;}
    @AuraEnabled public String tType {get; set;}
    @AuraEnabled public String tRecordId {get; set;}
    @AuraEnabled public String tRecordURL {get; set;}
    @AuraEnabled public Date tDate {get; set;}
}