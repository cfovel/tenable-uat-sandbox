public class fundClaimInstructionsController {
    public Id fundClaimId;      
    public Boolean emptyLines = TRUE;  
    public PageReference pageRef = ApexPages.currentPage();
    //private final Fund_Claim__c fundClaim;
    public Fund_Claim__c fundClaim {get; set;}
    //public List<ProofOfPerfWrapper> proofOfPerfList {get; set;}
    public String proofOfPerformance {get; set;}
    
    private final ApexPages.standardController stdController;                                       // Set-up page controller
    
    public fundClaimInstructionsController(ApexPages.StandardController stdController)
    {
        if ((ApexPages.currentPage().getParameters() != null) &&                                    // Make sure we have parameters
            (ApexPages.currentPage().getParameters().get('id') != null) &&                          // and we're passing in a fund claim id value
            (ApexPages.currentPage().getParameters().get('id') != '')) 
        {
            //stdController.addFields(new List<String>{'Fund_Activity_Type__c'});
            this.stdController = stdController;
            this.fundClaimId = ApexPages.currentPage().getParameters().get('id');                        // Save the fund claim id parameter
            
            this.fundClaim = [SELECT Fund_Activity_Type__c, POP_Loaded__c FROM Fund_Claim__c 
                                WHERE Id = :this.fundClaimId];
            init();                                                                                 // Go initialize all the other values we want for the VF Page
        }
    }
    
    public void init()                                                                             // Page Initialization
    {
        //this.proofOfPerfList = new List<ProofOfPerfWrapper>();
        this.proofOfPerformance = '';
        String filterLogic = '';
        //String output = '';
        Map<Integer, String> popItemMap = new Map<Integer, String>();
        
        for(Activity_POP_Setting__mdt aps : [SELECT Activity_Type__c, Line_Number__c, Proof_of_Performance__c, Qualifier__c, Filter_Logic__c
                FROM Activity_POP_Setting__mdt WHERE Activity_Type__c = :fundClaim.Fund_Activity_Type__c  order by Line_Number__c])
        {
            //ProofOfPerfWrapper pop = new ProofOfPerfWrapper();
            if (aps.Line_Number__c == 1 && aps.Filter_Logic__c > '')
            {
                filterLogic = aps.Filter_Logic__c;
            }
            
            String popItem = aps.Proof_of_Performance__c;
            if (aps.Qualifier__c > '')
            {
                 popItem = popItem + ' ' + aps.Qualifier__c;
            }   
             
            popItemMap.put(Integer.valueOf(aps.Line_Number__c), popItem);
        }
        
        if (filterLogic <= '')
        {
			for(Integer i = 1; i <= popItemMap.size(); i++)
            {
                filterLogic += String.valueOf(i);
                if (i < popItemMap.size())
                {
                    filterLogic += ' AND ';
                }
            }
        }
        
        proofOfPerformance = filterLogic;
        
        for(Integer i = 1; i <= popItemMap.size(); i++)
        {
            if(proofOfPerformance.contains(String.valueOf(i))){
                proofOfPerformance = proofOfPerformance.replace(String.valueOf(i),popItemMap.get(i));
            }
        }

           
        if (this.proofOfPerformance > '' ) 
        {
            emptyLines = false;
        }
    }   

    
    public Boolean getPOPEmpty() 
    {
        return emptyLines;
    }  
    
    public PageReference updatePOP() 
    {
        fundClaim.POP_Loaded__c = TRUE;
        update fundClaim;   
        return pageRef;
    }
    
        public Boolean getPOPLoaded() 
    {
        return fundClaim.POP_Loaded__c;
    }  

/*    
    public Boolean popIsEmpty(ProofOfPerfWrapper pop) 
    {
        boolean isEmpty = true;
        if (pop.popItem > '') {
            isEmpty = false;
        }
        return isEmpty;
    }  

    public List<ProofOfPerfWrapper> getProofOfPerfList() 
    {
        return this.proofOfPerfList;
    }
    
    public class ProofOfPerfWrapper 
    {
        public String instruction {get; set;}
        public Boolean loaded {get; set;}
        public String popItem {get; set;}
        public String andOr {get; set;}
        //public String activationCode {get; set;}
    }*/
}