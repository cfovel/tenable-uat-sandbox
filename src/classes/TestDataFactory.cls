@isTest
public class TestDataFactory {
    
    public static void insertUsers (List<String> profileNames) {
        List<Profile> profiles = [select id, name from Profile]; 
        Map<String, Id> profileMap = new Map<String, Id>();
        Id profileId; 
        List<User> usersToAdd = new List<User>();
        
        for (Profile p : profiles) {
            profileMap.put(p.Name, p.Id);
        }
        for (Integer i=0; i < profileNames.size(); i++) {
            User newUser = new User(alias = 'user' + i, email='user' + i + '@testemail.com', 
            emailencodingkey='UTF-8', firstname = 'User' + i, lastname='Test', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = profileMap.get(profileNames[i]), 
            timezonesidkey='America/Los_Angeles', username='user' + i + '@testemail.com');
            
            usersToAdd.add(newUser);
        }

        insert usersToAdd;
    }
    
    public static void insertAccounts (Integer nbrAccts) {
        List<Account> acctsToadd = new List<Account>();
        Utils.isTest = true;
        accountTriggerHandler.enforceCountry = false;
        for (Integer i=0; i < nbrAccts; i++) {
            Account acct = new Account(Name='Test' + i + 'Inc', BillingState='CA', BillingCountry='US', website='http://www.test' + i + '.com');
            acctsToAdd.add(acct);
        }
        acctsToAdd.add(new Account(Name='Dell SecureWorks', BillingState='CA', BillingCountry='US', website='http://www.test1.com'));

        insert acctsToAdd;
    }
    
    public static void insertContacts (Integer nbrCons) {
        // creates this number of contacts for each account
        List<Contact> consToadd = new List<Contact>();
        List<Account> accts = [SELECT Id, Name FROM Account WHERE Name LIKE 'Test%'];
        for (Account acct : accts) {
            for (Integer i=0; i < nbrCons; i++) {
                Contact con = new Contact(firstname='Con' + i,lastname='Con' + acct.Name, accountid=acct.id, email= 'Con' + i + acct.Name + '@test.com', MailingCountry='US', MailingState='CA');
                consToAdd.add(con);
            }
        }
        insert consToAdd;
    }
    
    
    public static void insertOpportunities (Integer nbrOpptys) {
        // creates this number of opportunities for each account
        Id pricebookId = Test.getStandardPricebookId();
        List<Opportunity> opptystoAdd = new List<Opportunity>();   
        List<Account> accts = [SELECT Id, Name FROM Account WHERE Name LIKE 'Test%'];
        for (Account acct : accts) {
            for (Integer i=0; i < nbrOpptys; i++) {
                Opportunity opp = new Opportunity(name='TestOpp' + i + acct.Name, accountid = acct.id, StageName='Proposal Submitted', pricebook2id = pricebookId, Purchase_Order_Number__c = '1234' + i, CloseDate=Date.newInstance(2015,10,10));
                opptysToAdd.add(opp);
            }
        }
        insert opptysToAdd;
    }
    
    public static void insertProducts () {

        Product2[] productstoAdd = new Product2[0];                                                 // Go create some test products to use
        
        productstoAdd.add(
            new Product2(name='test', geography__c = 'AMER', category__c='Products', Tier_Minimum__c=0, Tier_Maximum__c=4, 
                family='Nessus Professional', license_type__c='Subscription', unit__c='Each', isactive=true, description='test', CanUseRevenueSchedule=true, Service_Level__c = 'Standard'));      
        productstoAdd.add(
            new Product2(name='test', geography__c = 'AMER', category__c='Products', Tier_Minimum__c=0, Tier_Maximum__c=4, 
                family='Nessus Cloud', license_type__c='Subscription', unit__c='Each', isactive=true, description='test', CanUseRevenueSchedule=true, Service_Level__c = 'Advanced'));     
        productstoAdd.add(
            new Product2(name='test', geography__c = 'AMER', category__c='Products', Tier_Minimum__c=0, Tier_Maximum__c=4, 
                family='Nessus Manager', license_type__c='Test', unit__c='Each', isactive=true, description='test', CanUseRevenueSchedule=true, Service_Level__c = 'Elite'));       
        productstoAdd.add(
            new Product2(name='test', geography__c = 'AMER', category__c='Products', Tier_Minimum__c=0, Tier_Maximum__c=4, 
                family='SecurityCenter', license_type__c='Test', unit__c='Each', isactive=true, description='test', CanUseRevenueSchedule=true, Service_Level__c = 'Advanced'));       
        productstoAdd.add(
            new Product2(name='test', geography__c = 'AMER', category__c='Products', Tier_Minimum__c=0, Tier_Maximum__c=4, 
                family='SecurityCenter Continuous View', license_type__c='Test', unit__c='Each', isactive=true, description='test', CanUseRevenueSchedule=true, Service_Level__c = 'Advanced'));       
        productstoAdd.add(
            new Product2(name='test', geography__c = 'AMER', category__c='Products', Tier_Minimum__c=0, Tier_Maximum__c=4, 
                family='Log Correlation Engine', license_type__c='Test', unit__c='Each', isactive=true, description='test', CanUseRevenueSchedule=true, Service_Level__c = 'Advanced'));       
        productstoAdd.add(
            new Product2(name='test', geography__c = 'AMER', category__c='Products', Tier_Minimum__c=0, Tier_Maximum__c=4, 
                family='Passive Vulnerability Scanner', license_type__c='Test', unit__c='Each', isactive=true, description='test', CanUseRevenueSchedule=true));    
        productstoAdd.add(new Product2(Name = 'TestProd', ProductCode='SERV-MSSP-VM-M', Family = 'Test', Category__c = 'test', Order_Category__c = 'Test', Pricing_Term__c = 'Net 30'));
        productstoAdd.add(new Product2(Name = 'TestProd1', ProductCode='SERV-MSSP-WAS-M', Family = 'Test', Category__c = 'test', Order_Category__c = 'Test', Pricing_Term__c = 'Net 30'));
        productstoAdd.add(new Product2(Name = 'TestProd2', ProductCode='SERV-MSSP-PCI-M', Family = 'Test', Category__c = 'test', Order_Category__c = 'Test', Pricing_Term__c = 'Net 30'));          
               
        insert productstoAdd;
    }   
    
    public static void insertOppProducts () {
        List<OpportunityLineItem> olisToAdd = new List<OpportunityLineItem>();
        // Create map of Product2Id and PricebookEntryId
        Map<Id,Id> prodIdPbeIdMap = new Map<Id,Id>();
        for (PricebookEntry pbe : [SELECT Id, Product2Id FROM PricebookEntry])
        {
            prodIdPbeIdMap.put(pbe.Product2Id, pbe.Id);
        }
        List<Product2> prods = [SELECT Id, Name, Order_Category__c FROM Product2 WHERE Name LIKE 'test%' AND Service_Level__c != null];
        for (Opportunity opp : [SELECT Id, Name FROM Opportunity WHERE Name LIKE 'TestOpp%']) {
            for (Integer i=0; i < 6; i++) {
                olisToAdd.add(new OpportunityLineItem(OpportunityId = opp.Id, Product2Id = prods[i].id, pricebookentryid=prodIdPbeIdMap.get(prods[i].id), unitprice=100, quantity = 1, order_category__c = prods[i].order_category__c, Start_Date__c = system.today(), End_Date__c = system.today().addYears(1)));
            }
        }
        insert olisToadd;
    }
    
    
    public static void insertPricebooks () {
        Pricebook2 stdPb = new Pricebook2(Name='Standard Price Book', isActive=true);           
        insert stdPb;                                                                               // Create the standard pricebook
        
        Id pricebookId = Test.getStandardPricebookId();
        List<Product2> productsToAdd = [SELECT Id from Product2];

        PricebookEntry[] pricestoAdd = new PricebookEntry[0];                                       // Go set the pricing for these products
        pricestoAdd.add(new PricebookEntry(pricebook2id = pricebookId, product2id = productstoAdd[0].id, unitprice=1.0, isActive=true, usestandardprice=false));
        pricestoAdd.add(new PricebookEntry(pricebook2id = pricebookId, product2id = productstoAdd[1].id, unitprice=0.0, isActive=true, usestandardprice=false));
        pricestoAdd.add(new PricebookEntry(pricebook2id = pricebookId, product2id = productstoAdd[2].id, unitprice=1.0, isActive=true, usestandardprice=false));
        pricestoAdd.add(new PricebookEntry(pricebook2id = pricebookId, product2id = productstoAdd[3].id, unitprice=1.0, isActive=true, usestandardprice=false));
        pricestoAdd.add(new PricebookEntry(pricebook2id = pricebookId, product2id = productstoAdd[4].id, unitprice=1.0, isActive=true, usestandardprice=false));
        pricestoAdd.add(new PricebookEntry(pricebook2id = pricebookId, product2id = productstoAdd[5].id, unitprice=1.0, isActive=true, usestandardprice=false));
        pricestoAdd.add(new PricebookEntry(pricebook2id = pricebookId, product2id = productstoAdd[6].id, unitprice=1.0, isActive=true, usestandardprice=false));
        pricestoAdd.add(new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = productstoAdd[7].Id, UnitPrice = 100.00, IsActive = true));
        pricestoAdd.add(new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = productstoAdd[8].Id, UnitPrice = 100.00, IsActive = true));
        pricestoAdd.add(new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = productstoAdd[9].Id, UnitPrice = 100.00, IsActive = true));
        insert pricestoAdd;
        
    }   
    
    public static void insertQuotes (Integer nbrQuotes) {
        // creates this number of quotes for each opportunity
        List<Quote__c> quotesToAdd = new List<Quote__c>();   
        List<Opportunity> opptys = [SELECT Id FROM Opportunity WHERE Name LIKE 'Test%'];       
        List<Contact> cons = [SELECT Id FROM Contact WHERE LastName LIKE 'ConTest%'];     
        for (Opportunity opp : opptys) {      
            for (Integer i=0; i < nbrQuotes; i++) {      
                Quote__c q = new Quote__c(Opportunity__c = opptys[i].id, Geography_Price_List__c = 'AMER', Billing_Contact__c = cons[i].id, Licensee_Contact__c = cons[i].id, Primary__c=FALSE);  
                if (i == 0) {
                    q.Primary__c = TRUE;
                }
                quotesToAdd.add(q);
            }
        }
        insert quotesToAdd;
    }
    
    public static void insertQuoteLines () {
        List<QuoteLine__c> qLinesToAdd = new List<QuoteLine__c>();   
        List<Quote__c> quotes = [SELECT Id FROM Quote__c];       
        for (Quote__c quote : quotes) {           
            QuoteLine__c ql = new QuoteLine__c(quote__c=quote.id, quantity__c=1, category__c='Products', family__c='Nessus Professional', license_type__c='Test', 
                    product_name__c = 'test', unit__c='Each', term__c=12.0);  
            qlinesToAdd.add(new QuoteLine__c(quote__c=quote.id, quantity__c=1, category__c='Products', family__c='Nessus Cloud', license_type__c='Test', 
                        product_name__c = 'test', unit__c='Each', term__c=12.0));  
            qLinesToAdd.add(new QuoteLine__c(quote__c=quote.id, quantity__c=1, category__c='Products', family__c='Nessus Manager', license_type__c='Test', 
                        product_name__c = 'test', unit__c='Each', term__c=12.0));  
            qLinesToAdd.add(new QuoteLine__c(quote__c=quote.id, quantity__c=1, category__c='Products', family__c='SecurityCenter', license_type__c='Test', 
                        product_name__c = 'test', unit__c='Each', term__c=12.0));  
            qLinesToAdd.add(new QuoteLine__c(quote__c=quote.id, quantity__c=1, category__c='Products', family__c='SecurityCenter Continuous View', license_type__c='Test', 
                        product_name__c = 'test', unit__c='Each', term__c=12.0));  
            qLinesToAdd.add(new QuoteLine__c(quote__c=quote.id, quantity__c=1, category__c='Products', family__c='Log Correlation Engine', license_type__c='Test', 
                        product_name__c = 'test', unit__c='Each', term__c=12.0));  
            qLinesToAdd.add(new QuoteLine__c(quote__c=quote.id, quantity__c=1, category__c='Products', family__c='Passive Vulnerability Scanner', license_type__c='Test', 
                        product_name__c = 'test', unit__c='Each', term__c=12.0));  

        }
        insert qLinesToAdd;
    }
    
    public static void insertCampaigns (Integer nbrCmpgns) {
        // creates this number of campaigns
        List<Campaign> cmpgnsToAdd = new List<Campaign>();
        for (Integer i=0; i < nbrCmpgns; i++) {
            Campaign camp = new Campaign(name = 'TestCampaign' + i, event_title__c='TestEvent' + i, isActive = TRUE, type='Event', event_location__c='Test Location', event_state_province__c='CA', 
                startdate=System.today(), event_focus_country__c='US', Description='TestDesc' + i, event_type__c='Session', Number_Of_Target_Leads__c=100, Sales_Area__c='AMER',
                Sponsorship_Type__c='Industry', Synopsis__c='Test synopsis', Campaign_SubType__c = 'Other', Exclude_From_Metrics__c = true); 
            cmpgnsToAdd.add(camp);
        }
        insert cmpgnsToAdd;
    }
    
    public static void insertCases () {
        List<Account> accts = [SELECT Id, Name FROM Account WHERE Name LIKE 'Dell%'];
        List<Case> casesToAdd = new List<Case>();
        casesToAdd.add(new Case(Priority='P3 - Medium', Subject='Testing JIRA Case', Description='More Testing JIRA', Status='On Hold', AccountId=accts[0].Id, Product__c='SecurityCenter')); 
        casesToAdd.add(new Case(Subject='Testing JIRA Case', Description='More Testing JIRA'));
        casesToAdd.add(new Case(Priority='P3 - Medium', Subject='Testing JIRA Case', Description='More Testing JIRA', Status='On Hold', AccountId=accts[0].Id, Type = 'Test'));
        casesToAdd.add(new Case(Priority='P3 - Medium', Subject='Testing JIRA Case', Description='More Testing JIRA', Status='On Hold', AccountId=accts[0].Id, Type = 'Test', Jira_Case_Number__c = 'CSDEV-44',Product__c = 'Channel Partner')); 
        casesToAdd.add(new Case(Priority='P3 - Medium', Subject='Testing JIRA Case', Description='More Testing JIRA', Status='On Hold', AccountId=accts[0].Id, Type = 'Test', Product__c = 'Nessus Cloud', Jira_Type__c = 'Query'));
        insert casesToAdd;
    } 

    // Create data without inserting
    
    public static List<User> createUsers (List<String> profileNames) {
        List<Profile> profiles = [select id, name from Profile]; 
        Map<String, Id> profileMap = new Map<String, Id>();
        Id profileId; 
        List<User> usersToAdd = new List<User>();
        
        for (Profile p : profiles) {
            profileMap.put(p.Name, p.Id);
        }
        for (Integer i=0; i < profileNames.size(); i++) {
            User newUser = new User(alias = 'user' + i, email='user' + i + '@testemail.com', 
            emailencodingkey='UTF-8', firstname = 'User' + i, lastname='Test', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = profileMap.get(profileNames[i]), 
            timezonesidkey='America/Los_Angeles', username='user' + i + '@testemail.com');
            
            usersToAdd.add(newUser);
        }

        return usersToAdd;
    }
    
    public static List<Account> createAccounts (Integer nbrAccts) {
        List<Account> acctsToadd = new List<Account>();
        Utils.isTest = true;
        accountTriggerHandler.enforceCountry = false;
        for (Integer i=0; i < nbrAccts; i++) {
            Account acct = new Account(Name='Test' + i + 'Inc', BillingState='CA', BillingCountry='US', website='http://www.test' + i + '.com');
            acctsToAdd.add(acct);
        }
        acctsToAdd.add(new Account(Name='Dell SecureWorks', BillingState='CA', BillingCountry='US', website='http://www.test1.com'));

        return acctsToAdd;
    }
    
    public static List<Contact> createContacts (List<Account> accts, Integer nbrCons) {
        // creates this number of contacts for each account
        List<Contact> consToadd = new List<Contact>();
        for (Account acct : accts) {
            for (Integer i=0; i < nbrCons; i++) {
                Contact con = new Contact(firstname='Con' + i,lastname='Con' + acct.Name, accountid=acct.id, email= 'Con' + i + acct.Name + '@test.com', MailingCountry='US', MailingState='CA');
                consToAdd.add(con);
            }
        }
        return consToAdd;
    }
    
    
    public static List<Opportunity> createOpportunities (List<Account> accts,Integer nbrOpptys) {
        // creates this number of opportunities for each account
        List<Opportunity> opptystoAdd = new List<Opportunity>();   
        for (Account acct : accts) {
            for (Integer i=0; i < nbrOpptys; i++) {
                Opportunity opp = new Opportunity(name='TestOpp' + i + acct.Name, accountid = accts[0].id, StageName='Proposal Submitted', Purchase_Order_Number__c = '1234' + i, CloseDate=Date.newInstance(2015,10,10));
                opptysToAdd.add(opp);
            }
        }
        return opptysToAdd;
    }
    
    public static List<Product2> createProducts () {
        //Pricebook2 stdPb = new Pricebook2(Name='Standard Price Book', isActive=true, CurrencyISOCode = 'USD');            
        //insert stdPb;                                                                             // Create the standard pricebook

        Product2[] productstoAdd = new Product2[0];                                                 // Go create some test products to use
        
        productstoAdd.add(
            new Product2(name='test', geography__c = 'AMER', category__c='Products', Tier_Minimum__c=0, Tier_Maximum__c=4, 
                family='Nessus Professional', license_type__c='Test', unit__c='Each', isactive=true, description='test', CanUseRevenueSchedule=true));      
        productstoAdd.add(
            new Product2(name='test', geography__c = 'AMER', category__c='Products', Tier_Minimum__c=0, Tier_Maximum__c=4, 
                family='Nessus Cloud', license_type__c='Test', unit__c='Each', isactive=true, description='test', CanUseRevenueSchedule=true));     
        productstoAdd.add(
            new Product2(name='test', geography__c = 'AMER', category__c='Products', Tier_Minimum__c=0, Tier_Maximum__c=4, 
                family='Nessus Manager', license_type__c='Test', unit__c='Each', isactive=true, description='test', CanUseRevenueSchedule=true));       
        productstoAdd.add(
            new Product2(name='test', geography__c = 'AMER', category__c='Products', Tier_Minimum__c=0, Tier_Maximum__c=4, 
                family='SecurityCenter', license_type__c='Test', unit__c='Each', isactive=true, description='test', CanUseRevenueSchedule=true));       
        productstoAdd.add(
            new Product2(name='test', geography__c = 'AMER', category__c='Products', Tier_Minimum__c=0, Tier_Maximum__c=4, 
                family='SecurityCenter Continuous View', license_type__c='Test', unit__c='Each', isactive=true, description='test', CanUseRevenueSchedule=true));       
        productstoAdd.add(
            new Product2(name='test', geography__c = 'AMER', category__c='Products', Tier_Minimum__c=0, Tier_Maximum__c=4, 
                family='Log Correlation Engine', license_type__c='Test', unit__c='Each', isactive=true, description='test', CanUseRevenueSchedule=true));       
        productstoAdd.add(
            new Product2(name='test', geography__c = 'AMER', category__c='Products', Tier_Minimum__c=0, Tier_Maximum__c=4, 
                family='Passive Vulnerability Scanner', license_type__c='Test', unit__c='Each', isactive=true, description='test', CanUseRevenueSchedule=true));    
        productstoAdd.add(new Product2(Name = 'TestProd', ProductCode='SERV-MSSP-VM-M', Family = 'Test', Category__c = 'test', Order_Category__c = 'Test', Pricing_Term__c = 'Net 30'));
        productstoAdd.add(new Product2(Name = 'TestProd1', ProductCode='SERV-MSSP-WAS-M', Family = 'Test', Category__c = 'test', Order_Category__c = 'Test', Pricing_Term__c = 'Net 30'));
        productstoAdd.add(new Product2(Name = 'TestProd2', ProductCode='SERV-MSSP-PCI-M', Family = 'Test', Category__c = 'test', Order_Category__c = 'Test', Pricing_Term__c = 'Net 30'));          
                
        return productstoAdd;
        
    }   
    
    public static List<PricebookEntry> createPricebooks (List<Product2> productstoAdd) {

        Id pricebookId = Test.getStandardPricebookId();
        productsToAdd = [SELECT Id from Product2];

        PricebookEntry[] pricestoAdd = new PricebookEntry[0];                                       // Go set the pricing for these products
        pricestoAdd.add(new PricebookEntry(pricebook2id = pricebookId, product2id = productstoAdd[0].id, unitprice=1.0, isActive=true, usestandardprice=false));
        pricestoAdd.add(new PricebookEntry(pricebook2id = pricebookId, product2id = productstoAdd[1].id, unitprice=0.0, isActive=true, usestandardprice=false));
        pricestoAdd.add(new PricebookEntry(pricebook2id = pricebookId, product2id = productstoAdd[2].id, unitprice=1.0, isActive=true, usestandardprice=false));
        pricestoAdd.add(new PricebookEntry(pricebook2id = pricebookId, product2id = productstoAdd[3].id, unitprice=1.0, isActive=true, usestandardprice=false));
        pricestoAdd.add(new PricebookEntry(pricebook2id = pricebookId, product2id = productstoAdd[4].id, unitprice=1.0, isActive=true, usestandardprice=false));
        pricestoAdd.add(new PricebookEntry(pricebook2id = pricebookId, product2id = productstoAdd[5].id, unitprice=1.0, isActive=true, usestandardprice=false));
        pricestoAdd.add(new PricebookEntry(pricebook2id = pricebookId, product2id = productstoAdd[6].id, unitprice=1.0, isActive=true, usestandardprice=false));
        pricestoAdd.add(new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = productstoAdd[7].Id, UnitPrice = 100.00, IsActive = true));
        pricestoAdd.add(new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = productstoAdd[8].Id, UnitPrice = 100.00, IsActive = true));
        pricestoAdd.add(new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = productstoAdd[9].Id, UnitPrice = 100.00, IsActive = true));
        return pricestoAdd;
        
    }   
    
    
    public static List<Quote__c> createQuotes (List<Opportunity> opptys, List<Contact> cons, Integer nbrQuotes) {
        // creates this number of quotes for each opportunity
        List<Quote__c> quotesToAdd = new List<Quote__c>();      
        for (Opportunity opp : opptys) {      
            for (Integer i=0; i < nbrQuotes; i++) {      
                Quote__c q = new Quote__c(Opportunity__c = opptys[i].id, Geography_Price_List__c = 'AMER', Billing_Contact__c = cons[i].id, Licensee_Contact__c = cons[i].id, Primary__c=FALSE);  
                if (i == 0) {
                    q.Primary__c = TRUE;
                }
                quotesToAdd.add(q);
            }
        }
        return quotesToAdd;
    }
    
    public static List<QuoteLine__c> insertQuoteLines (List<Quote__c> quotes) {
        List<QuoteLine__c> qLinesToAdd = new List<QuoteLine__c>();   
        for (Quote__c quote : quotes) {           
            QuoteLine__c ql = new QuoteLine__c(quote__c=quote.id, quantity__c=1, category__c='Products', family__c='Nessus Professional', license_type__c='Test', 
                    product_name__c = 'test', unit__c='Each', term__c=12.0);  
            qlinesToAdd.add(new QuoteLine__c(quote__c=quote.id, quantity__c=1, category__c='Products', family__c='Nessus Cloud', license_type__c='Test', 
                        product_name__c = 'test', unit__c='Each', term__c=12.0));  
            qLinesToAdd.add(new QuoteLine__c(quote__c=quote.id, quantity__c=1, category__c='Products', family__c='Nessus Manager', license_type__c='Test', 
                        product_name__c = 'test', unit__c='Each', term__c=12.0));  
            qLinesToAdd.add(new QuoteLine__c(quote__c=quote.id, quantity__c=1, category__c='Products', family__c='SecurityCenter', license_type__c='Test', 
                        product_name__c = 'test', unit__c='Each', term__c=12.0));  
            qLinesToAdd.add(new QuoteLine__c(quote__c=quote.id, quantity__c=1, category__c='Products', family__c='SecurityCenter Continuous View', license_type__c='Test', 
                        product_name__c = 'test', unit__c='Each', term__c=12.0));  
            qLinesToAdd.add(new QuoteLine__c(quote__c=quote.id, quantity__c=1, category__c='Products', family__c='Log Correlation Engine', license_type__c='Test', 
                        product_name__c = 'test', unit__c='Each', term__c=12.0));  
            qLinesToAdd.add(new QuoteLine__c(quote__c=quote.id, quantity__c=1, category__c='Products', family__c='Passive Vulnerability Scanner', license_type__c='Test', 
                        product_name__c = 'test', unit__c='Each', term__c=12.0));  

        }
        return qLinesToAdd;
    }
    
    public static List<Campaign> createCampaigns (Integer nbrCmpgns) {
        // creates this number of campaigns
        List<Campaign> cmpgnsToAdd = new List<Campaign>();
        for (Integer i=0; i < nbrCmpgns; i++) {
            Campaign camp = new Campaign(name = 'TestCampaign' + i, event_title__c='TestEvent' + i, isActive = TRUE, type='Event', event_location__c='Test Location', event_state_province__c='CA', 
                startdate=System.today(), event_focus_country__c='US', Description='TestDesc' + i, event_type__c='Session', Number_Of_Target_Leads__c=100, Sales_Area__c='AMER',
                Sponsorship_Type__c='Industry', Synopsis__c='Test synopsis', Exclude_From_Metrics__c=true, Campaign_SubType__c='other'); 
            cmpgnsToAdd.add(camp);
        }
        return cmpgnsToAdd;
    }
    
    
    public static List<Case> createCases(List<Account> accts ) {
        List<Case> casesToAdd = new List<Case>();
        casesToAdd.add(new Case(Priority='P3 - Medium', Subject='Testing JIRA Case', Description='More Testing JIRA', Status='On Hold', AccountId=accts[0].Id, Product__c='SecurityCenter')); 
        casesToAdd.add(new Case(Subject='Testing JIRA Case', Description='More Testing JIRA'));
        casesToAdd.add(new Case(Priority='P3 - Medium', Subject='Testing JIRA Case', Description='More Testing JIRA', Status='On Hold', AccountId=accts[0].Id, Type = 'Test'));
        casesToAdd.add(new Case(Priority='P3 - Medium', Subject='Testing JIRA Case', Description='More Testing JIRA', Status='On Hold', AccountId=accts[0].Id, Type = 'Test', Jira_Case_Number__c = 'CSDEV-44',Product__c = 'Channel Partner')); 
        casesToAdd.add(new Case(Priority='P3 - Medium', Subject='Testing JIRA Case', Description='More Testing JIRA', Status='On Hold', AccountId=accts[0].Id, Type = 'Test', Product__c = 'Nessus Cloud', Jira_Type__c = 'Query'));

        return casesToAdd;
    } 
}