public with sharing class DeliveryElementAllocationsController {

    ApexPages.StandardController controller;
    private KimbleOne__DeliveryGroup__c deliveryGroup;
    public list<KimbleOne__DeliveryElement__c> TheDeliveryElements{get; set;}

    public DeliveryElementAllocationsController (ApexPages.StandardController stdController)
    {
        controller = stdController;

        deliveryGroup = [SELECT Id, KimbleOne__Proposal__r.KimbleOne__Opportunity__c FROM KimbleOne__DeliveryGroup__c WHERE Id = :controller.getId()];

        GetTheDeliveryElements();
    }

    private void GetTheDeliveryElements()
    {
      TheDeliveryElements = [SELECT Id, Name,KimbleOne__OriginatingProposal__r.KimbleOne__Opportunity__c, (SELECT Name,ProductCode,TotalPrice FROM OpportunityLineItems__r LIMIT 1)
                                    FROM KimbleOne__DeliveryElement__c
                                    WHERE KimbleOne__DeliveryGroup__c = :deliveryGroup.Id
                                    ORDER BY Name];

    }

    public KimbleOne__DeliveryElement__c theDeliveryElement {get; set;}

    public list<SelectOption> OppLineItemsForAlloc {get; set;}


    public void getElementForAlloc()
    {
        string elementId = ApexPages.currentPage().getParameters().get('deId');
        string lineDesc ='';

        theDeliveryElement = [SELECT Id, Name, KimbleOne__OriginatingProposal__r.KimbleOne__Opportunity__c,OpportunityLineItem__c, KimbleOne__Product__r.SF_ProductCode__c, KimbleOne__Product__r.KimbleOne__ProductDomain__c FROM KimbleOne__DeliveryElement__c WHERE Id = :elementId][0];

        // OpportunityLineItems that have the same PS Product Id and have not been allocated
        OppLineItemsForAlloc = new list<SelectOption>();

        if (theDeliveryElement.KimbleOne__OriginatingProposal__r.KimbleOne__Opportunity__c !=null)  //  handles original Elements or COs belonging to their own Opportunity
        {
        OppLineItemsForAlloc.add(new SelectOption('',''));
        for (OpportunityLineItem oppLineItem : [SELECT Id, Name, ProductCode, TotalPrice, CurrencyIsoCode FROM OpportunityLineItem
                                                    WHERE
                                                    OpportunityId = :theDeliveryElement.KimbleOne__OriginatingProposal__r.KimbleOne__Opportunity__c
                                                    AND ProductCode = :theDeliveryElement.KimbleOne__Product__r.SF_ProductCode__c
                                                    AND (DeliveryElement__c = null OR DeliveryElement__c = :theDeliveryElement.id)
                                                    ORDER BY Name])
        {
            if (oppLineItem.ProductCode != null)
            {
               lineDesc = oppLineItem.Name + ' - ' + oppLineItem.ProductCode + ' - ' + oppLineItem.CurrencyIsoCode + ' ' + string.valueOf(oppLineItem.TotalPrice);
               OppLineItemsForAlloc.add(new SelectOption(oppLineItem.Id, lineDesc));
             }

        }
        }
        else    // handles CO elements that belong to the original Opportunity
        {
        OppLineItemsForAlloc.add(new SelectOption('',''));
        for (OpportunityLineItem oppLineItem : [SELECT Id, Name, ProductCode, TotalPrice, CurrencyIsoCode FROM OpportunityLineItem
                                                    WHERE
                                                    OpportunityId = :DeliveryGroup.KimbleOne__Proposal__r.KimbleOne__Opportunity__c
                                                    AND ProductCode = :theDeliveryElement.KimbleOne__Product__r.SF_ProductCode__c
                                                    AND (DeliveryElement__c = null OR DeliveryElement__c = :theDeliveryElement.id)
                                                    ORDER BY Name])
        {
            if (oppLineItem.ProductCode != null)
            {
               lineDesc = oppLineItem.Name + ' - ' + oppLineItem.ProductCode + ' - ' + oppLineItem.CurrencyIsoCode + ' ' + string.valueOf(oppLineItem.TotalPrice);
               OppLineItemsForAlloc.add(new SelectOption(oppLineItem.Id, lineDesc));
             }

        }
        }

    }

    public PageReference SaveElementAllocation()
    {
        try
        {
            KimbleOne__DeliveryElement__c oldElementAlloc = [SELECT OpportunityLineItem__c FROM KimbleOne__DeliveryElement__c WHERE Id = :theDeliveryElement.Id][0];


               update theDeliveryElement;

            // Save lookup from OpportunityLineItem to Element (based on Id string)
            if (theDeliveryElement.OpportunityLineItem__c != null)
            {
                OpportunityLineItem theOppLineItem = [SELECT Id FROM OpportunityLineItem WHERE Id = :theDeliveryElement.OpportunityLineItem__c][0];
                theOppLineItem.DeliveryElement__c = theDeliveryElement.Id;

                update theOppLineItem;

            }

            // Remove previous allocation to OpportunityLineItem
            if (oldElementAlloc.OpportunityLineItem__c != null && oldElementAlloc.OpportunityLineItem__c != theDeliveryElement.OpportunityLineItem__c)
            {
                OpportunityLineItem oldOppLineItem = [SELECT Id FROM OpportunityLineItem WHERE Id = :oldElementAlloc.OpportunityLineItem__c][0];
                oldOppLineItem.DeliveryElement__c = null;

                update oldOppLineItem;
            }




            GetTheDeliveryElements();

            return null;
        }
        catch (Exception e)
        {
            ApexPages.addMessages(e);
            return null;
        }
    }

}