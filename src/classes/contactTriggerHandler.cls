public with sharing class contactTriggerHandler 
{
/*******************************************************************************
*
* Contact Trigger Handler Class
* Version 1.0a
* Copyright 2006-2014 (c) by CloudLogistix.com
* All Rights Reserved
* Your are allowed to make modifications only for yourself, not for distribution
* without permission from CloudLogistix.
*
*******************************************************************************/
    /*****************************************************************************************
    *   Constant Declaration
    ******************************************************************************************/
    public static Boolean enforceCountry = FALSE;
    public static Boolean requireCountry = FALSE;
    public static Boolean checkPhones = FALSE;
    public static Boolean dupeEmailPrevent = FALSE;
    public static Boolean dupeEmailWarn = FALSE;
    public static Boolean firstNameCapitalize = FALSE;
    public static Boolean lastNameCapitalize = FALSE;
    public static Boolean DigitISOCheck2 = TRUE;
    public static Boolean DigitISOCheck3 = FALSE;
    public static Boolean ignoreProfiles = FALSE;
    public static Boolean campaignMetrics = FALSE;
    public static String ignoreProfileIds = '';
    public static String ignoreUserIds = '';
    public static final String MULESOFT_USER = '00560000006W7gq';   

    /*****************************************************************************************
    *   Variable Initialization
    ******************************************************************************************/
    public static List <String> errorMessages = utils.validationerrs();                             // List of potential error messages
    public static Map <String, String> countryfixes = utils.countryfixes();                         // List of country spellings we can try to auto-fix

    /*****************************************************************************************
    *   ISO Country Code Evaluation
    ******************************************************************************************/
    public static ISOCountryCodes__c isCountryMatch;                                                // Store a match if we find it
    public static Map <String, ISOCountryCodes__c> countries = utils.countries();                   // Map of Countries by Name
    public static Map <String, ISOCountryCodes__c> cc = utils.countrycodes();                       // Map of Countries by ISO Code

    /*****************************************************************************************
    *   US and CA Postal Code Evaluation
    ******************************************************************************************/
    public static Map <String, PostalCodes__c> sm = utils.statecodes();                             // Define Map based on the State Code
    public static Map <String, PostalCodes__c> states = utils.states();                             // Define Map based on the State Name
    public static Set <String> Country = utils.countrymatches();                                    // Define a set based on the countries we'll check for
    public static PostalCodes__c isStateMatch;  
    
    public static Id currUserId;
	public static Map <Id, User> userMap;                                                    // Store a match if we find it

    public class ContactBeforeInsertHandler implements Triggers.Handler 
    {
        public void handle() 
        {
            TriggerConstants();
            GetUserData();
            UpdateSalesFields(Trigger.new);
            ProcessChecks(Trigger.new);
            for (sObject so : Trigger.new) 
            {
                Contact ct = (contact)so;
                ct.MM_First_Activity_Id__c = null;
                ct.MM_First_Activity__c = null;
                ct.MM_First_Activity_Date__c = null;
                ct.MM_Most_Recent_Activity_Id__c = null;
                ct.MM_Most_Recent_Activity__c = null;
                ct.MM_Most_Recent_Activity_Date__c = null;
            }
        }
    } 
    
    public class ContactBeforeUpdateHandler implements Triggers.Handler 
    {
        public void handle() 
        {
            TriggerConstants();
            GetUserData();
            UpdateSalesFields(Trigger.new);
            ProcessChecks(Trigger.new);
        }
    } 
    
    public class ContactAfterInsertHandler implements Triggers.Handler 
    {
        public void handle() 
        {
            ProcessContactNotes(Trigger.new);
            ProcessEvalRecords(Trigger.new, null);
        }
    }
    
    public class ContactAfterUpdateHandler implements Triggers.Handler 
    {
        public void handle() 
        {
            ProcessContactNotes(Trigger.new);
            UpdateAccounts(Trigger.new, Trigger.oldMap);
            ProcessEvalRecords(Trigger.new, Trigger.oldMap);
            ContactSyncWithNetSuite(Trigger.new, Trigger.oldMap);
        }
    }

    /*****************************************************************************************
    *   Common Functions
    ******************************************************************************************/
    public static void TriggerConstants() 
    {
        if (!utils.isTest) 
        {
            enforceCountry = utils.marketingappsettings().get('ContactEnforceCountry').Enabled__c;
            requireCountry = utils.marketingappsettings().get('ContactRequireCountry').Enabled__c;
            checkPhones = utils.marketingappsettings().get('ContactCheckPhones').Enabled__c;
            dupeEmailPrevent = utils.marketingappsettings().get('ContactEmailDuplicatePrevent').Enabled__c;
            dupeEmailWarn = utils.marketingappsettings().get('ContactEmailDuplicateWarn').Enabled__c;
            firstNameCapitalize = utils.marketingappsettings().get('ContactFirstNameCapitalize').Enabled__c;
            lastNameCapitalize = utils.marketingappsettings().get('ContactLastNameCapitalize').Enabled__c;
            DigitISOCheck2 = utils.marketingappsettings().get('2DigitISOCodes').Enabled__c;
            DigitISOCheck3 = utils.marketingappsettings().get('3DigitISOCodes').Enabled__c;
            campaignMetrics = utils.marketingappsettings().get('ContactSaveCampaign').Enabled__c;
            ignoreProfiles = utils.marketingappsettings().get('SystemAdminBypassChecks').Enabled__c;
            ignoreProfileIds = utils.marketingappsettings().get('SystemAdminBypassChecks').ProfileIds__c;
            ignoreUserIds = utils.marketingappsettings().get('SystemAdminBypassChecks').UserIds__c; 
        }
    }  
    
    public static void GetUserData() {
        Set<Id> userIds = new Set<Id>();
        currUserId = UserInfo.getUserID();
        
        if (currUserId != null)
        {
            userIds.add(currUserId);
        }

		userMap = new Map<Id,User>([SELECT Id, Sales_Classification__c FROM User WHERE Id IN :userIds]);
    }
    
    public static void UpdateSalesFields(Contact[] contacts) 
    {
        if (Utils.GetSalesClass(currUserId,userMap) == true)
        {
            for (Contact cont : contacts)
            {
                cont.Last_Modified_By_Sales__c = currUserId;
                cont.Last_Modified_By_Sales_Date__c = DateTime.now();
            }
        }
    }   

    public static void ProcessChecks(Contact[] cntcts) 
    {
        /*****************************************************************************************
        *   Variable Initialization
        ******************************************************************************************/
        Map <Id, CampaignMember> firstMatches = new Map <Id, CampaignMember>();                     // Store all First Campaign Matches
        Map <Id, CampaignMember> lastMatches = new Map <Id, CampaignMember>();                      // Store all Last Campaign Matches
        Set <String> contactids = new Set <String>();                                               // Store all ids we're processing
        Set <String> names = new Set <String>();
        Set <String> dupes = new Set <String>();
        Set <String> ignores = new set <String>();

        if ((!ignoreProfiles) ||
            ((ignoreProfileIds == null) || (ignoreProfileIds == '')) ||
            (!ignoreUserIds.contains(UserInfo.getUserId().substring(0,14))) ||
            (!ignoreProfileIds.contains(UserInfo.getProfileId().substring(0,14)))) {
    
            /*****************************************************************************************
            *   Record Pre-Processing
            ******************************************************************************************/
            for (Contact cntct : cntcts) 
            {                                                          // Loop thru all records being processed
                if (cntct.email != null) 
                {
                    names.add(cntct.email);
                    ignores.add(cntct.id+':'+cntct.email);                                          // Make sure we're not comparing with ourselves
                }
                contactids.add(cntct.id);                                                           // Store the id of the records we're processing
            }
            dupes = Utils.isDuplicateEmail(names, ignores);                                         // Go get if there are any duplicate records

            CampaignMember[] cmembs =                                                               // Go get all Campaign Membership records for these contacts
                ([SELECT ContactId, CampaignId, CreatedDate, FirstRespondedDate //, Campaign.Name, Campaign.Type
                FROM CampaignMember
                WHERE ContactId != null
                AND ContactId IN :contactids
                AND IsDeleted=FALSE
                AND HasResponded = TRUE
                ORDER BY ContactId, CreatedDate ASC]);
            for (CampaignMember cm : cmembs) 
            {                                                      // Loop thru the retrieved records
                if ((firstMatches == null) ||                                                       // Do we have first matches already?
                    (firstMatches.get(cm.contactid) == null)) 
                {                                     // or a match for this contact already?
                    firstMatches.put(cm.contactid, cm);                                             // No so store it as a first match
                }
                lastMatches.put(cm.contactid, cm);                                                  // Store the last matched campaign record
            }
            
            /*****************************************************************************************
            *   Record Processing
            ******************************************************************************************/
            for (Contact cntct : cntcts) 
            {

                /*****************************************************************************************
                *   Campaign Member Metrics
                ******************************************************************************************/
                if (campaignMetrics) 
                {                                                              // Are we processing campaign metrics
                    cntct.MM_First_Campaign_Source__c = null;                                       // Clear out the metrics
                    cntct.MM_First_Campaign_Source_Date__c = null;
                    cntct.MM_Most_Recent_Campaign_Source__c = null;
                    cntct.MM_Most_Recent_Campaign_Source_Date__c = null;
                    if ((firstMatches != null) &&
                        (firstMatches.size() > 0) &&
                        (firstMatches.get(cntct.id) != null)) 
                    {
                        cntct.MM_First_Campaign_Source__c = firstMatches.get(cntct.id).CampaignId;
                        cntct.MM_First_Campaign_Source_Date__c = Date.valueof(firstMatches.get(cntct.id).CreatedDate);
                        //cntct.MM_First_Campaign_Name__c = firstMatches.get(cntct.id).Campaign.Name;
                        //cntct.MM_First_Campaign_Type__c = firstMatches.get(cntct.id).Campaign.Type;
                    }
                    if ((lastMatches != null) &&
                        (lastMatches.size() > 0) &&
                        (lastMatches.get(cntct.id) != null)) 
                    {
                        cntct.MM_Most_Recent_Campaign_Source__c = lastMatches.get(cntct.id).CampaignId;
                        cntct.MM_Most_Recent_Campaign_Source_Date__c = Date.valueof(lastMatches.get(cntct.id).CreatedDate);
                        //cntct.MM_Most_Recent_Campaign_Name__c = lastMatches.get(cntct.id).Campaign.Name;
                        //cntct.MM_Most_Recent_Campaign_Type__c = lastMatches.get(cntct.id).Campaign.Type;
                    }
                }

                /*****************************************************************************************
                *   First Name/Last Name fixing
                ******************************************************************************************/
                if (firstNameCapitalize) 
                {
                    if (cntct.firstname != null) {cntct.firstname = cntct.firstname.capitalize();}
                }
                if (lastNameCapitalize) 
                {
                    if (cntct.lastname != null) {cntct.lastname = cntct.lastname.capitalize();}
                }

                /*****************************************************************************************
                *   Email Opt-out fixing
                ******************************************************************************************/
                if (cntct.HasOptedOutOfEmail) 
                {
                    cntct.Opt_In_Cookies__c = false;
                    cntct.Opt_In_Enterprise_Newsletter__c = false;
                    cntct.Opt_In_Event_Invitations__c = false;
                    cntct.Opt_In_Nessus_Newsletter__c = false;
                    cntct.Opt_In_Product_Bulletins__c = false;
                }

                /*****************************************************************************************
                *   Marketing Metrics
                ******************************************************************************************/
                if ((cntct.LeadSource != null) &&                                                   // If our Lead Source is not NULL
                    (cntct.LeadSource != '')) 
                {                                                     // AND Empty
                    cntct.MM_Most_Recent_Lead_Source__c = cntct.LeadSource;                         // Then set the Most Recent Lead Source value
                    if ((cntct.MM_First_Lead_Source__c == null) ||                                  // And if the First Lead Source was NULL
                        (cntct.MM_First_Lead_Source__c == '')) 
                    {                                    // OR Empty
                        cntct.MM_First_Lead_Source__c = cntct.LeadSource;                           // Then save it as our First Lead Source
                        if (((cntct.Lead_Source_Detail__c != null) &&                               // Do we have a Lead Source?
                            (cntct.Lead_Source_Detail__c != '')) &&
                            (cntct.MM_First_Lead_Source_Detail__c == null) ||                       // And if we dont have the First Lead Source Detail
                            (cntct.MM_First_Lead_Source_Detail__c == '')) 
                        {                         //
                            cntct.MM_First_Lead_Source_Detail__c = cntct.Lead_Source_Detail__c;     // Then set that too
                        }
                    }
                }
    
                if ((cntct.Lead_Source_Detail__c != null) &&                                        // Do we have a Lead Source Detail?
                    (cntct.Lead_Source_Detail__c != '')) 
                {
                    cntct.MM_Most_Recent_Lead_Source_Detail__c = cntct.Lead_Source_Detail__c;       // Save it as our most recent one
                }

                /*****************************************************************************************
                *   Duplicate email checking
                ******************************************************************************************/
                if (dupes.contains(cntct.email)) 
                {
                    if (dupeEmailPrevent) 
                    {
                        cntct.email.addError('This email address already exists as an existing lead or contact');
                    }
                    else if (dupeEmailWarn) 
                    {
                        cntct.isDuplicate__c = true;
                    }
                    else 
                    {
                        cntct.isDuplicate__c = false;
                    }
                }
                else 
                {
                    cntct.isDuplicate__c = false;
                }                   

                /*****************************************************************************************
                *   State/Country Code checking
                ******************************************************************************************/
                if (enforceCountry) 
                {
                    if (cntct.mailingcountry == null) 
                    {                                                 // We want to require a mailingcountry
                        if (requireCountry) 
                        {
                            cntct.mailingcountry.addError('You must provide a mailing country');        // Error if there isn't one
                        }
                    }
                    else 
                    {                                                                              // If there's a country code ...
                        if (countryFixes.containsKey(cntct.mailingcountry)) 
                        {                           // If it's one of the ones we can easily fix
                            cntct.mailingcountry = countryFixes.get(cntct.mailingcountry);              // ... then update it to the fixed one
                        }
                        string[] parts = cntct.mailingcountry.tolowercase().split(' ');
                        string newparts = '';
                        
                        for (Integer i=0; i < parts.size(); i++) 
                        {
                            newparts += parts[i].capitalize() + ' ';
                        }
                        
                        if (newparts.endswith(' ')) {newparts = newparts.substring(0, newparts.length() - 1);}
                        
                        if (countries.get(newparts) != null) 
                        {                                          // Second time to see if we can auto-fix it first
                            cntct.mailingcountry = countries.get(newparts).Country_Code__c;
                        }
                        
                        isCountryMatch = cc.get(cntct.mailingcountry);  // Go find it in the Custom Settings
                                                        
                        if (isCountryMatch == null) 
                        {
                            // No match means incorrect ISO Code
                            cntct.mailingcountry.addError('Mailing '+errorMessages[0]);
                        }                
                        else 
                        {                                                                          // Found a matching ISO Country Code
                            if (Country.contains(cntct.mailingcountry)) 
                            {                               // Is this a country that we care about the states?
                                if ((cntct.MailingState != null) &&
                                    states.get(cntct.mailingcountry+':'+cntct.mailingstate.tolowercase()) != null) 
                                {    // Second time to see if we can auto-fix it first
                                    cntct.mailingstate = states.get(cntct.mailingcountry+':'+cntct.mailingstate.tolowercase()).State_Code__c;
                                }
                                
                                isStateMatch = sm.get(cntct.mailingcountry+':'+cntct.mailingstate); // Try to match the country and state combination
                                                    
                                if (isStateMatch == null) 
                                {
                                    if(cntct.mailingCountry != 'GB')
                                    {                                             
                                        // Found a state match, now go ...
                                        if (cntct.mailingstate != null) 
                                        {
                                            cntct.mailingstate.addError('Mailing '+errorMessages[1]);
                                        }      // Is there a state but no state match?
                                    }
                                }
                                if (checkPhones) 
                                {
                                    if (Utils.fixphone(cntct.phone) == 'fail') 
                                    {                            
                                        // For US, CA - check phone format
                                        cntct.phone.addError(errorMessages[2]);
                                    }                            
                                        // ... return message if incorrect
                                    if (Utils.fixphone(cntct.fax) == 'fail') 
                                    {                              
                                    // For US, CA - check fax format
                                        cntct.fax.addError(errorMessages[3]);
                                    }                              
                                    // ... return message if incorrect
                                }
                            }
                        }
                    }
            
                    if (cntct.othercountry != null) 
                    {
                        if (countryFixes.containsKey(cntct.othercountry)) 
                        {                             // If it's one of the ones we can easily fix
                            cntct.othercountry = countryFixes.get(cntct.othercountry);                  // ... then update it to the fixed one
                        }
                        if (countries.get(cntct.othercountry) != null) 
                        {                                // Second time to see if we can auto-fix it first
                            cntct.othercountry = countries.get(cntct.othercountry).Country_Code__c;
                        }
                        isCountryMatch = cc.get(cntct.othercountry);                                    // If there's a country code ...
                        if (isCountryMatch == null) 
                        {
                            cntct.othercountry.addError('Other '+errorMessages[0]);
                        }
                        else 
                        {
                            if (Country.contains(cntct.othercountry)) 
                            {                                 // Is this a country that we care about the states?
                                if (states.get(cntct.othercountry+':'+cntct.otherstate) != null) 
                                {      // Second time to see if we can auto-fix it first
                                    cntct.otherstate = states.get(cntct.othercountry+':'+cntct.otherstate).State_Code__c;
                                }
                                isStateMatch = 
                                    sm.get(cntct.othercountry + ':' + cntct.otherstate);                // Try to match the country and state combination
                                if (isStateMatch == null && cntct.otherstate != null) 
                                {                 // Is there a state but no state match?
                                    cntct.otherstate.addError('Other '+errorMessages[1]);
                                }
                            }
                        }
                    }
                }
                
                // Replace Process Builder: On Contact Create and Update
                if (cntct.Support_Entitled_User__c == true)
                {
                	cntct.Chat_Support__c = true;
                }
                
                if (cntct.No_Longer_With_Company__c == true)
                {
                	cntct.isInactive__c = true;
                }
                
                if (cntct.Support_Designated_Admin__c == true)
                {
                	cntct.Add_Contacts__c = true;
                	cntct.Critical_Contact__c = true;
                }
            }
        }
    }
    
    public static void ProcessContactNotes(Contact[] cts) 
    {
        
        /*****************************************************************************************
        *   Variable Initialization
        ******************************************************************************************/
        Note[] notestoAdd = new Note[0];                                                            // Store any notes we need to create
        Set <Id> ctstoUpd = new Set <Id>();

        /*****************************************************************************************
        *   Record Processing
        ******************************************************************************************/
        for (Contact ct : cts) 
        {

            /*****************************************************************************************
            *   Create associated Note Records
            ******************************************************************************************/
            if (ct.event_notes__c != null) 
            {
                notestoAdd.add(new Note(parentid = ct.id, body = ct.event_notes__c, title = 'Event Note - '+String.valueof(System.today())));
                ctstoUpd.add(ct.id);
            }
        }

        if (notestoAdd.size() > 0) 
        {
            insert notestoAdd;
        }
    }
    
    public static void ProcessEvalRecords(List<Contact> contacts, Map<Id, SObject> oldContactMap)
    {
        map<String,Contact> mapUUIDContact = new map<String,Contact>();
        
        for(Contact c: contacts)
        {
            if (Trigger.isInsert)                                               // on insert
            {
                mapUUIDContact.put(c.UUID__c,c);
            }
            else
            {
                Contact oContact = (Contact) oldContactMap.get(c.Id);
                if(c.UUID__c != oContact.UUID__c && c.UUID__c != NULL)          // if UUID has changed
                {
                    mapUUIDContact.put(c.UUID__c,c);
                }
            }
        }

        if(!mapUUIDContact.isEmpty())
        {
            map<Id,Evaluation__c> mapUpdateEvals = new map<Id,Evaluation__c>([SELECT Id,Container_UUID__c,Contact__c FROM Evaluation__c WHERE Container_UUID__c in: mapUUIDContact.keySet()]);

            if(!mapUpdateEvals.isEmpty())
            {
                for(Evaluation__c ev: mapUpdateEvals.values())
                {
                    Contact c = mapUUIDContact.get(ev.Container_uuid__c);
                    if (c != null)
                    {
                        ev.Contact__c = c.Id;
                    }
                }  
                    
                update mapUpdateEvals.values();
            }
        }
    }
    
    public static void ContactSyncWithNetSuite(List<Contact> contacts, Map<Id, SObject> oldContactMap)
    {
        Map<Id, Set<Id>> payloadMap = new Map<Id, Set<Id>>();
        
        if (!UserInfo.getUserId().contains(MULESOFT_USER))
        {
	        for (Contact con : contacts)
	        {
	            Set<id> payloadIds = new Set<id>();
	            Contact oldContact;
	            if (oldContactMap != null)
	            {
	                oldContact = (Contact)oldContactMap.get(con.Id);
	            }
	            
	            if(oldContact != null && oldContact.LMS_Customer_ID__c != null && oldContact.LMS_Customer_Contact_ID__c != null && (oldContact.LMS_Customer_Contact_ID__c != con.LMS_Customer_Contact_ID__c || oldContact.LMS_Customer_Id__c != con.LMS_Customer_Id__c ||  
	                oldContact.Purchase_Agent__c != con.Purchase_Agent__c || oldContact.Submit_Tickets__c != con.Submit_Tickets__c || oldContact.Phone_Support__c != con.Phone_Support__c || oldContact.Chat_Support__c != con.Chat_Support__c || oldContact.isInactive__c != con.isInactive__c || 
	                oldContact.Add_Contacts__c != con.Add_Contacts__c || oldContact.MailingCity != con.MailingCity || oldContact.MailingState != con.MailingState || oldContact.MailingPostalCode != con.MailingPostalCode || oldContact.MailingCountry != con.MailingCountry || 
	                oldContact.Phone != con.Phone || oldContact.HomePhone != con.HomePhone || oldContact.MobilePhone != con.MobilePhone || oldContact.Fax != con.Fax || oldContact.MailingStreet != con.MailingStreet || oldContact.FirstName != con.FirstName || oldContact.LastName != con.LastName || 
	                oldContact.Salutation != con.Salutation || oldContact.Email != con.Email || oldContact.Support_Designated_Admin__c != con.Support_Designated_Admin__c || oldContact.Support_Entitled_User__c != con.Support_Entitled_User__c))
	            {
	                payloadIds.add(con.Id);
	                payloadMap.put(con.Id, payloadIds);
	            }
	        }        
        }

        if (!payloadMap.isEmpty() && payloadMap.size() <= 10)
        {
            for (Id conId : payloadMap.keyset())
            {
            	if (!utils.isTest) 
    			{
    				try
    				{
                		mulesoftIntegrationController.futureMulesoftEventHandler(payloadMap.get(conId), conId, 'contactSync');
    				}
    				catch (Exception e) {}       
    			}
            }
        }   
    }   
    
    public static void UpdateAccounts(List<Contact> contacts, Map<Id, SObject> oldContactMap) 
    {
        /*****************************************************************************************
        *   Variable Initialization
        ******************************************************************************************/
        List<Account> accountsToUpdate = new List<Account>();
        Map<Id, List<Contact>> acctIdContactMap = new Map<Id, List<Contact>>();
        
        for(Contact con : contacts)
        {
            /* 
              Get the old object record, and check if Lattice Last Scored has changed. If so, put it in a map 
              so we only have to use 1 SOQL query to do all checks.
            */
            Contact oldCon = new Contact();
            if (oldContactMap != null) 
            {
                oldCon = (Contact)oldContactMap.get(con.Id);
            }
            if (oldCon.Lattice_Last_Scored__c !=  con.Lattice_Last_Scored__c)
            { 
                if (acctIdContactMap.containsKey(con.accountId)) 
                {
                    acctIdContactMap.get(con.accountId).add(con);
                }
                else 
                {
                    acctIdContactMap.put(con.accountId,new List<Contact> {con});
                }
            }
        }  
        
        if (!acctIdContactMap.isEmpty())
        {
            decimal highScore = 0;
            decimal acctScore = 0;
            Id saveConId;
            for (Account acct : [SELECT Id, Lattice_Last_Scored__c, Lattice_Fit_Score__c, Has_Amazon__c, Has_Blue_Coat__c,
                Has_Cisco_Identity_Services_Engine__c, Has_CyberArk__c, Has_Dropbox__c, Has_EMC_RSA_NetWitness__c, Has_FireEye__c,
                Has_Fortinet__c, Has_Fortinet_Fortigate__c, Has_IBM_Tivoli_Endpoint_Manager__c, Has_Microsoft_System_Center_Data_Protect__c,
                Has_Palo_Alto_Networks__c, Has_Rackspace__c, Has_Red_Hat_Enterprise_Linux__c, Has_Salesforce__c, Has_ServiceNow__c,
                Has_Sophos__c, Has_SourceFire__c, Has_Splunk__c, Has_Symantec_Endpoint_Protection__c FROM Account WHERE Id IN :acctIdContactMap.keySet()])  
            {
                List<Contact> cons = acctIdContactMap.get(acct.Id);
                     
                if (cons != null && !cons.isEmpty())
                {
                    for (Contact con : cons)
                    {
                        if (con.Lattice_Fit_Score__c > highScore)
                        {
                            highScore = con.Lattice_Fit_Score__c;
                            saveConId = con.Id;
                        }  
                    }
                    
                    acctScore = acct.Lattice_Fit_Score__c;
                    if (acctScore == null)
                    {
                        acctScore = 0;
                    }    
                    
                    if (highScore > acctScore)
                    {
                        for (Contact con : cons)
                        {
                            if (con.Id == saveConId)
                            {
                                acct.Lattice_Last_Scored__c = con.Lattice_Last_Scored__c; 
                                acct.Lattice_Fit_Score__c = con.Lattice_Fit_Score__c;
                                acct.Has_Amazon__c = con.Has_Amazon__c; 
                                acct.Has_Blue_Coat__c = con.Has_Blue_Coat__c;
                                acct.Has_Cisco_Identity_Services_Engine__c = con.Has_Cisco_Identity_Services_Engine__c;
                                acct.Has_CyberArk__c = con.Has_CyberArk__c; 
                                acct.Has_Dropbox__c = con.Has_Dropbox__c; 
                                acct.Has_EMC_RSA_NetWitness__c = con.Has_EMC_RSA_NetWitness__c; 
                                acct.Has_FireEye__c = con.Has_FireEye__c;
                                acct.Has_Fortinet__c = con.Has_Fortinet__c; 
                                acct.Has_Fortinet_Fortigate__c = con.Has_Fortinet_Fortigate__c; 
                                acct.Has_IBM_Tivoli_Endpoint_Manager__c = con.Has_IBM_Tivoli_Endpoint_Manager__c; 
                                acct.Has_Microsoft_System_Center_Data_Protect__c = con.Has_Microsoft_System_Center_Data_Protect__c;
                                acct.Has_Palo_Alto_Networks__c = con.Has_Palo_Alto_Networks__c; 
                                acct.Has_Rackspace__c = con.Has_Rackspace__c; 
                                acct.Has_Red_Hat_Enterprise_Linux__c = con.Has_Red_Hat_Enterprise_Linux__c; 
                                acct.Has_Salesforce__c = con.Has_Salesforce__c; 
                                acct.Has_ServiceNow__c = con.Has_ServiceNow__c;
                                acct.Has_Sophos__c = con.Has_Sophos__c; 
                                acct.Has_SourceFire__c = con.Has_SourceFire__c; 
                                acct.Has_Splunk__c = con.Has_Splunk__c; 
                                acct.Has_Symantec_Endpoint_Protection__c = con.Has_Symantec_Endpoint_Protection__c;
                                
                                accountsToUpdate.add(acct);
                                break;
                            }     
                        }
                    }
                }
                highScore = 0;
            }   
            
            if (!accountsToUpdate.isEmpty())
            {
                update accountsToUpdate;
            }  
        }  
    }    
}