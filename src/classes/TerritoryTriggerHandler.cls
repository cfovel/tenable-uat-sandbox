public with sharing class TerritoryTriggerHandler
{
	//public static User automationUser = [SELECT Id FROM User WHERE ((FirstName = 'Automation' AND LastName = 'User') OR LastName = 'Automation User') AND IsActive = true LIMIT 1];
    //public static Map<Id, Segment__c> segments = new Map<Id, Segment__c>([SELECT Id, Name FROM Segment__c]);
    //public static Map<Id, Region__c> regions = new Map<Id, Region__c>([SELECT Id, Name, RegionOwner__c, AreaName__c, AreaOwner__c, TheaterName__c, TheaterOwner__c FROM Region__c]);
        
    public class TerritoryBeforeInsertHandler implements Triggers.Handler
    {
        public void handle() 
        {
            UpdateTerritoryData(Trigger.new);
        }
    } 
    
    public class TerritoryBeforeUpdateHandler implements Triggers.Handler
    {
        public void handle() 
        {
            UpdateTerritoryData(Trigger.new);
        }
    } 
    
    public class TerritoryAfterInsertHandler implements Triggers.Handler
    {
        public void handle() 
        {
            UpdateTerritoryOwners(Trigger.new);
        }
    } 
    
    public class TerritoryAfterUpdateHandler implements Triggers.Handler
    {
        public void handle() 
        {
            UpdateTerritoryOwners(Trigger.new);
        }
    } 
    
    public static void UpdateTerritoryData(List<Territory__c> newTerritories)
    {
        User automationUser = [SELECT Id FROM User WHERE ((FirstName = 'Automation' AND LastName = 'User') OR LastName = 'Automation User') AND IsActive = true LIMIT 1];
        Map<Id, Segment__c> segments = new Map<Id, Segment__c>([SELECT Id, Name FROM Segment__c]);
        Map<Id, Region__c> regions = new Map<Id, Region__c>([SELECT Id, Name, RegionOwner__c, AreaName__c, AreaOwner__c, TheaterName__c, TheaterOwner__c FROM Region__c]);
        
        if (Trigger.isInsert) 
        {

            for (Territory__c territory : newTerritories) 
            {
                territory.OwnerId = automationUser.Id;
                territory.RegionName__c = (territory.Region__c == null) ? null : regions.get(territory.Region__c).Name;
                territory.RegionOwner__c = (territory.Region__c == null) ? null : regions.get(territory.Region__c).RegionOwner__c;
                territory.AreaName__c = (territory.Region__c == null) ? null : regions.get(territory.Region__c).AreaName__c;
                territory.AreaOwner__c = (territory.Region__c == null) ? null : regions.get(territory.Region__c).AreaOwner__c;
                territory.TheaterName__c = (territory.Region__c == null) ? null : regions.get(territory.Region__c).TheaterName__c;
                territory.TheaterOwner__c = (territory.Region__c == null) ? null : regions.get(territory.Region__c).TheaterOwner__c;
                territory.SegmentName__c = (territory.Segment__c == null) ? null : segments.get(territory.Segment__c).Name;
            }

        } else if (Trigger.isUpdate) 
        {
            
            for (Territory__c territory : newTerritories) 
            {
                Territory__c oldTerritory = (Territory__c)Trigger.oldMap.get(territory.Id);
                
                if (territory.OwnerId != automationUser.Id) 
                {
                    territory.OwnerId = automationUser.Id;
                }
                
                if (territory.Region__c != oldTerritory.Region__c || territory.RegionName__c != oldTerritory.RegionName__c || territory.RegionOwner__c != oldTerritory.RegionOwner__c || territory.AreaName__c != oldTerritory.AreaName__c || territory.AreaOwner__c != oldTerritory.AreaOwner__c || territory.TheaterName__c != oldTerritory.TheaterName__c || territory.TheaterOwner__c != oldTerritory.TheaterOwner__c) {
                    territory.RegionName__c = (territory.Region__c == null) ? null : regions.get(territory.Region__c).Name;
                    territory.RegionOwner__c = (territory.Region__c == null) ? null : regions.get(territory.Region__c).RegionOwner__c;
                    territory.AreaName__c = (territory.Region__c == null) ? null : regions.get(territory.Region__c).AreaName__c;
                    territory.AreaOwner__c = (territory.Region__c == null) ? null : regions.get(territory.Region__c).AreaOwner__c;
                    territory.TheaterName__c = (territory.Region__c == null) ? null : regions.get(territory.Region__c).TheaterName__c;
                    territory.TheaterOwner__c = (territory.Region__c == null) ? null : regions.get(territory.Region__c).TheaterOwner__c;
                }
                
                if (territory.Segment__c != oldTerritory.Segment__c || territory.SegmentName__c != oldTerritory.SegmentName__c) {
                    territory.SegmentName__c = (territory.Segment__c == null) ? null : segments.get(territory.Segment__c).Name;
                }
                
                if (territory.Name != oldTerritory.Name || territory.RegionName__c != oldTerritory.RegionName__c || territory.AreaName__c != oldTerritory.AreaName__c || territory.TheaterName__c != oldTerritory.TheaterName__c || territory.SegmentName__c != oldTerritory.SegmentName__c) {
                    territory.ChangeAttributionAccount__c = true;
                }
                
                if (territory.ChangeAttributionAccount__c || territory.TerritoryOwner__c != oldTerritory.TerritoryOwner__c || territory.RegionOwner__c != oldTerritory.RegionOwner__c || territory.AreaOwner__c != oldTerritory.AreaOwner__c || territory.TheaterOwner__c != oldTerritory.TheaterOwner__c) {
                    territory.ChangeAttributionAssignment__c = true;
                }
            }            
        }
    }
    
    public static void UpdateTerritoryOwners(List<Territory__c> newTerritories)
    {
        Set<Id> ownerIds = new Set<Id>();
        
        if (Trigger.isInsert) {
            
            for (Territory__c territory : newTerritories) {
                ownerIds.add(territory.TerritoryOwner__c);
            }
            
        } else if (Trigger.isUpdate) {
            
            for (Territory__c territory : newTerritories) {
                Territory__c oldTerritory = (Territory__c)Trigger.oldMap.get(territory.Id);
                
                if (territory.TerritoryOwner__c != oldTerritory.TerritoryOwner__c) {
                    ownerIds.add(territory.TerritoryOwner__c);
                    ownerIds.add(oldTerritory.TerritoryOwner__c);
                }
            }  
        }
        
        if (!ownerIds.isEmpty())
            TerritoryAssignment.setUserTerritoryFuture(ownerIds);
            
    }
}