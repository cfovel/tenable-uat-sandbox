public with sharing class SalesforceToJiraAPICalls {
 	
 	
  	/*These public variables store the credentials and the endpoint to connect to the mulesoft server.  
  	  We are storing them in custom metadata types inside salesforce so that they can be changed without having to update the code. */

    public static String ClientCredentials;
    public static Blob headerValue;
    public static String authorizationHeader;
    public static String endPoint;
    
    public Static void SetAuthorizationHeaderValues()
  	{	
  		for(Integration_Setting__mdt is : [SELECT Environment_Ind__c, Client_ID__c,Client_Secret__c,Email_Address__c,Endpoint_URL__c
  				FROM Integration_Setting__mdt order by Environment_Ind__c asc limit 1])
    	{
			ClientCredentials = is.Client_ID__c + ':' + is.Client_Secret__c;
    		headerValue = Blob.valueOf(ClientCredentials);
    		authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
   			endpoint = is.Endpoint_URL__c + 'issues/'; 
    	}
  	}

  	/*In this method we are sending an http post in order to create a ticket in JIRA. The method will return the JSON with the JIRA Case information*/
    public static String CreateJiraTicket(map<string, string> fieldValueMap) {
    	SetAuthorizationHeaderValues();
    	
    	system.debug('ROY 2: ' + fieldValueMap);
    	
    	String thisEndPoint = endPoint + 'issues';
    	/*I am going to build this dynamically once I populate the above map parameter*/
    	string jsonBody = JSON.serialize(fieldValueMap);//'{"priority":"4","reporter":"ghyde.ctr","issueType":"1","project":"CSDEV","subject":"Bug created by Cloud Mule Instance","description":"Bug created by Cloud Mule Instance","status":"Open","caseNumber":"00268282","customerName":"Lockheed Martin-HUD","component":"16010","productName":"11209"}';
    	
    	system.debug('ROY 3: ' + jsonBody);
    	
        HTTP h = new HTTP();
		HTTPRequest req = new HTTPRequest();
		req.setEndpoint(thisEndPoint);
		req.setHeader('Authorization', authorizationHeader);
		req.setHeader('Content-Type', 'application/json');
		req.setTimeout(60000);
		req.setMethod('POST');
		req.setBody(jsonBody);
		HTTPResponse res = h.send(req);
		
		system.debug('Roy 4: ' + res.getBody());
		/*  This is what the return JSON should look like
			{"success":true,"issueNumber":"CSDEV-17","issueId":"206522","priority":"4","reporter":"ghyde.ctr","issueType":"1","project":"CSDEV","subject":"Bug created by Cloud Mule Instance","description":"Bug created by Cloud Mule Instance","status":"10919","resolution":null,"created":"2016-08-10T19:45:12Z","updated":"2016-08-10T19:45:12Z","assignee":null}
		*/
		
		return res.getBody();
	}
    
    /*In this method we are sending an http post in order to update a ticket in JIRA. The method will return the JSON with the JIRA Case information*/
    public static String UpdateJIRATicket(map<string, string> fieldValueMap, String issueNumber) {
    	SetAuthorizationHeaderValues();
    	
    	String thisEndPoint = endPoint + 'issues/'+issueNumber;
    	/*I am going to build this dynamically once I populate the above map parameter*/
    	String jsonBody = JSON.serialize(fieldValueMap);//'{"caseNumber":"00268282","priority":"2","reporter":"rjones"}';
    	
    	system.debug('ROY 10: ' + jsonBody);
    	
        HTTP h = new HTTP();
		HTTPRequest req = new HTTPRequest();
		req.setEndpoint(thisEndPoint);
		req.setHeader('Authorization', authorizationHeader);
		req.setHeader('Content-Type', 'application/json');
		req.setTimeout(60000);
		req.setMethod('POST');
		req.setBody(jsonBody);
		HTTPResponse res = h.send(req);
		
		system.debug('Roy 22 ' + res.getBody());
		/*  This is what the return JSON should look like
			{"success":true,"issueNumber":"CSDEV-17","issueId":"206522","priority":"4","reporter":"rjones","issueType":"1","project":"CSDEV","subject":"Bug created by Cloud Mule Instance","description":"Bug created by Cloud Mule Instance","status":"10919","resolution":null,"created":"2016-08-10T19:45:12Z","updated":"2016-08-10T19:45:12Z","assignee":null}
		*/
		
		return res.getBody();
    }
    
    /*In this method we are sending an http post in order to create a ticket comment in JIRA. The method will return the JSON with the JIRA Case information*/
    public static String CreateJiraTicketComment(map<String, String> fieldMap, String issueNumber) {
    	SetAuthorizationHeaderValues();
    	
    	String thisEndPoint = endPoint + 'issues/'+issueNumber+'/comments';
    	/*I am going to build this dynamically once I populate the above map parameter*/
    	String jsonBody = JSON.serialize(fieldMap);//'{"text":"This comment was created from a salesforce http callout","createdBy":"rjones"}';
    	
    	system.debug('ROY 8: ' + jsonBody);
    	
        HTTP h = new HTTP();
		HTTPRequest req = new HTTPRequest();
		req.setEndpoint(thisEndPoint);
		req.setHeader('Authorization', authorizationHeader);
		req.setHeader('Content-Type', 'application/json');
		req.setTimeout(60000);
		req.setMethod('POST');
		req.setBody(jsonBody);
		HTTPResponse res = h.send(req);
		
		system.debug('Roy 9 ' + res.getBody());
		/*  This is what the return JSON should look like
			{"success":true,"commentId":null,"issueNumber":"CSDEV-17"}
		*/
		
		return res.getBody();
    }
    
}