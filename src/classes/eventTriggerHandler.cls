public without sharing class eventTriggerHandler {
/*******************************************************************************
*
* Event Trigger Handler Class
* Version 1.0a
*
*******************************************************************************/
	public class eventAfterInsertHandler implements Triggers.Handler {
        public void handle() {
            CountEvents(Trigger.new);
        }
    } 
    
    public class eventAfterUpdateHandler implements Triggers.Handler {
        public void handle() {
            CountEvents(Trigger.new);
        }
    } 

    public class eventAfterDeleteHandler implements Triggers.Handler {
        public void handle() {
            CountEvents(Trigger.old);
        }
    } 

    public class eventAfterUndeleteHandler implements Triggers.Handler {
        public void handle() {
            CountEvents(Trigger.new);
        }
    } 
    
    public static void CountEvents(List<Event> events) {
    	//Update Activity Count
	    ActivityUtils au = new ActivityUtils(events);
	    au.updateContactActivityCount();
	    au.updateLeadActivityCount();
	}
}