/*************************************************************************************
Class: BatchUpdateTenableUsageLog
Author: Bharathi.M
Details: This batch gets all the contacts that are updated in a day and 
pushes the data to the Tenable.io Usage Log object.  The push will be
based on the container UUID
History : Created 03/17/2017 (79% code coverage)
          Modifed 03/22/2017 - Modifed the batch to update the existing Tenable Usage Log
          when Pendo details are blank else create a new Tenable Usage Log.
          Modified 03/27/2017 - Modified the batch to change the scope so it looks at the 
          installbase records and not contacts.  Pendo will try to push data to installbase
          object in production.
***************************************************************************************/
global class BatchUpdateTenableUsageLog //implements Database.Batchable<sobject>, Schedulable
{
	/*
	public static list<string> lstErrs = new list<string>();
	public static boolean hasErrors = false;
	public static boolean InsertUsageLogs = false;
	
	public static string query = 'SELECT Id, Account__c, Pendo_Container_Name__c,Pendo_Container_Site__c,UUID__c,' +
								 'Pendo_First_Visit__c, Pendo_Last_Visit__c, ' +
								 'Pendo_Number_Of_Days_Active__c, Pendo_Number_Of_Events__c, Pendo_Role__c, Pendo_Time_On_Site_Minutes__c FROM ' +
								 'Install_Base__c WHERE LastModifiedDate = Today AND UUID__c != NULL ' + (Test.isRunningTest() ? ' limit 49' : '');

	global BatchUpdateTenableUsageLog()
	{
    	query = query ;
	}
	
    global Database.QueryLocator start(Database.BatchableContext bc)
    {
    	return Database.getQueryLocator(query);
    }
    
    global void execute(SchedulableContext ctx)
    {
    	Database.executeBatch(new BatchUpdateTenableUsageLog());
    }
    
    global void execute(Database.BatchableContext bc, List<sObject> scope)
    {
    	set<string> UUIds = new set<string>();
    	list<Tenable_io_Usage_Log__c> lstInsertTenableUsagelog = new list<Tenable_io_Usage_Log__c>();
    	map<Id,Tenable_io_Usage_Log__c> mapUsage = new map<Id,Tenable_io_Usage_Log__c>();
    	map<string,string> mapUsageLogs = new map<string,string>();
    	
    	Id TenableUsageID;
		
		Id PendoRecType = [SELECT ID FROM RecordType WHERE sObjectType = 'Tenable_io_Usage_Log__c' and Name = 'Pendo Data'].Id;
		
		for(sObject sc: scope)
		{
			Install_Base__c ib = (Install_Base__c)sc;
			if(ib.UUID__c != NULL && ib.UUID__c != '')
    		{
    			UUIds.add(ib.UUID__c);
    		} 
		}

    	if(!UUIds.isEmpty())
    	{
    		mapUsage = new map<Id,Tenable_io_Usage_Log__c>([SELECT Id, UUID__c, Pendo_Container_Name__c, Pendo_Container_Site__c,Pendo_First_Visit__c,Pendo_Last_Visit__c,Pendo_Number_Of_Days_Active__c,Pendo_Number_Of_Events__c,Pendo_Role__c,Pendo_Time_On_Site_Minutes__c  FROM Tenable_io_Usage_Log__c WHERE UUID__c in : UUIds]);
    	}
    	
    	if(!mapUsage.isEmpty())
    	{
    		for(Tenable_io_Usage_Log__c tu : mapUsage.values())
    		{
    			if(!mapUsageLogs.containsKey(tu.UUID__c)) 
    			{
    				mapUsageLogs.put(tu.UUID__c,tu.Id);
    			}
    		}
    	}
    	
    	for(sObject sc: scope)
    	{
    		Install_Base__c ib = (Install_Base__c)sc;
    		TenableUsageID = NULL;

    		If(mapUsageLogs.containsKey(ib.UUID__c))
    		{
    			TenableUsageID = mapUsageLogs.get(ib.UUID__c);
    		}

    		if(!InsertUsageLogs && TenableUsageID != NULL)
    		{
    			Tenable_io_Usage_Log__c tu = mapUsage.get(TenableUsageID);

				InsertUsageLogs = tu.Pendo_Container_Name__c != ib.Pendo_Container_Name__c ? true : false;
				InsertUsageLogs = tu.Pendo_Container_Site__c != ib.Pendo_Container_Site__c ? true : false;
				InsertUsageLogs = tu.Pendo_First_Visit__c != ib.Pendo_First_Visit__c ? true : false;
				InsertUsageLogs = tu.Pendo_Last_Visit__c != ib.Pendo_Last_Visit__c ? true : false;
				InsertUsageLogs = tu.Pendo_Number_Of_Days_Active__c != ib.Pendo_Number_Of_Days_Active__c ? true : false;
				InsertUsageLogs = tu.Pendo_Number_Of_Events__c != ib.Pendo_Number_Of_Events__c ? true : false;
				InsertUsageLogs = tu.Pendo_Role__c != ib.Pendo_Role__c ? true : false;
				InsertUsageLogs = tu.Pendo_Time_On_Site_Minutes__c != ib.Pendo_Time_On_Site_Minutes__c ? true : false;
    		}
    		
    		if(TenableUsageID == NULL)
    		{
    			InsertUsageLogs = true;
    		}
    		
    		if(InsertUsageLogs)
    		{
    			Tenable_io_Usage_Log__c tUsage = new Tenable_io_Usage_Log__c();
    			tUsage.Install_Base__c = ib.ID;
				tUsage.UUID__c = ib.UUID__c != NULL ? ib.UUID__c : NULL;
				tUsage.Account__c = ib.Account__c != NULL ? ib.Account__c : NULL;
    			tUsage.Pendo_Container_Name__c = ib.Pendo_Container_Name__c != NULL ? ib.Pendo_Container_Name__c : NULL;
    			tUsage.Pendo_Container_Site__c = ib.Pendo_Container_Site__c != NULL ? ib.Pendo_Container_Site__c : NULL;
	 			tUsage.Pendo_First_Visit__c = ib.Pendo_First_Visit__c != NULL ? ib.Pendo_First_Visit__c : NULL;
    			tUsage.Pendo_Last_Visit__c = ib.Pendo_Last_Visit__c != NULL ? ib.Pendo_Last_Visit__c : NULL;
    			tUsage.Pendo_Number_Of_Days_Active__c = ib.Pendo_Number_Of_Days_Active__c != NULL ? ib.Pendo_Number_Of_Days_Active__c : NULL;
    			tUsage.Pendo_Number_Of_Events__c =ib.Pendo_Number_Of_Events__c != NULL ? ib.Pendo_Number_Of_Events__c : NULL;
    			tUsage.Pendo_Role__c = ib.Pendo_Role__c != NULL ? ib.Pendo_Role__c : NULL;
    			tUsage.Pendo_Time_On_Site_Minutes__c = ib.Pendo_Time_On_Site_Minutes__c != NULL ? ib.Pendo_Time_On_Site_Minutes__c : NULL;
    			tUsage.RecordTypeId = PendoRecType != NULL ? PendoRecType : NULL;
    			lstInsertTenableUsagelog.add(tUsage);
    		}
    	}
    	
    	if(!lstInsertTenableUsagelog.isEmpty())
    	{
    		try
    		{
    			insert lstInsertTenableUsagelog;
    		}
    		catch(exception ex)
    		{
    			hasErrors = true;
    			lstErrs.add(ex.getMessage());
    		}
    	}
    }
    
    global void finish(Database.BatchableContext bc)
    {
    	if(!test.isRunningTest())
    	{
	    	if(hasErrors)
	    	{
	    		string htmlBody = 'The following errors occured during processing: <br/>';
	    		
	    		for(string s: lstErrs)
	    		{
	    			htmlBody += s + '<br/>';
	    		}
	    		
	    		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	    		string[] toAddress = new string[] {'bizplatforms@tenable.com'};
	    		mail.setToAddresses(toAddress);
	    		mail.setReplyTo('bmuniswamy@tenable.com');
	    		mail.setSenderDisplayName('Pendo.io Integration');
	    		mail.setSubject('Pendo.io SF Integration Completed With Errors');
	    		mail.setHtmlBody(htmlbody);
	    		
	    		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	    	}
    	}
    }
    */
}