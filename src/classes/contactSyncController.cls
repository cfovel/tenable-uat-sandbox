public class contactSyncController{
    public Contact c;
	
    public contactSyncController(ApexPages.StandardController controller){
        this.c = (Contact)controller.getRecord();
    }
    
    public PageReference SendContactToMulesoft(){
        String ContactId = ApexPages.currentPage().getParameters().get('id');

        this.c = [SELECT Id, FirstName, LastName, Email FROM Contact WHERE Id = :ContactId];
        if ( String.isBlank(c.FirstName) || String.isBlank(c.LastName) || String.isBlank(c.Email))
        {
        	Utils.addError('Please populate all required fields on Contact before syncing Account with Netsuite: FirstName, LastName, Email.');

            return null;
        }
        set<id> payloadIds = new set<id>();
        
        system.debug('Contact Sync Button was Pressed');
        
        //Adding Contact to PayloadId set
        payloadIds.add(ContactId);
        
        
        HttpResponse contractMuleEvent = mulesoftIntegrationController.mulesoftEventHandler(payloadIds, ContactId, 'contactSync');
		
        PageReference page = new PageReference('/' + ContactId);
        page.setRedirect(true);
        system.debug('Roy8: ' + page);
        return page;
    }  
}