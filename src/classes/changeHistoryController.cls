public class changeHistoryController {
	public account2installbases__x a;
	public Contact c;
    public String productInstanceId;
    public String LMSCustomerId;
    public String LMSCustomerContactId;
    
    public changeHistoryController(ApexPages.StandardController controller)
    {
    	Id sId = ApexPages.currentPage().getParameters().get('id');
    	String sobjectType = sId.getSObjectType().getDescribe().getName();
    	system.debug('@@@ sobjectType: ' + sobjectType);
    	
    	List<String> fields = new List<String>(); 
    	if (sobjectType == 'Contact')
    	{
    		fields.add('LMS_Customer_ID__c');
    		fields.add('LMS_Customer_Contact_ID__c');
    	}
    	else
    	{
    		fields.add('productInstanceID__c');
    		fields.add('Id__c');
    	}
    	
    	if (!Test.isRunningTest()) 
    	{
    		controller.addFields(fields);
    	}	
    	
    	if (sobjectType == 'Contact')
    	{
    		this.c = (Contact)controller.getRecord();
    		LMSCustomerId = c.LMS_Customer_ID__c;
    		LMSCustomerContactId = c.LMS_Customer_Contact_ID__c;
    	}
    	else
    	{
	        this.a = (account2installbases__x)controller.getRecord();
	        if (a.productInstanceID__c != null)
	        {
	        	productInstanceId = String.valueOf(a.productInstanceID__c);
	        }
	        if (a.Id__c != null)
	        {
		        Account acct = [SELECT Id, LMS_Customer_ID__c FROM Account WHERE Id = :a.Id__c limit 1];
		        LMSCustomerId = acct.LMS_Customer_ID__c;
	        }
    	}
    }
    
    public PageReference changeHistoryProduct()
    {
        String tableauURL = 'https://us-east-1.online.tableau.com/#/site/tenablebusinessintelligence/views/LMS_Customer_Lookup_0/Product-Changes?LMS_ID=' + LMSCustomerId + '&ctxid=' + productInstanceId;
        system.debug('@@@ tableauURL: ' + tableauURL);
        PageReference page = new PageReference(tableauURL);
        
        return page;
    }    
    
    public PageReference changeHistoryContact()
    {
        String tableauURL = 'https://us-east-1.online.tableau.com/#/site/tenablebusinessintelligence/views/LMS_Customer_Lookup_0/Contact-Changes?LMS_ID=' + LMSCustomerId + '&ctxid=' + LMSCustomerContactId;
        system.debug('@@@ tableauURL: ' + tableauURL);
        PageReference page = new PageReference(tableauURL);
        
        return page;
    }    
}