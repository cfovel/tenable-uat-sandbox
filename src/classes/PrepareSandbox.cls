global class PrepareSandbox implements SandboxPostCopy {
    global void runApexClass(SandboxContext context) {
        System.debug('Org ID: ' + context.organizationId());
        System.debug('Sandbox ID: ' + context.sandboxId());
        System.debug('Sandbox Name: ' + context.sandboxName());

        ID leadBatchProcessid = Database.executeBatch(new baUpdateLeadEmailAddress(),200);
        
        ID contactBatchProcessid = Database.executeBatch(new baUpdateContactEmailAddress(),200);
        
        ID caseBatchProcessid = Database.executeBatch(new baUpdateCaseEmailAddress(),200);
    }
}