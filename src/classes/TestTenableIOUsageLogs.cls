@isTest(seealldata = true)
private class TestTenableIOUsageLogs 
{

    static testMethod void TestInstallBaseUsageLogBatch() 
    {

        Account acc  = new Account(Name='Test Batch Account Inc.', BillingState='MD', BillingCountry='US', website='http://www.testbatch.com');
        insert acc;
        
        
        Install_Base__c ibase = new Install_Base__c(Name = 'Test',Account__c = acc.Id, UUID__c = '90b89808-914b-4afa-bded-c74b740c7379');
        insert ibase;
        Evaluation__c eval = new Evaluation__c(Name = 'Test', container_uuid__c = '73a7ced3-7150-4bfa-a0a2-2f562d208d2d',Eval_Type__c = 'Tenable.io');
        insert eval;
        
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
            BatchUpdateTenableioUsageLog uBatch = new BatchUpdateTenableioUsageLog('');
            Id batchprocess = Database.executeBatch(uBatch,20);
        Test.stopTest();
    }

}