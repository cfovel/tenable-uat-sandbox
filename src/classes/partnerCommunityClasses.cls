global with sharing class partnerCommunityClasses {
    /******************************************************************************
*
* Classes in support of Partner Community Pages
* Version 1.0a
* Copyright 2006-2014 (c) by CloudLogistix.com
* All Rights Reserved
* Your are allowed to make modifications only for yourself, not for distribution
* without permission from CloudLogistix.
* 
*******************************************************************************/
    
    /*****************************************************************************************
*   Variable Declaration
******************************************************************************************/
    Integer numOpenLeads = 0;
    Integer numUnreadLeads = 0;
    Integer numPendingLeads = 0;
    Integer numAcceptedLeads = 0;
    Integer numNewLeads = 0;
    
    Integer numNewCases = 0;
    Integer numOpenCases = 0;
    Integer numPendingCases = 0;
    Integer numClosedCases = 0;
    
    Integer numOpenOpps = 0;
    Decimal valueOpenOpps = 0.0;
    Integer numWonOpps = 0;
    Decimal valueWonOpps = 0.0;
    
    Decimal fundsAvail = 0.0;
    Decimal fundsPending = 0.0;
    Decimal fundsApproved = 0.0;
    
    User prmUser = new User();
    Account prmAccount = new Account();
    Contact prmContact = new Contact();
    User prmManager = new User();
    
    /*****************************************************************************************
*   Page Initialization
******************************************************************************************/
    public PageReference init() {
        loadPartnerInfo();
        
        loadNumOpenLeads();
        loadNumUnreadLeads();
        loadNumPendingLeads();
        loadNumAcceptedLeads();
        loadNumNewLeads();
        
        loadOpenOpps();
        loadWonOpps();
        
        loadfundsAvail();
        loadPrograms();
        
        /*loadNumOpenCases();
        loadNumClosedCases();
        loadNumPendingCases();
        loadNumNewCases();*/
        loadnewKB();
        
        return null;
    }
    
    /*****************************************************************************************
*       Getter/Setter's to return values to VF Page(s)
******************************************************************************************/ 
    public User getPRMUser() {return prmUser;}
    public Account getPRMAccount() {return prmAccount;}
    public Contact getPRMContact() {return prmContact;}
    public User getPRMManager() {return prmManager;}
    public list<Community_Campaign__c> lstcamps {get;set;}
    public list<Community_Campaign__c> lstDistcamps {get;set;}
    public static list<Community_Campaign_Language__c> lstlanguages {get;set;}
    public static list<Community_Program__c> lstPrograms {get;set;}
    
    public void loadPartnerInfo() {
        prmUser = 
            ([SELECT ContactId, ManagerId, Profile.Name,
              Show_MDF__c, Show_MDF_Balances__c,
              UserType
              FROM User 
              WHERE Id = :UserInfo.getUserId() LIMIT 1]);
        if (prmUser.ContactId != null) {
            prmContact = 
                ([SELECT AccountId
                  FROM Contact
                  WHERE Id = :prmUser.ContactId LIMIT 1]);                
            prmAccount = 
                ([SELECT Id, Portal_Logo__c
                  FROM Account
                  WHERE Id = :prmContact.AccountId LIMIT 1]);
        }
        if (prmUser.ManagerId != null) {
            system.debug('The value of prmUser.ManagerId is : ' + prmUser.ManagerId + '   ' + UserInfo.getUserId());
            /*prmManager = 
                ([SELECT Name, Email, Phone
                  FROM User
                  WHERE Id = :prmUser.ManagerId]);*/
        }
    }
    
    public Integer getNumOpenLeads() {return numOpenLeads;}
    public void loadNumOpenLeads() {
        numOpenLeads = 0;
        Lead[] openLeads = 
            ([SELECT Id
              FROM Lead
              WHERE ownerid = :UserInfo.getUserId()
              AND Status = 'Not Contacted']);
        if (openLeads != null) {numOpenLeads = openLeads.size();}
    }
    
    public Integer getNumUnreadLeads() {return numUnreadLeads;}
    public void loadNumUnreadLeads() {
        numUnreadLeads = 0;
        Lead[] unreadLeads = 
            ([SELECT Id
              FROM Lead
              WHERE ownerid = :UserInfo.getUserId()
              AND isUnreadByOwner = TRUE]);
        if (unreadLeads != null) {numUnreadLeads = unreadLeads.size();}
    }
    
    public Integer getNumPendingLeads() {return numPendingLeads;}
    public void loadNumPendingLeads() {
        numPendingLeads = 0;
        Lead[] pendingLeads = 
            ([SELECT Id
              FROM Lead
              WHERE ownerid = :UserInfo.getUserId()
              AND Status = 'Submitted']);
        if (pendingLeads != null) {numPendingLeads = pendingLeads.size();}
    }
    
    public Integer getNumAcceptedLeads() {return numAcceptedLeads;}
    public void loadNumAcceptedLeads() {
        numAcceptedLeads = 0;
        Lead[] acceptedLeads = 
            ([SELECT Id
              FROM Lead
              WHERE ownerid = :UserInfo.getUserId()
              AND (Status = 'Accepted' OR Status = 'Qualified')]);
        if (acceptedLeads != null) {numAcceptedLeads = acceptedLeads.size();}
    }
    
    public Integer getNumNewLeads() {return numNewLeads;}
    public void loadNumNewLeads() {
        numNewLeads = 0;
        Lead[] newLeads = 
            ([SELECT Id
              FROM Lead
              WHERE ownerid = :UserInfo.getUserId()
              AND createddate = THIS_QUARTER]);
        if (newLeads != null) {numNewLeads = newLeads.size();}
    }
    
    public Integer getNumOpenOpps() {return numOpenOpps;}
    public Decimal getValueOpenOpps() {return valueOpenOpps;}
    public void loadOpenOpps() {
        numOpenOpps = 0;
        valueOpenOpps = 0.0;
        if (prmUser.UserType != 'Standard') {
            Opportunity[] opps = 
                ([SELECT Id, Amount
                  FROM Opportunity
                  WHERE isClosed = FALSE
                  AND Amount != NULL]);
            if (opps != null) {
                numOpenOpps = opps.size();
                for (Opportunity o : opps) {
                    valueOpenOpps += o.amount;
                }
            }
        }
    }
    
    public Integer getNumWonOpps() {return numWonOpps;}
    public Decimal getValueWonOpps() {return valueWonOpps;}
    public void loadWonOpps() {
        numWonOpps = 0;
        valueWonOpps = 0.0;
        if (prmUser.UserType != 'Standard') {
            Opportunity[] opps = 
                ([SELECT Id, Amount
                  FROM Opportunity
                  WHERE isWon = TRUE
                  AND Amount != NULL]);
            if (opps != null) {
                numWonOpps = opps.size();
                for (Opportunity o : opps) {
                    valueWonOpps += o.amount;
                }
            }
        }
    }
    
    public Decimal getfundsAvail() {return fundsAvail;}
    public Decimal getfundsPending() {return fundsPending;}
    public Decimal getfundsApproved() {return fundsApproved;}
    public void loadfundsAvail() {
        fundsAvail = 0.0;
        fundsPending = 0.0;
        
        if (prmUser.UserType != 'Standard') {
            AggregateResult[] fundStats = 
                ([SELECT Status__c, SUM(MDF_Amount__c) Totals
                  FROM MDF_Transaction__c
                  WHERE Status__c != 'Open'
                  AND Status__c != 'Rejected'
                  GROUP BY Status__c
                  ORDER BY Status__c]);
            for (Integer i=0; i < fundStats.size(); i++) {
                String status = String.valueof(fundStats[i].get('Status__c'));
                if (status == 'Submitted') {fundsPending += Decimal.valueof(String.valueof(fundStats[i].get('Totals')));}
                if (status == 'Approved') {fundsApproved += Decimal.valueof(String.valueof(fundStats[i].get('Totals')));}
                if (status != 'Submitted') {fundsAvail += Decimal.valueof(String.valueof(fundStats[i].get('Totals')));}
            }
        }
    }
    
    Community_Program__c[] PVSprogs = new Community_Program__c[0];
    Community_Program__c[] SCprogs = new Community_Program__c[0];
    Community_Program__c[] SCCVprogs = new Community_Program__c[0];
    Community_Program__c[] Nessusprogs = new Community_Program__c[0];
    Community_Program__c[] NEprogs = new Community_Program__c[0];
    Community_Program__c[] PSprogs = new Community_Program__c[0];
    Community_Program__c[] LCEprogs = new Community_Program__c[0];
    Community_Program__c[] Otherprogs = new Community_Program__c[0];
    public Community_Program__c[] getPVSprograms() {return PVSprogs;}
    public Community_Program__c[] getSCprograms() {return SCprogs;}
    public Community_Program__c[] getSCCVprograms() {return SCCVprogs;}
    public Community_Program__c[] getNessusprograms() {return Nessusprogs;}
    public Community_Program__c[] getNEprograms() {return NEprogs;}
    public Community_Program__c[] getPSprograms() {return PSprogs;}
    public Community_Program__c[] getLCEprograms() {return LCEprogs;}
    public Community_Program__c[] getOtherprograms() {return Otherprogs;}
    public void loadPrograms() {
        lstcamps = new list<Community_Campaign__c>([SELECT Campaign_Name__c, Active__c,Comments__c FROM Community_Campaign__c WHERE Active__c=True AND Type__c != 'Distributor' ORDER BY Display_Order__c ASC]);  
        lstDistcamps = new list<Community_Campaign__c>([SELECT Campaign_Name__c, Active__c, Comments__c FROM Community_Campaign__c WHERE Active__c = True AND Type__c='Distributor']);
        
        Community_Program__c[] progs = 
            ([SELECT Title__c, URL_Link__c, Page__c, Section__c, 
              Enabled__c, Valid_From__c, Valid_To__c
              FROM Community_Program__c
              WHERE Enabled__c = TRUE
              AND Community__c = 'Partner'
              AND Valid_From__c <= :System.today()
              AND Valid_To__c >= :System.today()
              ORDER BY Title__c]);
        
        for (Community_Program__c cp : progs) {
            if (cp.Section__c == 'Nessus') {Nessusprogs.add(cp);}
            if (cp.Section__c == 'NE') {NEprogs.add(cp);}
            if (cp.Section__c == 'PVS') {PVSprogs.add(cp);}
            if (cp.Section__c == 'PS') {PSprogs.add(cp);}
            if (cp.Section__c == 'SC') {SCprogs.add(cp);}
            if (cp.Section__c == 'Cross-Product') {Otherprogs.add(cp);}
        }
    }
    
    @RemoteAction
    global static list<Community_Campaign_Language__c> getCommLanguages(string CampaignId)
    {
        lstlanguages = new list<Community_Campaign_Language__c>([SELECT ID,Language__c,URL_Link__c FROM Community_Campaign_Language__c WHERE Community_Campaign__c =: CampaignId AND Active__c = True ORDER BY Language__c ASC]);
        return lstlanguages;
    }
    
    @RemoteAction
    global static list<Community_Program__c> getCommPrograms(string CampaignId)
    {
        lstPrograms = new list<Community_Program__c>([SELECT Title__c,URL_Link__c from Community_Program__c
                                                      WHERE Community__c='Partner' and Community_Campaign__c=:CampaignId
                                                      ORDER BY Display_Order__c ASC]);
        return lstPrograms;
    }
    
    public Integer getNumOpenCases() {return numOpenCases;}
    public void loadNumOpenCases() {
        numOpenCases = 0;
        Case[] openCases = 
            ([SELECT Id
              FROM Case
              WHERE ContactId = :prmContact.Id
              AND isClosed = FALSE]);
        if (openCases != null) {numOpenCases = openCases.size();}
    }
    
    public Integer getNumClosedCases() {return numClosedCases;}
    public void loadNumClosedCases() {
        numClosedCases = 0;
        Case[] closedCases = 
            ([SELECT Id
              FROM Case
              WHERE ContactId = :prmContact.Id
              AND isClosed = TRUE]);
        if (closedCases != null) {numClosedCases = closedCases.size();}
    }
    
    public Integer getNumPendingCases() {return numPendingCases;}
    public void loadNumPendingCases() {
        numPendingCases = 0;
        Case[] pendingCases = 
            ([SELECT Id
              FROM Case
              WHERE ContactId = :prmContact.Id
              AND Status = 'Pending - Customer']);
        if (pendingCases != null) {numPendingCases = pendingCases.size();}
    }
    
    public Integer getNumNewCases() {return numNewCases;}
    public void loadNumNewCases() {
        numNewCases = 0;
        Case[] newCases = 
            ([SELECT Id
              FROM Case
              WHERE ContactId = :prmContact.Id
              AND Status = 'New']);
        if (newCases != null) {numNewCases = newCases.size();}
    }
    
    KnowledgeArticleVersion[] newKBarticles = new KnowledgeArticleVersion[0];
    public KnowledgeArticleVersion[] getnewKB() {return newKBarticles;}
    public void loadnewKB() {
        newKBarticles = 
            ([SELECT Title, Summary, PublishStatus, LastPublishedDate, IsVisibleInCsp, 
              URLName, ArticleType, ArticleNumber 
              FROM KnowledgeArticleVersion 
              WHERE PublishStatus = 'Online' 
              AND Language = 'en_US' 
              ORDER BY LastPublishedDate DESC LIMIT 5]);
    }
}