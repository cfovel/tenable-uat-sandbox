//This method will be used to create a generic Json object to send to Mulesoft
///////////////////
    ////FUTURE
    //1) Blacklist fields. Add Black listed fields to the query
    //2) Add related related records to the query such as email address instead of Id. Also add related record to Integration Setting
    //2) send Master object separate from others
    //3) define Parent and child objects in the meta data
    //4) allow other objects to be sent in the idSet if necessary
    //5) figure out a way to send special things like PDFs
    //6) Have mulesoftEventHandler and mulesoftMultipleEventHandler

public class mulesoftIntegrationController {
    
    @future (callout=true)
    public static void futureMulesoftEventHandler(set<id> idSet, id masterId, string eventType){
        HttpResponse muleEvent = mulesoftEventHandler(idSet, masterId, eventType);
    }
    
    //Handles mulesoft request when provided a set of ids of the same object type and redefined mulesfot event
    public static HttpResponse mulesoftEventHandler(set<id> idSet, id masterId, string eventType){
        //boolean validRequest          = true;
        
        //check if we are in a sandbox below
        boolean isSandbox             = runningInASandbox();
        sObject tempObj;
        set<string> validObjectSet    = new set<string>();
        set<string> childObjectSet    = new set<string>();
        set<string> parentObjectSet   = new set<string>();
        list<sObject> tempList        = new list<sObject>();
        list<Integration_Setting__mdt> authHelperList = new list<Integration_Setting__mdt>();
        map<string, string> headerMap = new map<string, string>();
        map<id, list<sObject>> objMap = new map<id, list<sObject>>();
        
        //getting the integration setting based on the event type
        Integration_Setting__mdt currentMuleProcess = getMuleProcess(eventType);
        authHelperList.add(currentMuleProcess);
        //getting the integration setting based on the Environment. Sandbox or Prod
        Integration_Setting__mdt envMuleProcess;
        if(isSandbox){
            envMuleProcess = getMuleProcess('Sandbox');
        }else{
            envMuleProcess = getMuleProcess('Production');
        }
        authHelperList.add(envMuleProcess);
        
        //getting a list of the of the globally black listed fields 
        string blacklistedFields           = envMuleProcess.Black_Listed_Fields__c;
        list<string> globalBlacklistedList = new list<string>();
        if(blacklistedFields != null){
            globalBlacklistedList = blacklistedFields.split(',');
        }
        
        //getting a list of the of the black listed fields for the defined process
        string restrictedFields           = currentMuleProcess.Black_Listed_Fields__c;
        list<string> blacklistedList = new list<string>();
        if(restrictedFields != null){
            blacklistedList = restrictedFields.split(',');
        }
        //combining the 2 lists
        globalBlacklistedList.addAll(blacklistedList);
        
        //getting a list of the of the Related listed fields for the defined process
        string relatedFields           = currentMuleProcess.Related_Fields__c;
        list<string> relatedFieldList = new list<string>();
        if(relatedFields != null){
            relatedFieldList = relatedFields.split(',');
        }
        
        //getting a list of the child objects. 
        string childObject           = currentMuleProcess.Child_Objects__c;
        if(childObject != null){
            list<string> childObjectList = childObject.split(',');
            childObjectSet.addAll(childObjectList);
        }
        
        //getting a list of the parent objects. 
        string parentObject           = currentMuleProcess.parent_Objects__c;
        if(parentObject != null){ 
            list<string> parentObjectList = parentObject.split(',');
            parentObjectSet.addAll(parentObjectList);
        }
               
        //system.debug('ROY2: ' + validRequest);
        
        //Getting a list of the objects that are in the ID set
        for(id i: idSet){
            tempObj = selectAllFromId(i, relatedFieldList, globalBlacklistedList);
            tempList.add(tempObj);
        }
        objMap.put(masterId, tempList);
        
        // Find Attachments on master record (for vendorCreate event only)
        List<Attachment> attachmentList = new List<Attachment>();
        /*
        if (eventType == 'vendorCreate')
        {
            attachmentList = [SELECT Id, Body, ContentType, Name FROM Attachment WHERE ParentId = :masterId];
            
        
	        System.debug('@@@ current heap size: ' + Limits.getHeapSize() + ', max heap size: ' + Limits.getLimitHeapSize());
	
	        Integer attRemoved = 0;
	        if (Limits.getHeapSize() > 3000000)
		    {
		        for(Integer i = attachmentList.size()-1;i>0;i--)
		        {
			        if (Limits.getHeapSize() > 2500000)
			        {
			        	attachmentList.remove(i);
			        	attRemoved++;
			        }
			        else
			        {
			        	i=0;
			        }
		        	
		        }

			    String str = 'Number of attachments removed = ' + attRemoved;
			    attachmentList.add(new Attachment(Name=str, ParentId=masterId, ContentType='txt',Body=Blob.valueOf(str)));

		    }
	        
	        System.debug('@@@ current heap size-2: ' + Limits.getHeapSize());
	        System.debug('@@@ attachmentList size-1: ' + attachmentList.size());
	        System.debug('@@@ attachments removed: ' + attRemoved);
        }
        */
        //Here we are getting everything we need to send to mulesoft
        String jsonResult = jsonObjectBuilder(objMap, attachmentList, eventType);

        headerMap         = SetAuthorizationHeaderValues(authHelperList);
        
        system.debug('ROY6: header map ' + headerMap);
        System.debug('@@@ current heap size-3: ' + Limits.getHeapSize());
        System.debug('@@@ attachmentList size-2: ' + attachmentList.size());
        HttpResponse theResponse = postToMulesoft(jsonResult, headerMap, eventType, currentMuleProcess.Request_Type__c, 'application/json');
        
        return theResponse;
    }
    
    //returns a JSON object form a set of Ids
    public static string jsonObjectBuilder(map<id, list<sObject>> objMap, List<Attachment> attachmentList, string eventType){
        
        //list<sObject> objList = objectListFromIdSet(idSet);
        list<sObject> objList;
        
        system.debug('ROY3: ' + eventType);
        string extraJSON;
        string jsonString = '{"EventType":' + '"' + eventType + '",';
        jsonString += '"Objects": [';
        
        //looping through the keyset so that we can get the list of objects tied to the key
        for(id t: objMap.keySet()){
        
            objList = objMap.get(t);
            system.debug('Roy3: Start of Inner JSON Map Loop ' + t);
            for(sObject s: objList){
                //finding the master object
                if(s.Id == t){
                    DescribeSObjectResult describeResult = t.getSObjectType().getDescribe();    
                    extraJSON = '"masterRecord":{"masterRecord":"' + t +'","masterRecordObject":"' + describeResult.getName() + '"}';
                //  jsonString += ',';
                    system.debug('extra JSON ' + extraJSON);
                }
                jsonString += JSON.serialize(s, true);
                jsonString += ',';
            }
        }
        
        //removing the last comma from the end to complete the array
        jsonString = jsonString.removeEnd(',');
        jsonString += '], ' + extraJSON + '}';
        
        /*
        if (!attachmentList.isEmpty())
        {
            jsonString = jsonString.removeEnd('}');
            jsonString += ',"attachments": [';
        }
        
        for (Attachment att : attachmentList)
        {
            String body = '"' + EncodingUtil.Base64Encode(att.body) + '",';
            String attJson = JSON.serialize(att, true);
            String part1 = attJson.substring(0,attJson.indexOf('"Body":')+7);
            String part2 = attJson.substring(attJson.indexOf('"ContentType":'));
            jsonString += part1 + body + part2;
            jsonString += ',';
        }
        
        if (!attachmentList.isEmpty())
        {
            //removing the last comma from the end to complete the array
            jsonString = jsonString.removeEnd(',');
            jsonString += ']}';
        }
        */
        
        system.debug('Roy4  jsonString: ' + jsonString);
        
        return jsonString;
    }
    
    //Returns an sObject with all the fields when you pass a set of Ids from the SAME object Type
    public static sObject selectAllFromId(Id recordId, list<string> relatedFields, list<string> blacklistedFields){
        
        DescribeSObjectResult describeResult = recordId.getSObjectType().getDescribe();
        List<String> fieldNames = new List<String>( describeResult.fields.getMap().keySet());
        
        //filtering out list of related fields based off of the current object
        list<string> removeList = new list<string>();
        string tempString;
        for(string s: relatedFields){
            if(s.contains(string.valueOf(describeResult.getName()) + ':')){
                tempString = s.removeStart(string.valueOf(describeResult.getName()) + ':');
                fieldNames.add(tempString);
            }
        }
        //removing Blacklisted fields from the field list
        for(string x: blacklistedFields){
            if(x.contains(string.valueOf(describeResult.getName()) + ':')){
                tempString = x.removeStart(string.valueOf(describeResult.getName()) + ':');
                removeList.add(tempString);
            }
        }
        Set<String> fieldsSet = new Set<String>(fieldNames);
        system.debug('Eeeeeeeeeeee1: ' + fieldsSet.size());
        for(string r: removeList){
            tempString = r.toLowerCase();
            system.debug('Eeeeeeeeeeee1.5: ' + tempString);
            fieldsSet.remove(tempString);
        }
        system.debug('Eeeeeeeeeeee2: ' + fieldsSet.size());
        list<string> newList = new list<string>(fieldsSet);
        
        
        String query =' SELECT ' + String.join( newList, ',' ) +' FROM ' + describeResult.getName() +' WHERE '+' id = :recordId LIMIT 1';       
        // return generic list of sobjects or typecast to expected type     
        List<SObject> records = Database.query( query );        
        
        return records[0];
    }
    
    public static Integration_Setting__mdt getMuleProcess(string eventType){
        list<Integration_Setting__mdt> intSettingsList = [SELECT Environment_Ind__c, Client_ID__c,Client_Secret__c, Related_Fields__c,
                Email_Address__c, Black_Listed_Fields__c, Label, URL_Suffix__c, Parent_Objects__c, Child_Objects__c, Endpoint_URL__c,
                Request_Type__c FROM Integration_Setting__mdt WHERE Label =: eventType limit 1];
        return intSettingsList[0];
    }
    
    public Static map<string, string> SetAuthorizationHeaderValues(list<Integration_Setting__mdt> settingsList){   
        map<string, string> headerMap = new map<string, string>();
        string ClientCredentials, authorizationHeader, endpoint, endPointSuffix;
        blob headerValue;
        
        for(Integration_Setting__mdt is: settingsList){
            if(is.Label == 'Sandbox' || is.Label == 'Production'){
                ClientCredentials = is.Client_ID__c + ':' + is.Client_Secret__c;
                headerValue = Blob.valueOf(ClientCredentials);
                authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                headerMap.put('Authorization', authorizationHeader);
                endpoint = is.Endpoint_URL__c;
                //headerMap.put('endpoint', endpoint);
            }else{
                endPointSuffix = is.URL_Suffix__c;
            }
        }
        headerMap.put('endpoint', endpoint + endPointSuffix);        
        return headerMap;
    }
    
    public static set<string> getObjTypes(set<id> idSet){
        string objType;
        set<string> objSet = new set<string>();
        
        for(id i: idSet){
            objType = string.ValueOf(i.getsobjecttype());
            system.debug('ROY2.3: ' + objType);
            objSet.Add(objType);
            system.debug('ROY2.4: ' + objSet);
        }
        return objSet;
    }
    
    public static Boolean runningInASandbox(){
        return [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
    }
    
    /********************************************************************************/
    /*  Create http request, send request, and get response                         */
    /********************************************************************************/  
    public static HttpResponse postToMulesoft(String jsonString, map<string, string> headerValues, string eventType, string requestType, string contentType) {
        
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        List<String> sendTo = new List <String>{'cfovel@tenable.com', 'rjones@tenable.com'};

        Http h = new Http();
        HttpRequest req = new HttpRequest();
        
        req.setHeader('Content-Type', contentType);
        if (headerValues.get('Authorization') != null){
        	req.setHeader('Authorization',headerValues.get('Authorization'));
        }
        
        req.setEndpoint(headerValues.get('endpoint'));
        req.setMethod(requestType);
        req.setTimeout(60000);
        if(jsonString != null){
        	req.setBody(jsonString);
        }
        HttpResponse response = h.send(req);                                                            // send http request
        system.debug('ROY7 response:' + response);
        if (response.getStatusCode() != 200) {                                        // send email if there is an error
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setReplyTo('sfdc@tenable.com');
            mail.setSenderDisplayName('Salesforce Automation');
            mail.setSubject('SFDC To Mulesfot ERROR: ' + eventType);
            String body = 'There was an error an error for this salesforce to mulesoft transaction.' + eventType + '  Response Code: ' + response.getStatusCode();
            mail.setToAddresses(sendTo);
            mail.setHtmlBody(body);
            mails.add(mail);
            try{
                Messaging.sendEmail(mails);
            }
            catch(Exception e){
                system.debug('@@ exception sending email:'+e);
            }
        }
        
        return response;
    }
    
    /*
    ////this method is for non standard mulesoft events. parameters will be event type and a Map<id,sObject> 
    //This method will only handle one value at a time
    public static boolean specialMulesoftEvent(map<id, sObject> objMap, string eventType){
        boolean validRequest          = true;
        boolean isSandbox             = runningInASandbox();
        set<string> validObjectSet    = new set<string>();
        
        //getting the integration setting based on the event type
        Integration_Setting__mdt currentMuleProcess = getMuleProcess(eventType);
        
        //getting the integration setting based on the Environment. Sandbox or Prod
        Integration_Setting__mdt envMuleProcess;
        if(isSandbox){
            envMuleProcess = getMuleProcess('Sandbox');
        }else{
            envMuleProcess = getMuleProcess('Production');
        }
        
        //getting a list of the valid objects. 
        string validObjects          = currentMuleProcess.Expected_Objects__c;
        list<string> validObjectList = validObjects.split(',');
        validObjectSet.addAll(validObjectList);
        
        set<string> objTypes = getObjTypes(objMap.keySet());
        system.debug('ROY1: ' + validObjectSet + ' and ' + objTypes);
        for(string s: objTypes){
            if(!validObjectSet.contains(s)){
                validRequest = false;
            }
        }
        
        return validRequest;
    }     

     //Return a map of objects based off of parent, child, and master id records. NOT DONE YET 
    public static map<id, list<sObject>> requestMapBuilder(set<id> idSet, set<string> parentObjects, set<string> childObjects, set<string> blacklist){
        map<id, list<sObject>> objMap = new map<id, list<sObject>>();
        string objType, tempString;
        list<sObject> objList;
        list<sObject> tempList;
        
        system.debug('ObjectMapBuilder: ');
        
        ////STEP 1 Get a map of the master record
        id sampleId;
        for(id d: idSet){
            sampleId = d;
        }    
        objList = selectAllFromId(idSet, sampleId, 'Master', blacklist);
        //adding the master object to the map also
        for(sObject s: objList){
            tempList = new list<sObject>();
            tempList.add(s);
            objMap.put(s.id, tempList);
            for(string c: parentObjects){
                if(c.contains('__c')){
                    tempString = '';
                }else{
                    tempString = 'Id';
                }
                
            }
        }
       
        //STEP 2 Get the values of the Parent objects
        for(String s: parentObjects){
          //  objList = selectAllFromId(idSet, , 'Parent', blacklist);
        }
        
        return objMap;
    }
    
    //Returns an sObject with all the fields when you pass a set of Ids from the SAME object Type
    public static list<sObject> selectAllFromId(set<Id> recordIds, id sampleId, string relationship, set<string> blacklist){
        
        DescribeSObjectResult describeResult = sampleId.getSObjectType().getDescribe(); 
        List<String> fieldNames = new List<String>( describeResult.fields.getMap().keySet() );      
        String query =' SELECT ' + String.join( fieldNames, ',' ) +' FROM ' + describeResult.getName() +' WHERE '+' id = :recordId ';       
        // return generic list of sobjects or typecast to expected type     
        List<SObject> records = Database.query( query );        
        
        return records;
    }
     */ 
}