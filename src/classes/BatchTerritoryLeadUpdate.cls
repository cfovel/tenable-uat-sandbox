global class BatchTerritoryLeadUpdate implements Database.Batchable<sObject> {
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([SELECT Id, Industry, DSCORGPKG__NAICS_Codes__c, NumberOfEmployees, City, State, Country, PostalCode, RecordTypeId, LeanData__Reporting_Matched_Account__c FROM Lead WHERE isConverted = false AND Status NOT IN ('Dead', 'Rejected', 'Recycle', 'Bad Data', 'JunQue')]); //AND Territory__c = null]);
    }
    
    global void execute(Database.BatchableContext bc, List<Lead> leads) {

        TerritoryAssignment assignment = new TerritoryAssignment();
        
        assignment.startLeads();
        assignment.setLeadTerritory(leads);
        assignment.finishLeads();
    }
    
    global void finish(Database.BatchableContext bc) {}
    
}