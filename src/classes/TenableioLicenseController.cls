public class TenableioLicenseController {

    /*
     * Add field to eval record to record errors
     */
    
    @RemoteAction
    public static Map<String, Object> getTenableioLicense(String uuid){
     
        boolean isSandbox = mulesoftIntegrationController.runningInASandbox();
        list<Integration_Setting__mdt> authHelperList = new list<Integration_Setting__mdt>();
        map<string, string> headerMap = new map<string, string>();
        
        //getting the integration setting based on the Environment. Sandbox or Prod
        Integration_Setting__mdt envMuleProcess;
        if(isSandbox){
            envMuleProcess = mulesoftIntegrationController.getMuleProcess('Sandbox');
        }else{
            envMuleProcess = mulesoftIntegrationController.getMuleProcess('Production');
        }
        authHelperList.add(envMuleProcess);
        
        //getting the header map ready for the request
        headerMap = mulesoftIntegrationController.SetAuthorizationHeaderValues(authHelperList);
        
        system.debug('Roy 1 header map ' + headerMap);
        string epSuffix = 'tenableio/container/' + uuid + '/license';
        headerMap.put('endpoint', envMuleProcess.Endpoint_URL__c + epSuffix);
        
        system.debug('ROY2: header map ' + headerMap);
        HttpResponse theResponse = mulesoftIntegrationController.postToMulesoft(null, headerMap, 'getTenableioLicense', 'GET', 'application/json');
        system.debug('Roy3: reponse body: ' + theResponse.getBody());
        
        //string theBody = EncodingUtil.urlDecode(theResponse.getBody(), 'UTF-8');
        Map<String, Object> theBody = (Map<String, Object>) JSON.deserializeUntyped(theResponse.getBody());
        system.debug('Roy4: reponse body: ' + theBody);
        
        return theBody; 
    }
    
    @future(callout=true)
    public static void futurePostLicenseExtension(Map<String,String> extDates)
    {
    	for (String licCode : extDates.values())
    	{
    		postLicenseExtension(licCode,extDates.get(licCode));
    	}
    }
    
    public static boolean postLicenseExtension(String licenseCode, string endDate){
     
        boolean isSandbox = mulesoftIntegrationController.runningInASandbox();
        list<Integration_Setting__mdt> authHelperList = new list<Integration_Setting__mdt>();
        map<string, string> headerMap = new map<string, string>();
        
        //getting the integration setting based on the Environment. Sandbox or Prod
        Integration_Setting__mdt envMuleProcess;
        if(isSandbox){
            envMuleProcess = mulesoftIntegrationController.getMuleProcess('Sandbox');
        }else{
            envMuleProcess = mulesoftIntegrationController.getMuleProcess('Production');
        }
        authHelperList.add(envMuleProcess);
        
        //getting the header map ready for the request
        headerMap = mulesoftIntegrationController.SetAuthorizationHeaderValues(authHelperList);
        
        system.debug('Roy 1 header map ' + headerMap);
        string epSuffix = 'installbase/' + licenseCode + '/extension/' + endDate;
        headerMap.put('endpoint', envMuleProcess.Endpoint_URL__c + epSuffix);
        
        system.debug('ROY2: header map ' + headerMap);
        HttpResponse theResponse = mulesoftIntegrationController.postToMulesoft(null, headerMap, 'postLicenseUpdate', 'POST', 'application/json');
        system.debug('Roy3: reponse body: ' + theResponse.getBody());
        
        //string theBody = EncodingUtil.urlDecode(theResponse.getBody(), 'UTF-8');
       // Map<String, Object> theBody = (Map<String, Object>) JSON.deserializeUntyped(theResponse.getBody());
       // system.debug('Roy4: reponse body: ' + theBody);
        
        return true; 
    }
    
    @RemoteAction
    public static boolean updateTenableioEval(string[] appStringArray, string uuid){
		//appStringArray looks like (consec:2018-10-20, pci:, was:2018-10-20, vm:2018-10-20)   
		  
        system.debug('appStringArray: ' + appStringArray);
        
       // try{
        boolean isSandbox = mulesoftIntegrationController.runningInASandbox();
        list<Integration_Setting__mdt> authHelperList = new list<Integration_Setting__mdt>();
        map<string, string> headerMap = new map<string, string>();
        map<string, long> appDateMap = new map<string, long>();
        map<string, string> evalDateMap = new map<string, string>();
        
        //getting the integration setting based on the Environment. Sandbox or Prod
        Integration_Setting__mdt envMuleProcess;
        if(isSandbox){
            envMuleProcess = mulesoftIntegrationController.getMuleProcess('Sandbox');
        }else{
            envMuleProcess = mulesoftIntegrationController.getMuleProcess('Production');
        }
        authHelperList.add(envMuleProcess);
        
        //getting the header map ready for the request
        headerMap = mulesoftIntegrationController.SetAuthorizationHeaderValues(authHelperList);
        
        system.debug('Roy 1 header map ' + headerMap);
        string epSuffix = 'tenableio/container/' + uuid + '/license';
        headerMap.put('endpoint', envMuleProcess.Endpoint_URL__c + epSuffix);
        
        system.debug('ROY2: header map ' + headerMap);
        //Getting the current license in Tenable.io
        HttpResponse theResponse = mulesoftIntegrationController.postToMulesoft(null, headerMap, 'getTenableioLicense', 'GET', 'application/json');
        system.debug('Roy3: reponse body: ' + theResponse.getBody());
        //string theBody = EncodingUtil.urlDecode(theResponse.getBody(), 'UTF-8');
        Map<String, Object> theBody = (Map<String, Object>) JSON.deserializeUntyped(theResponse.getBody());
        system.debug('Roy4: reponse body: ' + theBody);
            
       
        //Looping through app array to get the end date for each app
        for(String s: appStringArray){
            system.debug('ROy: ' + s);
            
            String[] stringArray = s.split(':');
            system.debug('ROy: ' + stringArray.size());
            if(stringArray.size() > 1){
                DateTime d = dateTime.valueOf((stringArray[1] + ' 23:59:59'));
                //converting milliseconds to seconds
                long l = (d.getTime() / 1000);
                appDateMap.put(stringArray[0],l);
            
                //creating a map for eval record updates
                evalDateMap.put(stringArray[0], stringArray[1]);
            }
            
        }
        system.debug('Roy5: ' + appDateMap);
        
        //looping through map to update TIO object
        Map<String, Object>  appMap = (Map<String, Object>) theBody.get('apps');
        Map<String, Object>  tempMap;
        for(string x: appDateMap.keySet()){
            if(x == 'vm'){
                theBody.put('expiration_date', appDateMap.get(x));
            }else{
                tempMap = (Map<String, Object>) appMap.get(x);
                
                tempMap.put('expiration_date', appDateMap.get(x));
                system.debug('ROy 3432: ' + tempMap);
                appMap.put(x, tempMap);
            }
        }
        theBody.put('apps', appMap);
        //Adding the enterprise pause tab
        theBody.put('enterprise_pause', false);
        
        system.debug('Roy6: ' + theBody);
        String JSONString = JSON.serialize(theBody);
        system.debug('Roy7: ' + uuid + ' body ' + JSONString);
        
        //sending request to update tenable.io license
        HttpResponse theResponse2 = mulesoftIntegrationController.postToMulesoft(JSONString, headerMap, 'updateTenableioLicense', 'PUT', 'application/json');
        system.debug('Roy8: reponse body: ' + theResponse2.getBody());
        
        ///Here we are updating the evaluation record to in salesforce to show the new end Date
        list<Evaluation__c> evalList = [select id, container_uuid__c, email__c, Eval_Product__c, Eval_Unique__c, Expiration_Date__c, Enterprise_Pause__c   from Evaluation__c where container_uuid__c =: uuid ];
        boolean wasPaused  = false;
        string evalEmail   = '';
        string evalproduct = '';
        string evalArray   = '';
        //setting up a feed item list to update
        list<FeedItem> feedItemList = new list<FeedItem>();
        FeedItem fItem;
        
        //Looping through to update the eval record in salesforce
        for(Evaluation__c e: evalList){
            //if any of the evals have enterprise_pause__c = true we need to send an unpause to marketo
            if(e.Enterprise_Pause__c == true){
                wasPaused = true;
            }
            evalEmail = e.Email__c;
            if(e.Eval_Product__c == 'TIO-CS;TIO-VM;TIO-WAS'){
                e.Expiration_Date__c = date.valueOf(evalDateMap.get('vm'));
                e.Enterprise_Pause__c  = false;
                evalProduct = e.Eval_Product__c + ';';
                evalArray = '"consec","vm","was"';
                fItem = new FeedItem(parentId = e.Id, 
        					body = 'changed Expiration Date to ' + evalDateMap.get('vm'), 
        					isRichText = false, 
        					Visibility = 'InternalUsers');
                feedItemList.add(fItem);
            }else if(e.Eval_Product__c == 'TIO-CS'){
                e.Expiration_Date__c = date.valueOf(evalDateMap.get('consec'));
                e.Enterprise_Pause__c  = false;
                evalProduct += e.Eval_Product__c + ';';
                evalArray   += '"consec",';
                fItem = new FeedItem(parentId = e.Id, 
        					body = 'changed Expiration Date to ' + evalDateMap.get('consec'), 
        					isRichText = false, 
        					Visibility = 'InternalUsers');
                feedItemList.add(fItem);
            }else if(e.Eval_Product__c == 'TIO-VM'){
                e.Expiration_Date__c = date.valueOf(evalDateMap.get('vm'));
                e.Enterprise_Pause__c  = false;
                evalProduct += e.Eval_Product__c + ';';
                evalArray   += '"vm",';
                fItem = new FeedItem(parentId = e.Id, 
        					body = 'changed Expiration Date to ' + evalDateMap.get('vm'), 
        					isRichText = false, 
        					Visibility = 'InternalUsers');
                feedItemList.add(fItem);
            }else if(e.Eval_Product__c == 'TIO-WAS'){
                e.Expiration_Date__c = date.valueOf(evalDateMap.get('was'));
                e.Enterprise_Pause__c  = false;
                evalProduct += e.Eval_Product__c + ';';
                evalArray   += '"was",';
                fItem = new FeedItem(parentId = e.Id, 
        					body = 'changed Expiration Date to ' + evalDateMap.get('was'), 
        					isRichText = false, 
        					Visibility = 'InternalUsers');
                feedItemList.add(fItem);
            }
        }
        update evalList; 
      
        //finally we are going to send a request to marketo to update add the eval unpause activity
        evalProduct = evalProduct.removeEnd(';');
        evalArray   = evalArray.removeEnd(',');
        string unpauseJSON = '';
        if(wasPaused == true){
            //updating the endpoing of the header map
            headerMap.put('endpoint', envMuleProcess.Endpoint_URL__c + 'tenableio/marketo/activity');
            unpauseJSON = '{"email":"' + evalEmail + '","apps":[' + evalArray + '],"uuid":"' + uuid + '","eval_product_code":"'+ evalProduct +'"}';
            system.debug('ROY: eval unpause json: ' + unpauseJSON);
            futureCallout(unpauseJSON, headerMap, 'POST');
        }
        
        //inserting feed items
        insert feedItemList;
        
        return true; 
    }
    
    @future(callout=true)
    public static void futureUpdateTenableioLicense(Map<String,String> extDates)
    {
    	for (String uuid : extDates.values())
    	{
    		String appString = extDates.get(uuid);
    		String[] appStringArray = appString.split(',');					
    		updateTenableioLicense(appStringArray,uuid);
    	}
    }
    
    public static boolean updateTenableioLicense(string[] appStringArray, string uuid){
		//appStringArray looks like (consec:2018-10-20, pci:, was:2018-10-20, vm:2018-10-20)   
		  
        system.debug('appStringArray: ' + appStringArray);
        
       // try{
        boolean isSandbox = mulesoftIntegrationController.runningInASandbox();
        list<Integration_Setting__mdt> authHelperList = new list<Integration_Setting__mdt>();
        map<string, string> headerMap = new map<string, string>();
        map<string, long> appDateMap = new map<string, long>();
        map<string, string> evalDateMap = new map<string, string>();
        
        //getting the integration setting based on the Environment. Sandbox or Prod
        Integration_Setting__mdt envMuleProcess;
        if(isSandbox){
            envMuleProcess = mulesoftIntegrationController.getMuleProcess('Sandbox');
            uuid = 'c5e06105-aebd-403a-ad53-741ba77bc05d';										// test with this uuid in sandbox
        }else{
            envMuleProcess = mulesoftIntegrationController.getMuleProcess('Production');
        }
        authHelperList.add(envMuleProcess);
        
        //getting the header map ready for the request
        headerMap = mulesoftIntegrationController.SetAuthorizationHeaderValues(authHelperList);
        
        system.debug('Roy 1 header map ' + headerMap);
        string epSuffix = 'tenableio/container/' + uuid + '/license';
        headerMap.put('endpoint', envMuleProcess.Endpoint_URL__c + epSuffix);
        
        system.debug('ROY2: header map ' + headerMap);
        //Getting the current license in Tenable.io
        HttpResponse theResponse = mulesoftIntegrationController.postToMulesoft(null, headerMap, 'getTenableioLicense', 'GET', 'application/json');
        system.debug('Roy3: reponse body: ' + theResponse.getBody());
        //string theBody = EncodingUtil.urlDecode(theResponse.getBody(), 'UTF-8');
        Map<String, Object> theBody = (Map<String, Object>) JSON.deserializeUntyped(theResponse.getBody());
        system.debug('Roy4: reponse body: ' + theBody);
            
       
        //Looping through app array to get the end date for each app
        for(String s: appStringArray){
            system.debug('ROy: ' + s);
            
            String[] stringArray = s.split(':');
            system.debug('ROy: ' + stringArray.size());
            if(stringArray.size() > 1){
                DateTime d = dateTime.valueOf((stringArray[1] + ' 23:59:59'));
                //converting milliseconds to seconds
                long l = (d.getTime() / 1000);
                appDateMap.put(stringArray[0],l);
            
                //creating a map for eval record updates
                evalDateMap.put(stringArray[0], stringArray[1]);
            }
            
        }
        system.debug('Roy5: ' + appDateMap);
        
        //looping through map to update TIO object
        Map<String, Object>  appMap = (Map<String, Object>) theBody.get('apps');
        Map<String, Object>  tempMap;
        for(string x: appDateMap.keySet()){
            if(x == 'vm'){
                theBody.put('expiration_date', appDateMap.get(x));
            }else{
                tempMap = (Map<String, Object>) appMap.get(x);
                
                tempMap.put('expiration_date', appDateMap.get(x));
                system.debug('ROy 3432: ' + tempMap);
                appMap.put(x, tempMap);
            }
        }
        theBody.put('apps', appMap);
        //Adding the enterprise pause tab
        theBody.put('enterprise_pause', false);
        
        system.debug('Roy6: ' + theBody);
        String JSONString = JSON.serialize(theBody);
        system.debug('Roy7: ' + uuid + ' body ' + JSONString);
        
        //sending request to update tenable.io license
        HttpResponse theResponse2 = mulesoftIntegrationController.postToMulesoft(JSONString, headerMap, 'updateTenableioLicense', 'PUT', 'application/json');
        system.debug('Roy8: reponse body: ' + theResponse2.getBody());
        
        return true; 
    }
    
    
    //Callout to add eval unpause activity
    @future(callout=true)
    public static void futureCallout(string jsonString, map<string, string> headerMap, string requestType){
        system.debug('ROY10: HTTP Response ' );
        HttpResponse theResponse = mulesoftIntegrationController.postToMulesoft(jsonString, headerMap, 'marketoUnpause', requestType, 'application/json');   
        system.debug('ROY11: HTTP Response ' + theResponse);
    }
    
    
}