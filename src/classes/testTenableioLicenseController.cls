@isTest
public class testTenableioLicenseController {
    //static accountDemoKey testAccGenerateKey;
    
    private static Evaluation__c dummyEval(){
        
        Utils.isTest = true;
        
        Evaluation__c a = new Evaluation__c(container_uuid__c='17ed3eef-d7d3-4b49-8e11-2cba14ae824e', Eval_Type__c='Tenable.io CONSEC;VM;WAS', eval_product__c='TIO-CS;TIO-VM;TIO-WAS', email__c='rjones@tenable.com', Eval_Unique__c='17ed3eef-d7d3-4b49-8e11-2cba14ae824e-TIO-CS;TIO-VM;TIO-WAS', name='rjones@tenable.com TIO-CS;TIO-VM;TIO-WAS');
        insert a; 
        
        return a;
    }
    
    @isTest
    static void testLicenseFunctions() {
        
        Evaluation__c testEval = dummyEval();
        //boolean isTrue;
        Test.startTest();
        
        // Set mock callout class                                                       
        Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
        //getTenableioLicense(testEval.container_uuid__c);  
        string[] stringArray = new string[]{};
        stringArray.add('vm:2018-10-23');
        stringArray.add('consec:2018-10-23');
        stringArray.add('was:2018-10-23');
        map<string, object> isTrue = TenableioLicenseController.getTenableioLicense(testEval.container_uuid__c);
        boolean isFalse = TenableioLicenseController.updateTenableioEval(stringArray,testEval.container_uuid__c);
        Test.stopTest();
    }
    
   /* private static void testAccountDemoKeyButtons(Account acc) {
    
       PageReference pageRef = Page.accountDemoKeyButton;
       Test.setCurrentPage(pageRef);
       ApexPages.StandardController sc = new ApexPages.standardController(acc);
       EvalExtensionButtonController   controller = new EvalExtensionButtonController(sc);
       System.assertNotEquals(null,controller.demoKeyLoad());
    
    }*/
}