public with sharing class archivedCodeClasses {


/*****************************************************************************************
*	LEAD REASSIGNMENTS
******************************************************************************************/
/*
			if ((oldMap == null) &&
				(String.valueof(l.OwnerId).startswith('00G60000002CaCk'))) {
				lids.add(l.id);
			}
			if (((oldMap != null) &&
				(oldMap.get(l.id) != null) &&
				(!String.valueof(oldMap.get(l.id).ownerid).startswith('00G60000002CaCk')) &&
				(String.valueof(l.OwnerId).startswith('00G60000002CaCk')))) {						// the lead is in the reassignment queue
				lids.add(l.id);
			}
*/


/*****************************************************************************************
*	LEAD TASK CREATION
******************************************************************************************/
//			CreateTasks(Trigger.new);

//	public static void CreateTasks(Lead[] lds) {
		/*****************************************************************************************
		*	Variable Initialization
		******************************************************************************************/
//		Task[] taskstoadd = new Task[0];															// Store any tasks we're creating
//		Set <Id> groups = new Set <Id>();															// Save the queues we find

		/*****************************************************************************************
		*	Record Pre-Processing
		******************************************************************************************/
/*
		for (Lead l : lds) {																		// Loop thru records we just added
			if ((l.leadsource.startswith('Webinar')) ||												// Only do this if we are a Webinar
				(l.leadsource.startswith('Event')) &&												// or an Event
				(String.valueof(l.OwnerId).startswith('00G'))) {									// And it's owned by a queue
				groups.add(l.OwnerId);																// Go save the queue id to go fetch the members
			}
		}

		GroupMember[] groupusers = 																	// Go get all the members of these groups
			([SELECT GroupId, UserOrGroupId 
			FROM GroupMember
			WHERE GroupId IN :groups]);
*/
		/*****************************************************************************************
		*	Record Processing
		******************************************************************************************/
/*
		for (Lead l : lds) {
			if ((l.leadsource.startswith('Webinar')) ||												// Only do this if we are a Webinar
				(l.leadsource.startswith('Event')) &&												// or an Event
				(!String.valueof(l.OwnerId).startswith('00G'))) {									// And it's owned by a user
				taskstoadd.add(new Task(Subject='Webinar/Event Follow-up', WhoId = l.id, Ownerid = l.ownerid));
			}

		}
*/
		/*****************************************************************************************
		*	Record Post-Processing
		******************************************************************************************/
//		if (taskstoadd.size() > 0) {insert taskstoadd;}												// Go create the tasks
//	}

/*******************************************************************************
*
* RSS Feed Class
* Version 1.0a
* Copyright 2006-2014 (c) by CloudLogistix.com
* All Rights Reserved
* Your are allowed to make modifications only for yourself, not for distribution
* without permission from CloudLogistix.
*
*	Read from a feed, return a list of feed entry records
*
*******************************************************************************/
	/*****************************************************************************************
	*	Constant Declaration
	******************************************************************************************/
/*
//	public rssFeedController() {system.debug('rssFeedController constructor');}
	integer debug = 2;
	string feedURLd;
	Integer entriesCountd = 5;																		// provide a default for this
	transient HttpResponse res = null;
	transient XMLDom dom;
*/
	/*****************************************************************************************
	*	Getter/Setters
	******************************************************************************************/
/*
	public integer getentriesToShow() {return entriesCountd;}

	public void setentriesToShow(integer i) {entriesCountd = i;}

//	public void setFeedURL(String u) {
//		system.debug('set feed url ');
//		feedURLd=u;
//	}

	public void setCompFeedURL(String u) {feedURLd = u;}

	public String getCompFeedURL() {return feedURLd;}
	public String getFeedURL() {return feedURLd;}

	public HttpResponse getResponse() {return this.res;}

	public string feedcachename {get;set;}

	public RssEntry[] getfeedcontentfromCache() {
		RssEntry[] ret = new RssEntry[]{};
		Document d = null;
		try {
			d = 
				([SELECT d.Body 
				FROM Document d 
				WHERE name = :feedcachename 
				LIMIT 1]);
		} 
		catch(exception e) {
			return ret;
		}

		dom = new XMLDom(d.body.tostring());
		if (debug> 5) dom.dumpAll();
		integer i=1;
	
		for (xmldom.element e: dom.getElementsByTagName('entry')) {
			ret.add(new RssEntry(e, i++));
			if ( i> entriesCountd) {break;}
		}

		if (ret.size() == 0 ) { // look at alt formats
			list<xmldom.element> items = dom.getElementsByTagName('item');
// deal with different formats
// example, community.salesforce.com has
// rss -> channel -> item list ( title, link, description pubDate, date, guid)
			for (xmldom.element e:items) {
				if (debug > 0) {e.dumpAll();}
				ret.add(new RssEntry(e, i++));
				if (i> entriesCountd) {break;}
			}
		}
		return ret;
	}

	public RssEntry[] getfeedcontent() {
		makeRequest(getCompFeedURL());
		string body = res.getBody();
		system.debug('length :'+body.length());

		if (debug> 3) {system.debug(body);}
		RssEntry[] ret = new RssEntry[]{};
		// return ret;
		dom = new XMLDom(body);
		if (debug> 3) {dom.dumpAll();}

		integer i=1;

		for (xmldom.element e: dom.getElementsByTagName('entry')) {
			ret.add(new RssEntry(e, i++));
			if (i> entriesCountd) {break;}
		}

		if (ret.size() == 0) { // look at alt formats
			list<xmldom.element> items = dom.getElementsByTagName('item');
// deal with different formats
// example, community.salesforce.com has
// rss -> channel -> item list ( title, link, description pubDate, date, guid)
			for (xmldom.element e:items) {
				if (debug > 0) {e.dumpAll();}
				ret.add(new RssEntry(e, i++));
				if (i> entriesCountd) {break;}
			}
		}
		return ret;
	}

	public class RssEntry {

		public RssEntry(Xmldom.Element ein, integer index) {
			e = ein; 
			idx = index;
		}

// some feeds return <date...
// others return <created... parse both
		public string getDate2() {
			try {
				Pattern p = Pattern.compile('[TZ]'); // cleanup the feed provided datetime
				Matcher m = p.matcher(e.getValue('date'));
				Datetime t = Datetime.valueof(m.replaceAll(' '));
				return '(' + t.format('MMMM d') + ')'; // format it
			} 
			catch (exception ex) {
// also parse this type of date string
// Wed, 30 Jan 2008 12:00:00 GMT
				return e.getValue('date');
			}
		}

		public string getDate() {
			try {
				Pattern p = Pattern.compile('[TZ]'); 												// cleanup the feed provided datetime
				Matcher m = p.matcher(e.getValue('created'));
				Datetime t = Datetime.valueof( m.replaceAll(' ') );
				return '(' + t.format('MMMM d') + ')'; // format it
			} 
			catch ( exception ex) {
			// also parse this type of date string
			// Wed, 30 Jan 2008 12:00:00 GMT
				return e.getValue('created');
			}
		}

	// some feeds differ in these details
		public integer getIndex() {return idx;}
	
		public string getSummary() {
			string ret = e.getValue('summary');
			return ret;
		}
	
		public string getTitle() {return e.getValue('title');}

		public string getHref() {
			string ret = e.getValue('origLink');
			if ( ret == null ) {
				ret = e.getValue('link');
			}
			return ret;
		}
	
		public string getAuthor() {
			string ret = e.getValue('name');
			if ( ret == null ) {
				ret = e.getValue('creator');
			}
			return ret;
		}

		Xmldom.Element e;
		integer idx;
	}

	public void makeRequest(string url) { // leaves result in res instance member
		HttpRequest req = new HttpRequest();
		req.setEndpoint(url);
		req.setMethod('GET');
*/
		/* avoid getting more than we can process on the callout
		* max is 32000, more will throw an exception from the server
		* we could go back for a second range, but this will give ~10 normal blog entries
		*/
/*
		req.setHeader('Range','bytes=0-30000');														// not all feeds respect this
		req.setCompressed(true);
		system.debug(req);
		try {
			Http http = new Http();
			res = http.send(req);
	
			// below here is not tested by test methods, this is expected since tests stop at http.send();
	
			if (res.getStatusCode() != 200 ) {
//				System.debug(res.toString());
				System.debug('STATUS:'+res.getStatus());System.debug('STATUS_CODE:'+res.getStatusCode());
				System.debug('BODY: '+res.getBody());
			}
	
		} 
		catch (System.Exception e) {
			System.debug('ERROR: '+ e);
		}
	}
*/

}