global class TerritoryAssignmentNoGeographic implements Schedulable {
    /*************************************************************************************
       To run EVERY 15 MINUTES, login as Automation User, open Execute Anonymous window, and execute:     
       TerritoryAssignmentNoGeographic sch = new TerritoryAssignmentNoGeographic();
       system.schedule('Batch Territory Assignment No Geographic - job1', '0 05 * * * ?', sch);  
       system.schedule('Batch Territory Assignment No Geographic - job2', '0 20 * * * ?', sch);  
       system.schedule('Batch Territory Assignment No Geographic - job3', '0 35 * * * ?', sch);  
       system.schedule('Batch Territory Assignment No Geographic - job4', '0 50 * * * ?', sch);                                  
     *************************************************************************************/
    
    global void execute(SchedulableContext sc) {
        
        Database.executeBatch(new BatchTerritoryAssignmentNoGeographic(), 200);
        
    }
   
}