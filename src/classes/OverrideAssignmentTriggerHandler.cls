public with sharing class OverrideAssignmentTriggerHandler
{
    public static List<OverrideAssignment__c> overrideAssignmentsToUpdate;
    public static List<OverrideAssignment__c> overrideAssignmentsToDelete;
    public static List<TerritoryAssignment__c> territoryAssignmentsToInsert;
    public static Map<Id, TerritoryAssignment__c> territoryAssignmentsToUpdate;
    public static List<TerritoryAssignment__c> territoryAssignmentsToDelete;
    
    public class OverrideAssignmentBeforeInsertHandler implements Triggers.Handler
    {
        public void handle() 
        {
            UpdateOverrideAssignmentData(Trigger.new);
        }
    } 
    
    public class OverrideAssignmentBeforeUpdateHandler implements Triggers.Handler
    {
        public void handle() 
        {
            UpdateOverrideAssignmentData(Trigger.new);
        }
    } 
    
    public class OverrideAssignmentBeforeDeleteHandler implements Triggers.Handler
    {
        public void handle() 
        {
            UpdateOverrideAssignmentData(Trigger.new);
        }
    } 
    
    public class OverrideAssignmentAfterInsertHandler implements Triggers.Handler
    {
        public void handle() 
        {
            UpdateTerritoryData(Trigger.new);
        }
    } 
    
    public class OverrideAssignmentAfterUpdateHandler implements Triggers.Handler
    {
        public void handle() 
        {
            UpdateTerritoryData(Trigger.new);
        }
    } 
    
    public static void UpdateOverrideAssignmentData(List<OverrideAssignment__c> newOverrideAssignments)
    {
        overrideAssignmentsToUpdate = new List<OverrideAssignment__c>();
        overrideAssignmentsToDelete = new List<OverrideAssignment__c>();
        territoryAssignmentsToInsert = new List<TerritoryAssignment__c>();
        territoryAssignmentsToUpdate = new Map<Id, TerritoryAssignment__c>();
        territoryAssignmentsToDelete = new List<TerritoryAssignment__c>();
        
        DateTime now = DateTime.now();
        
         if (Trigger.isInsert) {
            
            for (OverrideAssignment__c overrideAssignment : newOverrideAssignments) {
                
                if (overrideAssignment.Approved__c) {
                    if (overrideAssignment.StartAt__c == null && overrideAssignment.ApprovedAt__c != null)
                    {
                        overrideAssignment.StartAt__c = overrideAssignment.ApprovedAt__c;
                    }

                    if (overrideAssignment.StartAt__c <= now.addHours(-1))
                        overrideAssignment.addError('You cannot change things that have already happened.');
                    if (overrideAssignment.EndAt__c != null && overrideAssignment.EndAt__c <= now.addHours(-1))
                        overrideAssignment.addError('You cannot change things that have already happened.');

                }
                
            }
            
        } else if (Trigger.isUpdate) {
            
            for (OverrideAssignment__c overrideAssignment : newOverrideAssignments) {
            
                OverrideAssignment__c oldOverrideAssignment = (OverrideAssignment__c)Trigger.oldMap.get(overrideAssignment.Id);
                
                if (overrideAssignment.Approved__c) {
                    
                    if (overrideAssignment.Approved__c != oldOverrideAssignment.Approved__c && overrideAssignment.StartAt__c == null)
                    {
                        overrideAssignment.StartAt__c = now;
                    }
                    
                    if (overrideAssignment.Approved__c != oldOverrideAssignment.Approved__c && overrideAssignment.ApprovedBy__c == null)
                    {
                        overrideAssignment.ApprovedBy__c = Userinfo.getUserId();
                    }
                    
                    if (overrideAssignment.StartAt__c != oldOverrideAssignment.StartAt__c && (overrideAssignment.StartAt__c == null || overrideAssignment.StartAt__c <= now.addHours(-1) || (oldOverrideAssignment.StartAt__c != null && oldOverrideAssignment.StartAt__c <= now.addHours(-1))))
                    {
                        overrideAssignment.addError('You cannot change things that have already happened.');
                    }
                    
                    if (overrideAssignment.EndAt__c != oldOverrideAssignment.EndAt__c && ((overrideAssignment.EndAt__c != null && overrideAssignment.EndAt__c <= now.addHours(-1)) || (oldOverrideAssignment.EndAt__c != null && oldOverrideAssignment.EndAt__c <= now.addHours(-1))))
                    {
                        overrideAssignment.addError('You cannot change things that have already happened.');
                    }

                }
                
                if (overrideAssignment.Account__c != oldOverrideAssignment.Account__c)
                {
                    overrideAssignment.addError('You cannot change the Account on a Named Account.');
                }
                
                if (overrideAssignment.Approved__c != oldOverrideAssignment.Approved__c && !overrideAssignment.Approved__c)
                {
                    overrideAssignment.addError('You cannot unapprove a Named Account.');
                }
                
            }
            
        } else if (Trigger.isDelete) {
            
            for (TerritoryAssignment__c territoryAssignment : [SELECT Id, StartAt__c, EndAt__c FROM TerritoryAssignment__c WHERE OverrideAssignment__c IN :Trigger.oldMap.keySet() AND (EndAt__c = null OR EndAt__c > :now)]) {
                
                if (territoryAssignment.StartAt__c > now) {
                    
                    territoryAssignmentsToDelete.add(territoryAssignment);
                    
                } else {
                    
                    if (territoryAssignmentsToUpdate.containsKey(territoryAssignment.Id) && territoryAssignmentsToUpdate.get(territoryAssignment.Id) != null)
                        territoryAssignment = territoryAssignmentsToUpdate.get(territoryAssignment.Id);
                    
                    territoryAssignment.EndAt__c = now;
                    territoryAssignmentsToUpdate.put(territoryAssignment.Id, territoryAssignment);
                    
                }
            }
        }
        PerformUpdates();
    }
    
    public static void UpdateTerritoryData(List<OverrideAssignment__c> newOverrideAssignments)
    {
        overrideAssignmentsToUpdate = new List<OverrideAssignment__c>();
        overrideAssignmentsToDelete = new List<OverrideAssignment__c>();
        territoryAssignmentsToInsert = new List<TerritoryAssignment__c>();
        territoryAssignmentsToUpdate = new Map<Id, TerritoryAssignment__c>();
        territoryAssignmentsToDelete = new List<TerritoryAssignment__c>();
        
        User automationUser = [SELECT Id FROM User WHERE ((FirstName = 'Automation' AND LastName = 'User') OR LastName = 'Automation User') AND IsActive = true LIMIT 1];
        RecordType overrideAssignmentRecordType = [SELECT Id FROM RecordType WHERE sObjectType = 'TerritoryAssignment__c' AND DeveloperName = 'OverrideAssignment' AND IsActive = true LIMIT 1];
        
        if (Trigger.isInsert) {
            
            Map<Id, Account> mapAccounts = new Map<Id, Account>();
            
            for (OverrideAssignment__c overrideAssignment : newOverrideAssignments) {
                
                if (overrideAssignment.Approved__c) {
                    mapAccounts.put(overrideAssignment.Account__c, null);
                }
                
            }
            
            mapAccounts = new Map<Id, Account>([SELECT Id, (SELECT Id, StartAt__c, EndAt__c FROM OverrideAssignments__r) FROM Account WHERE Id IN :mapAccounts.keySet()]);
            
            for (OverrideAssignment__c overrideAssignment : newOverrideAssignments) 
            {
                if (overrideAssignment.Approved__c) 
                {
                    if (mapAccounts.containsKey(overrideAssignment.Account__c)) 
                    {
                        for (OverrideAssignment__c overrideAssignmentSibling : mapAccounts.get(overrideAssignment.Account__c).OverrideAssignments__r) 
                        {
                            if (overrideAssignmentSibling.Id != overrideAssignment.Id && overrideAssignment.StartAt__c != null) 
                            {
                                if (overrideAssignment.StartAt__c > overrideAssignmentSibling.StartAt__c && (overrideAssignmentSibling.EndAt__c == null || overrideAssignment.StartAt__c < overrideAssignmentSibling.EndAt__c)) 
                                {
                                    overrideAssignmentsToUpdate.add(new OverrideAssignment__c(
                                        Id = overrideAssignmentSibling.Id,
                                        EndAt__c = overrideAssignment.StartAt__c
                                    ));
                                } 
                                else if (overrideAssignment.StartAt__c <= overrideAssignmentSibling.StartAt__c) 
                                {
                                    if (overrideAssignment.EndAt__c == null || (overrideAssignmentSibling.EndAt__c != null && overrideAssignment.EndAt__c >= overrideAssignmentSibling.EndAt__c)) 
                                    {
                                        overrideAssignmentsToDelete.add(overrideAssignmentSibling);
                                    } 
                                    else if (overrideAssignmentSibling.EndAt__c == null || overrideAssignment.EndAt__c < overrideAssignmentSibling.EndAt__c) 
                                    {
                                        overrideAssignmentsToUpdate.add(new OverrideAssignment__c(
                                            Id = overrideAssignmentSibling.Id,
                                            StartAt__c = overrideAssignment.EndAt__c
                                        ));
                                    }
                                }
                            }
                        }
                    }
                    territoryAssignmentsToInsert.add(new TerritoryAssignment__c(
                        RecordTypeId = overrideAssignmentRecordType.Id,
                        OverrideAssignment__c = overrideAssignment.Id,
                        Account__c = overrideAssignment.Account__c,
                        Territory__c = overrideAssignment.Territory__c,
                        OwnerId = automationUser.Id,
                        StartAt__c = overrideAssignment.StartAt__c,
                        EndAt__c = overrideAssignment.EndAt__c,
                        Preview__c = false
                    ));
                    
                }
            }
            
        } else if (Trigger.isUpdate) {
            
            Set<Id> startAtDelta = new Set<Id>();
            Set<Id> endAtDelta = new Set<Id>();
            
            Map<Id, Account> mapAccounts = new Map<Id, Account>();
            
            for (OverrideAssignment__c overrideAssignment : newOverrideAssignments) {
                
                OverrideAssignment__c oldOverrideAssignment = (OverrideAssignment__c)Trigger.oldMap.get(overrideAssignment.Id);
                
                if (overrideAssignment.Approved__c != oldOverrideAssignment.Approved__c && overrideAssignment.Approved__c) {
                    mapAccounts.put(overrideAssignment.Account__c, null);
                }
                
                if (overrideAssignment.StartAt__c != oldOverrideAssignment.StartAt__c) {
                    startAtDelta.add(overrideAssignment.Id);
                }
                
                if (overrideAssignment.EndAt__c != oldOverrideAssignment.EndAt__c) {
                    endAtDelta.add(overrideAssignment.Id);
                }
                
            }
            
            mapAccounts = new Map<Id, Account>([SELECT Id, (SELECT Id, StartAt__c, EndAt__c FROM OverrideAssignments__r) FROM Account WHERE Id IN :mapAccounts.keySet()]);
            
            for (OverrideAssignment__c overrideAssignment : newOverrideAssignments) {
            
                OverrideAssignment__c oldOverrideAssignment = (OverrideAssignment__c)Trigger.oldMap.get(overrideAssignment.Id);
                
                if (overrideAssignment.Approved__c != oldOverrideAssignment.Approved__c && overrideAssignment.Approved__c) {
                    
                    if (mapAccounts.containsKey(overrideAssignment.Account__c)) {
                        for (OverrideAssignment__c overrideAssignmentSibling : mapAccounts.get(overrideAssignment.Account__c).OverrideAssignments__r) {
                            if (overrideAssignmentSibling.Id != overrideAssignment.Id && overrideAssignment.StartAt__c != null) {
                                if (overrideAssignment.StartAt__c > overrideAssignmentSibling.StartAt__c && (overrideAssignmentSibling.EndAt__c == null || overrideAssignment.StartAt__c < overrideAssignmentSibling.EndAt__c)) {
                                    overrideAssignmentsToUpdate.add(new OverrideAssignment__c(
                                        Id = overrideAssignmentSibling.Id,
                                        EndAt__c = overrideAssignment.StartAt__c
                                    ));
                                } else if (overrideAssignment.StartAt__c <= overrideAssignmentSibling.StartAt__c) {
                                    if (overrideAssignment.EndAt__c == null || (overrideAssignmentSibling.EndAt__c != null && overrideAssignment.EndAt__c >= overrideAssignmentSibling.EndAt__c)) {
                                        overrideAssignmentsToDelete.add(overrideAssignmentSibling);
                                    } else if (overrideAssignmentSibling.EndAt__c == null || overrideAssignment.EndAt__c < overrideAssignmentSibling.EndAt__c) {
                                        overrideAssignmentsToUpdate.add(new OverrideAssignment__c(
                                            Id = overrideAssignmentSibling.Id,
                                            StartAt__c = overrideAssignment.EndAt__c
                                        ));
                                    }
                                }
                            }
                        }
                    }
                    
                    territoryAssignmentsToInsert.add(new TerritoryAssignment__c(
                        RecordTypeId = overrideAssignmentRecordType.Id,
                        OverrideAssignment__c = overrideAssignment.Id,
                        Account__c = overrideAssignment.Account__c,
                        Territory__c = overrideAssignment.Territory__c,
                        OwnerId = automationUser.Id,
                        StartAt__c = overrideAssignment.StartAt__c,
                        EndAt__c = overrideAssignment.EndAt__c,
                        Preview__c = false
                    ));
                    
                }
                
            }
            
            if (!startAtDelta.isEmpty()) {
                
                for (OverrideAssignment__c overrideAssignment : [SELECT Id, (SELECT Id, StartAt__c FROM TerritoryAssignments__r) FROM OverrideAssignment__c WHERE Id IN :startAtDelta]) {
                
                    OverrideAssignment__c oldOverrideAssignment = (OverrideAssignment__c)Trigger.oldMap.get(overrideAssignment.Id);
                    
                    for (TerritoryAssignment__c territoryAssignment : overrideAssignment.TerritoryAssignments__r) {
                        
                        if (territoryAssignment.StartAt__c != oldOverrideAssignment.StartAt__c)
                            continue;
                        
                        if (territoryAssignmentsToUpdate.containsKey(territoryAssignment.Id) && territoryAssignmentsToUpdate.get(territoryAssignment.Id) != null) {
                            territoryAssignment = territoryAssignmentsToUpdate.get(territoryAssignment.Id);
                        }
                        
                        territoryAssignment.StartAt__c = ((OverrideAssignment__c)Trigger.newMap.get(overrideAssignment.Id)).StartAt__c;
                        //territoryAssignment.StartAt__c = overrideAssignment.StartAt__c;
                        territoryAssignmentsToUpdate.put(territoryAssignment.Id, territoryAssignment);
                        
                    }
                    
                }
            }
            
            if (!endAtDelta.isEmpty()) {
                
                for (OverrideAssignment__c overrideAssignment : [SELECT Id, (SELECT Id, EndAt__c FROM TerritoryAssignments__r) FROM OverrideAssignment__c WHERE Id IN :endAtDelta]) {
                
                    OverrideAssignment__c oldOverrideAssignment = (OverrideAssignment__c)Trigger.oldMap.get(overrideAssignment.Id);
                    
                    for (TerritoryAssignment__c territoryAssignment : overrideAssignment.TerritoryAssignments__r) {
                        
                        if (territoryAssignment.EndAt__c != oldOverrideAssignment.EndAt__c)
                            continue;
                        
                        if (territoryAssignmentsToUpdate.containsKey(territoryAssignment.Id) && territoryAssignmentsToUpdate.get(territoryAssignment.Id) != null) {
                            territoryAssignment = territoryAssignmentsToUpdate.get(territoryAssignment.Id);
                        }
                        
                        territoryAssignment.EndAt__c = ((OverrideAssignment__c)Trigger.newMap.get(overrideAssignment.Id)).EndAt__c;
                        //territoryAssignment.EndAt__c = overrideAssignment.EndAt__c;
                        territoryAssignmentsToUpdate.put(territoryAssignment.Id, territoryAssignment);
                        
                    }
                    
                }
                
            }    
        }
        
        PerformUpdates();
        
    }
    
    public static void PerformUpdates()
    {
        if (!territoryAssignmentsToInsert.isEmpty())
            insert territoryAssignmentsToInsert;
    
        if (!territoryAssignmentsToUpdate.isEmpty())
            update territoryAssignmentsToUpdate.values();
        
        if (!territoryAssignmentsToDelete.isEmpty())
            delete territoryAssignmentsToDelete;
        
        if (!overrideAssignmentsToUpdate.isEmpty())
            update overrideAssignmentsToUpdate;
        
        if (!overrideAssignmentsToDelete.isEmpty())
            delete overrideAssignmentsToDelete;
            
    }
}