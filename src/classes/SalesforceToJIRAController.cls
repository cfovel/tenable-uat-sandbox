global with sharing class SalesforceToJIRAController {
   
   public Id caseId = null;
   public SObject theCase;
   public ApexPages.StandardController controller;
   
   public SalesforceToJIRAController(ApexPages.StandardController c) {
		controller = c;
    	caseId = c.getRecord().id;
    	//muleUser = getMuleUser();
   }
  
   @RemoteAction
    global Static map<string,string> createJiraTicket(String caseId){
       	
       	String JIRAResponse, errorResponse;
   		boolean hasError = false;
   		/*Here we are creating an apex map from the return JSON*/
   		map<string, string> errorString  = new map<string, string>();
        map<String, String> returnJSON   = new map<String, String>();
        map<string, string> JIRAFieldMap = new map<string, string>{'issueNumber' => 'Jira_Case_Number__c', 'issueId' => 'Jira_Ticket_Number__c', 'priority' => 'Jira_Priority__c', 'reporter' => 'Jira_Assignee__c',
   																	 'status' => 'Jira_Status__c', 'resolution' => 'Jira_Resolution__c', 'created' => 'Jira_Created_Date__c', 'description' => 'Description',
   																	 'updated' => 'Jira_Updated_Date__c', 'assignee' => 'Jira_Assignee__c', 'subject' => 'Subject', 'customerName' => 'Account', 'issueType' => 'Jira_Type__c'};
   
		
		/*Getting the values in the current Case*/
		//string query = 'select id, CaseNumber, ContactId from '+objType+' where id = : objId';
		//SObject caseList = Database.query(query); 
		string query = 'select Id, Priority, Type, Subject, Description, Account.Name, Status, CaseNumber, Product__c, Contact.Name, '+
							'Jira_Assignee__c, Jira_Case_Number__c, Jira_Created_Date__c, Jira_Fixed_Versions__c, Jira_Priority__c,'+
							'Jira_Resolution__c, Jira_Status__c, Jira_Ticket_Link__c, Jira_Ticket_Number__c, Jira_Type__c, Jira_Updated_Date__c '+
							'from case where id =: caseId';
							
		SObject currentCase = Database.query(query); 
		
		/*Here we are getting the user name of the logged in user*/
		String userId   = UserInfo.getUserId();
		String UserName = [select CommunityNickname, email from User where id =: userId].email;
		UserName = UserName.substringBefore('@');
		
		map<String,String> caseFieldsMap = new map<String,String>{};
		caseFieldsMap.put('priority', String.valueOf(currentCase.get('Priority')));
		caseFieldsMap.put('caseId', String.valueOf(currentCase.get('Id')));
        if(UserName != null){
        	caseFieldsMap.put('reporter', UserName);
        }
        caseFieldsMap.put('issueType', String.valueOf(currentCase.get('Jira_Type__c')));
        /*The project is a value to distinguish the JIRA environment we are creating a ticket it.  If we run this code in the sandbox
          the ticket will be created in JIRA test instance.  If we run it is production it will be created in JIRA prod instance.*/
        if(runningInASandbox()){
        	caseFieldsMap.put('project', 'CSDEV');
        }else{
        	caseFieldsMap.put('project', 'CS');
        }
        caseFieldsMap.put('subject', String.valueOf(currentCase.get('Subject')));
        caseFieldsMap.put('description', String.valueOf(currentCase.get('Description')));
      	caseFieldsMap.put('status', String.valueOf(currentCase.get('Status')));
        
        caseFieldsMap.put('caseNumber', String.valueOf(currentCase.get('CaseNumber')));
        if(currentCase.getSObject('account') != null){
	        SObject acc = currentCase.getSObject('account');
			String accountName = (String) acc.get('name');
	        caseFieldsMap.put('customerName', accountName);
        }else{
        	caseFieldsMap.put('customerName', null);
        }
        if(currentCase.getSObject('Contact') != null){
	        SObject acc = currentCase.getSObject('Contact');
			String conName = (String) acc.get('Name');
	        caseFieldsMap.put('contactName', conName);
        }
        if(currentCase.get('Product__c') != null){
        	caseFieldsMap.put('component', String.valueOf(currentCase.get('Product__c')));
        	caseFieldsMap.put('productName', String.valueOf(currentCase.get('Product__c')));
        }else{
        	caseFieldsMap.put('component', 'SecurityCenter');
        	caseFieldsMap.put('productName', 'SecurityCenter');
        }
        
        errorResponse = 'The following required fields are blank: <br/><br/>';
        	
        integer i = 0;
        for(string s: caseFieldsMap.keySet()){
        	system.debug('Roy 3.5: ' + s + ', ' + caseFieldsMap.get(s));
			if(caseFieldsMap.get(s) == null){
				hasError = true;
				errorResponse += JIRAFieldMap.get(s) + ' <br/>';
				errorString.put(string.valueOf(i),JIRAFieldMap.get(s));
				i++;
				//caseFieldsMap.put(s, '');
			}else if(caseFieldsMap.get(s) == 'Channel Partner'){
				hasError = true;
				errorString.clear();
				errorResponse = 'Channel Partner is not a valid product for a JIRA ticket.';
				errorString.put('fatal',errorResponse);
			}
		}
        
        if(currentCase.get('Jira_Case_Number__c') != null && currentCase.get('Jira_Case_Number__c') != ''){
        	hasError = true;
			errorResponse = 'This case already has a Jira ticket: ' + string.valueOf(currentCase.get('Jira_Case_Number__c'));
			errorString.clear();
			errorString.put('fatal',errorResponse);
   		}
   		
   		/*We are making the call out to JIRA here*/
        if(hasError == false){
        	JIRAResponse = SalesforceToJiraAPICalls.CreateJiraTicket(caseFieldsMap);
        	try{
		        returnJSON = (map<String, String>)JSON.deserialize(JIRAResponse, Map<String,String>.class);
		        system.debug('ROY 5: ' + returnJSON);
		        /*calling the updateCaseFrom JIRA Method so that we can process the results*/
		   		if(returnJSON.get('success') == 'true'){
		    	
					PageReference pageRef = new PageReference('/' + caseId);
					pageRef.setRedirect(true);
					errorResponse = 'Ticket Created';
					errorString.clear();
					errorString.put('sucess', errorResponse);
					return errorString;
		    	}else{
		   			system.debug('Roy: The call was not a success: ' + returnJSON);
		   			errorResponse = 'Something went wrong. Error message is: ' + returnJSON;
					errorString.clear();
					errorString.put('fatal',errorResponse);
		            return errorString;
		   		}
		        }catch(exception e){
		        	system.debug('Roy 5.5 There is an error message: ' + JIRAResponse);
		       			errorResponse = 'Something went wrong. ' + JIRAResponse;
						errorString.clear();
						errorString.put('fatal',errorResponse);
		                return errorString;
		        }
        }else{
        	system.debug('Roy: There are fields that are missing ' + returnJSON);
   			return errorString;
        }	    
    }
    
    //Here we are updating the record on the fly from the visualforce page. 
    @RemoteAction
    global Static boolean updateCaseFields(string caseId, string fieldName, string fieldValue){
    	boolean isSuccess = true;
    	Case currentCase;
    	
    	String qry = '';
        currentCase = new Case();
        
        qry = 'SELECT Id, subject, description, Jira_Type__c FROM case WHERE ID  = \'' + caseId + '\'';

        currentCase = Database.Query(qry);
    	
    	currentCase.put(fieldName, fieldValue);
    	
    	update currentCase;
    	
    	return isSuccess;
    }
   
   //Here we are checking to see if there is an existing Jira ticket. If there is we are going to disable the create Jira ticket button.  
    @RemoteAction
    global Static boolean checkExistingRecord(string caseId){
    	boolean isSuccess = true;
    	Case currentCase;
    	
        currentCase =  [select Id, subject, description, Jira_Case_Number__c from Case where Id  =: caseId ];
    	
    	if(currentCase.Jira_Case_Number__c == null || currentCase.Jira_Case_Number__c == ''){
    		return false;
    	}else{
    		return true;
    	}
    	
    }
 
   /*This is the method to update an existing JIRA ticket with the corresponding Case informations */
    @future (callout=true)
	public static void UpdateJIRATicket(set<Id> caseIds, set<id> userIds) {
		string muleUser;
		set<Id> commenterIds                = new set<Id>();
		map<Id, String> commenterNameMap    = new map<Id, String>();
		map<String, String> FieldMap = new map<String, String>();
		
		muleUser = getMuleUser();//'00555000001q8WOAAY';
		commenterNameMap = getUserNickName(userIds);
		system.debug('Roy 22');
		
		list<Case> caseList = [select Id, Priority, Status, CaseNumber, LastModifiedById, Jira_Case_Number__c from Case where Id =: caseIds];
		for(Case c: caseList){
			if(c.LastModifiedById != muleUser && c.LastModifiedById != '00560000001T5Tz'){//Automation User
				FieldMap.clear();
				FieldMap.put('caseNumber', c.CaseNumber);
				FieldMap.put('caseId', c.Id);
				FieldMap.put('priority', c.Priority);
				FieldMap.put('status', c.Status);
				FieldMap.put('reporter', commenterNameMap.get(c.LastModifiedById));
				
				for(string s: FieldMap.keySet()){
					if(FieldMap.get(s) == null){
						FieldMap.put(s, '');
					}
				}
				
	    		String JIRAResponse = SalesforceToJiraAPICalls.UpdateJIRATicket(FieldMap, c.Jira_Case_Number__c);
			}
		}
		/*We are making the call out to JIRA here, to create a new ticket comment in JIRA*/
	}
   
   /*This method is going to handle adding a case comment to jira when there is a new comment on a salesforce ticket. */
    @future (callout=true)
	public static void CreateJiraCaseComments(Set<id> commentIds) {
		string muleUser;
		set<Id> commenterIds                = new set<Id>();
		map<Id, String> commenterNameMap    = new map<Id, String>();
		map<String, String> commentFieldMap = new map<String, String>();
		
		list<CaseComment> commentList = [select id, Parent.Jira_Case_Number__c, CommentBody, CreatedById, LastModifiedById from CaseComment where id =: commentIds];
		for(CaseComment x: commentList){
			system.debug('ROY 33: ' + x.Parent.Jira_Case_Number__c);
			if(!x.CommentBody.containsIgnoreCase('#nojira')){
				commenterIds.add(x.CreatedById);
			}
		}
		
		muleUser = getMuleUser();//'00555000001q8WOAAY';
		commenterNameMap = getUserNickName(commenterIds);
		
		/*list<User> userList = [select Id, Name, CommunityNickname from User where Id =: commenterIds];
		for(User u: userList){
			commenterNameMap.put(u.Id, u.CommunityNickname);
		}*/

		for(CaseComment c: commentList){
			commentFieldMap.clear();
			string thisNumber  = c.Parent.Jira_Case_Number__c;
			system.debug('Roy Mulesoft User! ' + muleUser);
			system.debug('Roy Last Modified User! ' + c.LastModifiedById);
			if(!c.CommentBody.contains('#nojira')){
				if((c.Parent.Jira_Case_Number__c != null && c.Parent.Jira_Case_Number__c != '') && (c.LastModifiedById != muleUser && c.LastModifiedById != '00560000001T5Tz')){
					if(thisNumber.startsWithIgnoreCase('cs')){
						commentFieldMap.put('text', c.CommentBody.remove('#jira'));
						commentFieldMap.put('createdBy', commenterNameMap.get(c.CreatedById));
						/*We are making the call out to JIRA here, to create a new ticket comment in JIRA*/
		        		String JIRAResponse = SalesforceToJiraAPICalls.CreateJiraTicketComment(commentFieldMap, c.Parent.Jira_Case_Number__c);
					}
				}
			}
		}
		
	}
   
   /*We are getting the schema map here to check the field type to make sure that we are converting the JSON values to the correct fields. */
 /*  public static Schema.DisplayType getFieldType(String fieldName){
   		
   		Schema.SObjectField field = caseFieldTypeMap.get(fieldName);
   		Schema.DisplayType FldType = field.getDescribe().getType();
   		return FldType;
   }*/
   
   /*Here we are getting the actual name of the user that is returned from the JSON */
 /*  public static String getUserName(String currentVal){
   		return [SELECT Id, CommunityNickname, Name FROM User where CommunityNickname =: currentVal LIMIT 1].Name;
   }*/
   
   /*Here we are getting the nickname of the user from their Id */
   public static map<id, String> getUserNickName(set<Id> idSet){
   		string emailAlias;
   		map<id, String> idMap = new map<Id, String>();
   		list<User> userList =[select id, Name, CommunityNickname, email from User where id =: idSet];
   		for(User u: userList){
   			emailAlias = u.email;
   			emailAlias = emailAlias.substringBefore('@');
   			idMap.put(u.Id, emailAlias);
   		}
   		return idMap;
   }
   
   /*Here we are checking if we are in a production or sandbox instance */
   public static Boolean runningInASandbox() {
   		return [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
   }
   
   /*Getting the value of the mulesoft user so that we don't have a problem with infinate loops*/
   public static Id getMuleUser(){
   		return [select Id from User where Name = 'User Mulesoft'].Id;
   } 
   
   
   
    public static map<String, Schema.SObjectField> caseFieldTypeMap = Schema.SObjectType.Case.fields.getMap();
   /*This is the map of all the JIRA JSON key values to the coresponing Case fields */
   public map<string, string> JIRAFieldMap2 = new map<string, string>{'issueNumber' => 'Jira_Case_Number__c', 'issueId' => 'Jira_Ticket_Number__c', 'priority' => 'Jira_Priority__c', 'reporter' => 'Jira_Assignee__c',
   																	 'status' => 'Jira_Status__c', 'resolution' => 'Jira_Resolution__c', 'created' => 'Jira_Created_Date__c', 'description' => 'Description',
   																	 'updated' => 'Jira_Updated_Date__c', 'assignee' => 'Jira_Assignee__c', 'subject' => 'Subject', 'customerName' => 'Account', 'issueType' => 'Jira Type'};
   
}