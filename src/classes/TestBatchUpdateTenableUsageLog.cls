/*************************************************************************************
Class: TestBatchUpdateTenableUsageLog
Author: Bharathi.M
Date: 03/17/2017
Details: This class is the test class for the batch BatchUpdateTenableUsageLog
***************************************************************************************/
//@isTest(seealldata = true)
//private class TestBatchUpdateTenableUsageLog 
public class TestBatchUpdateTenableUsageLog 
{
/*
    static testMethod void TestInstallBaseUsageLogBatch() 
    {

        Account acc  = new Account(Name='Test Batch Account Inc.', BillingState='MD', BillingCountry='US', website='http://www.testbatch.com');
        insert acc;
        
        
        Contact con = new Contact(FirstName = 'test', LastName = 'Batch', AccountID = acc.Id,Pendo_Container_Name__c='Test',
                                  Pendo_Container_Site__c = 'ac1', Pendo_Container_UUID__c = 'c5466e63-b81d-4b12-a0ae-c71bd7f3',  
                                  Pendo_First_Visit__c ='Feb 2, 2017 8:02:30 PM EDT',
                                  Pendo_Last_Visit__c='Mar 7, 2017 12:55:14 AM EDT',Pendo_Number_Of_Days_Active__c=10,
                                  Pendo_Number_Of_Events__c = 2,Pendo_Role__c = 'Administrator',Pendo_Time_On_Site_Minutes__c = 160);
        
        insert con;
        
        Install_Base__c ibase = new Install_Base__c(Name = 'Test',Account__c = acc.Id, UUID__c = 'c5466e63-b81d-4b12-a0ae-c71bd7f3');
        insert ibase;
        
        Test.startTest();
            BatchUpdateTenableUsageLog uBatch = new BatchUpdateTenableUsageLog();
            Id batchprocess = Database.executeBatch(uBatch);
        Test.stopTest();
    }
    
    static testMethod void TestEvalsUsageLogBatch() 
    {
        // TO DO: implement unit test
        Account acc  = new Account(Name='Test Batch Account Inc.', BillingState='MD', BillingCountry='US', website='http://www.testbatch.com');
        insert acc;
        
        
        Contact con = new Contact(FirstName = 'test', LastName = 'Batch', AccountID = acc.Id,Pendo_Container_Name__c='Test',
                                  Pendo_Container_Site__c = 'ac1', Pendo_Container_UUID__c = 'c5466e63-b81d-4b12-a0ae-c71bd7f3',  
                                  Pendo_First_Visit__c ='Feb 2, 2017 8:02:30 PM EDT',
                                  Pendo_Last_Visit__c='Mar 7, 2017 12:55:14 AM EDT',Pendo_Number_Of_Days_Active__c=10,
                                  Pendo_Number_Of_Events__c = 2,Pendo_Role__c = 'Administrator',Pendo_Time_On_Site_Minutes__c = 120);
        
        insert con;
        
        Evaluation__c Eval = new Evaluation__c(Name = 'Test',Eval_Type__c = 'Tenable.io',Contact__c = con.Id, Container_uuid__c = 'c5466e63-b81d-4b12-a0ae-c71bd7f3');
        insert Eval;
        
        Test.startTest();
            BatchUpdateTenableUsageLog uBatch = new BatchUpdateTenableUsageLog();
            Id batchprocess = Database.executeBatch(uBatch);
        Test.stopTest();
    }
*/
}