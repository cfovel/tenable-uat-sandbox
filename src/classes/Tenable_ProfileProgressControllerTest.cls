@isTest
public with sharing class Tenable_ProfileProgressControllerTest {

    @isTest
    public static void testGetSharing(){
        Utils.isTest = true;
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
        User testUser = Peak_TestUtils.createStandardUser();
        insert testUser;
        test.stopTest(); 

        System.runAs(new User(Id = UserInfo.getUserId())) {

            testUser.Completed_Welcome_Slide__c = true;
            testUser.Completed_Profile_Slide__c = true;
            testUser.Completed_Notification_Slide__c = true;
            testUser.Completed_Topics_Slide__c = true;
            testUser.Completed_Groups_Slide__c = true;
            testUser.Completed_Tours_Slide__c = true;
            update testUser;
        }

        system.runas(testUser){
            Tenable_ProfileProgressWrapper wrapper = Tenable_ProfileProgressController.getProgress();
            system.assert(wrapper.completedTour==true);
        }
    }
    
    @isTest
    public static void testGetUserRecord(){
        Utils.isTest = true;
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new TestResponseGenerator());
        User testUser = Peak_TestUtils.createStandardUser();
        insert testUser;
        test.stopTest();
        
        System.runAs(testUser){
            Tenable_ProfileProgressController.getUserRecord();
        }
    }
}