/*************************************************************************************
Class: evaluationTriggerHandler
Author: Roy Jones
Date: 02/14/2017
Details: This trigger upserts data on the Contact_Evaluation__c object
History: Modified the class by Bharathi.M on 3/21/2017 to add a new method copyEvalsToContact
         which updates the UUID field on Contact from the Eval object. 
         (94% code coverage)
***************************************************************************************/

public with sharing class evaluationTriggerHandler 
{

    public class evaluationAfterInsertHandler implements Triggers.Handler 
    {
        public void handle() 
        {
           copyEvalsToContact(trigger.new);
           UpdateUUIDOnContact(trigger.new);
        }
    }

    public class evaluationAfterUpdateHandler implements Triggers.Handler 
    {
        public void handle() 
        {
            copyEvalsToContact(trigger.new);
            UpdateUUIDOnContact(trigger.new);
        }
    }
    
    public static void UpdateUUIDOnContact(Evaluation__c[] evals)
    {
        map<Id,Contact> mapContact = new map<Id,Contact>();
        set<Id> contactIds = new set<Id>();
        map<Id,Contact> mapUpdateContact = new map<Id,Contact>();
        
        for(Evaluation__c ev : evals)
        {
            if(ev.Status__c == 'Activated')
            {
                contactIds.add(ev.Contact__c);
            }
        }
        
        mapContact = new map<Id,Contact>([SELECT Id,Pendo_Container_UUID__c FROM Contact WHERE Id in : contactIds]);
        
        for(Evaluation__c ev: evals)
        {
            if(mapContact.containsKey(ev.Contact__c))
            {
                Contact c = mapContact.get(ev.Contact__c);
                
                if(c.Pendo_Container_UUID__c == '' || c.Pendo_Container_UUID__c == NULL)
                {
                    Contact con = new Contact();
                    con.Id = c.Id;
                    con.Pendo_Container_UUID__c = ev.Container_UUID__c;
                    
                    if(!mapUpdateContact.containsKey(c.Id))
                    {
                    	mapUpdateContact.put(c.Id,con);
                    }
                }
            }
        }
        
        if(!mapUpdateContact.isEmpty())
        {
            update mapUpdateContact.values();
        }
    }
    
    public static void copyEvalsToContact(Evaluation__c[] evals)
    { 
        list<Contact_Evaluation__c> conEvalList = new list<Contact_Evaluation__c>();
        map<string, id> uuidMap = new map<string, id>();
        Contact_Evaluation__c conEval;
        set<string> uuidSet = new set<string>();
        
        for(Evaluation__c v: evals)
        {
            uuidSet.add(v.Container_uuid__c);
        }
        
        list<Contact_Evaluation__c> existingList = [select id, Container_uuid__c from Contact_Evaluation__c where Container_uuid__c =: uuidSet];
        for(Contact_Evaluation__c ce: existingList)
        {
            uuidMap.put(ce.Container_uuid__c, ce.Id);
        }
        
        for(Evaluation__c e: evals)
        {
            conEval = new Contact_Evaluation__c
            (
                Status__c               = e.Status__c, 
                Name                    = e.Name, 
                Marketo_Lead_Id__c      = e.Marketo_Lead_Id__c, 
                Location_Data__c        = e.Location_Data__c,
                Expiration_Date__c      = e.Expiration_Date__c, 
                Eval_Type__c            = e.Eval_Type__c, 
                Eval_Request_Type__c    = e.Eval_Request_Type__c, 
                Eval_Request_Id__c      = e.Eval_Request_Id__c, 
                Eval_Request_Date__c    = e.Eval_Request_Date__c, 
                Eval_Product__c         = e.Eval_Product__c, 
                Email__c                = e.Email__c,
                Date_Activation_Sent__c = e.Date_Activation_Sent__c, 
                Container_uuid__c       = e.Container_uuid__c, 
                Contact__c              = e.Contact__c, 
                Activation_URI__c       = e.Activation_URI__c, 
                Activation_DateTime__c  = e.Activation_DateTime__c, 
                Activated__c            = e.Activated__c 
            );
            
            if(uuidMap.get(e.Container_uuid__c) != null)
            {
                conEval.Id = uuidMap.get(e.Container_uuid__c);
            }
            
            if(conEval.container_uuid__c != null && conEval.container_uuid__c != '' && conEval.Contact__c != null)
            {
                conEvalList.add(conEval);
            }
        }
        
        try
        {
            upsert conEvalList;
        }
        catch(exception e)
        {
        }
    }
}