/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class TicketDetailsController {
    global TicketDetailsController() {

    }
    @RemoteAction
    global static Freshservice.remoteObject addComment(String tId, String note, String noteType, List<String> notify_emails) {
        return null;
    }
    @RemoteAction
    global static Freshservice.remoteObject createTicket(String dataString, String updateFlag, String tId, String eMail) {
        return null;
    }
    @RemoteAction
    global static Freshservice.remoteObject updateAgent(String username, String password) {
        return null;
    }
}
